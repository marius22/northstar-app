import requests
import unittest
import simplejson as json


APP_STATUS_URL = "https://eap.uat.everstring.com/app/status"
SERVICE_HOST = "http://service.uat.everstring.com"
SERVICE_STATUS_URL = SERVICE_HOST + "/northstar/status"
UI_URL = "https://eap.uat.everstring.com"
ENRICH_URL_1 = SERVICE_HOST + "/company_enrichment/analytics/"
ENRICH_URL_2 = SERVICE_HOST + "/company_enrichment/enrichment/"
ENRICH_URL_3 = SERVICE_HOST + "/company_enrichment/ns_contacts"
ENRICH_URL_4 = SERVICE_HOST + "/company_enrichment/filter"
ENRICH_URL_5 = SERVICE_HOST + "/company_enrichment/distinct_tech_count"
ENRICH_URL_6 = SERVICE_HOST + "/company_enrichment/metadata/get_company_id"
ENRICH_URL_7 = SERVICE_HOST + "/company_enrichment/metadata/tech/"
ENRICH_URL_8 = SERVICE_HOST + "/company_enrichment/metadata/industry/"
ENRICH_URL_9 = SERVICE_HOST + "/company_enrichment/metadata/location/"
ENRICH_URL_10 = SERVICE_HOST + "/company_enrichment/similar_domain"

#SERVICE_RECOMMENDATIOIN_URL="https://eap.uat.everstring.com/service/jobs/10020/recommendations"


class SmokeTest(unittest.TestCase):
    headers = {'Content-Type': 'application/json'}

    def setUp(self):
        pass

    def test_app_status(self):
        resp = requests.get(APP_STATUS_URL)
        self.assertEqual(resp.status_code, 200, msg="response code isn't 200")
        content = json.loads(resp.content)
        self.assertEqual(content.get("status"), "OK", msg="\"status : OK\" isn't in response body")

    def test_service_status(self):
        resp = requests.get(SERVICE_STATUS_URL)
        self.assertEqual(resp.status_code, 200, msg="response code isn't 200")
        content = json.loads(resp.content)
        self.assertEqual(content.get("status"), 'OK', msg="'status : OK' isn't in response body")

    def test_ui(self):
        resp = requests.get(UI_URL)
        self.assertEqual(resp.status_code, 200, msg="response code isn't 200")

    def test_enrich_analytics(self):
        domain = 'everstring.com'
        data = {'seedDomainList': [domain]}
        resp = requests.post(ENRICH_URL_1, headers=self.headers, json=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        self.assertGreaterEqual(len(resp.content), 100, "returned content length is less than 100")

    def test_enrich_enrichment(self):
        domain = 'everstring.com'
        data = {'seedDomainList': [domain]}
        resp = requests.post(ENRICH_URL_2, headers=self.headers, json=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        self.assertGreaterEqual(len(resp.content), 100, "returned content length is less than 100")

    def test_enrich_ns_contacts(self):
        company_id = '1073986465'

        data = {
            'companyIdList': [company_id],
            'limitPerCompany': 2,
            'accountId': 1
        }

        resp = requests.post(ENRICH_URL_3, headers=self.headers, json=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        self.assertGreaterEqual(len(resp.content), 100, "returned content length is less than 100")

    def test_enrich_filter(self):
        data = {
            'where': '{"industry": {"$in": ["Culture"]}, "state": {"$in": ["California"]}}'
        }
        resp = requests.get(ENRICH_URL_4, params=data)
        self.assertEqual(resp.status_code, 200, "reponse code isn't 200")
        resp_data = json.loads(resp.content)
        self.assertGreater(resp_data.get("data").get("count"), 0, "company data isn't greater than 0")


    def test_enrich_distinct_tech_count(self):
        company_id = '1073986465'

        data = {
            'companyIdList': [company_id]
        }
        resp = requests.post(ENRICH_URL_5, headers=self.headers, json=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        resp_data = json.loads(resp.content)
        self.assertIsNotNone(resp_data.get('data').get('techDistinct'), "response doesn't contain techDistinct")

    def test_meta_get_company_id(self):
        domains = "ibm.com,everstring.com"
        data = {
            "domains": domains
        }
        resp = requests.get(ENRICH_URL_6, headers=self.headers, params=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        resp_data = json.loads(resp.content)
        resp_dict = dict()
        for i in resp_data.get("data"):
            resp_dict[i['companyDomain']] = i['companyId']

        for domain in domains.split(','):
            if domain not in resp_dict:
                self.fail(msg="domain {0} not in resposne".format(domain))

    def test_meta_tech(self):
        resp = requests.get(ENRICH_URL_7)
        self.assertEqual(resp.status_code, 200, "reponse code isn't 200")
        resp_data = json.loads(resp.content)
        self.assertIsNotNone(resp_data.get("data").get("categories"), "'categories' is empty or None")

    def test_meta_industry(self):
        resp = requests.get(ENRICH_URL_8)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        resp_data = json.loads(resp.content)
        self.assertIsNotNone(resp_data.get("data").get("categories"), "'categories' is empty or None")

    def test_meta_location(self):
        resp = requests.get(ENRICH_URL_9)
        self.assertEqual(resp.status_code, 200, "reponse code isn't 200")
        resp_data = json.loads(resp.content)
        self.assertIsNotNone(resp_data.get("data").get("states"), "'states' is empty or None")

    def test_enrich_similar_domain(self):
        domains = "ibm.com,apple.com"
        data = {
            'domains': domains
        }
        resp = requests.get(ENRICH_URL_10, params=data)
        self.assertEqual(resp.status_code, 200, "response code isn't 200")
        resp_data = json.loads(resp.content)
        resp_set = set()
        for i in resp_data.get('data'):
            resp_set.add(i.get("domain"))

        for domain in domains.split(","):
            if domain not in resp_set:
                self.fail(msg="domain {0} not in result set".format(domain))


if __name__ == '__main__':
    import nose2
    nose2.main()

