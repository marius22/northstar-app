#
#===============================================================
#
# @Description: smoke test for the northstar UAT environment
#
#===============================================================
#

sleep 20s
set +e

HOST=https://northstar.uat.everstring.com
APP_BASE=${HOST}/app
SERVICE_BASE=http://service.uat.everstring.com/northstar
COMPANY_ENRICH_BASE=http://service.uat.everstring.com/company_enrichment

APP_STATUS_URL=${APP_BASE}/status
SERVICE_STATUS_URL=${SERVICE_BASE}/status

ENRICH_URL_1=${COMPANY_ENRICH_BASE}/analytics/
ENRICH_URL_2=${COMPANY_ENRICH_BASE}/enrichment/
ENRICH_URL_3=${COMPANY_ENRICH_BASE}/ns_contacts

INVALID_SERVICE_BASE=http://service.uat.everstring.com/northstar

response=$(curl --write-out %{http_code} --silent --output /dev/null $INVALID_SERVICE_BASE/status)
echo INVALID_SERVICE_BASE: $response
if [ $response -e 200 ]; then
    echo Should hide the invalid service url:$INVALID_SERVICE_BASE
    exit -1
fi

echo
# curl -v $APP_STATUS_URL
response=$(curl --write-out %{http_code} --silent --output /dev/null $APP_STATUS_URL)
echo APP_STATUS: $response
if [ $response -ne 200  ]; then
    exit -1
fi
response_body=$(curl --silent $APP_STATUS_URL)
if echo $response_body | grep -q '"status": "OK"'
then
    echo APP_STATUS: OK
else
    exit -1
fi


echo
# curl -v $SERVICE_STATUS_URL
response=$(curl --write-out %{http_code} --silent --output /dev/null $SERVICE_STATUS_URL)
echo SERVICE_STATUS: $response
if [ $response -ne 200  ]; then
    exit -1
fi
response_body=$(curl --silent $SERVICE_STATUS_URL)
if echo $response_body | grep -q '"status": "OK"'
then
    echo SERVICE_STATUS: OK
else
    exit -1
fi


echo
# curl -v $HOST
response=$(curl --write-out %{http_code} --silent --output /dev/null $HOST)
echo UI_STATUS: $response
if [ $response -ne 200  ]; then
    exit -1
fi


domain="everstring.com"
echo
# curl  -v -X POST -H "Content-Type: application/json" -d '{"seedDomainList": ["alf.org"]}' $ENRICH_URL
response=$(curl --write-out %{http_code} --silent --output /dev/null -X POST -H "Content-Type: application/json" -d '{"seedDomainList": ["'"$domain"'"]}' $ENRICH_URL_1)
echo ENRICH_COMPANIES_SERVICE_STATUS-ANALYTICS: $response
if [ $response -ne 200  ]; then
    exit -1
fi
response_body=$(curl --silent -X POST -H "Content-Type: application/json" -d '{"seedDomainList": ["'"$domain"'"]}' $ENRICH_URL_1)
if [ ${#response_body} -gt 100 ]; then
    echo ENRICH_COMPANIES_SERVICE_STATUS-ANALYTICS: OK
else
    exit -1
fi


echo
response=$(curl --write-out %{http_code} --silent --output /dev/null -X POST -H "Content-Type: application/json" -d '{"seedDomainList": ["'"$domain"'"]}' $ENRICH_URL_2)
echo ENRICH_COMPANIES_SERVICE_STATUS-ENRICHMENT: $response
if [ $response -ne 200  ]; then
    exit -1
fi
response_body=$(curl --silent -X POST -H "Content-Type: application/json" -d '{"seedDomainList": ["'"$domain"'"]}' $ENRICH_URL_2)
if echo $response_body | grep -q $domain
then
    echo ENRICH_COMPANIES_SERVICE_STATUS-ENRICHMENT: OK
else
    exit -1
fi


companyId=1073986465
echo
# curl -X POST -H "Content-Type: application/json" -d '{"companyIdList": [1073986465, 1074091551], "limitPerCompany": 2}' http://service.uat.everstring.com/company_enrichment/ns_contacts/
response=$(curl --write-out %{http_code} --silent --output /dev/null -X POST -H "Content-Type: application/json" -d '{"companyIdList": ["'"$companyId"'"], "limitPerCompany": 2, "accountId":1}' $ENRICH_URL_3)
echo ENRICH_COMPANIES_SERVICE_STATUS-CONTACTS: $response
if [ $response -ne 200  ]; then
    exit -1
fi
response_body=$(curl --silent -X POST -H "Content-Type: application/json" -d '{"companyIdList": ["'"$companyId"'"], "limitPerCompany": 2, "accountId":1}' $ENRICH_URL_3)
if echo $response_body | grep -q $companyId
then
    echo ENRICH_COMPANIES_SERVICE_STATUS-CONTACTS: OK
else
    exit -1
fi


# Setup
cd $WORKSPACE/api
virtualenv ../env
source ../env/bin/activate
pip install -r requirements.txt
cp $JENKINS_HOME/properties_dev/northstar-app/test.config.py instance/test.config.py
cp $JENKINS_HOME/properties_dev/northstar-app/test.config-service.py instance/test.config-service.py
# check alembic heads
cd $WORKSPACE/api
cp -n alembic.ini.example alembic.ini
HEAD_COUNT=`alembic heads | wc -l`
if [ $HEAD_COUNT -ne 1 ]; then
    echo 'multiple heads'
    exit -1
fi
