#How to Use?

##Install the requirements

```
$ pip install -r requirements.txt
```

##Code Structure

```
➜  tree ./test/functional/selenium -d
./test/functional/selenium
├── pages
├── tests
└── util
```

All the pages are implemented here.
It includes two parts: the actions in the page and the validation in the page.
All the code relevant Selenium are here.
```
pages
```

All the test case are implemented here.
It bases on the end-end user tests.
It invoke the pages which is defined to implement the test cases.
```
tests
```

All the utilities are here .
```
util
```

