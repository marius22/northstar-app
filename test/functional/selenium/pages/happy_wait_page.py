# -*- coding: utf-8 -*-

import uuid

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class HappyWaitPage(BasePage):
    path = '/?#/segment/create/success'
    _console_button = find_by(xpath="//button[contains(text(),'Go to Predictive Segment Console')]")

    def gotoSegmentListPage(self):
        self._console_button().click()

