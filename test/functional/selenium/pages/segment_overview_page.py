# -*- coding: utf-8 -*-

import uuid

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class SegmentOverviewPage(BasePage):
    _tab_company_list = find_by(xpath="//a[contains(@ui-sref,'segment.overviewTabs.companyList')]")
    _tab_seed_list = find_by(xpath="//a[contains(@ui-sref,'segment.overviewTabs.seedList')]")


    _tab_galaxy_list = find_by(xpath="//a[contains(@ui-sref,'segment.overviewTabs.galaxyView')]")
    _tab_overview_list = find_by(xpath="//a[contains(@ui-sref,'segment.overviewTabs.overview')]")