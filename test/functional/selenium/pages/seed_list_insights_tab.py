# -*- coding: utf-8 -*-



from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class SeedListInsightsTab(BasePage):
    _chart_link = find_by(xpath="//i[contains(@class,'btr bt-pie-chart')]")