# -*- coding: utf-8 -*-

import uuid
import time

from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class SegmentCreationPage(BasePage):
    path = "/?#/segment/create/upload"

    _segment_name_input = find_by(xpath="//input")
    _create_button = find_by(xpath="//button[contains(text(),'Create')]")

    # _upload_seed_from_csv = find_by(xpath="//li[@ng-click='uc.selectCSV()']")
    _upload_seed_from_csv = find_by(xpath="//ul[@class='select-type']/li[1]")
    _upload_seed_from_sfdc = find_by(xpath="//li[@ng-click='uc.openSFDCOauthWindow()']")


    _upload_box = find_by(xpath="//div[@class='drop-box']")
    _browser_select_csv_btn = find_by(link_text="click")
    _input_file = find_by(xpath="//input[contains(@type,'file')]")

    _upload_success_message = find_by(class_name="upload-success-message")

    _next_button = find_by(class_name="next-button")

    _run_button = find_by(xpath="//button[contains(@ng-click,'sic.runModel()')]")



    def name_segment(self):
        self.wait_for_element_ready(self._segment_name_input)
        self._segment_name_input().send_keys("auto test " + str(uuid.uuid1()))
        self._create_button().click()

    def is_ready_upload_csv(self):
        return True if self._upload_box() else False

    def upload_seed_from_csv(self, csv_path):
        print '..... 1'
        self.wait_for_element_ready(self._upload_seed_from_csv)
        print '..... 2'
        time.sleep(5)  # very important to wait the page render complete
        print '..... 3'
        self._upload_seed_from_csv().click()
        print '..... 4'
        # self._driver.execute_script("document.querySelector('.select-type li').click()")
        # ActionChains(self._driver).move_to_element(self._upload_seed_from_csv()).click().perform()

        # print '+++++++++', self._driver.current_url

        self.wait_for_element_ready(self._input_file)
        print '..... 5'
        self._input_file().send_keys(csv_path)
        print '..... 6'

    def next(self):
        self._next_button().click()
        time.sleep(5)

    def is_upload_successful(self):
        return True if self._upload_success_message() else False

    def start_model_run(self):
        self._run_button().click()
        time.sleep(5)