# -*- coding: utf-8 -*-


from util.pageobject_support import cacheable, callable_find_by as find_by

class PageHeader(object):
    _drop_down = find_by(xpath="//i[contains(@class,'btr bt-angle-down')]")
    _logout_button = find_by(xpath="//span[contains(text(),'Log Out')]")

    def __init__(self, driver, username):
        self._driver = driver
        self._username = username
        #self.locator_current_user_name = '//div[contains(@class, "profile-name") and contains(text(), "{}")]'.format(self._username)
        self.locator_current_user_name = '//div[contains(@class, "profile-name") and text()[contains(., "{}")]]'.format(self._username)

    @property
    def is_current_user_shown(self):
        element_current_user_name = self._driver.find_elements_by_xpath(self.locator_current_user_name)
        return True if len(element_current_user_name)==1 else False

    def logout(self):
        self._drop_down().click()
        self._logout_button().click()