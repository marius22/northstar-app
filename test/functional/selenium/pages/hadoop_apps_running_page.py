from base_page import BasePage
from util.pageobject_support import cacheable, callable_find_by as find_by


class HadoopAppsRunningPage(BasePage):

    def goto(self):
        self._driver.get("http://master01.prod.everstring.com:8088/cluster/apps/RUNNING")
        self._driver.implicitly_wait(30)

    def isRunning(self, appName):
        s = "//td[contains(normalize-space(text()), '" + appName + "')]"
        l = len(self._driver.find_elements_by_xpath(xpath=s))
        if l > 0:
            return True
        else:
            return False