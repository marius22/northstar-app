# -*- coding: utf-8 -*-

"""
@author: xz
@file: login_page.py
@time: 3/25/16 1:51 AM
"""
import time

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class LoginPage(BasePage):

    path= "/?#/user/login"
    _user_name = find_by(xpath="//input[contains(@ng-model,'lsu.username')]")
    _pass = find_by(xpath="//input[contains(@type,'password')]")
    _login_button = find_by(xpath="//button[@class='submit' and contains(text(),'Log In')]")
    _drop_down = find_by(xpath="//i[contains(@class,'btr bt-angle-down')]")

    def login(self, username, password):
        self._user_name().send_keys(username)
        self._pass().send_keys(password)
        self._login_button().click()
        # self.wait_for_element_ready(self._drop_down)


