# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from selenium import webdriver
import time
import uuid

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage


class SegmentListPage(BasePage):

    path = "/?#/segment/console"

    _request_button = find_by(xpath="//button[@class='request-btn']")

    _add_button = find_by(xpath="//span[contains(text(),'Add')]")
    _create_segment_button_locator = "//button[@class='create-segment']"
    _create_segment_button = find_by(xpath=_create_segment_button_locator)

    def is_open(self):
        return True if self.path in self._driver.current_url else False

    def is_blank(self):
        return True if len(self._driver.find_elements_by_xpath(self._create_segment_button_locator)) > 0 else False

    def add_segment(self):
        if self.is_blank():
            self._create_segment_button().click()
        else:
            self._add_button().click()

    def view_segment(self):
        self.wait_for_element_ready(self._tab_company_list)
        self._tab_company_list().click()
        self._tab_seed_list().click()
        self._pie_chart().click()
        self._tab_galaxy_list().click()

    def request_upgrade(self):
        self.wait_for_element_ready(self._request_button)
        self._request_button().click()






