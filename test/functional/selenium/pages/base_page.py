# -*- coding: utf-8 -*-
import time

from selenium.webdriver.remote.webelement import WebElement


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""
    path = ""

    def __init__(self, _driver, host):
        self._driver = _driver
        self.host = host

    @classmethod
    def wait_for_element_ready(cls, ui_element):
        i = 0
        while not isinstance(ui_element(), WebElement) and i < 30:
            time.sleep(2)
            print "wait for element"
            i += 1
        assert i < 30

    def wait_for_page_open(self):
        i = 0
        while not self.isOpen() and i < 30:
            time.sleep(2)
            print "wait for page"
            i += 1
        assert i < 30

    def open(self):
        self._driver.get(self.host + self.path)

    def isOpen(self):
        # print '------   ', self.path, 'in', self._driver.current_url
        return True if self.path in self._driver.current_url else False

    def go_to(self, path):
        self._driver.get(self.host + path)
