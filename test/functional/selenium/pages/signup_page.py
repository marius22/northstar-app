# -*- coding: utf-8 -*-

"""
@author: xz
@file: SignupPage.py
@time: 3/25/16 2:23 AM
"""
import time

from util.pageobject_support import cacheable, callable_find_by as find_by
from base_page import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class SignUpPage(BasePage):
    path = "/?#/user/sign-up"
    _user_name = find_by(xpath="//input[contains(@ng-model,'lsu.username')]")
    _password = find_by(xpath="//input[contains(@type,'password')]")
    _signup_link = find_by(xpath='//button[contains(@href,"sign-up")]')
    _signup_submit_button = find_by(xpath="//button[@class='submit' and contains(text(),'Sign Up')]")

    def signUp(self, username, password):
        # print '.....', self._driver.current_url
        self._user_name().send_keys(username)
        time.sleep(1)
        self._password().send_keys(password)
        time.sleep(1)
        self._signup_submit_button().click()
        time.sleep(3)

    def open(self):
        super(SignUpPage, self).open()
        # print '1111 ' + self._driver.current_url
        if self.path not in self._driver.current_url:
            self._signup_link().click()
            # print '2222 ' + self._driver.current_url

    @property
    def is_signup_submit_found(self):
        return True if self._signup_submit_button() else False