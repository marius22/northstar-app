from simple_salesforce import Salesforce
import logging.config

logging.config.fileConfig('logging.conf')

class SFAPI:

    def __init__(self, username, password, security_token):
        self.logger = logging.getLogger('file')
        self.sf = Salesforce(username=username, password=password,security_token=security_token)

    def create_lead(self, lastName, email,company):
        self.sf.Lead.create({'LastName':lastName,'Email':email, 'Company': company})
        leadId = self.sf.query("select Id from Lead WHERE email = '%s'" % email)
        self.logger.info("Create Lead %s", leadId)

    def create_contact(self, lastName, email):
        self.sf.Contact.create({'LastName':lastName,'Email':email})
        contactId = self.sf.query("select Id from Contact WHERE email = '%s'" % email)
        self.logger.info("Create Contact %s", contactId)

    def create_account(self, name):
        self.sf.Account.create({'name':name})
        accountId = self.sf.query("select Id from Account WHERE name = '%s'" % name)
        self.logger.info("Create Account %s", accountId)

    def delete_leads_by_lastName(self, lastName):
        leads = self.sf.query("select Id from Lead WHERE LastName = '%s'" % lastName)
        for lead in leads['records']:
            self.sf.Lead.delete(lead['Id'])
            self.logger.info("Lead %s is deleted.", lead['Id'])

    def delete_contacts_by_lastName(self, lastName):
        contacts = self.sf.query("select Id from Contact WHERE LastName = '%s'" % lastName)
        for contact in contacts['records']:
            self.sf.Contact.delete(contact['Id'])
            self.logger.info("Contact %s is deleted.", contact['Id'])

    def delete_accounts_by_name(self, name):
        accounts = self.sf.query("select Id from Account WHERE Name = '%s'" % name)
        for account in accounts['records']:
            self.sf.Account.delete(account['Id'])
            self.logger.info("Account %s is deleted.", account['Id'])

    def get_leads(self, filter):
        leads = self.sf.query("select Id, LastName, FirstName, Title, Phone, Email from Lead WHERE %s" % filter)
        self.logger.info("Get leads %s", leads)
        return leads['records']

    def get_contacts(self, filter):
        contacts = self.sf.query("select Id, LastName, FirstName, Title, Phone, Email from Contact WHERE %s" % filter)
        self.logger.info("Get contacts %s", contacts)
        return contacts['records']

    def get_accounts(self, filter):
        accounts = self.sf.query("select Id,NAME from Account WHERE %s" % filter)
        self.logger.info("Get accounts %s", accounts)
        return accounts['records']

# if __name__ == '__main__':
#     sf = SFAPI("li.l_test2@everstring.com", "Everstring123","MMwBkTetbYYafljeDE9CKWRF")

    # sf.create_lead("Li", "testLi1@es.com", "es")
    # sf.create_account("es1")
    # sf.create_contact("Li", "testLi4@es.com")

    # contacts = sf.get_contacts("LastName != null")
    # for contact in contacts:
    #     print contact['Email']

    # leads = sf.get_leads("LastName != null")
    # for lead in leads:
    #     print lead['Email']

    # accounts = sf.get_accounts("Name != null")
    # for account in accounts:
    #     print account['Name']

    # sf.delete_contacts_by_lastName("Li")
    # contacts = sf.get_contacts("LastName != null")
    # for contact in contacts:
    #     print contact['Email']

    # sf.delete_leads_by_lastName("Li")
    # leads = sf.get_leads("LastName != null")
    # for lead in leads:
    #     print lead['Email']

    # sf.delete_accounts_by_name("es")
    # accounts = sf.get_accounts("Name != null")
    # for account in accounts:
    #     print account['Name']


