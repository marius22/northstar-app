# -*- coding: utf-8 -*-

"""
@author: xz
@file: generator.py
@time: 3/25/16 9:29 PM
"""
from faker import Factory
import uuid


class Generator(object):

    faker = Factory.create()

    def __int__(self):
        pass

    @classmethod
    def free_email(cls):
        return cls.faker.freeEmail()

    @classmethod
    def company_email(cls):
        return cls.faker.companyEmail()

    @classmethod
    def uuid(cls):
        return str(uuid.uuid1())

    @classmethod
    def phone(cls):
        _phone = cls.faker.phoneNumber()
        try:
            ind = _phone.index("x")
            return _phone[:ind]
        except:
            return _phone

    @classmethod
    def first_name(cls):
        return cls.faker.firstName()

    @classmethod
    def last_name(cls):
        return cls.faker.lastName()

