# -*- coding: utf-8 -*-

"""
@author: xz
@file: test_signup_page.py
@time: 3/26/16 3:18 PM
"""

import unittest

from test_base import TestBase
from pages.signup_page import SignUpPage
from pages.page_header import PageHeader
from util.generator import Generator



class TestSignUpPage(TestBase):
    def setUp(self):
        super(TestSignUpPage, self).setUp()
        self.signup_page = SignUpPage(self._driver, self.host)
        self.signup_page.open()

    def tearDown(self):
        self.signup_page = None
        super(TestSignUpPage, self).tearDown()

    def test_open(self):
        self.assertTrue(self.signup_page.path in self._driver.current_url, "URL is not right.")
        self.assertTrue(self.signup_page.is_signup_submit_found, 'Sign Up submit button was not found!')

    def test_signup(self):
        username = Generator.company_email()
        print username
        self.signup_page.signUp(username, self.password)
        # print self._driver.current_url
        head = PageHeader(self._driver, username)
        self.assertTrue(head.is_current_user_shown)


if __name__ == '__main__':
    unittest.main()
