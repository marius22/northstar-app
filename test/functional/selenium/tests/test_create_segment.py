# -*- coding: utf-8 -*-

import unittest
import os

from test_base import TestBase
from pages.segment_list_page import SegmentListPage
from pages.signup_page import SignUpPage
from pages.segment_creation_page import SegmentCreationPage
from pages.page_header import PageHeader
from pages.happy_wait_page import HappyWaitPage

from util.generator import Generator


class TestCreateSegment(TestBase):

    def test_create_segment(self):
        print 1111
        signup_page = SignUpPage(self._driver, self.host)
        signup_page.open()
        cmpEMail = Generator.company_email()
        signup_page.signUp(cmpEMail, self.password)

        print 2222
        segment_list_page = SegmentListPage(self._driver, self.host)
        self.assertTrue(segment_list_page.is_open())

        print 3333
        header = PageHeader(self._driver, cmpEMail)
        self.assertTrue(header.is_current_user_shown)

        segment_list_page.add_segment()

        print 4444
        segment_creation_page = SegmentCreationPage(self._driver, self.host)
        segment_creation_page.wait_for_page_open()

        print 5555
        segment_creation_page.name_segment()
        segment_creation_page.upload_seed_from_csv(os.getcwd() + "/30domains.csv")

        self.assertTrue(segment_creation_page.is_upload_successful())

        print 6666
        segment_creation_page.next()
        segment_creation_page.start_model_run()

        print 7777
        happy_wait_page = HappyWaitPage(self._driver, self.host)
        self.assertTrue(happy_wait_page.isOpen())


if __name__ == '__main__':
    unittest.main()