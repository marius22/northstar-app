from unittest import TestCase
from selenium import webdriver


class TestBase(TestCase):
    # host = "https://northstar.uat.everstring.com"
    host = "https://ns.everstring.com"
    # signup_url = "https://northstar.uat.everstring.com/#/user/sign-up"
    # login_url = "https://northstar.uat.everstring.com/#/user/login"
    # logout_url= "https://northstar.uat.everstring.com/#/user/logout"
    # console_url="https://northstar.uat.everstring.com/#/segment/console"
    # segment_detail_url = "https://northstar.uat.everstring.com/#/segment/416/overview"
    username = "pu@everstring.com"
    password = "123456"

    username0 = "jason27@everstring.com"
    password0 = "1"

    def setUp(self):
        # please download and unzip it, http://chromedriver.storage.googleapis.com/2.21/chromedriver_mac32.zip
        # self._driver = webdriver.Chrome(executable_path='/Users/peter/apps/chromedriver')
        # self._driver.maximize_window()
        self._driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true', '--web-security=false'])
        self._driver.set_window_size(1920, 1200)
        self._driver.implicitly_wait(30)


    def tearDown(self):
        self._driver.close()
        self._driver.quit()
