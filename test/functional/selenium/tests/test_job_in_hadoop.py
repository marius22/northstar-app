import unittest
from test_base import TestBase
from pages.hadoop_apps_running_page import HadoopAppsRunningPage


class TestJobSubmission(TestBase):

    def test_job_exists(self):
        hadoop_running_apps_page = HadoopAppsRunningPage(self._driver)
        hadoop_running_apps_page.goto()
        # TODO need to get the job Id from the DB.
        self.assertTrue(hadoop_running_apps_page.isRunning('ModelFlow-427'))


if __name__ == '__main__':
    unittest.main()
