# -*- coding: utf-8 -*-

"""
@author: xz
@file: test_view_segment.py
@time: 3/25/16 5:34 PM
"""
import unittest
import time

from test_base import TestBase
from pages.login_page import LoginPage
from pages.segment_list_page import SegmentListPage


class TestViewSegment(TestBase):

    def test_view_segment(self):
        login_page = LoginPage(self._driver, self.host)
        login_page.open()
        login_page.login(self.username,self.password)
        segment_list_page = SegmentListPage(self._driver, self.host)
        segment_list_page.go_to("/#/segment/406/overview")
        segment_list_page.view_segment()

if __name__ == '__main__':
    unittest.main()