# -*- coding: utf-8 -*-

"""
@author: xz
@file: test_login_page.py
@time: 3/25/16 5:30 PM
"""
import unittest

from test_base import TestBase

from pages.login_page import LoginPage
from pages.page_header import PageHeader


class TestLogin(TestBase):

    def test_login(self):
        login_page = LoginPage(self._driver, self.host)
        login_page.open()
        self.assertTrue(login_page.path in self._driver.current_url, "URL is not right.")
        login_page.login(self.username, self.password)
        head = PageHeader(self._driver, self.username)
        self.assertTrue(head.is_current_user_shown)

if __name__ == '__main__':
    unittest.main()