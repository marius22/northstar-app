# -*- coding: utf-8 -*-

import requests
import random


host_url = 'https://eap.uat.everstring.com'
base_url = host_url + '/app'
accountId = 4
user_email = '5342476@qq.com'
user_password = '111111'
to_verify = False
s = requests.session()


def signup():
    # signup
    signup_url = base_url + '/users/register'
    payload = {"email": user_email, "password": user_password, "sendEmail": False}
    r = s.post(signup_url, payload, verify=to_verify)
    # print r
    return r


def login():
    # login
    login_url = base_url + '/users/login'
    payload = {"email": user_email, "password": user_password}
    r = s.post(login_url, json=payload, verify=to_verify)
    print r.url, r.status_code, r.elapsed
    return r.status_code


def create_segment(segment_name):
    create_segment_url = base_url + '/segments'
    payload = {"segmentName": segment_name, "accountId": accountId}
    r = s.post(create_segment_url, json=payload, verify=to_verify, timeout=600)
    print r.url, r.status_code, r.elapsed
    if r.status_code != requests.codes.ok:
        print r
        return -1
    seg_id = r.json()["data"]["segmentId"]
    return seg_id


all_states = ["Alaska", "Texas", "California", "Montana", "New Mexico", "Arizona", "Nevada", "Colorado", "Oregon",
              "Wyoming", "Michigan", "Minnesota", "Utah", "Idaho", "Kansas", "Nebraska", "South Dakota",
              "Washington", "North Dakota", "Oklahoma", "Missouri", "Florida", "Wisconsin", "Georgia", "Illinois",
              "Iowa", "New York", "North Carolina", "Arkansas", "Alabama", "Louisiana", "Mississippi",
              "Pennsylvania", "Ohio", "Virginia", "Tennessee", "Kentucky", "Indiana", "South Carolina",
              "West Virginia", "Maryland", "Hawaii", "Massachusetts", "Vermont", "New Hampshire", "New Jersey",
              "Connecticut", "Delaware", "Rhode Island", "District of Columbia", "Puerto Rico",
              "Northern Mariana Islands", "US Virgin Islands", "American Samoa", "Guam"]
all_employee_size = ["1-10", "11-50", "51-200", "201-500", "501-1000", "1001-5000", "5001-10000", "10000+"]
all_industries = ["Accounting", "Agriculture", "Airlines/Aviation", "Apparel & Fashion", "Automotive", "Biotechnology",
                  "Building Materials", "Business Service", "Business Supplies and Equipment", "Chamber", "Chemicals",
                  "Civil Engineering", "Commercial Real Estate", "Computer Hardware", "Computer Network & Security",
                  "Computer Software", "Construction", "Consumer Electronics", "Consumer Goods", "Consumer Services",
                  "Cosmetics", "Cultural", "Culture", "Defense & Space", "Education",
                  "Electrical/Electronic Manufacturing", "Energy", "Environmental Services", "Facilities Services",
                  "Farming", "Finance", "Financial Services", "Food & Beverages", "Furniture", "Gambling & Casinos",
                  "Glass, Ceramics & Concrete", "Government", "Health, Wellness and Fitness", "Healthcare",
                  "Hospital & Health Care", "Hospitality", "Human Resources", "Individual & Family Services",
                  "Industrial Automation", "Information Technology and Services", "Insurance", "Internet", "Legal",
                  "Leisure, Travel & Tourism", "Logistics and Supply Chain", "Luxury Goods & Jewelry", "Machinery",
                  "Management Consulting", "Manufacturing", "Maritime", "Marketing and Advertising",
                  "Mechanical or Industrial Engineering", "Media", "Media and Entertainment", "Medical Devices",
                  "Metals & Minerals", "Mining & Metals", "Municipal", "Oil & Energy", "Organizations",
                  "Outsourcing/Offshoring", "Packaging and Containers", "Paper & Forest Products", "Pharmaceuticals",
                  "Plastics", "Printing", "Public Relations and Communications", "Real Estate", "Realestate",
                  "Religious Institutions", "Research", "Restaurants", "Retail", "Security and Investigations",
                  "Software", "Sporting Goods", "Staffing and Recruiting", "Telecom", "Telecommunications", "Tobacco",
                  "Transportation", "Transportation/Trucking/Railroad", "Utilities", "Wholesale"]
all_department = ["Administrative", "IT", "Education", "Engineering", "Finance", "HR", "Legal", "Marketing",
                  "Health", "Operations", "Other", "Ownership & Board", "R&D", "Sales"]
all_tech = ["123ContactForm", "1ShoppingCart", "247 Media", "2Checkout", "33 Across", "3D Cart", "3D Systems",
                 "500 Friends", "A Small Orange", "AB Tasty", "Acquia", "AcquireTM", "Acquisio", "Act-On", "Acteva",
                 "Actinic", "Active Conversion", "Active Network", "activEngage", "Acxiom", "Adap.TV", "AdBrite",
                 "AdCash", "Adconion", "AddShoppers", "AddThis", "Adestra", "Adform", "adgoal", "Adicio", "Adknowledge",
                 "adloox", "Adobe AudienceManager", "Adobe Business Catalyst", "Adobe Campaign", "Adobe Coldfusion",
                 "Adobe Connect", "Adobe CQ", "Adobe Dynamic Tag Management", "Adobe EchoSign", "Adobe Edge",
                 "Adobe Media Optimizer", "Adobe Test&Target", "Adotube", "ADP AdvancedMD", "ADP Employease",
                 "ADP Payroll", "ADP Workforce Now", "AdParlor", "adrolays", "AdRoll", "Adtegrity",
                 "Advanced Hosters DNS", "Advantage Media", "Advertising.com", "Adyen", "Adyoulike", "AdZerk",
                 "Affiliate Window", "affilinet", "Agile CRM", "AgilOne", "Airbrake", "Akamai", "Akamai DNS",
                 "AlphaSSL", "Amadeus", "Amazon Associates", "Amazon AWS", "Amazon CloudFront",
                 "Amazon Elastic Load Balancing", "Amazon Payments", "Amazon S3", "Amazon SES", "Amazon Webstore",
                 "Amazon Widgets", "AmeriCommerce", "Amplience", "Amplitude", "AngularJS", "Animoto", "Apache",
                 "Apache Solr", "AppDynamics", "ApplicantPro", "AppNexus", "AppRiver", "Apptix", "Aptimize", "ArcGIS",
                 "Ascentium Commerce Server", "ASP.NET", "AspDotNetStorefront", "AT Internet", "Atex", "Athenahealth",
                 "ATT DNS", "AudienceScience", "Autotask", "Availo", "AvanGate", "AvantLink", "Avature", "Aviary",
                 "Avvo", "AWeber", "Azure", "Backbase", "Background Sound", "Badgeville", "Baidu Maps", "BambooHR",
                 "Barilliance", "BaseKit", "Baynote", "Bazaarvoice", "BBB Seal", "BigCartel", "BigCommerce", "BigDoor",
                 "Binero", "Bing Maps", "BirdDog", "BitGravity", "BitPay", "Bitrix", "Bits on the Run", "Bizrate",
                 "Black Lotus", "Blackbaud", "Blackboard", "BlackMesh", "Blip TV", "Blogger", "BloomReach",
                 "Blue State Digital", "BlueHornet", "BlueHost", "BlueKai", "Blueknow", "BlueTie", "Bold Chat",
                 "BorderFree", "Bounce Exchange", "Box", "Brainshark", "Braintree", "Brandscreen", "Brightcove",
                 "BrightEdge", "Brightpearl", "BrightRoll", "Brilig", "Bronto", "Buddy Media", "BuddyPress", "BugHerd",
                 "Bullhorn", "Bunchball", "buuteeq", "BuySafe", "BuySell Ads", "C3 Metrics", "CacheFly", "CallRail",
                 "CallSource", "CallTracks", "Campaign Monitor", "CampaignerCRM", "CampaignMonitor", "Carbon",
                 "CareCloud", "Carpathia Hosting", "CartoDB", "Catchpoint", "CCBill", "CDN77", "CDNetworks", "CDNVideo",
                 "Celerant", "Celeros", "CentUp", "CenturyLink", "Ceridian", "Cerner", "Certona", "Chango",
                 "ChannelAdvisor", "Channeltivity", "Chargify", "Chartbeat", "Chase Paymentech Seal", "CheetahMail",
                 "ChinaCache", "ChinaNetCenter", "Chitika", "Cision", "Citrix NetScaler", "ClearSlide", "Clickability",
                 "ClickBank", "Clickbooth", "ClickDensity", "ClickDesk", "ClickDimensions", "Clicksor", "ClickTale",
                 "Clicky", "CloudFlare", "CloudFlare CDN", "CloudFlare DNS", "CloudFlare Hosting", "Cloudinary",
                 "CNN Media", "Cobalt", "Coinbase", "Collective Media", "Colt", "Comcast", "Comm100", "CommentLuv",
                 "Communicator Corp", "Compete", "comScore", "Concrete5", "Confirmit", "Connectria", "Constant Contact",
                 "Contenido", "Content ad", "Convertro", "Convio", "Conviva", "CoreCommerce", "Coremetrics",
                 "CoreMotives", "Cornerstone Ondemand", "CoverItLive", "CPanel", "CPMStar", "CPX Interactive",
                 "CrazyEgg", "Criteo", "Critsend", "Cross Pixel Media", "Crowd Ignite", "Crowd Science", "CrownPeak",
                 "CS Cart", "CubeCart", "Cura Software", "Curalate", "Customer.io", "Cvent", "cXense", "CyberSource",
                 "Dailymotion", "Daisycon", "Datalicious Supertag", "Datalogix", "DataPipe", "DataXu", "Datonics",
                 "DC Storm", "Dealer", "Dealer.com", "Delivra", "Demandbase", "Demandware", "DeviceAtlas", "DigiCert",
                 "Digilant", "Digital River", "DigitalOcean", "Disqus", "DNS Made Easy", "DNS Pod", "DNSimple",
                 "Docebo", "DocuSign", "dotCMS", "dotMailer", "DotNetNuke", "Dotomi", "DoubleClick Floodlight",
                 "DoubleVerify", "Drawbridge", "DreamHost", "Dropbox", "Drupal", "Dstillery", "DTDC", "DudaMobile",
                 "Dwolla", "E-junkie", "easyDNS", "eBridge", "Echo360", "eClinicalWorks", "Ecwid", "EdgeCast",
                 "EdgeDirector", "Edgewebhosting.net", "Effective Measure", "eGain", "Ektron", "Elastic Email",
                 "Elasticsearch", "Eloqua", "Email Center Pro", "Embedly", "Emediate", "Endeca",
                 "Endurance International", "Enecto", "Ensighten", "Entrust EV", "EPiServer", "Epsilon", "Equinix",
                 "Errorception", "Ethnio", "Etology", "etouches", "ETracker", "ETrigue", "Etsy", "Eventbrite",
                 "Everbridge", "Evergage", "Exact", "ExactHire", "ExactTarget", "ExoClick", "Experian", "Experiment.ly",
                 "Extole", "eXTReMe Tracking", "Facebook Badge", "Facebook Comments", "Facebook Conversion Tracking",
                 "Facebook Like", "Facebook Like Button", "Facebook Send Button", "Fasthosts", "Fastly CDN",
                 "FastSpring", "FederatedMedia", "Feedbackify", "Findly", "Firehost", "FIS Global BancLine", "Fishbowl",
                 "Flashtalking", "Flattr", "Flickr API", "Flickr Badge", "Flickr PhotoStream", "Flickr Slideshow",
                 "Flickr Video", "Flite", "Flowplayer", "Flowplayer Commercial", "Fontdeck", "Fonts.com", "ForgeRock",
                 "Formsite", "Formstack", "Freespee", "FreshBooks", "Freshdesk", "Friendbuy", "Fry", "Fujitsu",
                 "Gaug.es", "GenBook", "Genius", "Genoo", "GeoTrust EV", "GeoTrust Verfication", "GetResponse",
                 "GetSatisfaction", "Gigya", "Glam Media", "GlobalSign", "GoDaddy", "GoDaddy Hosting Ads", "GoDataFeed",
                 "GoECart", "GoGrid", "Google", "Google Adsense", "Google Adsense for Domains", "Google Analytics",
                 "Google App Engine", "Google Apps", "Google Checkout", "Google Cloud Storage", "Google Font API",
                 "Google Maps", "Google PageSpeed Service", "Google Remarketing", "Google Sites", "Google Tag Manager",
                 "Google Translate API", "Google Translate Widget", "Google Trusted Store", "Google Website Optimizer",
                 "Gorilla Nation", "GoSquared", "GotoMeeting", "Granify", "Gravity Forms", "GreaterGiving", "GrooveHQ",
                 "GumGum", "Gumroad", "Gunicorn", "Halogen Software", "HasOffers", "HealthcareSource", "Hello Bar",
                 "Help On Click", "Help Scout", "Helpjuice", "Herok", "Hetzner", "Hi Media Performance", "HiConversion",
                 "Highwinds CDN", "Histats", "Hitslink", "Homestead", "HookLogic", "Host Analytics", "HostDime",
                 "HostGator", "Hosting.com", "HostNine", "HostPaPa", "Hostway", "HP Internet Services", "HRsmart",
                 "HubSpot", "I-Behavior", "IBM HTTP Server", "IBM Lotus Domino", "IBM Lotus Notes", "IBM WebSphere",
                 "iCIMS", "iContact", "IgnitionOne", "iGoDigital", "iland", "Impact Radius", "Improve Digital",
                 "Improvely", "Incapsula", "Industry Brains", "Infinity Tracking", "Infoblox", "Infolinks",
                 "Infor PeopleAnswers", "Infusionsoft", "InSales", "Insight Express", "Insightera", "Inspectlet",
                 "Instapaper", "Instart Logic", "Instec", "Integral Ad Science", "IntelliAd", "IntenseDebate",
                 "Intent Media", "Intercom", "Internap", "Internap CDN", "Internap DNS", "Intershop", "InterviewStream",
                 "Invision Power Board", "InviteReferrals", "Invodo", "iPerceptions",
                 "ISA Web Publishing Load Balancer", "iseek Communications", "ISPrime", "Janrain", "Jimdo", "Jimdo DNS",
                 "JivoSite", "JobDiva", "Jobscience", "Jobscore", "Jobvite", "Join.Me", "Joomla", "JotForm", "Joyent",
                 "jPlayer", "JustUno", "JW Player", "Kalio", "Kaltura", "Kameleoon", "Kampyle", "Kapost", "Kayako",
                 "Keen IO", "Kenexa Brassring", "Kenshoo", "Kentico", "Kewego", "KeyMetric", "Keyweb", "Kiosked",
                 "KISSmetrics", "KIT Digital", "Klarna", "Klaviyo", "Komoona", "KonaKart", "Kontera", "Korrelate",
                 "Kronos", "Kwantek", "Latisys", "Layered Tech", "Lead Forensics", "LeadLander", "LeaseWeb", "Lengow",
                 "Level3", "Lever", "LexisNexis", "Lexity", "Liferay", "Ligatus", "Lighthouse 360", "Limelight CDN",
                 "Linkpulse", "LinkShare", "LinkSmart", "LinkTrust", "Linode", "Liquid Web", "Listrak",
                 "Lithium Technologies", "Litmos", "LiveChat", "Liveclicker", "LiveFyre", "LiveHelpNow", "LiveInternet",
                 "LiveRail", "Liveramp", "Livestream", "LiveZilla", "LocalEdge", "Localytics", "Log My Calls", "Loggly",
                 "Logicworks", "LoginRadius", "LogNormal", "LoopFuse", "Lotame", "Luceo", "Lucky Orange", "Lumesse",
                 "Luminate", "Lyris", "MadAdsMedia", "MadMobile", "Magento", "Magento Enterprise", "Magnetic",
                 "MailChimp", "MailChimp Mandrill", "MailChimp SPF", "MailJet", "Manticore", "MapBox", "MapQuest",
                 "Marchex", "Market 2 Lead", "MarketLive", "Marketo", "MarkMonitor", "Marqui", "MasterPass", "MaxCDN",
                 "MaxMind", "Maxymiser", "Medallia", "Media Temple", "Media.net", "mediaFORGE", "MediaHawk",
                 "MediaMath", "MediaSilo", "Meetrics", "Melbourne IT", "Meltwater", "Mercent", "MessageBus",
                 "MessageGears", "Meteor", "Mezzobit", "MGI", "MicroAd", "Microsoft", "Microsoft Frontpage",
                 "Microsoft SharePoint", "MicroStrategy", "Mimecast", "Mint", "Miva Merchant", "Mixpanel", "Moat",
                 "Mobify", "mod_pagespeed", "Mollie", "Monetate", "Moodle", "MoovWeb", "Mouseflow", "MovableType",
                 "Movideo", "Mulesoft", "MX Logic", "MyBuys", "myStaffingPro", "myThings", "Nanigans", "NationBuilder",
                 "Nativo", "Navegg", "Navisite", "Needles", "Net-Results", "NetIQ", "NetNames", "Neto", "NetSeer",
                 "NetSuite", "NetSuite eCommerce", "Network Solutions", "Network Solutions Site Seal", "NeuLion",
                 "Neustar", "New Relic", "Nexcess", "Nextopia", "Ngenix", "Nielsen//NetRatings", "Nominum",
                 "nopCommerce", "nrelate", "nRelate", "NTT America", "NuggAd", "Offerpop", "Olapic", "Olark", "ON24",
                 "OnApp", "OneAll", "Online Tech", "Onswipe", "Ontraport", "Ooyala", "Open Tracker", "OpenCart",
                 "OpenID", "OpenSSL", "OpenTable", "OpenX", "OpinionLab", "Optify", "Optimizely", "Optimost",
                 "Oracle Application Server", "Oracle OLAP", "OrangeSoda", "OsCommerce", "Outbrain", "OutSystems",
                 "OVH", "OwnerIQ", "PageUp People", "Panopto", "Parallels", "Parature", "Pardot", "Parse.ly", "Paychex",
                 "Paylocity", "Paymill", "PayPal", "PayPal Express Checkout", "PaySimple", "PCrecruiter", "Peak 10",
                 "Peerius", "Peoplefluent", "PeopleMatter", "Performable", "Personyze", "PHP", "Phusion Passenger",
                 "PicReel", "Pingdom", "Piwik", "Pixafy", "Pixeleze", "PlentyMarkets", "plista", "Pluck", "Pmetrics",
                 "PointRoll", "PollDaddy", "Post Affiliate Pro", "PostageApp", "Postini", "Postmark", "Postpresso",
                 "PowerReviews", "PR Newswire", "Predicta", "Predictive Response", "PrestaShop", "PriceGrabber",
                 "Prolexic", "Prolexic DNS", "Promas", "Proofpoint", "Prototype", "Provide Support", "PRTracker",
                 "PubMatic", "PulsePoint", "PunchTab", "Pusher", "Quadax", "Qualaroo", "Qualtrics",
                 "Qualtrics Site Intercept", "Quantcast", "QuBit OpenTag", "QuestBack", "QuickBooks Online",
                 "QuinStreet", "QuoteMedia", "Rackspace", "Rackspace CDN", "Rackspace DNS", "Rackspace Mailgun",
                 "RadiumOne", "ReachForce", "ReachLocal", "React", "Readypulse", "ReadyTalk", "Realtime Targeting",
                 "RealTracker", "reCAPTCHA", "Recurly", "Recurly JS", "Redit International", "Reevoo", "Referral Candy",
                 "Reflected Networks", "RegOnline", "Reinvigorate", "Rejoiner", "Relay42", "RES Software",
                 "ResellerRatings", "Respond", "ResponseTap", "Responsys", "ReTargeter", "Return Path", "Revenue Wire",
                 "RichRelevance", "RingCentral", "RobinHQ", "RocketFuel", "Rubicon Project", "Ruby on Rails", "Saba",
                 "Sage CRM", "Sailthru", "SaleCycle", "SaleMove", "Salesforce", "SalesFUSION", "Samanage", "Savvis",
                 "ScaleEngine", "Schedulicity", "Schoology", "Scout Analytics", "ScribbleLive",
                 "Search Discovery Satellite", "SearchDex", "SearchForce", "SecurePay", "Seesmic", "SeeWhy",
                 "Segment.io", "Selligent", "Sendgrid", "Sentry", "SEOshop", "ServerBeach", "ServiceNow", "ServInt",
                 "SessionCam", "ShareASale", "ShareThis", "Sharethrough", "ShinyStat", "Shop2Market", "Shopify",
                 "Shopp", "ShopSite", "ShopSocially", "ShopVisible", "Shopware", "Shopzilla", "Sidecar", "Sift Science",
                 "SilkRoad", "Silverpop", "SilverStripe", "Simple Helix", "Simple Share Buttons", "SimpleReach",
                 "Simpli.fi", "SingleHop", "Site Meter", "Site5", "Sitecore", "Siteheart", "SiteLock", "SiteScout",
                 "SiteSpect", "Skillsoft", "SkimLinks", "SLI Systems", "Slimbox", "Smart Adserver", "SmartRecruiters",
                 "SnapEngage", "SnapWidget", "Snoobi", "SOASTA", "Sociable Labs", "Social Annex", "Social Twist",
                 "Sociomantic", "Softlayer", "Solutionreach", "Solve Media", "SoundCloud", "Specific Media", "Spinnakr",
                 "Spongecell", "Sports Direct", "SpotXchange", "Spree", "Spring Metrics", "Sprinklr", "Sprint",
                 "Squarespace", "Stack", "Star Analytics", "Starfield SSL", "Starfield Technologies", "Stat 24",
                 "StatCounter", "SteelHouse", "Stibo Systems", "Strangeloop", "Strato", "StreamSend", "Strikingly",
                 "Stripe", "Struq", "Sublime Video", "SuccessFactors", "SugarCRM", "Super Stats", "SurveyGizmo",
                 "SurveyMonkey", "Sweet Tooth", "Swiftype", "Symphony Commerce", "Tableau Public", "Taboola", "Tacoda",
                 "Tag Commander", "TagMan", "Talend", "Taleo Business Edition", "Taleo Enterprise", "Taleo Performance",
                 "Taobao", "Tapad", "TapInfluence", "TargetBase", "TargetX", "Tealium", "Technorati", "Telepacific",
                 "Telerik Sitefinity", "TellApart", "Telligent", "Tencent QQ", "Terremark", "Thawte", "Thawte Seal",
                 "The Trade Desk", "thePlatform", "Thismoment", "Tictail", "TimeTrak", "TinderBox", "Tint", "TOPdesk",
                 "Totango", "TouchCommerce", "ToutApp", "TradeDoubler", "TradeTracker", "TransIP", "Tremor Video",
                 "TrialPay", "Tribal Fusion", "Triggit", "Trust Guard", "TRUSTe", "TrustedForm", "TrustPilot",
                 "Trustwave Seal", "TrustYou", "TubeMogul", "Tumblr", "turboSMTP", "Turn", "Twilio", "Twistage",
                 "Twitter Follow Button", "Twitter HashTag Button", "Twitter Mention Button", "Tyler Technologies",
                 "Typekit", "TYPO3", "Ubercart", "Ubuntu", "UltiPro", "UltraCart", "Umbel", "Umbraco", "Unbounce",
                 "Undertone", "Unica", "UnitedLayer", "UniteU", "Unruly Media", "UpSellit", "Upshot Commerce",
                 "Usabilla", "Usablenet", "UserEcho", "UserMood", "Userneeds", "Userpulse", "UserReport", "UserVoice",
                 "Ustream", "Vanilla Forums", "Varnish", "vBulletin", "Ve Interactive", "Vee24", "Velaro", "Venda",
                 "Venyu Solutions", "Verio", "Verisign EV", "Verisign Seal", "Verizon", "Vero", "Vertical Acuity",
                 "VerticalResponse", "Viafoura", "Viddler", "VideoBloom", "Videodesk", "VideoEgg", "VideoJS",
                 "Videology", "Vidyard", "VigLink", "Vimeo", "VirtueMart", "Visible", "Vision Critical", "VisiStat",
                 "VisitorTrack", "Visual Revenue", "Visual Visitor", "Visual Website Optimizer", "Visualsoft",
                 "Vivocha", "Vizury", "VMIX", "Volusion", "Volusion Live Chat", "VTEX Integrated Store", "W3 Counter",
                 "WalkMe", "Web Cube", "Web.com", "WebCollage", "WebEngage", "Webgains", "WebLinc", "Weborama",
                 "WebRezPro", "Website Tonight", "Website x5", "Websitealive", "WebsPlanet", "Webtrekk", "Webtrends",
                 "WebTrends Optimize", "Webydo", "Webzilla", "Weebly", "WePay", "WhatCounts", "Whipple Hill",
                 "whos.amung.us", "WhosOn", "Wiredrive", "Wistia", "Wix", "Woobox", "WooCommerce", "Woopra",
                 "WordPress", "WordPress VIP", "Wordstream", "WP eCommerce", "WP Engine", "Wufoo", "X-Cart", "XenForo",
                 "XiTi", "XO Communications", "Yahoo Site Builder", "Yahoo Store", "Yahoo Tag Manager",
                 "Yahoo Web Analytics", "Yandex Metrika", "YCharts", "Yesmail", "Yext", "YieldBot", "Yieldify",
                 "Yieldlab", "Yodle", "Yola", "Yotpo", "Yottaa", "YouTube", "YuMe", "Zanox", "Zedo", "Zemanta",
                 "Zen Cart", "Zendesk", "ZergNet", "Zerolag", "Zeus", "ZipRecruiter", "Zoho CRM", "Zoho Recruit",
                 "Zombaio", "ZoneEdit", "Zoovy", "ZServer", "Zuora"]

def get_states(is_all=False):
    if is_all:
        return all_states
    return random.sample(all_states, random.randint(1, len(all_states)))

def get_EmployeeSize_buckets(is_all=False):
    if is_all:
        return all_employee_size
    return random.sample(all_employee_size, random.randint(1, len(all_employee_size)))

def get_industries(is_all=False):
    if is_all:
        return all_industries
    return random.sample(all_industries, random.randint(1, len(all_industries)))

def get_tech(is_all=False):
    if is_all:
        return all_tech
    return random.sample(all_tech, random.randint(0, len(all_tech)))

def get_departments(is_all=False):
    if is_all:
        return all_department
    return random.sample(all_department, random.randint(0, len(all_department)))



def save_qualification(seg_id):
    qualification_url = base_url + '/segments/%d/qualification' % seg_id
    payload = {"qualification": {
        "state": ["Alaska", "Texas", "California", "Montana", "New Mexico", "Arizona", "Nevada", "Colorado", "Oregon",
                  "Wyoming", "Michigan", "Minnesota", "Utah", "Idaho", "Kansas", "Nebraska", "South Dakota",
                  "Washington", "North Dakota", "Oklahoma", "Missouri", "Florida", "Wisconsin", "Georgia", "Illinois",
                  "Iowa", "New York", "North Carolina", "Arkansas", "Alabama", "Louisiana", "Mississippi",
                  "Pennsylvania", "Ohio", "Virginia", "Tennessee", "Kentucky", "Indiana", "South Carolina",
                  "West Virginia", "Maryland", "Hawaii", "Massachusetts", "Vermont", "New Hampshire", "New Jersey",
                  "Connecticut", "Delaware", "Rhode Island", "District of Columbia", "Puerto Rico",
                  "Northern Mariana Islands", "US Virgin Islands", "American Samoa", "Guam"],
        "industry": ["Agriculture", "Business Service", "Chamber", "Construction", "Consumer Services", "Cultural",
                     "Culture", "Education", "Energy", "Finance", "Government", "Healthcare", "Hospitality",
                     "Insurance", "Legal", "Manufacturing", "Media", "Metals & Minerals", "Municipal", "Organizations",
                     "Realestate", "Retail", "Software", "Telecom", "Transportation"],
        "employeeSize": ["1-10", "11-50", "51-200", "201-500", "501-1000", "1001-5000", "5001-10000", "10000+"]}}
    r = s.put(qualification_url, json=payload, verify=to_verify, timeout=600)
    print r.url, r.status_code, r.elapsed
    return r.status_code


def upload_seed(seg_id):
    csv_seed_url = base_url + '/segments/%d/seeds/csv' % seg_id
    r = s.post(csv_seed_url, files={'file': open('HighFitPaymentSolutions.csv', 'r')}, verify=to_verify, timeout=600)
    print r.url, r.status_code, r.elapsed
    return r.status_code


def save_expansion(seg_id):
    # save expansion
    expansion_url = base_url + '/segments/%d/expansion' % seg_id
    payload = {"expansion": {
        "department": ["Administrative", "IT", "Education", "Engineering", "Finance", "HR", "Legal", "Marketing",
                       "Health", "Operations", "Other", "Ownership & Board", "R&D", "Sales"],
        "tech": ["AB Tasty", "adgoal", "Adobe CQ", "Amazon SES", "Animoto", "ArcGIS", "Athenahealth", "Aviary",
                 "BloomReach", "Bunchball", "ChannelAdvisor", "Chitika", "Criteo", "Ethnio", "Facebook Badge",
                 "Facebook Like Button", "Flowplayer Commercial", "Freespee", "FreshBooks", "Freshdesk", "Google",
                 "Hitslink", "Host Analytics", "Internap CDN", "Kenexa Brassring", "Klaviyo", "Korrelate", "LiveChat",
                 "LiveInternet", "LocalEdge", "Lumesse", "Network Solutions", "NeuLion", "OpenCart", "Oracle OLAP",
                 "Pingdom", "PrestaShop", "Prolexic DNS", "RealTracker", "Samanage", "SearchForce", "Site5",
                 "SiteSpect", "SOASTA", "Solve Media", "SoundCloud", "Strato", "TargetX", "TrustPilot", "Umbraco",
                 "Venyu Solutions", "Vivocha", "WP Engine", "Yahoo Web Analytics"]}}
    r = s.put(expansion_url, json=payload, verify=to_verify, timeout=600)
    print r.url, r.status_code, r.elapsed
    return r.status_code


def start_model_run(seg_id):
    model_run_url = base_url + '/segments/%d/model_run' % seg_id
    r = s.post(model_run_url, verify=to_verify, timeout=600)
    print r.url, r.status_code, r.elapsed
    return r.status_code


if __name__ == '__main__':
    import time

    host_url = 'https://eap.everstring.com'
    accountId = 35
    user_email = 'perftest@everstring.com'
    user_password = '1'
    to_verify = True

    # host_url = 'https://eap.staging.everstring.com'
    # accountId = 4
    # user_email = '5342476@qq.com'
    # user_password = '1'
    # to_verify = True

    # host_url = 'https://eap.uat.everstring.com'
    # accountId = 176
    # user_email = 'perftest@everstring.com'
    # user_password = '1'
    # to_verify = True

    # host_url = 'https://ns.everstring.com'
    # accountId = 1
    # user_email = 'pu@everstring.com'
    # user_password = '123456'
    # to_verify = False

    base_url = host_url + '/app'

    if login() != requests.codes.ok:
        print 'login failed.'
        s.close()
        exit -1
    for i in range(159, 160):
        segment_id = create_segment('auto_test_same_seed_%d' % i)
        if segment_id > 0:
            print '>>>>>', i, segment_id
            save_qualification(segment_id)
            upload_seed(segment_id)
            save_expansion(segment_id)
            start_model_run(segment_id)
            print 'started model run\n\n'
            # time.sleep(10)
            # print '\n\n'
    s.close()