# -*- coding: utf-8 -*-
'''
--host=http://service.uat.everstring.com
'''

from locust import HttpLocust, TaskSet, task


class UserBehavior(TaskSet):
    domains = []

    companyIds = []
    accountId = 77
    limitPerCompany = 5

    where = 'where={"$or": [{"employeeSize": {"$lte": 10}}, {"employeeSize": {"$lte": 50, "$gt": 10}}, {"employeeSize": {"$lte": 200, "$gt": 50}}, {"employeeSize": {"$lte": 500, "$gt": 200}}, {"employeeSize": {"$lte": 1000, "$gt": 500}}, {"employeeSize": {"$lte": 5000, "$gt": 1000}}, {"employeeSize": {"$lte": 10000, "$gt": 5000}}, {"employeeSize": {"$lte": 100000000, "$gt": 10000}}], "state": {"$in": ["Texas", "California", "Montana", "New Mexico", "Arizona", "Nevada", "Colorado", "Oregon", "Michigan", "Minnesota", "Utah", "Idaho", "Nebraska", "Washington", "Oklahoma", "Missouri", "Florida", "Wisconsin", "Georgia", "Illinois", "Iowa", "New York", "North Carolina", "Arkansas", "Alabama", "Louisiana", "Pennsylvania", "Ohio", "Virginia", "Tennessee", "Indiana", "South Carolina", "West Virginia", "Maryland", "Massachusetts", "New Hampshire", "New Jersey", "Connecticut", "District of Columbia"]}, "industry": {"$in": ["Business Service", "Consumer Services", "Education", "Energy", "Finance", "Government", "Healthcare", "Hospitality", "Insurance", "Manufacturing", "Media", "Municipal", "Organizations", "Realestate", "Retail", "Software", "Telecom", "Transportation"]}}'

    def on_start(self):
        self.getDomains()
        self.getCompanyIds()

    def getDomains(self):
        with open('test_seed_1K.csv', 'r') as f:
            self.domains.append(f.readline())

    def getCompanyIds(self):
        with open('test_companyIds_1K.csv', 'r') as f:
            self.companyIds.append(f.readline())

    def anaylytics(self):
        self.client.post('/company_enrichment/analytics', {
            'seedDomainList': self.domains
        })

    @task(5)
    def enrichment(self):
        self.client.post('/company_enrichment/enrichment', {
            'seedDomainList': self.domains
        })

    @task(10)
    def ns_contacts(self):
        self.client.post('/company_enrichment/ns_contacts', {
            'companyIdList': self.companyIds,
            'limitPerCompany': self.limitPerCompany,
            'accountId': self.accountId
        })

    def filter(self):
        self.client.get('/company_enrichment/filter', {
            'where': self.where
        })

    def metadata(self):
        self.client.get('/company_enrichment/metadata/get_company_id', {
            'domains': self.domains
        })

    @task(3)
    def metadata_tech(self):
        self.client.get('/company_enrichment/metadata/tech')

    @task(3)
    def metadata_industry(self):
        self.client.get('/company_enrichment/metadata/industry')

    @task(3)
    def metadata_location(self):
        self.client.get('/company_enrichment/metadata/location')


class AppUser(HttpLocust):
    task_set = UserBehavior

    min_wait = 5 * 1e3
    max_wait = 20 * 1e3
