# -*- coding: utf-8 -*-

from locust import HttpLocust, TaskSet, task
from faker import Factory
import random



class SignUp(TaskSet):
    faker = Factory.create()

    def index(self):
        self.client.get("/")

    @task
    def signup(self):
        user = self.faker.firstName()+'@everstring.test.com'
        password = '1'
        self.client.post('/app/users/register', {
            'email': user,
            'password': '1'
        })
        with open('users.txt', 'a') as f:
            f.write(user + '\n')

    def create_segment(self):
        segment_name = 'test-99'
        self.client.post('/app/segments', {'segmentName': segment_name})

    

class SignIn(TaskSet):
    def on_start(self):
        user = random.choice(list(open('users.txt')))
        print user
        password = '1'
        self.client.post('/app/users/login', {
            'email': user,
            'password': password
        })

    @task
    def logout(self):
        self.client.delete('/app/users/logout')

class TrialUser(HttpLocust):
    task_set = SignIn

    min_wait = 5 * 1e3
    max_wait = 9 * 1e3