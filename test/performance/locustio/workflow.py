# -*- coding: utf-8 -*-
"""
Main performance test definition.

How to use:

    1. Install locustio:

        $ pip install locustio

    2. Execute the command line:

        $ locust -f workflow.py --host=https://northstar.uat.everstring.com

    3. Visit the web monitor:

        http://localhost:8089/
"""

from locust import HttpLocust, TaskSet, task

# for production environment
# userId = '2'
# segmentId = '4'

# for uat environment
userId = '2'
segmentId = '84'

# for staging environment
# userId = '1'
# segmentId = '2'



class UserBrowseBehavior(TaskSet):
    """
    This class mimic the following user behavior:

        1. user logins
        2. visits the segment console
        3. click a segment to see the overview
    """

    def on_start(self):
        self.isLogin = False
        self.login()
        self.isLogin = True

    def login(self):
        """
        Login with the super admin account
        """
        self.client.post('/app/users/login', {
            'email': '5342476@qq.com',
            'password': '1'
        })

    @task(50)
    def segment_list(self):
        """
        Visit the segment console 50% of the time.
        """
        if not self.isLogin:
            return
        self.client.get('/app/segments')
        self.client.get('/app/users/me')

    @task(30)
    def segment_overview_graph(self):
        """
        Visit the segment overview 10% of the time.
        """
        if not self.isLogin:
            return
        self.client.get('/app/segments/' + segmentId + '')
        self.client.get('/app/recommendations/dimension')
        self.client.get('/app/segments/' + segmentId + '/companies/overview')
        # self.client.get('/app/segments/' + segmentId +
                        # '/companies/distinct_counts?keys=revenue,employeeSize,industry,state')
        # self.client.get('/app/segments/' + segmentId +
        #             '/companies/distinct_counts?keys=tech')

    @task(30)
    def segment_overview_table(self):
        if not self.isLogin:
            return
        self.client.get('/app/users/me')
        # self.client.get('/app/segments/' + segmentId +
        #                 '/companies/distinct_counts?keys=revenue,employeeSize,industry,country,state,city')
        self.client.get('/app/segments/' + segmentId + '/companies?limit=10&offset=0&onlyExported=0')
        # self.client.get('/app/segments/' + segmentId + '/companies?limit=10&offset=100&onlyExported=0')
        # self.client.get('/app/segments/' + segmentId + '/companies?limit=10&offset=0&onlyExported=1')
        # self.client.get('/app/segments/' + segmentId +
        #                 '/companies?limit=10&offset=0&onlyExported=0&where=%7B%22revenue%22:%7B%22$in%22:%5B%22$0M-$1M%22,%22$100M-$250M%22,%22$10M-$25M%22,%22$1B-$5B%22%5D%7D%7D')


    def export(self):
        if not self.isLogin:
            return
        self.client.get('/app/users/' + userId + '/sfdc/managed_package_status')

    # @task(30)
    def advanced_export(self):
        if not self.isLogin:
            return
        self.client.get('/app/segments/' + segmentId + '/sector_company_cnt')

    @task(10)
    def seed_list_insight(self):
        if not self.isLogin:
            return
        self.client.get('/app/segments/' + segmentId + '')
        self.client.get('/app/seeds/dimension')
        self.client.get('/app/segments/' + segmentId + '/seed_candidates?id=4&limit=10&offset=0')
        self.client.get('/app/segments/' + segmentId +
                        '/seeds/distinct_counts?keys=revenue,employeeSize,industry,state')
        self.client.get('/app/segments/' + segmentId +
                    '/seeds/distinct_counts?keys=tech')



class WebsiteUser(HttpLocust):
    task_set = UserBrowseBehavior

    min_wait = 120 * 1e3
    max_wait = 120 * 1e3
