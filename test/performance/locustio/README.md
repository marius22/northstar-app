# How to use:

- Install locustio:

```
$ pip install locustio
```

- Execute the command line:

```
$ locust -f workflow.py --host=https://northstar.uat.everstring.com
```


- Visit the web monitor:

```
http://localhost:8089/
```