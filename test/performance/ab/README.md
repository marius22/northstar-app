# How to use?


- install siege

```
$ brew install siege
```

- create a new configuration file or use existing one

```
$ siege.config
$ ls ~/.siege/
```

- if new configuration file, set the login-url in siege.conf

```
login-url = https://northstar.staging.everstring.com/app/users/login POST email=5342476@qq.com&password=1
```

- change the `host` and `segmentId` in the urls.txt
```
HOST=https://northstar.staging.everstring.com
segmentId=84
```

- start the performance test in command line

```
siege  -f urls.txt --concurrent=25 --reps 5
siege  -f urls.txt --concurrent=50 --reps 5
```

- OR just start the script

```
source ./load_test.sh
```


# Note: need to change for different environment

