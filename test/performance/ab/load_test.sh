#!/usr/bin/env bash

rm ./log.txt
echo
echo "========================"
echo START RUN 25 CONCURRENT USERS
echo "========================"
echo
siege -q --file=./urls.txt --rc=./siege.conf --log=./log.txt --concurrent=25 --reps 5
echo
echo


#echo
#echo
#echo "========================"
#echo START RUN 50 CONCURRENT USERS
#echo "========================"
#siege -q --file=./urls.txt --rc=./siege.conf --log=./log.txt --concurrent=50 --reps 5
#echo
#echo

#echo
#echo
#echo "========================"
#echo START RUN 100 CONCURRENT USERS
#echo "========================"
#siege -q --file=./urls.txt --rc=./siege.conf --log=./log.txt --concurrent=100 --reps 5
#echo
#echo


#echo
#echo
#echo "========================"
#echo START RUN 500 CONCURRENT USERS
#echo "========================"
#siege -q --file=./urls.txt --rc=./siege.conf --log=./log.txt --concurrent=500 --reps 5
#echo
#echo

cat ./log.txt

