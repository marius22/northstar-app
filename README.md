# Northstar Web Services
This repo holds the code for [EverString Audience Platform (EAP)](eap).

## How to run Northstar locally
Install [Docker](https://www.docker.com/). Find out your docker-machine's IP address with `docker-machine ip`
Add an entry to your `/etc/hosts` like this:

```
$DOCKER_HOST dockerhost.com
```

where `$DOCKER_HOST` is your docker-machine's IP address.

Known issue:
After running `docker-compose up` for the first time, the `db` container will fail with this error:
```
db_1      | ERROR 1396 (HY000) at line 1: Operation CREATE USER failed for 'root'@'%'
```

In order to get authentication working, there are a few more steps. First, in your `dev.config.py` use this value:
```py
SESSION_COOKIE_DOMAIN = 'dockerhost.com'
```

Similarly, in your `config.dev.js`, set your `apiBaseURL` to `'http://dockerhost.com:9000/app/'`.


If this happens, just kill your `docker-compose` instance with `docker-compose stop` or just `Ctrl-C` your
`docker-compose` process.
## Folder structure

This is the folder structure of our web services code

    - ns
     |- controller
        |- app
        |- service
     |- model
     |- schema
     |- util
     |- vendor
    - instance
     |- config.py.example
    - script
    - test

We'll go over each folder one by one:

    controller/

API implementation here. Parameters validation should be done in this layer. Each API could include one or more model methods.

    controller/app
Application side implementation. API in this folder can ONLY be called by client JavaScript.

    controller/service
Backend side implementation. API in this folder can ONLY be called by application Python code, or ETL pipeline, NOT accessible to client JavaScript.

    model/

DB models and entity class here. module files in this folder should cover CRUD functions for a certain table.
Complex db queries and simple business logic here.

    vendor/

Encapsulate 3rd party API or SDK here

    util/

utilities are coded here, convenient methods such as date time format validation are here

    test/

unit test here

    instance/

Webservice configurations here, which are loaded during application startup. They are env depended
copy 'config.py.example' to 'config.py' and make corresponding changes based on enviroment.

    script/

Some scripts outside of flask web application, but may depend some modules in ns package.

## Setup

Install `pre-push` git hook which runs unit tests before pushing to remote, prevents the failure commits from pushing into remote.

    # In project's root directory
    api/script/setup_git_hooks.py

## Running tests
To run our server-side tests, go to the `api` folder and run `python test-runner.py`.

To run an individual test file, run

`PYTHONPATH=. python test/test_public_api.py`

Setting the `PYTHONPATH` variable is necessary for imports of other modules in this repo to work.

where you replace `test_segment` with the name of the python file whose test you're running and `TestSegment` with the name of the test case class.

# Northstar Client
Most of our code is Angular 1.6, but we're moving to React. We have things set up so that we can use React libraries and React components as well as use our own components via [ngReact](https://github.com/ngReact/ngReact).
## Folder structure
Welcome to the Northstar client code! This part of the codebase handles taking front-end assets (html, js, css, images, etc.) and packages them for service to the user.

This is the folder structure inside our `client/` folder:

    - gulpfile.js
    - karma.conf.js
    - node_modules
    - package.json
    - public
    - src
    - webpack.config.js

We'll go over each folder one by one:

    gulpfile.js/

Our front-end code uses a build system called [gulp](gulp) This build system is responsible for taking front-end assets and pre-processing them (ES6 javascript to ES5, SCSS to CSS) for delivery to the browser.

All our gulp configuration lives in this folder.

    karma.conf.js

We use the javascript test runner [Karma](karma) to run our javascript tests. This file configures Karma to find our tests, run them, report on test output, generate coverage reports, etc.

    node_modules/
    package.json
We use [Node](node) to run `gulp`, our [Karma](karma) tests, javascript linting, etc. Every node project has a `package.json` file, which documents basic metadata about the project. More importantly, it serves as the source of truth of the dependencies of the project, similar to a pip `requirements.txt` file in Python.

When `npm install` is run, it installs the dependencies specified by `package.json`into `node_modules`. These dependencies are local to this project, so we don't need to worry about globally installed node modules or modules installed by other projects on your machine to affect this project.

    public/

This folder holds assets (html, js, css, images, etc) that can be served to the browser. So `src` holds the assets in a form that developers interact with (ES6, SCSS, etc.) and `public/` holds the fully processed, built assets that are concatenated and minified and ready to be handed off to the browser.

    src/

This folder holds all front-end source files before processing.

    webpack.config.js

We use [Webpack](webpack) to package our assets to serve to the browser. This file specifies how and where to package these assets.
## Setup
Ensure that you have the versions of [node](node) and `npm` specified in this project's `package.json`. Then, run
`npm install` to install the front-end dependencies. Enter src/javascripts/modules/config directory and copy config.dev.example.js to config.dev.js, modify the api to fit your env.


## Run code
Use the versions of `node`/`npm` specified in our project's `package.json`. For convenience, you may want to install [Node Version Manager (nvm)](https://github.com/creationix/nvm). Then all you need to do is go into `client/`' and run `nvm use`.

First, create `/client/src/javascripts/modules/config/config.local.js` using `config.local.example.js` as a template.

In development, run `npm run gulp` to run the app. The build takes a few seconds, and as it progresses, there's log output. Once it's done, `localhost:3000` will be opened in your default browser.

To test the production build, run `npm run build:prod`. This will create production assets but not serve them. To serve them, run `npm run gulp server`.
## Nginx configuration
See client/src/server/nginx.md for detail

## Tests
Run `npm run test` to run our unit tests with [Karma](karma)

## Code Style
We use [ESLint](es-lint) to lint our code. The rules are specified via multiple `.eslintrc.js` files. We'll maintain the invariant that all ESLint rules must pass. If you wish to change a rule, please discuss with @jasoncyu. We want to avoid someone changing a rule, updating the rest of the codebase to follow that rule, and then being forced to change it back.

Our docstrings are styled according to the syntax described by the [Closure Compiler](closure-compiler)

# Public API
Run with

```python
ENV=local python public_api.py
```

or run with docker-compose and

[node]: https://nodejs.org/ "NodeJS - server-side javascript"
[karma]: https://karma-runner.github.io "KarmaJS - test runner"
[gulp]: http://gulpjs.com/ "GulpJS - task runner"
[webpack]: https://webpack.github.io/ "Webpack - module bundler"
[es-lint]: http://eslint.org/ "ESLint - javascript linter"
[closure-compiler]: https://developers.google.com/closure/compiler/docs/js-for-compiler#tags "Closure Compiler"
[eap]: https://eap.everstring.com "EAP"
