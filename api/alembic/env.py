"""
Environment for alembic
"""

from __future__ import with_statement
import sys

import os

sys.path.append(os.getcwd())
from ns.model.account import Account
from ns.model.company_new import CompanyNew
from ns.model.crm_token import CRMToken
from ns.model.job import Job
from ns.model.job_status_history import JobStatusHistory
from ns.model.qualification import Qualification
from ns.model.role import Role
from ns.model.seed import Seed
from ns.model.seed_candidate import SeedCandidate
from ns.model.seed_filter import SeedFilter
from ns.model.segment import Segment
from ns.model.source import Source
from ns.model.user import User
from ns.model.user_role import UserRole
from ns.model.user_segment import UserSegment
from ns.model.code import Code
from ns.model.account_exported_company import AccountExportedCompany
from ns.model.account_exported_contact import AccountExportedContact
from ns.model.company_insight import CompanyInsight
from ns.model.account_quota import AccountQuota
from ns.model.domain_dictionary import DomainDictionary
from ns.model.sf_pull_request import SFPullRequest
from ns.model.user_invitation import UserInvitation
from ns.model.tmp_account_segment import TmpAccountSegment
from ns.model.user_download import UserDownload
from ns.model.company_recommendation_count import CompanyRecommendationCount
from ns.model.tmp_segment_stage import TmpSegmentStage
from ns.model.company_dispute import CompanyDispute
from ns.model.segment_metric import SegmentMetric
from ns.model.api_token import ApiToken
from ns.model.sfdc_config import SFDCConfig
from ns.model.model import Model
from ns.model.model_seed import ModelSeed
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.model.mas_token import MASToken
from ns.model.mas_field_mapping import MASFieldMapping
from ns.model.audience import Audience
from ns.model.audience_criteria import AudienceCriteria
from ns.model.audience_keywords import AudienceKeywords
from ns.model.audience_exclusion_list import AudienceExclusionList
from ns.model.user_preference import UserPreference


from logging.config import fileConfig

from alembic import context
from sqlalchemy import engine_from_config, pool, MetaData

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)


def combine_metadata(*args):
    m = MetaData()
    for metadata in args:
        for t in metadata.tables.values():
            t.tometadata(m)
    return m

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = combine_metadata(Account.metadata,
                                   CompanyNew.metadata,
                                   CRMToken.metadata,
                                   Job.metadata,
                                   JobStatusHistory.metadata,
                                   Qualification.metadata,
                                   Role.metadata,
                                   Seed.metadata,
                                   SeedCandidate.metadata,
                                   SeedFilter.metadata,
                                   Segment.metadata,
                                   Source.metadata,
                                   User.metadata,
                                   UserRole.metadata,
                                   UserSegment.metadata,
                                   Code.metadata,
                                   AccountExportedCompany.metadata,
                                   CompanyInsight.metadata,
                                   AccountQuota.metadata,
                                   DomainDictionary.metadata,
                                   SFPullRequest.metadata,
                                   UserInvitation.metadata,
                                   TmpAccountSegment.metadata,
                                   UserDownload.metadata,
                                   TmpSegmentStage.metadata,
                                   CompanyRecommendationCount.metadata,
                                   CompanyDispute.metadata,
                                   SegmentMetric.metadata,
                                   ApiToken.metadata,
                                   SFDCConfig.metadata,
                                   Model.metadata,
                                   ModelSeed.metadata,
                                   IndicatorWhiteList.metadata,
                                   AccountExportedContact.metadata,
                                   MASToken.metadata,
                                   MASFieldMapping.metadata,
                                   Audience.metadata,
                                   AudienceCriteria.metadata,
                                   AudienceKeywords.metadata,
                                   UserPreference.metadata,
                                   AudienceExclusionList.metadata)

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.

# x_arguments = context.get_x_argument(as_dictionary=True)
# print config.get_section(config.config_ini_section)


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url, target_metadata=target_metadata, literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix='sqlalchemy.',
        poolclass=pool.NullPool)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
