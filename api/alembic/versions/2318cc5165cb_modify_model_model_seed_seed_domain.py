"""modify model model_seed, seed_domain

Revision ID: 2318cc5165cb
Revises: 4fd29c175f58
Create Date: 2016-08-04 10:43:45.855773

"""

# revision identifiers, used by Alembic.
revision = '2318cc5165cb'
down_revision = '4fd29c175f58'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():

    op.add_column('model', sa.Column('account_id', sa.Integer(), nullable=False))
    op.add_column('model', sa.Column('model_seed_id', sa.Integer(), nullable=True))
    op.add_column('model', sa.Column('status', sa.String(length=50), nullable=False))
    op.add_column('model', sa.Column('batch_id', sa.String(length=50), nullable=False))

    op.add_column('model_seed', sa.Column('account_id', sa.Integer(), nullable=False))

    op.create_index('idx_account_id', 'model', ['account_id'], unique=False)


def downgrade():
    op.drop_column('model_seed', 'account_id')

    op.drop_column('model', 'batch_id')
    op.drop_column('model', 'status')
    op.drop_column('model', 'model_seed_id')
    op.drop_column('model', 'account_id')

    op.drop_index('idx_account_id', table_name='model')
