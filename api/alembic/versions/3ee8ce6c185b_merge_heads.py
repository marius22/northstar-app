"""merge heads

Revision ID: 3ee8ce6c185b
Revises: 1da83e77106a, baa9f8b9234
Create Date: 2017-02-28 17:30:19.211244

"""

# revision identifiers, used by Alembic.
revision = '3ee8ce6c185b'
down_revision = ('1da83e77106a', 'baa9f8b9234')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
