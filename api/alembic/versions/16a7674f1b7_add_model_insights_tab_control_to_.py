"""add model insights tab control to account quota

Revision ID: 16a7674f1b7
Revises: 211aacb59327
Create Date: 2016-10-12 13:47:48.014256

"""

# revision identifiers, used by Alembic.
revision = '16a7674f1b7'
down_revision = '211aacb59327'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('account_quota', sa.Column('enable_model_insights', sa.Boolean(), server_default="0", nullable=False))


def downgrade():
    op.drop_column('account_quota', 'enable_model_insights')
