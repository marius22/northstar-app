"""Add a new api token table. This is a revision of the existing table, eventhough the table can be created brand new

Revision ID: 5652cb4982c7
Revises: 40bfd9019982
Create Date: 2017-02-21 13:01:17.310665

"""

# revision identifiers, used by Alembic.
revision = '5652cb4982c7'
down_revision = '40bfd9019982'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_table('api_token')
    op.create_table('api_token',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('account_id', sa.BigInteger(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('token', sa.String(length=128), server_default='', nullable=False),
        sa.Column('customer_identifier', sa.String(length=64), server_default='', nullable=False),
        sa.ForeignKeyConstraint(['account_id'], ['account.id'], ),
	      sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('api_token')
    op.create_table('api_token',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('token', sa.String(length=255), server_default='', nullable=False),
        sa.Column('app_version', sa.String(length=100), server_default='', nullable=False),
        sa.Column('ip', sa.String(length=50), server_default='', nullable=False),
        sa.Column('locked', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('token', 'app_version')
    )
