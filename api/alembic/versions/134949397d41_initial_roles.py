"""initial roles

Revision ID: 134949397d41
Revises: 2d22e6ec5c02
Create Date: 2016-03-21 21:19:28.276156

"""

# revision identifiers, used by Alembic.
revision = '134949397d41'
down_revision = '38a3544c56b2'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("insert into role (id, created_ts, updated_ts, name) values(null, now(), now(), 'ES_SUPER_ADMIN')")
    op.execute("insert into role (id, created_ts, updated_ts, name) values(null, now(), now(), 'ES_ADMIN')")
    op.execute("insert into role (id, created_ts, updated_ts, name) values(null, now(), now(), 'ACCOUNT_ADMIN')")
    op.execute("insert into role (id, created_ts, updated_ts, name) values(null, now(), now(), 'ACCOUNT_USER')")
    op.execute("insert into role (id, created_ts, updated_ts, name) values(null, now(), now(), 'TRIAL_USER')")


def downgrade():
    op.execute("delete from role")
