"""merge

Revision ID: d9ec29a4518
Revises: 2d04679cb05e, 220d04b0966e
Create Date: 2016-07-13 13:25:55.961660

"""

# revision identifiers, used by Alembic.
revision = 'd9ec29a4518'
down_revision = ('2d04679cb05e', '220d04b0966e')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
