"""merge

Revision ID: 42658f91f242
Revises: 2468663ffdff, 129d4b06c7f5, 395b4f6bfc95
Create Date: 2016-04-07 22:11:01.624912

"""

# revision identifiers, used by Alembic.
revision = '42658f91f242'
down_revision = ('129d4b06c7f5', '2468663ffdff', '395b4f6bfc95')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
