"""sfdc config add enrich controll

Revision ID: 268cffa81e4
Revises: 2318cc5165cb
Create Date: 2016-08-11 11:01:31.616254

"""

# revision identifiers, used by Alembic.
revision = '268cffa81e4'
down_revision = '4fd29c175f58'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.add_column('sfdc_config', sa.Column('enrich_enabled', sa.Boolean(),
                                           server_default=sa.schema.DefaultClause("0"), nullable=False))
    op.add_column('sfdc_config', sa.Column('enrich_enabled_updated_status_ts', sa.TIMESTAMP(), nullable=True))
    op.add_column('sfdc_config', sa.Column('enrich_start_ts', sa.TIMESTAMP(), nullable=True))
    op.add_column('sfdc_config', sa.Column('enrich_score', sa.Boolean(),
                                           server_default=sa.schema.DefaultClause("0"), nullable=False))
    op.add_column('sfdc_config', sa.Column('enrich_firmographics', sa.Boolean(),
                                           server_default=sa.schema.DefaultClause("0"), nullable=False))


def downgrade():
    op.drop_column('sfdc_config', 'enrich_enabled')
    op.drop_column('sfdc_config', 'enrich_enabled_updated_status_ts')
    op.drop_column('sfdc_config', 'enrich_start_ts')
    op.drop_column('sfdc_config', 'enrich_score')
    op.drop_column('sfdc_config', 'enrich_firmographics')
