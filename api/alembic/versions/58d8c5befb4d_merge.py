"""merge

Revision ID: 58d8c5befb4d
Revises: 4b77e4462a46, 59e045782877
Create Date: 2016-04-21 22:03:52.770209

"""

# revision identifiers, used by Alembic.
revision = '58d8c5befb4d'
down_revision = ('4b77e4462a46', '59e045782877')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
