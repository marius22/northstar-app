"""add invalid domain table

Revision ID: 1c4966ac771e
Revises: 557eecc27dbe
Create Date: 2016-03-22 18:25:35.736246

"""

# revision identifiers, used by Alembic.
revision = '1c4966ac771e'
down_revision = '557eecc27dbe'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('domain_dictionary',
                    sa.Column('id', sa.BigInteger(), nullable=False),
                    sa.Column('domain', sa.String(length=255), nullable=False),
                    sa.Column('domain_type', sa.SmallInteger(), nullable=False),
                    sa.PrimaryKeyConstraint('id')
                    )


def downgrade():
    op.drop_table('domain_dictionary')

