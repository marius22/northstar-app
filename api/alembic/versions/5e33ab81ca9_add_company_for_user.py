"""add company for user

Revision ID: 5e33ab81ca9
Revises: 305f3648aac5
Create Date: 2016-09-10 11:11:24.642789

"""

# revision identifiers, used by Alembic.
revision = '5e33ab81ca9'
down_revision = '305f3648aac5'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.add_column('user', sa.Column('company', sa.String(length=255), nullable=True))


def downgrade():
    op.drop_column('user', 'company')
