"""add mas field mapping table

Revision ID: 30249eb4f48b
Revises: 3edd215249f1
Create Date: 2016-08-05 16:46:58.363488

"""

# revision identifiers, used by Alembic.
revision = '30249eb4f48b'
down_revision = '3edd215249f1'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('mas_field_mapping',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('account_id', sa.Integer(), nullable=False),
                    sa.Column('mas_type', sa.String(length=255), nullable=False),
                    sa.Column('mapping', sa.String(length=2048), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    )


def downgrade():
    op.drop_table('mas_field_mapping')
