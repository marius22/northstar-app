"""some nullable bug

Revision ID: 1013f559fe47
Revises: 7c31390b38a
Create Date: 2016-03-10 16:47:23.756133

"""

# revision identifiers, used by Alembic.
revision = '1013f559fe47'
down_revision = '7c31390b38a'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('job', 'preference_ids',
               existing_type=mysql.VARCHAR(length=4096),
               nullable=True)
    op.alter_column('job', 'qualification_ids',
               existing_type=mysql.VARCHAR(length=4096),
               nullable=True)
    op.drop_column('segment', 'status')
    op.add_column('segment', sa.Column('status', sa.Enum('EMPTY', 'DRAFT', 'IN PROGRESS', 'COMPLETED'), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('job', 'qualification_ids',
               existing_type=mysql.VARCHAR(length=4096),
               nullable=False)
    op.alter_column('job', 'preference_ids',
               existing_type=mysql.VARCHAR(length=4096),
               nullable=False)
    op.drop_column('segment', 'status')
    op.add_column('segment', sa.Column('status', sa.Enum('DRAFT', 'IN PROGRESS', 'COMPLETED'), nullable=False))
    ### end Alembic commands ###
