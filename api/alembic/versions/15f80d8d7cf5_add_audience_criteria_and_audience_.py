"""add audience criteria and audience keywords

Revision ID: 15f80d8d7cf5
Revises: 56c6f3416751
Create Date: 2016-08-18 10:36:38.711720

"""

# revision identifiers, used by Alembic.
revision = '15f80d8d7cf5'
down_revision = '56c6f3416751'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from sqlalchemy.dialects.mysql import LONGTEXT


def upgrade():
    op.create_table('audience_criteria',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('audience_id', sa.BigInteger(), nullable=False),
        sa.Column('criteria_type', sa.String(length=50), nullable=False),
        sa.Column('criteria_val', LONGTEXT, nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.ForeignKeyConstraint(['audience_id'], ['audience.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('audience_id', 'criteria_type', name='uniq_audience_id_criteria_type'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )
    op.create_table('audience_keywords',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('audience_id', sa.BigInteger(), nullable=False),
        sa.Column('keywords', LONGTEXT, nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.ForeignKeyConstraint(['audience_id'], ['audience.id'], ),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )


def downgrade():
    op.drop_table('audience_keywords')
    op.drop_table('audience_criteria')
