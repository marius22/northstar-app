"""remove accountInsights add indicatorWhiteLIst

Revision ID: 46427316c0f1
Revises: 1285e5e90939
Create Date: 2016-07-08 15:44:20.551770

"""

# revision identifiers, used by Alembic.
revision = '46427316c0f1'
down_revision = '1285e5e90939'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():

    op.create_table('indicator_white_list',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('category', sa.String(length=50), nullable=False),
        sa.Column('indicator', sa.String(length=100), nullable=False),
        sa.Column('display_name', sa.String(length=255), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('category', 'indicator'),
    )
    op.drop_table('account_insights')


def downgrade():

    op.create_table('account_insights',
        sa.Column('id', mysql.INTEGER(display_width=11), nullable=False),
        sa.Column('created_ts', mysql.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', mysql.TIMESTAMP(), nullable=True),
        sa.Column('account_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
        sa.Column('job_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=False),
        sa.Column('category', mysql.VARCHAR(length=50), nullable=False),
        sa.Column('insight', mysql.VARCHAR(length=100), nullable=False),
        sa.Column('display_name', mysql.VARCHAR(length=255), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        mysql_default_charset=u'latin1',
        mysql_engine=u'InnoDB'
    )
    op.drop_table('indicator_white_list')
