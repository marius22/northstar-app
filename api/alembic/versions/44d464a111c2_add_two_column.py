"""add two column

Revision ID: 44d464a111c2
Revises: 15f80d8d7cf5
Create Date: 2016-08-22 15:21:51.803367

"""

# revision identifiers, used by Alembic.
revision = '44d464a111c2'
down_revision = '15f80d8d7cf5'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from sqlalchemy import sql


def upgrade():
    op.add_column('audience', sa.Column('real_time_scoring_enable_ts', sa.TIMESTAMP(), nullable=True))
    op.add_column('audience', sa.Column('real_time_scoring_enabled', sa.Boolean(),
                                        server_default=sql.expression.false(), nullable=False))


def downgrade():
    op.drop_column('audience', 'real_time_scoring_enabled')
    op.drop_column('audience', 'real_time_scoring_enable_ts')

