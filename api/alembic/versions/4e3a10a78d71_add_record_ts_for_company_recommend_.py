"""add record_ts for company_recommend_count

Revision ID: 4e3a10a78d71
Revises: 426884791e6d
Create Date: 2016-04-06 17:51:12.020529

"""

# revision identifiers, used by Alembic.
revision = '4e3a10a78d71'
down_revision = '426884791e6d'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('company_recommendation_count',
                  sa.Column('record_ts', sa.TIMESTAMP(), nullable=True))


def downgrade():
    op.drop_column('company_recommendation_count', 'record_ts')
