"""merge

Revision ID: 4862bfea958c
Revises: 15f80d8d7cf5, 268cffa81e4
Create Date: 2016-08-23 13:43:38.702215

"""

# revision identifiers, used by Alembic.
revision = '4862bfea958c'
down_revision = ('15f80d8d7cf5', '268cffa81e4')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
