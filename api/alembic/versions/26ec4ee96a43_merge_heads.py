"""merge heads

Revision ID: 26ec4ee96a43
Revises: 3ee8ce6c185b, 4528421c40ef
Create Date: 2017-03-02 20:30:28.482008

"""

# revision identifiers, used by Alembic.
revision = '26ec4ee96a43'
down_revision = ('3ee8ce6c185b', '4528421c40ef')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
