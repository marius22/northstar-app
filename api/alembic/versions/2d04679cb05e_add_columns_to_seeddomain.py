"""add columns to seedDomain

Revision ID: 2d04679cb05e
Revises: 4da11967af7d
Create Date: 2016-07-12 14:47:04.782882

"""

# revision identifiers, used by Alembic.
revision = '2d04679cb05e'
down_revision = '4da11967af7d'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.add_column('seed_domain', sa.Column('company_name', sa.String(length=255), nullable=True))
    op.add_column('seed_domain', sa.Column('sf_account_id', sa.String(length=255), nullable=True))
    op.add_column('seed_domain', sa.Column('sf_account_name', sa.String(length=255), nullable=True))
    op.add_column('sf_pull_request', sa.Column('account_id', sa.Integer(), server_default='0', nullable=False))
    op.drop_constraint(u'sf_pull_request_ibfk_1', 'sf_pull_request', type_='foreignkey')


def downgrade():
    op.create_foreign_key(u'sf_pull_request_ibfk_1', 'sf_pull_request', 'segment', ['segment_id'], ['id'])
    op.drop_column('sf_pull_request', 'account_id')
    op.drop_column('seed_domain', 'sf_account_name')
    op.drop_column('seed_domain', 'sf_account_id')
    op.drop_column('seed_domain', 'company_name')
