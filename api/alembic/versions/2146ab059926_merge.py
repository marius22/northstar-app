"""merge

Revision ID: 2146ab059926
Revises: 2b4fe5fd2bf7, 58d8c5befb4d
Create Date: 2016-04-22 19:17:47.487444

"""

# revision identifiers, used by Alembic.
revision = '2146ab059926'
down_revision = ('2b4fe5fd2bf7', '58d8c5befb4d')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
