"""empty message

Revision ID: 2a229a26d508
Revises: 39d3e32b120d, 470dcdd18982
Create Date: 2016-09-18 13:23:13.840218

"""

# revision identifiers, used by Alembic.
revision = '2a229a26d508'
down_revision = ('39d3e32b120d', '470dcdd18982')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
