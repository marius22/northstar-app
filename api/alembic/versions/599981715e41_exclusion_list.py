"""exclusion list

Revision ID: 599981715e41
Revises: 7cf89619843
Create Date: 2016-11-16 13:50:45.407490

"""

# revision identifiers, used by Alembic.
revision = '599981715e41'
down_revision = '7cf89619843'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('audience_exclusion_list',
                    sa.Column('id', sa.BigInteger(), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('audience_id', sa.BigInteger(), nullable=False),
                    sa.Column('exclusion_list_id', sa.BigInteger(), nullable=False),
                    sa.ForeignKeyConstraint(['audience_id'], ['audience.id']),
                    sa.ForeignKeyConstraint(['exclusion_list_id'], ['audience.id']),
                    sa.PrimaryKeyConstraint('id'),
                    mysql_charset='utf8',
                    mysql_engine='InnoDB'
                    )


def downgrade():
    op.drop_table('audience_exclusion_list')
