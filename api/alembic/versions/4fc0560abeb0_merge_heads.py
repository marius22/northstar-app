"""merge heads

Revision ID: 4fc0560abeb0
Revises: 2a1f04b5767f, 2effe78a389b
Create Date: 2017-01-12 15:45:25.026178

"""

# revision identifiers, used by Alembic.
revision = '4fc0560abeb0'
down_revision = ('2a1f04b5767f', '2effe78a389b')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
