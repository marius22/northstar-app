"""add segment metric

Revision ID: 45886dcddb82
Revises: aa2847f064e
Create Date: 2016-04-28 13:35:59.817475

"""

# revision identifiers, used by Alembic.
revision = '45886dcddb82'
down_revision = 'aa2847f064e'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('segment_metric',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('segment_id', sa.BigInteger(), nullable=False),
        sa.Column('type', sa.String(length=50), nullable=False),
        sa.Column('value', sa.Text(), nullable=False),
        sa.ForeignKeyConstraint(['segment_id'], ['segment.id'], ),
        sa.UniqueConstraint('segment_id', 'type'),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('segment_metric')
