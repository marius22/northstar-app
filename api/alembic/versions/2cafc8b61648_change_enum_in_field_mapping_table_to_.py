"""change enum in field mapping table to support target_type marketo_sandbox

Revision ID: 2cafc8b61648
Revises: 40bfd9019982
Create Date: 2017-02-13 19:45:10.315835

"""

# revision identifiers, used by Alembic.
revision = '2cafc8b61648'
down_revision = '40bfd9019982'
branch_labels = None
depends_on = None

from alembic import op


def upgrade():
    op.execute("ALTER TABLE publish_field_mapping CHANGE "
               "target_type target_type ENUM('salesforce','marketo','csv', 'marketo_sandbox')")


def downgrade():
    op.execute("ALTER TABLE publish_field_mapping CHANGE "
               "target_type target_type ENUM('salesforce','marketo','csv')")
