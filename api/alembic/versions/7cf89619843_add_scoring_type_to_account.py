"""add scoring type to account

Revision ID: 7cf89619843
Revises: 1592f5e919e6
Create Date: 2016-11-09 16:05:16.783506

"""

# revision identifiers, used by Alembic.
revision = '7cf89619843'
down_revision = '1592f5e919e6'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('account', sa.Column('realtime_scoring', sa.SmallInteger(),
                                       server_default=sa.schema.DefaultClause("0"), nullable=True))
    op.execute("update  account set realtime_scoring = 2 where real_time_scoring_object>0")


def downgrade():
    op.drop_column('account', 'realtime_scoring')
