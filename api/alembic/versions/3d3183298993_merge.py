"""merge

Revision ID: 3d3183298993
Revises: 5866589234ad, 18d7887d10e0
Create Date: 2016-07-22 15:13:58.209766

"""

# revision identifiers, used by Alembic.
revision = '3d3183298993'
down_revision = ('5866589234ad', '18d7887d10e0')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
