"""mas integration and field mapping add key

Revision ID: 58313f43dc9
Revises: 55b3e457b75e
Create Date: 2016-09-10 17:12:31.700764

"""

# revision identifiers, used by Alembic.
revision = '58313f43dc9'
down_revision = '55b3e457b75e'
branch_labels = None
depends_on = None

from alembic import op


def upgrade():
    op.execute("ALTER TABLE mas_token CHANGE id id INT(11) AUTO_INCREMENT PRIMARY KEY")
    op.execute("ALTER TABLE mas_field_mapping CHANGE id id INT(11) AUTO_INCREMENT PRIMARY KEY")


def downgrade():
    op.execute("ALTER TABLE mas_token CHANGE id id INT(11)")
    op.execute("ALTER TABLE mas_token DROP PRIMARY KEY")
    op.execute("ALTER TABLE mas_field_mapping CHANGE id id INT(11)")
    op.execute("ALTER TABLE mas_field_mapping DROP PRIMARY KEY")
