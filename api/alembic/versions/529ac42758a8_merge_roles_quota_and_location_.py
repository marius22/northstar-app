"""merge roles/quota and location migrations

Revision ID: 529ac42758a8
Revises: 53e146d93e6d, a2ac1801762
Create Date: 2016-03-23 10:10:39.580786

"""

# revision identifiers, used by Alembic.
revision = '529ac42758a8'
down_revision = ('53e146d93e6d', 'a2ac1801762')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
