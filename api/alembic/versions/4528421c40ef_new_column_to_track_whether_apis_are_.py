"""New column to track whether APIs are locked

Revision ID: 4528421c40ef
Revises: baa9f8b9234
Create Date: 2017-03-02 10:31:29.671833

"""

# revision identifiers, used by Alembic.
revision = '4528421c40ef'
down_revision = 'baa9f8b9234'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('account_quota', sa.Column('enable_apis', sa.Boolean(), server_default=sa.text(u'true'), nullable=True))


def downgrade():
    op.drop_column('account_quota', 'enable_apis')
