"""change sfdc config's value to longtext 

Revision ID: 4fd29c175f58
Revises: 3d3183298993
Create Date: 2016-07-26 14:42:27.048881

"""

# revision identifiers, used by Alembic.
revision = '4fd29c175f58'
down_revision = '3d3183298993'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.mysql import LONGTEXT


def upgrade():
    op.alter_column('sfdc_config', 'config',
                    existing_type=sa.Text(),
                    type_=LONGTEXT)


def downgrade():
    op.alter_column('sfdc_config', 'config',
                    existing_type=LONGTEXT,
                    type_=sa.Text())
