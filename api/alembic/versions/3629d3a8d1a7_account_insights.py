"""account insights

Revision ID: 3629d3a8d1a7
Revises: 339ac7e7165f
Create Date: 2016-06-28 12:41:29.782147

"""

# revision identifiers, used by Alembic.
revision = '3629d3a8d1a7'
down_revision = '339ac7e7165f'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_table('account_insights',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('account_id', sa.Integer(), nullable=False),
        sa.Column('job_id', sa.Integer(), nullable=False),
        sa.Column('category', sa.String(length=50), nullable=False),
        sa.Column('insight', sa.String(length=100), nullable=False),
        sa.Column('display_name', sa.String(length=255), nullable=False),
        sa.UniqueConstraint('account_id', 'job_id', 'category', 'insight'),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('segment_insights')
