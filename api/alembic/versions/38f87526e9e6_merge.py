"""merge

Revision ID: 38f87526e9e6
Revises: 1204c8c0d630, 18076831374a
Create Date: 2016-04-27 14:32:01.857913

"""

# revision identifiers, used by Alembic.
revision = '38f87526e9e6'
down_revision = ('1204c8c0d630', '18076831374a')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
