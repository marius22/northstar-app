"""add mas token table

Revision ID: 3edd215249f1
Revises: 4fd29c175f58
Create Date: 2016-08-04 15:12:00.261930

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '3edd215249f1'
down_revision = '4fd29c175f58'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('mas_token',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('account_id', sa.Integer(), nullable=False),
                    sa.Column('mas_type', sa.String(length=255), nullable=False),
                    sa.Column('mas_id', sa.String(length=255), nullable=False),
                    sa.Column('token', sa.String(length=2048), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    )


def downgrade():
    op.drop_table('mas_token')

