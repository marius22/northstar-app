"""more indexes on recommendation table

Revision ID: 573c197778dd
Revises: 2146ab059926
Create Date: 2016-04-27 14:51:10.550698

"""

# revision identifiers, used by Alembic.
revision = '573c197778dd'
down_revision = '2146ab059926'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index("ix_recommendation_job_id_revenue", "recommendation", ["job_id", "revenue"])
    op.create_index("ix_recommendation_job_id_industry", "recommendation", ["job_id", "industry"])
    op.create_index("ix_recommendation_job_id_employee_size", "recommendation", ["job_id", "employee_size"])
    op.create_index("ix_recommendation_job_id_state", "recommendation", ["job_id", "state"])


def downgrade():
    op.drop_index("ix_recommendation_job_id_state", "recommendation")
    op.drop_index("ix_recommendation_job_id_employee_size", "recommendation")
    op.drop_index("ix_recommendation_job_id_industry", "recommendation")
    op.drop_index("ix_recommendation_job_id_revenue", "recommendation")
