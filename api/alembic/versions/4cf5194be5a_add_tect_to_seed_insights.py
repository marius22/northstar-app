"""add tect to seed insights

Revision ID: 4cf5194be5a
Revises: 1aaa4b367285
Create Date: 2016-03-29 21:48:57.217891

"""

# revision identifiers, used by Alembic.
revision = '4cf5194be5a'
down_revision = '1aaa4b367285'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('seed_candidate', sa.Column('tech', sa.String(length=255), nullable=True))


def downgrade():
    op.drop_column('seed_candidate', 'tech')

