"""add company_column_name to company_insightOC

Revision ID: 10cba38197c6
Revises: 589e04e010d9
Create Date: 2016-09-01 14:30:24.498520

"""

# revision identifiers, used by Alembic.
revision = '10cba38197c6'
down_revision = '589e04e010d9'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():

    op.add_column('company_insight', sa.Column('company_column_name',
                                               sa.String(length=255), server_default='', nullable=False))
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_operations'")
    op.execute("update company_insight set company_column_name = 'company.domain' where column_name = 'domain'")
    op.execute("update company_insight set company_column_name = 'company.revenue' where column_name = 'revenue'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'similar_departments'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_research_dev'")
    op.execute(
        "update company_insight set company_column_name = 'company.linkedin_url' where column_name = 'linkedin_url'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'growth'")
    op.execute("update company_insight set company_column_name = 'company.city' where column_name = 'city'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'similar_technologies'")
    op.execute("update company_insight set company_column_name = 'company.id' where column_name = 'company_id'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'alexa_rank'")
    op.execute("update company_insight set company_column_name = 'company.zipcode' where column_name = 'zipcode'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_legal'")
    op.execute("update company_insight set company_column_name = 'company.state' where column_name = 'state'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'score'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_educator'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_administrative'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_finance'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_medical_heath'")
    op.execute("update company_insight set company_column_name = 'company.name' where column_name = 'company_name'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_hr'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_marketing'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'tech_categories'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_computing_it'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_sales'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'similar_companies'")
    op.execute(
        "update company_insight set company_column_name = 'company.employee_size' where column_name = 'employee_size'")
    op.execute("update company_insight set company_column_name = 'company.country' where column_name = 'country'")
    op.execute("update company_insight set company_column_name = 'company.industry' where column_name = 'industry'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'd_engineering'")
    op.execute("update company_insight set company_column_name = '' where column_name = 'tech'")

    op.add_column('account_quota',
                  sa.Column('real_time_scoring_insight_ids', sa.String(length=4096), server_default='[]', nullable=False))
    op.add_column('account_quota', sa.Column('enable_in_crm', sa.Boolean(), server_default='0', nullable=False))
    op.add_column('account_quota', sa.Column('enable_not_in_crm', sa.Boolean(), server_default='0', nullable=False))
    op.add_column('account_quota',
                  sa.Column('inbound_leads_target', sa.String(length=20), server_default='accounts', nullable=False))


def downgrade():
    op.drop_column('company_insight', 'company_column_name')

    op.drop_column('account_quota', 'inbound_leads_target')
    op.drop_column('account_quota', 'enable_not_in_crm')
    op.drop_column('account_quota', 'enable_in_crm')
    op.drop_column('account_quota', 'real_time_scoring_insight_ids')
