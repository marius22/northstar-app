"""add show in sfdc to audiencd

Revision ID: 589e04e010d9
Revises: 4883f9a18dfa
Create Date: 2016-08-30 14:16:53.960657

"""

# revision identifiers, used by Alembic.
revision = '589e04e010d9'
down_revision = '4883f9a18dfa'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('audience', sa.Column('show_in_sfdc', sa.Boolean(), server_default='0', nullable=False))
    op.alter_column('audience', 'is_private', server_default='1')


def downgrade():

    op.drop_column('audience', 'show_in_sfdc')
    op.alter_column('audience', 'is_private', server_default=None)
