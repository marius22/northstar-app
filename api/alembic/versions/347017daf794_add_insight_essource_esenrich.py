"""add insight essource, esenrich

Revision ID: 347017daf794
Revises: 2cafc8b61648
Create Date: 2017-02-21 13:17:30.509619

"""

# revision identifiers, used by Alembic.
revision = '347017daf794'
down_revision = '2cafc8b61648'
branch_labels = None
depends_on = None

from alembic import op

new_insights = ["esEnriched", "esSource"]


def upgrade():
    for insight in new_insights:
        op.execute("insert into company_insight"
                   "(created_ts, updated_ts, column_name, display_name, company_column_name)"
                   " values (now(), now(), '', '{insight_name}', '')".
                   format(insight_name=insight))


def downgrade():
    for insight in new_insights:
        op.execute("delete from company_insight where display_name = '{insight_name}'".format(insight_name=insight))
