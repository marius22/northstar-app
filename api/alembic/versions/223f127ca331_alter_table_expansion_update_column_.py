"""alter table expansion update column expansion val to longtext

Revision ID: 223f127ca331
Revises: 598c52932f44
Create Date: 2016-07-20 17:10:53.747214

"""

# revision identifiers, used by Alembic.
revision = '223f127ca331'
down_revision = '598c52932f44'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.mysql import LONGTEXT


def upgrade():
    op.alter_column('expansion', 'expansion_val',
        existing_type=sa.Text(),
        type_=LONGTEXT)


def downgrade():
    op.alter_column('expansion', 'expansion_val',
        existing_type=LONGTEXT,
        type_=sa.Text())
