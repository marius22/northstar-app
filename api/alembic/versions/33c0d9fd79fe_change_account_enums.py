"""Change account enums

Revision ID: 33c0d9fd79fe
Revises: 8272ffee510
Create Date: 2016-03-23 16:14:16.428320

"""

# revision identifiers, used by Alembic.
revision = '33c0d9fd79fe'
down_revision = '8272ffee510'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('account', 'account_type')
    op.add_column('account', sa.Column('account_type', sa.Enum('ES', 'PAYING', 'TRIAL'), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('account', 'account_type')
    op.add_column('account', sa.Column('account_type', sa.Enum('TRIAL', 'PAYING'), nullable=False))
    ### end Alembic commands ###
