"""add api token

Revision ID: 24062aa00d8e
Revises: 1f0b4de99351
Create Date: 2016-06-16 17:52:12.328707

"""

# revision identifiers, used by Alembic.
revision = '24062aa00d8e'
down_revision = '1f0b4de99351'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('api_token',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('token', sa.String(length=255), server_default='', nullable=False),
        sa.Column('app_version', sa.String(length=100), server_default='', nullable=False),
        sa.Column('ip', sa.String(length=50), server_default='', nullable=False),
        sa.Column('locked', sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('token', 'app_version')
    )
    op.execute("ALTER TABLE company_new DROP FOREIGN KEY company_new_ibfk_1")
    op.drop_column('company_new', 'segment_id')


def downgrade():
    op.drop_table('api_token')
    op.add_column('company_new', sa.Column('segment_id', sa.Integer(), nullable=True))

