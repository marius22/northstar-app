"""add role_id in user_invitation table

Revision ID: 1d38e97243c8
Revises: 36475feaa0c3
Create Date: 2016-09-14 16:09:54.923130

"""

# revision identifiers, used by Alembic.
revision = '1d38e97243c8'
down_revision = '36475feaa0c3'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('user_invitation', sa.Column('role_id', sa.Integer(), nullable=False))


def downgrade():
    op.drop_column('user_invitation', 'role_id')
