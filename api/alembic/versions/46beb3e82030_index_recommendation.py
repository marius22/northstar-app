"""index recommendation

Revision ID: 46beb3e82030
Revises: 42658f91f242
Create Date: 2016-04-08 16:24:56.479345

"""

# revision identifiers, used by Alembic.
revision = '46beb3e82030'
down_revision = '42658f91f242'
branch_labels = None
depends_on = None

from alembic import op


def upgrade():
    op.create_index("index_job_id", "recommendation", ["job_id"])


def downgrade():
    op.drop_index("index_job_id", "recommendation")
