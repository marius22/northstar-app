"""recommendation table partition

Revision ID: 129d4b06c7f5
Revises: 4e3a10a78d71
Create Date: 2016-04-07 09:44:59.297234

"""

# revision identifiers, used by Alembic.
revision = '129d4b06c7f5'
down_revision = '4e3a10a78d71'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("ALTER TABLE recommendation DROP PRIMARY KEY, ADD PRIMARY KEY(`id`, `job_id`)")
    op.execute("ALTER TABLE recommendation PARTITION BY hash(`job_id`) PARTITIONS 100")


def downgrade():
    op.execute("Alter TABLE recommendation remove partitioning")
    op.execute("ALTER TABLE recommendation DROP PRIMARY KEY, ADD PRIMARY KEY(`id`)")
