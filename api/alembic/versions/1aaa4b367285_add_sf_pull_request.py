"""add sf_pull_request

Revision ID: 1aaa4b367285
Revises: 4d7ae0d7640
Create Date: 2016-03-28 22:01:13.357410

"""

revision = '1aaa4b367285'
down_revision = '4d7ae0d7640'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('sf_pull_request',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('start_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('end_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('user_id', sa.BigInteger(), nullable=False),
        sa.Column('segment_id', sa.BigInteger(), nullable=False),
        sa.Column('status', sa.String(length=255), nullable=False),
        sa.ForeignKeyConstraint(['segment_id'], ['segment.id'], ),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.add_column('seed_candidate', sa.Column('sf_account_id', sa.String(length=255), nullable=True))
    op.add_column('seed_candidate', sa.Column('sf_account_name', sa.String(length=255), nullable=True))
    conn = op.get_bind()
    conn.execute("insert into source(source_name) values('SALESFORCE')")


def downgrade():
    op.drop_column('seed_candidate', 'sf_account_name')
    op.drop_column('seed_candidate', 'sf_account_id')
    op.drop_table('sf_pull_request')
    conn = op.get_bind()
    conn.execute("delete from source where source_name = 'SALESFORCE'")
