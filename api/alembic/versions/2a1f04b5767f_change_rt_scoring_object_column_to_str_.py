"""change rt scoring object column to str type

Revision ID: 2a1f04b5767f
Revises: 38f4802f4c11
Create Date: 2017-01-04 11:51:05.632979

"""

# revision identifiers, used by Alembic.
revision = '2a1f04b5767f'
down_revision = '38f4802f4c11'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('account', sa.Column('realtime_scoring_object', sa.String(length=255), nullable=True, server_default='["account"]'))
    op.execute("update account set realtime_scoring_object = '[]' where real_time_scoring_object = 0")
    op.execute("update account set realtime_scoring_object = '[\"lead\",\"account\"]' where real_time_scoring_object = 1")
    op.execute("update account set realtime_scoring_object = '[\"lead\"]' where real_time_scoring_object = 2")
    op.execute("update account set realtime_scoring_object = '[\"account\"]' where real_time_scoring_object = 3")
    op.drop_column('account', 'real_time_scoring_object')

def downgrade():
    op.add_column('account', sa.Column('real_time_scoring_object', sa.SmallInteger(), server_default='3', nullable=True))
    op.execute("update account set real_time_scoring_object = 0 where realtime_scoring_object = '[]'")
    op.execute("update account set real_time_scoring_object = 1 where realtime_scoring_object = '[\"lead\",\"account\"]'")
    op.execute("update account set real_time_scoring_object = 2 where realtime_scoring_object = '[\"lead\"]'")
    op.execute("update account set real_time_scoring_object = 3 where realtime_scoring_object = '[\"account\"]'")
    op.drop_column('account', 'realtime_scoring_object')
