"""merge branch

Revision ID: 4d4c603f5536
Revises: 36475feaa0c3, a8abe89107f
Create Date: 2016-09-17 16:22:57.401930

"""

# revision identifiers, used by Alembic.
revision = '4d4c603f5536'
down_revision = ('36475feaa0c3', 'a8abe89107f')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
