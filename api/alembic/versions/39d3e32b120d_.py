"""empty message

Revision ID: 39d3e32b120d
Revises: 4d4c603f5536, 1d38e97243c8
Create Date: 2016-09-17 20:51:25.768413

"""

# revision identifiers, used by Alembic.
revision = '39d3e32b120d'
down_revision = ('4d4c603f5536', '1d38e97243c8')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
