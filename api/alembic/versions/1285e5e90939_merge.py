"""merge

Revision ID: 1285e5e90939
Revises: 454b41920fbe, 3629d3a8d1a7
Create Date: 2016-06-30 11:33:25.412379

"""

# revision identifiers, used by Alembic.
revision = '1285e5e90939'
down_revision = ('454b41920fbe', '3629d3a8d1a7')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
