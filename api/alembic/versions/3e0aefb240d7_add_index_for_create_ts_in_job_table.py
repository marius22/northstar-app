"""add index for create_ts in job table

Revision ID: 3e0aefb240d7
Revises: 1d2cc0603b64
Create Date: 2016-05-09 18:11:48.189525

"""

# revision identifiers, used by Alembic.
revision = '3e0aefb240d7'
down_revision = '1d2cc0603b64'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_index('idx_job_created_ts', 'job', ['created_ts'], unique=False)


def downgrade():
    op.drop_index('idx_job_created_ts', table_name='job')
