"""delete domain from account

Revision ID: 1e8d69cc49b
Revises: 38a3544c56b2
Create Date: 2016-03-17 16:08:42.685080

"""

# revision identifiers, used by Alembic.
revision = '1e8d69cc49b'
down_revision = '38a3544c56b2'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():

    op.drop_index('domain', table_name='account')
    op.drop_column('account', 'domain')
    op.add_column('user', sa.Column('activated', sa.Boolean(), nullable=False))
    op.create_table('code',
                    sa.Column('id', sa.BigInteger(), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('code', sa.String(length=255), nullable=False),
                    sa.Column('value', sa.String(length=255), nullable=False),
                    sa.Column('code_type', sa.SMALLINT, nullable=False),
                    sa.Column('expired_ts', sa.TIMESTAMP(), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('code'),
                    mysql_charset='utf8',
                    mysql_engine='InnoDB'
                    )


def downgrade():

    op.drop_column('user', 'activated')
    op.add_column('account', sa.Column('domain', mysql.VARCHAR(length=255), nullable=False))
    op.create_index('domain', 'account', ['domain'], unique=True)
    op.drop_table('code')
