"""add enable ads config column

Revision ID: 1a1dd22e26c
Revises: 4c25fa7a3b08
Create Date: 2016-04-18 15:42:24.727256

"""

# revision identifiers, used by Alembic.
revision = '1a1dd22e26c'
down_revision = '4c25fa7a3b08'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('account_quota', sa.Column('enable_ads', sa.Boolean(), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('account_quota', 'enable_ads')
    ### end Alembic commands ###
