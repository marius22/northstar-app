"""add sfdc preference

Revision ID: 35c9e3d0c9dc
Revises: 305f3648aac5
Create Date: 2016-09-05 13:35:51.504443

"""

# revision identifiers, used by Alembic.
revision = '35c9e3d0c9dc'
down_revision = '305f3648aac5'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_table('user_preference',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('user_id', sa.BigInteger(), nullable=False),
        sa.Column('app_name', sa.String(length=50), nullable=False),
        sa.Column('type', sa.String(length=50), nullable=False),
        sa.Column('val', sa.Text(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('user_id', 'app_name', 'type', name='uniq_user_id_app_name_type')
    )


def downgrade():
    op.drop_table('user_preference')
