"""add new column to user_preference and user_invitation

Revision ID: 36475feaa0c3
Revises: 348e3b4e0a10
Create Date: 2016-09-14 13:39:54.164667

"""

# revision identifiers, used by Alembic.
revision = '36475feaa0c3'
down_revision = '348e3b4e0a10'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('user_preference', sa.Column('email', sa.String(length=255), server_default='', nullable=False))
    op.add_column('user_preference', sa.Column('sfdc_user_id', sa.String(length=255), server_default='', nullable=False))
    op.create_unique_constraint('uniq_sfdc_user_id_app_name_type', 'user_preference', ['sfdc_user_id', 'app_name', 'type'])
    op.drop_constraint(u'user_preference_ibfk_1', 'user_preference', type_='foreignkey')
    op.drop_index('uniq_user_id_app_name_type', table_name='user_preference')
    op.drop_column('user_preference', 'user_id')


def downgrade():
    op.add_column('user_preference', sa.Column('user_id', mysql.BIGINT(display_width=20), autoincrement=False, nullable=False))
    op.create_foreign_key(u'user_preference_ibfk_1', 'user_preference', 'user', ['user_id'], ['id'])
    op.create_index('uniq_user_id_app_name_type', 'user_preference', ['user_id', 'app_name', 'type'], unique=True)
    op.drop_constraint('uniq_sfdc_user_id_app_name_type', 'user_preference', type_='unique')
    op.drop_column('user_preference', 'sfdc_user_id')
    op.drop_column('user_preference', 'email')
