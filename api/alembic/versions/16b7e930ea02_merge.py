"""merge

Revision ID: 16b7e930ea02
Revises: 4b0cd942a69e, 3099e595279
Create Date: 2016-04-19 14:45:33.481659

"""

# revision identifiers, used by Alembic.
revision = '16b7e930ea02'
down_revision = ('4b0cd942a69e', '3099e595279')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
