"""add is_deleted to model

Revision ID: 4f060be5b048
Revises: 3884ff21ed0
Create Date: 2016-09-22 17:13:48.713007

"""

# revision identifiers, used by Alembic.
revision = '4f060be5b048'
down_revision = '3884ff21ed0'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('model', sa.Column('is_deleted', sa.Boolean(), default=False, nullable=False))


def downgrade():
    op.drop_column('model', 'is_deleted')
