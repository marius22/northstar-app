"""add two columns

Revision ID: 3eed7f346372
Revises: 4f060be5b048
Create Date: 2016-09-22 12:22:47.871324

"""

# revision identifiers, used by Alembic.
revision = '3eed7f346372'
down_revision = '4f060be5b048'
branch_labels = None
depends_on = None


import sqlalchemy as sa
from alembic import op
from sqlalchemy import sql


def upgrade():
    op.add_column('account_quota', sa.Column('enable_find_company', sa.Boolean(), server_default="0", nullable=False))
    op.add_column('account_quota', sa.Column('enable_score_list', sa.Boolean(), server_default="1", nullable=False))


def downgrade():
    op.drop_column('account_quota', 'enable_find_company')
    op.drop_column('account_quota', 'enable_score_list')