"""modify similar scores column data type

Revision ID: 2168bc29b93a
Revises: 2146ab059926
Create Date: 2016-04-26 15:29:14.730910

"""

# revision identifiers, used by Alembic.
revision = '2168bc29b93a'
down_revision = '2146ab059926'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('recommendation', 'similar_c_score', type_=sa.SmallInteger, existing_type=sa.Integer)
    op.alter_column('recommendation', 'similar_t_score', type_=sa.SmallInteger, existing_type=sa.Integer)
    op.alter_column('recommendation', 'similar_d_score', type_=sa.SmallInteger, existing_type=sa.Integer)
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('recommendation', 'similar_c_score', type_=sa.Integer, existing_type=sa.SmallInteger)
    op.alter_column('recommendation', 'similar_t_score', type_=sa.Integer, existing_type=sa.SmallInteger)
    op.alter_column('recommendation', 'similar_d_score', type_=sa.Integer, existing_type=sa.SmallInteger)
    ### end Alembic commands ###
