"""create table publish_field_mapping

Revision ID: 2effe78a389b
Revises: 38f4802f4c11
Create Date: 2017-01-03 13:46:25.565676

"""

# revision identifiers, used by Alembic.
revision = '2effe78a389b'
down_revision = '38f4802f4c11'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'publish_field_mapping',
        sa.Column('account_id', sa.BigInteger(), nullable=False),
        sa.Column('insight_id', sa.BigInteger(), nullable=True),
        sa.Column('model_id', sa.Integer(), nullable=True),
        sa.Column('target_type', sa.Enum('salesforce', 'marketo', 'csv', name='target_type'), nullable=False),
        sa.Column('target_object', sa.Enum('lead', 'contact', 'account', 'company', name='target_object'),
                  nullable=False),
        sa.Column('target_org_id', sa.String(255), nullable=False),
        sa.Column('target_field', sa.String(255), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.ForeignKeyConstraint(['account_id'], ['account.id'], ),
        sa.ForeignKeyConstraint(['insight_id'], ['company_insight.id'], ),
        sa.ForeignKeyConstraint(['model_id'], ['model.id'], ),
        sa.PrimaryKeyConstraint('target_type', 'target_object', 'target_org_id', 'target_field')
    )


def downgrade():
    op.drop_table('publish_field_mapping')
