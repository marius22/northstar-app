"""merge heads

Revision ID: 305f3648aac5
Revises: 30249eb4f48b, 515f5a29292d
Create Date: 2016-09-02 10:43:35.160742

"""

# revision identifiers, used by Alembic.
revision = '305f3648aac5'
down_revision = ('30249eb4f48b', '515f5a29292d')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
