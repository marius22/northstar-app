"""merge

Revision ID: 41bee2195cdf
Revises: 10cba38197c6, 305f3648aac5
Create Date: 2016-09-06 14:23:56.256050

"""

# revision identifiers, used by Alembic.
revision = '41bee2195cdf'
down_revision = ('10cba38197c6', '305f3648aac5')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
