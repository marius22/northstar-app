"""add_fields_account_user_role_for_register

Revision ID: 1e11cd9a45ba
Revises: 348e3b4e0a10
Create Date: 2016-09-14 13:48:04.447664

"""

# revision identifiers, used by Alembic.
revision = '1e11cd9a45ba'
down_revision = '348e3b4e0a10'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('account', sa.Column('domain', sa.String(length=255), nullable=True))
    op.add_column('account', sa.Column('is_primary', sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column('account', 'is_primary')
    op.drop_column('account', 'domain')
