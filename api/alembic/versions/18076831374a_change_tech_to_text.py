"""change tech to text

Revision ID: 18076831374a
Revises: 2146ab059926
Create Date: 2016-04-26 11:35:43.635285

"""

# revision identifiers, used by Alembic.
revision = '18076831374a'
down_revision = '2146ab059926'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('seed_candidate', 'tech', type_=sa.Text(), existing_type=sa.String(4096))
    op.alter_column('recommendation', 'tech', type_=sa.Text(), existing_type=sa.String(4096))


def downgrade():
    op.alter_column('seed_candidate', 'tech', type_=sa.String(4096), existing_type=sa.Text())
    op.alter_column('recommendation', 'tech', type_=sa.String(4096), existing_type=sa.Text())
