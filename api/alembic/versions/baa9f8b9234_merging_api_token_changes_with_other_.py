"""Merging api_token changes with other schema changes

Revision ID: baa9f8b9234
Revises: 347017daf794, 5652cb4982c7
Create Date: 2017-02-22 15:10:58.860145

"""

# revision identifiers, used by Alembic.
revision = 'baa9f8b9234'
down_revision = ('347017daf794', '5652cb4982c7')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
