"""merge multiple head

Revision ID: 4d7ae0d7640
Revises: 185d1abd865b, 33c0d9fd79fe
Create Date: 2016-03-23 22:50:54.310065

"""

# revision identifiers, used by Alembic.
revision = '4d7ae0d7640'
down_revision = ('185d1abd865b', '33c0d9fd79fe')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
