"""merge

Revision ID: 1f847485b749
Revises: 191dce1a85f8, 4cf5194be5a
Create Date: 2016-03-30 14:34:43.182130

"""

# revision identifiers, used by Alembic.
revision = '1f847485b749'
down_revision = ('191dce1a85f8', '4cf5194be5a')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
