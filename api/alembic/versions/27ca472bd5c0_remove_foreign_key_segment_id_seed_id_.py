"""remove foreign key segment_id, seed_id of job

Revision ID: 27ca472bd5c0
Revises: 1285e5e90939
Create Date: 2016-07-04 12:57:36.272303

"""

# revision identifiers, used by Alembic.
revision = '27ca472bd5c0'
down_revision = '1285e5e90939'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('job', sa.Column('account_id', sa.Integer(), server_default='0', nullable=False))
    op.add_column('job', sa.Column('type', sa.String(length=50), server_default='segment', nullable=False))
    op.drop_constraint(u'job_ibfk_2', 'job', type_='foreignkey')
    op.drop_constraint(u'job_ibfk_1', 'job', type_='foreignkey')

    op.create_table('model',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('name', sa.String(length=100), nullable=False),
                    sa.Column('owner_id', sa.Integer(), nullable=False),
                    sa.Column('type', sa.String(length=20), nullable=False),
                    sa.Column('seed_id', sa.Integer(), nullable=False),
                    sa.Index('idx_owner_id_type', 'owner_id', 'type'),
                    sa.PrimaryKeyConstraint('id'),
                    mysql_charset='utf8',
                    mysql_engine='InnoDB'
                    )
    op.create_table('model_seed',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('seed_domain_ids', sa.Text(), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    mysql_charset='utf8',
                    mysql_engine='InnoDB'
                    )
    op.create_table('seed_domain',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
                    sa.Column('domain', sa.String(length=100), nullable=False),
                    sa.Index('uniq_domain', 'domain', unique=True),
                    sa.PrimaryKeyConstraint('id'),
                    mysql_charset='utf8',
                    mysql_engine='InnoDB'
                    )


def downgrade():
    op.create_foreign_key('job_ibfk_1', 'job', 'seed', ['seed_id'], ['id'])
    op.create_foreign_key('job_ibfk_2', 'job', 'segment', ['segment_id'], ['id'])
    op.drop_column('job', 'type')
    op.drop_column('job', 'account_id')

    op.drop_table('seed_domain')
    op.drop_table('model_seed')
    op.drop_table('model')
