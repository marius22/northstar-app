"""add company dispute

Revision ID: 4b0cd942a69e
Revises: 4c25fa7a3b08
Create Date: 2016-04-18 11:36:26.095658

"""

# revision identifiers, used by Alembic.
revision = '4b0cd942a69e'
down_revision = '4c25fa7a3b08'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('company_dispute',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('segment_id', sa.BigInteger(), nullable=False),
        sa.Column('company_id', sa.Integer(), nullable=False),
        sa.Column('field_name', sa.String(length=255), nullable=False),
        sa.ForeignKeyConstraint(['segment_id'], ['segment.id'], ),
        sa.Index('idx_segment_id_company_id', 'segment_id', 'company_id'),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('company_dispute')
