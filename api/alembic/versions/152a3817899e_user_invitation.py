"""user invitation

Revision ID: 152a3817899e
Revises: bf26671a599
Create Date: 2016-04-04 22:41:34.352375

"""

# revision identifiers, used by Alembic.
revision = '152a3817899e'
down_revision = 'bf26671a599'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_table('user_invitation',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('account_id', sa.BigInteger(), nullable=False),
        sa.Column('email', sa.String(length=255), nullable=False),
        sa.Column('first_name', sa.String(length=255), nullable=True),
        sa.Column('last_name', sa.String(length=255), nullable=True),
        sa.Column('status', sa.String(length=255), server_default='PENDING', nullable=False),
        sa.ForeignKeyConstraint(['account_id'], ['account.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.add_column(u'user', sa.Column('status', sa.Enum('DELETED', 'INACTIVE', 'ACTIVE'), nullable=False,
                                     server_default='INACTIVE'))
    op.drop_column(u'user', 'activated')


def downgrade():
    op.add_column(u'user', sa.Column('activated', mysql.TINYINT(display_width=1), autoincrement=False, nullable=False))
    op.drop_column(u'user', 'status')
    op.drop_table('user_invitation')
