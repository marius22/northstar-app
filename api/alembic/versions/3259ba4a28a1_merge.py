"""merge

Revision ID: 3259ba4a28a1
Revises: 35c9e3d0c9dc, 41bee2195cdf
Create Date: 2016-09-09 10:51:49.918847

"""

# revision identifiers, used by Alembic.
revision = '3259ba4a28a1'
down_revision = ('35c9e3d0c9dc', '41bee2195cdf')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
