"""add default value for model.fitscore

Revision ID: 348e3b4e0a10
Revises: 348caa42ca2b
Create Date: 2016-09-13 14:14:20.535604

"""

# revision identifiers, used by Alembic.
from alembic.ddl import mysql

revision = '348e3b4e0a10'
down_revision = '348caa42ca2b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("update company_insight set company_column_name = 'model_fitscore.score' where column_name = 'score'")
    op.drop_column('account_quota', 'inbound_leads_target')
    op.execute("insert into company_insight(column_name, display_name, company_column_name)"
               " values ('', 'companyPhone', 'company.phone')")
    op.execute("insert into company_insight(column_name, display_name, company_column_name)"
               " values ('', 'street', 'company.street')")


def downgrade():
    op.execute("update company_insight set company_column_name = '' where column_name = 'score'")

    op.add_column('account_quota', sa.Column('inbound_leads_target', sa.String(length=20),
                                             server_default="accounts", nullable=False))
    op.execute("delete from company_insight where display_name = 'companyPhone'")
    op.execute("delete from company_insight where display_name = 'street'")
