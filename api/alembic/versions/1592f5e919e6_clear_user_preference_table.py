"""clear user preference table

Revision ID: 1592f5e919e6
Revises: 7d5e674746
Create Date: 2016-11-07 11:37:36.534357

"""

# revision identifiers, used by Alembic.
revision = '1592f5e919e6'
down_revision = '7d5e674746'
branch_labels = None
depends_on = None

from alembic import op


def upgrade():
    op.execute("CREATE TABLE user_preference_backup LIKE user_preference")
    op.execute("INSERT user_preference_backup SELECT * FROM user_preference")
    op.execute("Delete from user_preference")


def downgrade():
    try:
        op.execute("INSERT user_preference SELECT * FROM user_preference_backup")
        op.drop_table('user_preference_backup')
    except:
        print "user_preference_backup have been deleted manually"


