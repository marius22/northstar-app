"""delete first_name last_name from invitation

Revision ID: 38ff7deca944
Revises: 152a3817899e
Create Date: 2016-04-05 15:07:59.127374

"""

# revision identifiers, used by Alembic.
revision = '38ff7deca944'
down_revision = '152a3817899e'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():

    op.add_column('account', sa.Column('deleted', sa.Boolean(), nullable=False))
    op.drop_column('user_invitation', 'first_name')
    op.drop_column('user_invitation', 'last_name')


def downgrade():
    op.add_column('user_invitation', sa.Column('last_name', mysql.VARCHAR(length=255), nullable=True))
    op.add_column('user_invitation', sa.Column('first_name', mysql.VARCHAR(length=255), nullable=True))
    op.drop_column('account', 'deleted')
