"""merge 3f3bad34cdb4 573c197778dd

Revision ID: 1d2cc0603b64
Revises: 3f3bad34cdb4, 573c197778dd
Create Date: 2016-05-03 17:45:01.582021

"""

# revision identifiers, used by Alembic.
revision = '1d2cc0603b64'
down_revision = ('3f3bad34cdb4', '573c197778dd')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
