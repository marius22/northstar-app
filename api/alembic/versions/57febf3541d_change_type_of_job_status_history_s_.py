"""change type of job_status_history's status

Revision ID: 57febf3541d
Revises: 3cd5226c2ffd
Create Date: 2016-05-02 17:24:09.445626

"""

# revision identifiers, used by Alembic.
revision = '57febf3541d'
down_revision = '3cd5226c2ffd'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.alter_column('job_status_history', 'status', type_=sa.String(255),
                    existing_type=sa.Enum('JobCreated', 'JobSubmitionInQueued', 'JobSubmitionDeQueued', 'JobSubmitted',
                                          'JobRunningQueued', 'JobRunning', 'JobFinished', 'JobFailed', 'DataSyncStarted',
                                          'DataSyncFailed', 'DataSyncFinished'))


def downgrade():
    op.alter_column('job_status_history', 'status',
                    type_=sa.Enum('JobCreated', 'JobSubmitionInQueued', 'JobSubmitionDeQueued', 'JobSubmitted',
                                  'JobRunningQueued', 'JobRunning', 'JobFinished', 'JobFailed', 'DataSyncStarted',
                                  'DataSyncFailed', 'DataSyncFinished'),
                    existing_type=sa.String(255))
