"""add similar tech/similar deptments

Revision ID: 3e1d499a18cf
Revises: 7cb9d0501fb
Create Date: 2016-04-11 15:04:19.634745

"""

# revision identifiers, used by Alembic.
revision = '3e1d499a18cf'
down_revision = '7cb9d0501fb'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('recommendation', sa.Column('similar_departments', sa.String(length=4096), nullable=True))
    op.add_column('recommendation', sa.Column('similar_technologies', sa.String(length=4096), nullable=True))


def downgrade():
    op.drop_column('recommendation', 'similar_technologies')
    op.drop_column('recommendation', 'similar_departments')
