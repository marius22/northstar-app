"""add contact_export_flag to user

Revision ID: 1f0b4de99351
Revises: 3e0aefb240d7
Create Date: 2016-06-03 11:01:47.543524

"""

# revision identifiers, used by Alembic.
revision = '1f0b4de99351'
down_revision = '3e0aefb240d7'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('user', sa.Column('contact_export_flag', sa.Boolean(), nullable=False))


def downgrade():
    op.drop_column('user', 'contact_export_flag')
