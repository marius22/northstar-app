"""add init data for source table

Revision ID: 24235bfcb615
Revises: 3200e437a211
Create Date: 2016-03-11 16:48:47.936145

"""

# revision identifiers, used by Alembic.
revision = '24235bfcb615'
down_revision = '3200e437a211'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    conn = op.get_bind()
    conn.execute("insert into source(source_name) values('CSV')")


def downgrade():
    conn = op.get_bind()
    conn.execute("delete from source where source_name = 'CSV'")
