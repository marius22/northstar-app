"""column_modification

Revision ID: 557eecc27dbe
Revises: 1e8d69cc49b
Create Date: 2016-03-21 09:33:59.771817

"""

# revision identifiers, used by Alembic.
revision = '557eecc27dbe'
down_revision = '1e8d69cc49b'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('user', sa.Column('phone', sa.String(length=255), nullable=True))
    op.add_column('user', sa.Column('salt', sa.String(length=255), nullable=False, server_default=""))

    op.alter_column('user', 'first_name',
               existing_type=mysql.VARCHAR(length=255),
               nullable=True)
    op.alter_column('user', 'last_name',
               existing_type=mysql.VARCHAR(length=255),
               nullable=True)


def downgrade():
    op.alter_column('user', 'last_name',
               existing_type=mysql.VARCHAR(length=255),
               nullable=False)
    op.alter_column('user', 'first_name',
               existing_type=mysql.VARCHAR(length=255),
               nullable=False)
    op.drop_column('user', 'phone')
    op.drop_column('user', 'salt')
