"""merge

Revision ID: 18d7887d10e0
Revises: 598c52932f44, d9ec29a4518
Create Date: 2016-07-21 16:45:17.266424

"""

# revision identifiers, used by Alembic.
revision = '18d7887d10e0'
down_revision = ('598c52932f44', 'd9ec29a4518')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
