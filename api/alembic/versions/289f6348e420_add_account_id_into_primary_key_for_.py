"""add account id into primary key for field mapping table

Revision ID: 289f6348e420
Revises: 2b8db8c3991b
Create Date: 2017-03-20 20:28:48.099887

"""

# revision identifiers, used by Alembic.
revision = '289f6348e420'
down_revision = '2b8db8c3991b'
branch_labels = None
depends_on = None

import datetime
from alembic import op
import sqlalchemy as sa

table_name = "publish_field_mapping"


def upgrade():

    now_str = datetime.datetime.now().strftime("%y_%m_%d_%H_%M")
    sql_create_table = ("CREATE TABLE {table_name}"+"_" + now_str + " LIKE {table_name};").format(table_name=table_name)
    sql_backup = ("INSERT {table_name}"+"_" + now_str + " SELECT * FROM {table_name};").format(table_name=table_name)

    op.execute(sql_create_table)
    op.execute(sql_backup)

    op.execute("""delete from publish_field_mapping
        where model_id in
        (select id from model where is_deleted=1 and type = 'fit')
        """)
    op.execute("ALTER TABLE {table_name} DROP PRIMARY KEY;".format(table_name=table_name))
    op.execute("""ALTER TABLE {table_name} ADD PRIMARY KEY (account_id,
               target_type, target_object, target_org_id, target_field);""".format(table_name=table_name))


def downgrade():
    op.execute("ALTER TABLE {table_name} DROP PRIMARY KEY;".format(table_name=table_name))
    op.execute("""ALTER TABLE {table_name} ADD PRIMARY KEY
               (target_type, target_object, target_org_id, target_field);""".format(table_name=table_name))
