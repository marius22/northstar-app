"""update account's real time object value

Revision ID: 211aacb59327
Revises: 8f42aa4d224
Create Date: 2016-09-29 11:41:16.417584

"""

# revision identifiers, used by Alembic.
revision = '211aacb59327'
down_revision = '8f42aa4d224'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("update account set real_time_scoring_object = 3")


def downgrade():
    op.execute("update account set real_time_scoring_object = 0")
