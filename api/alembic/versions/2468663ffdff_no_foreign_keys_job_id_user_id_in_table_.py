"""no foreign keys(job_id, user_id) in table recommendation

Revision ID: 2468663ffdff
Revises: 4e3a10a78d71
Create Date: 2016-04-07 18:40:52.602005

"""

# revision identifiers, used by Alembic.
revision = '2468663ffdff'
down_revision = '4e3a10a78d71'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("ALTER TABLE recommendation DROP FOREIGN KEY recommendation_ibfk_1")
    op.execute("ALTER TABLE recommendation DROP FOREIGN KEY recommendation_ibfk_2")


def downgrade():
    op.execute(
        "ALTER TABLE recommendation add CONSTRAINT recommendation_ibfk_1 FOREIGN KEY (job_id) REFERENCES job(id)")
    op.execute(
        "ALTER TABLE recommendation add CONSTRAINT recommendation_ibfk_2 FOREIGN KEY (user_id) REFERENCES user(id)")
