"""add_user_register_log

Revision ID: 470dcdd18982
Revises: 1e11cd9a45ba
Create Date: 2016-09-17 14:50:20.108088

"""

# revision identifiers, used by Alembic.
revision = '470dcdd18982'
down_revision = '1e11cd9a45ba'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_table('user_register_log',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
    sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
    sa.Column('first_name', sa.String(length=255), nullable=True),
    sa.Column('last_name', sa.String(length=255), nullable=True),
    sa.Column('email', sa.String(length=255), nullable=False),
    sa.Column('company', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('user_register_log')
