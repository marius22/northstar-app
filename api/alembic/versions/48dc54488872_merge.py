"""merge

Revision ID: 48dc54488872
Revises: 4301cb0a5543, 5892ea6aa350
Create Date: 2016-04-21 18:23:55.400909

"""

# revision identifiers, used by Alembic.
revision = '48dc54488872'
down_revision = ('4301cb0a5543', '5892ea6aa350')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
