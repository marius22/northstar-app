"""Re-add index on column job_id in recommendation table

Revision ID: 4c25fa7a3b08
Revises: 2eab39084de2
Create Date: 2016-04-15 14:11:35.739525

"""

# revision identifiers, used by Alembic.
revision = '4c25fa7a3b08'
down_revision = '2eab39084de2'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index("ix_recommendation_job_id", "recommendation", ["job_id"])


def downgrade():
    op.drop_index("ix_recommendation_job_id", "recommendation")
