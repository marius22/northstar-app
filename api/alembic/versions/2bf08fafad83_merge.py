"""merge

Revision ID: 2bf08fafad83
Revises: 154615da5b66, aa2847f064e
Create Date: 2016-04-28 18:06:20.338315

"""

# revision identifiers, used by Alembic.
revision = '2bf08fafad83'
down_revision = ('154615da5b66', 'aa2847f064e')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
