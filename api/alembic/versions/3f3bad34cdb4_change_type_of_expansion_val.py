"""change_type_of_expansion_val

Revision ID: 3f3bad34cdb4
Revises: 57febf3541d
Create Date: 2016-05-02 17:53:14.201078

"""

# revision identifiers, used by Alembic.
revision = '3f3bad34cdb4'
down_revision = '57febf3541d'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('expansion', 'expansion_val',
                    type_=sa.Text(),
                    existing_type=sa.String(8192))


def downgrade():
    op.alter_column('expansion', 'expansion_val',
                    type_=sa.String(8192),
                    existing_type=sa.Text())
