"""create api_tracking table

Revision ID: 1da83e77106a
Revises: 40bfd9019982
Create Date: 2017-02-14 13:27:12.775645

"""

# revision identifiers, used by Alembic.
revision = '1da83e77106a'
down_revision = '40bfd9019982'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'rest_api_tracking',
        sa.Column('id', sa.BigInteger(), primary_key=True),
        sa.Column('customer_id', sa.String(16)),
        sa.Column('url', sa.String(512)),
        sa.Column('path', sa.String(128)),
        sa.Column('ip_address', sa.String(128)),
        sa.Column('api_version', sa.String(16)),
        sa.Column('blueprint', sa.String(32)),
        sa.Column('request_method', sa.String(16)),
        sa.Column('request_timestamp', sa.TIMESTAMP()),
        sa.Column('request_size', sa.String(32)),
        sa.Column('request_args', sa.String(2048)),
        sa.Column('response_size', sa.String(32)),
        sa.Column('user_agent', sa.String(128)),
        sa.Column('response_status', sa.Integer()),
        sa.Column('authorized', sa.Boolean())
    )


def downgrade():
    op.drop_table('rest_api_tracking')
