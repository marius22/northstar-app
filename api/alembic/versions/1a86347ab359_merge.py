"""merge

Revision ID: 1a86347ab359
Revises: 32854768e416, 3e1d499a18cf, 43b099f759c4
Create Date: 2016-04-13 16:39:43.941420

"""

# revision identifiers, used by Alembic.
revision = '1a86347ab359'
down_revision = ('32854768e416', '3e1d499a18cf', '43b099f759c4')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
