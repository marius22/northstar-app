"""merge

Revision ID: 4da11967af7d
Revises: 46427316c0f1, 27ca472bd5c0
Create Date: 2016-07-12 14:13:28.776731

"""

# revision identifiers, used by Alembic.
revision = '4da11967af7d'
down_revision = ('46427316c0f1', '27ca472bd5c0')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
