"""alter audience table

Revision ID: 35d20ead480e
Revises: 599981715e41
Create Date: 2016-11-16 14:02:12.187548

"""

# revision identifiers, used by Alembic.
revision = '35d20ead480e'
down_revision = '599981715e41'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('audience', 'type', new_column_name='source_type',
                    existing_type=sa.Enum('found', 'enriched'),
                    nullable=False)
    op.add_column('audience', sa.Column('meta_type', sa.SMALLINT(), nullable=False, doc="test"))
    op.execute("update audience set meta_type = 100")


def downgrade():
    op.alter_column('audience', 'source_type', new_column_name='type',
                    existing_type=sa.Enum('found', 'enriched'), nullable=False)
    op.drop_column('audience', 'meta_type')
