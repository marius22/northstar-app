"""merge

Revision ID: 59e045782877
Revises: 1a1dd22e26c, 48dc54488872
Create Date: 2016-04-21 21:51:53.036899

"""

# revision identifiers, used by Alembic.
revision = '59e045782877'
down_revision = ('1a1dd22e26c', '48dc54488872')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
