"""add sfdc config

Revision ID: 339ac7e7165f
Revises: 24062aa00d8e
Create Date: 2016-06-22 15:00:22.448948

"""

# revision identifiers, used by Alembic.
revision = '339ac7e7165f'
down_revision = '24062aa00d8e'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.create_table('sfdc_config',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('account_id', sa.BigInteger(), nullable=False),
        sa.Column('config', sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(['account_id'], ['account.id'], ),
        sa.UniqueConstraint('account_id'),
        sa.PrimaryKeyConstraint('id'),
        mysql_charset='utf8',
        mysql_engine='InnoDB'
    )


def downgrade():
    op.drop_table('sfdc_config')
