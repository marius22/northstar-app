"""merge

Revision ID: 4883f9a18dfa
Revises: 44d464a111c2, 4862bfea958c
Create Date: 2016-08-29 16:50:39.581177

"""

# revision identifiers, used by Alembic.
revision = '4883f9a18dfa'
down_revision = ('44d464a111c2', '4862bfea958c')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
