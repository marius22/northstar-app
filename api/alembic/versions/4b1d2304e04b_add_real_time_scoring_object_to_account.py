"""add real_time_scoring_object to account

Revision ID: 4b1d2304e04b
Revises: 4883f9a18dfa
Create Date: 2016-08-30 16:55:35.310002

"""

# revision identifiers, used by Alembic.
revision = '4b1d2304e04b'
down_revision = '4883f9a18dfa'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('account', sa.Column('real_time_scoring_object', sa.SmallInteger(), server_default=sa.schema.DefaultClause("0"), nullable=True))


def downgrade():
    op.drop_column('account', 'real_time_scoring_object')