"""alter account_quota table

Revision ID: 2b8db8c3991b
Revises: 1134dac6d8a7
Create Date: 2017-03-10 09:11:45.473035

"""

# revision identifiers, used by Alembic.
revision = '2b8db8c3991b'
down_revision = '1134dac6d8a7'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('account_quota', sa.Column('start_ts', sa.TIMESTAMP(), nullable=True))


def downgrade():
    op.drop_column('account_quota', 'start_ts')
