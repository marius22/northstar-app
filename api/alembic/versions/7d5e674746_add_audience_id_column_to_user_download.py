"""add audience_id column to user_download

Revision ID: 7d5e674746
Revises: 16a7674f1b7
Create Date: 2016-10-14 16:11:28.207754

"""

# revision identifiers, used by Alembic.
revision = '7d5e674746'
down_revision = '16a7674f1b7'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('user_download', sa.Column('audience_id', sa.BigInteger(), server_default='0', nullable=True))
    op.drop_constraint(u'user_download_ibfk_2', 'user_download', type_='foreignkey')
    

def downgrade():
    op.create_foreign_key('user_download_ibfk_2', 'user_download', 'segment', ['segment_id'], ['id'])
    op.drop_column('user_download', 'audience_id')
