"""create count of recommend company table

Revision ID: 426884791e6d
Revises: 38ff7deca944
Create Date: 2016-04-06 14:09:57.944095

"""

# revision identifiers, used by Alembic.
revision = '426884791e6d'
down_revision = '38ff7deca944'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('company_recommendation_count',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('created_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('updated_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('company_id', sa.BigInteger(), nullable=False),
        sa.Column('count', sa.BigInteger(), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )

    op.create_index(op.f('ix_company_recommendation_count_company_id'), 'company_recommendation_count', ['company_id'], unique=False)

def downgrade():
    op.drop_index(op.f('ix_company_recommendation_count_company_id'), table_name='company_recommendation_count')
    op.drop_table('company_recommendation_count')
