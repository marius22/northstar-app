"""merge

Revision ID: 55b3e457b75e
Revises: 3259ba4a28a1, 5e33ab81ca9
Create Date: 2016-09-10 12:25:35.339677

"""

# revision identifiers, used by Alembic.
revision = '55b3e457b75e'
down_revision = ('3259ba4a28a1', '5e33ab81ca9')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
