"""create account_quota_ledger table

Revision ID: 1134dac6d8a7
Revises: 26ec4ee96a43
Create Date: 2017-03-10 09:10:24.645727

"""

# revision identifiers, used by Alembic.
revision = '1134dac6d8a7'
down_revision = '26ec4ee96a43'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'account_quota_ledger',
        sa.Column('id', sa.BigInteger(), nullable=False),
        sa.Column('account_id', sa.BigInteger(), nullable=False),
        sa.Column('user_id', sa.BigInteger(), nullable=False),
        sa.Column('operation_ts', sa.TIMESTAMP(), nullable=True),
        sa.Column('value', sa.Integer(), nullable=True),
        sa.Column('op_code', sa.Enum('ADJ', 'CHG_LIMIT', name='op_code'), nullable=False),
        sa.Column('description', sa.String(4096), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.ForeignKeyConstraint(['account_id'], ['account.id']),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'])
    )


def downgrade():
    op.drop_table('account_quota_ledger')
