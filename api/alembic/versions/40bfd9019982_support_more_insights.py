"""support more insights

Revision ID: 40bfd9019982
Revises: 4fc0560abeb0
Create Date: 2017-01-18 10:49:44.066951

"""

# revision identifiers, used by Alembic.
revision = '40bfd9019982'
down_revision = '4fc0560abeb0'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa

new_insights = ["audienceNames",
                "keywords",
                "marketingSophistication",
                "businessToBusiness",
                "businessToConsumer",
                "facilitiesInMultipleLocations",
                "employeesInMultipleLocations"
                ]


def upgrade():
    for insight in new_insights:
        op.execute("insert into company_insight"
                   "(created_ts, updated_ts, column_name, display_name, company_column_name)"
                   " values (now(), now(), '', '{insight_name}', '')".
                   format(insight_name=insight))

    op.execute("insert into company_insight (created_ts, updated_ts, column_name, display_name, company_column_name)"
               " values (now(), now(), '', 'facebookUrl', 'company.facebook_url')")
    op.execute("insert into company_insight (created_ts, updated_ts, column_name, display_name, company_column_name)"
               " values (now(), now(), '', 'twitterUrl', 'company.twitter_url')")
    op.execute("update company_insight set company_column_name='company.website_traffic_rank_monthly' "
               "where display_name='alexaRank'")


def downgrade():
    for insight in new_insights + ["twitterUrl", "facebookUrl"]:
        op.execute("delete from company_insight where display_name = '{insight_name}'".format(insight_name=insight))
