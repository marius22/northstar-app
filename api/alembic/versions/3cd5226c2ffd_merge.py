"""merge

Revision ID: 3cd5226c2ffd
Revises: 1fe89e39d2de, 2bf08fafad83, 45886dcddb82
Create Date: 2016-04-29 18:14:28.899202

"""

# revision identifiers, used by Alembic.
revision = '3cd5226c2ffd'
down_revision = ('1fe89e39d2de', '2bf08fafad83', '45886dcddb82')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
