"""add audience company number update time

Revision ID: 38f4802f4c11
Revises: 35d20ead480e
Create Date: 2016-11-27 20:29:23.423439

"""

# revision identifiers, used by Alembic.
revision = '38f4802f4c11'
down_revision = '35d20ead480e'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('audience', sa.Column('company_number_refresh_ts', sa.TIMESTAMP(), nullable=True))
    op.execute("update audience set company_number_refresh_ts = created_ts")


def downgrade():
    op.drop_column('audience', 'company_number_refresh_ts')
