"""merge multi head

Revision ID: 7cb9d0501fb
Revises: 14b1f6da3676, 46beb3e82030
Create Date: 2016-04-11 11:44:43.664240

"""

# revision identifiers, used by Alembic.
revision = '7cb9d0501fb'
down_revision = ('14b1f6da3676', '46beb3e82030')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
