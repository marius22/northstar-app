"""add departmentSize and growthIndicator to seedCandidate

Revision ID: 2b4fe5fd2bf7
Revises: 4301cb0a5543
Create Date: 2016-04-20 15:44:17.942050

"""

# revision identifiers, used by Alembic.
revision = '2b4fe5fd2bf7'
down_revision = '4301cb0a5543'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('seed_candidate', sa.Column('department_size', sa.String(length=4096),
                                              nullable=True, server_default="[]"))
    op.add_column('seed_candidate', sa.Column('growth', sa.String(length=4096),
                                              nullable=True, server_default="[]"))
    op.alter_column('seed_candidate', 'tech', type_=sa.String(4096), existing_type=sa.String(255),
                    server_default="[]")


def downgrade():
    op.drop_column('seed_candidate', 'growth')
    op.drop_column('seed_candidate', 'department_size')
    op.alter_column('seed_candidate', 'tech', existing_type=sa.String(4096), type_=sa.String(255))
