"""modify default value for account_real_time_scoring

Revision ID: 3884ff21ed0
Revises: 521775a33f6f
Create Date: 2016-09-20 11:12:08.806430

"""

# revision identifiers, used by Alembic.
revision = '3884ff21ed0'
down_revision = '521775a33f6f'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from sqlalchemy.dialects.mysql import LONGTEXT


def upgrade():
    op.alter_column('account', 'real_time_scoring_object', server_default='3')


def downgrade():
    op.alter_column('account', 'real_time_scoring_object', server_default='0')

