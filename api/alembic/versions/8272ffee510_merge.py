"""merge

Revision ID: 8272ffee510
Revises: a2ac1801762, 53e146d93e6d, 1c4966ac771e
Create Date: 2016-03-23 12:30:15.785192

"""

# revision identifiers, used by Alembic.
revision = '8272ffee510'
down_revision = ('a2ac1801762', '53e146d93e6d', '1c4966ac771e')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
