"""empty message

Revision ID: 185d1abd865b
Revises: 1fff992f3724, 8272ffee510
Create Date: 2016-03-23 13:05:49.923938

"""

# revision identifiers, used by Alembic.
revision = '185d1abd865b'
down_revision = ('1fff992f3724', '8272ffee510')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
