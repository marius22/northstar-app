"""add first_name, last_name .. to user_invitation

Revision ID: 4b77e4462a46
Revises: 4301cb0a5543
Create Date: 2016-04-20 11:36:51.410183

"""

# revision identifiers, used by Alembic.
revision = '4b77e4462a46'
down_revision = '4301cb0a5543'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def upgrade():
    op.add_column('user_invitation', sa.Column('first_name', sa.String(length=255), nullable=True))
    op.add_column('user_invitation', sa.Column('last_name', sa.String(length=255), nullable=True))
    op.add_column('user_invitation', sa.Column('phone', sa.String(length=255), nullable=True))
    op.add_column('user_invitation', sa.Column('user_id', sa.BigInteger(), nullable=False))
    op.create_foreign_key(None, 'user_invitation', 'user', ['user_id'], ['id'])


def downgrade():
    op.drop_constraint(None, 'user_invitation', type_='foreignkey')
    op.drop_column('user_invitation', 'user_id')
    op.drop_column('user_invitation', 'phone')
    op.drop_column('user_invitation', 'last_name')
    op.drop_column('user_invitation', 'first_name')
