"""alter table segment_metric update column value to longtext

Revision ID: 5866589234ad
Revises: 223f127ca331
Create Date: 2016-07-21 10:51:05.489915

"""

# revision identifiers, used by Alembic.
revision = '5866589234ad'
down_revision = '223f127ca331'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.mysql import LONGTEXT


def upgrade():
    op.alter_column('segment_metric', 'value',
        existing_type=sa.Text(),
        type_=LONGTEXT)


def downgrade():
    op.alter_column('segment_metric', 'value',
        existing_type=LONGTEXT,
        type_=sa.Text())
