"""merge

Revision ID: 515f5a29292d
Revises: 4b1d2304e04b, 589e04e010d9
Create Date: 2016-09-02 10:19:27.452835

"""

# revision identifiers, used by Alembic.
revision = '515f5a29292d'
down_revision = ('4b1d2304e04b', '589e04e010d9')
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
