# Database Schema Version Control

## Tool
[Alembic](alembic.readthedocs.org): a lightweight database migration tool for usage with the SQLAlchemy Database Toolkit for Python.

## Upgrade to the Latest Version

    $ cd api
    $ alembic upgrade head

## Auto Generate Migration
[Documentation](http://alembic.readthedocs.org/en/latest/autogenerate.html)

    $ cd api
    $ alembic revision --autogenerate -m "Added account table"

    # Review the file and adjust it if necessary
    $ vim alembic/versions/$(generated_version_file)
    
If new model is added, you need to inform `env.py` with the metadata of the model by appending the model in it's `target_metadata` variable. Check [here](http://alembic.readthedocs.org/en/latest/autogenerate.html#auto-generating-migrations) for more information.

## Manually Generate Migration

    $ cd api
    $ alembic revision -m "Change account column names"