import json
import unittest
from mock import patch
import httpretty
from six import StringIO, BytesIO
import unicodecsv
from http_client_base import AppHttpClientBase
import re
from datetime import datetime
from mixer.backend.flask import mixer
from ns.model.seed_company import SeedCompany
from ns.model.model import Model
from ns.controller.app import app
from ns.model.account import Account
from ns.model.user import User
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.util.company_db_client import CompanyDBClient


def generate_csv(num_lines):
    f = BytesIO()

    w = unicodecsv.writer(f, encoding='utf-8')
    w.writerow(("companyName_1", "www.domain_1.com"))
    # write some duplicate records
    w.writerow(("companyName_2", "www.domain_2.com"))
    w.writerow(("companyName_3", "www.domain_2.com"))
    w.writerow(("companyName_4", "https://www.domain_3.com"))
    w.writerow(("companyName_5", ""))
    w.writerow(("companyName_6", "domain5.com"))

    # Rewind
    f.seek(0)
    return f


def _get_now():
    return datetime.utcnow()


class TestModelApp(AppHttpClientBase):

    def setUp(self):
        super(TestModelApp, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @patch('ns.util.queue_service_client.QueueServiceClient.submit_account_model')
    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @httpretty.activate
    def test_create_model_by_csv(self, mock_insert, mock_client, mock_fetch):
        mock_insert.side_effect = None
        mock_client.side_effect = None
        mock_fetch.side_effect = None
        with app.app_context():

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domains3.com", "name": "company_name_3"},
                                           {"domain": "domains4.com", "name": "company_name_4"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            account = mixer.blend(Account)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            content = generate_csv(10)

            file_payload = (content, 'My CSV.txt')

            resp = self.client.post('/accounts/1/fit_models/create_by_csv?modelName=testModelName',
                                    data={'file': file_payload})

            self.assertEqual(resp.status_code, 200)

            models = Model.get_by_account_id_and_type(1, Model.FIT_MODEL)
            self.assertEqual(len(models), 1)

            model = models[0]
            self.assertEqual(model.status, Model.IN_QUEUE)
            self.assertEqual(model.name, "testModelName")

    def test_get_models(self):
        with app.app_context():
            account = mixer.blend(Account, id=1)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='fit', updated_ts=_get_now(), status="IN_PROGRESS")
            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="IN_PROGRESS")
            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="IN_PROGRESS")
            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='galaxy', updated_ts=_get_now(), status="COMPLETED")

            resp = self.client.get("/accounts/1/fit_models")
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)

    def test_get_crm_fit_status(self):
        with app.app_context():
            account = mixer.blend(Account, id=1)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "NO_MODEL")

            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="IN_PROGRESS")
            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "IN_PROGRESS")

            mixer.blend(Model, account_id=1, created_ts=_get_now(),
                        type='galaxy', updated_ts=_get_now(), status="COMPLETED")
            resp1 = self.client.get("/accounts/1/crm_fit_models")
            data1 = json.loads(resp1.data).get("data")
            self.assertEqual(data1, "IN_PROGRESS")

            mixer.blend(Model, account_id=1, batch_id="456", created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="COMPLETED")
            mixer.blend(Model, account_id=1, batch_id="456", created_ts=_get_now(),
                        type='feature_analysis', updated_ts=_get_now(), status="IN_PROGRESS")
            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "IN_PROGRESS")

            mixer.blend(Model, account_id=1, batch_id="789", created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="COMPLETED")
            mixer.blend(Model, account_id=1, batch_id="789", created_ts=_get_now(),
                        type='feature_analysis', updated_ts=_get_now(), status="IN_PROGRESS")
            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "IN_PROGRESS")

            mixer.blend(Model, account_id=1, batch_id="1", created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="IN_PROGRESS")
            mixer.blend(Model, account_id=1, batch_id="1", created_ts=_get_now(),
                        type='feature_analysis', updated_ts=_get_now(), status="COMPLETED")
            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "IN_PROGRESS")

            mixer.blend(Model, account_id=1, batch_id="123", created_ts=_get_now(),
                        type='account_fit', updated_ts=_get_now(), status="COMPLETED")

            mixer.blend(Model, account_id=1, batch_id="123", created_ts=_get_now(),
                        type='feature_analysis', updated_ts=_get_now(), status="COMPLETED")

            resp = self.client.get("/accounts/1/crm_fit_models")
            data = json.loads(resp.data).get("data")
            self.assertEqual(data, "COMPLETED")
