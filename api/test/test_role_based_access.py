import httplib
import unittest
import json

from mixer.backend.flask import mixer
from mock import patch

from ns.controller.app import app
from ns.model import db
from ns.model.account import Account
from ns.model.role import Role
from ns.model.user import User
from ns.model.user_role import UserRole
from http_client_base import AppHttpClientBase


class TestRoleBasedAccess(AppHttpClientBase):

    def setUp(self):
        super(TestRoleBasedAccess, self).setUp()
        app.config['ROLE_ENABLED'] = True

    def tearDown(self):
        super(TestRoleBasedAccess, self).tearDown()
        app.config['ROLE_ENABLED'] = False

    def test_get_users_with_right_role(self):
        with app.app_context():

            acc = mixer.blend(Account, name='es_test', account_type=Account.ES)
            user = mixer.blend(User, account=acc, email='test@everstring.com', activated=True)
            role = mixer.blend(Role, name='ES_SUPER_ADMIN')
            mixer.blend(UserRole, user=user, role=role)

            url = '/users'

            resp = self.client.get(url)
            data = json.loads(resp.data)

            # Assert user counts
            # This might not contain all the users once the pagination is added.
            self.assertEqual(len(data['data']), 1)
            self.assertEqual(data['total'], 1)

    def test_get_users_with_wrong_role(self):
        with app.app_context():
            acc = mixer.blend(Account, name='es_test', account_type=Account.TRIAL)
            user = mixer.blend(User, account=acc, email='test@everstring.com', activated=True)
            role = mixer.blend(Role, name=Role.TRIAL_USER)
            mixer.blend(UserRole, user_id=user.id, role_id=role.id)

            url = '/users'
            res = self.client.get(url)
            self.assertEqual(res.status_code, httplib.FORBIDDEN)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_request_upgrade_with_right_role(self, mock_sender):
        mock_sender.side_effect = None
        with app.app_context():
            acc = mixer.blend(Account, name='es_test', account_type=Account.TRIAL)
            user = mixer.blend(User, account=acc, email='test@everstring.com', activated=True)
            role = mixer.blend(Role, name=Role.TRIAL_USER)
            mixer.blend(UserRole, user=user, role=role)

            url = '/users/request_upgrade'
            data = {'firstName': 'Bo', 'lastName': 'Xia', 'phone': '123456789'}
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertEqual(res.status_code, httplib.OK)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_request_upgrade_with_wrong_role(self, mock_sender):
        mock_sender.side_effect = None
        with app.app_context():
            acc = mixer.blend(Account, name='es_test', account_type=Account.ES)
            user = mixer.blend(User, account=acc, email='test@everstring.com', activated=True)
            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user_id=user.id, role_id=role.id)

            url = '/users/request_upgrade'
            data = {'first_name': 'Bo', 'last_name': 'Xia', 'phone': '123456789'}
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertEqual(res.status_code, httplib.FORBIDDEN)
