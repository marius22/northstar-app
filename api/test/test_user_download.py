import json
from ns.controller.service import app
from http_client_base import ServiceHttpClientBase


class TestUserDownload(ServiceHttpClientBase):
    def setUp(self):
        super(TestUserDownload, self).setUp()

    def test_bad_request(self):
        with app.app_context():
            url = '/user_download/csv_track'
        res = self.client.post(url, data=json.dumps({}), content_type='application/json')
        self.assertEqual(res.status_code, 422)

    def test_track_csv_file(self):
        with app.app_context():
            url = '/user_download/csv_track'
            post_data = {'user_id': '1', 'audience_id': '1',
                         'company_csv_s3_key': 'company_key',
                         'contact_csv_s3_key': 'contact_key'}

            res = self.client.post(url, data=json.dumps(post_data), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            
            post_data = {'audience_id': '1',
                         'company_csv_s3_key': 'company_key',
                         'contact_csv_s3_key': 'contact_key'}
            res = self.client.post(url, data=json.dumps(post_data), content_type='application/json')
            self.assertEqual(res.status_code, 422)
