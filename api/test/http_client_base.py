from base_case import AppTestBase
from base_case import ServiceTestBase
from ns.controller.app import app as app_app
from ns.controller.service import app as service_app


class AppHttpClientBase(AppTestBase):
    
    def setUp(self):
        super(AppHttpClientBase, self).setUp()
        self.client = app_app.test_client()


class ServiceHttpClientBase(ServiceTestBase):

    def setUp(self):
        super(ServiceHttpClientBase, self).setUp()
        self.client = service_app.test_client()
