import httplib
import json
import datetime
from mock import patch, mock
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model import db
from ns.model.job import Job
from ns.model.job_status_history import JobStatusHistory
from ns.model.segment import Segment
from http_client_base import ServiceHttpClientBase
from ns.model.account import Account


class TestJobStatusService(ServiceHttpClientBase):

    def setUp(self):
        super(TestJobStatusService, self).setUp()
        with app.app_context():
            segment = mixer.blend(Segment)
            self.job = mixer.blend(Job, segment_id=segment.id)

    def test_create_job_status_empty(self):
        with app.app_context():
            # case1: have no segment 'status' in post data
            data = {}
            url = '/jobs/%d/status' % self.job.id
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertEqual(res.status_code, httplib.BAD_REQUEST)

    def test_create_job_status_for_not_existed_job(self):
        with app.app_context():
            # case2: job_id not existed
            data = {'status': JobStatusHistory.STATUS_FINISHED}
            url = '/jobs/%d/status' % -1
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertEqual(res.status_code, httplib.NOT_FOUND)

    @patch('ns.controller.service.job_status.send_summary_email')
    def test_create_job_status_ok(self, mock_send_email):
        with app.app_context():

            mock_send_email.side_effect = None

            # case3: normal flow
            data = {'status': JobStatusHistory.SEGMENT_FINISHED}
            url = '/jobs/%d/status' % self.job.id
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertDictEqual({'status': 'OK'}, json.loads(res.data))
            mock_send_email.assert_called_once_with(mock.ANY)

            mock_send_email.reset_mock()

            account = mixer.blend(Account)
            job = mixer.blend(Job, account_id=account.id, segment_id=1)

            res = self.client.post('/jobs/%d/status' % job.id, data=json.dumps(data), content_type='application/json')
            self.assertDictEqual({'status': 'OK'}, json.loads(res.data))
            mock_send_email.assert_called_once_with(mock.ANY)
            self.assertEqual(job.status, Job.COMPLETED)

    def test_get_jobs(self):
        with app.app_context():
            segment = mixer.blend(Segment,
                                  is_deleted=False)
            mixer.blend(Job, segment_id=segment.id, status='COMPLETED', type=Job.SEGMENT)
            mixer.blend(Job, segment_id=segment.id, status='COMPLETED', type=Job.SEGMENT)
            mixer.blend(Job,
                        segment_id=segment.id, status='IN PROGRESS',
                        type=Job.SEGMENT, created_ts=datetime.datetime.utcnow() - datetime.timedelta(seconds=1000))
            mixer.blend(Job,
                        segment_id=segment.id, status='IN PROGRESS',
                        type=Job.SEGMENT, created_ts=datetime.datetime.utcnow() - datetime.timedelta(seconds=3601))
            mixer.blend(Job,
                        segment_id=segment.id, status='IN PROGRESS',
                        type=Job.SEGMENT, created_ts=datetime.datetime.utcnow() - datetime.timedelta(seconds=3601))
            mixer.blend(Job,
                        segment_id=segment.id, status='IN PROGRESS',
                        type=Job.SEGMENT, created_ts=datetime.datetime.utcnow() - datetime.timedelta(seconds=3601))

            url = '/jobs'
            query_str = {'elapse': 3600, 'status': 'IN PROGRESS'}
            res = self.client.get(url, query_string=query_str)
            self.assertEqual(3, len(json.loads(res.data)['data']))
