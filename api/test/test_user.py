import unittest
import datetime
from mock import patch, PropertyMock
from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.account import Account
from ns.model.user import User
from ns.util.string_util import is_blank
from ns.model.role import Role
from base_case import AppTestBase
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from ns.model.crm_token import CRMToken
from ns.model.model import Model
from ns.model.publish_field_mapping import PublishFieldMapping, PublishFieldMappingStatus
import json


class TestUser(AppTestBase):

    def setUp(self):
        super(TestUser, self).setUp()
        with app.app_context():
            self.client = app.test_client()

    def create_account(self, email):
        account_name = Account.generate_name_by_email(email)
        account = Account.create_account(
            account_name,
            Account.TRIAL
        )
        return account

    def test_create_and_get(self):

        with app.app_context():

            test_email = "test_email@everstring.com"
            test_password = "test_password123"
            account = self.create_account(test_email)

            user = User.create_user(account.id, test_email, test_password)

            self.assertIsNotNone(user)
            self.assertIsNotNone(user.id)
            self.assertEquals(user.email, test_email)

            db_user = User.get_user_by_email(test_email)

            self.assertIsNotNone(db_user)
            self.assertEquals(user.email, test_email)

    def test_password_management(self):

        with app.app_context():

            test_email = "test_email@everstring.com"
            test_password_1 = "test_password_1"

            account = self.create_account(test_email)
            user = User.create_user(account.id, test_email, test_password_1)

            self.assertTrue(user.check_password(test_password_1))

            test_password_2 = "test_password_2"
            user.change_password(test_password_1, test_password_2)

            self.assertTrue(user.check_password(test_password_2))

            test_password_3 = "test_password_3"
            user.reset_password(test_password_3)

            self.assertTrue(user.check_password(test_password_3))

    def test_update_name_and_phone(self):
        with app.app_context():
            test_email = "test_email@everstring.com"
            test_password = "test_password_1"

            account = self.create_account(test_email)
            User.create_user(account.id, test_email, test_password)

            user = User.get_user_by_email(test_email)
            self.assertTrue(is_blank(user.first_name))
            self.assertTrue(is_blank(user.last_name))
            self.assertTrue(is_blank(user.phone))

            user.update_name_and_phone("first_name", "last_name", "phone")

            user = User.get_user_by_email(test_email)
            self.assertIsNotNone(user.first_name)
            self.assertIsNotNone(user.last_name)
            self.assertIsNotNone(user.phone)

    @patch('ns.model.user.User.role_names', new_callable=PropertyMock)
    def test_user_can_grant_role(self, mock_property):
        with app.app_context():
            mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(Role, name=Role.TRIAL_USER)

            user = User()
            mock_property.return_value = []
            self.assertFalse(user.can_grant_role([]))

            mock_property.return_value = [Role.TRIAL_USER, Role.ACCOUNT_USER]
            self.assertFalse(user.can_grant_role([Role.ACCOUNT_ADMIN]))
            self.assertTrue(user.can_grant_role([Role.ACCOUNT_USER]))

            mock_property.return_value = [Role.ES_ADMIN, Role.TRIAL_USER, Role.ACCOUNT_USER]
            self.assertFalse(user.can_grant_role([Role.ES_SUPER_ADMIN]))
            self.assertTrue(user.can_grant_role([Role.ES_ADMIN]))
            self.assertTrue(user.can_grant_role([Role.ACCOUNT_USER]))

    def test_user_me(self):
        with app.app_context():
            account = mixer.blend(Account)
            user = mixer.blend(
                User,
                account=account,
                email="test@everstring.com")

            mixer.blend(
                AccountQuota,
                account=account,
                exposed_csv_insight_ids='[]',
                exposed_ui_insight_ids='[]',
                real_time_scoring_insight_ids='[]'
            )

            # created before TIME_TO_ALLOW_USE_PREVIOUS_VERSION
            app.config["TIME_TO_ALLOW_USE_PREVIOUS_VERSION"] = \
                datetime.datetime.utcnow() + datetime.timedelta(days=1)
            resp = self.client.get("/users/me")

            self.assertEqual(resp.status_code, 200)
            result = json.loads(resp.data).get("data")
            self.assertEqual(result["canUsePreviousVersion"], True)

            # created after TIME_TO_ALLOW_USE_PREVIOUS_VERSION
            app.config["TIME_TO_ALLOW_USE_PREVIOUS_VERSION"] = \
                datetime.datetime.utcnow() + datetime.timedelta(days=-1)
            resp = self.client.get("/users/me")

            self.assertEqual(resp.status_code, 200)
            result = json.loads(resp.data).get("data")
            self.assertEqual(result["canUsePreviousVersion"], False)

    @patch('ns.model.publish_field_mapping.PublishFieldMapping.get_target_available_fields')
    @patch('ns.model.account_quota.AccountQuota.get_by_account_id')
    def test_publish_field_mapping(self, account_quota_mock, get_salesforce_fields):
        one_mapping = ["Model Fit Score", "Model Fit Score 2", "ES Revenue",  "ES Emp Size"]
        get_salesforce_fields.side_effect = [one_mapping, one_mapping, one_mapping, one_mapping,
                                             one_mapping, one_mapping, one_mapping]
        with app.app_context():
            account = mixer.blend(Account)
            revenue_insight = mixer.blend(CompanyInsight, display_name="revenue")
            emp_size_insight = mixer.blend(CompanyInsight, display_name="employeeSize")
            model = mixer.blend(Model, account_id=account.id, name="My Model", type=Model.FIT_MODEL)
            mixer.blend(CRMToken, account_id=account.id, organization_id="123456")

            class Obj:
                allowed_insight_ids = [revenue_insight.id]
                scoring_enabled = 1
            account_quota_mock.return_value = Obj

            # create user to initialize g.user
            mixer.blend(
                User,
                account=account,
                email="test@everstring.com")

            base_url = "/account/%d/field/mapping" % account.id

            # create a new field mapping
            payload1 = {
                "targetType": "salesforce",
                "targetObject": "account",
                "targetFields": [{"targetField": "Model Fit Score", "modelId": model.id}]
            }
            resp = self.client.post(base_url, data=json.dumps(payload1), content_type="application/json")
            result = json.loads(resp.data).get('data')
            # check that there exists one mapping
            self.assertEqual(len(result), 1)
            mapping = result[0]
            self.assertEqual(mapping.get("modelId"), model.id)
            self.assertEqual(mapping.get("targetField"), "Model Fit Score")

            # update and create a field mapping
            # Note: employeeSize will be created but can't be fetched in GET request because
            # it is not in the user's account quota
            payload2 = {
                "targetType": "salesforce",
                "targetObject": "account",
                "targetFields": [
                    {"targetField": "Model Fit Score 2", "modelId": model.id},
                    {"targetField": "ES Revenue", "insightId": revenue_insight.id},
                    {"targetField": "ES Emp Size", "insightId": emp_size_insight.id}
                ]
            }
            resp = self.client.post(base_url, data=json.dumps(payload2), content_type="application/json")
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 2)
            # check get method also returns 2 mappings
            resp = self.client.get("%s?targetType=%s&targetObject=%s" % (base_url, "salesforce", "account"))
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 2)

            # disable scoring, should remove model field and only return insight field
            Obj.scoring_enabled = 0
            resp = self.client.get("%s?targetType=%s&targetObject=%s" % (base_url, "salesforce", "account"))
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 1)
            Obj.scoring_enabled = 1

            # delete model fit score field with wrong name
            resp = self.client.delete(base_url, data=json.dumps(payload1), content_type="application/json")
            result = json.loads(resp.data).get('data')
            # should still have 2 mappings
            self.assertEqual(len(result), 2)

            # delete model fit score field
            payload3 = {
                "targetType": "salesforce",
                "targetObject": "account",
                "targetFields": [{"targetField": "Model Fit Score 2", "modelId": model.id}]
            }
            resp = self.client.delete(base_url, data=json.dumps(payload3), content_type="application/json")
            result = json.loads(resp.data).get('data')
            # only one field returned
            self.assertEqual(len(result), 1)
            # check get method also returns 1 mapping
            resp = self.client.get("%s?targetType=%s&targetObject=%s" % (base_url, "salesforce", "account"))
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 1)

            #########################
            ## test error requests ##
            #########################
            # 1. invalid model id or insight id
            payload = {
                "targetType": "salesforce",
                "targetObject": "account",
                "targetFields": []
            }
            payload["targetFields"] = [{
                "targetField": "Model Fit Score 2", "modelId": model.id + 1}]
            resp = self.client.post(base_url, data=json.dumps(payload), content_type="application/json")
            self.assertEqual(resp.status_code, 400)
            resp = self.client.delete(base_url, data=json.dumps(payload), content_type="application/json")
            self.assertEqual(resp.status_code, 400)
            payload["targetFields"] = [{
                "targetField": "ES Revenue", "insightId": revenue_insight.id + 100}]
            resp = self.client.post(base_url, data=json.dumps(payload), content_type="application/json")
            self.assertEqual(resp.status_code, 400)
            resp = self.client.delete(base_url, data=json.dumps(payload), content_type="application/json")
            self.assertEqual(resp.status_code, 400)

    @patch('ns.model.publish_field_mapping.PublishFieldMapping.get_target_available_fields')
    @patch('ns.model.account_quota.AccountQuota.get_by_account_id')
    def test_limit_one_account_fit_publish_field(self, account_quota_mock, get_salesforce_fields):
        one_mapping = ["Model Fit Score", "My Model"]
        get_salesforce_fields.side_effect = [one_mapping, one_mapping, one_mapping]
        with app.app_context():
            account = mixer.blend(Account)
            mixer.blend(Model, account_id=account.id, name="FA 1", type=Model.FEATURE_ANALYSIS,
                        status=Model.COMPLETED, is_deleted=False, batch_id=1)
            model_old = mixer.blend(Model, account_id=account.id, name="My Model", type=Model.ACCOUNT_FIT,
                                    status=Model.COMPLETED, is_deleted=False, batch_id=1)
            mixer.blend(Model, account_id=account.id, name="FA 2", type=Model.FEATURE_ANALYSIS,
                        status=Model.COMPLETED, is_deleted=False, batch_id=2)
            model_new = mixer.blend(Model, account_id=account.id, name="My New Model", type=Model.ACCOUNT_FIT,
                                    status=Model.COMPLETED, is_deleted=False, batch_id=2)
            org_id = "123456"
            mixer.blend(CRMToken, account_id=account.id, organization_id=org_id)

            class Obj:
                allowed_insight_ids = []
                scoring_enabled = 1
            account_quota_mock.return_value = Obj

            # create user to initialize g.user
            mixer.blend(
                User,
                account=account,
                email="test@everstring.com")

            base_url = "/account/%d/field/mapping" % account.id
            # create a new field mapping for account fit model
            payload1 = {
                "targetType": "salesforce",
                "targetObject": "lead",
                "targetFields": [
                    {"targetField": "Model Fit Score", "modelId": model_old.id}
                ]
            }
            resp = self.client.post(base_url, data=json.dumps(payload1), content_type="application/json")
            result = json.loads(resp.data)
            self.assertEqual(len(result), 1)
            # check get method returns mapping for model_new
            resp = self.client.get("%s?targetType=%s&targetObject=%s" % (base_url, "salesforce", "lead"))
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 1)
            self.assertEqual(result[0].get("modelId"), model_new.id)

            # assume that db state is faulty, there exists field mappings for multiple account fit models
            mixer.blend(PublishFieldMapping, model_id=model_old.id, target_field="My Model", target_type="salesforce",
                        target_object="lead", target_org_id=org_id, account_id=account.id)
            resp = self.client.get("%s?targetType=%s&targetObject=%s" % (base_url, "salesforce", "lead"))
            # check get method still only returns mapping for model_new
            result = json.loads(resp.data).get('data')
            self.assertEqual(len(result), 1)
            self.assertEqual(result[0].get("modelId"), model_new.id)

    def test_get_supported_publish_fields_for_account(self):
        with app.app_context():
            account = mixer.blend(Account)
            mixer.cycle(5).blend(Model, account_id=account.id, type=Model.FEATURE_ANALYSIS,
                        status=Model.COMPLETED, is_deleted=0, batch_id=(i for i in range(1,6)))
            mixer.cycle(5).blend(Model, account_id=account.id, type=Model.ACCOUNT_FIT,
                                    status=Model.COMPLETED, is_deleted=0, batch_id=(i for i in range(1, 6)))
            mixer.cycle(10).blend(Model, account_id=account.id, type=Model.FEATURE_ANALYSIS,
                        status=Model.COMPLETED, is_deleted=0, batch_id=(i for i in range(6, 16)))
            mixer.cycle(10).blend(Model, account_id=account.id, type=Model.FIT_MODEL,
                                    status=Model.COMPLETED, is_deleted=0, batch_id=(i for i in range(6, 16)))

            mixer.blend(CRMToken, account_id=account.id, organization_id="123456")

            insight_objs = mixer.cycle(7).blend(CompanyInsight, company_column_name="c")
            insight_ids = map(lambda x: x.id, insight_objs)
            quota = mixer.blend(AccountQuota, account=account, exposed_csv_insight_ids=json.dumps(insight_ids),
                                enable_in_crm=1)

            # create user to initialize g.user
            mixer.blend(
                User,
                account=account,
                email="test@everstring.com")

            url = '/account/%d/supported_publish_fields?targetType=salesforce' % account.id
            resp = self.client.get(url)
            result = json.loads(resp.data).get('data')
            # expect 10 fit models and 1 account fit model
            self.assertEqual(len(result.get('models')), 11)

            # change quota to not allow scoring
            quota.enable_in_crm = 0
            resp = self.client.get(url)
            result = json.loads(resp.data).get('data')
            # expect no results
            self.assertEqual(len(result.get('models')), 0)
