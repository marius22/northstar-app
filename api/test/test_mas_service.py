# -*- coding: utf-8 -*-
import json
from mixer.backend.flask import mixer
from http_client_base import ServiceHttpClientBase
from ns.controller.service import app
from ns.model.mas_token import MASToken
from ns.model.mas_field_mapping import MASFieldMapping
from ns.model.account import Account


class TestMasService(ServiceHttpClientBase):

    def setUp(self):
        super(TestMasService, self).setUp()
        with app.app_context():
            self.acct = mixer.blend(Account)
            self.token = {"client_id": 'client_id', "client_secret": "client_secret", "endpoint": "endpoint"}
            self.mas_token = mixer.blend(MASToken, account_id=self.acct.id, mas_type="marketo", token=json.dumps(self.token))
            self.mapping = {"123": "456"}
            self.mas_field_mapping = mixer.blend(MASFieldMapping,
                                                 account_id=self.acct.id,
                                                 mas_type="marketo",
                                                 mapping= json.dumps(self.mapping))

    def test_get_mas_token(self):
        with app.app_context():
            url = '/mas/integration?account_id={account_id}&mas_type=marketo'.format(account_id=self.acct.id)
            resp = self.client.get(url)
            data = json.loads(resp.data)['data']
            self.assertEqual(data, self.token)

    def test_get_mas_fields_mapping(self):
        with app.app_context():
            url = '/mas/fields_mapping?account_id={account_id}&mas_type=marketo'.format(account_id=self.acct.id)
            resp = self.client.get(url)
            data = json.loads(resp.data)['data']
            self.assertEqual(data.get("mapping"), self.mapping)

