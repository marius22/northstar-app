import datetime

from mixer.backend.flask import mixer

import unittest
from ns.model import db

from ns.controller.app import app
from ns.model.segment import Segment
from ns.model.user_segment import UserSegment
from ns.model.segment import SegmentError
from ns.model.user import User
from base_case import AppTestBase
from ns.model.account import Account
from unit_test_util import check_db_type


microsoft_id = 1
ms_user_ids = [1, 2, 3]
apple_id = 2
apple_user_ids = [4, 5]


class TestSegmentShare(AppTestBase):

    def setUp(self):
        super(TestSegmentShare, self).setUp()
        with app.app_context():
            microsoft = mixer.blend(
                Account,
                name='Microsoft',
                id=microsoft_id)
            apple = mixer.blend(
                Account,
                name='Salesforce',
                domain='salesforce.com',
                account_type='TRIAL')

            ms_users = []
            for i in range(3):
                ms_users.append(mixer.blend(User, account=microsoft))

            apple_users = []
            for i in range(3):
                apple_users.append(mixer.blend(User, account=apple))

            ms_segment = mixer.blend(Segment, owner=ms_users[0],
                                     status=Segment.COMPLETED,
                                     is_deleted=False)

            self.microsoft_id = microsoft.id
            self.ms_segment_id = ms_segment.id
            self.apple_id = apple.id
            self.ms_user_ids = [u.id for u in ms_users]
            self.apple_user_ids = [u.id for u in apple_users]

            db.session.add_all(ms_users)
            db.session.add_all(apple_users)
            db.session.add_all([microsoft, apple, ms_segment])
            db.session.commit()

    def test_basic(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)
            other_ms_user = User.query.get(self.ms_user_ids[1])

            ms_segment.share([other_ms_user.id])
            num_user_segments = UserSegment.query.filter_by(
                user_id=other_ms_user.id,
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()
            self.assertEqual(
                num_user_segments, 1,
                "There should be a UserSegment after sharing a segment.")

    def test_double_share(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)
            other_ms_user = User.query.get(self.ms_user_ids[1])

            ms_segment.share([other_ms_user.id])
            ms_segment.share([other_ms_user.id])

            num_user_segments = UserSegment.query.filter_by(
                user_id=other_ms_user.id,
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()

            self.assertEqual(
                num_user_segments, 1,
                "After sharing with the same user once, there's still only a single UserSegment")

    def test_share_multiple(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)

            ms_segment.share([ms_user_ids[1], ms_user_ids[2]])

            num_user_segments = UserSegment.query.filter_by(
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()

            self.assertEqual(
                num_user_segments, 2,
                "There should be two UserSegments after sharing with two users.")

    def test_unshare(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)
            ms_segment.share([ms_user_ids[1], ms_user_ids[2]])
            ms_segment.unshare([ms_user_ids[1], ms_user_ids[2]])

            num_user_segments = UserSegment.query.filter_by(
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()
            self.assertEqual(
                num_user_segments, 0,
                "After unsharing, there should be no UserSegments")

    # Publishing should share with the other 2 members of this account.
    def test_publish(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)

            ms_segment.publish()
            num_user_segments = UserSegment.query.filter_by(
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()

            self.assertEqual(
                num_user_segments, 2,
                "Should have been 2 user segments. There were %(num_user_segments)s" % locals())

    def test_unpublish(self):
        with app.app_context():
            ms_segment = Segment.query.get(self.ms_segment_id)

            ms_segment.publish()
            ms_segment.unpublish()
            num_user_segments = UserSegment.query.filter_by(
                segment_id=self.ms_segment_id,
                permission=UserSegment.READ_PERMISSION).count()

            self.assertEqual(
                num_user_segments, 0,
                "After publishing and unpublishing, this segment should be shared with 0 users.")
