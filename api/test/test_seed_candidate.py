import unittest

from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.seed_candidate import SeedCandidate
from ns.model.segment import Segment
from ns.model.source import Source
from base_case import AppTestBase


class TestSeedCandidate(AppTestBase):

    def test_delete(self):
        with app.app_context():
            csv_src = mixer.blend(Source, name='csv')
            crm_src = mixer.blend(Source, name='crm')

            csv_cnt = 2
            crm_cnt = 5

            sgmt = mixer.blend(Segment)
            csv_seeds = mixer.cycle(csv_cnt).blend(SeedCandidate, segment=sgmt, source=csv_src)
            crm_seeds = mixer.cycle(crm_cnt).blend(SeedCandidate, segment=sgmt, source=crm_src)

            self.assertEqual(sgmt.seed_candidates.count(), csv_cnt + crm_cnt)

            # Remove candidates with CSV source
            SeedCandidate.delete_by_seg_source_id(sgmt.id, csv_src.id)
            self.assertEqual(sgmt.seed_candidates.count(), crm_cnt)
