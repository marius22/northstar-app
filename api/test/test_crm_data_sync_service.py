import json
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model.account import Account
from http_client_base import ServiceHttpClientBase
from ns.model.crm_token import CRMToken, SFDCToken
from mock import patch


class TestCrmDataSyncService(ServiceHttpClientBase):

    def setUp(self):
        super(TestCrmDataSyncService, self).setUp()
        with app.app_context():
            raw_token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "System Administrator"
            }

            test_email = "test_email@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account = mixer.blend(Account, id=20, name=account_name, type=Account.TRIAL,
                                  crm_data_initialized=True)

            token = mixer.blend(CRMToken, id=1, account_id=20, user_id=1,
                                 crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                 token=json.dumps(raw_token))

            token2 = mixer.blend(CRMToken, id=100, account_id=30, user_id=20,
                                 crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                 token=json.dumps(raw_token))

            test_email = "test_email_2@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account2 = mixer.blend(Account, id=30, name=account_name, type=Account.TRIAL, crm_data_initialized=0)

    def test_crm_initialized(self):
        with app.app_context():
            url = '/crm_token/data_sync_init/tokens'
            resp = self.client.get(url)
            body = json.loads(resp.data)
            self.assertEqual(body.get('data')[0].get("account_id"), 30)

            url = '/data_sync/initialized/30'
            resp = self.client.post(url)
            self.assertEqual(200, resp.status_code)

            url = '/crm_token/data_sync_inc/tokens'
            resp = self.client.get(url)
            body = json.loads(resp.data)
            self.assertEqual(body.get('data')[0].get("account_id"), 20)
            self.assertEqual(body.get('data')[1].get("account_id"), 30)

