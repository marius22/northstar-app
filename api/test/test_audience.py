import unicodecsv
import json
import httpretty
import re

from data_http_client_base import AppDataClientBase
from ns.model.audience import Audience
from ns.model.audience_company import AudienceCompany
from ns.controller.app import app
from ns.model.company_insight import CompanyInsight
from ns.model.user import User
from ns.model.user_role import UserRole
from ns.model.role import Role
from ns.model.account import Account
from ns.model.model import Model
from ns.util.company_db_client import CompanyDBClient
from ns.model.crm_token import CRMToken
from ns.controller.app import audience
from ns.model.audience_exclusion_list import AudienceExclusionList
from ns.controller.app.audience import update_all_related_audiences_company_number
from mock import patch
from six import BytesIO
from mixer.backend.flask import mixer


def generate_csv(num_lines):
    faker = mixer.faker
    f = BytesIO()
    for i in xrange(num_lines):
        w = unicodecsv.writer(f, encoding='utf-8')
        w.writerow((faker.company(), faker.hostname()))

    # Rewind
    f.seek(0)
    return f


class TestAudience(AppDataClientBase):

    def setUp(self):
        super(TestAudience, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()
            # user = mixer.blend(User, email="test@everstring.com")
            role = mixer.blend(Role, name="ES_ADMIN")
            mixer.blend(UserRole, role=role, user=self.current_user)

    @patch('ns.util.company_db_client.CompanyDBClient.create_audience')
    def test_create_audience(self, create_audience_mock):
        create_audience_mock.return_value = {"created": 10}
        url = "/audiences"
        payload = {'type': 'found',
                   'name': 'test',
                   'description': 'test',
                   'modelId': 1,
                   'criteria': {},
                   'metaType': 'audience'}
        resp = self.client.post(url, data=json.dumps(payload), content_type='application/json')
        self.assertEqual(resp.status_code, 200)

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @patch('ns.util.company_db_client.CompanyDBClient.create_audience')
    def test_create_enrich_list(self, create_audience_mock, fetch_mock):
        with app.app_context():
            num_lines = 10
            url = "/audiences"
            params = {'type': 'enriched',
                      'name': 'test',
                      'description': 'test',
                      'modelId': 1,
                      'criteria': {},
                      'metaType': 'audience'}

            create_audience_mock.return_value = {"created": 10}
            fetch_mock.return_value = {'a.com': 'a.com', 'b.com': 'b.com'}
            mixer.blend(AudienceCompany, audience_id=1, company_id=0)
            mixer.blend(AudienceCompany, audience_id=1, company_id=2)
            content = generate_csv(num_lines)
            file_payload = (content, 'My CSV.txt')
            resp = self.client.post(url, query_string=params, data={'file': file_payload})
            self.assertEqual(resp.status_code, 200)

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @patch('ns.util.company_db_client.CompanyDBClient.get_preview')
    def test_create_audience_with_enrich_list(self, enrich_mock, mock_fetch):
        mock_fetch.side_effect = None
        num_lines = 10
        url = "/audiences/preview"
        enrich_mock.return_value = {'a.com': 'a.com', 'b.com': 'b.com'}
        content = generate_csv(num_lines)
        file_payload = (content, 'My CSV.txt')
        resp = self.client.post(url, data={'file': file_payload})
        self.assertEqual(resp.status_code, 200)

    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count')
    def test_delete_audiece_id(self, get_audience_companies_count_mock):
        get_audience_companies_count_mock.side_effect = [300, 400]
        with app.app_context():
            audience = mixer.blend(Audience, is_deleted=False, company_total_num=100, meta_type=100)
            resp = self.client.delete('/audiences/' + str(audience.id))
            self.assertEqual(resp.status_code, 200)
            self.assertEqual(audience.is_deleted, True)

            audience2 = mixer.blend(Audience, is_deleted=False, company_total_num=100, meta_type=100)
            audience3 = mixer.blend(Audience, is_deleted=False, company_total_num=100, meta_type=100)
            ex1 = mixer.blend(Audience, is_deleted=True, meta_type=200)
            ex2 = mixer.blend(Audience, is_deleted=False, meta_type=200)
            ex3 = mixer.blend(Audience, is_deleted=False, meta_type=200)
            mixer.blend(AudienceExclusionList, audience_id=audience2.id, exclusion_list_id=ex1.id)
            mixer.blend(AudienceExclusionList, audience_id=audience2.id, exclusion_list_id=ex2.id)
            mixer.blend(AudienceExclusionList, audience_id=audience3.id, exclusion_list_id=ex2.id)
            mixer.blend(AudienceExclusionList, audience_id=audience2.id, exclusion_list_id=ex3.id)
            resp = self.client.delete('/audiences/' + str(ex2.id))
            self.assertEqual(resp.status_code, 200)
            audience2 = Audience.get_by_id(audience2.id)
            self.assertEqual(audience2.company_total_num, 300)
            self.assertEqual(audience3.company_total_num, 400)

    def test_get_audience(self):
        with app.app_context():
            mixer.cycle(10).blend(Audience, user=self.current_user)
            res = self.client.get('/audiences')
            self.assertEqual(res.status_code, 200)

    def test_switch_fit_model(self):
        with app.app_context():
            account_1 = mixer.blend(Account, deleted=False)
            account_2 = mixer.blend(Account, deleted=False)
            user = mixer.blend(User, account=account_1, status="ACTIVE")

            audience_id = 1
            mixer.blend(Audience, id=audience_id, model_id=None, user=user)

            audience = Audience.get_by_id(audience_id)
            self.assertIsNone(audience.model_id)

            # fit_model with wrong account_id
            fit_model_2 = mixer.blend(Model, account_id=account_2.id)
            resp = self.client.post("/audiences/%s/switch_fit_model" % audience.id,
                                    data=json.dumps({"fit_model_id": fit_model_2.id}),
                                    content_type="application/json"
                                    )
            self.assertEqual(resp.status_code, 403)

            # fit_model with wrong type
            fit_model_1 = mixer.blend(Model, account_id=account_1.id, type=Model.FEATURE_ANALYSIS)
            resp = self.client.post("/audiences/%s/switch_fit_model" % audience.id,
                                    data=json.dumps({"fit_model_id": fit_model_1.id}),
                                    content_type="application/json"
                                    )
            self.assertEqual(resp.status_code, 403)

            fit_model_1 = mixer.blend(Model, account_id=account_1.id, type=Model.FIT_MODEL)
            resp = self.client.post("/audiences/%s/switch_fit_model" % audience.id,
                                    data=json.dumps({"fit_model_id": fit_model_1.id}),
                                    content_type="application/json"
                                    )
            self.assertEqual(resp.status_code, 200)
            audience = Audience.get_by_id(audience_id)
            self.assertEqual(audience.model_id, fit_model_1.id)

    @httpretty.activate
    def test_get_audience_companies(self):
        with app.app_context():
            model = mixer.blend(Model, type=Model.FIT_MODEL, status=Model.COMPLETED)
            audience = mixer.blend(Audience, model_id=model.id)

            httpretty.register_uri('POST',
                                   re.compile(self.company_db_client.common_service_base_url),
                                   body=json.dumps({
                                       "data": [
                                           {"fitScore": 0.7, "employeeSize": 100, "revenueRange": 2000},
                                           {"fitScore": 0.5, "employeeSize": 10, "revenueRange": 100}
                                       ],
                                       "total": 2
                                   }),
                                   content_type="application/json"
                                   )

            resp = self.client.get("/audiences/%s/companies" % audience.id)
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            total = json.loads(resp.data).get("total")
            self.assertEqual(total, 2)
            self.assertEqual(data[0]["fitScore"], 70)
            self.assertEqual(data[0]["employeeSize"], '51-100')
            self.assertEqual(data[0]["revenueRange"], '$1M-$5M')
            self.assertEqual(data[1]["fitScore"], 50)
            self.assertEqual(data[1]["employeeSize"], '1-10')
            self.assertEqual(data[1]["revenueRange"], '$0M-$1M')

    @httpretty.activate
    def test_get_audience_filters(self):
        with app.app_context():

            resp = self.client.get("/audiences/filters")
            self.assertEqual(resp.status_code, 200)

    def test_save_and_get_audience_criteria(self):
        with app.app_context():
            mixer.blend(Audience)
            resp = self.client.post(
                "/audiences/1/criteria",
                data=json.dumps({
                    "criteria": {
                        "haha": ["123456", "etfdagtfda"],
                        "hehe": ["654321", "dfagdasrt"],
                        "heihei": ["fhdsagh", "dfadgtet"],
                        "houhou": {"hhhh": "hdahfh"}
                    }
                }
                ),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            resp = self.client.get("/audiences/1/criteria")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 4)

    def test_save_and_get_audience_keywords(self):
        with app.app_context():
            mixer.blend(Audience)
            resp = self.client.post(
                "/audiences/1/keywords",
                data=json.dumps({
                    "keywords": ["k1", "k2", "k3", "k4"]
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            resp = self.client.get("/audiences/1/keywords")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 4)

    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count')
    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count_with_dedupe')
    @patch('ns.controller.app.audience.get_company_contact_num')
    def test_publish_summary(self, comp_cont_num_mock, deduped_company_count_mock, company_count_mock):
        with app.app_context():
            mixer.blend(AudienceCompany, audience_id=1, company_id=0)
            mixer.blend(AudienceCompany, audience_id=1, company_id=2)
            mixer.blend(CRMToken, user=self.current_user,
                        crm_type=CRMToken.SALESFORCE, account_id=self.current_account.id,
                        organization_id='test_org')

            company_count_mock.return_value = {'total': 2}
            deduped_company_count_mock.return_value = 1
            comp_cont_num_mock.return_value = (2, 10)
            criteria = {"departmentStrength": {
                "Engineering": [
                    "Medium",
                    "Low"
                ],

                "Sales": ["High"]
            },
                "employeeSize": [
                    "1001-5000",
                    "10000+"],

                "industry": [
                    "Public Relations and Communications",
                    "Consumer Goods"],
                "revenue": [
                    "$5M-$10M", "$100M-$250M"],
                "tech": ["Weebly", "Oracle Enterprise Manager"]
            }

            titles = [
                {"jobTitle": "CEO", "match": "Exact", "limit": 2},
            ]

            seniority = ["Director", "VP Level"]

            new = {
                "publishOnlyIfContactFound": True,
                "publishCompaniesTo": "account",
                "publishLeadsTo": "lead"
            }

            existing = {
                "publishOnlyIfContactFound": True,
                "foundInAccount": ["lead"],
                "foundInAccountsAndLeads": ["lead"]
            }

            request_data = {
                "audienceId": 1,
                "modelId": 1,
                "criteria": criteria,
                "titles": titles,
                "manageLevels": seniority,
                "publishType": "CSV0",
                "contactLimitPerCompany": 2,
                "compareAgainstAccounts": True,
                "compareAgainstLeads": True,
                "totalPublishAccountNum": 100,
            }

            resp = self.client.post(
                "/audiences/publish_summary",
                data=json.dumps(request_data),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200, resp.data)


            request_data = {
                "audienceId": 1,
                "modelId": 1,
                "criteria": criteria,
                "titles": titles,
                "manageLevels": seniority,
                "publishType": "SFDC",
                "contactLimitPerCompany": 2,
                "compareAgainstAccounts": True,
                "compareAgainstLeads": True,
                "totalPublishAccountNum": 100,
                "publishNew": True,
                "publishExisting": True,
                "dedupeAgainstPreviousPublish": True,
                "new": new,
                "existing": existing,
                "totalPublishAccountNum": 42
            }

            resp = self.client.post(
                "/audiences/publish_summary",
                data=json.dumps(request_data),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200, resp.data)

            new = {
                "publishOnlyIfContactFound": False,
                "publishCompaniesTo": "account",
                "publishLeadsTo": "lead"
            }

            existing = {
                "publishOnlyIfContactFound": True,
                "foundInAccount": ["lead"],
                "foundInAccountsAndLeads": ["lead"]
            }

            request_data = {
                "audienceId": 1,
                "modelId": 1,
                "criteria": criteria,
                "titles": titles,
                "manageLevels": seniority,
                "publishType": "SFDC",
                "contactLimitPerCompany": 2,
                "compareAgainstAccounts": True,
                "compareAgainstLeads": True,
                "totalPublishAccountNum": 100,
                "publishNew": False,
                "publishExisting": True,
                "dedupeAgainstPreviousPublish": True,
                "new": new,
                "existing": existing,
                "totalPublishAccountNum": 42
            }

            resp = self.client.post(
                "/audiences/publish_summary",
                data=json.dumps(request_data),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200, resp.data)

            new = {
                "publishOnlyIfContactFound": False,
                "publishCompaniesTo": "account",
                "publishLeadsTo": "lead"
            }

            existing = {
                "publishOnlyIfContactFound": True,
                "foundInAccount": ["lead"],
                "foundInAccountsAndLeads": ["lead"]
            }

            request_data = {
                "audienceId": 1,
                "modelId": 1,
                "criteria": criteria,
                "titles": titles,
                "manageLevels": seniority,
                "publishType": "SFDC",
                "contactLimitPerCompany": 2,
                "compareAgainstAccounts": True,
                "compareAgainstLeads": True,
                "totalPublishAccountNum": 100,
                "publishNew": True,
                "publishExisting": True,
                "dedupeAgainstPreviousPublish": True,
                "new": new,
                "existing": existing,
                "totalPublishAccountNum": 42
            }

            resp = self.client.post(
                "/audiences/publish_summary",
                data=json.dumps(request_data),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200, resp.data)

    @patch('ns.util.ns_data_service.NSDataServiceClient.get_company_contacts_num')
    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_ids_with_dedupe')
    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies')
    def test_get_company_contact_num(self, company_mock, company_dedupe_mock, comp_cont_num_mock):
        with app.app_context():
            mixer.blend(AudienceCompany, audience_id=1, company_id=0)
            mixer.blend(AudienceCompany, audience_id=1, company_id=2)
            mixer.blend(AudienceCompany, audience_id=1, company_id=3)
            mixer.blend(CRMToken, user=self.current_user,
                        crm_type=CRMToken.SALESFORCE, account_id=self.current_account.id,
                        organization_id='test_org')
            company_id_dict = [{'companyId':1}, {'companyId':2}, {'companyId':3}]
            company_dedupe_mock.return_value = company_id_dict
            company_mock.return_value = {'data': company_id_dict}
            comp_cont_num_mock.return_value = (2, 10)
            criteria = {"departmentStrength": {
                "Engineering": [
                    "Medium",
                    "Low"
                ],

                "Sales": ["High"]
            },
                "employeeSize": [
                    "1001-5000",
                    "10000+"],

                "industry": [
                    "Public Relations and Communications",
                    "Consumer Goods"],
                "revenue": [
                    "$5M-$10M", "$100M-$250M"],
                "tech": ["Weebly", "Oracle Enterprise Manager"]
            }

            titles = [
                {"jobTitle": "CEO", "match": "Exact", "limit": 2},
            ]

            seniority = ["Director", "VP Level"]

            companies_num = 3
            company_params = {'criteria': criteria,
                              'dedupe': True,
                              'select': ['companyId']
            }
            contact_params = {'titles': titles,
                              'limitPerCompany': 5,
                              'manameLevels': 'VP'

            }
            publish_type = "SFDC"
            companies_num = 3
            audience_id = 1
            model_id = 1
            expected_company_num, expected_contacts_num = audience.get_company_contact_num(
                audience_id, model_id, companies_num, company_params, contact_params, publish_type)

            self.assertEqual(expected_company_num, 2)
            self.assertEqual(expected_contacts_num, 10)

    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count')
    def test_update_audience(self, get_audience_companies_count_mock):
        get_audience_companies_count_mock.return_value = 123
        with app.app_context():
            user = mixer.blend(User, account=self.current_account)
            audience = mixer.blend(Audience, show_in_sfdc=True,
                                   is_private=True,
                                   real_time_scoring_enabled=True,
                                   user=self.current_user,
                                   meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE))

            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                        "name": "hahah_update",
                        "description": "desc_update",
                        "showInSfdc": False,
                        "isPrivate": False,
                        "realTimeScoringEnabled": False,
                        "userId": user.id,
                        'modelId': -1
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            audience = Audience.get_by_id(audience.id)
            self.assertEqual(audience.name, "hahah_update")
            self.assertEqual(audience.description, "desc_update")
            self.assertEqual(audience.show_in_sfdc, False)
            self.assertEqual(audience.is_private, False)
            self.assertEqual(audience.real_time_scoring_enabled, False)
            self.assertEqual(audience.user_id, user.id)

            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                        "showInSfdc": True,
                        "modelId": 1
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            audience = Audience.get_by_id(audience.id)
            self.assertEqual(audience.show_in_sfdc, True)
            self.assertEqual(audience.model_id, 1)

            a0 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))
            mixer.blend(AudienceExclusionList, audience_id=audience.id, exclusion_list_id=a0.id)
            a1 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))
            a2 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))
            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                        "exclusionListIds": [a1.id, a2.id]
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            audience = Audience.get_by_id(audience.id)
            self.assertEqual([one["id"] for one in audience.exclusion_lists], [a1.id, a2.id])

            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            audience = Audience.get_by_id(audience.id)
            self.assertEqual([one["id"] for one in audience.exclusion_lists], [a1.id, a2.id])

            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                        "exclusionListIds": None
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            audience = Audience.get_by_id(audience.id)
            self.assertEqual([one["id"] for one in audience.exclusion_lists], [a1.id, a2.id])

            resp = self.client.post(
                "/audiences/%s" % audience.id,
                data=json.dumps({
                    "audience": {
                        "exclusionListIds": []
                    }
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            audience = Audience.get_by_id(audience.id)
            self.assertEqual([one["id"] for one in audience.exclusion_lists], [])

    def test_get_publish_fields_supported(self):
        with app.app_context():
            mixer.blend(CompanyInsight, display_name="employeeSize", company_column_name="column1")
            mixer.blend(CompanyInsight, display_name="industry", company_column_name="column2")
            mixer.blend(CompanyInsight, display_name="revenue", company_column_name="column3")
            mixer.blend(CompanyInsight, display_name="display4", company_column_name="")
            mixer.blend(CompanyInsight, display_name="display5", company_column_name=None)
            expected = ["employeeSize", "industry", "revenue"]
            resp = self.client.get("/audiences/publish_fields_supported")
            self.assertListEqual(expected, json.loads(resp.data)['data'])

    @httpretty.activate
    def test_audience_distinct_count(self):
        with app.app_context():
            mixer.blend(Audience, id=1)

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.common_service_base_url),
                                   body=json.dumps({
                                       "data": {
                                           "industry": [{"value": "1-2", "count": 2}, {"value": "2-3", "count": 1}],
                                           "state": [{"value": "23-5", "count": 12}]
                                   }}),
                                   content_type='application/json')

            resp = self.client.get("/audiences/1/companies/distinct_counts?keys=industry")
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)
            self.assertEqual(len(data["industry"]), 2)
            self.assertEqual(len(data["state"]), 1)

    def test_save_and_get_audience_exclusion_list(self):
        with app.app_context():
            mixer.blend(Audience)
            a2 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))
            resp = self.client.post(
                "/audiences/1/exclusion_lists",
                data=json.dumps({
                    "exclusionListId": a2.id
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            resp = self.client.get("/audiences/1")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data["exclusionLists"][0]["id"], a2.id)

    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count')
    def test_refresh_count(self, get_audience_companies_count_mock):
        get_audience_companies_count_mock.return_value = 123
        with app.app_context():
            audience = mixer.blend(Audience, meta_type=100, company_total_num=12)
            resp = self.client.post(
                "/audiences/{}/refresh_count".format(audience.id),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            audience_new = Audience.get_by_id(audience.id)
            self.assertEqual(audience_new.company_total_num, 123)

    @patch('ns.util.company_db_client.CompanyDBClient.get_audience_companies_count')
    def test_update_all_related_audiences_company_number(self, get_audience_companies_count_mock):
        get_audience_companies_count_mock.side_effect = [99, 199, 299]
        with app.app_context():
            e1 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))
            e2 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION))

            a1 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE),
                             company_total_num=10)
            a2 = mixer.blend(Audience, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE),
                             company_total_num=20)

            mixer.blend(AudienceExclusionList, audience_id=a1.id, exclusion_list_id=e1.id)
            mixer.blend(AudienceExclusionList, audience_id=a1.id, exclusion_list_id=e2.id)
            mixer.blend(AudienceExclusionList, audience_id=a2.id, exclusion_list_id=e1.id)

            self.assertEqual(Audience.get_by_id(a1.id).company_total_num, 10)
            self.assertEqual(Audience.get_by_id(a2.id).company_total_num, 20)

            update_all_related_audiences_company_number(e2.id)
            self.assertEqual(Audience.get_by_id(a1.id).company_total_num, 99)
            self.assertEqual(Audience.get_by_id(a2.id).company_total_num, 20)

            update_all_related_audiences_company_number(e1.id)
            self.assertEqual(Audience.get_by_id(a1.id).company_total_num, 199)
            self.assertEqual(Audience.get_by_id(a2.id).company_total_num, 299)
