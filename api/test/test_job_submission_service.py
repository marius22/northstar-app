import json
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model.job import Job
from ns.model.segment import Segment
from http_client_base import ServiceHttpClientBase
from ns.model.account import Account
from mock import patch
from ns.model.user import User
from ns.model.seed_candidate import SeedCandidate
from ns.model.seed import Seed


class TestJobSubmissionService(ServiceHttpClientBase):

    def setUp(self):
        super(TestJobSubmissionService, self).setUp()
        with app.app_context():
            self.segment = mixer.blend(Segment, is_deleted=False)
            self.job = mixer.blend(Job, segment_id=self.segment.id, type=Job.SEGMENT)

    def test_create_model_run_request(self):
        with app.app_context():
            url_tmpl = '/jobs/{0}/recommendations/user/{1}/segment/{2}/job_submission'
            url = url_tmpl.format(
                self.job.id, self.segment.owner_id,
                self.segment.id)

            resp = self.client.post(
                url,
                data=json.dumps({"seed_list": ["example.com"]}),
                content_type='application/json')

            data = json.loads(resp.data)
            self.assertDictEqual({
                'status': 'OK'
            }, data)

    def test_no_seedlist(self):
        with app.app_context():
            url = '/jobs/{0}/recommendations/user/{1}/segment/{2}/job_submission'.format(self.job.id, self.segment.owner_id, self.segment.id)
            resp = self.client.post(url, data=json.dumps({}), content_type='application/json')
            self.assertEqual(resp.status_code, 400)
            self.assertIn('message', resp.data)
            data = json.loads(resp.data)
            self.assertDictEqual({
                'message': 'seed list is required'
            }, data)

    def test_nonexistent_job_id(self):
        with app.app_context():
            url = '/jobs/{0}/recommendations/user/{1}/segment/{2}/job_submission'.format(self.job.id + 100, self.segment.owner_id, self.segment.id)
            resp = self.client.post(url, data=json.dumps({"seed_list": ["example.com"]}), content_type='application/json')
            self.assertEqual(resp.status_code, 404)
            self.assertIn('message', resp.data)
            data = json.loads(resp.data)
            self.assertDictEqual({
                'message': 'Job(ID = {0}) is not found'.format(self.job.id + 100)
            }, data)

    def test_nonexistent_segment_id(self):
        with app.app_context():
            url = '/jobs/{0}/recommendations/user/{1}/segment/{2}/job_submission'.format(self.job.id, self.segment.owner_id, self.segment.id + 100)
            resp = self.client.post(url, data=json.dumps({"seed_list": ["example.com"]}), content_type='application/json')
            self.assertEqual(resp.status_code, 404)
            self.assertIn('message', resp.data)
            data = json.loads(resp.data)
            self.assertDictEqual({
                'message': 'Segment(ID = {0}) is not found'.format(self.segment.owner_id + 100)
            }, data)

    def test_trigger_feature_analysis(self):
        with app.app_context():
            account = mixer.blend(Account, account_type=Account.TRIAL)
            user = mixer.blend(User, account=account)
            segment = mixer.blend(Segment, owner=user, is_deleted=False)

            seed_candidate_1 = mixer.blend(SeedCandidate)
            seed_candidate_2 = mixer.blend(SeedCandidate)
            seed = mixer.blend(Seed, seed_candidate_ids=json.dumps([seed_candidate_1.id, seed_candidate_2.id]))
            job = mixer.blend(Job, acount_id=account.id, segment_id=segment.id, type=Job.SEGMENT, seed_id=seed.id)

            resp = self.client.post("/jobs/segment_jobs/%s/feature_analysis" % job.id)
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data)
            self.assertDictEqual({
                'status': 'OK'
            }, data)
