import json
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model.job import Job
from http_client_base import ServiceHttpClientBase


class TestFeedbackService(ServiceHttpClientBase):

    def setUp(self):
        super(TestFeedbackService, self).setUp()
        with app.app_context():
            job = mixer.blend(Job, id=1)

    def test_job_not_found(self):
        with app.app_context():
            url = '/jobs/2/recommendations/everstring.com/feedback/like'
            resp = self.client.post(url)
            self.assertEqual(resp.status_code, 404)
            self.assertIn('message', resp.data)

    def test_wrong_feedback(self):
        with app.app_context():
            url = '/jobs/1/recommendations/everstring.com/feedback/blah'
            resp = self.client.post(url)
            self.assertEqual(resp.status_code, 400)
            body = json.loads(resp.data)
            self.assertEqual(body['message'], 'Feedback is not like | dislike')

    def test_normal_feedback(self):
        with app.app_context():
            base_url = '/jobs/1/recommendations/everstring.com/feedback/'
            feedback_list = ['like', 'dislike']
            for feedback in feedback_list:
                url = base_url + feedback
                resp = self.client.post(url)
                self.assertEqual(resp.status_code, 200)
                body = json.loads(resp.data)
                self.assertEqual(body['status'], 'OK')
