import json
from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model import db
from ns.model.crm_token import CRMToken, SFDCToken
from ns.model.user import User
from base_case import AppTestBase
from ns.model.account import Account
from mock import patch, call, mock, MagicMock
from simple_salesforce.api import SalesforceResourceNotFound, SalesforceExpiredSession

class TestCRMToken(AppTestBase):

    def test_save(self):
        with app.app_context():
            user = mixer.blend(User)
            db_token = CRMToken._save_token(user.account_id, user.id, CRMToken.SALESFORCE, "{}", 'orgid_1', 'test1@test.com', 'admin1')
            self.assertIsNotNone(db_token.id)
            db.session.remove()
            db_token2 = CRMToken._save_token(user.account_id, user.id, CRMToken.SALESFORCE, json.dumps({"1": "2"}),
                                             'orgid_2', 'test2@test.com', 'admin2')
            self.assertIsNotNone(db_token2.id)
            self.assertEqual(db_token.id, db_token2.id)
            self.assertNotEqual(db_token.token, db_token2.token)

    def test_sfdc_token_properties(self):

        sfdc_token = SFDCToken(1, 1, 1)
        self.assertEqual(sfdc_token.user_id, 1)
        self.assertEqual(sfdc_token.account_id, 1)

        with self.assertRaises(AssertionError):
            access_token = sfdc_token.access_token

        test_token = "access_token"
        sfdc_token.access_token = test_token

        self.assertEqual(sfdc_token.access_token, test_token)

    def test_load_from_crm_token(self):

        self.assertIsNone(SFDCToken.load_from_crm_token(None), None)

        token = {
            SFDCToken.ACCESS_TOKEN_KEY: "access token",
            SFDCToken.INSTANCE_URL_KEY: "instance url",
            SFDCToken.REFRESH_TOKEN_KEY: "refresh token"
        }
        crm_token = CRMToken(account_id=1, user_id=1,
                             crm_type=CRMToken.SALESFORCE,
                             token=json.dumps(token))
        sfdc_token = SFDCToken.load_from_crm_token(crm_token)

        self.assertEqual(sfdc_token.instance_url, "instance url")
        self.assertEqual(sfdc_token.access_token, "access token")
        self.assertEqual(sfdc_token.refresh_token, "refresh token")

    def test_sfdc_token_update(self):
        with app.app_context():
            user = mixer.blend(User, id=1, account_id=1)

            sfdc_token = SFDCToken(user.account_id, user.id, CRMToken.SALESFORCE_SANDBOX)

            with self.assertRaisesRegexp(AssertionError, "token has no value for"):
                sfdc_token.save()

            with self.assertRaisesRegexp(AssertionError, "missing"):
                sfdc_token.access_token = ""
                sfdc_token.save()

            # test save
            sfdc_token.access_token = "access_token_1"
            sfdc_token.instance_url = "instance_1"
            sfdc_token.refresh_token = "refresh_1"
            sfdc_token.organization_id = "org_id_1"
            sfdc_token.email = "email_1"
            sfdc_token.role = "admin_1"

            sfdc_token.save()

            sfdc_token_2 = CRMToken.get_sfdc_token(user.id)

            self.assertEqual(sfdc_token_2.access_token, "access_token_1")
            self.assertEqual(sfdc_token_2.instance_url, "instance_1")
            self.assertEqual(sfdc_token_2.refresh_token, "refresh_1")

            # test update by dict
            token_dict = {SFDCToken.ACCESS_TOKEN_KEY: "access_token_2"}
            sfdc_token_2.update_from_dict(token_dict)

            sfdc_token_3 = CRMToken.get_sfdc_token(user.id)
            self.assertEqual(sfdc_token_3.access_token, "access_token_2")
            self.assertEqual(sfdc_token_3.instance_url, "instance_1")
            self.assertEqual(sfdc_token_3.refresh_token, "refresh_1")

            # test create
            user2 = mixer.blend(User, id=2, account_id=2)
            token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "role_3"
            }

            saved_token = CRMToken.create_sfdc_token(user2.account_id, user2.id, token, CRMToken.SALESFORCE_SANDBOX)
            db_token = CRMToken.get_sfdc_token(user2.id)
            self.assertEqual(saved_token.access_token, db_token.access_token)
            self.assertEqual(saved_token.instance_url, db_token.instance_url)
            self.assertEqual(saved_token.refresh_token, db_token.refresh_token)

    def test_get_salesforce_admin_by_crm_type_accounts(self):
        with app.app_context():
            raw_token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "System Administrator"
            }

            test_email = "test_email@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account = mixer.blend(Account, id=20, name=account_name, type=Account.TRIAL,
                                  crm_data_initialized=True)

            token = mixer.blend(CRMToken, id=1, account_id=20, user_id=1,
                                crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                token=json.dumps(raw_token))

            token2 = mixer.blend(CRMToken, id=100, account_id=30, user_id=20,
                                 crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                 token=json.dumps(raw_token))

            test_email = "test_email_2@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account2 = mixer.blend(Account, id=30, name=account_name, type=Account.TRIAL,
                                   crm_data_initialized=False)
            tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([20, 30])
            self.assertEqual(len(tokens), 2)

    @patch('ns.model.crm_token.SFDCToken.is_expired')
    @patch('ns.model.crm_token.SFDCToken.refresh_token_and_update')
    def test_get_by_id_refresh_if_expired(self, is_expired_fn_mock, refersh_fn_mock):
        with app.app_context():
            raw_token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "System Administrator"
            }

            test_email = "test_email@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account = mixer.blend(Account, id=20, name=account_name, type=Account.TRIAL,
                                  crm_data_initialized=True)

            token = mixer.blend(CRMToken, id=1, account_id=20, user_id=1,
                                crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                token=json.dumps(raw_token))

            is_expired_fn_mock.return_value = False
            crm_token = CRMToken.get_by_id_refresh_if_expired(1)
            self.assertEqual(crm_token.id, 1)
            is_expired_fn_mock.assert_called_once_with(token.crm_type)

            crm_token = CRMToken.get_by_id_refresh_if_expired(12)
            self.assertEqual(crm_token, None)

            refersh_fn_mock.return_value = None
            is_expired_fn_mock.return_value = True
            crm_token = CRMToken.get_by_id_refresh_if_expired(1)
            self.assertEqual(crm_token.id, 1)
            refersh_fn_mock.assert_called_with()
    @patch("simple_salesforce.Salesforce.query")
    def test_is_expired(self, sf_query_fn_mock):
        with app.app_context():
            raw_token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "System Administrator"
            }

            test_email = "test_email@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account = mixer.blend(Account, id=20, name=account_name, type=Account.TRIAL,
                                  crm_data_initialized=True)

            token = mixer.blend(CRMToken, id=1, account_id=20, user_id=1,
                                crm_type=CRMToken.SALESFORCE, role="System Administrator",
                                token=json.dumps(raw_token))
            crm_token = CRMToken.get_by_id(1)
            sf_query_fn_mock.return_value = None
            sdfc_token = SFDCToken.load_from_crm_token(crm_token)
            self.assertFalse(sdfc_token.is_expired())

            sf_query_fn_mock.side_effect = SalesforceExpiredSession("/query/lead", 405, "lead fields", "expired token")
            self.assertTrue(sdfc_token.is_expired())

            sf_query_fn_mock.side_effect = SalesforceResourceNotFound("/query/lead", 405, "lead fields", "not found")
            self.assertFalse(sdfc_token.is_expired())

