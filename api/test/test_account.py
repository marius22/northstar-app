import json
from six import StringIO, BytesIO
import unicodecsv
from mock import patch, PropertyMock, mock
from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.account import Account
from ns.model.seed_domain import SeedDomain
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.account_quota import AccountQuota
from ns.model.role import Role
from ns.model.user_invitation import UserInvitation
from http_client_base import AppHttpClientBase
from ns.model.user_role import UserRole
from ns.model.model import Model
from ns.model.seed_company import SeedCompany
from ns.model.model_seed import ModelSeed
from ns.model.job import Job
import httpretty
import re
from ns.util.company_db_client import CompanyDBClient
from ns.util.queue_service_client import QueueServiceClient


def generate_csv(num_lines):
    f = BytesIO()
    for i in xrange(num_lines):
        w = unicodecsv.writer(f, encoding='utf-8')
        w.writerow(("companyName_" + str(i), "www.domain_" + str(i) + ".com"))
        # write some duplicate records
        w.writerow(("companyName_" + str(i), "www.domain_" + str(i) + ".com"))

    # Rewind
    f.seek(0)
    return f


class TestAccount(AppHttpClientBase):

    def setUp(self):
        super(TestAccount, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()
            self.queue_client = QueueServiceClient.instance()

    def test_create_and_get(self):

        with app.app_context():

            test_email = "test_email@everstring.com"
            account_name = Account.generate_name_by_email(test_email)
            account = Account.create_account(
                account_name,
                Account.TRIAL
            )

            self.assertIsNotNone(account)
            self.assertIsNotNone(account.id)
            self.assertEquals(account.name, account_name)

            account2 = Account.get_by_name(account_name)

            self.assertIsNotNone(account2)
            self.assertIsNotNone(account2.id)
            self.assertEquals(account2.name, account_name)
            self.assertEqual(account2.account_type, Account.TRIAL)

    def test_get_owned_segments(self):
        self.maxDiff = None
        with app.app_context():
            acct = mixer.blend(Account)
            mixer.cycle().blend(User, account=acct)

            sgmt_cnt = 42
            sgmts = mixer.cycle(sgmt_cnt).blend(Segment, owner=mixer.SELECT,
                                                is_deleted=False, status=Segment.DRAFT)

            self.assertItemsEqual(acct.owned_segments.all(), sgmts)

    @patch('ns.model.account_quota.AccountQuota.used_segment_cnt', new_callable=PropertyMock)
    def test_segment_cnt_quota_not_reached(self, mock_quota):
        quota = AccountQuota(segment_cnt=3)
        account = Account()
        account.account_quota = quota

        # used cnt  < limit cnt
        mock_quota.return_value = 1
        self.assertTrue(account.segment_cnt_quota_not_reached)

        # used cnt = limit cnt
        mock_quota.return_value = 3
        self.assertFalse(account.segment_cnt_quota_not_reached)

        # used cnt > limit cnt
        mock_quota.return_value = 4
        self.assertFalse(account.segment_cnt_quota_not_reached)

    @patch('ns.model.segment.Segment.publish_segments_to_user')
    @patch('ns.model.account.Account.published_segments', new_callable=PropertyMock)
    @patch('ns.model.user_role.UserRole.reset_role_for_user')
    @patch('ns.model.role.Role.get_by_id')
    @patch('ns.model.user.User.update')
    @patch('ns.model.user.User.get_user_by_email')
    @patch('ns.model.user_invitation.UserInvitation.delete_by_account_user')
    @patch('ns.model.user_invitation.UserInvitation.get_by_account_user_id')
    def test_add_new_user(self, mock_invitation_get, mock_delete,
                          mock_user_get, mock_user_update,
                          mock_role_get, mock_user_role,
                          mock_published_segments, mock_segment_update):
        mock_invitation_get.return_value \
            = UserInvitation(first_name="new_first_name", last_name="new_last_name", phone=None, role_id=2)
        mock_delete.side_effect = None
        mock_user_get.return_value = User()
        mock_user_update.side_effect = None
        mock_role_get.side_effect = None
        mock_user_role.side_effect = None
        mock_published_segments.return_value = None
        mock_segment_update.side_effect = None

        account = Account(id=1)
        account.add_user(User(id=1, email="test_email", phone="old_phone"))

        mock_invitation_get.assert_called_once_with(1, 1)
        mock_delete.assert_called_once_with(1, 1)
        mock_role_get.assert_called_once_with(2)
        mock_user_role.assert_called_once_with(mock.ANY, mock.ANY)
        mock_user_update.assert_called_once_with(
            {"status": User.ACTIVE,
             "accountId": 1,
             "firstName": "new_first_name",
             "lastName": "new_last_name",
             "phone": "old_phone"
             }
        )
        mock_segment_update.assert_called_once_with(mock.ANY, mock.ANY)

    def test_get_accounts(self):
        with app.app_context():
            mixer.blend(Account, account_type=Account.TRIAL)
            mixer.blend(Account, account_type=Account.PAYING)
            mixer.blend(Account, account_type=Account.ES)

            res = self.client.get('/accounts')
            self.assertIn('TRIAL', res.data)
            self.assertIn('PAYING', res.data)
            self.assertIn('ES', res.data)
            data = json.loads(res.data).get('data')
            self.assertEqual(len(data), 3)

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @httpretty.activate
    def test_upload_account_seeds(self, mock_insert, mock_fetch):
        mock_insert.side_effect = None
        mock_fetch.side_effect = None
        with app.app_context():
            account = mixer.blend(Account)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            mixer.blend(SeedDomain, domain="domain_1.com")
            mixer.blend(SeedDomain, domain="domain_11.com")

            content = generate_csv(100)

            file_payload = (content, 'My CSV.txt')

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domains3.com", "name": "company_name_3"},
                                           {"domain": "domains4.com", "name": "company_name_4"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            resp = self.client.post('/accounts/%s/seeds' % account.id, data={'file': file_payload})
            self.assertEqual(resp.status_code, 200)

            models = Model.get_by_account_id_and_type(account.id, Model.ACCOUNT_FIT)
            self.assertEqual(len(models), 1)

    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @patch('ns.model.salesforce.NSSalesforce.query_accounts_from_opportunities')
    @httpretty.activate
    def test_upload_account_seeds_sfdc(self, mock_sfdc_client, mock_insert):
        mock_insert.side_effect = None
        with app.app_context():
            account = mixer.blend(Account)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            mock_sfdc_client.return_value = [
                {"Id": "sfdc_accountId_1", "Name": "sfdc_account_name_1", "Website": ""},
                {"Id": "sfdc_accountId_2", "Name": "sfdc_account_name_2", "Website": None},
                {"Id": "sfdc_accountId_3", "Name": "sfdc_account_name_3", "Website": "domain_3.com"},
                {"Id": "sfdc_accountId_4", "Name": "sfdc_account_name_4", "Website": "domain_4.com"}
            ]

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domains3.com", "name": "company_name_3"},
                                           {"domain": "domains4.com", "name": "company_name_4"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            resp = self.client.post('/accounts/%s/seeds/sfdc' % account.id)
            self.assertEqual(resp.status_code, 200)

            model = account.latest_crm_fit_model
            self.assertIsNotNone(model)

    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @patch('ns.model.salesforce.NSSalesforce.query_accounts_from_opportunities')
    @httpretty.activate
    def test_account_seeds_overview(self, mock_sfdc_client, mock_insert):
        mock_insert.side_effect = None

        with app.app_context():
            account = mixer.blend(Account)
            user = mixer.blend(User, account=account, email="test@everstring.com")

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            mock_sfdc_client.return_value = [
                {"Id": "sfdc_accountId_1", "Name": "sfdc_account_name_1", "Website": ""},
                {"Id": "sfdc_accountId_2", "Name": "sfdc_account_name_2", "Website": None},
                {"Id": "sfdc_accountId_3", "Name": "sfdc_account_name_3", "Website": "domain_3.com"},
                {"Id": "sfdc_accountId_4", "Name": "sfdc_account_name_4", "Website": "domain_4.com"}
            ]

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domains3.com", "name": "company_name_3"},
                                           {"domain": "domains4.com", "name": "company_name_4"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            resp = self.client.post('/accounts/%s/seeds/sfdc' % account.id)
            self.assertEqual(resp.status_code, 200)
            resp = self.client.get("/accounts/%s/seeds/sfdc/overview?"
                                   "limit=10&offset=0&type=unknown&order=-companyName" % account.id)
            self.assertEqual(resp.status_code, 200)

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @patch('ns.model.model_seed.ModelSeed.get_related_domains')
    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @httpretty.activate
    def test_submit_account_fit_model(self, mock_insert, mock_domains, mock_fetch):
        mock_insert.side_effect = None
        mock_domains.return_value = ["", ""]
        mock_fetch.side_effect = None
        with app.app_context():
            # prepare domains
            account = mixer.blend(Account)
            user = mixer.blend(User, account=account, email="test@everstring.com")
            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)

            mixer.blend(UserRole, user=user, role=role)

            content = generate_csv(100)

            file_payload = (content, 'My CSV.txt')

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.queue_client.base_url),
                                   body=json.dumps({
                                       "status": "OK"
                                   }),
                                   content_type="application/json")

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domains3.com", "name": "company_name_3"},
                                           {"domain": "domains4.com", "name": "company_name_4"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            resp = self.client.post('/accounts/%s/seeds' % account.id, data={'file': file_payload})
            self.assertEqual(resp.status_code, 200)

            self.client.post("/accounts/%s/submit_fit_job" % account.id,
                             data=json.dumps({"type": "CSV"}),
                             content_type="application/json")

            models = Model.get_by_account_id_and_type(account.id, Model.ACCOUNT_FIT)
            self.assertEqual(len(models), 1)

    def test_get_accounts_by_crm_data_initialized(self):
        with app.app_context():
            account1 = mixer.blend(Account, id=1, account_type=Account.PAYING, crm_data_initialized=True)
            account2 = mixer.blend(Account, id=2, account_type=Account.PAYING, crm_data_initialized=False)

            initialized_accounts = Account.get_datasync_initialized()
            uninitialized_accounts = Account.get_datasync_uninitialized()
            self.assertEqual(len(initialized_accounts), 1)
            self.assertEqual(initialized_accounts[0], 1)

            self.assertEqual(len(Account.get_datasync_uninitialized()), 1)
            self.assertEqual(uninitialized_accounts[0], 2)
