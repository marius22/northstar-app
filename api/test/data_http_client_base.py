from ns.controller.app import app as app_app
from ns.controller.service import app as service_app
from init_data_base import AppWithDataBase, ServiceWithDataBase


class AppDataClientBase(AppWithDataBase):

    def setUp(self):
        super(AppDataClientBase, self).setUp()
        self.client = app_app.test_client()


class ServiceDataClientBase(ServiceWithDataBase):

    def setUp(self):
        super(ServiceDataClientBase, self).setUp()
        self.client = service_app.test_client()
