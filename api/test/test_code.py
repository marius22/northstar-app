import datetime
from ns.controller.app import app
from ns.model.code import Code
from base_case import AppTestBase


class TestCode(AppTestBase):

    def test_create_and_get(self):

        with app.app_context():

            test_code = "code"

            code = Code.create_code(
                test_code,
                "value",
                Code.EMAIL_CODE,
                datetime.datetime.utcnow() + datetime.timedelta(days=-1)
            )

            self.assertIsNotNone(code)
            self.assertEquals(code.code, test_code)

            code2 = Code.get_by_code(test_code)

            self.assertIsNotNone(code2)
            self.assertEquals(code2.code, test_code)

            self.assertTrue(code2.hash_expired())

    def test_remove_by_email(self):
        with app.app_context():
            test_code = "code"
            test_email = "test_email"
            Code.create_code(
                test_code,
                test_email,
                Code.EMAIL_CODE,
                datetime.datetime.utcnow() + datetime.timedelta(days=-1)
            )

            result = Code.get_by_code(test_code)
            self.assertIsNotNone(result)

            Code.remove_by_email_code_type(test_email, Code.EMAIL_CODE)

            result = Code.get_by_code(test_code)
            self.assertIsNone(result)
