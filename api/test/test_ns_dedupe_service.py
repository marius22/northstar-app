import json
import unittest
import re
from mock import patch, call, mock, MagicMock

from ns.util.sf_data_service import SFDataServiceClient
from ns.util.ns_dedupe_service import NSDedupeService
from ns.controller.service import app


class TestNSDedupeService(unittest.TestCase):
    def setUp(self):
        with app.app_context():
            self.client = NSDedupeService.instance()
            self.org_id = "test_org_id"
            self.domains = ['a.com', 'b.com', 'c.com']
            self.companies = ['e', 'f']
            self.emails = ['a@a.com', 'b@a.com']

    @patch('ns.util.sf_data_service.SFDataServiceClient.query_sf_accounts_by_contact')
    def test_query_sf_contacts(self, query_mock):
        self.client.merge_results = {
            'a.com': {'accounts': []},
            'b.com': {'accounts': []},
        }
        domains = ['a.com', 'b.com']
        query_mock.return_value = [{'domain': 'a.com'}]

        self.client._dedupe_sf_contacts(self.org_id, domains)
        expected_results = {
            'a.com': {'accounts': [{'domain': 'a.com'}]},
            'b.com': {'accounts': []},
        }
        self.assertDictEqual(expected_results, self.client.merge_results)

    @patch('ns.util.sf_data_service.SFDataServiceClient.query_sf_leads')
    def test_query_sf_leads(self, query_mock):

        self.client.merge_results = {
            'a.com': {'leads': []},
            'b.com': {'leads': []},
        }
        self.client.name2domain = {'a': 'a.com',
                                   'b': 'b.com'}
        domains = {'a.com', 'b.com'}
        companies = {'a', 'b'}
        query_mock.return_value = [{'domain': 'e.com', 'company': 'ac', 'emailDomain': 'a.com'},
                                   {'domain': 'c.com', 'company': 'b', 'emailDomain': 'e.com'}]
        self.client._dedupe_sf_leads(self.org_id, domains, companies)
        expected_results = {
            'a.com': {'leads': [{'domain': 'e.com', 'company': 'ac', 'emailDomain': 'a.com'}]},
            'b.com': {'leads': [{'domain': 'c.com', 'company': 'b', 'emailDomain': 'e.com'}]}
        }
        self.assertDictEqual(expected_results, self.client.merge_results)

    @patch('ns.util.sf_data_service.SFDataServiceClient.query_sf_accounts')
    def test_query_sf_accounts(self, query_mock):
        self.client.merge_results = {
            'a.com': {'accounts': []},
            'b.com': {'accounts': []},
        }
        self.client.name2domain = {'a': 'a.com',
                                   'b': 'b.com'}
        domains = {'a.com', 'b.com'}
        companies = {'a', 'b'}
        query_mock.return_value = [{'domain': 'a.com', 'name': 'a'},
                                   {'domain': 'c.com', 'name': 'b'}]
        self.client._dedupe_sf_accounts(self.org_id, domains, companies)
        expected_results = {
            'a.com': {'accounts': [{'domain': 'a.com', 'name': 'a'}]},
            'b.com': {'accounts': [{'domain': 'c.com', 'name': 'b'}]}
        }
        self.assertDictEqual(expected_results, self.client.merge_results)
