import json
import logging
from werkzeug import exceptions
from ns.controller.app import app
from ns.model import db
from http_client_base import AppHttpClientBase

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

app.testing = True


class TestCommonAPI(AppHttpClientBase):

    def test_sanity_check(self):
        self.assertTrue(True)

    def test_json_status_ok(self):
        resp = self.client.get('/status')
        self.assertEqual(resp.status_code, 200)

        body = json.loads(resp.data)
        self.assertDictContainsSubset({'status': 'OK'}, body)

    def test_greet(self):
        resp = self.client.get('/')
        self.assertIn('EverString', resp.data)

    def test_exception_json_response(self):
        message = 'Something wrong'

        # Bypass the restriction to setup all stuffs before the first request
        app._got_first_request = False

        @app.route('/test/raise_500')
        def test_raise_500():
            raise exceptions.InternalServerError(message)

        resp = self.client.get('/test/raise_500')
        data = json.loads(resp.data)

        self.assertDictEqual(data, {'message': message})
