# -*- coding: utf-8 -*-
"""
Test the seed candidates CSV upload flow, includes the components below:
    - API
    - Reader
"""
import httplib
import json
import re
import httpretty
from mock import patch
import unicodecsv
import unittest
from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model import db
from ns.model.seed_csv_reader import SeedCSVReader
from ns.model.segment import Segment
from ns.model.source import Source
from ns.model.user import User
from ns.util.company_db_client import CompanyDBClient
from six import StringIO, BytesIO
from http_client_base import AppHttpClientBase


def generate_csv(num_lines):
    faker = mixer.faker
    f = BytesIO()
    for i in xrange(num_lines):
        w = unicodecsv.writer(f, encoding='utf-8')
        w.writerow((faker.company(), faker.hostname()))

    # Rewind
    f.seek(0)
    return f


class TestSeedCSVUpload(AppHttpClientBase):

    def setUp(self):
        super(TestSeedCSVUpload, self).setUp()
        with app.app_context():
            mixer.blend(Source, source_name=Source.CSV)

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    @httpretty.activate
    def test_file_ok(self, mock_fetch):
        mock_fetch.side_effect = None
        with app.app_context():
            user = mixer.blend(User, email='test@everstring.com')
            sgmt = mixer.blend(Segment, status=Segment.DRAFT, is_deleted=False, owner=user)
            url = '/segments/%d/seeds/csv' % sgmt.id

            num_lines = 10

            app.config['MAX_SEED_CSV_ROW_COUNTS'] = num_lines + 1

            f = BytesIO()
            w = unicodecsv.writer(f, encoding='utf-8')
            w.writerow(())
            w.writerow(("company", "domain.com"))
            w.writerow(("domain_1.com", "domain_2.com"))
            w.writerow((u"域名",))
            f.seek(0)

            file_payload = (f, 'My CSV.txt')

            company_fixtures = [{'domain': ''}] * num_lines

            client = CompanyDBClient.instance()
            httpretty.register_uri(httpretty.POST,
                                   re.compile(client.company_service_base_url),
                                   body=json.dumps({'data': {'companies': company_fixtures}}))

            resp = self.client.post(url, data={'file': file_payload})
            data = json.loads(resp.data)

            self.assertDictEqual({
                'total': 2
            }, data)

    def test_file_missing(self):
        with app.app_context():
            user = mixer.blend(User, email='test@everstring.com')
            sgmt = mixer.blend(Segment, status=Segment.DRAFT, is_deleted=False, owner=user)
            url = '/segments/%d/seeds/csv' % sgmt.id
            resp = self.client.post(url, data={'file': None})
            self.assertEqual(resp.status_code, httplib.BAD_REQUEST)

    def test_csv_line_num_exceeds(self):
        with app.app_context():
            max_lines = 1
            app.config['MAX_SEED_CSV_ROW_COUNTS'] = max_lines

            user = mixer.blend(User, email='test@everstring.com')
            sgmt = mixer.blend(Segment, status=Segment.DRAFT, is_deleted=False, owner=user)
            url = '/segments/%d/seeds/csv' % sgmt.id

            num_lines = 10
            content = generate_csv(num_lines)

            file_payload = (content, 'My CSV.txt')

            resp = self.client.post(url, data={'file': file_payload})
            data = json.loads(resp.data)

            self.assertDictEqual({
                'message': "Please make sure your CSV doesn't have more than %d rows."
                             % (max_lines)
            }, data)

    def test_not_allow_csv_overwrite(self):
        with app.app_context():
            user = mixer.blend(User, email='test@everstring.com')
            sgmt = mixer.blend(Segment, status=Segment.COMPLETED, is_deleted=False, owner=user)

            url = '/segments/%d/seeds/csv' % sgmt.id

            num_lines = 10
            app.config['MAX_SEED_CSV_ROW_COUNTS'] = num_lines + 1
            content = generate_csv(num_lines)

            file_payload = (content, 'My CSV.txt')

            resp = self.client.post(url, data={'file': file_payload})
            data = json.loads(resp.data)
            self.assertDictEqual({
                'message': "Segment is no longer in draft, so you can't upload a new seed list."
            }, data)


class TestSeedCSVReader(unittest.TestCase):

    @patch('ns.model.company_name_2_domain.fetch_domains_list_by_company_names')
    def test_get_domains_from_csv(self, mock_fetch):
        mock_fetch.side_effect = None

        f = BytesIO()
        w = unicodecsv.writer(f, encoding='utf-8')
        w.writerow(())
        w.writerow(("", ""))
        w.writerow(("hahhahah",))
        w.writerow(("company_name", ""))
        w.writerow(("", "domain.com"))
        w.writerow(("company", "domain.com"))
        w.writerow(("domain_1.com", "domain_2.com"))
        w.writerow(("domain_1.com",))
        w.writerow((u"域名",))
        f.seek(0)

        domains = SeedCSVReader.read_domains_from_file(f)
        self.assertEqual(len(domains), 3)

