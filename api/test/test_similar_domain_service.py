import httpretty
import re
import json
from mixer.backend.flask import mixer

from ns.controller.service import app
from http_client_base import ServiceHttpClientBase
from ns.util.company_db_client import CompanyDBClient
from ns.model.account import Account
from ns.model.model import Model


class TestSimilarDomainsService(ServiceHttpClientBase):

    def setUp(self):
        super(TestSimilarDomainsService, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    @httpretty.activate
    def test_get_similar_domains_with_fitscore(self):
        with app.app_context():
            account = mixer.blend(Account)
            mixer.blend(Model, account_id=account.id, batch_id="x", status=Model.COMPLETED, type=Model.ACCOUNT_FIT)
            mixer.blend(Model, account_id=account.id, batch_id="x", status=Model.COMPLETED, type=Model.FEATURE_ANALYSIS)

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": [
                                           {"name": "mock_1"},
                                           {"name": "mock_2"}
                                       ]
                                   }),
                                   content_type="application/json")

            resp = self.client.post(
                "/similar_domains/top_similarity_in_filter",
                data=json.dumps({
                    "accountId": account.id,
                    "domain": "everstring.com",
                    "limit": 10,
                    "offset": 0,
                    "filters": {"zipcode": {"$in": ["94041"]}}
                }),
                content_type="application/json"
                )

            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)

    @httpretty.activate
    def test_search_domains_by_tag(self):
        with app.app_context():
            account = mixer.blend(Account)
            mixer.blend(Model, account_id=account.id, batch_id="x", status=Model.COMPLETED, type=Model.ACCOUNT_FIT)
            mixer.blend(Model, account_id=account.id, batch_id="x", status=Model.COMPLETED, type=Model.FEATURE_ANALYSIS)

            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": [
                                           {"name": "mock_1"},
                                           {"name": "mock_2"}
                                       ]
                                   }),
                                   content_type="application/json")

            resp = self.client.post(
                "/similar_domains/search_by_tag_with_fitscore",
                data=json.dumps({
                    "accountId": account.id,
                    "ngrams": "everstring.com",
                    "limit": 10
                }),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)
