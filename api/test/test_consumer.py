import json
import unittest

import re
import httpretty
from mock import patch

from consumer.export_csv import export_csv
from consumer.export_csv import ContactServiceClient
from consumer.export_csv import NSServiceClient


class TestConsumer(unittest.TestCase):

    class Config(object):
        def __init__(self):
            self.EMAIL_CLIENT_FROMADDR = ''
            self.EMAIL_CLIENT_USERNAME = ''
            self.EMAIL_CLIENT_PASSWORD = ''
            self.EMAIL_CLIENT_SMTP_SERVER = ''
            self.EMAIL_ENABLED = False
            self.COMPANY_SERVICE_BASE_URL = 'http://localhost'
            self.CONTACT_REQUEST_SIZE = 10
            self.CONTACT_REQUEST_RETRY = 3
            self.S3_UPLOAD_RETRY = 3
            self.SEND_EMAIL_RETRY = 2
            self.SQLALCHEMY_DATABASE_URI = "sqlite:///"
            self.AWS_ACCESS_KEY_ID = ''
            self.AWS_SECRET_ACCESS_KEY = ''
            self.S3_CSV_BUCKET = ''
            self.NS_SERVICE_BASE_URL = 'http://localhost.everstring.com:5000'
            self.CUSTOMER_SUCCESS_EMAILS = "noreply@everstring.com"
            self.S3_EXPIRED_TIME = 604800

    @patch('consumer.export_csv.S3ClientDefault.upload_to_s3')
    @patch('consumer.export_csv.extract_company_ids')
    @patch('consumer.user_download.UserDownload.create')
    @patch('ns.util.email_client.EmailClient.send_mail')
    @patch('consumer.export_csv.NSServiceClient.get_company_csv_s3_info')
    @patch('consumer.export_csv.NSServiceClient.get_company_csv')
    @httpretty.activate
    def test_consumer_export_csv(self, company_csv_mock, company_csv_info_mock, email_mock,
                                 db_mock, extract_company_mock, s3_mock):
        email_mock.side_effect = None
        db_mock.side_effect = None
        company_csv_info_mock.return_value = 'company-csv-key', 'company-csv-link', True
        extract_company_mock.return_value = [1, 2, 3]
        s3_mock.return_value = ('contact-csv-key', 'contact-csv-link')
        company_csv_mock.return_value = 'FILE_PATH'
        data = {'user_id': 1,
                'user_email': 'test@everstring.com',
                'account_id': 1,
                'segment_id': 1,
                'segment_name': 'test',
                'departments': ['ABC', 'EFG'],
                'titles': ['123', '456'],
                'contact_limit_per_company': 1,
                'num_companies': 2,
                'csv_url': '/export_csv'}
        config = TestConsumer.Config()

        contact_res = {'data':
                           {'contacts':
                                [
                                    {'companyName': 'A',
                                     'companyURL': 'B',
                                     'firstName': 'C',
                                     'lastName': 'D',
                                     'managementLevel': 'E',
                                     'jobTitle': 'F',
                                     'email': 'G',
                                     'id': 8,
                                     'directPhone': 'H',
                                     'companyPhone': 'I',
                                     'linkedinURL': 'J'},
                                    {'companyName': '1',
                                     'companyURL': '2',
                                     'firstName': '3',
                                     'lastName': '4',
                                     'managementLevel': '5',
                                     'jobTitle': '6',
                                     'email': '7',
                                     'id': 9,
                                     'directPhone': '8',
                                     'companyPhone': '9',
                                     'linkedinURL': '10'},
                                    {'companyName': '1',
                                     'companyURL': '2',
                                     'firstName': '3',
                                     'lastName': '4',
                                     'managementLevel': '5',
                                     'jobTitle': '6',
                                     'email': '7',
                                     'id': 6,
                                     'directPhone': '8',
                                     'companyPhone': '9',
                                     'linkedinURL': '10'},
                                    {'companyName': '1',
                                     'companyURL': '2',
                                     'firstName': '3',
                                     'lastName': '4',
                                     'managementLevel': '5',
                                     'jobTitle': '6',
                                     'email': '7',
                                     'id': 7,
                                     'directPhone': '8',
                                     'companyPhone': '9',
                                     'linkedinURL': '10'}
                                ]
                           }
                    }

        client = ContactServiceClient.instance(config)
        httpretty.register_uri(httpretty.POST, re.compile(client.base_url + '/ns_contacts'),
                               body=json.dumps(contact_res))
        contact_client = NSServiceClient.instance(config)

        remaining_qouta = {'data': 2}
        httpretty.register_uri(httpretty.GET, contact_client.base_url + '/contacts/quota_remaining',
                               body=json.dumps(remaining_qouta))

        is_admin = {'status': False}
        httpretty.register_uri(httpretty.GET, contact_client.base_url + '/contacts/admin_check',
                               body=json.dumps(is_admin))

        contact_ids = {'data': {
            'new_contact_ids': [6, 7, 8],
            'exist_contact_ids': [1, 2, 3, 4, 5, 9]
        }}
        httpretty.register_uri(httpretty.POST, contact_client.base_url + '/contacts/contacts_check',
                               body=json.dumps(contact_ids))
        status = {'status': 'OK'}
        httpretty.register_uri(httpretty.POST, contact_client.base_url + '/contacts/contacts_track',
                               body=json.dumps(status))
        httpretty.register_uri(httpretty.POST, contact_client.base_url + '/companies/user_companies_track',
                               body=json.dumps(status))
        httpretty.register_uri(httpretty.POST, contact_client.base_url + '/companies/companies_track',
                               body=json.dumps(status))
        export_csv('test', config, data)
