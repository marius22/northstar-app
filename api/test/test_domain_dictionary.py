import json
from ns.controller.service import app
from http_client_base import ServiceHttpClientBase
from mixer.backend.flask import mixer
from ns.model.domain_dictionary import DomainDictionary


class TestDomainDictionary(ServiceHttpClientBase):
    def setUp(self):
        super(TestDomainDictionary, self).setUp()

    def test_filter_out_email_domain(self):
        with app.app_context():
            mixer.blend(DomainDictionary, id=1, domain="yahoo.com", domain_type=1)
            mixer.blend(DomainDictionary, id=2, domain="infer.com", domain_type=2)

            domains = ["everstring.com", "yahoo.com", "infer.com"]
            url = '/domain_dictionary/email_check?domains=%s' % ','.join(domains)
            resp = self.client.get(url)
            body = json.loads(resp.data)
            data = body.get('data')
            self.assertEqual(data, ["everstring.com"])

            mixer.blend(DomainDictionary, id=3, domain="everstring.com", domain_type=1)
            domains = ["everstring.com", "yahoo.com", "infer.com"]
            url = '/domain_dictionary/email_check?domains=%s' % ','.join(domains)
            resp = self.client.get(url)
            body = json.loads(resp.data)
            data = body.get('data')
            self.assertEqual(data, [])
