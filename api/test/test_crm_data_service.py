from http_client_base import ServiceHttpClientBase
from mock import patch
from ns.controller.service import crm_data
import logging

logger = logging.getLogger(__name__)


class TestCrmDataService(ServiceHttpClientBase):

    def setUp(self):
        super(TestCrmDataService, self).setUp()
        self.domains = ["google.com", "amazon.com", "jd.com", "linkin.com"]
        self.org_id = "test_org_id"
        self.account_table = "crm_account"
        self.lead_table = "crm_lead"
        self.opportunity_table = "crm_opportunity"
        self.email_domain_field = "email_domain"
        self.domain_field = "domain"

    @patch('ns.controller.service.crm_data.get_find_on_accounts_with_opportunity_sql')
    @patch('ns.controller.service.crm_data.get_find_on_accounts_without_opportunity_sql')
    @patch('ns.util.redshift_client.RedshiftClient.instance')
    def test_find_on_accounts(self, instance, without_opps_sql_fn, with_opps_sql_fn):
        instance.query.return_value = None
        crm_data.find_on_accounts(self.domains, self.org_id, {}, True)
        without_opps_sql_fn.assert_not_called()
        with_opps_sql_fn.assert_called_once_with(self.domains, self.org_id)
        crm_data.find_on_accounts(self.domains, self.org_id, {}, False)
        with_opps_sql_fn.assert_called_once_with(self.domains, self.org_id)

    @patch('ns.controller.service.crm_data.get_find_on_leads_with_opportunity_sql')
    @patch('ns.controller.service.crm_data.get_find_on_leads_without_opportunity_sql')
    @patch('ns.util.redshift_client.RedshiftClient.instance')
    def test_find_on_leads(self, instance, without_opps_sql_fn, with_opps_sql_fn):
        instance.query.return_value = None
        crm_data.find_on_leads(self.domain_field, self.domains, self.org_id, {}, True)
        without_opps_sql_fn.assert_not_called()
        with_opps_sql_fn.assert_called_once_with(self.domain_field, self.domains, self.org_id)
        crm_data.find_on_leads(self.domain_field, self.domains, self.org_id, {}, False)
        with_opps_sql_fn.assert_called_once_with(self.domain_field, self.domains, self.org_id)

    def test_process_find_on_result(self):
        in_crms = [["google.com", "1", "obj_id_1", "Close"],["amazon.com", "1", "obj_id_2", "Going"],
                   ["jd.com", "2", "obj_id_3", "UNKnow"]]
        findOn = "lead"
        result = {}
        result_domains, result_records = crm_data.process_find_on_result(in_crms, findOn, result, self.domains)
        self.assertEqual(result_domains, ["linkin.com"])
        self.assertIsNotNone(result_records["google.com"])
        self.assertIsNotNone(result_records["amazon.com"])
        self.assertIsNotNone(result_records["jd.com"])

    @patch('ns.controller.service.crm_data.find_on_accounts')
    def test_search_on_account(self, findOn_fn):
        findOn_fn.return_value = self.domains, {}
        crm_data.search_on_account(self.domains, self.org_id, {}, True)
        findOn_fn.assert_called_with(self.domains, self.org_id, {}, False)

    @patch('ns.controller.service.crm_data.find_on_leads')
    def test_search_on_lead(self, findOn_fn):
        findOn_fn.return_value = self.domains, {}
        crm_data.search_on_lead(self.domains, self.org_id, {}, True)
        findOn_fn.assert_called_with(self.email_domain_field, self.domains, self.org_id, {}, False)

    @patch('ns.controller.service.crm_data.search_on_lead')
    @patch('ns.controller.service.crm_data.search_on_account')
    def test_find_in_crm(self, account_fn, lead_fn):
        pass


    def test_find_on_accounts_without_opportunity(self):
        sql = crm_data.get_find_on_accounts_without_opportunity_sql(self.domains, self.org_id)
        expect_sql = '''SELECT a.domain, a.owner_id, a.crm_id, '' as stage_name
                FROM {} as a
                JOIN (SELECT domain, org_id, MAX(created_date) AS created_date
                    FROM {}
                    WHERE org_id = '{}'
                    AND domain in('{}')
                    GROUP BY org_id, domain
                    ) as b
                ON a.org_id = b.org_id
                AND a.domain = b.domain
                AND a.created_date = b.created_date
                '''.format(self.account_table, self.account_table, self.org_id, "','".join(self.domains))
        self.assertEqual(expect_sql, sql)

    def test_find_on_accounts_with_opportunity(self):
        sql = crm_data.get_find_on_accounts_with_opportunity_sql(self.domains, self.org_id)
        expect_sql = '''SELECT a.domain, a.owner_id, a.crm_id, b.stage_name
            FROM {} as a
            JOIN {} AS b
              ON a.org_id = '{}'
              AND a.crm_id = b.account_id
              AND b.org_id = a.org_id
            JOIN (SELECT c.domain, c.org_id, MAX(d.created_date) AS created_date
                    FROM {} AS c
                    JOIN {} AS d
                      ON c.org_id = '{}'
                     AND domain in ('{}')
                     AND c.crm_id = d.account_id
                     AND d.org_id = c.org_id
                     GROUP BY c.org_id,c.domain) AS e
            ON a.domain = e.domain
            AND b.created_date = e.created_date
            '''.format(self.account_table, self.opportunity_table, self.org_id, self.account_table, self.opportunity_table,
                       self.org_id, "','".join(self.domains))
        logger.info(sql)
        self.assertEqual(expect_sql, sql)

    def test_find_on_leads_without_opportunity(self):
        sql = crm_data.get_find_on_leads_without_opportunity_sql(self.domain_field, self.domains, self.org_id)
        expect_sql = '''SELECT a.{}, a.owner_id, a.crm_id, '' as stage_name
            FROM {} as a
            JOIN (SELECT {}, org_id, MAX(created_date) AS created_date
                FROM {}
                WHERE org_id = '{}'
                AND {} in('{}')
                GROUP BY org_id, {}
                ) as b
            ON a.org_id = b.org_id
            AND a.{} = b.{}
            AND a.created_date = b.created_date
            ''' \
        .format(self.domain_field, self.lead_table, self.domain_field, self.lead_table, self.org_id, self.domain_field,
                "','".join(self.domains), self.domain_field, self.domain_field, self.domain_field)
        logger.info(sql)
        self.assertEqual(expect_sql, sql)

    def test_find_on_leads_with_opportunity(self):
        sql = crm_data.get_find_on_leads_with_opportunity_sql(self.domain_field, self.domains, self.org_id)
        expect_sql = '''SELECT a.{}, a.owner_id, a.crm_id, c.stage_name
            FROM {} as a
            JOIN (SELECT {}, org_id, MAX(created_date) AS created_date
                FROM {}
                WHERE org_id = '{}'
                AND {} in('{}')
                AND converted_opportunity_id is not null
                GROUP BY org_id, {}
                ) as b
            ON a.org_id = b.org_id
            AND a.{} = b.{}
            AND a.created_date = b.created_date
            JOIN {} as c
            ON a.org_id = c.org_id
            AND c.crm_id = a.converted_opportunity_id''' \
        .format(self.domain_field, self.lead_table, self.domain_field, self.lead_table, self.org_id, self.domain_field,
                "','".join(self.domains), self.domain_field, self.domain_field, self.domain_field, self.opportunity_table)
        logger.info(sql)
        self.assertEqual(expect_sql, sql)






