import unittest
from flask import g, session

from mixer.backend.flask import mixer
import datetime
from mock import patch, mock
from json import dumps, loads
from werkzeug import exceptions
from ns.controller.app import app
from ns.model import db
from ns.model.role import Role
from ns.model.user import User
from ns.model.account import Account
from ns.model.code import Code
from ns.util.password_util import generate_salt_str
from ns.model.user_role import UserRole
from ns.controller.app.auth import authenticated, fresh_required, check_domain_for
from ns.model.session import RedisSession
from ns.model.user_invitation import UserInvitation
from ns.model.domain_dictionary import DomainDictionary
from data_http_client_base import AppDataClientBase


class TestUserAuth(AppDataClientBase):

    def setUp(self):
        super(TestUserAuth, self).setUp()
        with app.app_context():
            mixer.blend(Role, name=Role.TRIAL_USER)

    def register_a_user(self, email, password):
        with app.app_context():
            account = Account.create_trial_account("account_name")
            return User.create_user(account.id, email, password)

    def test_login_logout(self):
        with app.app_context():
            test_email = "kangkangh@everstring.com"
            test_pw = "test$password123456"
            self.register_a_user(test_email, test_pw)
            login_data = dict(
                email=test_email,
                password=test_pw
            )
            user = User.get_user_by_email(test_email)
            user.status = User.ACTIVE
            db.session.commit()

            # login
            res = self.client.post('/users/login', data=dumps(login_data), content_type='application/json')
            self.assertIn("id", loads(res.data)['data'])

            cookie = res.headers['Set-Cookie']
            self.assertIsNotNone(cookie)

            # logout
            cookie_field = cookie.split(';')[0]
            cookie_name = cookie_field.split('=')[0].strip()
            cookie_value = cookie_field.split('=')[1].strip()
            self.client.set_cookie("", cookie_name, cookie_value)

            res2 = self.client.delete('/users/logout')
            self.assertEqual(loads(res2.data)['status'], "OK")

            # if deleted
            user = User.get_user_by_email("kangkangh@everstring.com")
            user.status = User.DELETED
            db.session.commit()
            # login
            res3 = self.client.post('/users/login', data=dumps(login_data), content_type='application/json')
            self.assertEqual(res3.status_code, 403)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_register_logout(self, mock_sender):
        mock_sender.side_effect = None
        with app.app_context():
            test_email = "kangkangh@everstring.com"
            test_pw = "testPasswd123456"
            register_data = dict(
                email=test_email,
                password=test_pw,
                sendEmail=False
            )
            # login
            res = self.client.post('/users/register', data=dumps(register_data), content_type='application/json')
            self.assertEqual(loads(res.data)['status'], "OK")

    @patch('ns.model.user.User.activate')
    @patch('ns.model.user.User.get_user_by_email')
    @patch('ns.model.code.Code.get_by_code')
    def test_confirm_email(self, mock_code, mock_user_get, mock_user_update):
        code = Code(value="test_email", expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=3))
        mock_code.return_value = code
        mock_user_get.return_value = User(status=User.ACTIVE)
        mock_user_update.side_effect = None

        res = self.client.get('/users/activate?code=test_code&redirect_url=redirect_url')

        mock_code.assert_called_once_with("test_code")
        mock_user_update.assert_called_once_with()
        self.assertEqual(res.status_code, 302)

        # test invalid code
        mock_code.return_value = None

        res = self.client.get('/users/activate?code=test_code&redirect_url=redirect_url')
        self.assertEqual(res.status_code, 400)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    @patch('ns.model.user.User.get_user_by_email')
    def test_forget_password(self, mock_user, mock_sender):
        mock_user.return_value = User(salt=generate_salt_str(), status=User.ACTIVE)
        mock_sender.side_effect = None

        res = self.client.put('/users/forget_password',
                              data=dumps({"email": "kangkangh@everstring.com"}),
                              content_type='application/json')
        self.assertEqual(loads(res.data)['status'], "OK")

    @patch('ns.model.user.User.reset_password')
    @patch('ns.model.user.User.get_user_by_email')
    def test_change_password(self, mock_user_get, mock_user_update):
        mock_user_get.return_value = User()
        mock_user_update.side_effect = None

        res = self.client.post('/users/change_password',
                               data=dumps(
                                   {
                                       # "email": "kangkangh@everstring.com",
                                       # "password": "123456",
                                       "new_password": "testPassword654321"
                                    }),
                               content_type='application/json')
        self.assertEqual(loads(res.data)['status'], "OK")
        mock_user_update.assert_called_once_with("testPassword654321")

    def test_get_all_roles(self):
        with app.app_context():
            user = User.get_user_by_email("test@everstring.com")
            es_admin = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            account_admin = mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(Role, name=Role.ES_ADMIN)

            mixer.blend(UserRole, user=user, role=account_admin)

            res = self.client.get('/roles')
            role_names = [role["name"]for role in loads(res.data)['data']]
            self.assertIn(Role.ACCOUNT_ADMIN, role_names)
            self.assertIn(Role.ACCOUNT_USER, role_names)
            self.assertNotIn(Role.ES_SUPER_ADMIN, role_names)
            self.assertNotIn(Role.ES_ADMIN, role_names)
            self.assertNotIn(Role.TRIAL_USER, role_names)

            mixer.blend(UserRole, user=user, role=es_admin)

            res = self.client.get('/roles')
            role_names = [role["name"]for role in loads(res.data)['data']]
            self.assertIn(Role.ACCOUNT_ADMIN, role_names)
            self.assertIn(Role.ACCOUNT_USER, role_names)
            self.assertIn(Role.ES_SUPER_ADMIN, role_names)
            self.assertIn(Role.ES_ADMIN, role_names)
            self.assertIn(Role.TRIAL_USER, role_names)

    def test_assign_roles_to_user(self):
        with app.app_context():
            user = self.current_user

            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            UserRole.assign_roles_to_user(user, [role])

            roles = UserRole.get_roles_by_user(user)

            self.assertEqual(len(roles), 1)
            self.assertIn(role, roles)

            r1 = mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            r2 = mixer.blend(Role, name=Role.ES_ADMIN)
            self.client.put('/users/%s/roles' % user.id,
                            data=dumps(
                                {
                                    "roleNames": ["ES_ADMIN", "ACCOUNT_ADMIN"]
                                }),
                            content_type='application/json'
                            )
            roles = UserRole.get_roles_by_user(user)

            self.assertEqual(len(roles), 2)
            self.assertIn(r1, roles)
            self.assertIn(r2, roles)

    def test_authenticated(self):
        with app.test_request_context() as context:

            session = RedisSession()
            context.session = session

            session._status = RedisSession.INVALID
            self.assertFalse(authenticated())
            with self.assertRaises(AttributeError):
                self.assertIsNone(g.user)

            session._status = RedisSession.FRESH
            session["userId"] = 1
            self.assertTrue(authenticated())
            self.assertIsNotNone(g.user)

    def test_fresh_required(self):
        with app.test_request_context() as context:
            session = RedisSession()
            context.session = session

            @fresh_required
            def foo(num):
                return num + 1

            session._status = RedisSession.UN_FRESH
            with self.assertRaises(exceptions.Unauthorized):
                foo(1)

            session._status = RedisSession.FRESH
            session._last_access_ts = datetime.datetime.utcnow() + datetime.timedelta(days=1)
            self.assertEqual(foo(1), 2)

    def test_check_domain(self):

        # case 1: invalid email
        test_email = ""
        with self.assertRaises(exceptions.BadRequest):
            check_domain_for(test_email)
        with app.app_context():
            mixer.blend(DomainDictionary,
                        domain="free.com", domain_type=DomainDictionary.FREE_EMAIL_DOMAIN)
            mixer.blend(DomainDictionary,
                        domain="competitor.com", domain_type=DomainDictionary.COMPETITOR_DOMAIN)

            # test domain for free email
            test_email = "test@free.com"
            with self.assertRaises(exceptions.BadRequest):
                check_domain_for(test_email)

            # test competitor domain
            test_email = "test@competitor.com"
            with self.assertRaises(exceptions.Forbidden):
                check_domain_for(test_email)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_resend_confirmation_email(self, mock_email_client):

        mock_email_client.side_effect = None

        with app.app_context():
            res = self.client.post('/users/resend_confirmation_email',
                                   data=dumps(
                                     {
                                         "email": "kangkangh@everstring.com"
                                     }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 201)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_account_admin_invite_user(self, mock_email_client):

        mock_email_client.side_effect = None

        with app.app_context():
            cur_user = User.get_user_by_email("test@everstring.com")
            role = Role(name=Role.ACCOUNT_ADMIN)
            mixer.blend(UserRole, user=cur_user, role=role)
            mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(Role, name=Role.CHROME_USER)
            role_id_for_account_user = Role.get_by_name(Role.ACCOUNT_USER).id
            role_id_for_chrome_user = Role.get_by_name(Role.CHROME_USER).id

            # account doesn't exist
            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@everstring.com",
                                           "accountId": -1,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 400)

            # target accountId is not same as accountAdmin's
            account = mixer.blend(Account, name="test_account_1", deleted=False)
            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@everstring.com",
                                           "accountId": account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 403)

            # target email is not same as accountAdmin's
            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@mock.com",
                                           "accountId": cur_user.account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 403)

            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@mock.com",
                                           "accountId": cur_user.account.id,
                                           "roleId": role.id
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 403)

            # invite a new user
            not_exist_email = "not_exist@everstring.com"
            cur_account = cur_user.account
            self.assertIsNone(User.get_user_by_email(not_exist_email))
            self.assertIsNone(Account.get_by_name(Account.generate_name_by_email(not_exist_email)))

            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": not_exist_email,
                                           "accountId": cur_account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')

            self.assertEqual(res.status_code, 200)
            self.assertIsNotNone(User.get_user_by_email(not_exist_email))
            self.assertIsNotNone(Account.get_by_name(Account.generate_name_by_email(not_exist_email)))
            self.assertIsNotNone(UserInvitation.get_by_account_email(cur_account, not_exist_email))

            # invite a existed user who doesn't in inviter's account
            existed_email = "existed@everstring.com"
            cur_account = cur_user.account
            target_user = mixer.blend(User, account=account, email=existed_email, status=User.ACTIVE)
            self.assertIsNotNone(User.get_user_by_email(existed_email))

            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": existed_email,
                                           "accountId": cur_account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')

            self.assertEqual(res.status_code, 200)
            self.assertIsNotNone(User.get_user_by_email(existed_email))
            self.assertIsNone(Account.get_by_name(Account.generate_name_by_email(existed_email)))
            self.assertIsNotNone(UserInvitation.get_by_account_email(cur_account, existed_email))

            # invite a existed user who already in inviter's account
            existed_email_1 = "existed_1@everstring.com"
            cur_account = cur_user.account
            target_user = mixer.blend(User, account=cur_account, email=existed_email_1, status=User.ACTIVE)
            self.assertIsNotNone(User.get_user_by_email(existed_email))

            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": existed_email_1,
                                           "accountId": cur_account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')

            self.assertEqual(res.status_code, 400)

    @patch('ns.util.email_client.NSEmailClient.send_email')
    def test_es_admin_invite_user(self, mock_email_client):

        mock_email_client.side_effect = None

        with app.app_context():
            cur_user = User.get_user_by_email("test@everstring.com")
            role = Role(name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=cur_user, role=role)

            mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(Role, name=Role.CHROME_USER)
            role_id_for_account_user = Role.get_by_name(Role.ACCOUNT_USER).id
            role_id_for_chrome_user = Role.get_by_name(Role.CHROME_USER).id

            # target accountId is not same as admin's
            account = mixer.blend(Account, name="test_account_1", deleted=False)
            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@everstring.com",
                                           "accountId": account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 200)

            # target email is not same as admin's
            res = self.client.post('/users/invite',
                                   data=dumps(
                                       {
                                           "email": "kangkangh@mock.com",
                                           "accountId": cur_user.account.id,
                                           "roleId": role_id_for_account_user
                                       }),
                                   content_type='application/json')
            self.assertEqual(res.status_code, 200)

    def test_confirm_invitation(self):

        with app.app_context():
            test_email = "kangkangh@everstring.com"

            account1 = mixer.blend(Account, name="account_1")
            account2 = mixer.blend(Account, name="account_2")

            user = mixer.blend(User, account=account1, email=test_email)
            role = Role(name=Role.ACCOUNT_ADMIN)
            mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(Role, name=Role.CHROME_USER)
            mixer.blend(UserRole, user=user, role=role)

            mixer.blend(UserInvitation, email=test_email, account_id=account2.id,
                        user_id=user.id, status="PENDING", role_id=Role.get_by_name(Role.CHROME_USER).id)

            self.assertEqual(user.role_names, [Role.ACCOUNT_ADMIN])
            self.assertEqual(user.account_id, account1.id)
            invitation = UserInvitation.get_by_account_email(account2, test_email)
            self.assertIsNotNone(invitation)
            self.assertEqual(invitation.account_id, account2.id)

            test_code_key = "test_code_key"
            Code.create_code(code_str=test_code_key,
                             value=dumps({
                                 "accountId": account2.id,
                                 "userId": user.id
                             }),
                             code_type=Code.EMAIL_CODE)
            res = self.client.get('/users/confirm_invitation?code=test_code_key&redirect_url=redirect_url')
            self.assertEqual(res.status_code, 302)
            user = User.get_user_by_email(test_email)
            roles = UserRole.get_roles_by_user(user)
            self.assertEqual([role.name for role in roles], [Role.CHROME_USER])
            self.assertEqual(user.account_id, account2.id)
            invitation = UserInvitation.get_by_account_email(account2, test_email)
            self.assertIsNone(invitation)
