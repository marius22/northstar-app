# coding=utf-8
import unittest
from mock import patch, Mock
from werkzeug import exceptions
import smtplib
from ns.controller.service import app
from ns.util.file_util import allowed_file_type
from ns.util.file_util import append_ts
from ns.util.string_util import is_not_blank, none_to_empty, generate_hash_str
from ns.util.domain_util import parse_domain, is_domain
from ns.util.password_util import generate_random_pw
from ns.util.password_util import check_password_format, \
    generate_salt_str, encrypt_password_with_salt, \
    encrypt_password, check_password_hash
from ns.util.email_client import NSEmailClient
from ns.util import csv_suite
from ns.util.list_util import distinct_count
from ns.util.queries_filter_builder import InBuilder
from ns.util.param_util import get_value
from ns.util.list_util import split_list


class TestUtils(unittest.TestCase):

    def test_allowed_file_type(self):
        self.assertFalse(allowed_file_type("file.csv", None))
        self.assertFalse(allowed_file_type("file.csv", []))
        self.assertFalse(allowed_file_type("file", ["csv"]))
        self.assertFalse(allowed_file_type("file.html", ["csv"]))
        self.assertFalse(allowed_file_type("file.csv.html", ["csv"]))
        self.assertTrue(allowed_file_type("file.csv", ["csv"]))
        self.assertTrue(allowed_file_type("file.csv", ["csv", "html"]))

    def test_append_ts(self):
        self.assertTrue(append_ts(None).startswith("file"))
        self.assertTrue(append_ts("").startswith("file"))
        test_file = "test_file"
        filename = append_ts(test_file)
        self.assertTrue(append_ts(filename).startswith(test_file))

    def test_not_blank(self):
        self.assertFalse(is_not_blank(None))
        self.assertFalse(is_not_blank(""))
        self.assertFalse(is_not_blank("  "))
        self.assertFalse(is_not_blank([]))
        self.assertFalse(is_not_blank({}))
        self.assertTrue(is_not_blank("_"))
        self.assertTrue(is_not_blank("a"))
        self.assertTrue(is_not_blank([""]))
        self.assertTrue(is_not_blank(u"测试"))

    def test_none_to_empty(self):
        self.assertEqual(none_to_empty(None), "")
        self.assertEqual(none_to_empty([]), "")

    def test_parse_domain(self):
        self.assertEqual(parse_domain(None), "")
        self.assertEqual(parse_domain(""), "")
        self.assertEqual(parse_domain("test_domain.com"), "test_domain.com")
        self.assertEqual(parse_domain("www.test_domain.com"), "test_domain.com")
        self.assertEqual(parse_domain("http://www.test_domain.com"), "test_domain.com")
        self.assertEqual(parse_domain("http://test_domain.com"), "test_domain.com")
        self.assertEqual(parse_domain("test_domain.com.cn"), "test_domain.com.cn")
        self.assertEqual(parse_domain("test_domain.uk.com"), "test_domain.uk.com")
        self.assertEqual(parse_domain("test_domain.us.com"), "test_domain.us.com")
        self.assertEqual(parse_domain("test_DOMASIN.us.com"), "test_domasin.us.com")
        self.assertEqual(parse_domain("\' \"test_DOMASIN.us.com\" \'"), "test_domasin.us.com")

    def test_generate_random_ps(self):
        self.assertEqual(len(generate_random_pw()), 10)
        self.assertEqual(len(generate_random_pw(8)), 8)

    def test_check_password_format(self):
        try:
            check_password_format("123456789---hk")
        except exceptions.BadRequest as e:
            self.fail("check_password_format() raised exception; but should not have since password complies with "
                      "format")
        self.assertRaises(exceptions.BadRequest, check_password_format, None)
        self.assertRaises(exceptions.BadRequest, check_password_format, "")
        self.assertRaises(exceptions.BadRequest, check_password_format, "12345")
        self.assertRaises(exceptions.BadRequest, check_password_format, "12345678901234567890")
        self.assertRaises(exceptions.BadRequest, check_password_format, "123456789012")
        self.assertRaises(exceptions.BadRequest, check_password_format, "hdashguahdguha")
        self.assertRaises(exceptions.BadRequest, check_password_format, "*&&*^*^%^%^$%^$")

    def test_encrypt_password_with_salt(self):
        salt = generate_salt_str()
        plain_pw = "to_be_encrypted"
        pw1 = encrypt_password_with_salt(salt, plain_pw)
        pw2 = encrypt_password_with_salt(salt, plain_pw)
        self.assertEqual(pw1, pw2)

    def test_encrypt_password(self):
        plain_pw = "to_be_encrypted"
        pw1 = encrypt_password(plain_pw)

        self.assertTrue(check_password_hash(pw1, plain_pw))

    def test_generate_hash_str(self):
        test_seed = "test_seed"
        hash_str = generate_hash_str(test_seed)
        self.assertIsNotNone(hash_str)

    @patch('ns.util.email_client.EmailClient._open_connection')
    @patch('ns.util.email_client.EmailClient._get_connection_status')
    def test_email_client_connected(self, mock_get_conn_status, mock_open_conn):
        with app.app_context():
            mock_get_conn_status.return_value = NSEmailClient.CONNECTION_OK
            mock_open_conn.side_effect = None

            NSEmailClient.send_email("kangkangh@everstring.com", "subject", "i am the content")

            mock_get_conn_status.assert_called_once_with()
            mock_open_conn.assert_not_called()

    @patch('ns.util.email_client.EmailClient._open_connection')
    @patch('ns.util.email_client.EmailClient._get_connection_status')
    def test_email_client_connect_gone_away(self, mock_get_conn_status, mock_open_conn):
        with app.app_context():
            mock_get_conn_status.return_value = -1
            mock_open_conn.side_effect = None

            NSEmailClient.send_email("kangkangh@everstring.com", "subject", "i am the content")

            mock_get_conn_status.assert_called_once_with()
            mock_open_conn.assert_called_once_with()

    def test_csv_suite(self):

        class Testiterator(csv_suite.BatchQuerier):
            def total(self):
                return 10

            def get_batch(self, start, end):
                return range(start, end)

        result = list(csv_suite.BatchBasedIterator(Testiterator(), 2))

        self.assertEqual(result, range(10))

    def test_distinct_count(self):
        data = [["1", "2"], ["2"], ["3"], ["2", "4"]]
        result = distinct_count(data)
        self.assertEqual(result.get("1"), 1)
        self.assertEqual(result.get("2"), 3)
        self.assertEqual(result.get("3"), 1)
        self.assertEqual(result.get("4"), 1)

    def test_sample_csv_data_loader(self):
        data_loader = csv_suite.SampleCSVDataLoader(20)
        result = [item for item in data_loader]

        self.assertEqual(len(result), 100)
        self.assertEqual(result[0].get("key"), "key0")
        self.assertEqual(result[99].get("key"), "key99")

    def test_write_csv(self):
        with app.app_context():
            data_loader = csv_suite.SampleCSVDataLoader(20)
            res = csv_suite.write_csv_data_to_response("test.csv", data_loader)
            self.assertEqual(res.headers['Content-Type'], 'text/csv')

            self.assertEqual(res.data.count("key"), data_loader.total() + 1)

    def test_filter_builder(self):
        test_key = 'test_key'
        test_value = 'test_value'

        with self.assertRaises(AssertionError):
            InBuilder.build_filter(test_key, test_value)

        test_value = ['test_value']
        result = InBuilder.build_filter(test_key, test_value)
        self.assertEqual(result.get(test_key), {"$in": test_value})

    def test_param_util(self):
        d = None
        self.assertEqual(get_value(d, "key", []), [])

        d = {
            "key": "value",
            "key_1": None
        }
        self.assertEqual(get_value(d, "key", "default"), "value")
        self.assertEqual(get_value(d, "key_1", "default"), "default")
        self.assertEqual(get_value(d, "key_2", "default"), "default")

    def test_validate_domain(self):
        self.assertTrue(is_domain("www.domain.com"))
        self.assertTrue(is_domain("domain.com"))
        self.assertTrue(is_domain("www.domain.com.cn"))
        self.assertTrue(is_domain("www.domain.com.hk"))
        self.assertTrue(is_domain("www.domain.hah.us.com"))
        self.assertTrue(is_domain("www.domain.hh.uk.com"))
        self.assertTrue(is_domain("hahg.cjoagodh"))

        self.assertFalse(is_domain(""))
        self.assertFalse(is_domain("haha,dhiga"))
        self.assertFalse(is_domain(None))
        self.assertFalse(is_domain("jdishagha"))

    def test_split_list(self):
        s = set(range(1, 23))

        result = split_list(s, 10)
        total_counter = 0
        for _ in result:
            total_counter += 1
        self.assertEqual(total_counter, 3)

        result = split_list(s, 10)
        expected = []
        for item in result:
            expected.extend(item)
        self.assertEqual(expected, list(range(1, 23)))

        result = split_list(s, 7)
        total_counter = 0
        for _ in result:
            total_counter += 1
        self.assertEqual(total_counter, 4)

