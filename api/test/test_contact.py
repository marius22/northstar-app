# -*- coding: utf-8 -*-
import json

import unicodecsv as csv
import datetime
import httplib
import io
import unittest
from mixer.backend.flask import mixer
from mock import patch
from ns.controller.service import app
from ns.model import db
from ns.model.account import Account
from ns.model.job import Job
from ns.model.account_exported_contact import AccountExportedContact
from ns.model.seed import Seed
from ns.model.segment import Segment
from ns.model.user import User
from ns.util.contact_helper import ContactHelper
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from base_case import ServiceTestBase
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.util.data_api_client import DataAPIClient
from http_client_base import ServiceHttpClientBase


class TestContact(ServiceHttpClientBase):

    def setUp(self):
        super(TestContact, self).setUp()
        with app.app_context():
            self.acct = mixer.blend(Account)
            mixer.blend(AccountQuota, account=self.acct,
                        exposed_ui_insight_ids="[1,2]",
                        exposed_csv_insight_ids="[1,2]",
                        expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=10),
                        csv_company_limit=9999,
                        contact_limit=100)

            mixer.blend(AccountExportedContact, id=1, account=self.acct, contact_id=1)
            mixer.blend(AccountExportedContact, id=2, account=self.acct, contact_id=2)
            mixer.blend(AccountExportedContact, id=3, account=self.acct, contact_id=3)
            mixer.blend(AccountExportedContact, id=4, account=self.acct, contact_id=4)
            mixer.blend(AccountExportedContact, id=5, account=self.acct, contact_id=5)

    def test_quota_remaining(self):
        with app.app_context():
            resp = self.client.get('/contacts/quota_remaining?account_id=%d' % self.acct.id)
            data = json.loads(resp.data)['data']
            self.assertEqual(data, 95)
            resp_bad = self.client.get('/contacts/quota_remaining?account_id=324321')
            self.assertEqual(resp_bad.status_code, 400)

    def test_contacts_check(self):
        with app.app_context():
            resp = self.client.post("/contacts/contacts_check",
                                    data=json.dumps({"account_id": self.acct.id, "contact_ids":
                                    [1, 2, 3, 4, 5, 6, 7, 8, 9]}),
                                    content_type="application/json")
            data = json.loads(resp.data)['data']
            expected_new_contact_ids = [6, 7, 8, 9]
            expected_exist_contact_ids = [1, 2, 3, 4, 5]
            new_contact_ids = data.get('new_contact_ids')
            exist_contact_ids = data.get('exist_contact_ids')
            self.assertEqual(
                len(new_contact_ids),
                len(expected_new_contact_ids)
            )
            self.assertEqual(
                len(exist_contact_ids),
                len(expected_exist_contact_ids)
            )
            self.assertItemsEqual(new_contact_ids, expected_new_contact_ids)
            self.assertItemsEqual(exist_contact_ids, expected_exist_contact_ids)

            resp_bad = self.client.post("/contacts/contacts_check",
                                    data=json.dumps({"account_id": 12334234, "contact_ids":
                                        [1, 2, 3, 4, 5, 6, 7, 8, 9]}),
                                    content_type="application/json")
            self.assertEqual(resp_bad.status_code, 400)

    def test_save_contacts(self):
        with app.app_context():
            self.user = mixer.blend(User, account=self.acct, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(UserRole, user=self.user, role=role)
            resp_ok = self.client.post("/contacts/contacts_track",
                                       data=json.dumps({"account_id": self.acct.id,
                                                        "user_id": self.user.id,
                                                        "contact_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            data = json.loads(resp_ok.data)['status']
            self.assertEqual(resp_ok.status_code, 200)
            self.assertEqual(data, 'OK')

            resp_bad = self.client.post("/contacts/contacts_track",
                                       data=json.dumps({"account_id": 121321,
                                                        "user_id": self.user.id,
                                                        "contact_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            self.assertEqual(resp_bad.status_code, 400)

            self.user_admin = mixer.blend(User, account=self.acct, email='test1@everstring.com')
            role_admin = mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(UserRole, user=self.user_admin, role=role_admin)
            resp_admin = self.client.post("/contacts/contacts_track",
                                       data=json.dumps({"account_id": self.acct.id,
                                                        "user_id": self.user_admin.id,
                                                        "contact_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            data = json.loads(resp_admin.data)['status']
            self.assertEqual(resp_admin.status_code, 200)
            self.assertEqual(data, 'OK')

            self.user_super_admin = mixer.blend(User, account=self.acct, email='test2@everstring.com')
            role_super_admin = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.user_super_admin, role=role_super_admin)
            resp_super_admin = self.client.post("/contacts/contacts_track",
                                          data=json.dumps({"account_id": self.acct.id,
                                                           "user_id": self.user_super_admin.id,
                                                           "contact_ids": [6, 7, 8, 9]}),
                                          content_type="application/json")
            data = json.loads(resp_super_admin.data)['status']
            self.assertEqual(resp_super_admin.status_code, 200)
            self.assertEqual(data, 'OK')

    def test_admin_check(self):
        with app.app_context():
            self.user = mixer.blend(User, account=self.acct, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(UserRole, user=self.user, role=role)
            resp_ok = self.client.get('/contacts/admin_check?user_id=%d' % self.user.id)
            data = json.loads(resp_ok.data)['status']
            self.assertEqual(resp_ok.status_code, 200)
            self.assertEqual(data, False)

            self.user_admin = mixer.blend(User, account=self.acct, email='test1@everstring.com')
            role_admin = mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(UserRole, user=self.user_admin, role=role_admin)
            resp_admin = self.client.get('/contacts/admin_check?user_id=%d' % self.user_admin.id)
            data = json.loads(resp_admin.data)['status']
            self.assertEqual(resp_admin.status_code, 200)
            self.assertEqual(data, True)

            self.user_super_admin = mixer.blend(User, account=self.acct, email='test2@everstring.com')
            role_super_admin = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.user_super_admin, role=role_super_admin)
            resp_super_admin = self.client.get('/contacts/admin_check?user_id=%d' % self.user_super_admin.id)
            data = json.loads(resp_super_admin.data)['status']
            self.assertEqual(resp_super_admin.status_code, 200)
            self.assertEqual(data, True)

    @patch('ns.util.data_api_client.DataAPIClient.contact_list')
    def test_publish_contact_apis(self, post_request_mock):
        with app.app_context():
            post_request_mock.return_value = [
                {"contact.esId": 1, "account.esId": 1234, "account.domain": "example1.com"},
                {"contact.esId": 2, "account.esId": 2345, "account.domain": "example2.com"},
                {"contact.esId": 3, "account.esId": 2345, "account.domain": "example2.com"}
            ]

            # /contacts/num_contact
            resp = self.client.post('/contacts/num_contact', data={"companyIds": [1234, 2345], "manageLevels": ["VP"],
                                                                   "limitPerCompany": 5})
            self.assertEqual(resp.status_code, httplib.OK)
            expected = {"data": {"example1.com": 1, "example2.com": 2}}
            self.assertEqual(json.loads(resp.data), expected)

            # /contacts/contact_info
            resp = self.client.post('/contacts/contact_info', data={"companyIds": [1234, 2345], "manageLevels": ["VP"],
                                                                    "limitPerCompany": 5})
            self.assertEqual(resp.status_code, httplib.OK)
            expected = {"data": {
                "example1.com": [
                    {"id": 1, "companyId": 1234, "domain": "example1.com"}
                ],
                "example2.com": [
                    {"id": 2, "companyId": 2345, "domain": "example2.com"},
                    {"id": 3, "companyId": 2345, "domain": "example2.com"}
                ]}}

            self.assertEqual(json.loads(resp.data), expected)

            # /contacts/has_contact
            resp = self.client.post('/contacts/has_contact', data={"companyIds": [1234, 2345], "manageLevels": ["VP"]})
            self.assertEqual(resp.status_code, httplib.OK)
            expected = {"data": [1234, 2345]}
            self.assertItemsEqual(json.loads(resp.data), expected)

    @patch('ns.util.data_api_client.DataAPIClient.contact_title_expand')
    def test_title_expansion(self, api_mock):
        expanded_titles = [
            "ceo",
            "chief executive officer",
            "cfo",
            "chief financial officer",
            "cmo",
            "chief marketing officer",
            "dir of sales",
            "director of sales",
            "sales director",
            "sales dir"
        ]
        api_mock.return_value = expanded_titles

        resp = self.client.get('/contacts/title/expand',
                               data={'titles': ["ceo", "cfo", "cmo", "dir of sales", "sales director"]})
        self.assertEqual(resp.status_code, httplib.OK)
        expected = {"data": expanded_titles}
        self.assertEqual(json.loads(resp.data), expected)

    def test_validate_email(self):
        resp = self.client.post('/contacts/validate',
                                data=json.dumps(
                                    {"emails": ["ex2@example.com", "ex3@example.com", "ex4@example.com"],
                                     "validateEmails": False}
                                ),
                                content_type="application/json")
        self.assertEqual(resp.status_code, httplib.OK)
        expected = {
            "data": {
                "ex2@example.com": True,
                "ex3@example.com": True,
                "ex4@example.com": True
            }
        }
        self.assertEqual(json.loads(resp.data), expected)

    def test_clean_contact_job_titles(self):
        with app.app_context():
            args = {'manageLevels': [], 'titles':
                [{u'jobTitle': u'director - assistant', u'limit': 1, u'match': u'Fuzzy'},
                 {u'jobTitle': u'director -assistant', u'limit': 1, u'match': u'Fuzzy'},
                 {u'jobTitle': u'director & assistant', u'limit': 1, u'match': u'Fuzzy'},
                 {u'jobTitle': u"director's management", u'limit': 1, u'match': u'Fuzzy'},
                 {u'jobTitle': u'director    department', u'limit': 1, u'match': u'Fuzzy'}],
             'limitPerCompany': 1,
             'companyIds': []}

            payloads = {'account': {'esId': []}, 'select': ['contact.esId', 'account.esId', 'account.domain']}
            contact_helper = ContactHelper(args=args,
                                           payload=payloads)
            contact_helper.clean_contact_job_titles()
            self.assertEqual(contact_helper.titles[0].get('jobTitle'), "director assistant")
            self.assertEqual(contact_helper.titles[1].get('jobTitle'), "director assistant")
            self.assertEqual(contact_helper.titles[2].get('jobTitle'), "director assistant")
            self.assertEqual(contact_helper.titles[3].get('jobTitle'), "director s management")
            self.assertEqual(contact_helper.titles[4].get('jobTitle'), "director department")
