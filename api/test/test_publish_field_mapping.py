# -*- coding: utf-8 -*-
import json


from ns.controller.app import app
from data_http_client_base import AppDataClientBase
from ns.model.account_quota import AccountQuota
from ns.model.crm_token import CRMToken, SFDCToken
from ns.model.company_insight import CompanyInsight
from ns.model.model import Model
from ns.model.salesforce import NSSalesforce

from mixer.backend.flask import mixer
from mock import patch, PropertyMock, mock
from ns.constants.default_field_mapping_constants import TARGET_TYPE_SALESFORCE, \
    TARGET_OBJECT_LEAD, DEFAULT_MAPPING, TARGET_TYPE_CSV, TARGET_OBJECT_COMPANY

from ns.constants.insight_constants import InsightConstants


class TestPublishFieldMapping(AppDataClientBase):
    
    @patch('ns.model.crm_token.SFDCToken.get_sfdc_crm_token')
    @patch('ns.model.publish_field_mapping.PublishFieldMapping.get_target_available_fields')
    def test_get_es_fields(self, get_salesforce_fields, get_sfdc_crm_token):
        with app.app_context():
            # create mock sfdc data
            get_salesforce_fields.side_effect = [
                ["ES_APP__ESAlexaRank__c", "ES_APP__ESCountry__c"],
                ["ES_APP__ESAlexaRank__c", "ES_APP__ESCountry__c"]
            ]


            # create mock sfdc token
            sfdc_token = SFDCToken(1, 1, "2")
            sfdc_token.token = { "access_token":"DFDSFDF",
                                 "instance_url": "dfsafd",
                                 "refresh_token": "dsafdfdf",
                                 "organization_id": "organization_id",
                                 "email": "tesfsessfd",
                                 "role": "ewf444g4g"}
            get_sfdc_crm_token.return_value = mixer.blend(CRMToken, account_id = self.current_account.id,
                                                          crm_type=CRMToken.SALESFORCE,
                                                          token=json.dumps(sfdc_token.token),
                                                          organization_id="organization_id")


            # create account, crm token
            account = self.current_account

            mixer.blend(
                CRMToken,
                account_id=account.id,
                organization_id="testing_org_id",
                )

            # add in account fit and feature analysis for models
            # need both to "create" a user facing model
            mixer.blend(Model,
                        account_id=account.id,
                        type=Model.ACCOUNT_FIT,
                        status=Model.COMPLETED,
                        batch_id="123")

            mixer.blend(Model,
                        account_id=account.id,
                        type=Model.FEATURE_ANALYSIS,
                        status=Model.COMPLETED,
                        batch_id="123")

            # set up quota on visible insights for account
            comp_insights = []
            insight_names = [InsightConstants.INSIGHT_NAME_ALEXA_RANK,
                             InsightConstants.INSIGHT_NAME_COUNTRY,
                             InsightConstants.INSIGHT_NAME_INDUSTRY]
            for insight_n in insight_names:
                comp_insight = mixer.blend(
                    CompanyInsight, display_name=insight_n)
                comp_insights.append(comp_insight)

            account_quota = AccountQuota.get_by_account_id(account.id)
            account_quota.update({"exposedCSVInsights":
                                  [comp_insight.display_name
                                   for comp_insight in comp_insights]})

            check_url_sfdc = ("/account/{account_id}/field/mapping?targetType=" +
                             TARGET_TYPE_SALESFORCE + "&targetObject=" +
                             TARGET_OBJECT_LEAD).format(account_id=account.id)

            check_url_csv = ("/account/{account_id}/field/mapping?targetType=" +
                             TARGET_TYPE_CSV + "&targetObject=" +
                             TARGET_OBJECT_COMPANY).format(account_id=account.id)

            field_mapping_sfdc = self.client.get(check_url_sfdc)
            field_mapping_csv = self.client.get(check_url_csv)

            for target_type, field_mapping in [(TARGET_TYPE_SALESFORCE, field_mapping_sfdc),
                                               (TARGET_TYPE_CSV, field_mapping_csv)]:

                result_json_list = json.loads(field_mapping.data)["data"]
                for result_json in result_json_list:
                    insight_id = result_json["insightId"]
                    model_id = result_json["modelId"]
                    target_field = result_json["targetField"]
                    target_type = result_json["targetType"]
                    target_object = result_json["targetObject"]

                    # insightIds is 1 + index of display name,
                    # the target field, type and object from the result
                    # should match the default mapping from file
                    if insight_id:
                        if target_type in [TARGET_TYPE_SALESFORCE]:
                            self.assertEqual(DEFAULT_MAPPING[TARGET_TYPE_SALESFORCE]
                                             [TARGET_OBJECT_LEAD]
                                             [insight_names[insight_id - 1]],
                                             target_field)
                            self.assertEqual(TARGET_TYPE_SALESFORCE,
                                             target_type)
                            self.assertEqual(TARGET_OBJECT_LEAD,
                                             target_object)

                        if target_type in [TARGET_TYPE_CSV]:
                            self.assertEqual(DEFAULT_MAPPING[TARGET_TYPE_CSV]
                                             [TARGET_OBJECT_COMPANY]
                                             [insight_names[insight_id - 1]],
                                             target_field)
                            self.assertEqual(TARGET_TYPE_CSV,
                                             target_type)
                            self.assertEqual(TARGET_OBJECT_COMPANY,
                                             target_object)

                    if model_id:
                        if target_type in [TARGET_TYPE_SALESFORCE]:
                            self.assertEqual(DEFAULT_MAPPING[TARGET_TYPE_SALESFORCE]
                                             [TARGET_OBJECT_LEAD]
                                             [InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE],
                                             target_field)

                        if target_type in [TARGET_TYPE_CSV]:
                            self.assertEqual(DEFAULT_MAPPING[TARGET_TYPE_CSV]
                                             [TARGET_OBJECT_COMPANY]
                                             [InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE],
                                             target_field)
