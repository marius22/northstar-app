import unittest
import httpretty
import re
import json
from flask import current_app

from ns.util.ns_data_service import NSDataServiceClient
from ns.controller.app import app


class TestNSDataServiceClient(unittest.TestCase):

    def setUp(self):
        with app.app_context():
            self.base_url = current_app.config.get('APP_SERVICE_BASE_URL')
            self.client = NSDataServiceClient.instance()
            self.company_ids = [1, 2]

    @httpretty.activate
    def test_get_contact_num(self):
        url = self.base_url + "/contacts/num_contact"
        data = {
            "data": {"a": 10,
                     "b": 12},
        }
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        company_params = {
            "titles": [],
            "manageLevels": []
        }

        companies_num, contacts_num = self.client.get_company_contacts_num(self.company_ids, company_params)
        self.assertEqual(2, companies_num)
        self.assertEqual(22, contacts_num)

    @httpretty.activate
    def test_get_company_has_contact(self):

        url = self.base_url + "/contacts/has_contact"
        data = {"data": [3, 4]}
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        contact_params = {}
        matched_ids = self.client.get_company_has_contact(self.company_ids, contact_params)
        self.assertListEqual(matched_ids, data['data'])
