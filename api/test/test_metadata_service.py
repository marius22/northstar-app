import unittest
import httpretty
import re
import json
from mock import patch
from flask import current_app

from ns.controller.service import app
from http_client_base import AppHttpClientBase
from ns.util.company_db_client import CompanyDBClient
from ns.model.metadata import Metadata
from ns.model.role import Role


class TestMetadataService(AppHttpClientBase):
    def setUp(self):
        super(TestMetadataService, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    def test_get_all_categories(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories")
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), len(Metadata.get_all_categories()))

    @patch('ns.util.company_db_client.CompanyDBClient.proxy')
    def test_get_country(self, mock_db_client):
        mock_db_client.return_value = {"data": ["v_1", "v_2"]}
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=country")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("country")), 2)

    @patch('ns.util.company_db_client.CompanyDBClient.proxy')
    def test_get_state(self, mock_db_client):
        mock_db_client.return_value = {"data": ["v_1", "v_2"]}
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=state")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("state")), 2)

    @patch('ns.util.company_db_client.CompanyDBClient.proxy')
    def test_get_city(self, mock_db_client):
        mock_db_client.return_value = {"data": ["v_1", "v_2"]}
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=city")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("city")), 2)

    @patch('ns.util.company_db_client.CompanyDBClient.get_industries')
    def test_get_industry(self, mock_db_client):
        mock_db_client.return_value = ["v_1", "v_2"]
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=industry")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("industry")), 2)

    def test_get_revenue(self):
        with app.app_context():
            revenue_bucket = current_app.config.get('REVENUE_BUCKETS')
            resp = self.client.get("/metadata/categories/details?keys=revenue")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("revenue")), len(revenue_bucket))

    def test_get_employee_size(self):
        with app.app_context():
            employee_bucket = current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
            resp = self.client.get("/metadata/categories/details?keys=employeeSize")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("employeeSize")), len(employee_bucket))

    @patch('ns.util.company_db_client.CompanyDBClient.get_techs')
    def test_get_tech(self, mock_db_client):
        mock_db_client.return_value = ["v_1", "v_2"]
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=tech")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("tech")), 2)

    @patch('ns.util.company_db_client.CompanyDBClient.get_state_zipcode')
    def test_get_tech(self, mock_db_client):
        mock_db_client.return_value = ["v_1", "v_2"]
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=territory")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data.get("territory")), 2)

    def test_get_growth(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=growth")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("growth"), ["High", "Medium", "Low"])

    def test_get_department_strength(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=departmentStrength")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("departmentStrength"), ["High", "Medium", "Low"])

    def test_get_in_crm(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=inCrm")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("inCrm"), ["In Accounts", "In Leads"])

    def test_get_social(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=social")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("social"),
                             ["Has twitter", "Has facebook", "Has LinkedIn", "Has youtube channel"]
                             )

    def test_get_with_contact(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=withContact")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("withContact"),
                             ["Yes", "No"]
                             )

    @patch('ns.util.company_db_client.CompanyDBClient.proxy')
    def test_get_department(self, mock_db_client):
        mock_db_client.return_value = {"data": ["v_1", "v_2"]}
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=department")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("department"), ["v_1", "v_2"])

    def test_get_not_in_crm(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=notInCrm")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("notInCrm"), ["Not in Accounts", "Not in Leads"])

    def test_get_fit_score(self):
        with app.app_context():
            resp = self.client.get("/metadata/categories/details?keys=fitScore")
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("fitScore"), ["0-10", "11-20", "21-30", "31-40", "41-50",
                                                    "51-60", "61-70", "71-80", "81-90", "91-100"])

    @patch('ns.util.company_db_client.CompanyDBClient.proxy')
    def test_get_sub_categories(self, mock_db_client):
        mock_db_client.return_value = {"data": ["v_1", "v_2"]}
        with app.app_context():
            resp = self.client.get("/metadata/categories/subcategories?keys=tech,departmentStrength")
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertEqual(data.get("departmentStrength"), ["v_1", "v_2"])
            self.assertEqual(data.get("tech"), [])
