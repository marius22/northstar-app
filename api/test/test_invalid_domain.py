import unittest
from ns.controller.app import app
from ns.model import db
from ns.model.domain_dictionary import DomainDictionary
from base_case import AppTestBase


class TestInvalidDomain(AppTestBase):

    def test_get(self):

        with app.app_context():
            d1 = DomainDictionary(domain="domain1", domain_type=DomainDictionary.FREE_EMAIL_DOMAIN)
            d2 = DomainDictionary(domain="domain2", domain_type=DomainDictionary.COMPETITOR_DOMAIN)

            db.session.add(d1)
            db.session.add(d2)
            db.session.commit()

            free_domains = DomainDictionary.get_by_type(DomainDictionary.FREE_EMAIL_DOMAIN)
            refused_domains = DomainDictionary.get_by_type(DomainDictionary.COMPETITOR_DOMAIN)

            self.assertEqual(len(free_domains), 1)
            self.assertEqual(len(refused_domains), 1)
