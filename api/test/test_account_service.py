# -*- coding: utf-8 -*-
from http_client_base import ServiceHttpClientBase
from mixer.backend.flask import mixer
from mock import patch
from ns.controller.service import app
from ns.model.account import Account
from ns.model.crm_token import CRMToken


class TestAccountService(ServiceHttpClientBase):

    def setUp(self):
        super(TestAccountService, self).setUp()
        with app.app_context():
            self.acct = mixer.blend(Account)

    def test_get_account_publish_field_mappings(self):
        with app.app_context():
            mixer.blend(CRMToken, account_id=self.acct.id, crm_type="SALESFORCE", organization_id="org123")
            resp = self.client.get('/account/{account_id}/field/mapping?targetType={target_type}'.format(
                account_id=self.acct.id, target_type="salesforce"))
            print resp

