from mock import patch
import unittest
from ns.util.dynamic_config_client import get_configuration_object


class TestDynamicConfigClient(unittest.TestCase):

    @patch('esconfig.consul_es_config.ConsulEsConfig.query')
    def test_get_configuration_object(self, query):
        key = 'KEY'
        value = 'VALUE'
        query.return_value = "%s = \'%s\'" % (key, value)
        d = get_configuration_object("env", "application")
        self.assertEquals(value, getattr(d, key))
