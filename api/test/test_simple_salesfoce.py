import json
import logging
import unittest
import os
import re
from mixer.backend.flask import mixer
from mock import patch, MagicMock, Mock
from ns.controller.app import app
from ns.model import db
from ns.model.crm_token import CRMToken
from ns.model.job import Job
from ns.model.recommendation import Recommendation
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.account import Account
from ns.model.user_segment_sfdc_publisher import UserSegmentSFDCPublisher
from simple_salesforce import Salesforce
from ns.model.crm_token import SFDCToken
from simple_salesforce.api import SalesforceResourceNotFound
from base_case import AppTestBase

FUNCTIONAL_TEST = os.environ.get('FUNCTIONAL_TEST', False)

# Get test session from:
#   https://workbench.developerforce.com/sessionInfo.php
TEST_SESSION_ID = os.environ.get('SFDC_SESSION_ID')
TEST_INSTANCE = os.environ.get('SFDC_INSTANCE')


@unittest.skipIf(not FUNCTIONAL_TEST, 'Not Functional Test')
class TestSimpleSalesforce(unittest.TestCase):
    def setUp(self):
        self.sf = Salesforce(instance=TEST_INSTANCE,
                             session_id=TEST_SESSION_ID)

    def test_account_metadata(self):
        metadata = self.sf.Account.metadata()
        self.assertIn('objectDescribe', metadata)

    def test_top_level_describe(self):
        metadata = self.sf.describe()
        serialized = json.dumps(metadata, indent=4)
        self.assertIn('EverString_Predictive_Segment__c', serialized)

    def test_SFType_recognizes_predictive_segment(self):
        metadata = self.sf.EverString_Predictive_Segment__c.describe()
        self.assertDictContainsSubset({
            'createable': True,
            'custom': True
        }, metadata)

    def test_search(self):
        search_results = self.sf.quick_search('John')
        logging.debug(
            'SFDC quick search result for `John`: %s',
            search_results)


class TestSFDCToken(AppTestBase):

    def test_user_has_sfdc_token_ok(self):
        with app.app_context():
            user = mixer.blend(User)
            account = Account.get_by_id(user.id)
            t = {
                SFDCToken.ACCESS_TOKEN_KEY: mixer.faker.uuid(),
                SFDCToken.INSTANCE_URL_KEY: mixer.faker.uuid()
            }

            token = mixer.blend(CRMToken, crm_type=CRMToken.SALESFORCE,
                                user=user, token=json.dumps(t), account_id=account.id)

            self.assertEqual(t[SFDCToken.INSTANCE_URL_KEY], user.sfdc_token.instance_url)
            self.assertEqual(t[SFDCToken.ACCESS_TOKEN_KEY], user.sfdc_token.access_token)

    @unittest.skip('')
    def test_user_has_sfdc_token_missing(self):
        with app.app_context():
            user = mixer.blend(User)

            t = {
                SFDCToken.INSTANCE_URL_KEY: mixer.faker.uuid(),
            }

            token = mixer.blend(CRMToken, crm_type=CRMToken.SALESFORCE,
                                user=user, token=json.dumps(t))

            with self.assertRaisesRegexp(TypeError, re.compile('session_id\(None\) are required')) as ctx:
                instance = user.sfdc_token.instance


class TestUserSegmentSFDCPublisherInit(AppTestBase):

    def test_init_status_none(self):
        with app.app_context():
            user = mixer.blend(User)
            account = Account.get_by_id(user.account_id)
            t = {
                SFDCToken.ACCESS_TOKEN_KEY: mixer.faker.uuid(),
                SFDCToken.INSTANCE_URL_KEY: mixer.faker.uuid()
            }

            token = mixer.blend(CRMToken, crm_type=CRMToken.SALESFORCE,
                                user=user, token=json.dumps(t), account_id=account.id)

            publisher = UserSegmentSFDCPublisher(user)
            self.assertIsNone(publisher.status)

    @unittest.skipIf(not FUNCTIONAL_TEST, 'Not Functional Test')
    def test_fetch_metadata(self):
        with app.app_context():
            user = mixer.blend(User)

            t = {
                'SFDC_SESSION_ID': TEST_SESSION_ID,
                'SFDC_INSTANCE': TEST_INSTANCE
            }

            token = mixer.blend(CRMToken, crm_type=CRMToken.SALESFORCE,
                                user=user, token=json.dumps(t))

            publisher = UserSegmentSFDCPublisher(user)
            publisher.fetch_metadata()

            self.assertEqual(publisher.status, 'OK')


class TestUserSegmentSFDCPublisherPublish(AppTestBase):
    def setUp(self):
        super(TestUserSegmentSFDCPublisherPublish, self).setUp()
        with app.app_context():
            user = mixer.blend(User)
            t = {
                'SFDC_SESSION_ID': TEST_SESSION_ID,
                'SFDC_INSTANCE': TEST_INSTANCE
            }

            token = mixer.blend(CRMToken, crm_type=CRMToken.SALESFORCE,
                                user=user, token=json.dumps(t))

            self.publisher = SerialPublisher(user)

    @unittest.skipIf(not FUNCTIONAL_TEST, 'Not Functional Test')
    def test_publish_ok(self):
        with app.app_context():
            name = 'UnitTest-{}'.format(mixer.faker.nickname())
            sgmt = mixer.blend(Segment, name=name)

            job = mixer.blend(Job, segment=sgmt, status=Job.COMPLETED)

            recd_cnt = 5
            recds = mixer.cycle(recd_cnt).blend(
                Recommendation, company_name=mixer.FAKE, job=job)

            res = self.publisher.publish_segment(sgmt)

            self.assertEqual(len(res), recd_cnt)


class TestCheckSFDCManagedPackage(AppTestBase):
    def setUp(self):
        super(TestCheckSFDCManagedPackage, self).setUp()
        with app.app_context():
            self.client = app.test_client()

    def test_no_sfdc_token(self):
        with app.app_context():
            user = mixer.blend(User)

            url = '/users/%d/sfdc/managed_package_status' % user.id
            resp = self.client.get(
                url, query_string={'__user_id': user.id})

            data = json.loads(resp.data)

            self.assertDictEqual({
                'message': 'SFDC session_id is invalid',
                'code': 'SESSION_ID_INVALID'
            }, data)

    def test_package_not_install(self):
        with app.app_context():
            token_json = json.dumps({
                'access_token': 'foo',
                'instance_url': 'https://baz.salesforce.com',
                "id": "https://aa/a/b"
            })

            user = mixer.blend(User)
            account = Account.get_by_id(user.id)
            sfdc_token = mixer.blend(
                CRMToken, crm_type=CRMToken.SALESFORCE, token=token_json,
                user=user, account_id=account.id)

            with patch('simple_salesforce.api.SFType.describe') as mock:
                mock.side_effect = SalesforceResourceNotFound('foo', 400, 'es-sgmt', 'not found')
                with patch('simple_salesforce.api.Salesforce.query') as mock_query:
                    result = {
                        "totalSize": 1,
                        "done": True,
                        "records": [
                            {
                                "attributes": {
                                    "type": "PermissionSet",
                                    "url": "/services/data/v28.0/sobjects/PermissionSet/0PS280000010oSlGAI"
                                },
                                "PermissionsInstallPackaging": True
                            }
                        ]
                    }
                    mock_query.return_value = result

                    url = '/users/%d/sfdc/managed_package_status' % sfdc_token.user.id
                    resp = self.client.get(
                        url, query_string={'__user_id': sfdc_token.user_id})
                    data = json.loads(resp.data)

                    self.assertDictEqual({
                        'message': 'Have permission to install package',
                        'code': 'HAVE_PERMISSION_INSTALL_PACKAGE'
                    }, data)


                    result["records"][0]["PermissionsInstallPackaging"] = False
                    mock_query.return_value = result
                    url = '/users/%d/sfdc/managed_package_status' % sfdc_token.user.id
                    resp = self.client.get(
                        url, query_string={'__user_id': sfdc_token.user_id})
                    data = json.loads(resp.data)

                    self.assertDictEqual({
                        'message': 'No permission to install package',
                        'code': 'NO_PERMISSION_INSTALL_PACKAGE'
                    }, data)

                    del result["records"]
                    mock_query.return_value = result
                    url = '/users/%d/sfdc/managed_package_status' % sfdc_token.user.id
                    resp = self.client.get(
                        url, query_string={'__user_id': sfdc_token.user_id})
                    data = json.loads(resp.data)

                    self.assertDictEqual({
                        'message': 'Package has not been installed',
                        'code': 'PACKAGE_NOT_INSTALLED'
                    }, data)

    def test_segment_not_creatable(self):
        with app.app_context():
            token_json = json.dumps({
                'access_token': 'foo',
                'instance_url': 'https://baz.salesforce.com'
            })

            user = mixer.blend(User)
            account = Account.get_by_id(user.id)
            sfdc_token = mixer.blend(
                CRMToken, crm_type=CRMToken.SALESFORCE, token=token_json,
                user=user, account_id=account.id)

            with patch('simple_salesforce.api.SFType.describe') as mock:
                mock.return_value = {
                    'creatable': False
                }

                url = '/users/%d/sfdc/managed_package_status' % sfdc_token.user.id
                resp = self.client.get(
                    url, query_string={'__user_id': sfdc_token.user_id})
                data = json.loads(resp.data)

                # assert on the error message
                self.assertDictEqual({
                    'message': 'Segment custom object is not createable',
                    'code': 'SEGMENT_NOT_CREATEABLE'
                }, data)

                # assert the mock is called
                mock.assert_called_once_with()
