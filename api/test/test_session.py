import unittest
import datetime
from ns.model.session import RedisSession, SessionInterface


class TestSession(unittest.TestCase):

    def test_session(self):

        # init a session
        test_sid = "test_sid"
        test_session_key = "userId"
        test_session_value = "test_user_id"
        session = RedisSession({test_session_key: test_session_value}, test_sid)

        self.assertEqual(session.sid, test_sid)
        self.assertEqual(session[test_session_key], test_session_value)
        self.assertFalse(session.is_valid())

        # login in the session
        session.refresh()
        self.assertTrue(session.is_fresh())
        self.assertTrue(session.is_valid())

        # it has been a long time since last access
        # session is still valid, but is not a fresh one
        session._last_access_ts = datetime.datetime.utcnow() + datetime.timedelta(days=-7)
        self.assertTrue(session.is_valid())
        self.assertEqual(session._status, RedisSession.FRESH)
        self.assertFalse(session.is_fresh())
        self.assertEqual(session._status, RedisSession.UN_FRESH)

        # logout
        session.logout()
        self.assertFalse(session)



