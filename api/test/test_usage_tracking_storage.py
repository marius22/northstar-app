from test.base_case import AppTestBase
from ns.controller.public_api import public_api_app, company
from ns.controller.public_api import public_api_app, usage_tracking_storage
import datetime
import json


class TestUsageTrackingStorage(AppTestBase):
    def setUp(self):
        super(TestUsageTrackingStorage, self).setUp()
        with public_api_app.app_context():
            self.client = public_api_app.test_client()

    def call_test_usage_tracking_endpoint(self):
        with public_api_app.app_context():
            resp = self.client.get('/v1/companies/enrich/usage_tracking')
            data = json.loads(resp.data)['data']
            self.assertEqual(data, "Success")

    def test_store(self):
        with public_api_app.app_context():
            data = {
                'url': 'localhost:9000/v1/companies/enrich/usage_tracking',
                'api_version': 'v1',  # should return v1
                'blueprint': 'public_api',
                'request_method': 'GET',
                'path': '/v1/companies/enrich/usage_tracking',
                'request_timestamp': datetime.datetime.utcnow(),
                'ip_address': '127.0.0.1',
                'customer_id': 'cust_id',
                'request_args': 'None',
                'request_size': '0 bytes',
                'user_agent': 'TestCase',
                'authorized': 0,
                'response_size': '0  bytes',
                'response_status': -1
            }
            usage_tracking_storage.store(data=data)

    # TODO: to be updated later (special case: writing to db involves spawning thread)
    def test_get_usage(self):
        self.call_test_usage_tracking_endpoint()
        usage_tracking_storage.get_usage(limit=1, page=1)

    # TODO: to be updated later (special case: writing to db involves spawning thread)
    def test__get_raw(self):
        self.call_test_usage_tracking_endpoint()
        usage_tracking_storage._get_raw(limit=1, page=1)
