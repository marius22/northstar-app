# -*- coding: utf-8 -*-
import json
import datetime

from mixer.backend.flask import mixer
from mock import patch
from ns.controller.service import app
from ns.model.account import Account
from ns.model.user import User
from ns.model.audience import Audience
from ns.model.model import Model
from http_client_base import ServiceHttpClientBase
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight


class TestAudienceService(ServiceHttpClientBase):

    def setUp(self):
        super(TestAudienceService, self).setUp()
        with app.app_context():
            self.acct = mixer.blend(Account)
            self.user1 = mixer.blend(User, id=1, account=self.acct, status="ACTIVE")
            self.user2 = mixer.blend(User, id=2, account=self.acct, status="DELETED")
            self.user3 = mixer.blend(User, id=3, account=self.acct, status="INACTIVE")
            self.user4 = mixer.blend(User, id=4, account=self.acct, status="ACTIVE")
            mixer.blend(AccountQuota, account=self.acct,
                        real_time_scoring_insight_ids="[1,2,3,4,5]",
                        expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=10),
                        csv_company_limit=100,
                        contact_limit=100)

            model1 = mixer.blend(Model, id=1, name="model1")
            mixer.blend(Audience, id=1, user=self.user1, real_time_scoring_enabled=True, model=model1,
                        is_published_sfdc=False, latest_publish_ts=datetime.datetime(1990, 1, 12), show_in_sfdc=True)
            mixer.blend(Audience, id=11, user=self.user1, real_time_scoring_enabled=True,
                        is_published_sfdc=False, latest_publish_ts=datetime.datetime(2010, 1, 12), show_in_sfdc=True)
            mixer.blend(Audience, id=12, user=self.user1, real_time_scoring_enabled=False)
            mixer.blend(Audience, id=2, user=self.user2, real_time_scoring_enabled=True)
            mixer.blend(Audience, id=3, user=self.user3, real_time_scoring_enabled=True)
            mixer.blend(Audience, id=4, user=self.user4, real_time_scoring_enabled=True)
            mixer.blend(Audience, id=41, user=self.user4, real_time_scoring_enabled=False,
                        latest_publish_ts=datetime.datetime(2005, 1, 12), show_in_sfdc=False, is_published_sfdc=True)
            mixer.blend(Audience, id=100, user=self.user4, is_deleted=False,
                        real_time_scoring_enabled=False, show_in_sfdc=True)
            mixer.blend(Audience, id=101, user=self.user4, is_deleted=True,
                        real_time_scoring_enabled=False, show_in_sfdc=True)

    def test_get_need_score_audience(self):
        with app.app_context():
            resp = self.client.get('/audience/need_score?account_id=%d' % self.acct.id)
            data = json.loads(resp.data)['data']
            self.assertEqual(len(data), 3)

    @patch('ns.util.company_db_client.CompanyDBClient.get_matched_audience_by_domain')
    def test_get_audience_by_domain(self, get_matched_audience_mock):
        with app.app_context():
            request_json = {
                'accountId': self.acct.id,
                'domain': 'domain.com'
            }
            get_matched_audience_mock.return_value = {'matched_ids': [1, 11],
                                                      'published_ids': [41]}
            resp = self.client.get('/audience/domain_match',
                                   data=json.dumps(request_json), content_type='application/json')
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data)['data']
            self.assertEqual(3, len(data))

    def test_get_audience_insights(self):
        with app.app_context():

            mixer.blend(CompanyInsight, id=1, company_column_name="company.id")
            mixer.blend(CompanyInsight, id=2, company_column_name="company.name")
            mixer.blend(CompanyInsight, id=3, company_column_name="company.phone")
            mixer.blend(CompanyInsight, id=4, company_column_name=None)
            mixer.blend(CompanyInsight, id=5, company_column_name="")
            resp = self.client.get('/audience/insights?account_id=%d' % self.acct.id)
            data = json.loads(resp.data)['data']
            self.assertEqual(len(data), 3)
