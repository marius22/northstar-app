# -*- coding: utf-8 -*-
import json

import unicodecsv as csv
import datetime
import io
import unittest
from mixer.backend.flask import mixer
from mock import patch
from ns.controller.service import app
from ns.model import db
from ns.model.account import Account
from ns.model.audience import Audience
from ns.model.job import Job
from ns.model.account_exported_company import AccountExportedCompany
from ns.model.seed import Seed
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from base_case import ServiceTestBase
from ns.model.role import Role
from ns.model.user_published_company import UserPublishedCompany
from ns.model.user_role import UserRole
from http_client_base import ServiceHttpClientBase
from ns.model.user_exported_company import UserExportedCompany


class TestCompany(ServiceHttpClientBase):

    def setUp(self):
        super(TestCompany, self).setUp()
        with app.app_context():
            self.acct = mixer.blend(Account)
            mixer.blend(AccountQuota, account=self.acct,
                        exposed_ui_insight_ids="[1,2]",
                        exposed_csv_insight_ids="[1,2]",
                        expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=10),
                        csv_company_limit=100,
                        contact_limit=100)

            mixer.blend(AccountExportedCompany, id=1, account=self.acct, company_id=1)
            mixer.blend(AccountExportedCompany, id=2, account=self.acct, company_id=2)
            mixer.blend(AccountExportedCompany, id=3, account=self.acct, company_id=3)
            mixer.blend(AccountExportedCompany, id=4, account=self.acct, company_id=4)
            mixer.blend(AccountExportedCompany, id=5, account=self.acct, company_id=5)

    def test_quota_remaining(self):
        with app.app_context():
            resp = self.client.get('/companies/quota_remaining?account_id=%d' % self.acct.id)
            data = json.loads(resp.data)['data']
            self.assertEqual(data, 95)

    def test_companies_check(self):
        with app.app_context():
            resp = self.client.post("/companies/companies_check",
                                    data=json.dumps({"account_id": self.acct.id, "company_ids":
                                    [1, 2, 3, 4, 5, 6, 7, 8, 9]}),
                                    content_type="application/json")
            data = json.loads(resp.data)['data']
            expected_new_company_ids = [6, 7, 8, 9]
            expected_exist_company_ids = [1, 2, 3, 4, 5]
            new_company_ids = data.get('new_company_ids')
            exist_company_ids = data.get('exist_company_ids')
            self.assertEqual(
                len(new_company_ids),
                len(expected_new_company_ids)
            )
            self.assertEqual(
                len(exist_company_ids),
                len(expected_exist_company_ids)
            )
            self.assertItemsEqual(new_company_ids, expected_new_company_ids)
            self.assertItemsEqual(exist_company_ids, expected_exist_company_ids)
            resp_bad = self.client.post("/companies/companies_check",
                                    data=json.dumps({"account_id": 1231312, "company_ids":
                                        [1, 2, 3, 4, 5, 6, 7, 8, 9]}),
                                    content_type="application/json")
            self.assertEqual(resp_bad.status_code, 400)

    def test_save_companies(self):
        with app.app_context():
            self.user = mixer.blend(User, account=self.acct, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ACCOUNT_USER)
            mixer.blend(UserRole, user=self.user, role=role)
            resp_ok = self.client.post("/companies/companies_track",
                                       data=json.dumps({"account_id": self.acct.id,
                                                        "user_id": self.user.id,
                                                        "company_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            data = json.loads(resp_ok.data)['status']
            self.assertEqual(resp_ok.status_code, 200)
            self.assertEqual(data, 'OK')
            resp_bad = self.client.post("/companies/companies_track",
                                       data=json.dumps({"account_id": 123342342,
                                                        "user_id": self.user.id,
                                                        "company_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            self.assertEqual(resp_bad.status_code, 400)

            self.user_admin = mixer.blend(User, account=self.acct, email='test1@everstring.com')
            role_admin = mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(UserRole, user=self.user_admin, role=role_admin)
            resp_admin = self.client.post("/companies/companies_track",
                                          data=json.dumps({"account_id": self.acct.id,
                                                           "user_id": self.user_admin.id,
                                                           "company_ids": [6, 7, 8, 9]}),
                                          content_type="application/json")
            data = json.loads(resp_admin.data)['status']
            self.assertEqual(resp_admin.status_code, 200)
            self.assertEqual(data, 'OK')

            self.user_super_admin = mixer.blend(User, account=self.acct, email='test2@everstring.com')
            role_super_admin = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.user_super_admin, role=role_super_admin)
            resp_super_admin = self.client.post("/companies/companies_track",
                                                data=json.dumps({"account_id": self.acct.id,
                                                                 "user_id": self.user_super_admin.id,
                                                                 "company_ids": [6, 7, 8, 9]}),
                                                content_type="application/json")
            data = json.loads(resp_super_admin.data)['status']
            self.assertEqual(resp_super_admin.status_code, 200)
            self.assertEqual(data, 'OK')

    @patch('ns.model.user_published_company.UserPublishedCompany.count_by_audience')
    @patch('ns.util.redshift_client.RedshiftClient.select')
    @patch('ns.util.redshift_client.RedshiftClient.update')
    @patch('ns.util.redshift_client.RedshiftClient.insert')
    def test_save_user_published_companies(self, insert_mock, update_mock, select_mock, count_mock):
        with app.app_context():
            select_mock.return_value = [(6,), (9,)]
            count_mock.return_value = 2
            update_mock.side_effect = None
            insert_mock.side_effect = None

            self.user = mixer.blend(User, account=self.acct, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            mixer.blend(UserRole, user=self.user, role=role)
            self.audience = mixer.blend(Audience, user_id=self.user.id, company_published_num=1, is_deleted=False)
            mixer.blend(UserPublishedCompany, id=1, user_id=self.user.id, audience_id=self.audience.id, company_id=6)
            mixer.blend(UserPublishedCompany, id=2, user_id=self.user.id, audience_id=self.audience.id, company_id=9)

            resp_ok = self.client.post("/companies/user_published_track",
                                       data=json.dumps({"audience_id": self.audience.id,
                                                        "user_id": self.user.id,
                                                        "type": ["CSV", "CRM"],
                                                        "company_ids": [6, 7, 8, 9]}),
                                       content_type="application/json")
            self.assertEqual(resp_ok.status_code, 200)
            data = json.loads(resp_ok.data)['status']

            self.user_admin = mixer.blend(User, account=self.acct, email='test1@everstring.com')
            role_admin = mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(UserRole, user=self.user_admin, role=role_admin)
            self.audience_admin = mixer.blend(Audience, user_id=self.user_admin.id, is_deleted=False)
            resp_admin = self.client.post("/companies/user_published_track",
                                          data=json.dumps({"audience_id": self.audience_admin.id,
                                                           "user_id": self.user_admin.id,
                                                           "type": ["CSV"],
                                                           "company_ids": [6, 7, 8, 9]}),
                                          content_type="application/json")
            data = json.loads(resp_admin.data)['status']
            self.assertEqual(resp_admin.status_code, 200)
            self.assertEqual(data, 'OK')

            self.user_super_admin = mixer.blend(User, account=self.acct, email='test2@everstring.com')
            role_super_admin = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.user_super_admin, role=role_super_admin)
            self.audience_super_admin = mixer.blend(Audience, user_id=self.user_super_admin.id, is_deleted=False)
            resp_super_admin = self.client.post("/companies/user_published_track",
                                                data=json.dumps({"audience_id": self.audience_super_admin.id,
                                                                 "user_id": self.user_super_admin.id,
                                                                 "type": ["CRM"],
                                                                 "company_ids": [6, 7, 8, 9]}),
                                                content_type="application/json")
            data = json.loads(resp_super_admin.data)['status']
            self.assertEqual(resp_super_admin.status_code, 200)
            self.assertEqual(data, 'OK')

            self.audience_super_admin = mixer.blend(Audience, user_id=self.user_admin.id, is_deleted=False)
            resp_super_admin = self.client.post("/companies/user_published_track",
                                                data=json.dumps({"audience_id": self.audience_super_admin.id,
                                                                 "user_id": self.user_super_admin.id,
                                                                 "type": ["CRM"],
                                                                 "company_ids": [6, 7, 8, 9]}),
                                                content_type="application/json")
            data = json.loads(resp_super_admin.data)['status']
            self.assertEqual(resp_super_admin.status_code, 200)
            self.assertEqual(data, 'OK')
