# -*- coding: utf-8 -*-
import json
from flask import g
from ns.controller.app import app
from data_http_client_base import AppDataClientBase
from ns.model.mas_field_mapping import MASFieldMapping
from ns.model.audience import Audience

from mixer.backend.flask import mixer


class TestMASFieldMapping(AppDataClientBase):

    def test_save_and_get(self):
        with app.app_context():

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": None}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": ''}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": 'null'}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": 'Null'}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": 'None'}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": "ss"}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 200)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": "ss"}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 200)

            data = {"mas_type": "marketo", "mapping": {"standard": {"email": "ss", "domain": "ss"}}}
            resp = self.client.post('/mas/fields_mapping', data=json.dumps(data), content_type='application/json')
            self.assertEqual(resp.status_code, 417)

            self.client.get('/mas/fields_mapping?mas_type={}'.format(data.get("mas_type")))
            records = MASFieldMapping.get_by_account_id_mas_type(g.user.account.id, data.get("mas_type"))
            self.assertEqual(1, len(records))
            record = MASFieldMapping.get_one_by_account_id_mas_type(g.user.account.id, "sandbox")
            self.assertEqual(record, None)

    def test_get_es_fields(self):
        with app.app_context():
            mixer.cycle(10).blend(Audience, user=self.current_user)
            self.client.get('/mas/es_fields')
