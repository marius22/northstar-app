import json
from ns.controller.service import app
from http_client_base import ServiceHttpClientBase
from ns.util.producer import Producer, DummyProducer, ExportCSVMessage, ExportWithDedupeMessage, ExportMarketoMessage, ExportSFDCMessage
from werkzeug import exceptions


class TestMessageService(ServiceHttpClientBase):
    def setUp(self):
        super(TestMessageService, self).setUp()

    def test_bad_request(self):
        with app.app_context():
            url = '/message/publish_audience'
        res = self.client.post(url, data=json.dumps({}), content_type='application/json')
        self.assertEqual(res.status_code, 400)

    def test_create_export_message(self):
        with app.app_context():
            url = '/message/publish_audience'
            data1 = {'publish_type': 'SFDC',
                     'audience_request_endpoint': 3,
                     'audience_request_json': 4,
                     'total_publish_account_num': 6,
                     'contact_limit_per_account': 5,
                     'titles': 1,
                     'manage_level': 'S',
                     'user_id': 6,
                     'user_email': 7,
                     'org_id': 8,
                     'audience_id': 9,
                     'audience_name': 11,
                     'net_new': 12,
                     'existing': 1,
                     'account_id': 1,
                     "publish_fields": 1,
                     "models": [{'model_id': 1, 'model_name': "test"}],
                     "companies_num_in_audience": 10, "companies_num_in_audience_after_filter": 9,
                     "contacts_num_before_dedupe": 12}

            res = self.client.post(url, data=json.dumps(data1), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            data1.pop('manage_level')
            message = ExportSFDCMessage(data1)
            with self.assertRaises(exceptions.BadRequest) as context:
                message.validate(data1)
            self.assertTrue("Missing fields in post data" not in context.exception)

            data2 = {'publish_type': 'CSV0',
                     'audience_request_endpoint': 3,
                     'audience_request_json': 4,
                     'total_publish_account_num': 6,
                     'contact_limit_per_account': 5,
                     'titles': 1,
                     'manage_level': 'S',
                     'user_id': 6,
                     'user_email': 7,
                     'audience_id': 9,
                     'audience_name': 11,
                     'account_id': 1,
                     "publish_fields": 1,
                     "models": [{'model_id': 1, 'model_name': "test"}],
                     "companies_num_in_audience": 10, "companies_num_in_audience_after_filter": 9,
                     "contacts_num_before_dedupe": 12}

            res = self.client.post(url, data=json.dumps(data2), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            data2.pop('manage_level')
            message = ExportCSVMessage(data2)
            with self.assertRaises(exceptions.BadRequest) as context:
                message.validate(data2)
            self.assertTrue("Missing fields in post data" not in context.exception)

            data3 = {'publish_type': 'CSV',
                     'audience_request_endpoint': 3,
                     'audience_request_json': 4,
                     'total_publish_account_num': 6,
                     'contact_limit_per_account': 5,
                     'titles': 1,
                     'manage_level': 'S',
                     'user_id': 6,
                     'user_email': 7,
                     'org_id': 8,
                     'audience_id': 9,
                     'audience_name': 11,
                     'net_new': 12,
                     'existing': 1,
                     'account_id': 1,
                     "publish_fields": 1,
                     "models": [{'model_id': 1, 'model_name': "test"}],
                     "companies_num_in_audience": 10, "companies_num_in_audience_after_filter": 9,
                     "contacts_num_before_dedupe": 12}
            res = self.client.post(url, data=json.dumps(data3), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            data3.pop('manage_level')
            message = ExportWithDedupeMessage(data3)
            with self.assertRaises(exceptions.BadRequest) as context:
                message.validate(data3)
            self.assertTrue("Missing fields in post data" not in context.exception)

            data4 = {'publish_type': 'MAS',
                     'audience_request_endpoint': 3,
                     'audience_request_json': 4,
                     'total_publish_account_num': 6,
                     'contact_limit_per_account': 5,
                     'titles': 1,
                     'manage_level': 'S',
                     'user_id': 6,
                     'user_email': 7,
                     'account_id': 8,
                     'audience_id': 9,
                     'audience_name': 11,
                     'mas_lists': 12,
                     'mas_type': 1,
                     'account_id': 1,
                     "models": [{'model_id': 1, 'model_name': "test"}],
                     "dedupe_mas_lists": [1],
                     "net_new": True,
                     "existing": False,
                     "companies_num_in_audience": 10,
                     "companies_num_in_audience_after_filter": 9,
                     "contacts_num_before_dedupe": 12
                     }
            res = self.client.post(url, data=json.dumps(data4), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            data4.pop('manage_level')
            message = ExportMarketoMessage(data4)
            with self.assertRaises(exceptions.BadRequest) as context:
                message.validate(data4)
            self.assertTrue("Missing fields in post data" not in context.exception)
