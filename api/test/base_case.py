from unittest import TestCase
from ns.controller.app import app as app_app
from ns.controller.service import app as service_app
from ns.model import db
from mixer.backend.flask import mixer
from unit_test_util import check_db_type


class AppTestBase(TestCase):

    def setUp(self):
        with app_app.app_context():
            check_db_type(db)
            db.create_all()
            mixer.init_app(app_app)

    def tearDown(self):
        with app_app.app_context():
            check_db_type(db)
            db.drop_all()


class ServiceTestBase(TestCase):

    def setUp(self):
        with service_app.app_context():
            check_db_type(db)
            db.create_all()
            mixer.init_app(service_app)

    def tearDown(self):
        with service_app.app_context():
            check_db_type(db)
            db.drop_all()
