import json
import datetime

from mock import patch, Mock, mock
from data_http_client_base import AppDataClientBase
from data_http_client_base import ServiceDataClientBase
from ns.controller.app import app
from ns.controller.service import app as service_app
from ns.model.user import User
from ns.model.crm_token import CRMToken
from ns.model import salesforce
from ns.model.crm_token import SFDCToken
from ns.model.sfdc_config import SFDCConfig
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.model.publish_field_mapping import CompanyPublishData
from flask import current_app
from mixer.backend.flask import mixer
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.model.account_quota import AccountQuota
from ns.model.account import Account
from ns.util.sfdc_permission_utils import check_objects_permission, \
    SalesforcePermissionError, check_profile, check_visualforce_pages, check_entities_permission
from ns.model.salesforce import NSSalesforce, fetch_accounts_matching_domain, upsert_accounts_by_domain
from ns.model.crm_token import SFDCToken

token = {"access_token": "token", "refresh_token": "e",
         "organization_id": "ww", "instance_url": "instance_url", "id": "id"}


class TestSalesforceService(ServiceDataClientBase):

    def test_sfdc_config_query_api_in_service(self):
        with service_app.app_context():
            user = User.get_user_by_email("test@everstring.com")
            account = user.account

            config = {
                "insightHeadersCustom": [{"key": "country=Albania"}],
                "segmentInsightsBlackList": {},
                "showBizTags": False,
                "showInCRM": False,
                "showNotInCRM": False
            }
            mixer.blend(SFDCConfig, account_id=1, config=json.dumps(config))
            mixer.blend(IndicatorWhiteList, indicator="country=Albania", display_name="country Albania")

            resp = self.client.get('/sfdc_app/salesforce_config?account_id=1')
            self.assertEqual(resp.status_code, 200)
            result = json.loads(resp.data).get("data")
            self.assertNotEqual(result, None)

    def test_user_preference_api(self):

        with service_app.app_context():
            user = User.get_user_by_email("test@everstring.com")

            data = {
                "email": "test@everstring.com",
                "appName": "sfdc",
                "preference": {
                    "haha": ["123456", "etfdagtfda"],
                    "hehe": ["654321", "dfagdasrt"],
                    "heihei": ["fhdsagh", "dfadgtet"],
                    "houhou": {"hhhh": "hdahfh"}
                }
            }

            resp = self.client.post(
                '/sfdc_app/users/%s/preference' % user.id,
                data=json.dumps(data),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            resp = self.client.get('/sfdc_app/users/%s/preference?appName=sfdc' % user.id)
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 4)
            self.assertEqual(len(data.get("haha")), 2)
            self.assertEqual(len(data.get("houhou")), 1)

            resp = self.client.get('/sfdc_app/preferences/get_by_email?appName=sfdc&email=%s' % user.email)
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 4)
            self.assertEqual(len(data.get("haha")), 2)
            self.assertEqual(len(data.get("houhou")), 1)


def get_response(keys):
    result = {}
    for key in keys:
        result[key] = key
    return result


def get_mock_token():
    result = SFDCToken(1, 1, CRMToken.SALESFORCE)
    result.access_token = ""
    result.refresh_token = ""
    result.organization_id = ""
    result.instance_url = ""
    return result


class TestSalesforce(AppDataClientBase):

    def setUp(self):
        super(TestSalesforce, self).setUp()
        sfdc_token = SFDCToken("", "", "")
        sfdc_token.token = {"access_token": "access_token", "instance_url": "instance_url"}
        self.ns_salesforce = NSSalesforce(sfdc_token)

        self.simple_sf_instance = mock.MagicMock()

    @patch('ns.model.crm_token.SFDCToken.load_from_crm_token')
    @patch('simple_salesforce.Salesforce.query')
    def test_fetch_accounts_matching_domain(self, sf_query, load_from_crm_token):
        sf_query.return_value = {
            'records': [
                {
                    'Website': 'everstring.com'
                },
                {
                    'Website': 'https://www.everstring.com'
                },
                {
                    'Website': 'www.everstring.com'
                },
                {
                    'Website': 'infer.com'
                }
            ]
        }

        domain = 'everstring.com'
        matching_accounts = fetch_accounts_matching_domain({}, domain)

        self.assertEqual(len(matching_accounts), 3)

    @patch('simple_salesforce.Salesforce.Account.update')
    @patch('simple_salesforce.Salesforce')
    @patch('ns.model.salesforce.fetch_accounts_matching_domain')
    @patch('ns.model.crm_token.SFDCToken.load_from_crm_token')
    def test_upsert_accounts_by_domain(
            self, load_from_crm_token, fetch_accounts_matching_domain, MockSalesforce, update):
        fetch_accounts_matching_domain.return_value = [{
            'Id': 'some_id',
            'Website': 'everstring.com'
        }]

        domain = 'everstring.com'
        upsert_accounts_by_domain({}, domain, 'account', {}, 1, 1)

        # TODO: Get this test working. Trying to verify that update() was called,
        # but the mocked instance is different from the `update` we have here.
        # assert update.called

    def test_redirect_request(self):
        res = self.client.get('/salesforce/integration?isSandbox=true')
        self.assertEqual(res.status_code, 302)

    def test_revoke(self):
        with app.app_context():
            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.current_user, role=role)
            res = self.client.delete('/salesforce/revoke')
            self.assertEqual(res.status_code, 200)
            self.assertEqual(self.current_account.realtime_scoring_object, '[]')

    @patch('ns.model.crm_token.CRMToken.get_sfdc_token')
    def test_integrated(self, mock_token):

        mock_token.return_value = None

        res = self.client.get("/salesforce/integrated")
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.data)
        self.assertFalse(data.get("data"))

        mock_token.return_value = "true_result"

        res = self.client.get("/salesforce/integrated")
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.data)
        self.assertTrue(data.get("data"))

    @patch('ns.controller.app.salesforce.check_sfdc_permission')
    @patch('ns.model.crm_token.CRMToken.create_sfdc_token')
    @patch('ns.model.salesforce.get_user_info')
    @patch('ns.model.salesforce.request_access_token')
    def test_sf_call_back(self, mock_access_token,
                          mock_user_profile, mock_crm_token, check_sfdc_permission):
        with app.app_context():
            user = User.get_user_by_email("test@everstring.com")

            mock_token = {"access_token": "token", "id": "id"}
            mock_access_token.return_value = mock_token
            mock_user_profile.return_value = {"email": "email"}
            mock_crm_token.side_effect = None
            check_sfdc_permission.return_value = 'admin'

            res = self.client.get('/salesforce/call_back?code=test_code&state=' + str(user.id)+',sandbox')

            self.assertEqual(res.status_code, 200)
            mock_access_token.assert_called_once_with("test_code", "SALESFORCE_SANDBOX")
            mock_user_profile.assert_called_once_with("id", "token")
            mock_crm_token.assert_called_once_with(user.account_id, user.id, mock_token, "SALESFORCE_SANDBOX")
            self.assertTrue("email" in mock_token)

    @patch('ns.util.http_client.HTTPClient.post_json')
    def test_request_access_token(self, mock_http_client):
        with app.app_context():
            mock_http_client.return_value = get_response([SFDCToken.ACCESS_TOKEN_KEY])
            result = salesforce.request_access_token(None, CRMToken.SALESFORCE_SANDBOX)

            self.assertTrue(SFDCToken.ACCESS_TOKEN_KEY in result)

    @patch('ns.util.http_client.HTTPClient.post_json')
    def test_request_refresh_token(self, mock_http_client):
        with app.app_context():
            mock_http_client.return_value = get_response([SFDCToken.ACCESS_TOKEN_KEY])

            result = salesforce.request_refresh_token(None, CRMToken.SALESFORCE_SANDBOX)

            self.assertTrue(SFDCToken.ACCESS_TOKEN_KEY in result)

    @patch('ns.util.http_client.HTTPClient.get_json')
    def test_request_profile(self, mock_http_client):
        with app.app_context():
            mock_http_client.return_value = get_response([SFDCToken.EMAIL_KEY])

            result = salesforce.get_user_info("", "")

            self.assertTrue(SFDCToken.EMAIL_KEY in result)

    @patch('ns.model.crm_token.SFDCToken.save')
    @patch('ns.model.salesforce.request_refresh_token')
    @patch('ns.model.crm_token.CRMToken.get_sfdc_token')
    def test_refresh_token(self, mock_get, mock_request, mock_update):

        mock_token = {SFDCToken.REFRESH_TOKEN_KEY: "refresh_token"}

        mock_get.return_value = \
            SFDCToken.load_from_crm_token(CRMToken(account_id=1, user_id=1, crm_type=CRMToken.SALESFORCE,
                                                   token=json.dumps(mock_token)))
        mock_request.return_value = {SFDCToken.ACCESS_TOKEN_KEY: "access_token"}
        mock_update.side_effect = None

        self.assertIn(SFDCToken.REFRESH_TOKEN_KEY, mock_token)
        self.assertNotIn(SFDCToken.ACCESS_TOKEN_KEY, mock_token)

        result = salesforce.refresh_and_update_token(User(id=1))

        self.assertEqual(result.access_token, "access_token")
        self.assertEqual(result.refresh_token, "refresh_token")
        mock_update.assert_called_once_with()

    # @patch('simple_salesforce.Saleforce.query_all')
    def test_query_sf_accounts(self):
        with app.app_context():
            token = get_mock_token()
            client = salesforce.NSSalesforce(token)

            expect_results = [{'Id': '1'}, {'Id': '2'}]

            mock_accounts = [{"Account": {"Id": "1"}}, {"Account": {"Id": "2"}}]
            mock_client = Mock()
            mock_client.query.return_value = {"records": mock_accounts, "done": True}
            client.client = mock_client

            ts = datetime.datetime.utcnow()
            accounts = client.query_sf_accounts(start_date=ts, end_date=ts)

            self.assertEqual(accounts, expect_results)
            mock_client.query.assert_called_once_with(mock.ANY)

    def test_query_contacts_by_accounts(self):
        token = get_mock_token()
        client = salesforce.NSSalesforce(token)

        mock_accounts = ["1", "2"]
        mock_client = Mock()
        mock_client.query_all.return_value = {"records": mock_accounts}
        client.client = mock_client

        contacts = client.query_contacts_by_accounts([])
        self.assertEqual(contacts, [])
        mock_client.query_all.has_no_called()

        contacts = client.query_contacts_by_accounts(['account_id_1', 'account_id_2'])
        self.assertEqual(contacts, mock_accounts)
        mock_client.query_all.assert_called_once_with(mock.ANY)

    @patch('ns.model.salesforce.NSSalesforce.query_contacts_by_accounts')
    def test_fill_contact_for_account(self, mock_fill):
        token = get_mock_token()
        client = salesforce.NSSalesforce(token)

        mock_fill.return_value = [{"AccountId": "account_1", "Email": "test@everstring.com"}]

        accounts = [{"Id": "account_1", "Website": ""}, {"Id": "account_2", "Website": "domain"}]

        accounts = client.fill_website_for_account(accounts)

        for account in accounts:
            if account.get("Id") == "account_1":
                self.assertEqual(account.get("Website"), "everstring.com")

    def test_salesforce_config(self):
        with app.app_context():

            expected_headers = current_app.config.get("SFDC_APP_COMPANY_INSIGHTS_HEADERS")
            # test query
            user = User.get_user_by_email("test@everstring.com")
            account = user.account

            resp = self.client.get('/salesforce/%s/config' % str(account.id))
            self.assertEqual(resp.status_code, 200)
            result = json.loads(resp.data).get("data")
            expected_result = {"enrich_enabled":False, "enrich_score":False, "enrich_firmographics":False, "enrich_start_ts":""}
            expected_result["config"] = SFDCConfig.DEFAULT_CONFIG
            expected_result["config"]["insightHeadersDef"] = expected_headers

            self.assertEqual(result["config"], expected_result["config"])

            # test save
            config = {
                "insightHeaders": ["haha", "hehe"],
                "segmentInsightsBlackList": {},
                "showBizTags": False,
                "showInCRM": False,
                "showNotInCRM": False
            }

            data = {"config": config,
                    "enrich_enabled": False,
                    "enrich_score": False,
                    "enrich_firmographics": False,
                    "enrich_start_ts": 1389177318000
                    }

            resp = self.client.post(
                '/salesforce/%s/config' % str(account.id),
                data=json.dumps(data),
                content_type='application/json')

            self.assertEqual(resp.status_code, 200)

            resp = self.client.get('/salesforce/%s/config' % str(account.id))
            self.assertEqual(resp.status_code, 200)
            result = json.loads(resp.data).get("data")

            # config['insightHeadersDef'] = expected_headers
            expected_result["enrich_start_ts"]= "2014-01-08 10:35:18"
            expected_result["config"] = config
            expected_result["config"]['insightHeadersDef'] = expected_headers
            self.assertEqual(result, expected_result)

    def test_get_package_link(self):
        res = self.client.get("/salesforce/package_link?isSandbox=true")
        self.assertEqual(res.status_code, 200)
        self.assertNotEqual(res.data, "")

    def test_get_package_guide_link(self):
        res = self.client.get("/salesforce/package_guide_link")
        self.assertEqual(res.status_code, 302)
        self.assertNotEqual(res.data, "")

    def test_get_crm_type(self):
        with app.app_context():
            raw_token = {
                SFDCToken.ACCESS_TOKEN_KEY: "access_3",
                SFDCToken.INSTANCE_URL_KEY: "instance_3",
                SFDCToken.REFRESH_TOKEN_KEY: "refresh_3",
                SFDCToken.ORGANIZATION_ID_KEY: "orgid_3",
                SFDCToken.EMAIL_KEY: "email_3",
                SFDCToken.ROLE_KEY: "System Administrator"
            }

            token = mixer.blend(CRMToken, id=1, account_id=1, user_id=1,
                                crm_type=CRMToken.SALESFORCE_SANDBOX, role="System Administrator",
                                token=json.dumps(raw_token))
            res = self.client.get("/salesforce/crm_type")
            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)['data']
            self.assertEqual(data, CRMToken.SALESFORCE_SANDBOX)

    @patch('ns.util.company_db_client.CompanyDBClient.find_in_crm')
    def test_find_in_crm(self, request_mock):
        with app.app_context():
            in_crm_data = {
                    "example1.com": True,
                    "example2.com": True,
                    "example3.com": False
            }
            request_mock.return_value = in_crm_data
            mixer.blend(CRMToken, id=1, account_id=1, user_id=1,
                    crm_type=CRMToken.SALESFORCE_SANDBOX, role="System Administrator")
            res = self.client.get("/salesforce/find_in_crm?accountId=%d&findOn=lead,account&domains=%s"
                % (1, ','.join(in_crm_data.keys())))
            data = json.loads(res.data).get('data')
            self.assertEqual(data, in_crm_data)


    @patch('ns.model.salesforce.upsert_accounts_by_domain')
    @patch('ns.model.salesforce.fetch_accounts_matching_domain')
    @patch('ns.model.publish_field_mapping.PublishFieldMapping.get_target_available_fields')
    @patch('ns.model.salesforce._track_company_export')
    @patch('ns.model.salesforce.NSSalesforce.add_sfdc_object')
    @patch('ns.model.crm_token.SFDCToken.get_sfdc_crm_token')
    @patch('ns.model.sfdc_config.SFDCConfig.config_dict')
    @patch('ns.model.publish_field_mapping.PublishFieldMapping.get_by_filters')
    @patch('ns.util.company_db_client.CompanyDBClient.get_publish_data_for_domains')
    def test_add_sfdc_object(self,
                             mock_get_publish_data,
                             mock_get_field_mapping,
                             mock_config_dict,
                             get_sfdc_crm_token,
                             mock_add_sfdc_object,
                             mock_track_company_export,
                             get_salesforce_fields,
                             fetch_accounts_matching_domain,
                             upsert_accounts_by_domain):
        get_salesforce_fields.side_effect = [[], []]

        with app.app_context():
            acct1 = mixer.blend(Account)
            acct_quota = mixer.blend(AccountQuota,
                                     account=acct1,
                                     csv_company_limit=1,
                                     exposed_csv_insight_ids='[1, 2, 3]')
            mock_config_dict.return_value = {
                "insightHeadersCustom": [{"key": "country=Albania"}],
                "segmentInsightsBlackList": {},
                "showBizTags": False,
                "allowCRMAdd": True,
                "showNotInCRM": True
            }

            mock_get_publish_data.return_value = {
                'test.com': CompanyPublishData({
                    'domain': 'test.com',
                    'companyName': 'Test, Inc.',
                    'id': 111
                })
            }

            mock_get_field_mapping.return_value = []

            mock_track_company_export.side_effect = None
            crm_token1 = mixer.blend(CRMToken, account_id=acct1.id,
                                     crm_type=CRMToken.SALESFORCE, token=json.dumps(token),
                                     organization_id=1)
            acct2 = mixer.blend(Account)
            crm_token2 = mixer.blend(CRMToken, account_id=acct2.id,
                                     crm_type=CRMToken.SALESFORCE, token=json.dumps(token),
                                     organization_id=1)
            get_sfdc_crm_token.side_effect = [crm_token1, crm_token2]
            # sfdc_client = salesforce.NSSalesforce(SFDCToken.load_from_crm_token(get_sfdc_crm_token))
            mock_add_sfdc_object.return_value = "test"

            resp = self.client.post("/salesforce/add_sfdc_obj",
                                    data=json.dumps({
                                        "account_id": acct1.id,
                                        "domain": "test.com"
                                    }),
                                    content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200)

    @patch('ns.model.salesforce.NSSalesforce.query')
    def test_check_objects_permission(self, query):
        query_result = {}
        query.return_value = query_result
        result = check_objects_permission(self.ns_salesforce, "profile_id")
        self.assertEqual(4, len(result))

    def test_check_profile(self):
        permissions = ["PermissionsApiEnabled", "PermissionsCustomizeApplication"]
        status = [True, True, True]
        profile = dict(zip(permissions, status))
        result = check_profile(profile)
        self.assertEqual(0, len(result))
        self.assertEqual(1, len(check_profile({})))
        self.assertEqual(2, len(check_profile(dict(zip(permissions, [False, False])))))

    @patch('ns.model.salesforce.NSSalesforce.query')
    def test_check_visualforce_pages(self, query):
        query.return_value = {}
        result = check_visualforce_pages(self.ns_salesforce)
        self.assertEqual(1, len(result[1]))
        query.return_value = {"records": [{"Id": "1"}, {"Id": "2"}, {"Id": "3"}, {"Id": "4"}]}
        check_visualforce_pages(self.ns_salesforce)

    @patch('ns.model.salesforce.NSSalesforce.query')
    def test_check_entities_permission(self, query):
        query.return_value = {"records": [{"Id": "1"}]}
        result = check_entities_permission(self.ns_salesforce, ["name1"], "")
        self.assertEqual(0, len(result))
        query.return_value = {"records": []}
        result = check_entities_permission(self.ns_salesforce, ["name1"], "")
        self.assertEqual(1, len(result))
