import unittest

# from ns.controller.public_api_app import public_api_app, public_api
from ns.controller.public_api import public_api_app, company
from ns.model.publish_field_mapping import CompanyPublishData


class TestFormatResponse(unittest.TestCase):
    def test_format_data_for_response(self):
        with public_api_app.app_context():
            identifying_data_with_meta = [
                {
                    'matchedDomain': u'everstring.com',
                    'matchedOn': 'website',
                    'website': u'everstring.com'
                },
                {
                    'matchedDomain': u'google.com',
                    'matchedOn': 'website',
                    'website': u'google.com'
                },
                {
                    'matchedDomain': u'amazon.com',
                    'matchedOn': 'website',
                    'website': u'amazon.com'
                }
            ]

            # Amazon's id
            company_ids_exceeding_quota = [1076975694]

            company_publish_data = {
                'everstring.com': CompanyPublishData({
                    u'companyName': u'EverString',
                    u'domain': u'everstring.com',
                    u'employeeSize': 125,
                    u'id': 1073986465,
                    u'industry': u'Computer Software',
                    u'revenue': 13750,
                    u'scores': {u'100': 0.0, u'97': 0.0},
                    u'state': u'California'
                }),
                'google.com': CompanyPublishData({
                    u'companyName': u'Google',
                    u'domain': u'google.com',
                    u'employeeSize': 53600,
                    u'id': 1074028800,
                    u'industry': u'Internet',
                    u'revenue': 71487000,
                    u'scores': {u'100': 0.0, u'97': 0.0},
                    u'state': u'California'
                }),
                'amazon.com': CompanyPublishData({
                    u'companyName': u'Amazon.com, Inc.',
                    u'domain': u'amazon.com',
                    u'employeeSize': 230800,
                    u'id': 1076975694,
                    u'industry': u'Internet',
                    u'revenue': 107010000,
                    u'scores': {u'100': 0.0, u'97': 0.0},
                    u'state': u'Washington'
                })
            }

            actual = company.format_data_for_response(
                identifying_data_with_meta,
                company_publish_data,
                company_ids_exceeding_quota
            )

            # Just make sure our output has the same # of items as input
            self.assertEqual(len(actual), 3)

            # Make sure that we've preserved the original input order.
            # This also lightly tests that our response is in an expected format.
            # I don't want to check the full exact response format in this test because
            # that might change a lot and it'll be annoying to maintain here.
            domain_of_first_item = actual[0]['data']['domain']
            self.assertEqual(domain_of_first_item, u'everstring.com')
            domain_of_second_item = actual[1]['data']['domain']
            self.assertEqual(domain_of_second_item, u'google.com')

            # Make sure that Amazon's data is None, respecting company_ids_exceeding_quota
            amazon_data = actual[2]['data']
            self.assertEqual(amazon_data, None)

    def test_advanced_insights(self):
        advanced_insights = [
            {
                "value": "Yes",
                "name": "exact_name_1",
                "domain": "everstring.com"
            },
            {
                "value": "High",
                "name": "exact_name_2",
                "domain": "everstring.com"
            },
            {
                "value": "No",
                "name": "exact_name_3",
                "domain": "everstring.com"
            }
        ]
        advanced_insights_map = {
            "name_1": "exact_name_1",
            "name_2": "exact_name_2",
            "name_3": "exact_name_3"
        }

        actual = company.format_advanced_insights(
            advanced_insights, advanced_insights_map)

        expected = {
            "name_1": {
                "value": True,
                "name": "exact_name_1"
            },
            "name_2": {
                "value": "High",
                "name": "exact_name_2"
            },
            "name_3": {
                "value": False,
                "name": "exact_name_3"
            }
        }

        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
