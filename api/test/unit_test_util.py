
def check_db_type(db):
    assert db.engine.driver == 'pysqlite', "UT must run on pysqlite DB"
