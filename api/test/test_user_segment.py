import unittest

from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model import db
from ns.model.source import Source
from ns.model.account import Account
from ns.model.user import User
from ns.model.segment import Segment
from ns.model.user_segment import UserSegment
from base_case import AppTestBase


class TestUserSegment(AppTestBase):

    def setUp(self):
        super(TestUserSegment, self).setUp()
        with app.app_context():
            self.account = mixer.blend(Account)

    def test_modify_mapping(self):
        with app.app_context():

            user1 = mixer.blend(User, account_id=self.account.id)
            user2 = mixer.blend(User, account_id=self.account.id)
            user_ids = [user1.id, user2.id]

            seg1 = mixer.blend(Segment, owner=user1)
            seg2 = mixer.blend(Segment, owner=user2)
            segment_ids = [seg1.id, seg2.id]

            UserSegment._add_permission_mapping(segment_ids, user_ids, UserSegment.READ_PERMISSION)

            self.assertIsNotNone(UserSegment.get_by_user_segment(user1, seg1))
            self.assertIsNotNone(UserSegment.get_by_user_segment(user1, seg2))
            self.assertIsNotNone(UserSegment.get_by_user_segment(user2, seg1))
            self.assertIsNotNone(UserSegment.get_by_user_segment(user2, seg2))

            UserSegment._remove_permission_mapping(segment_ids, user_ids, UserSegment.READ_PERMISSION)

            self.assertIsNone(UserSegment.get_by_user_segment(user1, seg1))
            self.assertIsNone(UserSegment.get_by_user_segment(user1, seg2))
            self.assertIsNone(UserSegment.get_by_user_segment(user2, seg1))
            self.assertIsNone(UserSegment.get_by_user_segment(user2, seg2))


