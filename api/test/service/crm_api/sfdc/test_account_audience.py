from test.http_client_base import ServiceHttpClientBase
from ns.controller.service import app
from mock import patch
import json


class TestAccountAudience(ServiceHttpClientBase):

    @patch('ns.controller.service.crm_api.sfdc.utils.operate_sfdc')
    def test_insert_account_audiences(self, operate_sfdc_fn):
        operate_sfdc_fn.return_value = ["sfdcid1", "sfdcid2"]
        url = '/crm/sfdc/account_audiences'
        with app.app_context():
            resp = self.client.post(url, data=json.dumps({'token': "111", "instance_url": "http://na30.salesforce.com",
                                                          "data": [{"accountId": 124}, {"accountId": 123}]}),
                                    content_type="application/json")
            rs = json.loads(resp.data)
            self.assertListEqual(rs.get("sfdc_object_ids"), ["sfdcid1", "sfdcid2"])