from test.http_client_base import ServiceHttpClientBase
from ns.controller.service import app
from mock import patch
import json


class TestContactAudience(ServiceHttpClientBase):

    @patch('ns.controller.service.crm_api.sfdc.utils.operate_sfdc')
    def test_insert_contact_audiences(self, operate_sfdc_fn):
        operate_sfdc_fn.return_value = ["sfdcid1", "sfdcid2"]
        url = '/crm/sfdc/contact_audiences'
        with app.app_context():
            resp = self.client.post(url, data=json.dumps({'token': "111", "instance_url": "http://na30.salesforce.com",
                                                          "data": [{"contactId": 124}, {"contactId": 123}]}),
                                    content_type="application/json")
            rs = json.loads(resp.data)
            self.assertListEqual(rs.get("sfdc_object_ids"), ["sfdcid1", "sfdcid2"])