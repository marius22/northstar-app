import unittest
from mock import patch, PropertyMock
from ns.controller.app.permission import check_change_segment_owner_permission
from ns.model.segment import Segment
from ns.model.role import Role
from ns.model.user import User
from werkzeug import exceptions


class TestPermission(unittest.TestCase):

    @patch('ns.model.user.User.role_names', new_callable=PropertyMock)
    def test_check_change_segment_owner_permission(self, mock_role_names):

        # owner
        segment = Segment(owner_id=1)
        user = User(id=1)

        check_change_segment_owner_permission(user, segment)

        # es admin
        segment = Segment(owner_id=1)
        user = User(id=2)
        mock_role_names.return_value = [Role.ES_ADMIN]
        check_change_segment_owner_permission(user, segment)

        # account admin, same account
        segment = Segment(owner_id=1)
        segment.owner = User(id=3, account_id=1)
        user = User(id=2, account_id=1)
        mock_role_names.return_value = [Role.ACCOUNT_ADMIN]

        check_change_segment_owner_permission(user, segment)

        # account admin, same account
        segment = Segment(owner_id=1)
        segment.owner = User(id=3, account_id=1)
        user = User(id=2, account_id=1)
        mock_role_names.return_value = [Role.ACCOUNT_ADMIN]

        check_change_segment_owner_permission(user, segment)

        # account admin, different account
        with self.assertRaises(exceptions.Forbidden):
            segment = Segment(owner_id=1)
            segment.owner = User(id=3, account_id=1)
            user = User(id=2, account_id=2)
            mock_role_names.return_value = [Role.ACCOUNT_ADMIN]

            check_change_segment_owner_permission(user, segment)

        # regular user and not owner
        with self.assertRaises(exceptions.Forbidden):
            segment = Segment(owner_id=1)
            user = User(id=2, account_id=2)
            mock_role_names.return_value = [Role.ACCOUNT_USER]

            check_change_segment_owner_permission(user, segment)

