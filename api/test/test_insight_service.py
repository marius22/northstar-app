import unittest
from data_http_client_base import ServiceDataClientBase
from ns.controller.service import app
import json
from mixer.backend.flask import mixer
from ns.model.account import Account
from ns.model.job import Job
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.util.company_db_client import CompanyDBClient
import httpretty
import re


class TestInsightsService(ServiceDataClientBase):

    def setUp(self):
        super(TestInsightsService, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    def test_save_account_insights(self):

        with app.app_context():
            account = mixer.blend(Account)
            job = mixer.blend(Job)

            url = "/jobs/123/accountInsights/summary"
            data = {
                "job_id": 123,
                "insights": [
                    {
                        "category": "tech",
                        "indicator": "java",
                        "displayValue": "Java"
                    },
                    {
                        "category": "tech",
                        "indicator": "python",
                        "displayValue": "Python"
                    },
                    {
                        "category": "industry",
                        "indicator": "industry_1",
                        "displayValue": "Industry_1"
                    }
                ]
            }
            res = self.client.post(url, data=json.dumps(data), content_type='application/json')
            self.assertEqual(res.status_code, 200)
            cat_size = IndicatorWhiteList.get_indicator_categories()
            self.assertEqual(len(cat_size), 2)

    @httpretty.activate
    def test_domain_indicators(self):
        with app.app_context():
            mixer.blend(IndicatorWhiteList, category="category",
                        indicator="indicator_1", display_name="displayName")

            httpretty.register_uri(httpretty.GET,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": ["indicator_1", "indicator_2"]
                                   }),
                                   content_type="application/json")
            resp = self.client.post(
                "/sfdc_app/domain_indicators",
                data=json.dumps(
                    {
                        "domain": "test_domain.com",
                        "indicators": ["indicator_1"]
                    }
                ),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            result = json.loads(resp.data).get("data")

            self.assertEqual(len(result), 1)


if __name__ == '__main__':
    unittest.main()
