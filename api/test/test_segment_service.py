# -*- coding: utf-8 -*-
import json
import re
import datetime
from mixer.backend.flask import mixer
from mock import patch, PropertyMock
from ns.controller.service import app
from ns.model.account import Account
from ns.model.job import Job
from ns.model.recommendation import Recommendation
from ns.model.seed import Seed
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from base_case import ServiceTestBase
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.model.segment import Segment
from ns.model.sfdc_config import SFDCConfig
import httpretty
from ns.util.company_db_client import CompanyDBClient
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.model.model import Model


class TestSegmentService(ServiceTestBase):

    def setUp(self):
        super(TestSegmentService, self).setUp()
        with app.app_context():
            acct = mixer.blend(Account)
            self.current_account = acct
            mixer.blend(AccountQuota, account=acct,
                        exposed_ui_insight_ids="[1,2]",
                        exposed_csv_insight_ids="[1,2]",
                        expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=10),
                        csv_company_limit=9999)

            mixer.blend(CompanyInsight, id=1, display_name="companyId")
            mixer.blend(CompanyInsight, id=2, display_name="companyName")

            self.user = mixer.blend(User, account=acct, email='test@everstring.com', status=User.ACTIVE)
            role = mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            mixer.blend(UserRole, user=self.user, role=role)
            self.sgmt = mixer.blend(Segment, owner=self.user,
                                    last_run_ts=datetime.datetime.now(),
                                    is_deleted=False,
                                    status=Segment.COMPLETED)
            self.seed = mixer.blend(Seed, segment=self.sgmt)
            self.client = app.test_client()

    @patch('ns.controller.service.segment.upload_to_s3')
    def test_export_company_csv(self, s3_mock):
        with app.app_context():
            s3_mock.return_value = 's3_key', 's3_url'

            job = mixer.blend(Job, segment_id=self.sgmt.id, type=Job.SEGMENT, seed_id=self.seed.id, status='COMPLETED')
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=101, segment_id=job.segment_id)
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=102, segment_id=job.segment_id)

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url, query_string={"format": "csv", "userId": 1})

            res_dict = json.loads(resp.data)['data']
            self.assertEqual(res_dict['company_csv_s3_key'], 's3_key')
            self.assertEqual(res_dict['company_csv_s3_url'], 's3_url')

    @patch('ns.controller.service.segment.upload_to_s3')
    def test_export_company_csv_by_sector(self, s3_mock):
        with app.app_context():
            s3_mock.return_value = 's3_key', 's3_url'

            job = mixer.blend(Job, segment_id=self.sgmt.id, type=Job.SEGMENT, seed_id=self.seed.id, status='COMPLETED')
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=101, segment_id=job.segment_id,
                        similar_c_score=1, similar_t_score=4, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=102, segment_id=job.segment_id,
                        similar_c_score=1, similar_t_score=4, similar_d_score=4)

            url = '/segments/%d/sector_companies' % self.sgmt.id
            # &dedupe=0&format=email&limit=100&sectors=1&titles=
            resp = self.client.get(url,
                                   query_string={
                                       "format": "csv",
                                       "userId": 1,
                                       "sectors": "1,2",
                                       "contactLimitPerCompany": 1,
                                       "dedupe": 1,
                                       "limit": 100,
                                   })

            res_dict = json.loads(resp.data)['data']
            self.assertEqual(res_dict['company_csv_s3_key'], 's3_key')
            self.assertEqual(res_dict['company_csv_s3_url'], 's3_url')

    @httpretty.activate
    @patch('ns.util.company_db_client.CompanyDBClient.get_account_fit_score')
    def test_get_owner_segments(self, mock_db):

        mock_db.return_value = {"test.com": 0.5}

        with app.app_context():
            account = self.current_account
            user = self.user

            mixer.blend(Job, account_id=account.id, type=Job.ACCOUNT_FIT, status=Job.COMPLETED)

            segment_1 = mixer.blend(Segment, owner=user,
                                    last_run_ts=datetime.datetime.now(),
                                    is_deleted=False,
                                    status=Segment.COMPLETED)
            segment_2 = mixer.blend(Segment, owner=user,
                                    last_run_ts=datetime.datetime.now(),
                                    is_deleted=False,
                                    status=Segment.COMPLETED)

            job_1 = mixer.blend(Job, segment_id=segment_1.id, type=Job.SEGMENT, status=Job.COMPLETED)
            job_2 = mixer.blend(Job, segment_id=segment_2.id, type=Job.SEGMENT, status=Job.COMPLETED)

            recom_1 = mixer.blend(Recommendation,
                                  job_id=job_1.id, company_id=1, segment_id=segment_1.id, domain="test.com", score=0.1)
            recom_2 = mixer.blend(Recommendation,
                                  job_id=job_2.id, company_id=1, segment_id=segment_2.id, domain="test.com", score=0.4)

            # test without black list
            resp = self.client.get("/sfdc_app/owner_segments?account_id=%s&domain=test.com" % account.id)

            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 3)
            # assert the first is the highest score segment
            self.assertEqual(data[0].get("id"), segment_2.id)
            self.assertIsNone(data[2].get("id"))

    @httpretty.activate
    @patch('ns.model.account.Account.owned_segments', new_callable=PropertyMock)
    @patch('ns.util.company_db_client.CompanyDBClient.get_account_fit_score')
    def test_get_owner_segments_without_segment_only(self, mock_db, mock_account):

        class MyMock(object):
            def all(self):
                return []

        mock_account.return_value = MyMock()
        mock_db.return_value = {"test.com": 0.5}

        with app.app_context():
            user = self.user
            account = self.current_account
            mixer.blend(Job, account_id=account.id, type=Job.ACCOUNT_FIT, status=Job.COMPLETED)

            Segment.get_by_owner(user.id)
            resp = self.client.get("/sfdc_app/owner_segments?account_id=%s&domain=test.com"
                                   % self.current_account.id)
            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 1)
            self.assertEqual(data[0].get("fitScore"), 0.5)
            self.assertIsNone(data[0].get("id"))

    @httpretty.activate
    def test_segment_insights(self):
        with app.app_context():
            account = self.current_account
            user = self.user
            segment_1 = mixer.blend(Segment, owner=user,
                                    last_run_ts=datetime.datetime.now(),
                                    is_deleted=False,
                                    status=Segment.COMPLETED)

            mock_data = [
                    {
                        "coef": 0.145631450561491,
                        "indicator": "python"
                    },
                    {
                        "coef": 0.8420787427179901,
                        "indicator": "java"
                    },
                    {
                        "coef": 0.7420787427179901,
                        "indicator": "mysql"
                    },
                    {
                        "coef": 0.990787427179901,
                        "indicator": "it"
                    }
                ]

            client = CompanyDBClient.instance()

            httpretty.register_uri(httpretty.GET,
                                   re.compile(client.base_common_url),
                                   body=json.dumps({
                                       "data": mock_data
                                   }),
                                   content_type="application/json")

            # mock white list and black list
            mixer.blend(SFDCConfig, account_id=account.id,
                        config=json.dumps({
                            "segmentInsightsBlackList": [
                                {"key": "java", "value": "Java"}
                            ]
                        }))
            mixer.blend(IndicatorWhiteList, category="tech", indicator="java", display_name="Java")
            mixer.blend(IndicatorWhiteList, category="tech", indicator="python", display_name="Python")
            mixer.blend(IndicatorWhiteList, category="industry", indicator="it", display_name="It")

            resp = self.client.get("/sfdc_app/segment_indicators?segment_id=%s&domain=test.com" % segment_1.id)

            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)
            self.assertItemsEqual(
                {
                    "category": "tech",
                    "indicator": "python",
                    "displayName": "Python"
                }, data[1])
            self.assertItemsEqual(
                {
                    "category": "tech",
                    "indicator": "it",
                    "displayName": "It"
                }, data[0])

    @httpretty.activate
    def test_account_insights(self):

        with app.app_context():
            account = self.current_account
            mixer.blend(Model, account_id=account.id, type='feature_analysis', status='COMPLETED')

            mock_data = [
                    {
                        "coef": 0.145631450561491,
                        "indicator": "python"
                    },
                    {
                        "coef": 0.8420787427179901,
                        "indicator": "java"
                    },
                    {
                        "coef": 0.7420787427179901,
                        "indicator": "mysql"
                    },
                    {
                        "coef": 0.990787427179901,
                        "indicator": "it"
                    }
                ]
            client = CompanyDBClient.instance()

            httpretty.register_uri(httpretty.GET,
                                   re.compile(client.base_common_url),
                                   body=json.dumps({
                                       "data": mock_data
                                   }),
                                   content_type="application/json")

            # mock white list and black list
            mixer.blend(SFDCConfig, account_id=account.id,
                        config=json.dumps({
                            "segmentInsightsBlackList": [
                                {"key": "java", "value": "Java"}
                            ]
                        }))
            mixer.blend(IndicatorWhiteList, category="tech", indicator="java", display_name="Java")
            mixer.blend(IndicatorWhiteList, category="tech", indicator="python", display_name="Python")
            mixer.blend(IndicatorWhiteList, category="industry", indicator="it", display_name="It")

            resp = self.client.get("/sfdc_app/account_indicators?account_id=%s&domain=test.com" % account.id)

            self.assertEqual(resp.status_code, 200)
            data = json.loads(resp.data).get("data")
            self.assertEqual(len(data), 2)
            self.assertItemsEqual(
                {
                    "category": "tech",
                    "indicator": "python",
                    "displayName": "Python"
                }, data[1])
            self.assertItemsEqual(
                {
                    "category": "tech",
                    "indicator": "it",
                    "displayName": "It"
                }, data[0])
