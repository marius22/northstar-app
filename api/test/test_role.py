
import unittest
from ns.model.role import Role


class TestRole(unittest.TestCase):

    def test_sort(self):
        r1 = Role(name=Role.TRIAL_USER)
        result1 = Role.get_most_prior([r1])
        self.assertEqual(result1.name, Role.TRIAL_USER)

        r2 = Role(name=Role.ACCOUNT_ADMIN)
        result2 = Role.get_most_prior([r1, r2])
        self.assertEqual(result2.name, Role.ACCOUNT_ADMIN)

        r3 = Role(name=Role.ES_SUPER_ADMIN)
        result3 = Role.get_most_prior([r1, r2, r3])
        self.assertEqual(result3.name, Role.ES_SUPER_ADMIN)

    def test_priority(self):
        r1 = Role(name=Role.ACCOUNT_ADMIN)
        r2 = Role(name=Role.ES_ADMIN)
        self.assertFalse(r2.more_prior_than(r2))
        self.assertTrue(r2.more_prior_than(r1))

        self.assertTrue(r2.prior_or_same_as(r2))
        self.assertTrue(r2.prior_or_same_as(r1))
        self.assertFalse(r2.more_prior_than(None))
        self.assertFalse(r2.prior_or_same_as(None))
