import json
from mock import patch
import httpretty
from http_client_base import AppHttpClientBase
import re
from ns.controller.app import app
from ns.util.company_db_client import CompanyDBClient
from ns.model.seed_company import SeedCompany


class TestSeedCompany(AppHttpClientBase):

    def setUp(self):
        super(TestSeedCompany, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    @patch('ns.util.redshift_client.RedshiftClient.insert')
    @httpretty.activate
    def test_save_seed_companies(self, mock_insert):
        mock_insert.side_effect = None
        with app.app_context():
            httpretty.register_uri(httpretty.POST,
                                   re.compile(self.company_db_client.base_common_url),
                                   body=json.dumps({
                                       "data": {"companies": [
                                           {"domain": "domain1.com", "name": "company_name_3"}
                                       ]
                                       }
                                   }),
                                   content_type="application/json")

            model_seed_id = 1
            seed_companies = [
                SeedCompany(domain="domain1.com"),
                SeedCompany(domain="www.domain2.com"),
                SeedCompany(domain="www.domain3.com"),
                SeedCompany(domain="www.domain3.com"),
                SeedCompany(domain="http://www.domain5.com"),
                SeedCompany(domain=""),
                SeedCompany(domain="www.DOMAIN7.com"),
                SeedCompany(domain="https://www.domain6.com",
                            sf_account_id="test_account_id",
                            sf_account_name="test_account_name")
            ]
            # de-dupe
            result = SeedCompany.save_seed_companies(model_seed_id, seed_companies)
            self.assertEqual(len(result), 7)
            domain_3_list = [item for item in result if item.domain == "domain3.com"]
            self.assertEqual(len(domain_3_list), 1)

            mappings = {item.domain: item for item in result}
            # with company_name
            self.assertEqual(mappings.get("domain1.com").company_name, "company_name_3")
            # with default company_name
            self.assertEqual(mappings.get("domain2.com").company_name, "")
            # re-write to lower case
            self.assertIsNotNone(mappings.get("domain7.com"))
            # trim http
            self.assertIsNotNone(mappings.get("domain5.com"))
            # with sf_account_id
            self.assertEqual(mappings.get("domain6.com").sf_account_id, "test_account_id")
            unknown_domain_list = [item for item in result
                                   if item.domain.startswith(SeedCompany.UNKNOWN_DOMAIN_PREFIX)]
            self.assertEqual(len(unknown_domain_list), 1)

