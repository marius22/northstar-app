import json
import unittest
import re
import httpretty
from flask import current_app

from ns.util.sf_data_service import SFDataServiceClient
from ns.controller.app import app


class TestSFDataServiceClient(unittest.TestCase):

    def setUp(self):
        with app.app_context():
            self.base_url = current_app.config.get('COMMON_SERVICE_BASE_URL')
            self.client = SFDataServiceClient.instance()
            self.org_id = "test_org_id"
            self.domains = ['a.com', 'b.com', 'c.com']
            self.companies = ['e', 'f']
            self.emails = ['a@a.com', 'b@a.com']

    @httpretty.activate
    def test_query_accounts(self):
        data = {'data': [
            {'crmId': 'e',
             'orgId': 'f',
             'name': 'f',
             'website': 'e.com',
             'phone': '123',
             'domain': 'e.com'}, ]
        }
        url = "{0}/crm/sfdc/{1}/accounts".format(self.base_url, self.org_id)
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        accounts = self.client.query_sf_accounts(self.org_id, self.domains, self.companies)
        self.assertListEqual(data['data'], accounts)

    @httpretty.activate
    def test_query_leads(self):
        data = {'data': [
            {'crmId': 'e',
             'orgId': 'f',
             'firstName': 'f',
             'lastName': 'g',
             'email': 'a@g.com',
             'website': 'e.com',
             'company': 'e',
             'phone': '123',
             'domain': 'e.com',
             'title': '1',
             'mobilePhone': '2'}, ]

        }

        url = "{0}/crm/sfdc/{1}/leads".format(self.base_url, self.org_id)
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        accounts = self.client.query_sf_leads(self.org_id, self.domains, self.companies)
        self.assertListEqual(data['data'], accounts)

    @httpretty.activate
    def test_query_accounts_by_contact(self):
        data = {'data': [
            {'crmId': 'e',
             'orgId': 'f',
             'name': 'f',
             'website': 'e.com',
             'phone': '123',
             'domain': 'e.com'}, ]
        }
        url = "{0}/crm/sfdc/{1}/contacts".format(self.base_url, self.org_id)
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        accounts = self.client.query_sf_accounts_by_contact(self.org_id, self.domains)
        self.assertListEqual(data['data'], accounts)

    @httpretty.activate
    def test_query_leads_by_email(self):
        data = {'data': [
            {'crmId': 'e',
             'orgId': 'f',
             'firstName': 'f',
             'lastName': 'g',
             'email': 'a@g.com',
             'website': 'e.com',
             'company': 'e',
             'phone': '123',
             'domain': 'e.com',
             'title': '1',
             'mobilePhone': '2'}, ]
        }

        url = "{0}/crm/sfdc/{1}/leads".format(self.base_url, self.org_id)
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        leads = self.client.query_sf_leads_by_email(self.org_id, self.emails)
        self.assertListEqual(data['data'], leads)

    @httpretty.activate
    def test_query_contacts_by_email(self):
        data = {'data': [
            {'crmId': 'e',
             'orgId': 'f',
             'firstName': 'f',
             'lastName': 'g',
             'email': 'a@g.com',
             'accountId': 'e',
             'phone': '123',
             'domain': 'e.com',
             'title': '1',
             'mobilePhone': '2'}, ]
        }

        url = "{0}/crm/sfdc/{1}/contacts".format(self.base_url, self.org_id)
        httpretty.register_uri(httpretty.POST, re.compile(url),
                               body=json.dumps(data),
                               status=200)
        contacts = self.client.query_sf_contacts_by_email(self.org_id, self.emails)
        self.assertListEqual(data['data'], contacts)
