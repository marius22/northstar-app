import datetime
import httplib
import json
import random
import re

import httpretty as httpretty
from mixer.backend.flask import mixer
from mock import patch, Mock, mock, PropertyMock, ANY
from ns.controller.app import app
from ns.model import db
from ns.model.account import Account
from ns.model.account_quota import AccountQuota
from ns.model.crm_token import CRMToken
from ns.model.expansion import Expansion
from ns.model.job import Job
from ns.model.qualification import Qualification
from ns.model.recommendation import Recommendation
from ns.model.role import Role
from ns.model.seed import Seed
from ns.model.seed_candidate import SeedCandidate
from ns.model.segment import Segment
from ns.model.sf_pull_request import SFPullRequest
from ns.model.source import Source
from ns.model.tmp_segment_stage import TmpSegmentStage
from ns.model.user import User
from ns.model.user_exported_company import UserExportedCompany
from ns.model.user_role import UserRole
from ns.model.user_segment import UserSegment
from ns.util.company_db_client import CompanyDBClient
from ns.model.segment_metric import SegmentMetric
from data_http_client_base import AppDataClientBase


class TestSegment(AppDataClientBase):

    def test_create_segment(self):
        data = {}
        res = self.client.post('/segments', data=json.dumps(data), content_type='application/json')
        self.assertEqual(res.status_code, httplib.UNPROCESSABLE_ENTITY)

    def test_create_segment_ok(self):
        sgmt_name = 'Test Segment'
        data = {'segmentName': sgmt_name}
        res = self.client.post('/segments', data=json.dumps(data), content_type='application/json')

        self.assertIn('segmentId', json.loads(res.data)['data'])

        res = self.client.get('/segments/' + str(json.loads(res.data)['data']['segmentId']))
        self.assertEqual('Test Segment', json.loads(res.data)['data']['segmentName'])

    def test_create_segment_quota_exceed(self):
        with app.app_context():

            valid_status = [Segment.DRAFT, Segment.IN_PROGRESS, Segment.COMPLETED]

            quota = self.current_account.account_quota.segment_cnt
            mixer.cycle(quota).blend(Segment, name=mixer.sequence("foo_{0}"),
                                     owner=self.current_user, is_deleted=False,
                                     status=random.choice(valid_status))

            data = {'segmentName': 'baz'}
            res = self.client.post('/segments', data=json.dumps(data),
                                   content_type='application/json')

            self.assertIn('Insufficient segment quota', res.data)

    def test_shared_account_quota(self):
        """
        Two users(Mr. Foo and Mr. Buz) belongs to a account,
         account has segment quota = 2, which has been used up by Mr.Foo
         Mr. Buz should fail to create a new segment due to the quota
         limit, even though he hasn't create any segment yet.

        :return:
        """

        with app.app_context():
            # TODO: make the fixure preparation separate with the preceeding status
            foo = mixer.blend(User, account=self.current_account)
            quota = self.current_account.account_quota
            quota.segment_cnt = 2

            # Mr.Foo creates 2 segments
            mixer.cycle(2).blend(Segment, owner=foo, is_deleted=False,
                                 name=mixer.sequence('foo_{0}'), status=Segment.DRAFT)
            # He also has 5 deleted segments that don't count
            mixer.cycle(5).blend(Segment, owner=foo, is_deleted=True,
                                 name=mixer.sequence('foo_deleted_{0}'))

            buz = self.current_user

            url = '/segments'
            payload = json.dumps({
                'segmentName': 'baz'
            })
            resp = self.client.post(url, data=payload, content_type='application/json')
            self.assertIn('Insufficient segment quota', resp.data)

            quota.segment_cnt = 1

    def test_get_segments(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            self.segment = mixer.blend(Segment, owner=user,
                                       last_run_ts=datetime.datetime.utcnow(),
                                       status='COMPLETED',
                                       is_deleted=False)
            res = self.client.get('/segments')
            self.assertTrue(isinstance(json.loads(res.data)['data'], list))
            self.assertTrue(len(json.loads(res.data)['data']) > 0)

            es_account = mixer.blend(Account, name='ES', account_type=Account.ES, deleted=False)
            es_account_quota = mixer.blend(AccountQuota, account=es_account, segment_cnt=2)
            es_admin = mixer.blend(User, account=es_account, email='es_admin@everstring.com', status=User.ACTIVE)
            role1 = mixer.blend(Role, name=Role.ES_ADMIN)
            mixer.blend(UserRole, user=es_admin, role=role1)
            es_admin_segment = mixer.blend(Segment, owner=es_admin, name='Test Segment 1', status=Segment.DRAFT,
                                           is_deleted=False)

            account = mixer.blend(Account, name='Test', account_type=Account.PAYING, deleted=False)
            account_quota = mixer.blend(AccountQuota, account=account, segment_cnt=1)
            account_admin = mixer.blend(User, account=account, email='user@everstring.com', status=User.ACTIVE)
            role2 = mixer.blend(Role, name=Role.ACCOUNT_ADMIN)
            mixer.blend(UserRole, user=account_admin, role=role2)

            # es admin help other account create segment
            data = {'segmentName': 'Test Segment 2', 'accountId': account.id}
            res = self.client.post('/segments?__user_id=%d' % es_admin.id, data=json.dumps(data), content_type='application/json')
            user_segment = json.loads(res.data)['data']
            segment = Segment.query.get(user_segment['segmentId'])
            segment.update_status(Segment.DRAFT)

            # without account_id in segments get
            res = self.client.get('/segments?__user_id=%d' % es_admin.id)
            segs = json.loads(res.data)['data']
            self.assertEqual(len(segs), 2)
            self.assertEqual(segs[1]['ownerId'], es_admin.id)
            self.assertEqual(segs[1]['segmentName'], 'Test Segment 1')
            can_create_segments = json.loads(res.data)['canCreateSegments']
            self.assertEqual(can_create_segments, True)

            # with account_id in segments get
            res = self.client.get('/segments?__user_id=%d&accountId=%d' % (es_admin.id, account.id))
            segs = json.loads(res.data)['data']
            self.assertEqual(len(segs), 1)
            self.assertEqual(segs[0]['ownerId'], es_admin.id)
            self.assertEqual(segs[0]['segmentName'], 'Test Segment 2')

            # Before change owner, account admin got no segment
            res = self.client.get('/segments?__user_id=%d' % account_admin.id)
            segs = json.loads(res.data)['data']
            self.assertEqual(len(segs), 0)

            # change owner of the segment
            res = self.client.put('/segments/%d/change_owner?__user_id=%d&userId=%d' % (user_segment['segmentId'], es_admin.id, account_admin.id))
            self.assertEqual(res.status_code, httplib.OK)

            # After change owner, account admin have the segment
            res = self.client.get('/segments?__user_id=%d' % account_admin.id)
            segs = json.loads(res.data)['data']
            self.assertEqual(len(segs), 1)
            self.assertEqual(segs[0]['ownerId'], account_admin.id)
            self.assertEqual(segs[0]['segmentName'], 'Test Segment 2')

    def test_get_segment(self):
        # case1: Normal flow to get get segment
        data = {'segmentName': 'Test Segment'}
        res = self.client.post('/segments', data=json.dumps(data), content_type='application/json')
        segment = json.loads(res.data)['data']
        self.assertIn('segmentId', segment)
        res = self.client.get('/segments/' + str(segment['segmentId']))

        response_data = json.loads(res.data)['data']
        self.assertIn('segmentId', response_data)
        self.assertTrue(response_data.get("canExport"))

        # case2: segment id not existed
        res = self.client.get('/segments/10000')
        self.assertEqual(res.status_code, httplib.BAD_REQUEST)

        # case3: cannot access other user's segment
        segment_id = -1
        with app.app_context():
            account = Account(name='ABC',
                              account_type='TRIAL')
            user = User(email='test@abc.com',
                        password='123456',
                        first_name='Test',
                        last_name='ABC',
                        status=User.INACTIVE,
                        account=account,
                        contact_export_flag=False)
            segment = Segment(owner=user,
                              name='ABC Test',
                              last_run_ts=datetime.datetime.utcnow(),
                              status=Segment.EMPTY,
                              is_deleted=False)
            db.session.add_all([account, user, segment])
            db.session.commit()
            segment_id = segment.id
        res = self.client.get('/segments/' + str(segment_id))
        self.assertEqual(res.status_code, httplib.FORBIDDEN)

        # case4: can access but can not export result
        with app.app_context():
            mixer.blend(UserSegment, user_id=self.current_user.id, segment_id=segment_id)
        res = self.client.get('/segments/' + str(segment_id))
        self.assertFalse(json.loads(res.data)['data'].get("canExport"))

    def test_delete_segment(self):
        data = {'segmentName': 'Test Segment'}
        res = self.client.post('/segments', data=json.dumps(data), content_type='application/json')
        segment = json.loads(res.data)['data']
        self.assertIn('segmentId', segment)
        segment_id1 = segment['segmentId']
        res = self.client.get('/segments/' + str(segment_id1))
        self.assertIn('segmentId', json.loads(res.data)['data'])

        # case1: cannot delete other user's segment
        segment_id2 = -1
        with app.app_context():
            account = Account(name='ABC',
                              account_type='TRIAL')
            user = User(email='test@abc.com',
                        password='123456',
                        first_name='Test',
                        last_name='ABC',
                        status=User.INACTIVE,
                        account=account,
                        contact_export_flag=False)
            segment = Segment(owner=user,
                              name='ABC Test',
                              last_run_ts=datetime.datetime.utcnow(),
                              status=Segment.EMPTY,
                              is_deleted=False)
            db.session.add_all([account, user, segment])
            db.session.commit()
            segment_id2 = segment.id
        res = self.client.delete('/segments/' + str(segment_id2))
        self.assertEqual(res.status_code, httplib.FORBIDDEN)

        # case2: Normal workflow for delete segment
        res = self.client.delete('/segments/' + str(segment_id1))
        self.assertEqual(res.status_code, httplib.OK)

        # case3: Delete a segment which already deleted
        res = self.client.delete('/segments/' + str(segment_id1))
        self.assertEqual(res.status_code, httplib.BAD_REQUEST)

    @patch('ns.util.queue_service_client.QueueServiceClient.submit_model_run_request')
    def test_model_run(self, submit_request_mock):
        submit_request_mock.side_effect = None

        # case1: segment id not existed
        res = self.client.post('/segments/{}/model_run'.format(10000))
        self.assertEqual(res.status_code, httplib.BAD_REQUEST)

        # case2: Normal flow to get segment
        data = {'segmentName': 'Test Segment'}
        res = self.client.post('/segments', data=json.dumps(data), content_type='application/json')
        segment = json.loads(res.data)['data']
        self.assertIn('segmentId', segment)
        with app.app_context():
            seg = Segment.get_by_id(segment['segmentId'])
            source = mixer.blend(Source, source_name='CSV')
            seed_candidate = mixer.blend(SeedCandidate, segment=seg, source=source)
            seg.update_status(Segment.DRAFT)
        res = self.client.post('/segments/{}/model_run'.format(segment['segmentId']),
                               content_type='application/json')
        self.assertIn('segmentId', json.loads(res.data)['data'])

        # case3: Only segment in DRAFT is allowed to run model
        with app.app_context():
            seg = Segment.get_by_id(segment['segmentId'])
            seg.update_status(Segment.IN_PROGRESS)
        res = self.client.post('/segments/{}/model_run'.format(segment['segmentId']))
        self.assertEqual(res.status_code, httplib.FORBIDDEN)

        # case4: cannot access other user's segment
        segment_id = -1
        with app.app_context():
            account = Account(name='ABC',
                              account_type='TRIAL')
            user = User(email='test@abc.com',
                        password='123456',
                        first_name='Test',
                        last_name='ABC',
                        status=User.INACTIVE,
                        account=account,
                        contact_export_flag=False)
            segment = Segment(owner=user,
                              name='ABC Test',
                              last_run_ts=datetime.datetime.utcnow(),
                              status=Segment.EMPTY,
                              is_deleted=False)
            db.session.add_all([account, user, segment])
            db.session.commit()
            segment_id = segment.id
        res = self.client.post('/segments/{}/model_run'.format(segment_id))
        self.assertEqual(res.status_code, httplib.FORBIDDEN)

    def test_get_segment_expansion(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')

            segment = mixer.blend(Segment, owner=user, status=Segment.COMPLETED, is_deleted=False)
            seed = mixer.blend(Seed, segment=segment)
            expansion_dict = {'tech': ['AB', 'CD'], 'department': ['12', '34']}
            expansion = mixer.blend(Expansion, segment_id=segment.id,
                                    expansion_val=json.dumps(expansion_dict))
            job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED,
                              seed_id=seed.id, expansion_id=expansion.id)
            res = self.client.get('/segments/%d/expansion' % segment.id)
            data = json.loads(res.data).get('data')
            self.assertEqual(data, expansion_dict)

    def test_get_segment_qualification(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')

            segment = mixer.blend(Segment, owner=user, status=Segment.COMPLETED, is_deleted=False)
            qualification_dict = {
                "industry": ["Software", "Media"],
                "revenue": ['10B-50B', '1B-5B'],
                "employeeSize": ['1001-2000', '2001-5000','10001-1000000']
            }
            qualification = mixer.blend(Qualification, segment_id=segment.id,
                                        qualification_val=json.dumps(qualification_dict))
            job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED,
                              qualification_id=qualification.id)
            res = self.client.get('/segments/%d/qualification' % segment.id)
            data = json.loads(res.data).get('data')
            self.assertEqual(data, qualification_dict)

    def test_update_segment_expansion(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user, status=Segment.COMPLETED, is_deleted=False)
            expansion = mixer.blend(Expansion, segment_id=segment.id, expansion_val='{"tech": ["AB", "CD"]}')
            expansion_input = {'expansion': {'department': ['12', '34']}}
            expansion_val = json.dumps(expansion_input)
            res = self.client.put('/segments/%d/expansion' % segment.id, data=expansion_val, content_type='application/json')
            self.assertEqual(res.status_code, httplib.OK)
            expansion = Expansion.query.get(expansion.id)
            expected = {'department': ['12', '34'], 'tech': ['AB', 'CD']}
            self.assertEqual(expected, json.loads(expansion.expansion_val))

    def test_update_segment_qualification(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user, status=Segment.COMPLETED, is_deleted=False)
            qualification = mixer.blend(Qualification, segment_id=segment.id,
                                        qualification_val='{"industry": ["Software", "Media"]}')
            qualification_input = {
                "qualification": {
                    "state": ["CA", "VA"],
                    "employeeSize": ['1001-2000', '2001-5000', '10000+']
                }
            }
            qualification_val = json.dumps(qualification_input)
            res = self.client.put('/segments/%d/qualification' % segment.id, data=qualification_val,
                                  content_type='application/json')
            self.assertEqual(res.status_code, httplib.OK)
            qualification = Qualification.query.get(qualification.id)
            expected = {
                "state": ["CA", "VA"],
                "employeeSize": ['1001-2000', '2001-5000', '10000+'],
                "industry": ["Software", "Media"]
            }
            self.assertEqual(expected, json.loads(qualification.qualification_val))

    def test_update_segment_stage(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user, status=Segment.COMPLETED, is_deleted=False)
            data = {'stage': 1}
            res = self.client.put('/segments/%d/stage' % segment.id, data=json.dumps(data),
                                  content_type='application/json')
            self.assertEqual(res.status_code, httplib.OK)
            segment_stage = TmpSegmentStage.get_by_segment(segment.id)
            self.assertEqual(data.get('stage'), segment_stage.stage)

            data = {'stage': 2}
            res = self.client.put('/segments/%d/stage' % segment.id, data=json.dumps(data),
                                  content_type='application/json')
            self.assertEqual(res.status_code, httplib.OK)
            segment_stage = TmpSegmentStage.get_by_segment(segment.id)
            self.assertEqual(data.get('stage'), segment_stage.stage)

    def create_recommendations(self):
        with app.app_context():
            mixer.init_app(app)

            self.user = User.get_user_by_email('test@everstring.com')
            self.segment = mixer.blend(Segment, owner=self.user,
                                       last_run_ts=datetime.datetime.utcnow(),
                                       status='COMPLETED',
                                       is_deleted=False, recommendation_num=4, exported_num=2)
            self.seed = mixer.blend(Seed, segment=self.segment)
            self.job = mixer.blend(Job, segment_id=self.segment.id, type=Job.SEGMENT, status='COMPLETED')
            self.segment_id = self.segment.id

    def test_get_segment_companies_count(self):
        self.create_recommendations()
        res = self.client.get('/segments/' + str(self.segment_id))
        data = json.loads(res.data).get('data')

        self.assertDictContainsSubset({
            'segmentId': self.segment_id,
            'recommendationNum': 4,
            'exportedNum': 2,
        }, data)

    def test_company_firmographics_top_n(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)
            seed = mixer.blend(Seed, segment=segment)
            job = mixer.blend(Job, segment_id=segment.id, seed_id=seed.id, type=Job.SEGMENT, status='COMPLETED')

            mixer.cycle(6).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=(i for i in range(6)), industry='B2B', state='VA')
            mixer.cycle(5).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=(i for i in range(7, 12)), industry='B2B', state='CA')
            mixer.cycle(4).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=(i for i in range(13, 17)), industry='B2C', state='LA')
            mixer.cycle(3).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=(i for i in range(18, 21)), industry='B2C', state='MA')
            mixer.cycle(2).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=(i for i in range(22, 24)), industry='B2B', state='GA')
            mixer.cycle(1).blend(Recommendation, job_id=job.id, segment_id=job.segment_id, user_id=user.id, company_id=25, industry='B2C', state='PA')

            mixer.blend(SegmentMetric,
                        segment_id=segment.id,
                        type=SegmentMetric.METRIC_TECH,
                        value=json.dumps([{"Java": 10}, {"MySql": 9}, {"Python": 8}]))

            segment.recommendation_num = 6 + 5 + 4 + 3 + 2 + 1

            url = '/segments/%d/companies/distinct_counts' % segment.id

            resp = self.client.get(url, query_string={'keys': 'industry,state,tech',
                                                      'order': '-count', 'limit': 5, 'with_others': True})
            results = json.loads(resp.data).get('data')

            # {u'industry': [{u'count': 0, u'value': u'Others'}], u'state': [{u'count': 0, u'value': u'Others'}]}

            expected = {
                'industry': [
                    {'value': 'B2B', 'count': 13},
                    {'value': 'B2C', 'count': 8},
                    {'value': 'Others', 'count': 0}
                ],
                'state': [
                    {'value': 'VA', 'count': 6},
                    {'value': 'CA', 'count': 5},
                    {'value': 'LA', 'count': 4},
                    {'value': 'MA', 'count': 3},
                    {'value': 'GA', 'count': 2},
                    {'value': 'Others', 'count': 1}
                ],
                'tech': [
                    {"count": 10, "value": "Java"},
                    {"count": 9, "value": "MySql"},
                    {"count": 8, "value": "Python"}
                ]
            }
            self.assertDictEqual(expected, results)

    def test_get_seed_candidates(self):
        with app.app_context():
            mixer.init_app(app)
            user = User.get_user_by_email('test@everstring.com')
            source = mixer.blend(Source, source_name='CSV')
            sgmt = mixer.blend(Segment, is_deleted=False, owner=user)

            num = 5

            mixer.cycle(num).blend(SeedCandidate, segment=sgmt, source=source,
                                   tech="[]", department_size="[]", growth="High")

            url = '/segments/%d/seed_candidates' % sgmt.id

            resp = self.client.get(url)
            data = json.loads(resp.data).get('data')

            self.assertEqual(len(data), num)

            expected_fields = ['id', 'companyId', 'companyName', 'domain', 'revenue',
                               'industry', 'employeeSize', 'alexaRank', 'country',
                               'state', 'city', "SFDCAccountId", "SFDCAccountName",
                               "tech", "departmentSize", "growthIndicator", "growth"]
            for sc in data:
                self.assertItemsEqual(expected_fields, sc.keys())

    @httpretty.activate
    def test_get_seed_candidate_insights(self):
        with app.app_context():
            mixer.init_app(app)
            user = User.get_user_by_email('test@everstring.com')
            sgmt = mixer.blend(Segment, is_deleted=False, owner=user)

            domains = ('alf.org', 'databrain.com')
            num = len(domains)

            domain_gen = (domain for domain in domains)

            mixer.cycle(num).blend(SeedCandidate, segment=sgmt, domain=domain_gen)
            payload = {'data': {'employee_size': [{'displayName': 'dummy', 'value': num}]}}

            client = CompanyDBClient.instance()

            httpretty.register_uri(httpretty.POST, re.compile(client.company_service_base_url),
                                   body=json.dumps(payload))

            url = '/segments/%d/seed_candidates/insights' % sgmt.id

            resp = self.client.get(url)
            data = json.loads(resp.data)
            self.assertEqual(resp.status_code, httplib.OK)

            # Assert the employeeSize count == num
            count = sum(int(item['value']) for item in data['data']['employee_size'])
            self.assertEqual(num, count)

    @httpretty.activate
    def test_seed_candidate_enrich_all(self):
        with app.app_context():
            known_candidate_employee_size = {
                'alf.org': 9,
                'databrain.com': 26
            }

            # Fixture
            companies_fixture = []
            for domain, employee_size in known_candidate_employee_size.iteritems():
                companies_fixture.append({'domain': domain, 'employeeSize': employee_size})

            payload = {'data': {'companies': companies_fixture}}

            client = CompanyDBClient.instance()

            httpretty.register_uri(httpretty.POST,
                                   re.compile(client.company_service_base_url),
                                   body=json.dumps(payload))

            # Starter
            candidates = [SeedCandidate(domain=domain)
                          for domain, employee_size in
                          known_candidate_employee_size.iteritems()]

            SeedCandidate.enrich_all(candidates)

            for candidate in candidates:
                expected = SeedCandidate. \
                    bucketize_employee_size(known_candidate_employee_size[candidate.domain])
                self.assertEqual(candidate.employee_size, expected)

    @patch('ns.model.segment.Segment.seed_candidates_by_source')
    @patch('ns.model.sf_pull_request.SFPullRequest.get_by_user_segment')
    def test_get_sfdc_seed_status(self, mock_request_get, mock_segment):
        with app.app_context():

            user = User.get_user_by_email("test@everstring.com")
            segment = mixer.blend(Segment, is_deleted=False, owner=user)

            # without sf pull request
            mock_request_get.return_value = None

            res = self.client.get("/segments/" + str(segment.id) + "/seeds/sfdc")

            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)
            self.assertEqual(data.get("status"), "EMPTY")

            # pull data in progress
            mock_request_get.return_value = SFPullRequest(status=SFPullRequest.IN_PROGRESS)

            res = self.client.get("/segments/" + str(segment.id) + "/seeds/sfdc")

            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)
            self.assertEqual(data.get("status"), SFPullRequest.IN_PROGRESS)
            self.assertTrue("startDate" in data)
            self.assertTrue("endDate" in data)
            self.assertTrue("overview" not in data)

            # pull data completed
            mock_request_get.return_value = SFPullRequest(status=SFPullRequest.COMPLETED)
            mock_query = Mock()
            mock_query.all.return_value = [SeedCandidate(domain=""), SeedCandidate(domain="domain")]
            mock_segment.return_value = mock_query

            res = self.client.get("/segments/" + str(segment.id) + "/seeds/sfdc")

            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)
            self.assertEqual(data.get("status"), SFPullRequest.COMPLETED)
            self.assertTrue("startDate" in data)
            self.assertTrue("endDate" in data)
            self.assertTrue(data.get("overview").get("matches") == 1)
            self.assertTrue(data.get("overview").get("unknown") == 1)

    @patch('ns.model.segment.Segment.upload_sf_seeds')
    def test_pull_data(self, mock_upload):

        with app.app_context():
            user = User.get_user_by_email("test@everstring.com")
            segment = mixer.blend(Segment, is_deleted=False, owner=user, status=Segment.DRAFT)

            mock_upload.side_effect = None

            request_data = {
                "startDate": "2014-01-01T10:00:00",
                "endDate": "2017-01-01T10:00:00"
            }
            res = self.client.post("/segments/" + str(segment.id) + "/seeds/sfdc",
                                   json.dumps(request_data),
                                   content_type="application/json")
            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)
            self.assertEqual(data.get("status"), SFPullRequest.COMPLETED)
            self.assertTrue("startDate" in data)
            self.assertTrue("endDate" in data)
            self.assertTrue("overview" in data)

    @patch('ns.model.seed_candidate.SeedCandidate.query_seed_candidates')
    def test_sfdc_seed_overview(self, mock_seed_query):

        with app.app_context():
            user = User.get_user_by_email("test@everstring.com")
            segment = mixer.blend(Segment, is_deleted=False, owner=user, status=Segment.DRAFT)
            source = mixer.blend(Source, source_name=Source.SALESFORCE)

            mock_seed_query.return_value = 10, []

            res = self.client.get("/segments/" + str(segment.id) +
                                  "/seeds/sfdc/overview?limit=10&offset=10&order=&type=")

            self.assertEqual(res.status_code, 200)
            data = json.loads(res.data)
            self.assertEqual(data.get("total"), 10)
            self.assertEqual(data.get("limit"), 10)
            self.assertEqual(data.get("offset"), 10)
            self.assertEqual(data.get("data"), [])

    def test_get_segment_seed_attributes(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)

            mixer.cycle(6).blend(SeedCandidate, segment=segment, industry='B2B', state='VA', tech=json.dumps(['JS', 'C']))
            mixer.cycle(5).blend(SeedCandidate, segment=segment, industry='B2B', state='CA', tech=json.dumps(['JAVA', 'C++']))
            mixer.cycle(4).blend(SeedCandidate, segment=segment, industry='B2C', state='LA', tech=json.dumps(['JS', 'C++']))
            mixer.cycle(3).blend(SeedCandidate, segment=segment, industry='B2C', state='MA', tech=json.dumps(['JS', 'JAVA']))
            mixer.cycle(2).blend(SeedCandidate, segment=segment, industry='B2B', state='GA', tech=json.dumps(['JS', 'C']))
            mixer.cycle(1).blend(SeedCandidate, segment=segment, industry='B2C', state='PA', tech=json.dumps(['JS', 'HTML']))

            url = '/segments/%d/seed_attributes' % segment.id

            resp = self.client.get(url, query_string={'keys': 'industry,state,tech'})
            results = json.loads(resp.data).get('data')

            expected = {
                'industry': ['B2B', 'B2C'],
                'state': ['CA', 'GA', 'LA', 'MA', 'PA', 'VA'],
                'tech': ['C', 'C++', 'HTML', 'JAVA', 'JS']
            }
            self.assertDictEqual(expected, results)

    @httpretty.activate
    def test_get_segment_universe_attributes(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            client = CompanyDBClient.instance()
            res_states = {'data': {'states': ['California', 'Texas']}}
            httpretty.register_uri(httpretty.GET, re.compile(client.company_service_base_url + '/metadata/location/'),
                                   body=json.dumps(res_states))
            res_industry = {'data': {'categories': ['Accounting', 'Agriculture']}}
            httpretty.register_uri(httpretty.GET, re.compile(client.company_service_base_url + '/metadata/industry/'),
                                   body=json.dumps(res_industry))
            res_tech = {'data': {'categories': ['AdRoll', 'AppNexus']}}
            httpretty.register_uri(httpretty.GET, re.compile(client.company_service_base_url + '/metadata/tech/'),
                                   body=json.dumps(res_tech))

            url = '/segments/universe_attributes'

            resp = self.client.get(url, query_string={'keys': 'employeeSize,revenue,tech,industry,state'})
            results = json.loads(resp.data).get('data')

            expected = {
                'employeeSize': ['1-10', '11-20', '21-50', '51-100', '101-200', '201-500',
                                 '501-1,000', '1,001-2,000', '2,001-5,000', '5,001-10,000', '10,000+'],
                'revenue': ['$0M-$1M', '$1M-$5M', '$5M-$10M', '$10M-$25M', '$25M-$50M',
                            '$50M-$100M', '$100M-$250M', '$250M-$500M', '$500M-$1B',
                            '$1B-$5B', '$5B+'],
                'state': ['California', 'Texas'],
                'industry': ['Accounting', 'Agriculture'],
                'tech': ['AdRoll', 'AppNexus']
            }
            self.assertDictEqual(expected, results)

    @patch('ns.model.db.session.commit')
    @patch('ns.model.db.session.add')
    @patch('ns.model.user_segment.UserSegment.get_by_user_segment')
    def test_change_owner(self, mock_get, mock_add, mock_commit):
        mock_commit.side_effect = None
        mock_add.side_effect = None

        # new owner id is same with old owner id
        segment = Segment()
        segment.owner = User(id=1)
        user = User(id=1)
        segment.change_owner(user)
        mock_commit.has_not_called()

        # segment has not been shared
        segment = Segment(published=False)
        segment.owner = User(id=2)
        user = User(id=1)
        self.assertIsNone(segment.owner_id)

        segment.change_owner(user)
        mock_commit.assert_called_once_with()
        self.assertEqual(segment.owner_id, 1)

        # segment has been shared, new user is not a shared user
        mock_commit.reset_mock()
        mock_get.return_value = None
        segment = Segment(published=True)
        segment.owner = User(id=2)
        user = User(id=1)
        self.assertIsNone(segment.owner_id)

        segment.change_owner(user)
        mock_add.assert_called_once_with(mock.ANY)
        mock_commit.assert_called_once_with()
        self.assertEqual(segment.owner_id, 1)

        # segment has been shared, new user is a shared user
        mock_commit.reset_mock()
        mock_add.reset_mock()

        user_segment = UserSegment(user_id=0)

        mock_get.return_value = user_segment
        segment = Segment(published=True)
        segment.owner = User(id=2)
        user = User(id=1)
        self.assertIsNone(segment.owner_id)
        self.assertEqual(user_segment.user_id, 0)

        segment.change_owner(user)
        mock_add.has_not_called()
        mock_commit.assert_called_once_with()
        self.assertEqual(segment.owner_id, 1)
        self.assertEqual(user_segment.user_id, 2)

    def test_get_companies_overview(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)

            job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED)

            mixer.blend(SegmentMetric, segment_id=segment.id,
                        type=SegmentMetric.METRIC_IN_BOUNDARY_COUNT,
                        value=100)
            mixer.blend(SegmentMetric, segment_id=segment.id,
                        type=SegmentMetric.METRIC_SECTOR,
                        value=json.dumps({"1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7}))

            seed = mixer.blend(SeedCandidate, segment=segment)

            url = '/segments/%d/companies/overview' % segment.id

            resp = self.client.get(url)

            data = json.loads(resp.data)
            self.assertEqual(data["inBoundaryCnt"], 100)
            self.assertEqual(data["companiesCnt"], 0)
            self.assertEqual(data["bizSimilarCnt"], 1 + 4 + 5 + 7)
            self.assertEqual(data["techSimilarCnt"], 2 + 4 + 6 + 7)
            self.assertEqual(data["depSimilarCnt"], 3 + 5 + 6 + 7)
            self.assertEqual(data["seedsCnt"], 1)

    def test_seed_list_distinct_count(self):
        with app.app_context():
            mixer.init_app(app)

            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)

            seed1 = mixer.blend(SeedCandidate, segment=segment)
            seed2 = mixer.blend(SeedCandidate, segment=segment)
            seed1.tech = "[\"t1\", \"t2\"]"
            seed2.tech = "[\"t2\"]"
            db.session.commit()

            url = '/segments/%d/seeds/distinct_counts?keys=tech&order=-count&limit=10' % segment.id

            resp = self.client.get(url)

            data = json.loads(resp.data)["data"]["tech"]
            self.assertEqual(data[0]["value"], "t2")
            self.assertEqual(data[0]["count"], 2)
            self.assertEqual(data[1]["value"], "t1")
            self.assertEqual(data[1]["count"], 1)

    def test_get_company_count_by_sector(self):
        with app.app_context():
            user = User.get_user_by_email('test@everstring.com')
            segment = mixer.blend(Segment, owner=user,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)
            seed = mixer.blend(Seed, segment=segment)
            job = mixer.blend(Job, segment_id=segment.id, seed_id=seed.id, type=Job.SEGMENT, status='COMPLETED')

            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=1, similar_c_score=0, similar_t_score=4, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=2, similar_c_score=4, similar_t_score=0, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=3, similar_c_score=4, similar_t_score=4, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=4, similar_c_score=0, similar_t_score=0, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=5, similar_c_score=0, similar_t_score=4, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=6, similar_c_score=4, similar_t_score=0, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=7, similar_c_score=0, similar_t_score=0, similar_d_score=0)

            mixer.blend(UserExportedCompany, segment_id=segment.id, company_id=1)
            mixer.blend(UserExportedCompany, segment_id=segment.id, company_id=2)
            mixer.blend(UserExportedCompany, segment_id=segment.id, company_id=3)

            sector_metric = {"1": 1, "2": 1, "3": 1, "4": 1, "5": 1, "6": 1, "7": 1}
            mixer.blend(SegmentMetric, segment_id=segment.id, type=SegmentMetric.METRIC_SECTOR, value=json.dumps(sector_metric))

            url = '/segments/%d/sector_company_cnt' % segment.id
            resp = self.client.get(url)
            data = json.loads(resp.data)["data"]

            expected_result = {"1": [1, 1], "2": [1, 1], "3": [1, 1], "4": [1, 0], "5": [1, 0], "6": [1, 0], "7": [1, 0]}
            self.assertEqual(data, expected_result)

    def test_latest_completed_job(self):
        with app.app_context():
            mixer.init_app(app)

            segment = mixer.blend(Segment,
                                  status='COMPLETED',
                                  is_deleted=False)

            job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED)
            later_job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED)
            latest_completed_job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.COMPLETED)
            latest_incompleted_job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status=Job.IN_PROGRESS)

            db.session.commit()
            self.assertEqual(segment.latest_completed_job.id, latest_completed_job.id)

    @patch('ns.model.segment.Segment.check_exported_csv_permission')
    @patch('ns.model.segment.Segment.get_companies_by_sectors')
    @patch('ns.model.segment.Segment.get_page_result')
    @patch('ns.model.segment.Segment.check_export_quota')
    @patch('ns.model.user_segment_sfdc_publisher.UserSegmentSFDCPublisher.check_prerequisite', Mock())
    @patch('ns.util.queue_service_client.QueueServiceClient.send_export_message_with_dedupe')
    def test_export_sfdc_sends_correct_message(
            self, send_mk,
            quota_check_mk, result_mk,
            get_company_mk, check_permission_mk):

        send_mk.return_value = 200
        quota_check_mk.side_effect = None
        result_mk.side_effect = None
        get_company_mk.side_effect = None
        check_permission_mk.side_effect = None
        with app.app_context():
            segment = mixer.blend(
                Segment, owner=self.current_user, status=Segment.COMPLETED, is_deleted=False)
            owner = segment.owner
            CRMToken.create_sfdc_token(owner.account_id, owner.id, {
                'instance_url': 'url-test',
                'access_token': 'token-test',
                'refresh_token': 'token-test',
                'organization_id': 'orgid-test',
                'email': 'email-test',
                'role': 'role-test'
            }, CRMToken.SALESFORCE)

            url = ('/segments/{0}/publish_with_dedupe?'
                   '__user_id={1}').format(segment.id, owner.id)

            resp = self.client.post(url, data={'limit': 1})
            self.assertEqual(resp.status_code, 201)
            send_mk.assert_called_with(mock.ANY)
