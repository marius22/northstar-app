import unittest
from ns.controller.app import app

from ns.model.recommendation import Recommendation
from ns.model.company_insight import CompanyInsight
from base_case import AppTestBase


class TestRecommendation(AppTestBase):

    def test_for_company_insight(self):
        with app.app_context():
            field_display_names = Recommendation._maps(Recommendation)
            for display_name, field in field_display_names.items():
                    CompanyInsight.create_insights(field.name, display_name)

            names_in_recommendation = set(field_display_names.keys())
            names_in_db = set(CompanyInsight.get_all_display_names())

            self.assertEqual(names_in_db, names_in_recommendation)

    def test_sort_fileds(self):
        display_names = ["companyName", "domain", "companyId"]
        sorted_all_names = Recommendation._maps(Recommendation).keys()
        result = filter(lambda x: x in display_names, sorted_all_names)

        self.assertEqual(result, ["companyName", "companyId", "domain"])
