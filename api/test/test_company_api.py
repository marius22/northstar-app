# -*- coding: utf-8 -*-
import cStringIO
import unicodecsv as csv
from mock import PropertyMock
import datetime
import io
import unittest
from flask import json
from mixer.backend.flask import mixer
from mock import patch
from ns.controller.app import app
from ns.model.user_exported_company import UserExportedCompany
from ns.model.account import Account
from ns.model.job import Job
from ns.model.recommendation import Recommendation
from ns.model.seed import Seed
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.account_quota import AccountQuota
from http_client_base import AppHttpClientBase
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.model.company_insight import CompanyInsight
from ns.model import db

class TestCompanyAPI(AppHttpClientBase):
    """
    Test cases for interacting with recommended companies

    By default, the API returns in JSON format, it returns
    a downloadable CSV file if the format is specified as
    CSV.
    """

    def setUp(self):
        super(TestCompanyAPI, self).setUp()

        with app.app_context():
            acct = mixer.blend(Account)
            mixer.blend(CompanyInsight, id=1, display_name="companyName")
            mixer.blend(CompanyInsight, id=2, display_name="companyId")
            mixer.blend(AccountQuota, account=acct,
                        exposed_ui_insight_ids="[1,2]",
                        exposed_csv_insight_ids="[1,2]",
                        expired_ts=datetime.datetime.utcnow() + datetime.timedelta(days=10))
            self.user = mixer.blend(User, account=acct, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=self.user, role=role)
            self.sgmt = mixer.blend(Segment, owner=self.user,
                                    last_run_ts=datetime.datetime.now(),
                                    is_deleted=False,
                                    status=Segment.COMPLETED)
            self.seed = mixer.blend(Seed, segment=self.sgmt)

    @patch('ns.model.company_insight.CompanyInsight.get_display_name_by_ids')
    @patch('ns.model.account_quota.AccountQuota.remaining_csv_company_cnt', new_callable=PropertyMock)
    @patch('ns.model.account_quota.AccountQuota.expired')
    def test_get_companies_for_completed_segment(self, mock_expired, mock_limit, mock_insight):
        mock_expired.return_value = False
        mock_limit.return_value = 10000
        mock_insight.return_value = ["companyName", "domain", "companyId"]
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, status='COMPLETED', type=Job.SEGMENT)

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=101)

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url)
            self.assertEqual(resp.status_code, 200)

            body = json.loads(resp.data)
            data = body.get('data')

            self.assertDictContainsSubset({
                "companyId": recd1.company_id,
                "companyName": recd1.company_name
            }, data[0])

            # Assert the `total` is in the response body
            self.assertEqual(body.get('total'), 1)

            # Assert all the fields are returned
            fieldnames = [
                'domain',
                'companyId',
                'companyName',
                'revenue',
                'industry',
                'employeeSize',
                'tech',
                'growth',
                'alexaRank',
                'linkedinUrl',
                'country',
                'state',
                'city',
                'zipcode',
                'similarCompanies',
                'similarTechnologies',
                'similarDepartments',
                'exportedTs',
                'disputes',
                'operations',
                'finance',
                'researchDevelopment',
                'hr',
                'marketing',
                'sales',
                'legal',
                'engineering',
                'score',
                'educator',
                'medicalHealth',
                'administrative',
                'IT',
                'techCategories'
            ]
            self.assertSetEqual(set(data[0].keys()), set(fieldnames))

    @patch('ns.model.company_insight.CompanyInsight.get_display_name_by_ids')
    @patch('ns.model.account_quota.AccountQuota.remaining_csv_company_cnt', new_callable=PropertyMock)
    @patch('ns.model.account_quota.AccountQuota.expired')
    def test_get_exported_companies(self,  mock_expired, mock_limit, mock_insight):
        mock_expired.return_value = False
        mock_limit.return_value = 10000
        mock_insight.return_value = ["companyName", "domain", "companyId"]
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=100, segment_id=job.segment_id)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=101, segment_id=job.segment_id)
            mixer.blend(UserExportedCompany, segment_id=self.sgmt.id, company_id=100)
            self.sgmt.exported_num = 1

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url, query_string={"onlyExported": 1})
            self.assertEqual(resp.status_code, 200)

            body = json.loads(resp.data)
            data = body.get('data')

            self.assertDictContainsSubset({
                "companyId": recd1.company_id,
                "companyName": recd1.company_name
            }, data[0])

            # Assert the `total` is in the response body
            self.assertEqual(body.get('total'), 1)

            # Assert all the fields are returned
            fieldnames = [
                'domain',
                'companyId',
                'companyName',
                'revenue',
                'industry',
                'employeeSize',
                'tech',
                'growth',
                'alexaRank',
                'linkedinUrl',
                'country',
                'state',
                'city',
                'zipcode',
                'similarCompanies',
                'similarTechnologies',
                'similarDepartments',
                'exportedTs',
                'disputes',
                'operations',
                'finance',
                'researchDevelopment',
                'hr',
                'marketing',
                'sales',
                'legal',
                'engineering',
                'score',
                'educator',
                'medicalHealth',
                'administrative',
                'IT',
                'techCategories'
            ]
            self.assertItemsEqual(data[0].keys(), fieldnames)

    """
    @patch('ns.controller.app.segment.upload_to_s3')
    @patch('ns.model.company_insight.CompanyInsight.get_display_name_by_ids')
    @patch('ns.model.account_quota.AccountQuota.remaining_csv_company_cnt', new_callable=PropertyMock)
    @patch('ns.model.account_quota.AccountQuota.expired')
    def test_get_companies_for_completed_segment_in_csv(self,  mock_expired, mock_limit, mock_insight, mock_upload):
        mock_expired.return_value = False
        mock_limit.return_value = 10000
        mock_insight.return_value = ["companyName", "domain", "companyId"]
        mock_upload.return_value = ('ABC', 'EFG')

        with app.app_context():

            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=1, segment_id=job.segment_id)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=2, segment_id=job.segment_id)
            # Bundle together for asserting item count
            recommendations = [recd1, recd2]

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url, query_string={"format": "csv"})

            # Assert on headers to ensure browser treats the response as file
            self.assertEqual(resp.headers['Content-Type'], 'text/csv')
            content_disposition = 'attachment; filename=%s_account.csv' % self.sgmt.name
            self.assertEqual(resp.headers['Content-Disposition'], content_disposition)

            # Assert on CSV file content
            uni_body = resp.data.decode('unicode-escape')
            reader = csv.DictReader(io.StringIO(uni_body))

            # Assert items count are as expected
            self.assertEqual(len(list(reader)), len(recommendations))

    @patch('ns.controller.app.segment.upload_to_s3')
    @patch('ns.model.company_insight.CompanyInsight.get_display_name_by_ids')
    @patch('ns.model.account_quota.AccountQuota.remaining_csv_company_cnt', new_callable=PropertyMock)
    @patch('ns.model.account_quota.AccountQuota.expired')
    def test_export_company_csv_with_dedupe(self, mock_expired, mock_limit, mock_insight, mock_upload):
        mock_expired.return_value = False
        mock_limit.return_value = 10000
        mock_insight.return_value = ["companyName", "domain", "companyId"]
        mock_upload.return_value = ('ABC', 'EFG')
        with app.app_context():

            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=101, segment_id=job.segment_id)
            mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, company_id=102, segment_id=job.segment_id)

            mixer.blend(UserExportedCompany, segment_id=self.sgmt.id, company_id=101)
            # mixer.blend(UserExportedCompany, segment_id=self.sgmt.id, company_id=102)

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url, query_string={"format": "csv", "dedupe": 1})

            # Assert on headers to ensure browser treats the response as file
            self.assertEqual(resp.headers['Content-Type'], 'text/csv')
            content_disposition = 'attachment; filename=%s_account.csv' % self.sgmt.name
            self.assertEqual(resp.headers['Content-Disposition'], content_disposition)

            # Assert on CSV file content
            uni_body = resp.data.decode('unicode-escape')
            reader = csv.DictReader(io.StringIO(uni_body))

            cnt = 0
            for r in reader:
                self.assertEqual(int(r.get('companyId')), 102)
                cnt += 1
            self.assertEqual(cnt, 1)

    @patch('ns.controller.app.segment.upload_to_s3')
    @patch('ns.model.company_insight.CompanyInsight.get_display_name_by_ids')
    @patch('ns.model.account_quota.AccountQuota.remaining_csv_company_cnt', new_callable=PropertyMock)
    @patch('ns.model.account_quota.AccountQuota.expired')
    def test_special_characters_in_the_CSV(self, mock_expired, mock_limit, mock_insight, mock_upload):
        mock_expired.return_value = False
        mock_limit.return_value = 10000
        mock_insight.return_value = ["companyName", "domain", "companyId"]
        mock_upload.return_value = ('ABC', 'EFG')

        with app.app_context():

            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            special_company_name = u'Université Toulouse 1 Capitole'
            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id,
                                company_name=special_company_name, company_id=1, segment_id=job.segment_id)

            url = '/segments/%d/companies' % self.sgmt.id

            resp = self.client.get(url, query_string={"format": "csv"})

            # Assert on CSV file content
            reader = csv.DictReader(cStringIO.StringIO(resp.data))
            recd = reader.next()

            self.assertEqual(recd['companyName'], special_company_name)
    """

    def test_limit_params(self):
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=100)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=101)

            url = '/segments/%d/companies' % self.sgmt.id

            # Limit on return item counts
            limit1 = 1
            resp = self.client.get(url, query_string={"limit": limit1})
            data = json.loads(resp.data).get('data')
            self.assertEqual(len(data), limit1)

            limit2 = 2
            resp = self.client.get(url, query_string={"limit": limit2})
            data = json.loads(resp.data).get('data')
            self.assertEqual(len(data), limit2)

    def test_offset_params(self):
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=100)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=101)

            url = '/segments/%d/companies' % self.sgmt.id

            # This option should ensure we only get recd2
            paging_options = {
                "limit": 1,
                "offset": 1
            }
            resp = self.client.get(url, query_string=paging_options)
            data = json.loads(resp.data).get('data')
            self.assertDictContainsSubset({
                "companyName": recd2.company_name,
                "companyId": recd2.company_id
            }, data[0])

    def test_sort_params(self):
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, revenue='1', segment_id=job.segment_id, company_id=100)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, revenue='2', segment_id=job.segment_id, company_id=101)

            url = '/segments/%d/companies' % self.sgmt.id

            # Order by revenue ascend
            paging_options = {
                'order': 'revenue'
            }
            resp = self.client.get(url, query_string=paging_options)
            data = json.loads(resp.data).get('data')
            item_revenues = [recd.get('revenue') for recd in data]

            self.assertEqual(item_revenues, ['1', '2'])

            # Order by revenue descend
            paging_options = {
                'order': '-revenue'
            }
            resp = self.client.get(url, query_string=paging_options)
            data = json.loads(resp.data).get('data')
            item_revenues = [recd.get('revenue') for recd in data]

            self.assertEqual(item_revenues, ['2', '1'])

    def test_filters(self):
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            recd1 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, revenue='revenue-1', segment_id=job.segment_id, company_id=100)
            recd2 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, revenue='revenue-2', segment_id=job.segment_id, company_id=101)
            recd3 = mixer.blend(Recommendation, job_id=job.id, user_id=self.user.id, revenue='revenue-3', segment_id=job.segment_id, company_id=102)

            url = '/segments/%d/companies' % self.sgmt.id
            rs = [recd1.revenue, recd3.revenue]

            criterion = {
                'revenue': {
                    '$in': rs
                }
            }

            resp = self.client.get(url, query_string={'where': json.dumps(criterion)})
            data = json.loads(resp.data)
            self.assertEqual(data['total'], len(rs))

    def test_filter_distinct_counts(self):
        with app.app_context():
            job = mixer.blend(Job, segment_id=self.sgmt.id, seed_id=self.seed.id, type=Job.SEGMENT, status='COMPLETED')

            # Create `r1_num` recommendations with `revenue-1` and `r2_num` recommendation with `revenue-2`
            r1_num = 10
            r2_num = 7
            mixer.cycle(r1_num).blend(
                Recommendation,
                job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=(i for i in range(r1_num)),
                revenue='$0M-$1M', state='AK')
            mixer.cycle(r2_num).blend(
                Recommendation,
                job_id=job.id, user_id=self.user.id, segment_id=job.segment_id, company_id=(i for i in range(r1_num, r1_num+r2_num)),
                revenue='$1M-$5M', state='CA')
            self.sgmt.recommendation_num = r1_num + r2_num
            db.session.add(self.sgmt)
            db.session.commit()

            url = '/segments/%d/companies/distinct_counts' % self.sgmt.id

            resp = self.client.get(url, query_string={'keys': 'revenue,state', 'with_others': True})
            results = json.loads(resp.data).get('data')

            expected = {
                'revenue': [
                    {'value': '$0M-$1M', 'count': r1_num},
                    {'value': '$1M-$5M', 'count': r2_num},
                    {'value': 'Others', 'count': 0}
                ],
                'state': [
                    {'value': 'AK', 'count': r1_num},
                    {'value': 'CA', 'count': r2_num},
                    {'value': 'Others', 'count': 0}
                ]
            }

            self.assertDictEqual(expected, results)


if __name__ == '__main__':
    unittest.main()
