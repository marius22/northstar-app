from mixer.backend.flask import mixer
from mock import patch
from ns.controller.app import app
from http_client_base import AppHttpClientBase
from ns.model.domain_dictionary import DomainDictionary
from ns.util.domain_util import get_batch_domain_matches


class TestDomainUtil(AppHttpClientBase):

    def setUp(self):
        super(TestDomainUtil, self).setUp()
        with app.app_context():
            mixer.blend(DomainDictionary, id=1, domain="yahoo.com", domain_type=1)

    @patch('ns.model.company_name_2_domain.fetch_domains_dict_by_company_names')
    def test_get_batch_domain_matches(self,
                                      mock_fetch_domains_dict_by_company_names):
        with app.app_context():

            mock_fetch_domains_dict_by_company_names.return_value = {
              'EverString': 'everstring.com',
              'Google': 'google.com'
            }

            identifying_data = [
              {
                'name': 'EverString'
              },
              {
                'email': 'googleguy@yahoo.com',
                'name': 'Google'
              },
              {
                'website': 'amazon.com',
                'email': 'jeff@amazon.com',
                'id': 'foo'
              },
              {
                'email': 'bob@bobscompany.com',
                'name': 'Bobs Company'
              },
              {
                'email': 'alice@yahoo.com',
                'name': 'Unknown Company, Inc.'
              }
            ]

            # Test #1: Ensure that we get the expected domains output.
            # Use sets here so order doesn't matter.
            domains = set(get_batch_domain_matches(identifying_data))
            expected_domains = set(['amazon.com', 'bobscompany.com', 'everstring.com', 'google.com'])
            self.assertEqual(domains, expected_domains)


            # Test #2: Ensures that our original input is properly transformed, and that the
            # priorities (website -> email -> name) of the matching logic are respected.
            expected_identifying_data = [
                # 'name' is the only thing provided for this one, and 'EverString': 'everstring.com'
                # is in the mocked c2d output dict, so we use 'everstring.com' as our domain here.
                {
                    'matchedDomain': 'everstring.com',
                    'matchedOn': 'name',
                    'name': 'EverString'
                },
                # 'email' takes priority over 'name', but the email domain 'yahoo.com' is invalid
                # because it's in our DomainDictionary table (where we only store invalid domains),
                # so we fall back to 'name'. Our mocked c2d output dict has 'Google': 'google.com',
                # so we use 'google.com' as our domain here.
                {
                    'matchedDomain': 'google.com',
                    'matchedOn': 'name',
                    'email': 'googleguy@yahoo.com',
                    'name': 'Google'
                },
                # 'website' takes first priority so we should just match on website, use the website
                # as our domain, and we're done. 'email' should be ignored. We also make sure that 'id',
                # which was set on our original identifying_data, is preserved throughout the matching algorithm.
                {
                    'website': 'amazon.com',
                    'matchedDomain': u'amazon.com',
                    'matchedOn': 'website',
                    'email': 'jeff@amazon.com',
                    'id': 'foo'
                },
                # 'email' takes priority over name, and 'bobscompany.com' is not in the DomainDictionary table,
                # meaning that it's a valid domain, so we use 'bobscompany.com' as our domain here.
                {
                    'matchedDomain': 'bobscompany.com',
                    'matchedOn': 'email',
                    'email': 'bob@bobscompany.com',
                    'name': 'Bobs Company'
                },
                # 'yahoo.com' is an invalid email domain and 'Unknown Company, Inc.' is not in our c2d output, so
                # we have no match here.
                {
                    'matchedDomain': None,
                    'matchedOn': None,
                    'email': 'alice@yahoo.com',
                    'name': 'Unknown Company, Inc.'
                }
            ]

            self.assertEqual(identifying_data, expected_identifying_data)
