import unittest

from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.user import User
from ns.model.role import Role
from ns.model.user_role import UserRole
from ns.model.account import Account
from http_client_base import AppHttpClientBase


class TestUserRole(AppHttpClientBase):

    def setUp(self):
        super(TestUserRole, self).setUp()
        with app.app_context():
            self.current_account = mixer.blend(Account, name='es_test',
                                               account_type=Account.TRIAL)
            self.current_user = mixer.blend(User, account=self.current_account,
                                            email='test@everstring.com', activated=True)

    def test_remove_user_from_roles(self):
        with app.app_context():
            user = self.current_user
            r1 = mixer.blend(Role, name=Role.ES_ADMIN)
            r2 = mixer.blend(Role, name=Role.ACCOUNT_USER)

            UserRole.create(user, r1)
            UserRole.create(user, r2)

            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 2)

            UserRole.unassign_roles_from_user(user, roles)
            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 0)

    def test_add_user_to_roles(self):
        with app.app_context():
            user = self.current_user
            r1 = mixer.blend(Role, name=Role.ES_ADMIN)
            r2 = mixer.blend(Role, name=Role.ACCOUNT_USER)

            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 0)

            UserRole.assign_roles_to_user(user, [r1, r2])

            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 2)

    def test_reset_role_for_user(self):
        with app.app_context():
            user = self.current_user
            r1 = mixer.blend(Role, name=Role.ES_ADMIN)
            r2 = mixer.blend(Role, name=Role.ACCOUNT_USER)
            r3 = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)

            UserRole.create(user, r1)

            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 1)
            self.assertTrue(r1 in roles)

            UserRole.reset_role_for_user(user, [r2, r3])

            roles = UserRole.get_roles_by_user(user)
            self.assertEqual(len(roles), 2)
            self.assertTrue(r2 in roles)
            self.assertTrue(r3 in roles)
            self.assertTrue(r1 not in roles)
