import httplib
import unittest

import datetime

from flask import json
from mixer.backend.flask import mixer
from mock import patch
from ns.controller.service import app
from ns.model import db
from ns.model.job import Job
from ns.model.recommendation import Recommendation
from ns.model.segment import Segment
from ns.model.segment_metric import SegmentMetric
from http_client_base import ServiceHttpClientBase


class TestJobResultService(ServiceHttpClientBase):

    @patch('ns.model.job.Job.create_recommendations')
    def test_import_job_result(self, rec_mock):
        with app.app_context():
            sgmt = mixer.blend(Segment, last_run_ts=datetime.datetime.now())
            job = mixer.blend(Job, status='COMPLETED', segment_id=sgmt.id, type=Job.SEGMENT)

            history_job = mixer.blend(Job, status='COMPLETED', segment_id=sgmt.id, type=Job.SEGMENT)
            mixer.cycle(10).blend(Recommendation, job_id=history_job.id, segment_id=job.segment_id, company_id=(i for i in range(10)))

            another_history_job = mixer.blend(Job, status='COMPLETED', segment_id=sgmt.id, type=Job.SEGMENT)
            mixer.cycle(5).blend(Recommendation, job_id=another_history_job.id, segment_id=job.segment_id, company_id=(i for i in range(10, 15)))

            mixer.params['commit'] = False
            rs = []
            item_counts = 5
            for i in xrange(item_counts):
                # It should be fine to have no job, as soon as it's not commit
                # to the database
                r = mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=i)
                rs.append(r.serialized)
            mixer.params['commit'] = True

            # mocking create recommendations
            # temp solution because
            # db.engine.execute(
            #     Recommendation.__table__.insert(),
            #     rs
            # )
            # does not work with sqlalchemy_binds
            def rec_mock_side_effect(recommendation_dicts):
                rs = []
                for i in xrange(item_counts):
                    r = mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=i+100)
                    rs.append(r)
                return rs
            rec_mock.side_effect = rec_mock_side_effect

            url = '/jobs/%d/recommendations' % job.id
            payload = json.dumps({'recommendations': rs})
            resp = self.client.post(url, data=payload, content_type='application/json')

            data = json.loads(resp.data)
            # API should tell us the record count it has created
            self.assertEqual(data.get('total'), item_counts)
            self.assertEqual(resp.status_code, httplib.CREATED)

            self.assertEquals(history_job.recommendations.count(), 0)
            self.assertEquals(another_history_job.recommendations.count(), 0)

            # Still need to verify they have been created in the database
            db_item_counts = job.recommendations.count()
            self.assertEqual(db_item_counts, item_counts)

    def test_count_sector_companies(self):
        with app.app_context():
            segment = mixer.blend(Segment,
                                  last_run_ts=datetime.datetime.utcnow(),
                                  status='COMPLETED',
                                  is_deleted=False)
            job = mixer.blend(Job, segment_id=segment.id, type=Job.SEGMENT, status='COMPLETED')

            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=1, similar_c_score=0, similar_t_score=4, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=2, similar_c_score=4, similar_t_score=0, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=3, similar_c_score=4, similar_t_score=4, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=4, similar_c_score=0, similar_t_score=0, similar_d_score=4)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=5, similar_c_score=0, similar_t_score=4, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=6, similar_c_score=4, similar_t_score=0, similar_d_score=0)
            mixer.blend(Recommendation, job_id=job.id, segment_id=job.segment_id, company_id=7, similar_c_score=0, similar_t_score=0, similar_d_score=0)

            url = '/jobs/%d/count_sector_companies' % job.id
            resp = self.client.post(url)
            self.assertEqual(resp.status_code, httplib.OK)

            expected_result = {"1": 1, "2": 1, "3": 1, "4": 1, "5": 1, "6": 1, "7": 1}
            sector_metric = SegmentMetric.get_by_type(segment.id, SegmentMetric.METRIC_SECTOR)

            self.assertEqual(sector_metric.value, expected_result)
