from base_case import AppTestBase
from base_case import ServiceTestBase
from mixer.backend.flask import mixer
from ns.controller.app import app as app_app
from ns.controller.service import app as service_app
from ns.model.account import Account
from ns.model.account_quota import AccountQuota
from ns.model.user import User


class AppWithDataBase(AppTestBase):

    def setUp(self):
        super(AppWithDataBase, self).setUp()

        with app_app.app_context():
            self.current_account = mixer.blend(Account,
                                               name='es_test',
                                               account_type=Account.TRIAL)
            mixer.blend(AccountQuota,
                        account=self.current_account,
                        segment_cnt=1,
                        exposed_csv_insight_ids='[]',
                        exposed_ui_insight_ids='[]',
                        real_time_scoring_insight_ids='[]')
            self.current_user = mixer.blend(User, account=self.current_account,
                                            email='test@everstring.com',
                                            status=User.ACTIVE)


class ServiceWithDataBase(ServiceTestBase):

    def setUp(self):
        super(ServiceWithDataBase, self).setUp()

        with service_app.app_context():
            self.current_account = mixer.blend(Account,
                                               name='es_test',
                                               account_type=Account.TRIAL)
            mixer.blend(AccountQuota,
                        account=self.current_account,
                        segment_cnt=1,
                        exposed_csv_insight_ids='[]',
                        exposed_ui_insight_ids='[]')
            self.current_user = mixer.blend(User, account=self.current_account,
                                            email='test@everstring.com',
                                            status=User.ACTIVE)
