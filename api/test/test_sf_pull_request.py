import unittest
import datetime
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model import db
from ns.model.sf_pull_request import SFPullRequest
from ns.model.user import User
from ns.model.segment import Segment
from base_case import ServiceTestBase


class TestSFPullRequest(ServiceTestBase):

    def test_save(self):
        with app.app_context():
            user = mixer.blend(User)
            segment = mixer.blend(Segment)

            ts1 = datetime.datetime(2016, 01, 01)
            r1 = SFPullRequest.save_segment_request(user, segment, ts1, ts1)
            self.assertIsNotNone(r1.id)
            self.assertEqual(r1.status, SFPullRequest.IN_PROGRESS)
            self.assertEqual(r1.start_ts, ts1)

            r1.complete()
            self.assertEqual(r1.status, SFPullRequest.COMPLETED)

            ts2 = datetime.datetime(2017, 01, 01)
            r2 = SFPullRequest.save_segment_request(user, segment, ts2, ts2)
            self.assertEqual(r2.status, SFPullRequest.IN_PROGRESS)
            self.assertEqual(r2.start_ts, ts2)
