import json
import unittest
from http_client_base import ServiceHttpClientBase
from mixer.backend.flask import mixer
from ns.model.seed_company import SeedCompany
from ns.model.model import Model
from ns.controller.service import app


class TestModelService(ServiceHttpClientBase):

    def test_get_model_seeds(self):

        with app.app_context():
            mixer.blend(SeedCompany, model_seed_id=1, domain="domain1.com")
            mixer.blend(SeedCompany, model_seed_id=1, domain="domain2.com")

            resp = self.client.get("/model_seeds/1/domains")

            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")

            self.assertItemsEqual(data, ["domain1.com", "domain2.com"])

    def test_update_model_status(self):

        with app.app_context():

            model_id = 1
            mixer.blend(Model, id=model_id, status='IN_PROGRESS')

            model = Model.get_by_id(model_id)
            self.assertEquals(model.status, Model.IN_PROGRESS)

            resp = self.client.post(
                '/models/1/status',
                data=json.dumps(
                    {
                        "status": "hahahha",
                        "model_type": ""
                    }
                ),
                content_type="application/json"
            )

            self.assertEquals(resp.status_code, 500)

            model = Model.get_by_id(1)
            self.assertEquals(model.status, Model.IN_PROGRESS)

            resp = self.client.post(
                '/models/1/status',
                data=json.dumps(
                    {
                        "status": "JobCompleted",
                        "model_type": ""
                    }
                ),
                content_type="application/json"
            )

            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data).get("status")
            self.assertEquals(data, "OK")
            model = Model.get_by_id(1)
            self.assertEquals(model.status, Model.COMPLETED)

    def test_submit_model(self):
        with app.app_context():
            mixer.blend(Model, id=1, account_id=1, type="fit", model_seed_id=1)

            resp = self.client.post("/models/1/submit")

            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data).get("status")

            self.assertEqual(data, "OK")
