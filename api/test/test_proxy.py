import unittest
import httpretty
import re
import json
from mock import patch
from mixer.backend.flask import mixer

from ns.controller.app import app
from http_client_base import AppHttpClientBase
from ns.util.company_db_client import CompanyDBClient
from ns.model.role import Role
from ns.model.audience import Audience
from ns.model.account import Account
from ns.model.user import User
from ns.model.crm_token import CRMToken
from data_http_client_base import AppDataClientBase


class TestProxy(AppDataClientBase):

    def setUp(self):
        super(TestProxy, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    @httpretty.activate
    def test_get_profile(self):
        httpretty.register_uri(httpretty.POST,
                               re.compile(self.company_db_client.base_common_url),
                               body=json.dumps({
                                   "data": {"companies": [
                                       {"domain": "domains3.com", "name": "company_name_3",
                                        "employeeSize": 100, "revenueRange": 2000},
                                       {"domain": "domains4.com", "name": "company_name_4",
                                        "employeeSize": 10, "revenueRange": 100}
                                   ]
                                   }
                               }),
                               content_type="application/json")
        resp = self.client.get("/companies/profile?domains=domains3.com,domains4.com")
        self.assertEqual(resp.status_code, 200)

        data = json.loads(resp.data).get("data")
        self.assertEqual(len(data), 2)

    @patch('ns.util.http_client.HTTPClient._do_request')
    def test_proxy(self, mock_request):

        mock_request.side_effect = None
        with app.app_context():
            test_url = "test_url?param_1=haha&param_2=hehe"
            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url,
                    "method": "get",
                    "json": {}
                }),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200)
            mock_request.called_once_with(
                "get",
                self.company_db_client.base_common_url + test_url,
                json={}
            )

            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url,
                    "method": "post",
                    "json": {}
                }),
                content_type="application/json"
            )

            self.assertEqual(resp.status_code, 200)
            mock_request.called_once_with(
                "post",
                self.company_db_client.base_common_url + test_url,
                json={}
            )

            audience = mixer.blend(Audience, is_deleted=False, user_id=1)
            test_url2 = "/common/audiences/"+str(audience.id)+"/companies/count"
            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url2,
                    "method": "post",
                    "json": {}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 403)
            mock_request.called_once_with(
                "post",
                self.company_db_client.base_common_url + test_url2,
                json={}
            )

            mixer.blend(CRMToken, token=json.dumps({"organization_id": "org123"}),
                        organization_id="org123", account_id=2, crm_type="SALESFORCE")
            test_url3 = "/common/crm/sfdc/org123/leads"
            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url3,
                    "method": "post",
                    "json": {}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 400)
            mock_request.called_once_with(
                "post",
                self.company_db_client.base_common_url + test_url3,
                json={}
            )

            mixer.blend(CRMToken, token=json.dumps({"organization_id": "org123"}),
                        organization_id="org123", account_id=1, crm_type="SALESFORCE")
            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url3,
                    "method": "post",
                    "json": {}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
            mock_request.called_once_with(
                "post",
                self.company_db_client.base_common_url + test_url3,
                json={}
            )

            test_url4 = "/comds"
            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url4,
                    "method": "post",
                    "json": {"orgId": "org123"}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)

            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": test_url4,
                    "method": "post",
                    "json": {"orgId": "234"}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 403)

            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": "sss",
                    "method": "post",
                    "json": {"audienceIds": [audience.id]}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 403)

            resp = self.client.post(
                "/proxy",
                data=json.dumps({
                    "url": "sss",
                    "method": "post",
                    "json": {"audienceIds": []}
                }),
                content_type="application/json"
            )
            self.assertEqual(resp.status_code, 200)
