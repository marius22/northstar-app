from mixer.backend.flask import mixer
from ns.controller.app import app
from base_case import AppTestBase
from ns.model.user import User
from ns.model.account import Account
from ns.util.company_db_client import CompanyDBClient
from ns.model.company_name_2_domain import \
    fetch_domains_list_by_company_names,\
    fetch_domains_dict_by_company_names
import httpretty
import re
import json


class TestCompanyNameToDomain(AppTestBase):

    def setUp(self):
        super(TestCompanyNameToDomain, self).setUp()
        with app.app_context():
            self.company_db_client = CompanyDBClient.instance()

    def _mock_requst(self):
        httpretty.register_uri(httpretty.POST,
                               re.compile(self.company_db_client.base_common_url),
                               body=json.dumps({
                                   "result": [
                                       {
                                           "name": "google",
                                           "domain": "google.com",
                                           "score": 1,
                                           "status": {
                                               "errcode": 200,
                                               "errmsg": "success"
                                           }
                                       },
                                       {
                                           "name": "salesforce",
                                           "domain": "salesforce.com",
                                           "score": 1,
                                           "status": {
                                               "errcode": 200,
                                               "errmsg": "success"
                                           }
                                       }
                                   ],
                                   "status": 200
                               }),
                               content_type="application/json")

    @httpretty.activate
    def test_get_domains_dict_by_company_names(self):
        with app.app_context():
            self._mock_requst()

            result = fetch_domains_dict_by_company_names(["haha", "hehe"])
            self.assertEqual(result['google'], "google.com")
            self.assertEqual(result['salesforce'], "salesforce.com")

    @httpretty.activate
    def test_get_domains_list_by_company_names(self):
        with app.app_context():
            self._mock_requst()

            result = fetch_domains_list_by_company_names(["haha", "hehe"])
            self.assertIn("google.com", result)
            self.assertIn("salesforce.com", result)
