import json
from mixer.backend.flask import mixer
from ns.controller.service import app
from ns.model.account import Account
from http_client_base import ServiceHttpClientBase
from ns.model.crm_token import CRMToken, SFDCToken
from mock import patch
from ns.model.user import User
from ns.controller.service import user
import httplib


class TestUserService(ServiceHttpClientBase):

    def setUp(self):
        super(TestUserService, self).setUp()
        with app.app_context():
            account = mixer.blend(Account, id=20, account_type=Account.PAYING, name='admin@everstring', domain='everstring.com')
            mixer.blend(User, account=account, email='test@everstring.com')

    def test_get_user_by_email(self):
        with app.app_context():
            resp = self.client.get("/users/get_by_email?email=test@everstring.com")
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertIsNotNone(data)

    def test_get_user_by_id(self):
        with app.app_context():
            user = User.get_user_by_email("test@everstring.com")
            resp = self.client.get("/users/get_by_id?userId=%s" % user.id)
            self.assertEqual(resp.status_code, 200)

            data = json.loads(resp.data).get("data")
            self.assertIsNotNone(data)
