from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.company_dispute import CompanyDispute
from ns.controller.app.segment import Segment
from base_case import AppTestBase
from ns.model.user import User
from ns.model.account import Account


class TestCompanyDispute(AppTestBase):

    def setUp(self):

        super(TestCompanyDispute, self).setUp()

        with app.app_context():
            self.current_account = mixer.blend(Account, name='es_test')
            self.current_user = mixer.blend(User, account=self.current_account,
                            email='test@everstring.com', status=User.ACTIVE)

    def test_create_and_query(self):
        with app.app_context():
            segment = mixer.blend(Segment, owner=self.current_user,
                                  is_deleted=False,
                                  status=Segment.COMPLETED)

            dispute_1 = CompanyDispute.create_dispute(segment.id, 1, "test_field_name")
            self.assertIsNotNone(dispute_1.id)

            dispute_2 = CompanyDispute.get_by_id_and_field_name(segment.id, 1, "test_field_name")
            self.assertEqual(dispute_1.id, dispute_2.id)

            CompanyDispute.create_dispute(segment.id, 2, "test_field_name_2")
            CompanyDispute.create_dispute(segment.id, 2, "test_field_name_3")
            CompanyDispute.create_dispute(segment.id, 2, "test_field_name_4")

            result = CompanyDispute.get_by_segment_company_ids(segment.id, [1, 2])
            self.assertEqual(len(result), 4)
