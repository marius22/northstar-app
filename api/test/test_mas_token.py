from mixer.backend.flask import mixer
from ns.controller.app import app
from data_http_client_base import AppDataClientBase
from ns.model.mas_token import MASToken
from ns.model.user import User
from ns.model.account import Account


class TestMASToken(AppDataClientBase):

    client_id = "2bc078e0-ac6b-4143-9822-2d4b50651994"
    client_secret = "05tA4OEHPGMq7yRu06qWYckZYUCf4S48",
    endpoint = "https://872-HZV-359.mktorest.com"

    def test_save_and_get(self):
        with app.app_context():
            user = mixer.blend(User)
            token_info = {"client_id": "123", "client_secret": "123", "endpoint": "123"}
            db_token = MASToken.save_token(user.account_id, MASToken.MARKETO, token_info)
            self.assertIsNotNone(db_token.id)
            db_token2 = MASToken.save_token(user.account_id, MASToken.MARKETO, token_info)
            self.assertEqual(db_token.id, db_token2.id)
            token_result = MASToken.get_by_account_id_and_mas_type(user.account_id, MASToken.MARKETO)
            self.assertEqual(token_result.mas_id, "123")

    def test_get_marketo_list(self):

        with app.app_context():
            mixer.init_app(app)
            user = User.get_user_by_email('test@everstring.com')
            token_info = {"client_id": self.client_id, "client_secret": self.client_secret, "endpoint": self.endpoint}
            db_token = MASToken.save_token(user.account_id, MASToken.MARKETO, token_info)
            self.assertIsNotNone(db_token.id)
            res = self.client.get('/mas/marketo_list?mas_type={mas_type}'.format(mas_type=MASToken.MARKETO))

    def test_get_marketo_lead_fields(self):

        with app.app_context():
            mixer.init_app(app)
            user = User.get_user_by_email('test@everstring.com')
            token_info = {"client_id": self.client_id, "client_secret": self.client_secret, "endpoint": self.endpoint}
            db_token = MASToken.save_token(user.account_id, MASToken.MARKETO, token_info)
            self.assertIsNotNone(db_token.id)
            res = self.client.get('/mas/marketo_lead_fields?mas_type={mas_type}'.format(mas_type=MASToken.MARKETO))






