import httplib
import calendar
from faker.utils.datetime_safe import datetime
import datetime as py_datetime
from datetime import timedelta
from flask import json
from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.account import Account
from ns.model.role import Role
from ns.model.account_quota import AccountQuota
from ns.model.account_quota_ledger import AccountQuotaLedger
from ns.model.account_exported_company import AccountExportedCompany
from ns.model.company_insight import CompanyInsight
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.user_role import UserRole
from http_client_base import AppHttpClientBase


class TestAdmin(AppHttpClientBase):

    def setUp(self):
        super(TestAdmin, self).setUp()
        app.testing = True

    def test_get_user_account(self):
        with app.app_context():
            acct_cnt = 2
            user_cnt = 5

            mixer.cycle(acct_cnt).blend(Account)
            mixer.cycle(user_cnt).blend(User, account=mixer.SELECT)

            url = '/users'

            resp = self.client.get(url)
            data = json.loads(resp.data)

            # Assert user counts
            # This might not contain all the users once the pagination is added.
            self.assertEqual(len(data['data']), user_cnt)
            self.assertEqual(data['total'], user_cnt)

    def test_get_user_ok(self):
        with app.app_context():
            account = mixer.blend(Account)
            user = mixer.blend(User, account=account)
            mixer.cycle().blend(UserRole, user=user)

            url = '/users/%d' % user.id

            resp = self.client.get(url)
            data = json.loads(resp.data)

            self.assertDictEqual({
                'accountId': user.account.id,
                'accountName': user.account.name,

                'id': user.id,
                'name': user.name,
                'phone': user.phone,
                'email': user.email,
                'activated': user.activated,
                'firstName': user.first_name,
                'lastName': user.last_name,

                'roles': user.role_names,
                'status': user.status,
                'createdTs': calendar.timegm(user.created_ts.utctimetuple()),
                'contactExportFlag': user.contact_export_flag,
                'enableInCrm': False,
                'enableNotInCrm': False,
                'isPrimary': account.is_primary,
                'domain': account.domain
            }, data['data'])

    def test_get_user_not_found(self):
        url = '/users/%d' % -1

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, httplib.NOT_FOUND)

    def test_get_account_quota(self):
        with app.app_context():
            insight_cnt = 5

            exposed_ui_insights = mixer.cycle(insight_cnt).blend(CompanyInsight)
            ids = [insight.id for insight in exposed_ui_insights]
            insight_display_names = [insight.display_name for insight in exposed_ui_insights]

            acct_quota = mixer.blend(AccountQuota,
                                     exposed_ui_insight_ids=str(ids),
                                     exposed_csv_insight_ids='[]',
                                     real_time_scoring_insight_ids='[]',
                                     enable_in_crm=False,
                                     enable_not_in_crm=False)

            url = '/accounts/%d/quotas' % acct_quota.account.id

            resp = self.client.get(url)
            data = json.loads(resp.data)['data']

            self.assertDictContainsSubset({
                'segmentCnt': acct_quota.segment_cnt,
                'UICompanyLimit': acct_quota.ui_company_limit,
                'CSVCompanyLimit': acct_quota.csv_company_limit,
                'contactLimit': acct_quota.contact_limit,
                'exposedCSVInsights': [],
                'expiredTs': acct_quota.expired_ts,
                "enableInCrm": acct_quota.enable_in_crm,
                "enableNotInCrm": acct_quota.enable_not_in_crm,
                "realTimeScoringInsights": []
            }, data)

            self.assertItemsEqual(insight_display_names, data.get('exposedUIInsights'))

    def test_get_account_quota_includes_usage(self):
        with app.app_context():
            acct = mixer.blend(Account)
            mixer.blend(AccountQuota, account=acct,
                        exposed_ui_insight_ids='[]',
                        exposed_csv_insight_ids='[]',
                        real_time_scoring_insight_ids='[]')

            sgmt_cnt = 5
            mixer.cycle(sgmt_cnt).blend(Segment, owner__account=acct, is_deleted=False,
                                        status=Segment.DRAFT)

            url = '/accounts/%d/quotas' % acct.id

            resp = self.client.get(url, query_string={'includes_usage': True})
            data = json.loads(resp.data)['data']

            self.assertDictContainsSubset({
                'usedSegmentCnt': sgmt_cnt,
            }, data)

    def test_get_account_info_ok(self):
        with app.app_context():
            acct = mixer.blend(Account)
            url = '/accounts/%d' % acct.id

            resp = self.client.get(url)
            data = json.loads(resp.data)['data']

            self.assertDictEqual({
                'id': acct.id,
                'name': acct.name,
                'accountType': acct.account_type,
                'totalCreatedAudiences': len(acct.all_owned_audiences()),
                'domain': acct.domain,
                'isPrimary': acct.is_primary,
                'realtimeScoring': '',
                'inboundLeadsTarget': ["account"],
                'quotas': {}
            }, data)

    def test_get_account_info_not_found(self):
        with app.app_context():
            not_existed_id = 100
            url = '/accounts/%d' % not_existed_id

            resp = self.client.get(url)
            self.assertEqual(resp.status_code, httplib.NOT_FOUND)

            self.assertIn(str(not_existed_id), resp.data)

    def test_update_account_info_partial(self):
        with app.app_context():
            acct = mixer.blend(Account, name='old-name', account_type=Account.PAYING)
            mixer.blend(AccountQuota, account=acct,
                        exposed_csv_insight_ids="[1,2]",
                        exposed_ui_insight_ids="[1,2]",
                        real_time_scoring_insight_ids='[]')
            new_name = 'new-name'

            url = '/accounts/%d' % acct.id
            payload = json.dumps({'account': {'name': new_name}})
            resp = self.client.put(url, data=payload, content_type='application/json')
            data = json.loads(resp.data)['data']

            self.assertDictContainsSubset({
                'name': new_name
            }, data)

    def test_update_account_info_invalid_account_type(self):
        with app.app_context():
            acct = mixer.blend(Account, name='old-name', account_type=Account.PAYING)
            mixer.blend(AccountQuota, account=acct,
                        exposed_csv_insight_ids="[1,2]",
                        exposed_ui_insight_ids="[1,2]",
                        real_time_scoring_insight_ids='[]')
            #
            url = '/accounts/%d' % acct.id
            # payload = json.dumps({'account': {"name": 'test_name', 'accountType': 'foo'}})
            payload = json.dumps({'account': {'accountType': "foo"}})

            resp = self.client.put(url, data=payload, content_type='application/json')

            self.assertEqual(resp.status_code, httplib.UNPROCESSABLE_ENTITY)

    def test_update_user_info(self):
        with app.app_context():
            user = mixer.blend(User, email='test@everstring.com')
            role = mixer.blend(Role, name=Role.ES_SUPER_ADMIN)
            mixer.blend(UserRole, user=user, role=role)

            first_name, last_name = 'Foo', 'Buz'

            url = '/users/%d' % user.id
            payload = json.dumps({'user': {'lastName': last_name, 'firstName': first_name}})

            resp = self.client.put(url, data=payload, content_type='application/json')
            data = json.loads(resp.data)['data']

            self.assertDictContainsSubset({
                'lastName': last_name,
                'firstName': first_name
            }, data)

    def test_update_account_quotas(self):
        with app.app_context():
            insights = []
            insights.append(mixer.blend(CompanyInsight, display_name="companyName"))
            insights.append(mixer.blend(CompanyInsight, display_name="companyId"))
            insights.append(mixer.blend(CompanyInsight, display_name="domain"))
            insight_ids = [insight.id for insight in insights]

            acct_quota = mixer.blend(AccountQuota, exposed_ui_insight_ids=str(insight_ids),
                                     exposed_csv_insight_ids='[]', enable_ads=True,
                                     real_time_scoring_insight_ids='[]')
            acct = mixer.blend(Account, account_quota=acct_quota)

            self.assertEqual(len(json.loads(acct_quota.exposed_ui_insight_ids)), 3)

            segment_cnt = 5
            new_insights = insights[1:]
            new_insight_display_names = [insight.display_name for insight in new_insights]

            url = '/accounts/%d/quotas' % acct.id

            # Align with the precision in database
            expired_at = datetime.utcnow().replace(microsecond=0).isoformat()
            start_at = (datetime.utcnow().replace(microsecond=0) - py_datetime.timedelta(days=365)).isoformat()

            payload = json.dumps({
                'quotas': {
                    'segmentCnt': segment_cnt,
                    'exposedUIInsights': ','.join(new_insight_display_names),
                    'startTs': start_at,
                    'expiredTs': expired_at,
                    'inboundLeadsTarget': [],
                    "realTimeScoringInsights": [],
                    'enableInCrm': True,
                    'enableNotInCrm': True,
                    "realtimeScoring": Account.REALTIME_SCORING_AUDIENCE
                }
            })

            resp = self.client.put(url, data=payload, content_type='application/json')
            data = json.loads(resp.data)['data']

            self.assertDictContainsSubset({
                'CSVCompanyLimit': acct_quota.csv_company_limit,
                'UICompanyLimit': acct_quota.ui_company_limit,
                'contactLimit': acct_quota.contact_limit,
                'segmentCnt': acct_quota.segment_cnt,
                'exposedUIInsights': new_insight_display_names,
                'exposedCSVInsights': [],
                'startTs': start_at,
                'expiredTs': expired_at,
                'enableAds': acct_quota.enable_ads
            }, data)
            self.assertEqual(len(data["exposedUIInsights"]), 2)

    def test_adjust_account_quotas(self):
        with app.app_context():

            acct = mixer.blend(Account)
            mixer.blend(AccountQuota, account=acct,
                                             expired_ts=datetime.utcnow() - timedelta(days=10),
                                             csv_company_limit=100)

            mixer.blend(AccountExportedCompany, id=1, account=acct, company_id=1)
            mixer.blend(AccountExportedCompany, id=2, account=acct, company_id=2)
            mixer.blend(AccountExportedCompany, id=3, account=acct, company_id=3)
            mixer.blend(AccountExportedCompany, id=4, account=acct, company_id=4)
            mixer.blend(AccountExportedCompany, id=5, account=acct, company_id=5)

            # create user to initialize g.user
            user = mixer.blend(
                User,
                account=acct,
                email="test@everstring.com")

            url = '/accounts/%d/quotas/adjust' % acct.id

            payload = json.dumps({
                'value': 3,
                'description': 'testing refund'
            })

            ledger = mixer.blend(AccountQuotaLedger, id=1, account=acct, user=user, op_code='ADJ')
            self.assertEqual(ledger.account_id, acct.id)
            self.assertEqual(ledger.user_id, user.id)

            resp = self.client.post(url, data=payload, content_type='application/json')
            self.assertEqual(resp.status_code, 400)
            self.assertEqual(resp.data,
                             '{\n  "message": "Account is expired. Can not adjust publish quota."\n}')
