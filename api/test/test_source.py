from mixer.backend.flask import mixer
from ns.controller.app import app
from ns.model.source import Source
from base_case import AppTestBase


class TestSource(AppTestBase):

    def test_get_name(self):
        with app.app_context():
            source_name = 'CSV'

            mixer.blend(Source, source_name=source_name)
            source = Source.get_by_name(source_name)
            self.assertEqual(source.source_name, source_name)
