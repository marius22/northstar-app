from data_http_client_base import AppDataClientBase
from ns.controller.app import app
import json
from mixer.backend.flask import mixer
from ns.model.indicator_white_list import IndicatorWhiteList


class TestInsights(AppDataClientBase):

    def test_get_insights_categories(self):
        with app.app_context():

            mixer.blend(IndicatorWhiteList, account_id=self.current_account.id, job_id=1, category="tech")
            mixer.blend(IndicatorWhiteList, account_id=self.current_account.id, job_id=1, category="industry")

            resp = self.client.get("/insights/categories")
            self.assertEqual(resp.status_code, 200)

            result = json.loads(resp.data).get("data")
            self.assertItemsEqual(result, ["tech", "industry"])

    def test_get_category_details(self):
        with app.app_context():

            mixer.blend(IndicatorWhiteList,
                        account_id=self.current_account.id,
                        category="tech",
                        indicator="tech1",
                        display_name="Tech1")

            mixer.blend(IndicatorWhiteList,
                        account_id=self.current_account.id,
                        category="tech",
                        indicator="tech2",
                        display_name="Tech2")

            resp = self.client.get("/insights/%s/details" % "tech")
            self.assertEqual(resp.status_code, 200)

            result = json.loads(resp.data).get("data")
            self.assertItemsEqual(result, [{"key": "tech1", "value": "Tech1"},
                                           {"key": "tech2", "value": "Tech2"}])
