# whether on debugging features in Flask
DEBUG = False

# Whether to log sql statement to stderr
SQLALCHEMY_ECHO = False

# DB connection
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://northstar:Mt1p5SXrOl@es-mysql-dev.c0pjeiejow0t.us-east-1.rds.amazonaws.com:3306/northstar?charset=utf8"

SQLALCHEMY_BINDS = {
   "ads" : "mysql+pymysql://root:everstring_cn_123@192.168.20.247:3306/ads?charset=utf8",
    'data_tool_manager': "mysql+pymysql://root:everstring_cn_123@192.168.20.247:3306/ns_app_with_data_manager?charset=utf8",
    'data_tool_source_data': "redshift+psycopg2://dataadmin:6ixCIOBHjP@es-redshift-test.c3pfmkolah0h.us-east-1.redshift.amazonaws.com:5439/company_contact",
    'redshift': 'redshift+psycopg2://deveditor:kwC4Bhfa@es-redshift-test.c3pfmkolah0h.us-east-1.redshift.amazonaws.com:5439/northstar_dev'
}
SQLALCHEMY_TRACK_MODIFICATIONS = True

PORT = 9000


# seed list csv file upload
SEED_CSV_PATH_PREFIX = '/tmp'
SEED_CSV_ALLOWED_FILE_SUFFIX = set(['csv'])
MAX_SEED_CSV_ROW_COUNTS = 50000
COMPANY_SERVICE_HOST = "http://service.uat.everstring.com"
ENRICH_BATCH_SIZE = 1000


# email client config
EMAIL_CLIENT_FROMADDR = "noreply@everstring.com"
EMAIL_CLIENT_USERNAME = "AKIAIEMZ4Z6JKNPW2Q3Q"
EMAIL_CLIENT_PASSWORD = "please contact kangkangh to get the password"
EMAIL_CLIENT_SMTP_SERVER = "email-smtp.us-east-1.amazonaws.com:587"
EMAIL_ENABLED = True

COMPANY_SERVICE_BASE_URL = 'http://service.uat.everstring.com/company_enrichment'
APP_SERVICE_BASE_URL = 'http://northstar.uat.everstring.com/service'


# user management
PERMANENT_SESSION_LIFETIME = 3600 * 7 * 24
SESSION_REFRESH_EACH_REQUEST = False
SESSION_COOKIE_DOMAIN = '.everstring.com'
SESSION_COOKIE_NAME = 'NS_TICKET'
SESSION_COOKIE_LOGIN_ATTEMPTS_NAME = '$NS_LOGIN_ATTEMPTS'
#REDIS_URL = "redis://dev-redis.9mfsrq.0001.use1.cache.amazonaws.com:6379"
REDIS_URL = "redis://127.0.0.1:6379"

AUTH_ENABLE = True

NS_STAR_APP_API_HOST = "http://ns.everstring.com:5000/app"
NS_STAR_APP_HOST = "http://ns.everstring.com:5000"

# Role based access enabled
ROLE_ENABLED = True

EMPLOYEE_SIZE_BUCKETS = [
    {'max': 10, 'label': '1-10'},
    {'max': 20, 'label': '11-20'},
    {'max': 50, 'label': '21-50'},
    {'max': 100, 'label': '51-100'},
    {'max': 200, 'label': '101-200'},
    {'max': 500, 'label': '201-500'},
    {'max': 1000, 'label': '501-1,000'},
    {'max': 2000, 'label': '1,001-2,000'},
    {'max': 5000, 'label': '2,001-5,000'},
    {'max': 10000, 'label': '5,001-10,000'},
    {'max': float('inf'), 'label': '10,000+'},
]

REVENUE_BUCKETS = [
    {'max': 1e6, 'label': '$0M-$1M'},
    {'max': 5e6, 'label': '$1M-$5M'},
    {'max': 10e6, 'label': '$5M-$10M'},
    {'max': 25e6, 'label': '$10M-$25M'},
    {'max': 50e6, 'label': '$25M-$50M'},
    {'max': 100e6, 'label': '$50M-$100M'},
    {'max': 250e6, 'label': '$100M-$250M'},
    {'max': 500e6, 'label': '$250M-$500M'},
    {'max': 1e9, 'label': '$500M-$1B'},
    {'max': 5e9, 'label': '$1B-$5B'},
    {'max': float('inf'), 'label': '$5B+'}
]


# DEBUG_ROLE_PLAY_ENABLED = False

EMPLOYEE_SIZE_MAP = {
    '1-10': {'min': 0, 'max': 10},
    '11-20': {'min': 10, 'max': 20},
    '21-50': {'min': 20, 'max': 50},
    '51-100': {'min': 50, 'max': 100},
    '101-200': {'min': 100, 'max': 200},
    '201-500': {'min': 200, 'max': 500},
    '501-1,000': {'min': 500, 'max': 1000},
    '1,001-2,000': {'min': 1000, 'max': 2000},
    '2,001-5,000': {'min': 2000, 'max': 5000},
    '5,001-10,000': {'min': 5000, 'max': 10000},
    '10,000+': {'min': 10000, 'max': 100000000},
}

REVENUE_MAP = {
    '$0M-$1M': {'min': 0, 'max': 1e6},
    '$1M-$5M': {'min': 1e6, 'max': 5e6},
    '$5M-$10M': {'min': 5e6, 'max': 10e6},
    '$10M-$25M': {'min': 10e6, 'max': 25e6},
    '$25M-$50M': {'min': 25e6, 'max': 50e6},
    '$50M-$100M': {'min': 50e6, 'max': 100e6},
    '$100M-$250M': {'min': 100e6, 'max': 250e6},
    '$250M-$500M': {'min': 250e6, 'max': 500e6},
    '$500M-$1B': {'min': 500e6, 'max': 1e9},
    '$1B-$5B': {'min': 1e9, 'max': 5e9},
    '$5B+': {'min': 5e9, 'max': 100e9},
}

SFDC_APP_COMPANY_INSIGHTS_HEADERS = [
   {"key": "revenue", "value": "Revenue"},
   {"key": "industry", "value": "Industry"},
   {"key": "phone", "value": "Phone"},
   {"key": "employeeSize", "value": "Employee Size"},
   {"key": "zipcode", "value": "Zip Code"},
   {"key": "state", "value": "State"},
   ]
# DEBUG_ROLE_PLAY_ENABLED = False


# config for salesforce
AUTH_URL = 'https://login.salesforce.com/services/oauth2/authorize'
TOKEN_URL = 'https://login.salesforce.com/services/oauth2/token'
CLIENT_ID = '3MVG9uudbyLbNPZPK8RGQJy7NMhH_IPflgzhC0XPHMMKYiniNCNallh.c_5LUiB5BD8y3ju1clStvLiDkQ5h4'
CLIENT_SECRET = '9143526117991959181'
CALL_BACK_URL = 'https://ns.everstring.com/api/salesforce/call_back'

TRIAL_ACCOUNT_QUOTA_SEGMENT_COUNT = 1
TRIAL_ACCOUNT_QUOTA_EXPIRE_IN_MONTHS = 12
TRIAL_ACCOUNT_QUOTA_EXPOSED_UI_INSIGHTS = ['companyName', 'revenue']
TRIAL_ACCOUNT_QUOTA_EXPOSED_CSV_INSIGHTS = ['companyName', 'revenue']
TRIAL_ACCOUNT_QUOTA_UI_COMPANY_LIMIT = 5
TRIAL_ACCOUNT_QUOTA_CSV_COMPANY_LIMIT = 5

DEPARTMENT = ["Administrative", "IT", "Education", "Engineering", "Finance", "HR", "Legal",
              "Marketing", "Health", "Operations", "Other", "Ownership & Board", "R&D", "Sales"]
SENIORITY = ["C-Level", "VP", "Director", "Manager", "Regular"]


AWS_ACCESS_KEY_ID = 'example'
AWS_SECRET_ACCESS_KEY = 'example'
S3_CSV_BUCKET = 'es-northstar'

PACKAGE_LINK = 'https://login.salesforce.com/packaging/installPackage.apexp?p0=04t360000015vpk'
PACKAGE_GUIDE_LINK = 'https://support.everstring.com/hc/en-us/articles/224111608-Installing-the-Salesforce-Managed-Package-and-Canvas-App-'

MAX_WORKERS = 20
ZENDESK_SUB_DOMAIN = 'everstring'
ZENDESK_API_KEY ='MjWCGt4xF8z2GnzUYRfWs0rXQrzn1swLoviaWlZ2mGRrKdCY'

import datetime
TIME_TO_ALLOW_USE_PREVIOUS_VERSION = datetime.datetime.now()

# Usage Tracking configs
# turns freegeoip on or off (if set to on, will use default endpoint as http://freegeoip.net/json/
# however, it can be changedby using TRACK_USAGE_FREEGEOIP_ENDPOINT  and pointing to diff endpoint
TRACK_USAGE_USE_FREEGEOIP = False
# If endpoints / views should be included or excluded by default
# (default value by flask is 'exclude' meaning it tracks all endpoints)
TRACK_USAGE_INCLUDE_OR_EXCLUDE_VIEWS = 'include'
