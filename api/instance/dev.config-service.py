# whether on debugging features in Flask
DEBUG = True

# Whether to log sql statement to stderr
SQLALCHEMY_ECHO = False

# DB connection
SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:everstring_cn_123@192.168.20.247:3306/ns_app_with_corp?charset=utf8"

SQLALCHEMY_TRACK_MODIFICATIONS = True

PORT = 7000

# for segment summary email
NS_STAR_APP_HOST = "https://ns.everstring.com"
EMAIL_CLIENT_FROMADDR = "noreply@everstring.com"
EMAIL_CLIENT_USERNAME = "AKIAIEMZ4Z6JKNPW2Q3Q"
EMAIL_CLIENT_PASSWORD = "please contact kangkangh to get the password"
EMAIL_CLIENT_SMTP_SERVER = "email-smtp.us-east-1.amazonaws.com:587"
EMAIL_ENABLED = True

KAFKA_BROKERS = ['localhost:9092']
KAFKA_ENABLED = False
KAFKA_TOPIC_JOB = "ns-uat-job-submission"
KAFKA_TOPIC_FEEDBACK = "ns-uat-feedback"
KAFKA_TOPIC_EXPORT_CSV = "ns-uat-export-csv"

NS_SERVICE_BASE_URL = 'https://northstar.uat.everstring.com/service'
