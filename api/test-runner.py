import sys
import os

if __name__ == '__main__':
    import nose2
    os.environ['ENV'] = 'test'

    sys.exit(nose2.discover())
