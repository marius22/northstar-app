# -*- coding: utf-8 -*-


class InsightConstants(object):

    TABLE_FIELD_ID = "id"
    TABLE_FIELD_INSIGHT_NAME = "displayName"
    TABLE_FIELD_COMPANY_TABLE_COLUMN = "companyColumnName"

    INSIGHT_COMPANY_BASIC_INSIGHTS = "company_basic_insights"
    INSIGHT_COMPANY_MODEL_SCORES = "company_model_scores"
    INSIGHT_COMPANY_ADVANCED_INSIGHTS = "company_advanced_insights"

    INSIGHT_NAME_ALEXA_RANK = "alexaRank"
    INSIGHT_NAME_CITY = "city"
    INSIGHT_NAME_COMPANY_NAME = "companyName"
    INSIGHT_NAME_COMPANY_PHONE = "companyPhone"
    INSIGHT_NAME_COUNTRY = "country"
    INSIGHT_NAME_DOMAIN = "domain"
    INSIGHT_NAME_EMPLOYEE_SIZE = "employeeSize"
    INSIGHT_NAME_GROWTH = "growth"
    INSIGHT_NAME_INDUSTRY = "industry"
    INSIGHT_NAME_LINKEDIN_URL = "linkedinUrl"
    INSIGHT_NAME_REVENUE = "revenue"
    INSIGHT_NAME_STATE = "state"
    INSIGHT_NAME_STREET = "street"
    INSIGHT_NAME_TECHNOLOGY = "tech"
    INSIGHT_NAME_ZIPCODE = "zipcode"

    INSIGHT_NAME_FACEBOOK_URL = "facebookUrl"
    INSIGHT_NAME_TWITTER_URL = "twitterUrl"
    INSIGHT_NAME_AUDIENCE_NAMES = "audienceNames"
    INSIGHT_NAME_KEYWORDS = "keywords"
    INSIGHT_NAME_B2B = "businessToBusiness"
    INSIGHT_NAME_B2C = "businessToConsumer"
    INSIGHT_NAME_MARKETING_SOPHISTICATION = "marketingSophistication"
    INSIGHT_NAME_FACILITIES_IN_MULTIPLE_LOCATIONS = "facilitiesInMultipleLocations"
    INSIGHT_NAME_EMPLOYEES_IN_MULTIPLE_LOCATIONS = "employeesInMultipleLocations"

    INSIGHT_NAME_ES_ENRICHED = "esEnriched"
    INSIGHT_NAME_ES_SOURCE = "esSource"

    INSIGHT_DELIMITER = "|"

    INSIGHT_NAME_OVERALL_FIT_SCORE = "overallFitScore"

    ADVANCED_INSIGHT_NAME_EXACT_NAME_MAP = {
        INSIGHT_NAME_B2B: "Business-to-Business (B2B)",
        INSIGHT_NAME_B2C: "Business-to-Consumer (B2C)",
        INSIGHT_NAME_MARKETING_SOPHISTICATION: "Marketing Sophistication",
        INSIGHT_NAME_FACILITIES_IN_MULTIPLE_LOCATIONS: "Facilities in Multiple Locations",
        INSIGHT_NAME_EMPLOYEES_IN_MULTIPLE_LOCATIONS: "Employees in Multiple Locations"
    }

    ADVANCED_INSIGHTS = ADVANCED_INSIGHT_NAME_EXACT_NAME_MAP.keys()

    # This is a subset of SUPPORTED_PUBLISH_INSIGHTS that directly live on the company table.
    # Can use this as the "select" parameter to our /company_enrichment/profile endpoint
    # on our company enrichment service.
    COMPANY_TABLE_INSIGHTS = [
        INSIGHT_NAME_ALEXA_RANK,
        INSIGHT_NAME_CITY,
        INSIGHT_NAME_COMPANY_PHONE,
        INSIGHT_NAME_COUNTRY,
        INSIGHT_NAME_EMPLOYEE_SIZE,
        INSIGHT_NAME_INDUSTRY,
        INSIGHT_NAME_LINKEDIN_URL,
        INSIGHT_NAME_REVENUE,
        INSIGHT_NAME_STATE,
        INSIGHT_NAME_STREET,
        INSIGHT_NAME_ZIPCODE,
        INSIGHT_NAME_FACEBOOK_URL,
        INSIGHT_NAME_TWITTER_URL,
        # Company name isn't mappable but we always publish this.
        # Domain is always included by the /company_enrichment/profile endpoint,
        # so we don't need to include it here.
        INSIGHT_NAME_COMPANY_NAME
    ]

    SUPPORTED_PUBLISH_INSIGHTS = [
        INSIGHT_NAME_ALEXA_RANK,
        INSIGHT_NAME_CITY,
        INSIGHT_NAME_COMPANY_PHONE,
        INSIGHT_NAME_COUNTRY,
        INSIGHT_NAME_EMPLOYEE_SIZE,
        INSIGHT_NAME_INDUSTRY,
        INSIGHT_NAME_LINKEDIN_URL,
        INSIGHT_NAME_REVENUE,
        INSIGHT_NAME_STATE,
        INSIGHT_NAME_STREET,
        INSIGHT_NAME_TECHNOLOGY,
        INSIGHT_NAME_ZIPCODE,
        INSIGHT_NAME_FACEBOOK_URL,
        INSIGHT_NAME_TWITTER_URL,
        INSIGHT_NAME_AUDIENCE_NAMES,
        INSIGHT_NAME_KEYWORDS,
        INSIGHT_NAME_B2B,
        INSIGHT_NAME_B2C,
        INSIGHT_NAME_MARKETING_SOPHISTICATION,
        INSIGHT_NAME_FACILITIES_IN_MULTIPLE_LOCATIONS,
        INSIGHT_NAME_EMPLOYEES_IN_MULTIPLE_LOCATIONS
    ]

    INSIGHT_NAME_TECHNOLOGY_MAX_LENGTH = 4000
