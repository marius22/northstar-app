from insight_constants import InsightConstants

# default field mappings for sfdc, csv, marketo
SFDC_PACKAGE_NAMESPACE_PREFIX = "ES_APP__"
SFDC_CUSTOM_COMPONENT_SUFFIX = "__c"

# standard target types
# can later add in hubspot, dynamics, etc.
TARGET_TYPE_SALESFORCE = "salesforce"
TARGET_TYPE_CSV = "csv"
TARGET_TYPE_MARKETO = "marketo"
TARGET_TYPE_MARKETO_SANDBOX = "marketo_sandbox"

# standard target objects
TARGET_OBJECT_LEAD = "lead"
TARGET_OBJECT_CONTACT = "contact"
TARGET_OBJECT_ACCOUNT = "account"
TARGET_OBJECT_COMPANY = "company"

# do not use, use DEFAULT_MAPPING at bottom of file
_DEFAULT_SFDC_OBJECT_MAPPING_API_NAMES = {
    InsightConstants.INSIGHT_NAME_ALEXA_RANK: "ESAlexaRank",
    InsightConstants.INSIGHT_NAME_CITY: "ESCity",
    InsightConstants.INSIGHT_NAME_COMPANY_PHONE: "ESCompanyPhone",
    InsightConstants.INSIGHT_NAME_COUNTRY: "ESCountry",
    InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE: "ESEmployees",
    InsightConstants.INSIGHT_NAME_INDUSTRY: "ESIndustry",
    InsightConstants.INSIGHT_NAME_LINKEDIN_URL: "ESLinkedIn",
    InsightConstants.INSIGHT_NAME_REVENUE: "ESRevenue",
    InsightConstants.INSIGHT_NAME_STATE: "ESState",
    InsightConstants.INSIGHT_NAME_STREET: "ESStreet",
    InsightConstants.INSIGHT_NAME_TECHNOLOGY: "ESTechnologies",
    InsightConstants.INSIGHT_NAME_ZIPCODE: "ESZipcode",
    InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE: "ESOverallFitScore",
    InsightConstants.INSIGHT_NAME_FACEBOOK_URL: "ESFacebook",
    InsightConstants.INSIGHT_NAME_TWITTER_URL: "ESTwitter",
    InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES: "ESAudienceNames",
    InsightConstants.INSIGHT_NAME_KEYWORDS: "ESKeywords"
}

# do not use, use DEFAULT_MAPPING at bottom of file
_SFDC_ACCOUNTS_LEADS_MAPPING = {
    display_name: SFDC_PACKAGE_NAMESPACE_PREFIX + api_name +
    SFDC_CUSTOM_COMPONENT_SUFFIX for display_name, api_name in
    _DEFAULT_SFDC_OBJECT_MAPPING_API_NAMES.iteritems()
}

_SFDC_CONTACTS_MAPPING = {InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE:
                          SFDC_PACKAGE_NAMESPACE_PREFIX +
                          _DEFAULT_SFDC_OBJECT_MAPPING_API_NAMES.get(
                              InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE) +
                          SFDC_CUSTOM_COMPONENT_SUFFIX}

# do not use, use DEFAULT_MAPPING at bottom of file
DEFAULT_CSV_MAPPING = {
    InsightConstants.INSIGHT_NAME_ALEXA_RANK: "Alexa Rank",
    InsightConstants.INSIGHT_NAME_CITY: "City",
    InsightConstants.INSIGHT_NAME_COMPANY_PHONE: "Company Phone",
    InsightConstants.INSIGHT_NAME_COUNTRY: "Country",
    InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE: "Employees(Category)",
    InsightConstants.INSIGHT_NAME_INDUSTRY: "Industry",
    InsightConstants.INSIGHT_NAME_LINKEDIN_URL: "LinkedIn Url",
    InsightConstants.INSIGHT_NAME_REVENUE: "Revenue(Category)",
    InsightConstants.INSIGHT_NAME_STATE: "State",
    InsightConstants.INSIGHT_NAME_STREET: "Street",
    InsightConstants.INSIGHT_NAME_TECHNOLOGY: "Technologies",
    InsightConstants.INSIGHT_NAME_ZIPCODE: "Zip",
    InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE: "Overall Fit Score",
    InsightConstants.INSIGHT_NAME_FACEBOOK_URL: "Facebook Url",
    InsightConstants.INSIGHT_NAME_TWITTER_URL: "Twitter Url",
    InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES: "Audience Names",
    InsightConstants.INSIGHT_NAME_KEYWORDS: "Keywords",
    InsightConstants.INSIGHT_NAME_B2B: "B2B",
    InsightConstants.INSIGHT_NAME_B2C: "B2C",
    InsightConstants.INSIGHT_NAME_MARKETING_SOPHISTICATION: "Marketing Sophistication",
    InsightConstants.INSIGHT_NAME_FACILITIES_IN_MULTIPLE_LOCATIONS: "Facilities In Multiple Locations",
    InsightConstants.INSIGHT_NAME_EMPLOYEES_IN_MULTIPLE_LOCATIONS: "Employees In Multiple Locations"
}

# USE THIS!!!  See sample below:
DEFAULT_MAPPING = {
    TARGET_TYPE_SALESFORCE: {
        TARGET_OBJECT_ACCOUNT: _SFDC_ACCOUNTS_LEADS_MAPPING,
        TARGET_OBJECT_CONTACT: _SFDC_CONTACTS_MAPPING,
        TARGET_OBJECT_LEAD: _SFDC_ACCOUNTS_LEADS_MAPPING
    },
    TARGET_TYPE_CSV: {
        TARGET_OBJECT_COMPANY: DEFAULT_CSV_MAPPING
    },
    TARGET_TYPE_MARKETO: {
        TARGET_OBJECT_LEAD: {}
    },
    TARGET_TYPE_MARKETO_SANDBOX: {
        TARGET_OBJECT_LEAD: {}
    }
}


# SAMPLE DEFAULT MAPPING:
# {'csv': {'company': {'alexaRank': 'Alexa Rank',
#                      'audienceNames': 'Audience Names',
#                      'city': 'City',
#                      'companyPhone': 'Company Phone',
#                      'country': 'Country',
#                      'employeeSize': 'Employees(Category)',
#                      'facebookUrl': 'Facebook Url',
#                       ...}},
#  'marketo': {},
#  'salesforce': {'account': {'alexaRank': 'ES_APP__ESAlexaRank__c',
#                             'audienceNames': 'ES_APP__ESAudienceNames__c',
#                             'city': 'ES_APP__ESCity__c',
#                             'companyPhone': 'ES_APP__ESCompanyPhone__c',
#                             'country': 'ES_APP__ESCountry__c',
#                             'employeeSize': 'ES_APP__ESEmployees__c',
#                             'facebookUrl': 'ES_APP__ESFacebookUrl__c',
#                             ...},
#                 'contact': {},
#                 'lead': {'alexaRank': 'ES_APP__ESAlexaRank__c',
#                          'audienceNames': 'ES_APP__ESAudienceNames__c',
#                          'city': 'ES_APP__ESCity__c',
#                          'companyPhone': 'ES_APP__ESCompanyPhone__c',
#                          'country': 'ES_APP__ESCountry__c',
#                          'employeeSize': 'ES_APP__ESEmployees__c',
#                          'facebookUrl': 'ES_APP__ESFacebookUrl__c',
#                          ...}}}

CONTACT_DEFAULT_FIELD_MAPPING_FOR_MAS = {
    "mainPhone": "directPhone",
    "email": "email",
    "firstName": "firstName",
    "lastName": "lastName",
    "title": "jobTitle"
}
