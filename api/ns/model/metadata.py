from flask import current_app
from ns.util.company_db_client import CompanyDBClient
from ns.util.fuzzy_search_util import fuzzy_search


def _get_categories_by_proxy(category):
    result = CompanyDBClient.instance().proxy(
        "get",
        '/company_enrichment/metadata/company_metadata?category=%s' % category
    )
    return [item for item in result["data"] if item]


class BaseFetcher(object):
    _cache_valid_key = "NSA_metadata_base_valid"
    _cache = None
    _cache_time = 3600 * 24

    @classmethod
    def get_sub_category(cls, *args, **kwargs):
        return []

    @classmethod
    def get_values(cls, *args, **kwargs):
        from ns.controller.app import redis_store

        if not (redis_store.get(cls._cache_valid_key) and cls._cache):
            cls._cache = cls._fetcher_values(*args, **kwargs)
            redis_store.setex(cls._cache_valid_key, cls._cache_time, "true")

        return cls._cache

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return []


class CountryFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_country_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return _get_categories_by_proxy("country")


class StateFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_state_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return _get_categories_by_proxy("state")


class CityFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_city_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return _get_categories_by_proxy("city")


class IndustryFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_industry_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return CompanyDBClient.instance().get_industries()


class RevenueFetcher(BaseFetcher):

    _cache_valid_key = "NSA_metadata_revenue_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        revenue_bucket = current_app.config.get('REVENUE_BUCKETS')
        return [item["label"] for item in revenue_bucket]


class EmployeeSizeFetcher(BaseFetcher):

    _cache_valid_key = "NSA_metadata_employee_size_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        employee_size_bucket = current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
        return [item["label"] for item in employee_size_bucket]


class TechFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_tech_valid"
    _cache = None
    _cache_time = 3600 * 12

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return CompanyDBClient.instance().get_techs()


class GrowthFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_growth_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["High", "Medium", "Low"]


class DepartmentStrengthFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_department_strength_valid"
    _cache = None
    _sub_cats_cache = None

    @classmethod
    def get_sub_category(cls, *args, **kwargs):
        from ns.controller.app import redis_store

        if not (redis_store.get(cls._cache_valid_key) and cls._sub_cats_cache):
            cls._sub_cats_cache = DepartmentFetcher.get_values()
            redis_store.setex(cls._cache_valid_key, cls._cache_time, "true")

        return cls._sub_cats_cache

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["High", "Medium", "Low"]


class ZipcodeFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_zipcode_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return _get_categories_by_proxy("zipcode")


class InCrmFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_in_crm_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["In Accounts", "In Leads"]


class SocialFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_social_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["Has twitter", "Has facebook", "Has LinkedIn", "Has youtube channel"]


class WithContactFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_with_contact_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["Yes", "No"]


class DepartmentFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_department_valid"
    _cache = None
    _cache_time = 3600 * 12

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return _get_categories_by_proxy('department')


class NotInCrmFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_not_in_crm_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["Not in Accounts", "Not in Leads"]


class KeywordsFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_keywords_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        raise NotImplemented("Not implemented fetcher: keywords")


class FitScoreFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_fit_score_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["0-10", "11-20", "21-30", "31-40", "41-50",
                "51-60", "61-70", "71-80", "81-90", "91-100"]


class TerritoryFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_territory_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        data = CompanyDBClient.instance().get_state_zipcode()
        result = []
        # item is a tuple: (state, zipcode)
        for item in data:
            if not item[1]:
                continue
            result.append('-'.join(item) if item[0] else '-'.join(["UNKNOWN", item[1]]))
        result.sort()
        return result


class PublishFetcher(BaseFetcher):
    _cache_valid_key = "NSA_metadata_publish_valid"
    _cache = None

    @classmethod
    def _fetcher_values(cls, *args, **kwargs):
        return ["Published", "Not Published"]


class Metadata(object):

    # keys for categories
    COUNTRY = "country"
    STATE = "state"
    CITY = "city"
    INDUSTRY = "industry"
    REVENUE = "revenue"
    EMPLOYEE_SIZE = "employeeSize"
    TECH = "tech"
    GROWTH = "growth"
    DEPARTMENT_STRENGTH = "departmentStrength"
    ZIPCODE = "zipcode"
    IN_CRM = "inCrm"
    SOCIAL = "social"
    WITH_CONTACT = "withContact"
    DEPARTMENT = "department"
    NOT_IN_CRM = "notInCrm"
    KEYWORDS = "keywords"
    FIT_SCORE = "fitScore"
    TERRITORY = "territory"
    PUBLISH = "publishState"

    metadata_fetcher = {
        COUNTRY: CountryFetcher,
        STATE: StateFetcher,
        INDUSTRY: IndustryFetcher,
        CITY: CityFetcher,
        EMPLOYEE_SIZE: EmployeeSizeFetcher,
        REVENUE: RevenueFetcher,
        TECH: TechFetcher,
        GROWTH: GrowthFetcher,
        DEPARTMENT_STRENGTH: DepartmentStrengthFetcher,
        ZIPCODE: ZipcodeFetcher,
        IN_CRM: InCrmFetcher,
        SOCIAL: SocialFetcher,
        WITH_CONTACT: WithContactFetcher,
        DEPARTMENT: DepartmentFetcher,
        NOT_IN_CRM: NotInCrmFetcher,
        FIT_SCORE: FitScoreFetcher,
        TERRITORY: TerritoryFetcher,
        PUBLISH: PublishFetcher
    }

    @classmethod
    def get_all_categories(cls):
        return cls.metadata_fetcher.keys()

    @classmethod
    def get_values(cls, category, *args, **kwargs):
        fetcher = cls.metadata_fetcher.get(category)
        if not fetcher:
            raise NotImplemented("Not implemented fetcher for category: %s" % category)

        return fetcher.get_values(*args, **kwargs)

    @classmethod
    def get_values_fuzzily(cls, category, keyword, *args, **kwargs):
        collection = cls.get_values(category, *args, **kwargs)
        return fuzzy_search(keyword, collection)

    @classmethod
    def get_sub_categories(cls, category, *args, **kwargs):
        fetcher = cls.metadata_fetcher.get(category)
        if not fetcher:
            raise NotImplemented("Not implemented fetcher for category: %s" % category)

        return fetcher.get_sub_category(*args, **kwargs)
