from sqlalchemy import func
from ns.model.segment import Segment
from ns.util.csv_suite import AbstractCSVDataLoader


class SeedListLoader(AbstractCSVDataLoader):

    def __init__(self, data_source, per_batch):
        super(SeedListLoader, self).__init__(per_batch)
        self.segment = data_source

    def process_ele(self, ele):
        elems = ele.serialized
        # Need single quotes to prevent Excel from autoformating
        # 11 - 50 becomes Nov 50
        if elems['employeeSize']:
            elems['employeeSize'] = "'" + elems['employeeSize'] + "'"
        return elems

    def get_headers(self):
        return [
            'companyId',
            'companyName',
            'domain',
            'revenue',
            'industry',
            'employeeSize',
            'alexaRank',
            "country",
            "state",
            "city",
            "tech",
            "SFDCAccountId",
            "SFDCAccountName"
        ]

    def total(self):
        return self.segment.seed_candidates.with_entities(func.count(Segment.id)).scalar()

    def get_batch(self, start, end):
        limit = end - start
        return self.segment.seed_candidates.offset(start).limit(limit)
