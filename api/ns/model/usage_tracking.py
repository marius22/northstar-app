from flask_track_usage import TrackUsage
from flask.globals import _request_ctx_stack
from threading import Thread, Lock, current_thread
import copy
from time import gmtime, strftime
import logging
import base64
from werkzeug import exceptions
from ns.util.auth_util import get_token_str

logger = logging.getLogger(__name__)


class ESTrackUsage(TrackUsage):

    def __init__(self, app=None, storage=None, ctx=None, response=None):
        try:
            super(ESTrackUsage, self).__init__(app, storage)
            self.ctx = ctx
            self.response = response
            self.data = {}
            self.lock = Lock()
        except Exception as err:
            logger.error(msg="__init__: " + err.message)

    def init_app(self, app, storage):
        try:
            super(ESTrackUsage, self).init_app(app, storage)
        except Exception as err:
            logger.error(msg="init_app: " + err.message)

    def before_request(self):
        write_to_db = False
        try:
            request_timestamp = strftime("%Y-%m-%d %H:%M:%S", gmtime())

            super(ESTrackUsage, self).before_request()

            view_func = self.app.view_functions.get(_request_ctx_stack.top.request.endpoint)
            if self._type == 'exclude':
                if view_func in self._exclude_views:
                    logger.debug('view_func not in _exclude_views; nothing to log')
                    return
            elif self._type == 'include':
                if view_func not in self._include_views:
                    logger.debug('view_func not in _include_views; nothing to log')
                    return
            else:
                raise NotImplementedError('You must set include or exclude type.')

            write_to_db = True
            self.ctx = _request_ctx_stack.top

            token = None
            try:
                token = get_token_str()
            except exceptions.Unauthorized as err:
                logging.error("Getting token failed. " + err.message)

            customer_id = "Not Found"
            try:
                if token:
                    customer_id = base64.b64decode(token).split('.')[0]
            except (TypeError, ValueError, Exception, ) as err:
                logging.error("customer_id could not be extracted : " + err.message)

            self.data = {
                'url': self.ctx.request.url,
                'api_version': self.ctx.request.path.split('/')[1],     # should return v1
                'blueprint': self.ctx.request.blueprint,
                'request_method': self.ctx.request.method,
                'path': self.ctx.request.path,
                'request_timestamp': request_timestamp,
                'ip_address': self.ctx.request.remote_addr,
                'customer_id': customer_id,
                'request_args': self.ctx.request.data or self.ctx.request.query_string,
                'request_size': (str(self.ctx.request.content_length or 0)) + ' bytes',
                'user_agent': self.ctx.request.user_agent.string,
                'authorized': bool(self.ctx.request.headers.get("Authorization")),
                # Note: response metrics default arguments (these will be updated when after_request is called)
                'response_size': '0  bytes',
                'response_status': -1
            }
        except Exception as err:
            logger.error(msg="before_request: " + err.message)
        finally:
            try:
                # spawn a thread only if we are tracking that endpoint (based on include / exclude)
                if write_to_db:
                    before_request_thread = TrackingUsageThread(thread_name="before_request_thread",
                                                                es_track_usage=self, data_copy=copy.deepcopy(self.data))
                    before_request_thread.daemon = True
                    before_request_thread.start()
            except Exception as err:
                logger.error(msg="write_to_db err: " + err.message)

    def after_request(self, response):
        write_to_db = False
        try:
            if response is None:
                return response
            else:
                self.response = response

            if self.ctx is None:
                return self.response

            view_func = self.app.view_functions.get(self.ctx.request.endpoint)
            if self._type == 'exclude':
                if view_func in self._exclude_views:
                    logger.debug('view_func not in _exclude_views; nothing to log')
                    return self.response
            elif self._type == 'include':
                if view_func not in self._include_views:
                    logger.debug('view_func not in _include_views; nothing to log')
                    return self.response
            else:
                raise NotImplementedError(
                    'You must set include or exclude type.')

            write_to_db = True

            # Update self.data dict
            self.data["response_size"] = str(self.response.content_length or 0) + ' bytes',
            self.data["response_status"] = int(self.response.status_code)

        except RuntimeError as runtime_err:
            logger.error(msg="after_request: " + runtime_err.message)
        except Exception as err:
            logger.error(msg="after_request: " + err.message)
        finally:
            try:
                # spawn a thread only if we are tracking that endpoint (based on include / exclude)
                if write_to_db:
                    after_request_thread = TrackingUsageThread(thread_name="after_request_thread", es_track_usage=self,
                                                               data_copy=copy.deepcopy(self.data))
                    after_request_thread.daemon = True
                    after_request_thread.start()
            except Exception as err:
                logger.error(msg="write_to_db err: " + err.message)
            finally:
                return response

    def exclude(self, view):
        try:
            super(ESTrackUsage, self).exclude(view)
        except Exception as err:
            logger.error(msg="exclude: " + err.message)

    def include(self, view):
        try:
            super(ESTrackUsage, self).include(view)
        except Exception as err:
            logger.error(msg="include: " + err.message)

    def _modify_blueprint(self, blueprint, include_type):
        try:
            super(ESTrackUsage, self)._modify_blueprint(blueprint, include_type)
        except Exception as err:
            logger.error(msg="_modify_blueprint: " + err.message)

    def include_blueprint(self, blueprint):
        try:
            super(ESTrackUsage, self).include_blueprint(blueprint)
        except Exception as err:
            logger.error(msg="include_blueprint: " + err.message)

    def exclude_blueprint(self, blueprint):
        try:
            super(ESTrackUsage, self).exclude_blueprint(blueprint)
        except Exception as err:
            logger.error(msg="exclude_blueprint: " + err.message)

    @property
    def storage(self):
        return self._storage

    @storage.setter
    def storage(self, value):
        self._storage = value


# Creates new daemon thread for writing to db storage
class TrackingUsageThread(Thread):

    def __init__(self, thread_name, es_track_usage, data_copy):
        Thread.__init__(self, name=thread_name)
        self.es_track_usage = es_track_usage
        self.lock = es_track_usage.lock
        self.data_copy = data_copy

    def run(self):
        try:
            # acquire lock since we have 2 threads trying to write to db - request and response -request should be first
            self.lock.acquire()
            # Only for debugging: print current_thread(), " has data: ", self.data_copy

            if 'id' not in self.es_track_usage.data:
                record_id = self.es_track_usage.storage(self.data_copy)
                self.es_track_usage.data["id"] = record_id
            else:
                self.es_track_usage.storage(self.es_track_usage.data)

            # Only for debugging: print "finished storing data for: ", current_thread()
        except Exception as err:
            logger.error(msg="write_to_db error: " + err.message)
        finally:
            if self.lock is not None:
                self.lock.release()
