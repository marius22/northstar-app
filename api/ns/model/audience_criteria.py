import datetime
import json

from ns.model import db
from ns.model.base_model import BaseModel


class AudienceCriteria(db.Model):

    __tablename__ = 'audience_criteria'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    audience_id = db.Column(db.Integer, db.ForeignKey("audience.id"), nullable=False)
    criteria_type = db.Column(db.String(50), nullable=False)
    criteria_val = db.Column(db.Text, nullable=False)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    __table_args__ = (
        db.UniqueConstraint(
            'audience_id', 'criteria_type', name="uniq_audience_id_criteria_type"),
    )

    @property
    def criteria_val_obj(self):
        assert self.criteria_val, "Invalid criteria value"
        return json.loads(self.criteria_val)

    @property
    def serialized(self):
        return {
            "id": self.id,
            "audienceId": self.audience_id,
            "criteriaType": self.criteria_type,
            "criteriaVal": self.criteria_val_obj
        }

    @staticmethod
    def get_by_audience_id(audience_id):
        return AudienceCriteria.get_by_audience_id_and_type(audience_id, None)

    @staticmethod
    def get_by_audience_id_and_type(audience_id, types=None):
        query = AudienceCriteria.query.filter(AudienceCriteria.audience_id == audience_id)

        if types:
            query = query.filter(AudienceCriteria.criteria_type.in_(types))

        return query.all()

    @staticmethod
    def save_criteria(audience_id, type, dict_value):
        criteria = AudienceCriteria.get_by_audience_id_and_type(audience_id, [type])
        criteria = criteria[0] if criteria else None

        if not criteria:
            criteria = AudienceCriteria(
                audience_id=audience_id,
                criteria_type=type,
                criteria_val=json.dumps(dict_value)
            )
            db.session.add(criteria)
        else:
            criteria.criteria_val = json.dumps(dict_value)

        db.session.commit()
        return criteria
