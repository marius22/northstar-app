from sqlalchemy import func, and_
from ns.model import db
import datetime


class AccountQuotaLedger(db.Model):
    __tablename__ = 'account_quota_ledger'

    CHANGE_MAX_QUOTA = 'CHG_LIMIT'
    ADJUST_REMAINING_QUOTA = 'ADJ'

    op_code_enums = (CHANGE_MAX_QUOTA, ADJUST_REMAINING_QUOTA)

    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    account_id = db.Column(db.BigInteger, db.ForeignKey("account.id"), nullable=False)
    user_id = db.Column(db.BigInteger, db.ForeignKey("user.id"), nullable=False)
    operation_ts = db.Column(db.TIMESTAMP, nullable=True, default=datetime.datetime.utcnow)
    value = db.Column(db.Integer, nullable=True)
    op_code = db.Column(db.Enum(*op_code_enums), nullable=False)
    description = db.Column(db.String(4096), nullable=True)

    # Get sum of all 'adj' where operation_ts > start_ts and < expired_ts (from account_quota table for that account id)
    @staticmethod
    def adjusted_company_cnt(account_id, start_ts, expired_ts):
        try:
            if not account_id or not start_ts or not expired_ts:
                return 0
            net_adj_value = db.session.query(func.sum(AccountQuotaLedger.value)).\
                filter(AccountQuotaLedger.op_code == AccountQuotaLedger.ADJUST_REMAINING_QUOTA).\
                filter(and_(AccountQuotaLedger.operation_ts >= start_ts,
                            AccountQuotaLedger.operation_ts <= expired_ts)). \
                filter(AccountQuotaLedger.account_id == account_id). \
                scalar()
        except Exception as err:
            raise err
        # net_adj_value will be None if records exist in table, but result of above query is null
        # (or if no acct id found in table)
        return net_adj_value if net_adj_value else 0

    # This is not being used right now; but will be exposed in future when we want to increase / decrease max limit
    # This adds the "difference" to ledger (not exact new max limit)
    # @staticmethod
    # def adjusted_company_max_limit(account_id, start_ts, expired_ts):
    #     net_adj_max_limit_value = db.session.query(func.sum(AccountQuotaLedger.value)). \
    #         filter(AccountQuotaLedger.op_code == AccountQuotaLedger.CHANGE_MAX_QUOTA). \
    #         filter(and_(AccountQuotaLedger.operation_ts >= start_ts,
    #                     AccountQuotaLedger.operation_ts <= expired_ts)). \
    #         filter(AccountQuotaLedger.account_id == account_id). \
    #         scalar()

    #     return net_adj_max_limit_value

    @staticmethod
    def create_entry(account_id=None, user_id=None, value=0,
                     op_code=None, operation_ts=datetime.datetime.utcnow(), description='Default desc'):
        if not op_code or not account_id or not user_id:
            return Exception("operation type (op_code) must be specified")

        # we don' want to add 0 value (which is no adjustment)
        if not value:
            return

        account_quota_ledger_obj = AccountQuotaLedger(
            account_id=account_id,
            user_id=user_id,
            op_code=op_code,
            operation_ts=operation_ts,
            value=value,
            description=description
        )

        db.session.add(account_quota_ledger_obj)
        db.session.commit()
