from flask_track_usage.storage.sql import SQLStorage
from sqlalchemy.exc import SQLAlchemyError
from time import gmtime, strftime
import datetime
import logging

logger = logging.getLogger(__name__)


class UsageTrackingStorage(SQLStorage):

    def set_up(self, engine=None, metadata=None, table_name="flask_usage",
               db=None):
        # This is overriding the set up method in SQLStorage class
        try:
            import sqlalchemy as sql
            if db:
                self._eng = db.engine
                self._metadata = db.metadata
            else:
                if engine is None:
                    raise ValueError("Both db and engine args cannot be None")
                self._eng = engine
                self._metadata = metadata or sql.MetaData()
            self._con = None
            with self._eng.begin() as self._con:
                if not self._con.dialect.has_table(self._con, table_name):
                    self.track_table = sql.Table(
                        table_name, self._metadata,
                        sql.Column('id', sql.Integer, primary_key=True),
                        sql.Column('url', sql.String(512)),
                        sql.Column('api_version', sql.String(16)),
                        sql.Column('blueprint', sql.String(32)),
                        sql.Column('request_method', sql.String(16)),
                        sql.Column('path', sql.String(128)),
                        sql.Column('request_timestamp', sql.DateTime),
                        sql.Column('ip_address', sql.String(128)),
                        sql.Column('customer_id', sql.String(16)),
                        sql.Column('request_args', sql.String(2048)),
                        sql.Column('response_size', sql.String(32)),
                        sql.Column('request_size', sql.String(32)),
                        sql.Column('user_agent', sql.String(128)),
                        sql.Column('response_status', sql.Integer),
                        sql.Column('authorized', sql.Boolean)
                    )
                else:
                    self._metadata.reflect(bind=self._eng)
                    self.track_table = self._metadata.tables[table_name]
        except ValueError as v_err:
            logger.error(msg=v_err.message)
        except SQLAlchemyError as sql_alc_err:
            logger.error(msg=sql_alc_err.message)
        except Exception as err:
            logger.error(msg=err.message)

    def store(self, data):
        """
            This is overriding store method in SQLStorage class
            Called from TrackUsage class in flask_track_usage (__init__ module).
            In our case, called from ESTrackUsage class (inherits TrackUsage class)
        """
        record_id = 0
        try:
            with self._eng.begin() as con:
                stmt = None
                if "response_status" in data:
                    # We need to decide if we need to insert new row
                    # (for before_request where response_status is -1 : see before_request in usage_tracking)
                    # or update an existing row
                    # (for after_request where response code is actual http code like 401, 200, 500 etc)
                    new_record = (data.get('response_status', -1) == -1)
                    if not new_record:
                        stmt = self.track_table.update().\
                            where(self.track_table.c.id == data.get('id')).\
                            values(
                            response_status=data.get('response_status', 'response_status_not_found'),
                            response_size=data.get('response_size', 'response_size_not_found')
                        )

                    else:
                        stmt = self.track_table.insert().values(
                            url=data.get('url', 'url_not_found'),
                            api_version=data.get('api_version', 'api_version_not_found'),
                            blueprint=data.get('blueprint', 'blueprint_not_found'),
                            request_method=data.get('request_method', 'request_method_not_found'),
                            path=data.get('path', 'path_not_found'),
                            request_timestamp=data.get('request_timestamp', strftime("%Y-%m-%d %H:%M:%S", gmtime())),
                            ip_address=data.get('ip_address', 'ip_address_not_found'),
                            request_args=unicode(data.get('request_args', 'request_args_not_found')),
                            response_size=data.get('response_size', 'response_size_not_found'),
                            request_size=data.get('request_size', 'request_size_not_found'),
                            user_agent=data.get('user_agent', 'user_agent_not_found'),
                            customer_id=data.get('customer_id', 'customer_id_not_found'),
                            response_status=data.get('response_status', -1),
                            authorized=data.get('authorized', 9)    # 9 is default and we couldn't find it
                        )

                    if stmt is not None:
                        res = con.execute(stmt)
                        if res is not None and res.is_insert:
                            record_id = res.inserted_primary_key[0]
                    else:
                        logger.info("stmt (is None) could not be prepared. No api tracking entry.")

        except SQLAlchemyError as sql_alc_err:
            logger.error(msg=sql_alc_err.message)
        except Exception as err:
            logger.error(msg=err.message)
        finally:
            return record_id

    def get_usage(self, start_date=None, end_date=None, limit=500, page=1):
        """
        This is overriding get usage method in SQLStorage class
        This is what translates the raw data into the proper structure.

        :Parameters:
           - `start_date`: datetime.datetime representation of starting date
           - `end_date`: datetime.datetime representation of ending date
           - `limit`: The max amount of results to return
           - `page`: Result page number limited by `limit` number in a page
        """

        raw_data = self._get_raw(start_date, end_date, limit, page)
        usage_data = []

        if raw_data:
            usage_data = [
                {
                    'customer_id': r[1],
                    'url': r[2],
                    'path': r[3],
                    'ip_address': r[4],
                    'api_version': r[5],
                    'blueprint': r[6],
                    'request_method': r[7],
                    'request_timestamp': r[8],
                    'request_size': r[9],
                    'request_args': r[10],
                    'response_size': r[11],
                    'user_agent': r[12],
                    'response_status': r[13],
                    'authorized': r[14],
                }
                for r in raw_data
            ]

        return usage_data

    def _get_raw(self, start_date=None, end_date=None, limit=500, page=1):
        """
        This is the raw getter from database

        :Parameters:
           - `start_date`: strftime representation of starting time (uses request timestamp)
           - `end_date`: strftime representation of ending date (uses request timestamp)
           - `limit`: The max amount of results to return
           - `page`: Result page number limited by `limit` number in a page
        """
        import sqlalchemy as sql
        page = max(1, page)  # min bound
        if end_date is None:
            end_date = strftime("%Y-%m-%d %H:%M:%S", gmtime())     # datetime.datetime.utcnow()
        if start_date is None:
            start_date = strftime("%Y-%m-%d %H:%M:%S", datetime.datetime(year=1970, month=1, day=1,
                                                                         hour=00, minute=00, second=00).timetuple())
        with self._eng.begin() as con:
            _table = self.track_table
            stmt = sql.select([self.track_table]) \
                .where(_table.c.request_timestamp.between(start_date, end_date)) \
                .limit(limit) \
                .offset(limit * (page - 1)) \
                .order_by(sql.desc(self.track_table.c.request_timestamp))

            result = None
            try:
                res = con.execute(stmt)
                result = res.fetchall()
            except Exception as err:
                logging.error(err.message)
            finally:
                return result
