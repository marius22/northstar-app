"""
Company
"""
import json

from ns.model import db
from ns.constants.insight_constants import InsightConstants
from ns.constants.default_field_mapping_constants import TARGET_TYPE_MARKETO, \
    TARGET_TYPE_MARKETO_SANDBOX, \
    TARGET_TYPE_SALESFORCE
import datetime

from sqlalchemy import and_


class CompanyInsight(db.Model):
    __tablename__ = 'company_insight'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    column_name = db.Column(db.String(255), nullable=False)
    display_name = db.Column(db.String(255), nullable=False)
    company_column_name = db.Column(db.String(255), nullable=False, server_default="")

    @staticmethod
    def get_by_id(insight_id):
        return CompanyInsight.query.get(insight_id)

    @staticmethod
    def _sort_fields(display_names):
        from ns.model.recommendation import Recommendation
        sorted_all_names = Recommendation._maps(Recommendation).keys()
        return filter(lambda x: x in display_names, sorted_all_names)

    @staticmethod
    def create_insights(column_name, display_name):
        insight = CompanyInsight(column_name=column_name, display_name=display_name)
        db.session.add(insight)
        db.session.commit()

    @staticmethod
    def get_all_display_names():
        return CompanyInsight.get_display_names(
            CompanyInsight.query.all()
        )

    @staticmethod
    def get_supported_publish_fields(target_type=TARGET_TYPE_SALESFORCE):
        support_insights = InsightConstants.SUPPORTED_PUBLISH_INSIGHTS
        if target_type in [TARGET_TYPE_MARKETO, TARGET_TYPE_MARKETO_SANDBOX]:
            support_insights = support_insights + \
                [InsightConstants.INSIGHT_NAME_ES_ENRICHED, InsightConstants.INSIGHT_NAME_ES_SOURCE]

        return CompanyInsight.query.\
            order_by(CompanyInsight.display_name).\
            filter(CompanyInsight.display_name.in_(
                support_insights)).all()

    @staticmethod
    def get_supported_publish_field_for_display():
        fields = CompanyInsight.get_supported_publish_fields()
        return [f.display_name for f in fields] if fields else []

    @classmethod
    def get_display_names(cls, insights):
        return [insight.display_name for insight in insights]

    @classmethod
    def get_ids(cls, insights):
        return [insight.id for insight in insights]

    @classmethod
    def get_display_name_by_ids(cls, ids_str):
        """
        `ids_str` could be either string represented list
        or list object contains all the ids

        Parameters
        ----------
        ids_str

        Returns
        -------

        """
        ids_str = str(ids_str)

        # This throws an exception and none of the companies are shown on super admin settings page if ids_str is ''
        # This happens if some one modifies the db table directly.
        if ids_str.strip() == '':
            return []

        ids = json.loads(ids_str)

        if not ids:
            return []
        else:
            insights = cls.query.filter(cls.id.in_(ids))
            return cls.get_display_names(insights)

    @classmethod
    def get_display_name_by_id(cls, id):
        return cls.query.filter(cls.id == id).with_entities(cls.display_name)

    @classmethod
    def get_column_name_by_ids(cls, ids):
        insights = cls.query.filter(cls.id.in_(ids))

        return [insight.column_name for insight in insights]

    @classmethod
    def get_column_name_by_id(cls, id):
        return cls.query.filter(cls.id == id).with_entities(cls.column_name)

    @classmethod
    def get_company_column_name_by_ids(cls, ids):
        insights = cls.query.filter(cls.id.in_(ids))

        return [insight.company_column_name for insight in insights]

    @classmethod
    def get_ids_by_display_names(cls, names):
        """
        Get ids given a list of the display names.

        Parameters
        ----------
        names: predefined display names

        Returns
        -------

        """
        insights = cls.query.filter(cls.display_name.in_(names))
        return cls.get_ids(insights)

    @property
    def serialized(self):
        return {
            'id': self.id,
            'columnName': self.column_name,
            'displayName': self.display_name,
            'companyColumnName': self.company_column_name
        }
