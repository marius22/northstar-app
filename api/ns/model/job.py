"""
Job model
"""
import datetime
import ujson
import logging
from flask import current_app

from ns.model import db
from sqlalchemy import func, and_
from ns.model.recommendation import Recommendation

logger = logging.getLogger(__name__)


class Job(db.Model):
    __tablename__ = 'job'

    # job status
    IN_PROGRESS = "IN PROGRESS"
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"
    status_enums = (IN_PROGRESS, COMPLETED, FAILED)

    # job types
    SEGMENT = "segment"
    ACCOUNT_FIT = "account_fit_scores"
    ACCOUNT_GALAXY = "account_galaxy"
    FEATURE_ANALYSIS = "feature_analysis"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, index=True, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, nullable=False)
    seed_id = db.Column(db.Integer, nullable=False)
    qualification_id = db.Column(db.Integer, nullable=True)
    expansion_id = db.Column(db.Integer, nullable=True)
    status = db.Column(db.Enum(*status_enums), nullable=False)

    account_id = db.Column(db.Integer, nullable=False, server_default='0')
    type = db.Column(db.String(50), nullable=False, server_default="segment")

    @property
    def seed(self):
        from ns.model.seed import Seed
        return Seed.query.filter(Seed.id == self.seed_id).first()

    @property
    def segment(self):
        from ns.model.segment import Segment
        return Segment.query.filter(Segment.id == self.segment_id).first()

    @property
    def account(self):
        from ns.model.account import Account
        return Account.query\
            .filter(Account.id == self.account_id).first()

    @property
    def recommendations(self):
        return Recommendation.query.filter(Recommendation.job_id == self.id)

    @staticmethod
    def get_by_account_type_seed(account_id, type, seed_id):
        return Job.query\
            .filter(Job.account_id == account_id,
                    Job.type == type,
                    Job.seed_id == seed_id)\
            .first()

    @staticmethod
    def create(segment_id, account_id, type, seed_id, qualification_id, expansion_id, status=IN_PROGRESS):
        job = Job(segment_id=segment_id,
                  account_id=account_id,
                  type=type,
                  seed_id=seed_id,
                  qualification_id=qualification_id,
                  expansion_id=expansion_id,
                  status=status)
        db.session.add(job)
        db.session.commit()
        return job

    @staticmethod
    def get_by_id(job_id):
        return Job.query.get(job_id)

    @staticmethod
    def get_all_jobs_by_segment_id(segment_id):
        return Job.query.filter(Job.segment_id == segment_id, Job.type == Job.SEGMENT)

    @staticmethod
    def get_account_jobs(account_id, type):
        return Job.query\
            .filter(Job.account_id == account_id,
                    Job.type == type)\
            .all()

    def create_recommendations(self, recommendation_dicts):
        user = self.segment.owner

        rs = []
        for rec_dict in recommendation_dicts:

            r = dict()
            r["domain"] = rec_dict.get("domain")
            r["company_id"] = rec_dict.get("companyId")
            r["company_name"] = rec_dict.get("companyName")
            r["revenue"] = rec_dict.get("revenue")
            r["industry"] = rec_dict.get("industry")
            r["employee_size"] = rec_dict.get("employeeSize")
            r["tech"] = ujson.dumps(rec_dict.get(
                "tech") if rec_dict.get("tech") else [])
            r["growth"] = rec_dict.get("growth")
            r["alexa_rank"] = rec_dict.get("alexaRank")
            r["linkedin_url"] = rec_dict.get("linkedinUrl")
            r["country"] = rec_dict.get("country")
            r["state"] = rec_dict.get("state")
            r["city"] = rec_dict.get("city")
            r["zipcode"] = rec_dict.get("zipcode")
            r["score"] = rec_dict.get("score")
            r["similar_companies"] = ujson.dumps(
                rec_dict.get("similarDomains") if rec_dict.get("similarDomains") else [])
            r["similar_departments"] = ujson.dumps(
                rec_dict.get("similarDepartments") if rec_dict.get("similarDepartments") else [])
            r["similar_technologies"] = ujson.dumps(
                rec_dict.get("similarTechnologies") if rec_dict.get("similarTechnologies") else [])
            r["similar_c_score"] = rec_dict.get("companyScore")\
                if rec_dict.get("companyScore") is not None \
                else Recommendation.SIMILARITY_SCORE_THRESHOLD
            r["similar_d_score"] = rec_dict.get("departmentScore")\
                if rec_dict.get("departmentScore") is not None \
                else Recommendation.SIMILARITY_SCORE_THRESHOLD
            r["similar_t_score"] = rec_dict.get("technologyScore")\
                if rec_dict.get("technologyScore") is not None \
                else Recommendation.SIMILARITY_SCORE_THRESHOLD

            r["d_administrative"] = rec_dict.get("departmentAdministrative")
            r["d_computing_it"] = rec_dict.get("departmentComputingIT")
            r["d_educator"] = rec_dict.get("departmentEducator")
            r["d_engineering"] = rec_dict.get("departmentEngineering")
            r["d_finance"] = rec_dict.get("departmentFinance")
            r["d_hr"] = rec_dict.get("departmentHR")
            r["d_legal"] = rec_dict.get("departmentLegal")
            r["d_marketing"] = rec_dict.get("departmentMarketing")
            r["d_medical_heath"] = rec_dict.get("departmentMedicalHealth")
            r["d_operations"] = rec_dict.get("departmentOperations")
            r["d_research_dev"] = rec_dict.get("departmentResearchDevelopment")
            r["d_sales"] = rec_dict.get("departmentSales")

            r["tech_categories"] = ujson.dumps(
                rec_dict.get("techCategories") if rec_dict.get("techCategories") else [])
            r["job_id"] = self.id
            r["user_id"] = user.id
            r["segment_id"] = self.segment_id
            time_now = datetime.datetime.utcnow()
            r["created_ts"] = time_now
            r["updated_ts"] = time_now
            rs.append(r)

        # db.session.add_all(rs)
        db.engine.execute(
            Recommendation.__table__.insert(),
            rs
        )
        db.session.commit()

        return rs

    def recommendation_distinct_counts_by_field(self, companies_cnt, key,
                                                order=None, limit=None, with_others=False):
        field = Recommendation.get_field_by_key(key)
        value_counts = self.recommendations\
            .with_entities(func.count(1).label('count'), field.label('value'))\
            .filter(field.isnot(None))\
            .group_by(field)\
            .order_by(order)\
            .limit(limit)

        results = []
        for row in value_counts:
            results.append({
                'value': row.value,
                'count': row.count
            })
            companies_cnt -= row.count

        # sorts categorical values
        categorical_fields = {
            'revenue': current_app.config.get('REVENUE_BUCKETS'),
            'employeeSize': current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
        }

        bucket_plan = categorical_fields.get(key)
        if bucket_plan:
            idxes = [item['label'] for item in bucket_plan]
            # sorts the results by the item indexes in the bucket definition
            results = sorted(
                results,
                key=lambda item: idxes.index(item['value']))

        if with_others:
            results.append({'value': 'Others',
                            'count': companies_cnt})

        return results

    def update_status(self, status):
        self.status = status
        db.session.commit()
