"""
Code model
"""
import datetime
from ns.model import db


class CompanyDispute(db.Model):
    __tablename__ = 'company_dispute'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    company_id = db.Column(db.Integer, nullable=False)
    field_name = db.Column(db.String(255), nullable=False)

    @staticmethod
    def get_by_id_and_field_name(segment_id, company_id, field_name):
        return CompanyDispute.query.filter(
            CompanyDispute.segment_id == segment_id,
            CompanyDispute.company_id == company_id,
            CompanyDispute.field_name == field_name
        ).first()

    @staticmethod
    def delete_dispute(segment_id, company_id, field_name):
        CompanyDispute.query.filter(
            CompanyDispute.segment_id == segment_id,
            CompanyDispute.company_id == company_id,
            CompanyDispute.field_name == field_name
        ).delete()
        db.session.commit()

    @staticmethod
    def create_dispute(segment_id, company_id, field_name):
        dispute = CompanyDispute.get_by_id_and_field_name(segment_id, company_id, field_name)
        if dispute is None:
            dispute = CompanyDispute(
                segment_id=segment_id,
                company_id=company_id,
                field_name=field_name
            )
            db.session.add(dispute)
            db.session.commit()
        return dispute

    @staticmethod
    def get_by_segment_company_ids(segment_id, company_ids):
        return CompanyDispute.query.filter(
            CompanyDispute.segment_id == segment_id,
            CompanyDispute.company_id.in_(company_ids)
        ).all()

    @property
    def serialized(self):
        return {
            "segmentId": self.segment_id,
            "companyId": self.company_id,
            "fieldName": self.field_name,
            "createdTs": self.created_ts.isoformat()
        }
