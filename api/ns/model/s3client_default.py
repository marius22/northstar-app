from flask import current_app
from ns.util.s3client import S3Client


class S3ClientDefault(object):

    @staticmethod
    def get_instance():
        aws_access_key_id = current_app.config.get('AWS_ACCESS_KEY_ID')
        aws_secret_access_key = current_app.config.get('AWS_SECRET_ACCESS_KEY')
        bucket_name = current_app.config.get('S3_CSV_BUCKET')

        return S3Client(aws_access_key_id, aws_secret_access_key, bucket_name)
