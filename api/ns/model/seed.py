"""
Seed Model
"""
from ns.model import db
import datetime


class Seed(db.Model):
    __tablename__ = 'seed'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    seed_candidate_ids = db.Column(db.Text, nullable=False)

    @staticmethod
    def get_by_id(seed_id):
        return Seed.query.filter(Seed.id == seed_id).first()

    @property
    def job(self):
        from ns.model.job import Job
        return Job.query.filter(Job.seed_id == self.id).first()

    @staticmethod
    def create(segment_id, seed_candidate_ids):
        seed = Seed(segment_id=segment_id, seed_candidate_ids=seed_candidate_ids)
        db.session.add(seed)
        db.session.commit()
        return seed
