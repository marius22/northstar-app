"""
Account quota
"""
import datetime
import json
from flask import current_app
from ns.model import db
from ns.model.company_insight import CompanyInsight
from ns.model.account_quota_ledger import AccountQuotaLedger
import arrow
from sqlalchemy.orm import relationship
from sqlalchemy import and_
from ns.constants.insight_constants import InsightConstants


class QuotaExceedError(Exception):
    pass


class InvalidAccountQuotaError(Exception):
    """Error when an account can't be saved because it's invalid"""
    pass


class AccountQuota(db.Model):
    __tablename__ = 'account_quota'

    INBOUND_TARGET_ACCOUNTS = "account"
    INBOUND_TARGET_LEADS = "lead"
    INBOUND_TARGET_CONTACTS = "contact"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), unique=True, nullable=False)

    segment_cnt = db.Column(db.BigInteger, nullable=False)
    exposed_ui_insight_ids = db.Column(db.String(4096), nullable=False)
    exposed_csv_insight_ids = db.Column(db.String(4096), nullable=False)

    ui_company_limit = db.Column(db.BigInteger, nullable=False)
    csv_company_limit = db.Column(db.BigInteger, nullable=False)
    contact_limit = db.Column(db.BigInteger, nullable=False)

    expired_ts = db.Column(db.TIMESTAMP, nullable=True)
    start_ts = db.Column(db.TIMESTAMP, nullable=True)
    enable_ads = db.Column(db.Boolean, nullable=False)

    real_time_scoring_insight_ids = db.Column(db.String(4096), nullable=False, server_default="[]")
    enable_in_crm = db.Column(db.Boolean, nullable=False, server_default="0")
    enable_not_in_crm = db.Column(db.Boolean, nullable=False, server_default="0")
    # add two new columns
    enable_find_company = db.Column(db.Boolean, server_default="0", nullable=False)
    enable_score_list = db.Column(db.Boolean, server_default="1", nullable=False)
    enable_model_insights = db.Column(db.Boolean, server_default="0", nullable=False)
    enable_apis = db.Column(db.Boolean, server_default="1", nullable=False)

    @property
    def remaining_csv_company_cnt(self):

        # This is for backport use case. If start_ts is not found, we should default to 1970 and update start_ts in db
        if not self.start_ts:
            self.set_start_ts_default()

        # calculate the net adj value (any refunds given to the customer in case of bad data quality)
        net_adj_value = AccountQuotaLedger.adjusted_company_cnt(self.account_id, self.start_ts, self.expired_ts) \
            if AccountQuotaLedger else 0
        remaining_cnt = self.csv_company_limit - self.account.exported_companies_cnt + net_adj_value

        # This will not happen very often, but it may in case we have issues with our data in db
        # (if at all db gets messed up). We should return remaining as max limit in that case.
        # PS: adding adjustment also checks that adjustment must not be greater than (max-remaining)
        if remaining_cnt > self.csv_company_limit:
            return self.csv_company_limit
        else:
            return remaining_cnt

    @property
    def remaining_contact_cnt(self):
        return self.contact_limit - self.account.exported_contact_cnt

    def update(self, new_attrs):
        start_ts = new_attrs.get('startTs', self.start_ts)
        expired_ts = new_attrs.get('expiredTs', self.expired_ts)
        if (start_ts and expired_ts) and (start_ts >= expired_ts):
            raise InvalidAccountQuotaError("Quota start date must be before quota end date.")

        self.segment_cnt = new_attrs.get('segmentCnt', self.segment_cnt)
        self.ui_company_limit = new_attrs.get('UICompanyLimit', self.ui_company_limit)
        self.csv_company_limit = new_attrs.get('CSVCompanyLimit', self.csv_company_limit)
        self.contact_limit = new_attrs.get('contactLimit', self.contact_limit)
        self.start_ts = start_ts
        self.expired_ts = expired_ts
        self.enable_ads = new_attrs.get('enableAds', self.enable_ads)
        self.enable_in_crm = new_attrs.get('enableInCrm', self.enable_in_crm)
        self.enable_not_in_crm = new_attrs.get('enableNotInCrm', self.enable_not_in_crm)
        self.enable_find_company = new_attrs.get('enableFindCompany', self.enable_find_company)
        self.enable_score_list = new_attrs.get('enableScoreList', self.enable_score_list)
        self.enable_model_insights = new_attrs.get('enableModelInsights', self.enable_model_insights)
        self.enable_apis = new_attrs.get('enableApis', self.enable_apis)
        new_insights = new_attrs.get('exposedUIInsights')
        if new_insights is not None:
            self.exposed_ui_insight_ids = str(CompanyInsight.
                                              get_ids_by_display_names(new_insights))

        new_insights = new_attrs.get('exposedCSVInsights')
        if new_insights is not None:
            self.exposed_csv_insight_ids = str(CompanyInsight.
                                               get_ids_by_display_names(new_insights))
        else:
            # For a non-ES admin updating real_time_scoring_insights,
            # new_attrs will naturally not contain any exposedCSVInsights
            # changes, so we use the account_quota's current exposed_csv_insights
            # for the real_time_scoring insights logic below.
            new_insights = self.exposed_csv_insights

        real_time_scoring_insights = new_attrs.get('realTimeScoringInsights')

        if real_time_scoring_insights is not None:
            real_time_scoring_insights = filter(lambda x: x in new_insights, real_time_scoring_insights)
            self.real_time_scoring_insight_ids = \
                str(CompanyInsight.get_ids_by_display_names(real_time_scoring_insights))

        db.session.add(self)
        db.session.commit()

    @property
    def allowed_insight_ids(self):
        es_source_enriched_insights_ids = CompanyInsight.get_ids_by_display_names(
            [InsightConstants.INSIGHT_NAME_ES_ENRICHED,
             InsightConstants.INSIGHT_NAME_ES_SOURCE]
        )
        return json.loads(self.exposed_csv_insight_ids) + es_source_enriched_insights_ids

    @property
    def exposed_ui_insights(self):
        return CompanyInsight.get_display_name_by_ids(self.exposed_ui_insight_ids)

    @property
    def exposed_csv_insights(self):
        return CompanyInsight.get_display_name_by_ids(self.exposed_csv_insight_ids)

    @property
    def real_time_scoring_insights(self):
        return CompanyInsight.get_display_name_by_ids(self.real_time_scoring_insight_ids)

    @property
    def exposed_csv_insight_column_names(self):
        return CompanyInsight.get_column_name_by_ids(
            json.loads(self.exposed_csv_insight_ids))

    @property
    def exposed_insight_company_column_names(self):
        return CompanyInsight.get_company_column_name_by_ids(
            json.loads(self.real_time_scoring_insight_ids))

    @property
    def used_segment_cnt(self):
        """
        The count of all the segments created by the users of this account

        Returns
        -------

        """
        return self.account.owned_segments.count()

    @property
    def used_audience_cnt(self):
        return len(self.account.all_owned_audiences())

    @property
    def scoring_enabled(self):
        return self.enable_in_crm

    @property
    def apis_enabled(self):
        return self.enable_apis

    def _serialized(self, includes_usage=False):
        """
        Serialize self while loading the insights

        Parameters
        ----------
        includes_usage

        Returns
        -------

        """
        expired_at = self.expired_ts.isoformat() if self.expired_ts else self.expired_ts
        if not self.start_ts:
            self.set_start_ts_default()
        start_ts = self.start_ts.isoformat()

        quotas = {
            'segmentCnt': self.segment_cnt,
            'UICompanyLimit': self.ui_company_limit,
            'CSVCompanyLimit': self.csv_company_limit,
            'contactLimit': self.contact_limit,
            'exposedUIInsights': self.exposed_ui_insights,
            'exposedCSVInsights': self.exposed_csv_insights,
            'startTs': start_ts,
            'expiredTs': expired_at,
            'enableAds': self.enable_ads,
            'remainingCompanyQuota': self.remaining_csv_company_cnt,
            'remainingContactQuota': self.remaining_contact_cnt,
            'realTimeScoringInsights': self.real_time_scoring_insights,
            'enableInCrm': self.enable_in_crm,
            'enableNotInCrm': self.enable_not_in_crm,
            'enableFindCompany': self.enable_find_company,
            'enableScoreList': self.enable_score_list,
            'enableModelInsights': self.enable_model_insights,
            'enableApis': self.enable_apis,
        }

        # set inboundLeadsTarget
        from ns.model.account import Account
        account = Account.get_by_id(self.account_id)
        quotas['inboundLeadsTarget'] = AccountQuota.get_real_time_scoring_object(account)
        quotas['realtimeScoring'] = account.realtime_scoring_mode

        if includes_usage:
            usage = {
                'usedSegmentCnt': self.used_segment_cnt
            }

            quotas.update(usage)

        return quotas

    @property
    def serialized(self):
        return self._serialized(False)

    @property
    def serialized_with_usage(self):
        return self._serialized(True)

    @classmethod
    def make_trial_account_quota(cls):
        """
        Factory method to return a default trial account quota.

        Dependent Configurations
        -------
            TRIAL_ACCOUNT_QUOTA_SEGMENT_COUNT = 1
            TRIAL_ACCOUNT_QUOTA_EXPIRE_IN_MONTHS = 12
            TRIAL_ACCOUNT_QUOTA_EXPOSED_UI_INSIGHTS = ['companyName', 'revenue']
            TRIAL_ACCOUNT_QUOTA_EXPOSED_CSV_INSIGHTS = ['companyName', 'revenue']
            TRIAL_ACCOUNT_QUOTA_UI_COMPANY_LIMIT = 5
            TRIAL_ACCOUNT_QUOTA_CSV_COMPANY_LIMIT = 5

        """
        segment_cnt = current_app.config.get('TRIAL_ACCOUNT_QUOTA_SEGMENT_COUNT', 1)
        expire_in_months = int(current_app.config.get('TRIAL_ACCOUNT_QUOTA_EXPIRE_IN_MONTHS', 12))
        ui_company_limit = current_app.config.get('TRIAL_ACCOUNT_QUOTA_UI_COMPANY_LIMIT', 5)
        csv_company_limit = current_app.config.get('TRIAL_ACCOUNT_QUOTA_CSV_COMPANY_LIMIT', 5)

        exposed_ui_insights = current_app.config.get('TRIAL_ACCOUNT_QUOTA_EXPOSED_UI_INSIGHTS',
                                                     ['companyName', 'revenue'])
        ui_insight_ids_str = str(CompanyInsight.get_ids_by_display_names(exposed_ui_insights))

        exposed_csv_insights = current_app.config.get('TRIAL_ACCOUNT_QUOTA_EXPOSED_CSV_INSIGHTS',
                                                      ['companyName', 'revenue'])
        csv_insight_ids_str = str(CompanyInsight.get_ids_by_display_names(exposed_csv_insights))

        now = arrow.utcnow()
        expire_at = now.replace(months=expire_in_months).naive

        return cls(segment_cnt=segment_cnt,
                   exposed_ui_insight_ids=ui_insight_ids_str,
                   exposed_csv_insight_ids=csv_insight_ids_str,
                   ui_company_limit=ui_company_limit,
                   csv_company_limit=csv_company_limit,
                   contact_limit=0,
                   expired_ts=expire_at,
                   enable_ads=False)

    def expired(self):
        return self.expired_ts < datetime.datetime.utcnow()

    @staticmethod
    def get_by_account_id(account_id):
        return AccountQuota.query\
            .filter(AccountQuota.account_id == account_id)\
            .first()

    @staticmethod
    def deduct_quota(segment, company_ids_to_download):
        account = segment.owner.account
        company_ids = set(company_ids_to_download)
        new_company_ids_for_account = company_ids - set(account.exported_companies_ids)
        # deduct account quota
        account.account_quota.upsert_user_companies(segment, company_ids_to_download)
        account.add_exported_companies(new_company_ids_for_account)

    @staticmethod
    def upsert_user_companies(segment, company_ids_to_download, export_type):
        company_ids = set(company_ids_to_download)
        # deduct account quota
        segment_exported_company_ids = set(segment.exported_company_ids)
        new_company_ids_for_segment = list(company_ids - segment_exported_company_ids)
        segment.add_exported_companies(new_company_ids_for_segment, export_type)
        re_download_company_ids = segment_exported_company_ids.intersection(company_ids)
        segment.update_exported_companies(re_download_company_ids, export_type)

    @staticmethod
    def get_real_time_scoring_object(account):
        scoring_object = []
        if account.realtime_scoring_object:
            scoring_object = json.loads(account.realtime_scoring_object)
        return scoring_object

    @staticmethod
    def bulk_update_exposed_csv_insight_ids(exposed_csv_insight_ids):
        AccountQuota.query.update({AccountQuota.exposed_csv_insight_ids: str(exposed_csv_insight_ids)})
        db.session.commit()

    def set_start_ts_default(self):
        self.start_ts = datetime.datetime(year=1970, month=01, day=01, hour=00, minute=00, second=01)
        AccountQuota.query.\
            filter(and_(AccountQuota.account_id == self.account_id,
                        AccountQuota.id == self.id)).\
            update({AccountQuota.start_ts: self.start_ts})
        db.session.commit()
