import os
from flask import current_app
import unicodecsv
from ns.model.seed_candidate import SeedCandidate
from ns.model.seed_company import SeedCompany
from ns.util.domain_util import parse_domain, is_domain
from ns.util.file_util import write_file_to_local
from ns.model import company_name_2_domain


def _parse_domains_from_file_fields(csv_file, csv_data=None):
    if not csv_file and not csv_data:
        return []

    result = set()
    company_names = set()
    first_row_flag = True

    if csv_data is None:
        csv_data = SeedCSVReader(csv_file.read().splitlines())

    for row in csv_data:
        # Skip the first row, assuming a header row.
        if first_row_flag:
            first_row_flag = False
            continue

        num_cols = len(row)
        if num_cols == 1:
            domain = parse_domain(row[0])
            if is_domain(domain):
                result.add(domain)
            elif row[0]:
                company_names.add(row[0])

        if num_cols == 2:
            # Check if row[1] is a domain. If so, add it and move onto
            # the next row via continue statement.
            domain = parse_domain(row[1])
            if is_domain(domain):
                result.add(domain)
                continue
            # Check if row[0] is a domain. If so, add it and move onto
            # the next row via continue statement.
            domain = parse_domain(row[0])
            if is_domain(domain):
                result.add(domain)
                continue
            # At this point, we know that neither of our two row values
            # are a domain, so we fall back on adding the first non-Falsey
            # row value to company_names, from which we will try to find a domain
            # via the c2d service.
            if row[0]:
                company_names.add(row[0])
                continue
            if row[1]:
                company_names.add(row[1])

    # get domains by company_names
    result.update(
        company_name_2_domain.fetch_domains_list_by_company_names(company_names)
    )

    return list(result)


class SeedCSVReader(unicodecsv.py2.UnicodeReader):

    def __init__(self, csv_file, **kwargs):
        super(SeedCSVReader, self).__init__(csv_file, **kwargs)

    def next(self):
        while True:
            try:
                return super(SeedCSVReader, self).next()
            except Exception, e:
                if isinstance(e, StopIteration):
                    raise

    @staticmethod
    def get_seed_companies_from_file_path(csv_file_path):
        return [
            SeedCompany(domain=item, company_name="")
            for item in SeedCSVReader.read_domains_from_file_path(csv_file_path)
        ]

    # this one is only used for V1 segment stuff
    @staticmethod
    def get_seed_candidates_from_file_path(csv_file_path):
        domains = SeedCSVReader.read_domains_from_file_path(csv_file_path)
        return [
            SeedCandidate(domain=domain, company_name="")
            for domain in domains
        ]

    @staticmethod
    def read_domains_from_file_path(csv_file_path):
        with open(csv_file_path, 'rb') as f:
            return SeedCSVReader.read_domains_from_file(f)

    @staticmethod
    def read_domains_from_file(csv_file):
        return _parse_domains_from_file_fields(csv_file)

    # This doesn't actually use a csv file, but I want to take advantage
    # of the existing parse domains logic here to keep things DRY.
    @staticmethod
    def get_domains_from_file_data(csv_data):
        return _parse_domains_from_file_fields(None, csv_data)

    @staticmethod
    def num_lines(file_path):
        with open(file_path, 'rb') as f:
            count = sum(1 for line in f)
        return count

    @staticmethod
    def write_seed_csv_to_local(file_name, csv_file):

        seed_csv_path_prefix = current_app.config.get(
            "SEED_CSV_PATH_PREFIX", '/tmp')

        file_path = os.path.join(seed_csv_path_prefix, file_name)
        write_file_to_local(file_path, csv_file)
        return file_path


class RowCountsExceedsLimitError(Exception):
    pass


class NotEnoughRowsError(Exception):
    pass
