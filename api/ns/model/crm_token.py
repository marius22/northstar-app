"""
CRM token Model
"""
import datetime
import json

from flask import current_app
from ns.model import db
from ns.model.user_segment_sfdc_publisher import SFDCManagedPackageSegementNotCreateableError, \
    SFDCManagedPackageNotInstalledError, SFDCTokenInvalidError, SFDCIntegrationError, \
    SFDCNoPermissionInstallPackage, SFDCHavePermissionInstallPackage
from ns.util.string_util import is_not_blank
from requests_oauthlib import OAuth2Session
from simple_salesforce import Salesforce
from simple_salesforce.api import SalesforceResourceNotFound, SalesforceExpiredSession
from sqlalchemy import or_
from werkzeug.exceptions import NotFound


class CRMToken(db.Model):
    __tablename__ = "crm_token"

    SALESFORCE = "SALESFORCE"
    SALESFORCE_SANDBOX = "SALESFORCE_SANDBOX"
    # SALESFORCE_AMIND_ROLE = "System Administrator"
    # TODO: We will add the the profile check later

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    crm_type = db.Column(db.String(255), nullable=False)
    token = db.Column(db.String(2048), nullable=False)
    organization_id = db.Column(db.String(255), nullable=True)
    email = db.Column(db.String(255), nullable=True)
    role = db.Column(db.String(255), nullable=True)
    account_id = db.Column(db.Integer, nullable=True)

    @staticmethod
    def delete_token_by_user_id_type(user_id, crm_type):
        CRMToken.query.filter(CRMToken.user_id == user_id,
                              CRMToken.crm_type == crm_type).delete()
        db.session.commit()
        return

    @classmethod
    def check_token_format(cls, token_content):
        token = json.loads(token_content)
        assert isinstance(token, dict), "invalid format of token"

    @staticmethod
    def _get_by_user_crm_type(user_id, crm_type):
        from ns.model.user import User
        user = User.get_by_id(user_id)
        return CRMToken.query.filter(CRMToken.account_id == user.account_id,
                                     CRMToken.crm_type == crm_type).first()

    @staticmethod
    def get_by_id(id):
        return CRMToken.query.filter(CRMToken.id == id).first()

    @staticmethod
    def get_sfdc_token_by_user_id(user_id):
        from ns.model.user import User
        user = User.get_by_id(user_id)
        return CRMToken.query.filter(CRMToken.account_id == user.account_id,
                                     or_(CRMToken.crm_type == CRMToken.SALESFORCE, CRMToken.crm_type ==
                                         CRMToken.SALESFORCE_SANDBOX)).first()

    @staticmethod
    def get_by_account_id(account_id):
        return CRMToken.query.filter(CRMToken.account_id == account_id).first()

    @staticmethod
    def get_by_crm_type(crm_type):
        return CRMToken.query.filter(CRMToken.crm_type == crm_type).all()

    @staticmethod
    def get_salesforce_admin_by_crm_type_accounts(account_ids):
        return CRMToken.query.filter(or_(CRMToken.crm_type == CRMToken.SALESFORCE, CRMToken.crm_type ==
                                         CRMToken.SALESFORCE_SANDBOX),
                                     CRMToken.account_id.in_(account_ids)).all()

    @staticmethod
    def get_by_id_refresh_if_expired(token_id):
        crm_token = CRMToken.get_by_id(token_id)
        return CRMToken.refresh_if_expired(crm_token)

    @staticmethod
    def refresh_if_expired(crm_token):
        if not crm_token:
            return None
        sfdc_token = SFDCToken.load_from_crm_token(crm_token)
        if not sfdc_token.is_expired():
            return crm_token
        sfdc_token.refresh_token_and_update(crm_token.crm_type)
        return CRMToken.get_by_id(crm_token.id)

    @staticmethod
    def get_sfdc_token(user_id):
        crm_token = CRMToken._get_by_user_crm_type(user_id, CRMToken.SALESFORCE)
        if not crm_token:
            crm_token = CRMToken._get_by_user_crm_type(user_id, CRMToken.SALESFORCE_SANDBOX)
        return SFDCToken.load_from_crm_token(crm_token)

    @staticmethod
    def get_sfdc_tokens_by_org_id(org_id):
        assert org_id, "Invalid orgId"
        crm_tokens = CRMToken.query.filter(CRMToken.organization_id == org_id,
                                           or_(CRMToken.crm_type == CRMToken.SALESFORCE,
                                               CRMToken.crm_type == CRMToken.SALESFORCE_SANDBOX)).all()
        return crm_tokens

    @staticmethod
    def create_sfdc_token(account_id, user_id, token_dict, crm_type):
        result = SFDCToken(account_id, user_id, crm_type)
        return result.update_from_dict(token_dict)

    def _update_token_content(self, token, org_id, email, role):
        self.__class__.check_token_format(token)
        self.token = token
        self.organization_id = org_id
        self.email = email
        self.role = role
        db.session.commit()
        return self

    @staticmethod
    def _save_token(account_id, user_id, crm_type, token, org_id, email, role):
        CRMToken.check_token_format(token)
        db_token = CRMToken._get_by_user_crm_type(user_id, crm_type)
        if db_token is None:
            token = CRMToken(
                account_id=account_id,
                user_id=user_id,
                crm_type=crm_type,
                token=token,
                organization_id=org_id,
                email=email,
                role=role
            )
            db.session.add(token)
            db.session.commit()
            return token
        else:
            db_token._update_token_content(token, org_id, email, role)
            return db_token

    @staticmethod
    def get_token_by_domain(domain):
        crm_token = CRMToken.query.filter(CRMToken.email.like("%" + domain),
                                          CRMToken.crm_type == CRMToken.SALESFORCE
                                          ).first()
        if crm_token:
            return SFDCToken.load_from_crm_token(crm_token)
        return None


class SFDCToken(object):
    USER_ID_KEY = "user_id"
    ACCOUNT_ID_KEY = "account_id"
    ROLE_KEY = "role"
    EMAIL_KEY = "email"
    PROFILE_ID_KEY = 'ProfileId'
    SALESFORCE_ID_KEY = "id"
    ACCESS_TOKEN_KEY = "access_token"
    REFRESH_TOKEN_KEY = "refresh_token"
    INSTANCE_URL_KEY = "instance_url"
    ORGANIZATION_ID_KEY = "organization_id"
    CHECK_PACKAGE_INSTALL_PERMISSIONS = \
        "SELECT PermissionsInstallPackaging FROM PermissionSet " \
        "where profileid in (select profileid from user where id= '%s' )"

    KEYS = [EMAIL_KEY, SALESFORCE_ID_KEY, ACCESS_TOKEN_KEY,
            REFRESH_TOKEN_KEY, INSTANCE_URL_KEY, ORGANIZATION_ID_KEY, USER_ID_KEY, ROLE_KEY, ACCOUNT_ID_KEY]

    def __init__(self, account_id, user_id, crm_type):
        self.account_id = account_id
        self.user_id = user_id
        self.crm_type = crm_type
        self.token = {}

    def __setattr__(self, key, value):
        if key in ['account_id', 'user_id', 'token', 'crm_type']:
            super(SFDCToken, self).__setattr__(key, value)
        elif key in self.KEYS:
            self._set_value(key, value)
        else:
            raise KeyError(
                'Key `%s` is not known(%s)',
                key, self.KEYS)

    def __getattr__(self, key):
        if key in ['account_id', 'user_id', 'token', 'crm_type']:
            return super(SFDCToken, self).__getattr__(key)
        elif key in self.KEYS:
            return self._get_value(key)
        else:
            raise KeyError(
                'Key `%s` is not known(%s)',
                key, self.KEYS)

    def _get_value(self, key):
        assert key in self.token, "token has no value for key: [%s]" % key
        return self.token.get(key)

    def _set_value(self, key, value):
        self.token[key] = value

    def save(self):
        self.token = SFDCToken._filter_token_dict(self.token)
        self._check_token()
        CRMToken._save_token(self.account_id,
                             self.user_id,
                             self.crm_type, json.dumps(self.token),
                             self.token[SFDCToken.ORGANIZATION_ID_KEY],
                             self.token[SFDCToken.EMAIL_KEY],
                             self.token[SFDCToken.ROLE_KEY])
        return self

    def update_from_dict(self, token_dict):
        self.token.update(token_dict)
        self.save()
        return self

    @staticmethod
    def _filter_token_dict(token_dict):
        result = {}
        for key in token_dict.keys():
            if key in SFDCToken.KEYS:
                result[key] = token_dict.get(key)
        return result

    @staticmethod
    def load_from_crm_token(crm_token):
        if crm_token is None:
            return None

        assert crm_token.crm_type == CRMToken.SALESFORCE or crm_token.crm_type == CRMToken.SALESFORCE_SANDBOX, \
            "can only load from salesforce token"

        result = SFDCToken(crm_token.account_id, crm_token.user_id, crm_token.crm_type)
        result.token = json.loads(crm_token.token)
        return result

    def _check_token(self):
        assert is_not_blank(self.access_token), "access token missing"
        assert is_not_blank(self.instance_url), "instance url missing"
        assert is_not_blank(self.refresh_token), "refresh token missing"

    @property
    def serialized(self):
        return {
            'instance_url': self.instance_url,
            'session_id': self.access_token
        }

    def fetch_managed_package_status(self):
        client = Salesforce(
            session_id=self.access_token,
            instance_url=self.instance_url)

        custom_object_name = current_app.config.get(
            'SFDC_AUDIENCE_NAME',
            'ES_APP__Audience__c')

        handler = getattr(client, custom_object_name)

        # fetch the object description from SFDC
        try:
            # check the existence of the object,
            object_description = handler.describe()

            # check records creatable
            if not object_description.get('createable'):
                raise SFDCManagedPackageSegementNotCreateableError()
        except SalesforceExpiredSession:
            self.refresh_token_and_update(self.crm_type)

            # retry one more time
            return self.fetch_managed_package_status()
        except SalesforceResourceNotFound:
            self.check_permission_install_package(client)
        return True

    def check_permission_install_package(self, client):
        try:
            sf_user_id = self._get_value(SFDCToken.SALESFORCE_ID_KEY)
            sf_user_id = sf_user_id.split("/")[-1]
            query = SFDCToken.CHECK_PACKAGE_INSTALL_PERMISSIONS % sf_user_id
            result = client.query(query)
            records = result["records"]
            if records[0]["PermissionsInstallPackaging"]:
                raise SFDCHavePermissionInstallPackage()
            else:
                raise SFDCNoPermissionInstallPackage()
        except Exception, e:
            if isinstance(e, (SFDCHavePermissionInstallPackage, SFDCNoPermissionInstallPackage)):
                raise
            raise SFDCManagedPackageNotInstalledError()

    def refresh_token_and_update(self, crm_type):
        # refresh the token
        if crm_type == CRMToken.SALESFORCE:
            token_url = current_app.config.get('TOKEN_URL')
        elif crm_type == CRMToken.SALESFORCE_SANDBOX:
            token_url = current_app.config.get('TOKEN_URL_SANDBOX')
        client_id = current_app.config.get('CLIENT_ID')
        client_secret = current_app.config.get('CLIENT_SECRET')

        token = SFDCToken._filter_token_dict(self.token)

        extra = {
            'client_id': client_id,
            'client_secret': client_secret
        }

        sfdc = OAuth2Session(client_id, token=token)
        result = sfdc.refresh_token(token_url, **extra)

        self.update_from_dict(result)

    @staticmethod
    def get_sfdc_crm_token(account_id):
        crm_token = CRMToken.get_salesforce_admin_by_crm_type_accounts([account_id])
        if not crm_token:
            raise NotFound('No admin token is not found for account %s.' % account_id)
        crm_token = CRMToken.refresh_if_expired(crm_token[0])
        return crm_token

    def is_expired(self):
        client = Salesforce(
            session_id=self.access_token,
            instance_url=self.instance_url)
        query = 'select email from lead limit 1'
        try:
            client.query(query)
        except SalesforceExpiredSession:
            return True
        except:
            return False
        return False
