import datetime

from ns.model import db


class Expansion(db.Model):
    __tablename__ = 'expansion'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    expansion_val = db.Column(db.Text)

    @staticmethod
    def create(segment_id, expansion_val):
        expansion = Expansion(segment_id=segment_id,
                              expansion_val=expansion_val)
        db.session.add(expansion)
        db.session.commit()
        return expansion

    def update_expansion_val(self, expansion_val):
        self.expansion_val = expansion_val
        db.session.commit()
