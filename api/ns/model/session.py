import pickle

from datetime import datetime, timedelta
from uuid import uuid4
from werkzeug.datastructures import CallbackDict
from werkzeug import exceptions
from flask.sessions import SessionInterface, SessionMixin
from ns.constants import auth_constants
from ns.util.auth_util import get_login_cookie_key, check_is_locked, \
    set_session_ns_login_attempt, get_session_ns_login_attempt, set_session_user_email, get_session_user_email
from ns.model.user import User
import logging
import ast

logger = logging.getLogger(__name__)


class RedisSession(CallbackDict, SessionMixin):

    INVALID = "INVALID"
    UN_FRESH = "UN_FRESH"
    FRESH = "FRESH"

    def __init__(self, initial=None, sid=None, new=False):

        def on_update(self):
            self.modified = True

        CallbackDict.__init__(self, initial, on_update)
        self.sid = sid
        self.new = new
        self.modified = False
        self._ns_login_attempt = 0  # should be integer (watch for value from / to cookie - str)
        # just to initialize (actual value will be obtained from request in open_session() method)
        self._user_email = ''

    @property
    def _status(self):
        return self.get('_status', self.INVALID)

    @_status.setter
    def _status(self, value):
        self['_status'] = value

    @property
    def ns_login_attempt(self):
        return self._ns_login_attempt

    @ns_login_attempt.setter
    def ns_login_attempt(self, value):
        self._ns_login_attempt = value

    @property
    def user_email(self):
        return self._user_email

    @user_email.setter
    def user_email(self, value):
        self._user_email = value

    @property
    def _last_access_ts(self):
        return self.get('_last_access_ts', None)

    @_last_access_ts.setter
    def _last_access_ts(self, value):
        self['_last_access_ts'] = value

    def refresh(self):
        self._status = self.FRESH
        self._last_access_ts = datetime.utcnow()

    def is_valid(self):
        return self._status != self.INVALID

    def is_fresh(self):
        if self._status in (self.INVALID, self.UN_FRESH):
            return False
        else:
            if (datetime.utcnow() - timedelta(seconds=60)) \
                    < self._last_access_ts:
                return True
            else:
                self._status = self.UN_FRESH
                return False

    def update_last_ts(self):
        if self._status == self.FRESH:
            self._last_access_ts = datetime.utcnow()

    def logout(self):
        # if self.permanent:
        #     self._status = self.UN_FRESH
        # else:
            # pop everything
        self.clear()


class RedisSessionInterface(SessionInterface):

    serializer = pickle
    session_class = RedisSession

    def __init__(self, redis, prefix='ns:'):

        assert redis is not None

        self.redis = redis
        self.prefix = prefix

    @staticmethod
    def generate_sid():
        return str(uuid4())

    @staticmethod
    def get_redis_expiration_time(app, session):
        if session.permanent:
            return app.permanent_session_lifetime
        # For SOC2 reasons, sessions must not be permanent.
        return timedelta(minutes=120)

    def get_redis_seesion_name(self, name):
        return self.prefix + name

    def open_session(self, app, request):
        sid = request.cookies.get(app.session_cookie_name)

        # Get value for login_attempts and put it to session var; Use that session var as source of truth in the code.
        data_dict = None
        try:
            data_dict = ast.literal_eval(str(request.data)) if request.data else None
        except (SyntaxError, ValueError, Exception) as err:
            pass

        user = User.get_user_by_email(data_dict.get('email', '')) if data_dict else None

        # this is only for login logic; rest of endpoints should not be affected: hence we check path as well
        if '/users/login' in request.path:
            if user is None:
                logging.error("User not found.")
            else:
                set_session_user_email(user.email)
                key = get_login_cookie_key(email=user.email)
                if key:
                    login_attempts = request.cookies.get(key)
                    if login_attempts:
                        try:
                            set_session_ns_login_attempt(int(login_attempts))
                        except ValueError as err:
                            logging.error("Can not convert %s to integer type; Error: %s"
                                          % (login_attempts, err.message))
                            set_session_ns_login_attempt(0)
                    else:
                        set_session_ns_login_attempt(0)
                else:
                    logging.error("Cookie key could not be created using user email: %s " % user.email)

        if not sid:
            return self.session_class(sid=RedisSessionInterface.generate_sid(), new=True)

        val = self.redis.get(self.get_redis_seesion_name(sid))
        if val is not None:
            data = self.serializer.loads(val)
            return self.session_class(data, sid=sid)

        return self.session_class(sid=sid, new=True)

    def save_session(self, app, session, response):
        domain = self.get_cookie_domain(app)

        try:
            login_attempts = get_session_ns_login_attempt()

            key = get_login_cookie_key(email=get_session_user_email())

            if key is None:
                raise exceptions.NotFound("Key not found for cookie.")

            # We need to write the max value to cookie else it will always go to the check_password login in _user_login
            # and we are not enforcing the lock.
            # Basically, we are saying lock out at 5, but that 5 needs to be set in cookie value
            if 0 < login_attempts <= auth_constants.MAX_FAILED_LOGIN_ATTEMPTS:
                # (for dev test with localhost, this cookie will not have any impact/lock since domain is .everstring)
                response.set_cookie(key,
                                    str(login_attempts),
                                    expires=datetime.utcnow() + timedelta(
                                        minutes=auth_constants.USER_LOCK_TIMEOUT_MINS),
                                    httponly=True,
                                    domain=domain)

            check_is_locked(login_attempts)

            if login_attempts == 0:
                # if we have changed password / forget password and essentially reset password, we should delete any
                # cookie that exists.
                # However, if cookie does not exist, then this will fail and hence we simply catch exception
                try:
                    response.delete_cookie(key, domain=domain)
                except AttributeError as err:
                    logging.debug("Cookie does not exist (nothing to delete).")
                except Exception as err:
                    logging.warning(err.message)
        except (exceptions.Unauthorized, exceptions.NotFound, Exception) as err:
            logging.error(err.message)

        if not session:
            self.redis.delete(self.get_redis_seesion_name(session.sid))
            if session.modified:
                response.delete_cookie(app.session_cookie_name,
                                       domain=domain)
            return
        # update the last fresh access ts for session
        session.update_last_ts()
        val = self.serializer.dumps(dict(session))

        redis_exp = RedisSessionInterface.get_redis_expiration_time(app, session)
        cookie_exp = self.get_expiration_time(app, session)
        self.redis.setex(self.prefix + session.sid,
                         int(redis_exp.total_seconds()), val)
        response.set_cookie(app.session_cookie_name, session.sid,
                            expires=cookie_exp, httponly=True,
                            domain=domain)
