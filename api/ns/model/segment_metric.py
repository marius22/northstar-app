import datetime
import json
import logging
from ns.model import db

logger = logging.getLogger(__name__)


class MetricNotExistException(Exception):
    pass


class SegmentMetric(db.Model):
    __tablename__ = 'segment_metric'

    METRIC_TECH = "tech"
    METRIC_IN_BOUNDARY_COUNT = "inBoundaryCount"
    METRIC_SECTOR = "sector"

    metric_types = [METRIC_TECH, METRIC_IN_BOUNDARY_COUNT]

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    type = db.Column(db.String(50), nullable=False)
    value = db.Column(db.Text, nullable=False)

    @staticmethod
    def get_by_type(segment_id, metric_type):
        segment_metric = SegmentMetric._get_original_metric(segment_id, metric_type)

        if segment_metric is None:
            logger.warn("Segment [%s] doesn't have [%s] metric" % (segment_id, metric_type))
            raise MetricNotExistException("Segment metric doesn't exist")

        return _SegmentMetricWrapper(segment_metric)

    @staticmethod
    def _get_original_metric(segment_id, metric_type):
        return SegmentMetric.query.filter(
            SegmentMetric.segment_id == segment_id,
            SegmentMetric.type == metric_type
        ).first()

    @staticmethod
    def save_metric_by_type(segment_id, metric_type, metric_value):

        metric = SegmentMetric._get_original_metric(segment_id, metric_type)
        if not metric:
            metric = SegmentMetric(
                segment_id=segment_id,
                type=metric_type,
                value=json.dumps(metric_value, ensure_ascii=False)
            )
            db.session.add(metric)
        else:
            metric.value = json.dumps(metric_value, ensure_ascii=False)

        db.session.commit()
        return _SegmentMetricWrapper(metric)

    @staticmethod
    def save_tech_metric(segment_id, tech_value):
        return SegmentMetric.save_metric_by_type(
            segment_id,
            SegmentMetric.METRIC_TECH,
            tech_value
        )

    @staticmethod
    def save_in_boundary_count_metric(segment_id, boundary_value):
        return SegmentMetric.save_metric_by_type(
            segment_id,
            SegmentMetric.METRIC_IN_BOUNDARY_COUNT,
            boundary_value
        )

    @staticmethod
    def save_sector_metric(segment_id, metric_value):
        return SegmentMetric.save_metric_by_type(
            segment_id,
            SegmentMetric.METRIC_SECTOR,
            metric_value
        )


class _SegmentMetricWrapper(object):

    def __init__(self, segment_metric):
        self.type = segment_metric.type
        self.value = json.loads(segment_metric.value)
