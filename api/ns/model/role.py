"""
Role model
"""
from ns.model import db
import datetime


class Role(db.Model):
    __tablename__ = 'role'

    ES_SUPER_ADMIN = "ES_SUPER_ADMIN"
    ES_ADMIN = "ES_ADMIN"
    ACCOUNT_ADMIN = "ACCOUNT_ADMIN"
    ACCOUNT_USER = "ACCOUNT_USER"
    TRIAL_USER = "TRIAL_USER"
    CHROME_USER = "CHROME_USER"

    role_priority = {
        ES_SUPER_ADMIN: 100,
        ES_ADMIN: 90,
        ACCOUNT_ADMIN: 50,
        ACCOUNT_USER: 10,
        CHROME_USER: 7,
        TRIAL_USER: 5
    }

    @staticmethod
    def role_comparer(x, y):
        return Role.role_priority.get(x.name, 0) - Role.role_priority.get(y.name, 0)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    name = db.Column(db.String(255), unique=True, nullable=False)

    @property
    def priority(self):
        return Role.role_priority.get(self.name)

    @staticmethod
    def get_all():
        return Role.query.all()

    @staticmethod
    def get_by_name(role_name):
        return Role.query.filter(Role.name == role_name).first()

    @staticmethod
    def get_by_id(role_id):
        return Role.query.filter(Role.id == role_id).first()

    @staticmethod
    def get_by_names(role_names):
        assert isinstance(role_names, list), "Invalid param"
        return Role.query.filter(Role.name.in_(role_names)).all()

    @staticmethod
    def is_super_admin(role_names):
        return Role.ES_SUPER_ADMIN in role_names

    @staticmethod
    def is_es_admin(role_names):
        return Role.ES_SUPER_ADMIN in role_names or Role.ES_ADMIN in role_names

    @staticmethod
    def is_account_admin(role_names):
        return Role.ACCOUNT_ADMIN in role_names

    @staticmethod
    def is_trial_user(role_names):
        if not role_names:
            return True
        if len(role_names) == 1 and Role.TRIAL_USER in role_names:
            return True

    @staticmethod
    def get_most_prior(roles):
        if roles:
            roles.sort(cmp=Role.role_comparer, reverse=True)
            return roles[0]
        return None

    def more_prior_than(self, other):
        if not other:
            return False
        return self.priority > other.priority

    def prior_or_same_as(self, other):
        if not other:
            return False
        return self.priority >= other.priority

    @property
    def serialized(self):
        return {
            'id': self.id,
            'name': self.name
        }
