"""
Model for saving account exported companies
"""
import datetime

from ns.model import db


class AccountExportedCompany(db.Model):
    __tablename__ = 'account_exported_company'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    company_id = db.Column(db.Integer, index=True, nullable=False)
    __table_args__ = (db.UniqueConstraint('account_id', 'company_id'),)

    @staticmethod
    def create_exports(account_id, company_ids):
        if not company_ids:
            return

        exports = []
        for company_id in company_ids:
            c = {
                "account_id": account_id,
                "company_id": company_id
            }
            exports.append(c)

        db.engine.execute(AccountExportedCompany.__table__.insert(), exports)

        db.session.commit()
