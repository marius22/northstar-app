"""
user segment many-to-many relationship model
"""
from ns.model import db
import datetime


class UserSegment(db.Model):
    __tablename__ = 'user_segment'

    # Permission Values
    # User can read/view any information about the segment
    READ_PERMISSION = "READ"
    # User can write to the segment. In other words, he can
    # modify the segment, including deleting it.
    WRITE_PERMISSION = "WRITE"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    permission_enum = (READ_PERMISSION, WRITE_PERMISSION)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    permission = db.Column(db.Enum(*permission_enum), nullable=False)

    @staticmethod
    def get_user_segments_by_user(user_id):
        return UserSegment.query.filter_by(user_id=user_id).all()

    @staticmethod
    def get_by_user_segment(user, segment):
        return UserSegment.query.filter(UserSegment.user_id == user.id,
                                        UserSegment.segment_id == segment.id).first()

    @staticmethod
    def _remove_permission_mapping(segment_ids, user_ids, permission):
        if not (segment_ids and user_ids):
            return
        UserSegment.query.filter(UserSegment.user_id.in_(user_ids),
                                 UserSegment.segment_id.in_(segment_ids),
                                 UserSegment.permission == permission)\
            .delete(synchronize_session=False)
        db.session.commit()

    @staticmethod
    def _add_permission_mapping(segment_ids, user_ids, permission):
        if not (segment_ids and user_ids):
            return
        to_be_added = []
        for segment_id in set(segment_ids):
            for user_id in set(user_ids):
                to_be_added.append(
                    UserSegment(user_id=user_id, segment_id=segment_id, permission=permission)
                )
        db.session.add_all(to_be_added)
        db.session.commit()

    @staticmethod
    def add_reading_permission(segment_ids, user_ids):
        UserSegment._remove_permission_mapping(
            segment_ids, user_ids, UserSegment.READ_PERMISSION)

        UserSegment._add_permission_mapping(
            segment_ids, user_ids, UserSegment.READ_PERMISSION)
