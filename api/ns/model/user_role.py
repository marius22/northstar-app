"""
user role map model
"""
import datetime
from ns.model import db
from ns.model.role import Role
from sqlalchemy.orm import relationship


class UserRole(db.Model):
    __tablename__ = 'user_role'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"), nullable=False)

    user = relationship('User', backref='user_roles')
    role = relationship(Role)

    @staticmethod
    def get_users_by_role(role):
        """
        query users by role
        :param role:
        :return:
        """
        from ns.model.user import User
        if role is None:
            return []
        return User.query.join(UserRole) \
            .filter(UserRole.user_id == User.id, UserRole.role_id == role.id) \
            .all()

    @staticmethod
    def get_roles_by_user(user):
        if user is None:
            return []
        return Role.query.join(UserRole, Role.id == UserRole.role_id). \
            filter(UserRole.user_id == user.id).all()

    @staticmethod
    def create(user, role):
        user_role = UserRole(user_id=user.id, role_id=role.id)
        db.session.add(user_role)
        db.session.commit()

    @staticmethod
    def unassign_roles_from_user(user, roles):
        if not roles:
            return
        role_ids = [role.id for role in roles]

        db.session.query(UserRole). \
            filter(UserRole.user_id == user.id,
                   UserRole.role_id.in_(role_ids)). \
            delete(synchronize_session=False)
        db.session.commit()

    @staticmethod
    def assign_roles_to_user(user, roles):
        if not roles:
            return
        role_ids = [role.id for role in roles]
        user_roles = []
        for role_id in role_ids:
            user_roles.append(UserRole(user_id=user.id, role_id=role_id))
        db.session.add_all(user_roles)
        db.session.commit()

    @staticmethod
    def reset_role_for_user(user, roles):
        old_roles = UserRole.get_roles_by_user(user)
        UserRole.unassign_roles_from_user(user, old_roles)
        UserRole.assign_roles_to_user(user, roles)

    def __repr__(self):
        return '<UserRole user=%s, role=%s>' % (self.user, self.role)
