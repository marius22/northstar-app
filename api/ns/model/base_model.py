from sqlalchemy.ext.declarative import declared_attr
import os


class BaseModel(object):

    @declared_attr
    def __table_args__(cls):
        if os.environ.get('ENV', 'dev') == 'test':
            return {}
        return {'schema': 'northstar'}
