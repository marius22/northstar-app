"""
tmp account segment many-to-many relationship model
"""
from ns.model import db
import datetime


class TmpAccountSegment(db.Model):
    __tablename__ = 'tmp_account_segment'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)

    @staticmethod
    def create(account_id, segment_id):
        acc_seg = TmpAccountSegment.query.\
            filter_by(account_id=account_id, segment_id=segment_id).first()
        if not acc_seg:
            acc_seg = TmpAccountSegment(account_id=account_id, segment_id=segment_id)
            db.session.add(acc_seg)
            db.session.commit()
        return acc_seg

    @staticmethod
    def delete_by_account_segment(account_id, segment_id):
        TmpAccountSegment.query.filter_by(account_id=account_id, segment_id=segment_id).\
            delete(synchronize_session=False)
        db.session.commit()
