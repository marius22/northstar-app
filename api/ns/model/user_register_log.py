"""
user role map model
"""
import datetime
from ns.model import db


class UserRegisterLog(db.Model):
    __tablename__ = 'user_register_log'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    email = db.Column(db.String(255), unique=True, nullable=False)
    first_name = db.Column(db.String(255), nullable=True)
    last_name = db.Column(db.String(255), nullable=True)
    company = db.Column(db.String(255), nullable=True)

    @staticmethod
    def create_register_log(first_name, last_name, company, email):
        userRegisterLog = UserRegisterLog(
            first_name=first_name,
            last_name=last_name,
            email=email,
            company=company
        )
        db.session.add(userRegisterLog)
        db.session.commit()
