import datetime
import json

from ns.model import db


class UserPreference(db.Model):

    __tablename__ = 'user_preference'

    APP_SFDC = "sfdc"
    APP_CHROME_EXTENSION = "chrome"

    SUPPORT_APP_NAMES = [APP_SFDC, APP_CHROME_EXTENSION]

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sfdc_user_id = db.Column(db.String(255), nullable=False, server_default="")
    email = db.Column(db.String(255), nullable=False, server_default="")
    app_name = db.Column(db.String(50), nullable=False)
    type = db.Column(db.String(50), nullable=False)
    val = db.Column(db.Text, nullable=False)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    __table_args__ = (
        db.UniqueConstraint(
            'sfdc_user_id', "app_name", 'type', name="uniq_sfdc_user_id_app_name_type"),
    )

    @property
    def val_obj(self):
        assert self.val, "Invalid criteria value"
        return json.loads(self.val)

    @property
    def serialized(self):
        return {
            "id": self.id,
            "sfdcUserId": self.sfdc_user_id,
            "email": self.email,
            "appName": self.app_name,
            "type": self.type,
            "val": self.val
        }

    @staticmethod
    def get_by_email_app(email, app_name):
        return UserPreference.get_by_email_app_types(email, app_name, None)

    @staticmethod
    def get_by_email_app_types(email, app_name, types):
        query = UserPreference.query\
            .filter(UserPreference.email == email,
                    UserPreference.app_name == app_name)
        if types:
            query = query.filter(UserPreference.type.in_(types))

        return query.all()

    @staticmethod
    def get_by_sfdc_user_id_app(sfdc_user_id, app_name):
        return UserPreference.get_by_user_id_app_and_type(sfdc_user_id, app_name, None)

    @staticmethod
    def get_by_sfdc_user_id_app_and_type(sfdc_user_id, app_name, types=None):
        query = UserPreference.query\
            .filter(UserPreference.sfdc_user_id == sfdc_user_id,
                    UserPreference.app_name == app_name)

        if types:
            query = query.filter(UserPreference.type.in_(types))

        return query.all()

    @staticmethod
    def save_preference(sfdc_user_id, email, app_name, type, value_obj):
        assert email, "Invalid email"
        assert app_name in UserPreference.SUPPORT_APP_NAMES, "Invalid appName"

        preferences = UserPreference.get_by_sfdc_user_id_app_and_type(sfdc_user_id, app_name, [type])
        preference = preferences[0] if preferences else None

        if not preference:
            preference = UserPreference(
                sfdc_user_id=sfdc_user_id,
                email=email,
                app_name=app_name,
                type=type,
                val=json.dumps(value_obj)
            )
            db.session.add(preference)
        else:
            preference.val = json.dumps(value_obj)

        db.session.commit()
        return preference
