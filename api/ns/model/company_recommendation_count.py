"""
CompanyRecommendationCount model
"""
from ns.model import db
import datetime


class CompanyRecommendationCount(db.Model):
    __tablename__ = 'company_recommendation_count'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    record_date = db.Column(db.DATE, nullable=True)
    company_id = db.Column(db.Integer, index=True, nullable=False)
    count = db.Column(db.Integer, nullable=True)
