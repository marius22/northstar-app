"""
Publish field mapping table is used to map field names to publish field names.

Ex. Map internal Everstring name (ES_REVENUE) to customer's Salesforce custom
field name (REVENUE_REPORT)
"""
import calendar
import datetime

from ns.constants.insight_constants import InsightConstants
from ns.model import db
from ns.model.account_quota import AccountQuota
from ns.model.audience import Audience
from ns.model.company_insight import CompanyInsight
from ns.model.crm_token import CRMToken, SFDCToken
from ns.model.model import Model
from ns.model.mas_token import MASToken

from ns.constants.default_field_mapping_constants import DEFAULT_MAPPING, \
    TARGET_TYPE_SALESFORCE, TARGET_TYPE_MARKETO, TARGET_TYPE_MARKETO_SANDBOX, TARGET_TYPE_CSV, \
    TARGET_OBJECT_LEAD, TARGET_OBJECT_CONTACT, TARGET_OBJECT_ACCOUNT, \
    TARGET_OBJECT_COMPANY
from ns.model.salesforce import get_salesforce_fields


class PublishFieldMappingStatus(object):
    CREATED = "created"
    UPDATED = "updated"
    DELETED = "deleted"
    NO_ACTION = "no action"


class InvalidOrgId(Exception):
    pass


class CompanyPublishData(object):
    def __init__(self, company_data):
        self.data = company_data

    def _get_insight_value(self, insight_name):
        # advanced_insights is a list of advanced insight dictionaries. We need to...
        if insight_name in InsightConstants.ADVANCED_INSIGHTS:
            # ... use this EXACT_NAME_MAP to get the exact_name, then...
            exact_name = InsightConstants.ADVANCED_INSIGHT_NAME_EXACT_NAME_MAP.get(insight_name)
            # ... filter on the exact_name to get our desired advanced insight dictionary, then...
            advanced_insights = self.data.get("advanced_insights", [])
            advanced_insight = filter(
                lambda adv_insight: adv_insight["name"] == exact_name,
                advanced_insights
            )
            # ... return the "value" for that advanced insight, or None if it doesn't exist.
            return advanced_insight[0]["value"] if advanced_insight else None

        # keywords is a list of keyword strings, so we return a string of pipe-separated keywords.
        # We want to only return the top 20 keywords. keywords is already sorted by score.
        if insight_name == InsightConstants.INSIGHT_NAME_KEYWORDS:
            keywords = self.data.get(insight_name, [])
            return InsightConstants.INSIGHT_DELIMITER.join(keywords[:20])

        # Get audience names from audience_ids, then return a string of pipe-separated audience names.
        if insight_name == InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES:
            audience_ids = self.data.get('audience_ids', [])
            audiences = [Audience.get_by_id(audience_id) for audience_id in audience_ids]
            audience_names = [a.serialized["name"] for a in audiences if a]
            return InsightConstants.INSIGHT_DELIMITER.join(audience_names)

        # model_scores is a dictionary that maps model ids to corresponding fit scores.
        # Thus, insight_name will have to be a model id, so this is the only case in which
        # insight_name is an int type.
        insight_name_is_model_id = type(insight_name) == int
        if insight_name_is_model_id:
            model_scores = self.data.get('scores', {})
            return model_scores.get(str(insight_name))

        # If we hit this else case, we can handle all other insights in a general way.
        else:
            insight_value = self.data.get(insight_name)
            if type(insight_value) == list:
                return InsightConstants.INSIGHT_DELIMITER.join(insight_value)
            else:
                return insight_value

    def get_insight_value(self, insight_name):
        try:
            return self._get_insight_value(insight_name)
        except:
            return None


class PublishFieldMapping(db.Model):
    __tablename__ = 'publish_field_mapping'

    target_type_enums = [TARGET_TYPE_SALESFORCE,
                         TARGET_TYPE_MARKETO,
                         TARGET_TYPE_MARKETO_SANDBOX,
                         TARGET_TYPE_CSV]
    # company refers to csv as target type
    target_object_enums = [TARGET_OBJECT_LEAD,
                           TARGET_OBJECT_CONTACT,
                           TARGET_OBJECT_ACCOUNT,
                           TARGET_OBJECT_COMPANY]

    account_id = db.Column('account_id', db.BigInteger, primary_key=True, nullable=False)
    insight_id = db.Column('insight_id', db.BigInteger, nullable=True)
    model_id = db.Column('model_id', db.Integer, nullable=True)
    target_type = db.Column('target_type', db.Enum(*target_type_enums, name='target_type'),
                            primary_key=True)
    target_object = db.Column('target_object', db.Enum(*target_object_enums, name='target_object'),
                              primary_key=True)
    # target_org_id is salesforce org id for salesforce, account_id for csv
    target_org_id = db.Column('target_org_id', db.String(255), primary_key=True)
    target_field = db.Column('target_field', db.String(255), primary_key=True)
    created_ts = db.Column('created_ts', db.TIMESTAMP, nullable=True, default=datetime.datetime.utcnow)
    updated_ts = db.Column('updated_ts', db.TIMESTAMP, nullable=True, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @property
    def display_name(self):
        if self.insight_id is None:
            return Model.get_by_id(self.model_id).name
        return CompanyInsight.get_display_name_by_id(self.insight_id).scalar()

    @property
    def company_column_name(self):
        if self.insight_id is not None:
            return CompanyInsight.get_by_id(self.insight_id).company_column_name
        return None

    @property
    def serialized(self):
        return {
            "targetType": self.target_type,
            "targetObject": self.target_object,
            "targetField": self.target_field,
            "accountId": self.account_id,
            "insightId": self.insight_id,
            "modelId": self.model_id,
            "updatedTs": calendar.timegm(self.updated_ts.utctimetuple())
        }

    @staticmethod
    def get_by_filters(filter_dict, account_id):
        """Find rows in database with given values.

        :param: filter_dict (Dict[PublishFieldMapping attributes, AnyType])
        Ex. {PublishFieldMapping.account_id:1, PublishFieldMapping.insight_id:1}

        :return: SqlAlchemy objects
        """
        criteria = map(lambda (f, v): f == v, filter_dict.iteritems())
        filtered_mapping = PublishFieldMapping.query.filter(*criteria)
        filtered_results = filtered_mapping.all()

        # if no mappings found, then create the default mappings which are all
        if not filtered_results:
            # target_org_id is already the second item in values
            target_type = filter_dict.get(PublishFieldMapping.target_type)
            target_org_id = filter_dict.get(PublishFieldMapping.target_org_id)
            target_object = filter_dict.get(PublishFieldMapping.target_object)
            PublishFieldMapping.__create_default_fields(account_id,
                                                        target_type,
                                                        target_object,
                                                        target_org_id)
            filtered_results = PublishFieldMapping.query.filter(
                *criteria).all()
        return filtered_results

    @staticmethod
    def __create_default_fields(account_id, target_type, target_object, target_org_id):
        """loop through all fields for target type and target object in the
        DEFAULT MAPPING and call the self.__create function to create
        default rows in the db for this account"""

        # Marketo doesn't have any default mappings saved to the `publish_field_mapping` table.
        if target_type in [TARGET_TYPE_MARKETO, TARGET_TYPE_MARKETO_SANDBOX]:
            return

        display_to_target_field_dict = DEFAULT_MAPPING[target_type][target_object]
        insight_names = display_to_target_field_dict.keys()

        insight_ids_dict = {}
        for insight in insight_names:
            insight_ids = CompanyInsight.get_ids_by_display_names([insight])
            if insight_ids:
                insight_ids_dict[insight] = insight_ids[0]

        for insight_name, target_field in display_to_target_field_dict.iteritems():
            insight_id = insight_ids_dict.get(insight_name, None)
            model_id = None

            # overall fit score is a special default field that needs checking vs
            # models as opposed to company insights table
            if insight_name == InsightConstants.INSIGHT_NAME_OVERALL_FIT_SCORE:
                model_data = Model.get_latest_completed_model(account_id, Model.ACCOUNT_FIT)
                if model_data:
                    model_id = model_data.id

            if insight_id or model_id:
                PublishFieldMapping.__create(account_id, insight_id, model_id, target_type,
                                             target_object, target_org_id, target_field)

    @staticmethod
    def __get_one(account_id, insight_id, model_id, target_type, target_object, target_org_id):
        """Return a unique field mapping if it exists."""
        return PublishFieldMapping.query.filter(
            PublishFieldMapping.account_id == account_id,
            PublishFieldMapping.insight_id == insight_id,
            PublishFieldMapping.model_id == model_id,
            PublishFieldMapping.target_type == target_type,
            PublishFieldMapping.target_object == target_object,
            PublishFieldMapping.target_org_id == target_org_id
        ).first()

    @staticmethod
    def __create(account_id, insight_id, model_id, target_type, target_object, target_org_id, target_field):
        obj = PublishFieldMapping(
            account_id=account_id,
            insight_id=insight_id,
            model_id=model_id,
            target_type=target_type,
            target_object=target_object,
            target_org_id=target_org_id,
            target_field=target_field
        )
        db.session.add(obj)
        db.session.commit()

    @staticmethod
    def __update(obj, new_target_field):
        """Update an existing object."""
        if isinstance(obj, PublishFieldMapping) and obj.target_field != new_target_field:
            obj.target_field = new_target_field
            db.session.commit()

    @staticmethod
    def get_target_org_id(account_id, target_type):
        """Get unique id based on target type.

        Salesforce -> organization_id in crm_token table
        Marketo -> mas_id in mas_token table
        CSV -> account_id in account table
        """
        target_org_id = None
        if target_type == TARGET_TYPE_SALESFORCE:
            obj = CRMToken.get_by_account_id(account_id)
            target_org_id = obj.organization_id if obj else None
        elif target_type == TARGET_TYPE_MARKETO or target_type == TARGET_TYPE_MARKETO_SANDBOX:
            mas_token = MASToken.get_by_account_id_and_mas_type(account_id, target_type)
            target_org_id = mas_token.mas_id
        elif target_type == TARGET_TYPE_CSV:
            target_org_id = account_id
        else:
            raise TypeError("Invalid target type: %s" % target_type)
        if target_org_id is None:
            raise InvalidOrgId(
                "Account id %d cannot publish to %s" % (account_id, target_type))
        return str(target_org_id)

    @staticmethod
    def upsert(account_id, insight_id, model_id, target_type, target_object, target_org_id, target_field):
        # only one account_fit model allowed, therefore, if model type is account_fit,
        # always use the most recent account_fit model id
        if model_id and Model.get_by_id(model_id).type == Model.ACCOUNT_FIT:
            model_id = Model.get_latest_completed_model(
                account_id, Model.ACCOUNT_FIT).id

        obj = PublishFieldMapping.__get_one(
            account_id, insight_id, model_id, target_type, target_object, target_org_id)
        if obj is None:
            PublishFieldMapping.__create(account_id, insight_id, model_id, target_type, target_object, target_org_id,
                                         target_field)
        else:
            PublishFieldMapping.__update(obj, target_field)

    @staticmethod
    def delete(account_id, insight_id, model_id, target_type, target_object, target_org_id, target_field):
        obj = PublishFieldMapping.query.filter(
            PublishFieldMapping.account_id == account_id,
            PublishFieldMapping.insight_id == insight_id,
            PublishFieldMapping.model_id == model_id,
            PublishFieldMapping.target_type == target_type,
            PublishFieldMapping.target_object == target_object,
            PublishFieldMapping.target_org_id == target_org_id,
            PublishFieldMapping.target_field == target_field
        ).first()

        if obj is not None:
            db.session.delete(obj)
            db.session.commit()

    @staticmethod
    def delete_model_by_model_id(model_id):
        model = Model.get_by_id(model_id)
        Model.delete_by_id(model_id, commit=False)
        if model.type != Model.ACCOUNT_FIT:
            PublishFieldMapping.query.filter(
                PublishFieldMapping.model_id == model_id
            ).delete()
        db.session.commit()

    @staticmethod
    def verify_ids(account_id, insight_id, model_id):
        """Check that ids are valid."""
        from ns.model.account import Account
        assert Account.get_by_id(account_id)
        # cannot have both insight_id and model_id because a
        # field is either an insight or a model score
        assert insight_id is None or model_id is None, "Either insight id or model id has to be None"
        assert insight_id is not None or model_id is not None, "Either insight id or model id has to be valid"

        if insight_id is not None:
            assert CompanyInsight.get_by_id(insight_id), "Invalid insight id"
        elif model_id is not None:
            assert Model.get_by_id(model_id), "Invalid model id"

    @staticmethod
    def filter_old_account_fit_models(account_id, objs):
        """Given an array of publish_field_mapping objects, return a subset where there exists at most one account
        fit model field mapping (the account fit model is the most recent).
        """
        if objs:
            # partition fields into insights and models
            insight_fields = filter(lambda p: p.insight_id is not None, objs)
            model_fields = filter(lambda p: p.model_id is not None, objs)

            if model_fields:
                model_fields = PublishFieldMapping.regenerate_model_field_mapping(account_id, model_fields)
                objs = insight_fields + model_fields
        return objs

    @staticmethod
    def get_publish_field_mapping(account_id, target_type, target_org_id, target_object):
        # get row in database with specified publish type and publish object
        filter_dict = {
            PublishFieldMapping.target_type: target_type,
            PublishFieldMapping.target_org_id: target_org_id,
            PublishFieldMapping.account_id: account_id
        }

        if target_object:
            filter_dict[PublishFieldMapping.target_object] = target_object

        results = PublishFieldMapping.get_by_filters(
            filter_dict, account_id)

        # TODO need to handle marketo later
        if target_type in [TARGET_TYPE_SALESFORCE]:
            available_fields = PublishFieldMapping.get_target_available_fields(account_id,
                                                                               target_type,
                                                                               target_object)

            results = [one for one in results if one.target_field in available_fields]

        # make sure only one account fit model is returned
        results = PublishFieldMapping.filter_old_account_fit_models(
            account_id, results)
        results = [m.serialized for m in results]

        # get exposed_csv_insight_ids from account_quota table, these are the insights
        # exposed to the user.
        allowed_insight_ids = \
            AccountQuota.get_by_account_id(account_id).allowed_insight_ids

        results = filter(lambda k: k["insightId"] in allowed_insight_ids or k["insightId"] is None, results)

        # if scoring is not turned on, do not return model fields
        if not AccountQuota.get_by_account_id(account_id).scoring_enabled:
            # model fields have a None model_id
            results = filter(lambda x: x["modelId"] is None, results)

        for result in results:
            if result["insightId"]:
                result["displayName"] = CompanyInsight.get_display_name_by_id(result["insightId"]).one()[0]
            elif result["modelId"]:
                result["displayName"] = Model.get_by_id(result["modelId"]).name
        model_mappings = [result for result in results if result.get("modelId")]
        company_insight_mapping = [result for result in results if result.get("insightId")]
        model_mappings = sorted(model_mappings, key=lambda record: record.get("displayName").lower())
        company_insight_mapping = sorted(company_insight_mapping, key=lambda record: record.get("displayName").lower())
        return company_insight_mapping + model_mappings

    @staticmethod
    def regenerate_model_field_mapping(account_id, model_field_mappings):
        result = list()
        model_ids = [m.model_id for m in model_field_mappings]
        models = Model.get_by_ids(model_ids, keep_deleted_models=True)
        account_fit_model_ids = [model.id for model in models if model.type == Model.ACCOUNT_FIT]
        newest_account_fit_model = Model.get_latest_completed_model(account_id, Model.ACCOUNT_FIT)
        account_fit_num = 0
        for model_field_mapping in model_field_mappings:
            model_id = model_field_mapping.model_id
            if model_id in account_fit_model_ids:
                if account_fit_num == 0 and newest_account_fit_model:
                    model_field_mapping.model_id = newest_account_fit_model.id
                    result.append(model_field_mapping)
                    account_fit_num += 1
            else:
                result.append(model_field_mapping)
        return result

    @staticmethod
    def get_target_available_fields(account_id, target_type, target_object):

        if target_type in [TARGET_TYPE_SALESFORCE] and target_object:
            target_object_schema = get_salesforce_fields(account_id, target_object)
            fields_names = [one["name"] for one in target_object_schema]
            return fields_names
        else:
            return []
