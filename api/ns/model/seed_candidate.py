"""
Seed Candidate Model
"""
import datetime
import json
import logging

from flask import current_app
from ns.model import db
from ns.util.company_db_client import CompanyDBClient
from sqlalchemy.orm import relationship
from ns.util.param_util import get_value


logger = logging.getLogger(__name__)


class SeedCandidate(db.Model):
    __tablename__ = 'seed_candidate'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    source_id = db.Column(db.Integer, db.ForeignKey("source.id"), nullable=False)
    company_id = db.Column(db.Integer, nullable=False)
    company_name = db.Column(db.String(255), nullable=True)
    domain = db.Column(db.String(255), nullable=False)
    revenue = db.Column(db.String(255), nullable=True)
    industry = db.Column(db.String(255), nullable=True)
    employee_size = db.Column(db.String(255), nullable=True)
    alexa_rank = db.Column(db.Integer, nullable=True)

    # Location
    country = db.Column(db.String(255), nullable=True)
    state = db.Column(db.String(255), nullable=True)
    city = db.Column(db.String(255), nullable=True)

    sf_account_id = db.Column(db.String(255), nullable=True)
    sf_account_name = db.Column(db.String(255), nullable=True)

    tech = db.Column(db.Text, nullable=True)
    department_size = db.Column(db.String(4096), nullable=True)
    growth = db.Column(db.String(4096), nullable=True)

    source = relationship('Source')

    @staticmethod
    def get_by_ids(seed_candidate_ids):
        if not seed_candidate_ids:
            return []

        return SeedCandidate.query.filter(SeedCandidate.id.in_(seed_candidate_ids)).all()

    @staticmethod
    def delete_by_seg_source_id(segment_id, source_id):
        """
        Delete seeds that have the same segment_id, source_id value with arguments.
        For re-upload.
        :param segment_id:
        :param source_id:
        :return:
        """
        SeedCandidate.query.filter(SeedCandidate.segment_id == segment_id,
                                   SeedCandidate.source_id == source_id). \
            delete(synchronize_session=False)
        db.session.commit()

    @staticmethod
    def delete_by_seg_id(segment_id):
        """
        Delete seeds that have the same segment_id, source_id value with arguments.
        For re-upload.
        :param segment_id:
        :return:
        """
        SeedCandidate.query.filter(SeedCandidate.segment_id == segment_id). \
            delete(synchronize_session=False)
        db.session.commit()

    @staticmethod
    def save_batch(segment_id, source_id, seeds):
        """
        Save seed in seeds to table seed_candidate
        :param segment_id:
        :param source_id:
        :param seeds:
        :return:
        """

        def enrich_candidate(seed):
            seed.segment_id = segment_id
            seed.source_id = source_id
            return seed

        db.session.add_all(map(enrich_candidate, seeds))
        db.session.commit()

    @staticmethod
    def get_by_seg_source_id(segment_id, source_id, page, per_page):
        paginate = SeedCandidate.query \
            .filter(SeedCandidate.segment_id == segment_id,
                    SeedCandidate.source_id == source_id) \
            .paginate(page, per_page, False)
        return paginate.items

    @staticmethod
    def _maps(this):
        return {
            'id': this.id,
            'companyId': this.company_id,
            'companyName': this.company_name,
            'domain': this.domain,
            'revenue': this.revenue,
            'industry': this.industry,
            'employeeSize': this.employee_size,
            'alexaRank': this.alexa_rank,

            # Location
            "country": this.country,
            "state": this.state,
            "city": this.city,
            "SFDCAccountId": this.sf_account_id,
            "SFDCAccountName": this.sf_account_name,
            "tech": json.loads(this.tech) if isinstance(this, SeedCandidate) else this.tech,
            "departmentSize":
                json.loads(this.department_size)
                if isinstance(this, SeedCandidate) else this.department_size,

            "growthIndicator": this.growth,
            # alias
            "growth": this.growth
        }

    @property
    def serialized(self):
        return self.__class__._maps(self)

    @classmethod
    def get_field_by_key(cls, key):
        return cls._maps(cls).get(key)

    @classmethod
    def insights(cls, seed_candidates):
        """
        API request sent to the Company service(author: Yi-Shiuan)
        Parameters
        ----------
        seed_candidates

        Returns
        -------
            dict represented insights
        """
        domains = [sc.domain for sc in seed_candidates]

        client = CompanyDBClient.instance()
        return client.get_insights_by_domains(domains)

    def enrich_with_profile(self, external_profile):
        """
        Parameters
        ----------
        external_profile
            A dict that exhibits the profile of the seed candidate

        Returns
        -------

        """
        logger.debug('Enrich seed candidate with profile: '
                     'SeedCandidate(%s), Profile(%s)',
                     self,
                     json.dumps(external_profile))

        self.company_name = external_profile.get('name')
        self.industry = external_profile.get('industry')

        self.revenue = SeedCandidate.\
            bucketize_revenue(external_profile.get('revenueRange'))
        self.employee_size = SeedCandidate.\
            bucketize_employee_size(external_profile.get('employeeSize'))

        self.company_id = external_profile.get('id')

        location = external_profile.get('location', {})
        self.city = location.get('city')
        self.state = location.get('state')
        self.country = location.get('country')

        self.tech = json.dumps(get_value(external_profile, "tech", []))
        self.department_size \
            = json.dumps(get_value(external_profile, "departmentSize", []))
        self.growth = external_profile.get("growthIndicator", '')

        self.alexa_rank = external_profile.get('metrics', {}).get('alexaGlobalRank')

    def enrich_with_null(self):
        # Set defaults to either 1 or 'NA' for now depends on the column type
        """
        To be implemented, set defaults to either 1 or 'NA' for now depends on the column type
        Returns
        -------

        """

        fields = [
            'company_id',
            'company_name',
            'domain',
            'revenue',
            'industry',
            'employee_size',

            "country",
            "state",
            "city",

            "alexa_rank",
            "sf_account_id",
            "sf_account_name"
        ]

        for field in fields:
            if not getattr(self, field):
                column_def = getattr(SeedCandidate, field)
                if column_def.type.python_type == int:
                    value = 0
                else:
                    value = ''

                setattr(self, field, value)
        if not self.tech:
            self.tech = "[]"
        if not self.department_size:
            self.department_size = "[]"
        if not self.growth:
            self.growth = ""

    @classmethod
    def enrich_all(cls, seed_candidates):
        """
        Enrich seed_candidates with the domains by requesting the
        Company service(author: Yi-Shiuan) API

        This method just enrich the object in the memory, it doesn't save anything to the database

        Parameters
        ----------
        seed_candidates

        Returns
        -------
        Enriched seed candidates

        """
        start = datetime.datetime.utcnow()
        logger.info("Begin enrich seedCandidates")

        domains = [sc.domain for sc in seed_candidates]

        client = CompanyDBClient.instance()
        domain_profile_maps = client.get_enrichment_by_domains(domains)

        for sc in seed_candidates:
            if sc.domain in domain_profile_maps:
                # Found the profile: enrich candidate with the profile
                profile = domain_profile_maps[sc.domain]

                try:
                    sc.enrich_with_profile(profile)
                except Exception, e:
                    raise Exception('Failed enriching %s with external profile %s. '
                                    'Original exception: %s' % (sc, profile, e))

            # Enrich empty fields with null
            sc.enrich_with_null()
        logger.info("Enrich seedCandidates completed, time cost: {%s}"
                    % (datetime.datetime.utcnow() - start))

    @staticmethod
    def bucketize_revenue(num):
        # Coming data are in (\d+)k
        if num:
            num = int(num) * 1000
        buckets = current_app.config.get('REVENUE_BUCKETS')
        return SeedCandidate.bucketize(num, buckets)

    @staticmethod
    def bucketize_employee_size(num):
        buckets = current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
        return SeedCandidate.bucketize(num, buckets)

    @staticmethod
    def bucketize(num, buckets):
        if not num:
            return num

        for bucket in buckets:
            if num <= bucket.get('max'):
                return bucket.get('label')

    def __repr__(self):
        return '<SeedCandidate id=%s, company_name=%s, domain=%s>' % \
               (self.id, self.company_name, self.domain)

    @staticmethod
    def query_seed_candidates(segment_id, source_id, **kwargs):

        limit = kwargs.get("limit", 100)
        offset = kwargs.get("offset", 0)
        criterion = kwargs.get("criterion")
        type = kwargs.get("type")

        query = SeedCandidate.query.\
            filter(SeedCandidate.segment_id == segment_id,
                   SeedCandidate.source_id == source_id)
        if type == 'unknown':
            query = query.filter(SeedCandidate.domain == '')
        else:
            query = query.filter(SeedCandidate.domain != '')
        page = query.order_by(criterion).paginate(offset / limit + 1, limit, False)
        return page.total, page.items

    def to_dict(self):
        c = dict()
        c['segment_id'] = self.segment_id
        c['source_id'] = self.source_id
        c['company_id'] = self.company_id
        c['company_name'] = self.company_name
        c['domain'] = self.domain
        c['revenue'] = self.revenue
        c['industry'] = self.industry
        c['employee_size'] = self.employee_size
        c["country"] = self.country
        c["state"] = self.state
        c["city"] = self.city
        c["alexa_rank"] = self.alexa_rank
        c["sf_account_id"] = self.sf_account_id
        c["sf_account_name"] = self.sf_account_name
        c["tech"] = self.tech
        c["department_size"] = self.department_size
        c["growth"] = self.growth

        return c

if __name__ == '__main__':
    sc1 = SeedCandidate(domain='alf.org')
    sc2 = SeedCandidate(domain='databrain.com')

    candidates = [sc1, sc2]

    print(SeedCandidate.enrich_all(candidates))
