"""
Recommendation model
"""
import datetime
import os
import ujson
from collections import OrderedDict

import unicodecsv
from ns.model import db, AnnotatedBaseQuery
from ns.model.base_model import BaseModel
from ns.model.company_insight import CompanyInsight
from sqlalchemy import and_, or_
from ns.model.account_quota import AccountQuota
from ns.util.csv_suite import generate_csv_file
from tempfile import mkstemp


def decode_list(l):
    return repr([x.encode('utf-8') for x in l]).decode('string-escape')


class FilterBuildError(AttributeError):
    pass


class Contact:
    """
    Recommended contacts live in the memory
    """

    def __init__(self, first_name, last_name, account):
        self.first_name = first_name
        self.last_name = last_name
        self.account = account

    def __repr__(self):
        return '<Contact first_name=%s, last_name=%s, account=%s>' \
               % (self.first_name, self.last_name, self.account)


class Recommendation(db.Model, BaseModel):
    __bind_key__ = 'redshift'
    __tablename__ = 'recommendation'

    EXPORTED = 1
    NOT_EXPORTED = 0
    SIMILARITY_SCORE_THRESHOLD = 4
    DEPARTMENT_STRENGTH_MAP = {1: 'Low', 5: 'Medium', 9: 'High'}
    query_class = AnnotatedBaseQuery

    segment_id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, primary_key=True)
    job_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    company_name = db.Column(db.String(255), nullable=True)
    domain = db.Column(db.String(255), nullable=False)
    score = db.Column(db.Float, nullable=True)
    revenue = db.Column(db.String(255), nullable=True)
    industry = db.Column(db.String(255), nullable=True)
    employee_size = db.Column(db.String(255), nullable=True)
    alexa_rank = db.Column(db.Integer, nullable=True)
    tech = db.Column(db.Text, nullable=True)
    growth = db.Column(db.String(255), nullable=True)

    # Location
    country = db.Column(db.String(255), nullable=True)
    state = db.Column(db.String(255), nullable=True)
    city = db.Column(db.String(255), nullable=True)
    zipcode = db.Column(db.String(255), nullable=True)

    # Discussion:
    # http://stackoverflow.com/questions/219569/best-database-field-type-for-a-url
    linkedin_url = db.Column(db.Text, nullable=True)

    similar_companies = db.Column(db.Text, nullable=True)
    similar_technologies = db.Column(db.String(4096), nullable=True)
    similar_departments = db.Column(db.String(4096), nullable=True)
    tech_categories = db.Column(db.String(4096), nullable=True)

    # similar company score
    similar_c_score = db.Column(db.SmallInteger, index=True, nullable=True)
    # similar technology score
    similar_t_score = db.Column(db.SmallInteger, index=True, nullable=True)
    # similar department score
    similar_d_score = db.Column(db.SmallInteger, index=True, nullable=True)

    # departments
    d_administrative = db.Column(db.SmallInteger, nullable=True)
    d_computing_it = db.Column(db.SmallInteger, nullable=True)
    d_educator = db.Column(db.SmallInteger, nullable=True)
    d_engineering = db.Column(db.SmallInteger, nullable=True)
    d_finance = db.Column(db.SmallInteger, nullable=True)
    d_hr = db.Column(db.SmallInteger, nullable=True)
    d_legal = db.Column(db.SmallInteger, nullable=True)
    d_marketing = db.Column(db.SmallInteger, nullable=True)
    d_medical_heath = db.Column(db.SmallInteger, nullable=True)
    d_operations = db.Column(db.SmallInteger, nullable=True)
    d_research_dev = db.Column(db.SmallInteger, nullable=True)
    d_sales = db.Column(db.SmallInteger, nullable=True)

    @property
    def contacts(self):
        """
        Backend will return the contact to us, considering the volume of the data,
        We won't save them in the APP database.

        Returns
        -------
        Fixture contacts for the SFDC publish to work correctly.
        """

        fixture_contact = Contact('John', 'Smith', self)

        return [fixture_contact]

    @staticmethod
    def _maps(this):
        """
            Only expose fields required for the UI display

            Args:
                this: either the class or the object

            Returns:

        """
        return OrderedDict([
            ("companyName", this.company_name),
            ("companyId", this.company_id),
            ("domain", this.domain),
            ("similarCompanies", this.similar_companies),
            ("similarTechnologies", this.similar_technologies),
            ("similarDepartments", this.similar_departments),
            ("score", this.score),
            ("growth", this.growth),
            ("tech", this.tech),
            ("administrative", this.d_administrative),
            ("engineering", this.d_engineering),
            ("finance", this.d_finance),
            ("hr", this.d_hr),
            ("IT", this.d_computing_it),
            ("legal", this.d_legal),
            ("marketing", this.d_marketing),
            ("medicalHealth", this.d_medical_heath),
            ("operations", this.d_operations),
            ("researchDevelopment", this.d_research_dev),
            ("sales", this.d_sales),
            ("state", this.state),
            ("city", this.city),
            ("zipcode", this.zipcode),
            ("employeeSize", this.employee_size),
            ("revenue", this.revenue),
            ("alexaRank", this.alexa_rank),
            ("linkedinUrl", this.linkedin_url),
            ("industry", this.industry),
            ("country", this.country),
            ("educator", this.d_educator),
            ("techCategories", this.tech_categories),
        ])

    @property
    def serialized(self):
        recd = self.__class__._maps(self)
        recd['tech'] = \
            ujson.loads(recd['tech']) if recd['tech'] else []
        recd['similarCompanies'] =\
            ujson.loads(recd['similarCompanies']) if recd['similarCompanies'] else []
        recd['similarTechnologies'] =\
            ujson.loads(recd['similarTechnologies']) if recd['similarTechnologies'] else []
        recd['similarDepartments'] =\
            ujson.loads(recd['similarDepartments']) if recd['similarDepartments'] else []
        recd['techCategories'] = \
            ujson.loads(recd['techCategories']) if recd['techCategories'] else []

        score = recd["score"]
        recd['score'] = '%.2f' % score if score else "0.00"
        keys = [
            "administrative",
            "IT",
            "educator",
            "engineering",
            "finance",
            "hr",
            "legal",
            "marketing",
            "medicalHealth",
            "operations",
            "researchDevelopment",
            "sales"]
        for key in keys:
            recd[key] = Recommendation.DEPARTMENT_STRENGTH_MAP.get(recd[key])
        return recd

    @property
    def serialized_for_csv(self):
        recd = self.serialized
        recd["employeeSize"] = "'%s" % recd.get(
            "employeeSize") if recd.get("employeeSize") is not None else None
        recd["tech"] = decode_list(recd['tech'])
        recd["similarCompanies"] = decode_list(recd['similarCompanies'])
        recd["similarTechnologies"] = decode_list(recd['similarTechnologies'])
        recd["similarDepartments"] = decode_list(recd['similarDepartments'])
        recd["techCategories"] = decode_list(recd['techCategories'])
        return recd

    @classmethod
    def get_field_by_key(cls, key):
        return cls._maps(cls).get(key)

    @classmethod
    def build_filters(cls, clauses):
        """
        `clauses` is a dict that describes the filters of recommendations.

         Currently only supports
            - `$in` clause

         Documentation: https://parse.com/docs/rest/guide#queries-query-constraints

        Parameters
        ----------
        :type clauses: dict

        Returns
        -------

        """

        # A stub that doesn't filter anything out
        filters = True

        if not clauses:
            return filters

        for key, clause in clauses.iteritems():
            field = cls.get_field_by_key(key)
            if isinstance(clause, dict):
                # Relational comparison operators
                # Only supports `$in` for now
                if len(clause.keys()) > 1:
                    raise FilterBuildError('multiple operators(%s) for one key(%s)' % (clause, key))

                # Use for-loop to enable future multiple operators
                # but now there would be only one
                for operator, operand in clause.iteritems():
                    operand = filter(None, operand)
                    if len(operand) == 0:
                        raise FilterBuildError('filter values are required: %s' % key)

                    if operator == '$in':
                        filters = and_(filters, field.in_(operand))
                    else:
                        raise FilterBuildError('unknown operator: %s' % operator)
            else:
                # Extract matches
                filters = and_(filters, field == clause)

        return filters

    @classmethod
    def build_sector_filter(cls, sector_id):

        threshold = Recommendation.SIMILARITY_SCORE_THRESHOLD
        filters = True
        c_filter = and_(filters, Recommendation.similar_c_score < threshold)
        t_filter = and_(filters, Recommendation.similar_t_score < threshold)
        d_filter = and_(filters, Recommendation.similar_d_score < threshold)

        not_c_filter = and_(filters, Recommendation.similar_c_score == threshold)
        not_t_filter = and_(filters, Recommendation.similar_t_score == threshold)
        not_d_filter = and_(filters, Recommendation.similar_d_score == threshold)

        # 'C' for similar_companies, 'T' for similar_technologies, 'D' for similar_departments
        if sector_id == 1:
            # C & !T & !D
            filters = and_(c_filter, not_t_filter, not_d_filter)

        elif sector_id == 2:
            # T & !C & !D
            filters = and_(t_filter, not_c_filter, not_d_filter)

        elif sector_id == 3:
            # D & !C & !T
            filters = and_(d_filter, not_c_filter, not_t_filter)

        elif sector_id == 4:
            # C & T & !D
            filters = and_(c_filter, t_filter, not_d_filter)

        elif sector_id == 5:
            # C & D & !T
            filters = and_(c_filter, d_filter, not_t_filter)

        elif sector_id == 6:
            # T & D & !C
            filters = and_(t_filter, d_filter, not_c_filter)

        elif sector_id == 7:
            # C & T & D
            filters = and_(c_filter, t_filter, d_filter)

        return filters

    @classmethod
    def build_dedupe_filter(cls, segment):
        exported_company_ids = segment.exported_company_ids
        if exported_company_ids:
            filters = Recommendation.company_id.in_(exported_company_ids)
        else:
            filters = None
        return filters

    def __repr__(self):
        class_name = self.__class__.__name__

        return '<%s %s>' % (class_name, self.serialized)

    @staticmethod
    def generate_companies_csv(user, segment, recommendations):

        company_ids = [rec['companyId'] for rec in recommendations]
        headers = CompanyInsight.get_all_display_names()
        if not user.is_es_admin():
            AccountQuota.deduct_quota(segment, company_ids)
            configured_insights = user.account.account_quota.exposed_csv_insights
            # The code downloading the CSV needs this insight
            required_insights = ['companyId']
            headers = list(set(configured_insights) | set(required_insights))

        # generate company csv
        return generate_csv_file(headers, recommendations)

    @staticmethod
    def generate_tmp_companies_csv(user, recommendation_cursor, total_num, exceed_company_ids):
        headers = CompanyInsight.get_all_display_names()
        if not user.is_es_admin():
            configured_insights = user.account.account_quota.exposed_csv_insights
        # The code downloading the CSV needs this insight
            required_insights = ['companyId']
            headers = list(set(configured_insights) | set(required_insights))

        # generate company csv
        fd, file_path = mkstemp('_company_csv')
        os.close(fd)
        with open(file_path, 'w') as f:
            writer = unicodecsv.DictWriter(f, headers,
                                           encoding='utf-8', extrasaction='ignore')
            writer.writeheader()
            if total_num <= 10000:
                for recd in recommendation_cursor:
                    if user.is_es_admin() or recd.serialized_for_csv['companyId'] not in exceed_company_ids:
                        writer.writerow(recd.serialized_for_csv)

            else:
                offset = 0
                while offset < total_num:
                    limit = min(10000, total_num - offset)
                    for recd in recommendation_cursor.limit(limit).offset(offset):
                        if user.is_es_admin() or recd.serialized_for_csv['companyId'] not in exceed_company_ids:
                            writer.writerow(recd.serialized_for_csv)
                    offset += 10000

        return file_path

    @staticmethod
    def get_by_ids(recommendation_ids):
        return Recommendation.query.filter(Recommendation.id.in_(recommendation_ids)).all()

    @staticmethod
    def get_by_job_ids_and_domain(job_ids, domain):
        """
        query recommendations by job_ids and domain, sort the result by recommendation's score
        :param job_ids:
        :param domain:
        :return:
        """
        if not job_ids:
            return []
        assert domain, "Invalid domain"

        return Recommendation.query\
            .filter(Recommendation.job_id.in_(job_ids),
                    Recommendation.domain == domain)\
            .order_by(Recommendation.score.desc())\
            .all()

    @staticmethod
    def set_recommendations_exported(recommendation_ids):
        recommendations = Recommendation.get_by_ids(recommendation_ids)
        """
        for recommendation in recommendations:
            recommendation.is_exported = Recommendation.EXPORTED
        """
        db.session.add_all(recommendations)
        db.session.commit()
