# -*- coding: utf-8 -*-
import datetime
from ns.model import db


# mapping table for audience and exclusion lists
class AudienceExclusionList(db.Model):

    __tablename__ = 'audience_exclusion_list'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow, nullable=False)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow, nullable=False)
    audience_id = db.Column(db.Integer, db.ForeignKey("audience.id"), nullable=False)
    exclusion_list_id = db.Column(db.Integer, db.ForeignKey("audience.id"), nullable=False)

    @property
    def serialized(self):
        return {
            "id": self.id,
            "audienceId": self.audience_id,
            "exclusionListId": self.exclusion_list_id
        }

    @staticmethod
    def get_by_audience_id(audience_id):
        query = AudienceExclusionList.query.filter(AudienceExclusionList.audience_id == audience_id)
        return query.all()

    @staticmethod
    def get_by_audience_id_exclusion_list_id(audience_id, exclusion_list_id):
        query = AudienceExclusionList.query.filter(AudienceExclusionList.audience_id == audience_id,
                                                   AudienceExclusionList.exclusion_list_id == exclusion_list_id)
        return query.all()

    @staticmethod
    def get_by_exclusion_list_id(exclusion_list_id):
        query = AudienceExclusionList.query.filter(AudienceExclusionList.exclusion_list_id == exclusion_list_id)
        return query.all()

    @staticmethod
    def create_exclusion_list(audience_id, exclusion_list_id):
        exclusion_lists = AudienceExclusionList.get_by_audience_id_exclusion_list_id(audience_id, exclusion_list_id)
        if not exclusion_lists:
            exclusion_list = AudienceExclusionList(
                audience_id=audience_id,
                exclusion_list_id=exclusion_list_id
            )
            db.session.add(exclusion_list)
            db.session.commit()
            return exclusion_list
        else:
            return exclusion_lists[0]

    @staticmethod
    def update_exclusion_lists(audience_id, exclusion_list_ids):
        if len(exclusion_list_ids) >= 0:
            AudienceExclusionList.query.filter(AudienceExclusionList.audience_id == audience_id).delete()
            db.session.commit()
        for exclusion_list_id in exclusion_list_ids:
            exclusion_list = AudienceExclusionList(
                audience_id=audience_id,
                exclusion_list_id=exclusion_list_id
            )
            db.session.add(exclusion_list)
        db.session.commit()
