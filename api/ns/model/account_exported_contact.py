"""
Model for saving account exported contacts
"""
import datetime

from ns.model import db


class AccountExportedContact(db.Model):
    __tablename__ = 'account_exported_contact'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    contact_id = db.Column(db.Integer, index=True, nullable=False)
    __table_args__ = (db.UniqueConstraint('account_id', 'contact_id'),)

    @staticmethod
    def create_exports(account_id, contact_ids):
        if not contact_ids:
            return

        exports = []
        for contact_id in contact_ids:
            c = {
                "account_id": account_id,
                "contact_id": contact_id
            }
            exports.append(c)

        db.engine.execute(AccountExportedContact.__table__.insert(), exports)

        db.session.commit()

    @staticmethod
    def filter_by(filter):
        """
        :param filter
        :returns filtered array of similar domain objs
        """
        return AccountExportedContact.query \
                                     .filter(filter) \
                                     .all()

    @staticmethod
    def delete_by_filter(filter):
        """
        :param filter
        :returns filtered array of similar domain objs
        """
        return AccountExportedContact.query \
                                     .filter(filter) \
                                     .delete()
