"""
Qualification model
"""
from ns.model import db
import datetime


class Qualification(db.Model):
    __tablename__ = 'qualification'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    qualification_val = db.Column(db.String(8192), nullable=True)

    @staticmethod
    def create(segment_id, qualification_val):
        qualification = Qualification(segment_id=segment_id,
                                      qualification_val=qualification_val)
        db.session.add(qualification)
        db.session.commit()
        return qualification

    @staticmethod
    def get_by_id(qualification_id):
        return Qualification.query.get(qualification_id)

    def update_qualification_val(self, qualification_val):
        self.qualification_val = qualification_val
        db.session.commit()
