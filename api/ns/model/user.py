"""
User Model
"""
import calendar
from werkzeug import exceptions
import inflection
from ns.model.crm_token import CRMToken
from ns.model.user_segment import UserSegment
from ns.model import db, AnnotatedBaseQuery
from ns.util.password_util import generate_salt_str, encrypt_password_with_salt, check_password_format
from ns.util.string_util import is_not_blank
from ns.model.role import Role
from ns.model.user_role import UserRole
import datetime

from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship
from sqlalchemy import and_


class User(db.Model):
    __tablename__ = 'user'

    query_class = AnnotatedBaseQuery
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"
    DELETED = "DELETED"

    status_enums = [DELETED, INACTIVE, ACTIVE]

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    first_name = db.Column(db.String(255), nullable=True)
    last_name = db.Column(db.String(255), nullable=True)
    phone = db.Column(db.String(255), nullable=True)
    salt = db.Column(db.String(255), nullable=False, default="")
    status = db.Column(db.Enum(*status_enums), nullable=False)
    contact_export_flag = db.Column(db.Boolean, nullable=False)
    company = db.Column(db.String(255), nullable=True)
    segments = db.relationship('Segment', backref='owner', lazy='dynamic')
    audiences = db.relationship('Audience', backref='user', lazy='dynamic')

    roles = association_proxy('user_roles', 'role')

    crm_tokens = relationship('CRMToken', backref='user', lazy='dynamic')
    user_account_quota_ledger = relationship('AccountQuotaLedger', backref='user', uselist=False)

    @property
    def same_account_users(self):
        return User.query\
            .filter(User.account_id == self.account_id,
                    User.id != self.id)\
            .all()

    def owned_or_shared_audiences(self, meta_type):
        all_audiences = self.account.all_owned_audiences(meta_type=meta_type)

        return filter(
            lambda x: (x.user_id == self.id) or (not x.is_private),
            all_audiences
        )

    @property
    def full_name(self):
        if self.first_name:
            if self.last_name:
                return self.first_name + ' ' + self.last_name
            else:
                return self.first_name
        else:
            return ''

    @property
    def activated(self):
        return self.status == User.ACTIVE

    @activated.setter
    def activated(self, value):
        self.status = User.ACTIVE if value else User.INACTIVE

    @property
    def is_deleted(self):
        return self.status == User.DELETED

    @classmethod
    def activated_users(cls):
        return cls.query.filter_by(activated=True)

    @staticmethod
    def get_by_id(user_id):
        return User.query.get(user_id)

    @staticmethod
    def get_user_by_email(email):
        return User.query.filter_by(email=email).first()

    @staticmethod
    def get_active_user_by_email(email):
        return User.query.filter(User.email == email, User.status == User.ACTIVE).first()

    @staticmethod
    def get_users_for_account(account_id):
        return User.query.filter_by(account_id=account_id).all()

    @staticmethod
    def has_segment_access(user_id, segment):
        # If I created this segment, I can view it
        if segment.owner_id == user_id:
            return True

        # If I'm an es admin, I can view this segment.
        user = User.query.get(user_id)
        if user.is_es_admin():
            return True
        # if i'm an account admin, i can view all segments of my account
        if user.is_account_admin() \
                and segment.belongs_to(user.account_id):
            return True

        # If this segment has been shared with me, I can view it.
        user_segment = UserSegment.query.filter_by(user_id=user_id, segment_id=segment.id).first()
        return user_segment is not None

    @staticmethod
    def has_account_access(user_id, account_id):
        user = User.get_by_id(user_id)
        if not (user.is_es_admin() or user.account_id == account_id):
            return False
        return True

    @staticmethod
    def has_eap_access(user):
        return not (user.role_names and len(user.role_names) == 1 and Role.CHROME_USER == user.role_names[0])

    @staticmethod
    def has_audience_access_permission(user_id, audience_id):
        user = User.get_by_id(user_id)
        if user.is_es_admin():
            return True

        from ns.model.audience import Audience
        audience = Audience.get_by_id(audience_id)

        if audience.user.account_id == user.account_id:
            if user.is_account_admin():
                return True
            elif (not audience.is_private) or user.id == audience.user_id:
                return True

        return False

    @staticmethod
    def has_audience_edit_permission(user_id, audience_id):
        user = User.get_by_id(user_id)
        if user.is_es_admin():
            return True

        from ns.model.audience import Audience
        audience = Audience.get_by_id(audience_id)

        if audience.user.account_id == user.account_id:
            if user.is_account_admin():
                return True
            elif user.id == audience.user_id:
                return True

        return False

    @staticmethod
    def get_account_admin_by_account_id(account_id):
        users = User.query.join(UserRole, User.id == UserRole.user_id)\
            .filter(and_(User.account_id == account_id, User.status == User.ACTIVE, UserRole.role_id == 3)).all()
        return users

    @staticmethod
    def create_user(account_id, email, password,
                    status=INACTIVE, first_name="", last_name="", phone="",
                    contact_export_flag=False, company=""):
        user = User.get_user_by_email(email)

        if user is None:
            salt = generate_salt_str()
            check_password_format(password=password)
            user = User(
                account_id=account_id,
                email=email,
                password=encrypt_password_with_salt(salt, password),
                first_name=first_name,
                last_name=last_name,
                company=company,
                phone=phone,
                status=status,
                salt=salt,
                contact_export_flag=contact_export_flag
            )
            db.session.add(user)
            db.session.commit()
        return user

    def is_super_admin(self):
        return Role.is_super_admin(self.role_names)

    def is_es_admin(self):
        return Role.is_es_admin(self.role_names)

    def is_account_admin(self):
        return Role.is_account_admin(self.role_names)

    def has_limit_on_ui_company_cnt(self):
        return Role.is_trial_user(self.role_names)

    def update_name_and_phone(self, first_name, last_name, phone):
        """
        update first name, last name and phone for a user.
        all information is required
        :param first_name:
        :param last_name:
        :param phone:
        :return:
        """
        assert is_not_blank(first_name), "Invalid first name"
        assert is_not_blank(last_name), "Invalid last name"
        assert is_not_blank(phone), "Invalid phone"

        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        db.session.commit()

    def update_account_id(self, account_id):
        self.account_id = account_id
        db.session.commit()

    def check_password(self, password):
        """
        check password
        :param password:
        :return:
        """
        pw = encrypt_password_with_salt(self.salt, password)
        return self.password == pw

    def change_password(self, password, new_password):
        """
        change password
        :param password:  original password
        :param new_password:   new password
        :return:
        """
        if not self.check_password(password):
            return False
        self.reset_password(new_password)

    def reset_password(self, password):
        check_password_format(password=password)
        self.password = encrypt_password_with_salt(self.salt, password)
        db.session.commit()

    def activate(self):
        """
        set user to active
        :return:
        """
        self.activated = True
        db.session.commit()

    def update(self, new_attrs):
        """

        Parameters
        ----------
        @type new_attrs: dict
        """
        email = new_attrs.get("email")
        if email:
            user = User.get_user_by_email(email)
            if user and self.id != user.id:
                raise exceptions.BadRequest("Email already exists.")

        permitted_fields = ['email', 'first_name', 'last_name', 'phone', 'status', 'account_id',
                            'contact_export_flag']

        for field in permitted_fields:
            old_value = getattr(self, field)
            new_value = new_attrs.get(inflection.camelize(field, False), old_value)
            setattr(self, field, new_value)

            user_is_changing_accounts = field == 'account_id' and old_value != new_value
            if user_is_changing_accounts:
                from ns.model.account import Account
                account_id = new_value
                destination_acct = Account.get_by_id(account_id)
                destination_acct.add_user(self)

        db.session.add(self)
        db.session.commit()

    @property
    def sfdc_token(self):
        return CRMToken.get_sfdc_token(self.id)

    @property
    def role_names(self):
        return [role.name for role in self.roles]

    @property
    def name(self):
        name = filter(None, [self.first_name, self.last_name])

        return ' '.join(name)

    @property
    def serialized(self):
        """
        Use eager loading for quick access account attributes

        Returns
        -------

        """
        return {
            'accountId': self.account.id,
            'accountName': self.account.name,

            'id': self.id,
            'name': self.name,
            'email': self.email,
            'phone': self.phone,
            'activated': self.activated,
            'firstName': self.first_name,
            'lastName': self.last_name,

            'roles': self.role_names,
            'status': self.status,
            'enableInCrm': self.account.account_quota.enable_in_crm if self.account.account_quota else False,
            'enableNotInCrm': self.account.account_quota.enable_not_in_crm if self.account.account_quota else False,
            # UTC timestamp of creation date
            'createdTs': calendar.timegm(self.created_ts.utctimetuple()),
            'contactExportFlag': self.contact_export_flag,
            'isPrimary': self.account.is_primary,
            'domain': self.account.domain
        }

    @property
    def serialized_with_quota(self):
        serialized_user = self.serialized
        serialized_user['quota'] = self.account.account_quota.serialized
        return serialized_user

    def can_grant_role(self, role_names):
        self_roles = Role.get_by_names(self.role_names)
        self_most_prior = Role.get_most_prior(self_roles)
        if not self_most_prior:
            return False

        target_roles = Role.get_by_names(role_names)
        return self_most_prior.prior_or_same_as(
            Role.get_most_prior(target_roles)
        )

    def __repr__(self):
        class_name = self.__class__.__name__

        return '<%s %s>' % (class_name, self.serialized)

    def can_export_contacts(self):
        return self.is_es_admin() or self.contact_export_flag
