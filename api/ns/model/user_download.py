import datetime

from ns.model import db


class UserDownload(db.Model):
    __tablename__ = 'user_download'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    segment_id = db.Column(db.Integer, nullable=False)
    audience_id = db.Column(db.Integer, nullable=True)
    company_csv_s3_key = db.Column(db.String(255), nullable=True)
    contact_csv_s3_key = db.Column(db.String(255), nullable=True)

    # track segment csv file
    @staticmethod
    def create(user, segment, company_csv_s3_key, contact_csv_s3_key=None):
        user_download = UserDownload.query.filter_by(user_id=user.id,
                                                     segment_id=segment.id,
                                                     company_csv_s3_key=company_csv_s3_key,
                                                     contact_csv_s3_key=contact_csv_s3_key).first()
        if user_download is None:
            user_download = UserDownload(user_id=user.id,
                                         segment_id=segment.id,
                                         company_csv_s3_key=company_csv_s3_key,
                                         contact_csv_s3_key=contact_csv_s3_key)
            db.session.add(user_download)
            db.session.commit()

        return user_download

    # track audience csv file
    @staticmethod
    def create_for_audience(user_id, audience_id, company_csv_s3_key=None,
                            contact_csv_s3_key=None, segment_id=0):
        user_download = UserDownload.query.filter_by(user_id=user_id,
                                                     audience_id=audience_id,
                                                     company_csv_s3_key=company_csv_s3_key,
                                                     contact_csv_s3_key=contact_csv_s3_key,
                                                     segment_id=segment_id).first()
        if user_download is None:
            user_download = UserDownload(user_id=user_id,
                                         audience_id=audience_id,
                                         company_csv_s3_key=company_csv_s3_key,
                                         contact_csv_s3_key=contact_csv_s3_key,
                                         segment_id=segment_id)
            db.session.add(user_download)
            db.session.commit()
