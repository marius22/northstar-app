import datetime
from ns.model import db
from ns.util.queue_service_client import QueueServiceClient


class Model(db.Model):

    __tablename__ = 'model'

    # model types
    ACCOUNT_FIT = "account_fit"
    GALAXY = "galaxy"
    FIT_MODEL = "fit"
    FEATURE_ANALYSIS = "feature_analysis"

    model_types = set([ACCOUNT_FIT, GALAXY, FIT_MODEL, FEATURE_ANALYSIS])

    # model status
    INIT = "INIT"
    IN_QUEUE = "IN_QUEUE"
    IN_PROGRESS = "IN_PROGRESS"
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    type = db.Column(db.String(20), nullable=False)
    status = db.Column(db.String(50), nullable=False)
    model_seed_id = db.Column(db.Integer, nullable=True)
    batch_id = db.Column(db.String(50), nullable=False)
    owner_id = db.Column(db.Integer, nullable=False)
    is_deleted = db.Column(db.Boolean, default=False)

    # old column, just for data migration
    seed_id = db.Column(db.Integer, nullable=False)

    @staticmethod
    def get_all_models():
        return Model.query.all()

    @staticmethod
    def get_by_id(model_id):
        assert model_id, "Invalid modelId"
        return Model.query.filter(Model.id == model_id, Model.is_deleted.is_(False)).first()

    @staticmethod
    def get_by_ids(model_ids, keep_deleted_models=False):
        assert model_ids, "Invalid list of modelIds"
        query = Model.query.filter(Model.id.in_(model_ids))
        if not keep_deleted_models:
            return query.filter(Model.is_deleted.is_(False)).all()
        else:
            return query.all()

    @staticmethod
    def get_by_account_id(account_id, model_types=None, statuses=list()):
        assert account_id, "Invalid account_id"
        query = Model.query.filter(Model.account_id == account_id, Model.is_deleted.is_(False))
        if model_types:
            query = query.filter(Model.type.in_(model_types))
        if statuses:
            query = query.filter(Model.status.in_(statuses))

        return query.order_by(Model.created_ts.desc()).all()

    @property
    def is_completed(self):
        return self.status == Model.COMPLETED

    @property
    def serialized(self):
        serialized_model = {
            "id": self.id,
            "createdTs": self.created_ts.isoformat(),
            "updatedTs": self.updated_ts.isoformat(),
            "accountId": self.account_id,
            "name": self.name,
            "status": Model.IN_PROGRESS if self.status in [Model.IN_QUEUE, Model.FAILED] else self.status,
            "modelSeedId": self.model_seed_id,
            "type": self.type,
            "batchId": self.batch_id,
            "ownerId": self.owner_id if self.owner_id else None
        }
        is_complete_fit = self.is_completed and self.type in [Model.ACCOUNT_FIT, Model.FIT_MODEL]
        include_feat_analysis_m_id = self.batch_id and is_complete_fit
        if include_feat_analysis_m_id:
            fa_model_list = Model.get_same_batch_models(self.batch_id, [Model.FEATURE_ANALYSIS])
            if fa_model_list:
                # should only have one model in this list
                serialized_model["faModelId"] = fa_model_list[0].id
        return serialized_model

    @staticmethod
    def get_by_account_id_and_type(account_id, type):
        return Model.query\
            .filter(Model.account_id == account_id,
                    Model.type == type,
                    Model.is_deleted.is_(False))\
            .all()

    @staticmethod
    def get_latest_model(account_id, type, status=COMPLETED):
        query = Model.query\
            .filter(Model.account_id == account_id,
                    Model.type == type,
                    Model.is_deleted.is_(False))
        if status:
            query = query.filter(Model.status == status)
        return query.order_by(Model.id.desc()).first()

    @staticmethod
    def get_latest_completed_model(account_id, type):
        subquery = Model.query.filter(Model.account_id == account_id,
                                      Model.status == Model.COMPLETED,
                                      Model.type == type,
                                      Model.is_deleted.is_(False)).\
            with_entities(Model.batch_id).subquery()
        query = Model.query.filter(Model.batch_id.in_(subquery), Model.is_deleted.is_(False),
                                   Model.status == Model.COMPLETED,
                                   Model.type == Model.FEATURE_ANALYSIS).order_by(Model.id.desc())
        model = query.first()
        if not model:
            return None
        query = Model.query.filter(Model.batch_id == model.batch_id, Model.type == type)
        return query.order_by(Model.id.desc()).first()

    @staticmethod
    def create_model(name, account_id, type, batch_id, status=INIT, model_seed_id=None, owner_id=0):
        model = Model(
            name=name,
            account_id=account_id,
            type=type,
            batch_id=batch_id,
            status=status,
            model_seed_id=model_seed_id,
            owner_id=owner_id,
            seed_id=0
        )
        db.session.add(model)
        db.session.commit()

        return model

    @staticmethod
    def delete_by_id(model_id, commit=True):
        model = Model.query.get(model_id)
        model.is_deleted = True
        if commit:
            db.session.commit()

    def submit_model_run(self):
        assert self.model_seed_id, "Invalid modelSeedId"
        QueueServiceClient.instance().submit_account_model(self.account_id, self.id)
        self.update_status(Model.IN_QUEUE)

    def update_status(self, status):
        assert status in [Model.IN_PROGRESS, Model.COMPLETED, Model.IN_QUEUE, Model.FAILED], "Invalid model status"

        self.status = status
        db.session.commit()

    @staticmethod
    def get_same_batch_models(batch_id, types=None):
        assert batch_id, "Invalid batch_id"
        query = Model.query.filter(Model.batch_id == batch_id, Model.is_deleted.is_(False))
        if types:
            query = query.filter(Model.type.in_(types))
        return query.all()

    @staticmethod
    def all_models_in_batch_complete(batch_id):
        same_batch_models = Model.get_same_batch_models(batch_id)
        for model in same_batch_models:
            if not model.is_completed:
                return False
        return True

    @staticmethod
    def get_account_crm_fit_model_by_statuses(account_id, statuses=list()):
        query = Model.query.filter(
            Model.account_id == account_id, Model.is_deleted.is_(False), Model.type == Model.ACCOUNT_FIT)
        if statuses:
            query = query.filter(Model.status.in_(statuses))
        models = query.all()
        return models

    @staticmethod
    def get_account_crm_fit_models(account_id, status):
        models = Model.query.filter(
            Model.account_id == account_id, Model.is_deleted.is_(False),
            Model.type == Model.ACCOUNT_FIT, Model.status == status).all()
        return models

    @staticmethod
    def get_models_by_batch_ids(batch_ids):
        models = Model.query.filter(
            Model.batch_id.in_(batch_ids), Model.is_deleted.is_(False),
            Model.type == Model.FEATURE_ANALYSIS, Model.status == Model.COMPLETED).all()
        return models


class NotEnoughDomainsError(Exception):
    pass
