from flask import current_app
from ns.util.producer import DummyProducer, Producer, ExportSFDCMessage, ExportWithDedupeMessage
from simple_salesforce import Salesforce, \
    SalesforceGeneralError, SalesforceExpiredSession, SalesforceResourceNotFound


class SFDCIntegrationError(Exception):
    """
    Generic error class for all issues related with SFDC integration
    """
    error_code = 'SFDC_INTEGRATE_ERROR'
    message = 'SFDC integration error'


class SFDCManagedPackageError(SFDCIntegrationError):
    error_code = 'PACKAGE_ERROR'
    message = 'SFDC managed package error'


class SFDCManagedPackageNotInstalledError(SFDCManagedPackageError):
    error_code = 'PACKAGE_NOT_INSTALLED'
    message = 'Package has not been installed'


class SFDCNoPermissionInstallPackage(SFDCManagedPackageNotInstalledError):
    error_code = 'NO_PERMISSION_INSTALL_PACKAGE'
    message = 'No permission to install package'


class SFDCHavePermissionInstallPackage(SFDCManagedPackageNotInstalledError):
    error_code = 'HAVE_PERMISSION_INSTALL_PACKAGE'
    message = 'Have permission to install package'


class SFDCManagedPackageSegementNotCreateableError(SFDCManagedPackageError):
    error_code = 'SEGMENT_NOT_CREATEABLE'
    message = 'Segment custom object is not createable'


class SFDCTokenInvalidError(SFDCIntegrationError):
    error_code = 'SESSION_ID_INVALID'
    message = 'SFDC session_id is invalid'


class UserSegmentSFDCPublisher(object):
    def __init__(self, user):
        self.user = user

        # User must have a SFDC token, regardless of the expire status
        self.token = self.user.sfdc_token
        if not self.token:
            raise TypeError(
                'User must have a SFDC token'
            )

        self._sf = Salesforce(instance_url=self.token.instance_url,
                              session_id=self.token.access_token)

        self.metadata = None

        # Status of the client: None, OK, NOT_OK
        self.status = None

        self.segment_handler = self._sf.__getattr__(
            current_app.config.get('SFDC_SEGMENT_NAME',
                                   'EverString_Predictive_Segment__c')
        )
        self.account_handler = self._sf.__getattr__(
            current_app.config.get('SFDC_ACCOUNT_NAME',
                                   'Account')
        )

        self.contact_handler = self._sf.__getattr__(
            current_app.config.get('SFDC_CONTACT_NAME',
                                   'Contact')
        )

    def fetch_metadata(self):
        # todo: make this private
        try:
            self.metadata = self._sf.describe()
            self.status = 'OK'
        except Exception:
            self.status = 'NOT_OK'
            raise

    def check_prerequisite(self):
        current_app.logger.debug('Checking prerequisite')
        try:
            self.fetch_metadata()

            # Ping all types of the sobject we need
            self.segment_handler.describe()
            self.account_handler.describe()
            self.contact_handler.describe()
        except SalesforceExpiredSession:
            raise SFDCTokenInvalidError()
        except SalesforceResourceNotFound:
            # only regards the resource not found as
            # the indicator of having installed the package
            raise SFDCManagedPackageNotInstalledError()
        except SalesforceGeneralError:
            raise SFDCIntegrationError()

    def publish_segment(self, segment, **kwargs):
        raise NotImplementedError()

    def unpublish_segment(self):
        raise NotImplementedError()


class BackgroundPublisherWithDedupe(UserSegmentSFDCPublisher):
    def __init__(self, user):
        super(BackgroundPublisherWithDedupe, self).__init__(user)

        use_kafka = current_app.config.get('KAFKA_ENABLED')
        if use_kafka:
            brokers = current_app.config.get('KAFKA_BROKERS')
            self.job_producer = Producer.get_instance(brokers)
        else:
            self.job_producer = DummyProducer()

    def unpublish_segment(self):
        pass

    def publish_segment(self, message):
        self.check_prerequisite()
        self.job_producer.send_message(message)


class BackgroundPublisher(UserSegmentSFDCPublisher):
    def __init__(self, user):
        super(BackgroundPublisher, self).__init__(user)

        use_kafka = current_app.config.get('KAFKA_ENABLED')
        if use_kafka:
            brokers = current_app.config.get('KAFKA_BROKERS')
            self.job_producer = Producer.get_instance(brokers)
        else:
            self.job_producer = DummyProducer()

    def unpublish_segment(self):
        pass

    def publish_segment(self, segment, **kwargs):
        self.check_prerequisite()

        if 'csv_url' not in kwargs:
            raise ValueError('missing `csv_url`')
        else:
            csv_url = kwargs['csv_url']

        message = ExportSFDCMessage(
            self.user.account_id,
            self.user.id, segment.id, segment.name,
            self.user.sfdc_token, csv_url)

        self.job_producer.send_message(message)
