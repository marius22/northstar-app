import datetime
from ns.model import db
from ns.util.string_util import generate_hash_str
from ns.util.company_db_client import CompanyDBClient
from ns.util.domain_util import parse_domain


class SeedDomain(db.Model):

    __tablename__ = 'seed_domain'
    UNKNOWN_DOMAIN_PREFIX = "unknown_"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    domain = db.Column(db.String(100), nullable=False)
    company_name = db.Column(db.String(255), nullable=True)
    sf_account_id = db.Column(db.String(255), nullable=True)
    sf_account_name = db.Column(db.String(255), nullable=True)

    @staticmethod
    def get_by_ids(ids):
        if not ids:
            return []
        return SeedDomain.query.filter(SeedDomain.id.in_(ids)).all()

    @staticmethod
    def get_by_domains(domains):
        if not domains:
            return []
        return SeedDomain.query.filter(SeedDomain.domain.in_(domains)).all()

    @staticmethod
    def _add_seed_domains(seed_domains):
        if seed_domains:
            data = [{
                "domain": item.domain,
                "company_name": item.company_name,
                "sf_account_id": item.sf_account_id,
                "sf_account_name": item.sf_account_name
            } for item in seed_domains]

            db.engine.execute(
                SeedDomain.__table__.insert(),
                data
            )
            db.session.commit()
        return

    @staticmethod
    def save_seed_domains(seed_domains):
        if not seed_domains:
            return []

        uniq_domain_mapping = {}
        # domains in seed_domains table must be unique
        for item in seed_domains:
            if not item.domain:
                item.domain = SeedDomain.UNKNOWN_DOMAIN_PREFIX + generate_hash_str("")
            else:
                item.domain = parse_domain(item.domain.lower())
            uniq_domain_mapping[item.domain] = item

        domains = uniq_domain_mapping.keys()

        existed_domains = set([seed.domain for seed in SeedDomain.get_by_domains(domains)])
        to_save_domains = set(domains) - existed_domains

        SeedDomain._add_seed_domains([uniq_domain_mapping.get(domain) for domain in to_save_domains])

        return [seed.id for seed in SeedDomain.get_by_domains(domains)]

    @staticmethod
    def enrich_seed_domains(seed_domains):
        if seed_domains:
            domains = [item.domain for item in seed_domains if item.domain]
            db_client = CompanyDBClient.instance()
            profiles = db_client.get_enrichment_by_domains(domains)
            for seed in seed_domains:
                profile = profiles.get(seed)
                if profile:
                    seed.company_name = profile.get('name')

        return seed_domains

    @staticmethod
    def query_seed_domains(**kwargs):
        ids = kwargs.get("ids", [])
        limit = kwargs.get("limit", 100)
        offset = kwargs.get("offset", 0)
        criterion = kwargs.get("criterion")
        type = kwargs.get("type")

        query = SeedDomain.query.filter(SeedDomain.id.in_(ids))

        if type == 'unknown':
            query = query.filter(SeedDomain.domain.like(SeedDomain.UNKNOWN_DOMAIN_PREFIX + "%"))
        else:
            query = query.filter(SeedDomain.domain.notlike(SeedDomain.UNKNOWN_DOMAIN_PREFIX + "%"))

        page = query.order_by(criterion).paginate(offset / limit + 1, limit, False)
        return page.total, page.items

    @staticmethod
    def _maps(this):
        return {
            'id': this.id,
            'companyName': this.company_name,
            'domain': this.domain,
            "SFDCAccountId": this.sf_account_id,
            "SFDCAccountName": this.sf_account_name
        }

    @property
    def serialized(self):
        return self.__class__._maps(self)

    @classmethod
    def get_field_by_key(cls, key):
        return cls._maps(cls).get(key)
