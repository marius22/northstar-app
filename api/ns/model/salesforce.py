from datetime import datetime
from flask import current_app, jsonify
import json
import logging
import simple_salesforce
import tldextract

from ns.model.account_exported_company import AccountExportedCompany
from ns.model.account_quota import AccountQuota, QuotaExceedError
from ns.model.company_insight import CompanyInsight
from ns.model.crm_token import CRMToken, SFDCToken
from ns.model.model import Model
from ns.model.sfdc_config import SFDCConfig
from ns.util.bucket_util import bucketize
from ns.util.company_db_client import CompanyDBClient
from ns.util.http_client import HTTPClient
from ns.util.string_util import is_blank
from ns.util.domain_util import get_domain_from_email
from sqlalchemy.exc import IntegrityError

from ns.constants.default_field_mapping_constants import TARGET_TYPE_SALESFORCE, \
    TARGET_OBJECT_ACCOUNT, TARGET_OBJECT_LEAD, SFDC_PACKAGE_NAMESPACE_PREFIX, SFDC_CUSTOM_COMPONENT_SUFFIX
from ns.constants.insight_constants import InsightConstants

import traceback
import requests
from werkzeug import exceptions


logger = logging.getLogger(__name__)

# Constants referring to different SalesForce objects.
ACCOUNT_CONTACT = "account_contact"
ACCOUNT = "account"
CONTACT = "contact"
LEAD = "lead"


def fetch_accounts_matching_domain(crm_token, domain):
    """Get the Salesforce accounts that match the given domain."""
    sfdc_token = SFDCToken.load_from_crm_token(crm_token)
    access_token = sfdc_token.access_token
    instance_url = sfdc_token.instance_url
    sf = simple_salesforce.Salesforce(session_id=access_token, instance_url=instance_url)

    # The `Website` field of a Salesforce Account may have components other than the domain,
    # So if we want to find Accounts matching `everstring.com`, we want to match `http://www.everstring.com`,
    # and `www.everstring.com` for example.
    final_query = "SELECT Id, Website FROM Account WHERE Website LIKE '%{}%'".format(domain)
    accounts_dict = sf.query(final_query)

    # Accounts that match `domain`
    matching_accounts = []

    for sfdc_account in accounts_dict.get("records", []):
        extract_result = tldextract.extract(sfdc_account.get('Website', ''))
        sfdc_account_domain = '.'.join([extract_result.domain, extract_result.suffix])
        if domain == sfdc_account_domain:
            matching_accounts.append(sfdc_account)

    return matching_accounts


def upsert_accounts_by_domain(crm_token, domain, obj_type, sf_obj, account, company_id):
    """Upsert SFDC Accounts of the given domain."""
    sfdc_token = SFDCToken.load_from_crm_token(crm_token)

    access_token = sfdc_token.access_token
    instance_url = sfdc_token.instance_url
    sf = simple_salesforce.Salesforce(session_id=access_token, instance_url=instance_url)

    # If there are existing SFDC Accounts, we update them.
    matching_accounts = fetch_accounts_matching_domain(crm_token, domain)
    if len(matching_accounts) > 0:
        for matching_account in matching_accounts:
            sf.Account.update(matching_account['Id'], sf_obj)

        return {
            'operation_type': 'update',
            'num_accounts': len(matching_accounts)
        }

    # Otherwise, we create a new Account.
    else:
        sfdc_client = NSSalesforce(sfdc_token)
        sfdc_client.add_sfdc_object(obj_type, sf_obj)
        return {
            'operation_type': 'insert',
            'num_accounts': 1
        }

    _track_company_export(account, crm_token.user_id, [company_id])


def get_config(key):
    return current_app.config.get(key)


def request_access_token(auth_code, crm_type):
    if crm_type == CRMToken.SALESFORCE:
        token_url = get_config("TOKEN_URL")
    elif crm_type == CRMToken.SALESFORCE_SANDBOX:
        token_url = get_config("TOKEN_URL_SANDBOX")

    request_url = '%s?grant_type=authorization_code' \
                  '&code=%s&client_id=%s' \
                  '&client_secret=%s&redirect_uri=%s' \
                  % (token_url, auth_code, get_config("CLIENT_ID"),
                     get_config("CLIENT_SECRET"), get_config("CALL_BACK_URL"))

    json_result = HTTPClient.post_json(request_url)

    return dict([(k, json_result.get(k)) for k in
                [SFDCToken.ACCESS_TOKEN_KEY, SFDCToken.REFRESH_TOKEN_KEY,
                 SFDCToken.INSTANCE_URL_KEY, SFDCToken.SALESFORCE_ID_KEY]])


def get_user_info(salesforce_id, access_token):

    headers = {"Authorization": "OAuth " + access_token, "Accept": "application/json"}

    json_result = HTTPClient.get_json(salesforce_id, None, headers=headers)

    logger.info(json_result)

    return dict([(k, json_result.get(k)) for k in [
        SFDCToken.ORGANIZATION_ID_KEY, SFDCToken.EMAIL_KEY, SFDCToken.USER_ID_KEY]])


def request_refresh_token(refresh_token, crm_type):
    if crm_type == CRMToken.SALESFORCE:
        token_url = get_config("TOKEN_URL")
    elif crm_type == CRMToken.SALESFORCE_SANDBOX:
        token_url = get_config("TOKEN_URL_SANDBOX")

    request_url = '%s?grant_type=refresh_token' \
                  '&client_id=%s&client_secret=%s&refresh_token=%s' \
                  % (token_url, get_config("CLIENT_ID"),
                     get_config("CLIENT_SECRET"), refresh_token)

    json_result = HTTPClient.post_json(request_url)

    return dict([(k, json_result.get(k)) for k in
                 [SFDCToken.ACCESS_TOKEN_KEY, SFDCToken.INSTANCE_URL_KEY,
                  SFDCToken.SALESFORCE_ID_KEY]])


def refresh_and_update_token(user):

    sfdc_token = CRMToken.get_sfdc_token(user.id)

    refreshed_token = request_refresh_token(sfdc_token.refresh_token, sfdc_token.crm_type)

    return sfdc_token.update_from_dict(refreshed_token)


def publish_domain_to_sfdc(account_id, domain, obj_type=TARGET_OBJECT_ACCOUNT):
    """
    Add an Account or Lead object to SFDC.

    :param account_id: ID of the EAP account under which a user is trying to publish to SFDC.

    :param domain: Domain of the company that a user is publishing. We use this domain to query
                   our data repo for company information.

    :param obj_type: Should be 'account' or 'lead'.

    :return: Returns the response from SFDC after we POST the new Account/Lead object to it.
    """
    from ns.model.account import Account
    from ns.model.publish_field_mapping import PublishFieldMapping

    config = SFDCConfig.get_by_account_id(account_id)
    if not config.can_add_to_crm:
        raise exceptions.BadRequest("Add to CRM is disabled.")

    remaining_quota = AccountQuota.get_by_account_id(account_id). \
        remaining_csv_company_cnt

    if remaining_quota <= 0:
        raise QuotaExceedError

    try:
        crm_token = SFDCToken.get_sfdc_crm_token(account_id)
    except:
        raise exceptions.BadRequest("Account is not integrated.")

    account = Account.get_by_id(account_id)
    # Get the ids of all not-deleted audiences of this account.
    audience_ids = [audience.serialized["id"] for audience in account.all_owned_audiences()]
    # Get all completed models of types that have fit scores.
    models = Model.get_by_account_id(
        account.id,
        [Model.FIT_MODEL, Model.ACCOUNT_FIT],
        statuses=[Model.COMPLETED]
    )
    model_ids = [model.serialized["id"] for model in models]

    # get salesforce field mapping for Account object
    field_mapping = PublishFieldMapping.get_publish_field_mapping(
        account_id,
        TARGET_TYPE_SALESFORCE,
        PublishFieldMapping.get_target_org_id(account_id, TARGET_TYPE_SALESFORCE),
        obj_type
    )

    field_mapping_insight_ids = [mapping.get("insightId") for mapping in field_mapping if mapping.get("insightId")]
    # The method below is called get_display_name_by_ids(), but display_name is a misnomer;
    # We're using the display_name strings as internal key constants to identify insights,
    # rather than for display.
    field_mapping_insights = CompanyInsight.get_display_name_by_ids(field_mapping_insight_ids)

    # enrich
    company_publish_data = CompanyDBClient.instance().get_publish_data_for_domain(
        domain,
        field_mapping_insights,
        audience_ids,
        model_ids
    )

    # iterate through field mappings in this helper method to create the json
    # object that we send to salesforce
    sf_obj = _transform_to_sfdc_obj(company_publish_data, field_mapping, obj_type)

    company_id = company_publish_data.get_insight_value('id')
    result_dict = upsert_accounts_by_domain(
        crm_token, domain, obj_type, sf_obj, account, company_id)

    return result_dict


def _track_company_export(account, user_id, company_ids):
    from ns.controller.app.param_validator import check_user_id
    user = check_user_id(user_id)
    if user.is_es_admin() or user.is_super_admin():
        return
    try:
        new_company_ids_set, exist_company_ids_set = account.get_company_ids_set(account, company_ids)
        AccountExportedCompany.create_exports(account.id, list(new_company_ids_set))
    except IntegrityError:
        logger.error(traceback.format_exc())
    return


def _transform_to_sfdc_obj(company_publish_data, field_mapping, obj_type):
    """
    Iterate through field mappings and create an Account or Lead object dict from company data.
    We also automatically include some default fields (Website, Created Timestamp, etc.) regardless of field mappings.

    :param company_publish_data: Instance of CompanyPublishData, defined in publish_field_mapping.py.
                                 This basically behaves like a dictionary that maps

    :param field_mapping: A list of all field mappings of an EAP account. We iterate through this, and
                          for each field mapping, we get insight values out of company_publish_data
                          and build up our Account object dict.

    :param obj_type: Type of salesforce object. This method currently supports 'account' or 'lead' types.

    :return: Returns a dictionary representing an object of obj_type that we send as a param to SFDC
             inside of NSSalesforce.add_sfdc_object()
    """
    sf_obj = {}

    # Add Account-specific field(s).
    if obj_type == TARGET_OBJECT_ACCOUNT:
        sf_obj["Name"] = company_publish_data.get_insight_value(InsightConstants.INSIGHT_NAME_COMPANY_NAME)

    # Add Lead-specific field(s).
    if obj_type == TARGET_OBJECT_LEAD:
        sf_obj["Company"] = company_publish_data.get_insight_value(InsightConstants.INSIGHT_NAME_COMPANY_NAME)
        sf_obj["FirstName"] = ""
        sf_obj["LastName"] = "N/A"
        sf_obj["LeadSource"] = "EverString"

    # Everything below this comment involves adding fields that are shared between all salesforce object types.
    sf_obj["Website"] = company_publish_data.get_insight_value(InsightConstants.INSIGHT_NAME_DOMAIN)

    field_template = "{0}%s{1}".format(SFDC_PACKAGE_NAMESPACE_PREFIX, SFDC_CUSTOM_COMPONENT_SUFFIX)
    cur_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
    sf_obj[field_template % "ESCreatedTimestamp"] = cur_time
    sf_obj[field_template % "ESSource"] = True

    for mapping in field_mapping:
        insight_name = mapping.get("modelId") or mapping.get("displayName")
        insight_value = company_publish_data.get_insight_value(insight_name)

        # For Technology, Audience Names, and Keywords, we truncate insight_value based on the
        # character limits from our default mappings.
        # https://everstring.atlassian.net/wiki/display/NB/Multiple+Model+Scoring
        if insight_name == InsightConstants.INSIGHT_NAME_TECHNOLOGY:
            insight_value = insight_value[:4000]
        if insight_name in [InsightConstants.INSIGHT_NAME_KEYWORDS, InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES]:
            insight_value = insight_value[:2000]

        target_field = mapping.get("targetField")
        if insight_name == InsightConstants.INSIGHT_NAME_REVENUE:
            revenue_buckets = current_app.config.get("REVENUE_BUCKETS")
            sf_obj[target_field] = bucketize(insight_value * 1000, revenue_buckets)
        elif insight_name == InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE:
            employee_size_buckets = current_app.config.get("EMPLOYEE_SIZE_BUCKETS")
            sf_obj[target_field] = bucketize(insight_value, employee_size_buckets)
        elif insight_value is not None:
            sf_obj[target_field] = insight_value

    return sf_obj


def get_salesforce_fields(account_id, object_name):
    crm_token = SFDCToken.get_sfdc_crm_token(account_id)
    sfdc_client = NSSalesforce(SFDCToken.load_from_crm_token(crm_token))
    result = sfdc_client.get_sfdc_info_by_url('sobjects/{objectName}/describe/'.format(objectName=object_name))
    schema_list = result.get("fields", [])
    return schema_list


class NSSalesforce(object):

    def __init__(self, sfdc_token):
        self.client = simple_salesforce.Salesforce(session_id=sfdc_token.access_token,
                                                   instance_url=sfdc_token.instance_url)

    def add_sfdc_object(self, obj_type, data):

        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + self.client.session_id,
            'X-PrettyPrint': '1'
        }

        result = requests.post(
            'https://%s/services/data/v29.0/sobjects/%s/' % (self.client.sf_instance, obj_type),
            headers=headers,
            json=data)

        if result.status_code != 201:
            logger.warn("Add sfdc object error: %s" % result.json())
            raise exceptions.InternalServerError(result.json()[0].get("message"))

        return result.json().get("id")

    def query_profile_name(self, user_id):
        sql = "SELECT Name, Profile.id, Profile.NAME FROM user WHERE id = '{}'".format(user_id)
        result = self.client.query_all(sql)
        records = result.get("records")
        profile_name = None
        if len(records) > 0:
            profile_name = records[0].get("Profile").get("Name")
        return profile_name

    def query_sf_accounts(self, **kwargs):
        """
        query accounts(accountId, accountName, website(domain)) from SF

        :param token:
        :param kwargs:
        :return:
        """
        end_date = kwargs.get("end_date")
        start_date = kwargs.get("start_date")
        limit = kwargs.get("limit")

        assert isinstance(end_date, datetime), "Invalid endDate"
        assert isinstance(start_date, datetime), "Invalid startDate"
        limit = limit if limit else 10000

        soql = '''SELECT Opportunity.Account.Id,
                        Opportunity.Account.Name,
                        Opportunity.Account.Website
                    FROM Opportunity
                    WHERE CreatedDate >= {}
                    AND CreatedDate <= {}
                    '''.format(start_date.strftime('%Y-%m-%dT%H:%M:%SZ'), end_date.strftime('%Y-%m-%dT%H:%M:%SZ'))

        response = self.client.query(soql)
        accounts_map = {}
        while True:
            records = response.get("records")
            if len(records) < 1:
                break
            for record in records:
                account = record.get("Account")
                if accounts_map.get(account.get("Id")):
                    continue
                accounts_map[account.get("Id")] = account
                if len(accounts_map) == limit:
                    return accounts_map.values()
            if response['done']:
                break
            next_url = response['nextRecordsUrl']
            response = self.client.query_more(next_url, identifier_is_url=True)
        return accounts_map.values()

    def query_contacts_by_accounts(self, account_ids):
        """
        query contact email by accountIds
        :param account_ids:
        :return:
        """
        if not account_ids:
            return []

        def append_quota(s):
            return ''.join(['\'', str(s), '\''])

        account_ids = ','.join(map(append_quota, account_ids))

        contacts = self.client.query_all(
            "select AccountId, MAX(Email) Email "
            "FROM Contact "
            "WHERE AccountId IN (" + account_ids + ") "
            "GROUP BY AccountId")

        return contacts.get("records")

    def fill_website_for_account(self, sf_accounts):
        """
        fill domain for accounts that without valid website
        :param sf_accounts:
        :return:
        """
        no_domain_accounts = {}
        for account in sf_accounts:
            if is_blank(account.get("Website")):
                no_domain_accounts[account.get('Id')] = account

        if no_domain_accounts:

            contacts = self.query_contacts_by_accounts(no_domain_accounts.keys())

            account_id_2_email = {}
            for contact in contacts:
                account_id = contact.get("AccountId")
                if account_id:
                    account_id_2_email[account_id] = contact.get("Email")

            for key, value in account_id_2_email.items():
                no_domain_accounts.get(key)["Website"] = get_domain_from_email(value)

        return sf_accounts

    @staticmethod
    def query_accounts_from_opportunities(user, start_date, end_date, limit=10000):
        #   refresh token
        token = refresh_and_update_token(user)

        ns_salesforce = NSSalesforce(token)

        accounts = ns_salesforce.query_sf_accounts(
            start_date=start_date, end_date=end_date, limit=limit)

        accounts = ns_salesforce.fill_website_for_account(accounts)

        return accounts

    def get_sfdc_info_by_url(self, path):
        return self.client.restful(path, None)

    def query(self, soql):
        return self.client.query(soql)
