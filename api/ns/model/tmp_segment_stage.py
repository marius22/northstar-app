import datetime

from ns.model import db


class TmpSegmentStage(db.Model):
    __tablename__ = "tmp_segment_stage"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)
    stage = db.Column(db.SMALLINT, nullable=False)

    @staticmethod
    def get_by_segment(segment_id):
        return TmpSegmentStage.query.filter_by(segment_id=segment_id).first()

    @staticmethod
    def create_or_update(segment_id, stage):
        tmp_segment_stage = TmpSegmentStage.get_by_segment(segment_id)
        if tmp_segment_stage:
            tmp_segment_stage.stage = stage
        else:
            tmp_segment_stage = TmpSegmentStage(segment_id=segment_id,
                                                stage=stage)
            db.session.add(tmp_segment_stage)
        db.session.commit()
        return tmp_segment_stage
