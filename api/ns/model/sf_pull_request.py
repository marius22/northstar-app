"""
Source Model
"""
from ns.model import db
import datetime


class SFPullRequest(db.Model):

    __tablename__ = 'sf_pull_request'

    IN_PROGRESS = "IN_PROGRESS"
    COMPLETED = "COMPLETED"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    start_ts = db.Column(db.TIMESTAMP, nullable=True)
    end_ts = db.Column(db.TIMESTAMP, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    segment_id = db.Column(db.Integer, nullable=False, server_default='0')
    account_id = db.Column(db.Integer, nullable=False, server_default='0')
    status = db.Column(db.String(255), nullable=False)

    @staticmethod
    def get_by_user_segment(user, segment):
        return SFPullRequest.query\
            .filter(SFPullRequest.user_id == user.id,
                    SFPullRequest.segment_id == segment.id)\
            .first()

    @staticmethod
    def _get_by_user_segment_account(user_id, segment_id, account_id):
        return SFPullRequest.query\
            .filter(SFPullRequest.user_id == user_id,
                    SFPullRequest.account_id == account_id,
                    SFPullRequest.segment_id == segment_id)\
            .first()

    @staticmethod
    def save_account_request(user, account, start_date, end_date):
        return SFPullRequest._save_pull_request(user.id, 0, account.id, start_date, end_date)

    @staticmethod
    def save_segment_request(user, segment, start_date, end_date):
        return SFPullRequest._save_pull_request(user.id, segment.id, 0, start_date, end_date)

    @staticmethod
    def _save_pull_request(user_id, segment_id, account_id, start_date, end_date):
        db_request = SFPullRequest._get_by_user_segment_account(user_id, segment_id, account_id)
        if db_request is None:
            request = SFPullRequest(
                user_id=user_id,
                segment_id=segment_id,
                account_id=account_id,
                start_ts=start_date,
                end_ts=end_date,
                status=SFPullRequest.IN_PROGRESS
            )
            db.session.add(request)
            db.session.commit()
            return request
        else:
            db_request.reset(start_date, end_date)
            return db_request

    def reset(self, start_time, end_time):
        self.start_ts = start_time
        self.end_ts = end_time
        self.status = SFPullRequest.IN_PROGRESS
        db.session.commit()

    def complete(self):
        self.status = SFPullRequest.COMPLETED
        db.session.commit()
