import datetime
from marshmallow.fields import Field
import json

from ns.model import db


class MASToken(db.Model):

    __tablename__ = "mas_token"

    MARKETO = "marketo"
    MARKETO_SANDBOX = "marketo_sandbox"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    account_id = db.Column(db.Integer, nullable=True)
    mas_type = db.Column(db.String(255), nullable=False)
    mas_id = db.Column(db.String(255), nullable=False)
    token = db.Column(db.String(2048), nullable=False)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @staticmethod
    def get_by_account_id_and_mas_type(account_id, mas_type):
        return MASToken.query.filter(MASToken.account_id == account_id,
                                     MASToken.mas_type == mas_type).first()

    @staticmethod
    def save_token(account_id, mas_type, token):
        Token.check_format(token)
        db_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
        if db_token is None:
            token = MASToken(
                account_id=account_id,
                mas_type=mas_type,
                token=json.dumps(token),
                mas_id=token.get("client_id")
            )
            db.session.add(token)
            db.session.commit()
            return token
        else:
            db_token.update_token(account_id, mas_type, json.dumps(token), token.get("client_id"))
            return db_token

    def update_token(self, account_id, mas_type, token, mas_id):
        self.account_id = account_id
        self.mas_type = mas_type
        self.token = token
        self.mas_id = mas_id
        db.session.commit()
        return self

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @property
    def token_info(self):
        return json.loads(self.token)


class Token(Field):
    def deserialize(self, value, attr=None, data=None):
        return value

    @staticmethod
    def check_format(token):
        assert isinstance(token, dict), "invalid format of token"
        assert "client_id" in token
        assert "client_secret" in token
        assert "endpoint" in token


class PublishConfig(Field):
    def deserialize(self, value, attr=None, data=None):
        return value

    # @staticmethod
    # def check_format(token):
    #     assert isinstance(token, dict), "invalid format of token"
    #     assert "client_id" in token
    #     assert "client_secret" in token
    #     assert "endpoint" in token
