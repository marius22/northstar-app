"""
Company new
"""
from ns.model import db
import datetime


class CompanyNew(db.Model):
    __tablename__ = 'company_new'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    name = db.Column(db.String(255), nullable=False)
    domain = db.Column(db.String(255), nullable=False)

    @staticmethod
    def get_by_domain(domain):
        return CompanyNew.query.filter(CompanyNew.domain == domain).first()

    @staticmethod
    def add_new_domain(domain, name=""):
        db_domain = CompanyNew.get_by_domain(domain)
        if not db_domain:
            db_domain = CompanyNew(
                name=name,
                domain=domain
            )
            db.session.add(db_domain)
            db.session.commit()

        return db_domain

    @property
    def serialized(self):
        return {
            "domain": self.domain,
            "name": self.name
        }
