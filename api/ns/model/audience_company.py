import datetime

from ns.model import db
from ns.model.base_model import BaseModel
from sqlalchemy import func, and_, or_, asc, desc, sql, exists


class AudienceCompany(db.Model, BaseModel):
    __bind_key__ = 'redshift'
    __tablename__ = 'audience_company'

    audience_id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(255), nullable=True)
    domain = db.Column(db.String(255), nullable=False)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @staticmethod
    def get_page_result(audience_id, limit, offset):
        return

    @staticmethod
    def get_companies_count(audience_id):
        return AudienceCompany.query.filter_by(audience_id=audience_id).\
            with_entities(func.count(AudienceCompany.domain)).scalar()
