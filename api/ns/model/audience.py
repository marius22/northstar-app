import calendar
import datetime
import inflection

from sqlalchemy.orm import relationship
from werkzeug import exceptions

from ns.model import db
from ns.model.user import User
from ns.model.model import Model
from ns.model.audience_exclusion_list import AudienceExclusionList
from sqlalchemy import sql, desc, or_, and_


class Audience(db.Model):
    __tablename__ = 'audience'

    QUERY = "found"
    ENRICH = "enriched"

    source_type_enums = (QUERY, ENRICH)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    company_number_refresh_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)

    name = db.Column(db.String(255), nullable=False)
    source_type = db.Column(db.Enum(*source_type_enums), nullable=False)
    meta_type = db.Column(db.SmallInteger, nullable=False)
    description = db.Column(db.String(1000), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    model_id = db.Column(db.Integer, db.ForeignKey("model.id"))
    company_total_num = db.Column(db.Integer, default=0, server_default="0")
    company_published_num = db.Column(db.Integer, default=0, server_default="0")
    is_private = db.Column(db.Boolean, nullable=False, server_default='1')
    is_deleted = db.Column(db.Boolean, default=False)
    is_published_sfdc = db.Column(db.Boolean, default=False, server_default='0')
    model = relationship("Model")
    keywords = db.relationship('AudienceKeywords', backref='audience', lazy='dynamic')
    criteria = db.relationship('AudienceCriteria', backref='audience', lazy='dynamic')
    real_time_scoring_enabled = db.Column(db.Boolean, server_default=sql.expression.false(), default=False)
    real_time_scoring_enable_ts = db.Column(db.TIMESTAMP, nullable=True)
    show_in_sfdc = db.Column(db.Boolean, nullable=False, server_default='0')
    latest_publish_ts = db.Column(db.TIMESTAMP, nullable=True)

    META_TYPE_AUDIENCE = "audience"
    META_TYPE_EXCLUSION = "exclusion"
    META_TYPE_MAPPING = {
        META_TYPE_AUDIENCE: 100,
        META_TYPE_EXCLUSION: 200,
    }
    META_TYPE_NUM_TO_STRING = dict((value, key) for key, value in META_TYPE_MAPPING.iteritems())

    @staticmethod
    def get_by_id(audience_id):
        return Audience.query\
            .filter(Audience.id == audience_id,
                    Audience.is_deleted.is_(False))\
            .first()

    @staticmethod
    def get_by_ids(audience_ids):
        if len(audience_ids) == 0:
            return []

        return (Audience.query
                .filter(Audience.id.in_(audience_ids),
                        Audience.is_deleted.is_(False))
                .all())

    @staticmethod
    def get_matched_audiences_by_ids(matched_ids, published_ids):
        return Audience.query \
            .filter(or_(and_(Audience.id.in_(matched_ids),
                             Audience.show_in_sfdc.is_(True),
                             Audience.is_published_sfdc.is_(False)),
                        and_(Audience.id.in_(published_ids),
                             Audience.show_in_sfdc.is_(False),
                             Audience.is_published_sfdc.is_(True)))) \
            .order_by(desc(Audience.created_ts))

    @staticmethod
    def create_audience(name, description, user_id,
                        source_type=source_type, meta_type=meta_type):
        audience = Audience(name=name, description=description,
                            source_type=source_type, meta_type=meta_type,
                            user_id=user_id, is_private=True, is_deleted=False,
                            show_in_sfdc=True)
        db.session.add(audience)
        db.session.commit()
        return audience

    @staticmethod
    def delete_by_id(audience_id):
        audience = Audience.query.get(audience_id)
        audience.is_deleted = True
        db.session.commit()

    @property
    def exclusion_lists(self):
        exclusions = AudienceExclusionList.get_by_audience_id(self.id)
        exclusion_ids = [exclusion.exclusion_list_id for exclusion in exclusions]
        exclusions = Audience.query.filter(Audience.id.in_(exclusion_ids),
                                           Audience.meta_type == Audience.META_TYPE_MAPPING.get(
                                               Audience.META_TYPE_EXCLUSION),
                                           Audience.is_deleted.is_(False)).all()
        return [exclusion.serialized for exclusion in exclusions]

    @property
    def serialized_public(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }

    @property
    def serialized(self,):
        user = User.get_by_id(self.user_id)
        return {'userId': self.user_id,
                'id': self.id,
                'name': self.name,
                'description': self.description,
                'isPrivate': self.is_private,
                'sourceType': self.source_type,
                'metaType': self.META_TYPE_NUM_TO_STRING.get(self.meta_type),
                'isDeleted': self.is_deleted,
                'createdBy': user.full_name,
                'createdOn': calendar.timegm(self.created_ts.utctimetuple()),
                'recommendationNum': self.company_total_num,
                'publishedNum': self.company_published_num,
                'realTimeScoringEnabled': self.real_time_scoring_enabled,
                'modelId': self.model_id,
                'modelName': self.model.name if self.model else None,
                'showInSfdc': self.show_in_sfdc,
                'latestPublishTs': self.latest_publish_ts,
                'companyNumberRefreshTs': calendar.timegm(self.company_number_refresh_ts.utctimetuple()),
                'isPublishedSfdc': self.is_published_sfdc,
                'exclusionLists': self.exclusion_lists
                }

    def update_fit_model(self, fit_model_id):
        assert fit_model_id, "Invalid fitModelId"

        fit_model = Model.get_by_id(fit_model_id)
        user = User.get_by_id(self.user_id)
        valid_types = [Model.FIT_MODEL, Model.ACCOUNT_FIT]
        if not (fit_model and fit_model.type in valid_types and fit_model.account_id == user.account_id):
            raise exceptions.Forbidden("You can't set this fit model to this audience")

        self.model_id = fit_model_id
        db.session.commit()

    def update(self, current_user, new_attrs):

        new_user_id = new_attrs.get("userId")
        if new_user_id:
            new_user = User.get_by_id(new_user_id)
            if not (new_user and current_user.account_id == new_user.account_id):
                raise exceptions.Forbidden("Invalid new owner")

        permitted_fields = ['name', 'description', 'show_in_sfdc', 'is_private',
                            "real_time_scoring_enabled", "user_id", "model_id", "is_published_sfdc"]

        for field in permitted_fields:
            old_value = getattr(self, field)
            new_value = new_attrs.get(inflection.camelize(field, False), old_value)
            setattr(self, field, new_value)

        db.session.commit()

    def update_company_published_num(self, published_num, audience_publish_ts=None):
        if audience_publish_ts:
            self.latest_publish_ts = audience_publish_ts
        self.company_published_num = published_num
        db.session.commit()

    def update_company_number(self, company_number):
        self.company_total_num = company_number
        self.company_number_refresh_ts = datetime.datetime.utcnow()
        db.session.commit()
