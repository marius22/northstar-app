"""
Seed filter model
"""
from ns.model import db
import datetime


class SeedFilter(db.Model):
    __tablename__ = 'seed_filter'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), nullable=False)

    filter_type = db.Column(db.String(255), nullable=False)
    filter_value = db.Column(db.String(1024), nullable=False)
