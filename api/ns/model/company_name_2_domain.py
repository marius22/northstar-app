from flask import current_app
from ns.util.http_client import HTTPClient
from ns.util.list_util import split_list
import requests


def fetch_domains_dict_by_company_names(company_names):
    if not company_names:
        return {}
    assert isinstance(company_names, (list, set)), "Invalid param format"

    data = []

    name_to_domain_endpoint = current_app.config.get("COMPANY_NAME_TO_DOMAIN_HOST") + "/name_domain/c2d/"

    for names in split_list(company_names, 1000):
        resp = HTTPClient.post_json(
            name_to_domain_endpoint,
            json={
                "name": "||".join(names),
                'source': "eap",
                "enable_crawl": "1",
                "max_wait_seconds": "0"
            }
        )

        data.extend(resp["result"])

    result = {}
    for item in data:
        domain = item["domain"]
        if domain:
            result[item["name"]] = domain

    return result


def fetch_domains_list_by_company_names(company_names):
    data = fetch_domains_dict_by_company_names(company_names)
    return data.values()
