import datetime
import inflection
from ns.model import db
from ns.model.user import User


class UserInvitation(db.Model):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"), nullable=False)
    email = db.Column(db.String(255), unique=False, nullable=False)
    first_name = db.Column(db.String(255), unique=False, nullable=True)
    last_name = db.Column(db.String(255), unique=False, nullable=True)
    phone = db.Column(db.String(255), unique=False, nullable=True)
    status = db.Column(db.String(255), nullable=False, server_default='PENDING')
    role_id = db.Column(db.Integer, nullable=True)

    @staticmethod
    def get_by_account_email(account, email):
        return UserInvitation.query.filter(UserInvitation.account_id == account.id,
                                           UserInvitation.email == email).first()

    @staticmethod
    def get_by_account(account_id):
        return UserInvitation.query.filter(UserInvitation.account_id == account_id).all()

    @staticmethod
    def get_by_account_user_id(account_id, user_id):
        return UserInvitation.query.filter(UserInvitation.account_id == account_id,
                                           UserInvitation.user_id == user_id).first()

    @staticmethod
    def get_by_id(invitation_id):
        return UserInvitation.query.filter(UserInvitation.id == invitation_id).first()

    @staticmethod
    def create_invitation(account, email, role_id):
        user = User.get_user_by_email(email)
        db_invitation = UserInvitation.get_by_account_email(account, email)
        if db_invitation is None:
            invitation = UserInvitation(account_id=account.id,
                                        email=email,
                                        status='PENDING',
                                        user_id=user.id,
                                        role_id=role_id)
            db.session.add(invitation)
            db.session.commit()
            return invitation
        else:
            return db_invitation

    @staticmethod
    def delete_by_account_user(account_id, user_id):
        UserInvitation.query.filter(UserInvitation.account_id == account_id,
                                    UserInvitation.user_id == user_id).delete()
        db.session.commit()

    def update(self, new_attrs):
        """

        Parameters
        ----------
        @type new_attrs: dict
        """

        permitted_fields = ['email', 'first_name', 'last_name', 'phone']

        for field in permitted_fields:
            old_value = getattr(self, field)
            new_value = new_attrs.get(inflection.camelize(field, False), old_value)
            setattr(self, field, new_value)

        db.session.add(self)
        db.session.commit()

    @property
    def serialized(self):

        return {
            'invitationId': self.id,
            'accountId': self.account_id,
            'email': self.email,
            'status': self.status,
            'firstName': self.first_name,
            'lastName': self.last_name,
            'phone': self.phone,
            "roleId": self.role_id
        }
