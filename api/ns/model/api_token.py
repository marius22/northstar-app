import datetime
from ns.model import db
from os import urandom
from binascii import hexlify
import base64
from werkzeug import exceptions
from base64 import b64encode, b64decode


class ApiToken(db.Model):

    __tablename__ = "api_token"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), unique=True, nullable=False)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    customer_identifier = db.Column(db.String(16), unique=True, nullable=False)
    token = db.Column(db.String(255), nullable=False, server_default="")

    @staticmethod
    def generate_token_str():
        return hexlify(urandom(16))

    @staticmethod
    def generate_customer_identifier():
        return hexlify(urandom(4))

    @staticmethod
    def validate_and_retrieve_token(token_encoded):
        try:
            token_decoded = b64decode(token_encoded)
            customer_identifier, token_str = token_decoded.split(".")
        except:
            raise exceptions.BadRequest("Invalid token")
        if not customer_identifier or not token_str:
            raise exceptions.Unauthorized("Invalid token")
        return ApiToken.query.filter(ApiToken.token == token_str).\
            filter(ApiToken.customer_identifier == customer_identifier).first()

    @staticmethod
    def get_token_by_account_id(account_id):
        return ApiToken.query.filter(ApiToken.account_id == account_id).first()

    @staticmethod
    def create_api_token(account_id):
        token_str = ApiToken.generate_token_str()
        db_token = ApiToken.get_token_by_account_id(account_id)
        if not db_token:
            customer_identifier = ApiToken.generate_customer_identifier()
            token = ApiToken(
                token=token_str,
                account_id=account_id,
                customer_identifier=customer_identifier
            )
            db.session.add(token)
            db.session.commit()
            return token
        else:
            db_token.token = token_str
            db.session.commit()
            return db_token

    @property
    def serialized(self):
        return {
            "id": self.id,
            "account_id": self.account_id,
            "token": self.token,
        }

    @property
    def token_encoded(self):
        return b64encode(self.customer_identifier + "." + self.token)
