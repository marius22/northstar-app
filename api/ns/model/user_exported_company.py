"""
Model for saving user exported companies
"""
import datetime

from ns.model import db
from ns.model.base_model import BaseModel
from ns.util.redshift_client import RedshiftClient
from sqlalchemy import and_, false, func


class UserExportedCompany(db.Model, BaseModel):
    __bind_key__ = 'redshift'
    __tablename__ = 'user_exported_company'

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    segment_id = db.Column(db.Integer, db.ForeignKey("segment.id"), primary_key=True, nullable=False)
    company_id = db.Column(db.Integer, index=True, primary_key=True, nullable=False)
    is_csv = db.Column(db.Boolean, default=False)
    is_crm = db.Column(db.Boolean, default=False)

    @staticmethod
    def get_by_segment_and_company_ids(segment_id, company_ids):
        return UserExportedCompany.query\
            .filter(UserExportedCompany.segment_id == segment_id,
                    UserExportedCompany.company_id.in_(company_ids)).all()

    @staticmethod
    def update_exported_companies(segment_id, company_ids, export_type):
        if not company_ids:
            return

        current_time = datetime.datetime.utcnow()
        is_csv = True if "CSV" in export_type else False
        is_crm = True if "CRM" in export_type else False
        UserExportedCompany.query.\
            filter(UserExportedCompany.segment_id == segment_id,
                   UserExportedCompany.company_id.in_(company_ids)).\
            update({UserExportedCompany.updated_ts: current_time, UserExportedCompany.is_crm: is_crm,
                    UserExportedCompany.is_csv: is_csv}, synchronize_session=False)

        db.session.commit()

    @staticmethod
    def get_company_count_by_segment_id(segment_id):
        return UserExportedCompany.query.with_entities(func.count(UserExportedCompany.company_id)). \
            filter(UserExportedCompany.segment_id == segment_id).scalar()

    @staticmethod
    def count_by_segment(segment_id):
        where_clause = {'segment_id': segment_id}
        fields = ['count(1)']
        row = RedshiftClient.instance().select_one('user_exported_company', fields, where_clause)
        return row[0]
