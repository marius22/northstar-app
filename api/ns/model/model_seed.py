import datetime
from ns.model import db
from seed_company import SeedCompany
import json


class ModelSeed(db.Model):

    __tablename__ = 'model_seed'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, nullable=False)

    # old seed_domain_ids, just for data migration
    seed_domain_ids = db.Column(db.Text, nullable=True)

    @staticmethod
    def get_by_id(seed_id):
        return ModelSeed.query.filter(ModelSeed.id == seed_id).first()

    @staticmethod
    def create_seed_from(account_id, seed_companies):
        assert account_id, "Invalid accountID"
        assert seed_companies, "Invalid seed domains"

        seed = ModelSeed(
            account_id=account_id
        )

        db.session.add(seed)
        db.session.commit()

        SeedCompany.save_seed_companies(seed.id, seed_companies)

        return seed

    def get_related_domains(self):
        return [seed.domain for seed in SeedCompany.get_by_model_seed_id(self.id)]

    def get_related_seed_domain_entries(self):
        return SeedCompany.get_by_model_seed_id(self.id)
