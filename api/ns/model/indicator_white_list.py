import datetime
from collections import defaultdict
from ns.model import db
from sqlalchemy import distinct


class IndicatorWhiteList(db.Model):

    __tablename__ = 'indicator_white_list'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    category = db.Column(db.String(50), nullable=False)
    indicator = db.Column(db.String(100), nullable=False)
    display_name = db.Column(db.String(255), nullable=False)

    _cache = None
    _cache_valid_key = "NSA_INDICATOR_WHITE_LIST_CACHE"

    @classmethod
    def _get_cache(cls):
        from ns.controller.app import redis_store
        if not (redis_store.get(cls._cache_valid_key) and cls._cache):
            cls._cache = IndicatorWhiteList.query.all()
            redis_store.setex(cls._cache_valid_key, 3600 * 24, 'true')
        return cls._cache

    @staticmethod
    def get_by_indicators(indicators):
        if not indicators:
            return []
        return IndicatorWhiteList.query\
            .filter(IndicatorWhiteList.indicator.in_(indicators))\
            .all()

    @staticmethod
    def get_indicator_categories():
        categories = db.session.query(distinct(IndicatorWhiteList.category)).all()
        return [item[0] for item in categories]

    @staticmethod
    def get_indicator_category_details(categories):
        """
        return a dict of insight category and category details
        {
            "category1": [indicator_1, indicator_2],
            "category2": [indicator_1, indicator_2]
        }
        :param categories:
        :return:
        """
        result = defaultdict(list)

        if not categories:
            return result

        assert isinstance(categories, (list, set)), "Categories must be list or set"

        indicators = IndicatorWhiteList.query \
            .filter(IndicatorWhiteList.category.in_(categories)) \
            .all()

        for indicator in indicators:
            result[indicator.category].append(indicator)

        return result

    @staticmethod
    def get_all_indicators():
        return IndicatorWhiteList._get_cache()

    @staticmethod
    def save_indicators(indicators):
        if not indicators:
            return
            # db.session.add_all(rs)
        db.engine.execute(
            IndicatorWhiteList.__table__.insert(),
            indicators
        )
        db.session.commit()

        return

    @property
    def serialized(self):
        return {
            "category": self.category,
            "indicator": self.indicator,
            "displayName": self.display_name
        }
