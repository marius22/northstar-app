"""
Segment model
"""
import datetime

import httplib
import logging
import time
import json
import math
import ujson

import os
from werkzeug import exceptions
from flask import current_app
from ns.model.account_quota import QuotaExceedError
from ns.model.expansion import Expansion
from ns.model.qualification import Qualification
from ns.model.tmp_account_segment import TmpAccountSegment
from sqlalchemy import func, and_, or_, asc, desc, sql, exists
from ns.model.recommendation import FilterBuildError
from ns.model import db
from ns.model.user_exported_company import UserExportedCompany
from ns.model.job import Job
from ns.model.job_status_history import JobStatusHistory
from ns.model.recommendation import Recommendation
from ns.model.seed import Seed
from ns.model.seed_csv_reader import SeedCSVReader, RowCountsExceedsLimitError
from ns.model.source import Source
from ns.model.user import User
from ns.model.user_segment import UserSegment
from sqlalchemy.orm import relationship
from user_segment_sfdc_publisher import BackgroundPublisher as SFDCPublisher
from ns.util.domain_util import parse_domain
from ns.util.queue_service_client import QueueServiceClient
from ns.model.salesforce import NSSalesforce
from ns.model.seed_candidate import SeedCandidate
from ns.util.queries_filter_builder import InBuilder, OrBuilder
from ns.util.list_util import distinct_count
from ns.model.segment_metric import SegmentMetric
from ns.util.file_util import write_file_to_local
from ns.util.redshift_client import RedshiftClient
logger = logging.getLogger(__name__)


def _get_total_count(recommendation_query):
    return recommendation_query.with_entities(func.count(Recommendation.company_id)).scalar()


class Segment(db.Model):
    __tablename__ = "segment"

    filter_builders = {
        "industry": InBuilder,
        "employeeSize": OrBuilder,
        "state": InBuilder
    }

    EMPTY = "EMPTY"
    DRAFT = "DRAFT"
    IN_PROGRESS = "IN PROGRESS"
    COMPLETED = "COMPLETED"
    status_enums = (EMPTY, DRAFT, IN_PROGRESS, COMPLETED)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    owner_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    last_run_ts = db.Column(db.TIMESTAMP, nullable=True)
    status = db.Column(db.Enum(*status_enums), nullable=False)
    is_deleted = db.Column(db.Boolean, nullable=False)
    recommendation_num = db.Column(db.Integer, default=0, server_default="0")
    exported_num = db.Column(db.Integer, default=0, server_default="0")
    published = db.Column(db.Boolean, default=False,
                          server_default=sql.expression.false())

    seed_candidates = db.relationship(
        'SeedCandidate', backref='segment', lazy='dynamic')
    seed_filters = db.relationship(
        'SeedFilter', backref='segment', lazy='dynamic')

    seeds = relationship('Seed', backref='segment')
    qualification = relationship('Qualification', backref='segment')
    expansion = relationship('Expansion', backref='segment', lazy='dynamic')

    @property
    def exported_companies(self):
        return UserExportedCompany.query.filter(UserExportedCompany.segment_id == self.id)

    @property
    def jobs(self):
        return Job.query\
            .filter(Job.segment_id == self.id, Job.type == Job.SEGMENT)\
            .order_by(Job.id.desc())

    def delete(self):
        self.is_deleted = True
        db.session.commit()

    @property
    def latest_completed_job(self):
        completed_jobs = self.jobs.filter_by(status=Job.COMPLETED)
        return completed_jobs.first()

    @property
    def latest_recommendations(self):
        return Recommendation.query.filter(Recommendation.segment_id == self.id)

    @property
    def expansion(self):
        return Expansion.query.filter_by(segment_id=self.id).\
            order_by(desc(Expansion.id)).first()

    @property
    def qualification(self):
        return Qualification.query.filter_by(segment_id=self.id).\
            order_by(desc(Qualification.id)).first()

    @property
    def exported_company_ids(self):
        return [ec_id[0] for ec_id in
                self.exported_companies.with_entities(UserExportedCompany.company_id)]

    @property
    def sector_metric(self):
        sector_metric = SegmentMetric.get_by_type(self.id, SegmentMetric.METRIC_SECTOR)
        return sector_metric.value

    @property
    def biz_similar_cnt(self):
        # refer to app/segment.get_company_count_by_sector
        counts = self.sector_metric
        return counts["1"] + counts["4"] + counts["5"] + counts["7"]

    @property
    def tech_similar_cnt(self):
        # refer to app/segment.get_company_count_by_sector
        counts = self.sector_metric
        return counts["2"] + counts["4"] + counts["6"] + counts["7"]

    @property
    def dep_similar_cnt(self):
        # refer to app/segment.get_company_count_by_sector
        counts = self.sector_metric
        return counts["3"] + counts["5"] + counts["6"] + counts["7"]

    @property
    def all_similar_counts(self):
        # refer to app/segment.get_company_count_by_sector
        counts = self.sector_metric
        return dict(
            tech=counts["2"] + counts["4"] + counts["6"] + counts["7"],
            biz=counts["1"] + counts["4"] + counts["5"] + counts["7"],
            department=counts["3"] + counts["5"] + counts["6"] + counts["7"]
        )

    @property
    def in_boundary_companies_cnt(self):
        metric = SegmentMetric.get_by_type(self.id, SegmentMetric.METRIC_IN_BOUNDARY_COUNT)
        return metric.value

    def get_tech_distinct_count(self, limit=None, asc=False):

        metric = SegmentMetric.get_by_type(self.id, SegmentMetric.METRIC_TECH)
        result = metric.value
        if asc:
            result.reverse()
        return result[:limit] if limit else result

    @property
    def seed_candidates_cnt(self):
        return self.seed_candidates.with_entities(func.count(SeedCandidate.id)).scalar()

    def seed_candidates_by_source(self, source):
        return self.seed_candidates.filter(SeedCandidate.source == source)

    def seed_candidate_insights(self):
        """

        Returns
        -------
            Dict represented insights
        """
        current_app.logger.debug(
            'Get seed candidates insights: Segment(%s)', self)
        return SeedCandidate.insights(self.seed_candidates)

    @staticmethod
    def create_or_replace_segment(user, segment_name, account_id=None):
        """
        If draft segment with the same name exists, error will be raised,
        but we allow to create segment with the same name as in progress/completed segment
        segment could only allowed to be created when quota not reached
        :param user:
        :param segment_name:
        :param account_id:
        :return: segment object
        """

        if user.account.segment_cnt_quota_not_reached:
            segment = Segment.query.filter_by(owner_id=user.id, name=segment_name).\
                filter(Segment.status == Segment.EMPTY).first()
            if segment:
                segment.created_ts = datetime.datetime.utcnow()
                segment.updated_ts = datetime.datetime.utcnow()
                segment.status = Segment.EMPTY
                segment.is_deleted = False
            else:
                segment = Segment(name=segment_name, status=Segment.EMPTY,
                                  owner=user, is_deleted=False)
                db.session.add(segment)
            db.session.commit()

            # ES admin help other account to create segments
            if account_id and account_id != user.account.id:
                TmpAccountSegment.create(account_id, segment.id)
            return segment
        else:
            quota = user.account.account_quota._serialized(includes_usage=True)

            message = ('Insufficient segment quota: '
                       'quota = %s, account = %s') % (quota, user.account)
            raise QuotaExceedError(message)

    @staticmethod
    def get_by_user_and_name(user_id, name):
        return Segment.query.filter_by(owner_id=user_id, name=name, is_deleted=False).first()

    @staticmethod
    def get_by_owner(owner_id):
        return Segment.query.filter_by(owner_id=owner_id, is_deleted=False).filter(
            Segment.status != Segment.EMPTY).all()

    @staticmethod
    def get_by_user(user_id):

        segments = Segment.query.join(UserSegment, Segment.id == UserSegment.segment_id). \
            filter(and_(UserSegment.user_id == user_id,
                        Segment.is_deleted.is_(False))).all()
        return segments

    @staticmethod
    def get_shared_segments(user_id):
        shared_user_segments = UserSegment.get_user_segments_by_user(user_id)
        return Segment.query.filter(
            Segment.id.in_(us.segment_id for us in shared_user_segments)).all()

    @staticmethod
    def get_all_segments(user_id):
        user = User.query.get(user_id)
        if user is None:
            return []
        segment_lst = user.segments.filter(Segment.is_deleted.is_(False),
                                           Segment.status != Segment.EMPTY).all()
        segments_shared = Segment.get_by_user(user_id)
        segment_lst.extend(segments_shared)
        results = filter(lambda x: not x.is_deleted and x.status !=
                         Segment.EMPTY, segment_lst)
        return results

    @staticmethod
    def get_tmp_account_segments(account_id):
        segments = Segment.query.\
            join(TmpAccountSegment, Segment.id == TmpAccountSegment.segment_id). \
            filter(and_(TmpAccountSegment.account_id == account_id,
                        Segment.is_deleted.is_(False),
                        Segment.status != Segment.EMPTY)).all()
        return segments

    @staticmethod
    def get_by_id(segment_id):
        return Segment.query.filter_by(id=segment_id, is_deleted=False).first()

    def update_create_ts_to_now(self):
        self.created_ts = datetime.datetime.utcnow()
        db.session.commit()

    def to_dict(self):
        segment_dict = {
            'segmentId': self.id,
            'segmentName': self.name,
            'lastRunTime': self.last_run_ts.isoformat() if self.last_run_ts else None,
            'status': self.status,
            'recommendationNum': self.recommendation_num,
            'exportedNum': self.exported_num,
            'createdTime': self.created_ts.isoformat() if self.created_ts else None,
            'ownerId': self.owner_id
        }

        return segment_dict

    def submit_job(self):
        current_app.logger.info("Begin job submission: %s", time.time())
        seed_filters = self.seed_filters.all()
        if len(seed_filters) > 0:
            # TODO: Later stories
            pass
        else:
            # The seed_candidates may be a huge list
            seed_candidates = self.seed_candidates.all()
            domains = [
                seed_candidate.domain for seed_candidate in seed_candidates]
            seed_candidate_ids = [
                seed_candidate.id for seed_candidate in seed_candidates]

            seed = Seed.create(self.id, str(seed_candidate_ids))
            qualification = self.qualification
            expansion = self.expansion
            job = Job.create(self.id, self.owner.account_id, Job.SEGMENT, seed.id,
                             qualification.id if qualification else None,
                             expansion.id if expansion else None,
                             status=Job.IN_PROGRESS)

            qualification_dict =\
                ujson.loads(qualification.qualification_val) if qualification else {}

            # Transform employee size format
            employee_size_map = current_app.config.get('EMPLOYEE_SIZE_MAP')
            input_employee_size = qualification_dict.get('employeeSize')
            if input_employee_size:
                employee_size_lst = \
                    [employee_size_map[employee_size] for employee_size in input_employee_size]
                qualification_dict['employeeSize'] = employee_size_lst

            QueueServiceClient.instance().submit_model_run_request(
                self.owner_id, self.id, job.id, domains,
                qualification_dict=qualification_dict,
                expansion_dict=(ujson.loads(expansion.expansion_val) if expansion else {}))
            self.update_status(Segment.IN_PROGRESS)
            self.reset_recommendation_num()
            current_app.logger.info("End job submission:" + str(time.time()))

    def is_in_draft(self):
        return self.status == Segment.DRAFT

    def is_in_process(self):
        return self.status == Segment.IN_PROGRESS

    def is_completed(self):
        return self.status == Segment.COMPLETED

    def update_status(self, status):
        self.status = status
        db.session.commit()

    def reset_recommendation_num(self):
        self.recommendation_num = 0
        db.session.commit()

    def update_recommendation_num(self, number):
        self.recommendation_num = number
        db.session.commit()

    def update_exported_num(self, exported_num):
        self.exported_num = exported_num
        db.session.commit()

    @staticmethod
    def create_job_history(job, status, job_run_id, job_run_name, job_run_type):
        # save the feature_analysis_finished status in job_history table
        if status == JobStatusHistory.STATUS_FINISHED:
            JobStatusHistory(job_id=job.id,
                             status=JobStatusHistory.FEATURE_ANALYSIS_FINISHED,
                             job_run_id=job_run_id,
                             job_run_name=job_run_name,
                             job_run_type=job_run_type)

        job_status_history = JobStatusHistory(job_id=job.id,
                                              status=status,
                                              job_run_id=job_run_id,
                                              job_run_name=job_run_name,
                                              job_run_type=job_run_type)
        segment = Segment.query.get(job.segment_id)
        if status == JobStatusHistory.SEGMENT_FINISHED:
            job.status = Job.COMPLETED
            segment.status = Segment.COMPLETED
            segment.last_run_ts = datetime.datetime.utcnow()
        if status == JobStatusHistory.STATUS_FAILED:
            job.status = Job.FAILED
            segment.status = Segment.IN_PROGRESS

        db.session.add(job_status_history)
        db.session.commit()

    def attach_csv(self, csv_file):
        """
        Save the CSV to filesystem, parse it, then create the seed candidates from the CSV in batch.
        Returns
        -------

        """
        file_name = 'seed_candidates_seg_%d_%s.csv' \
                    % (self.id, datetime.datetime.utcnow().isoformat())

        file_path = SeedCSVReader.write_seed_csv_to_local(file_name, csv_file)

        max_rows = current_app.config.get('MAX_SEED_CSV_ROW_COUNTS', 500)
        file_rows = SeedCSVReader.num_lines(file_path)
        if file_rows > max_rows:
            raise RowCountsExceedsLimitError(
                {'max_rows': max_rows, 'file_rows': file_rows})

        candidates = SeedCSVReader.get_seed_candidates_from_file_path(file_path)

        source = Source.get_by_name(Source.CSV)
        self.update_seeds(source, candidates)

    def csv_overwrite_allowed(self):
        return self.status in [Segment.EMPTY, Segment.DRAFT]

    def is_shared(self):
        return self.published

    def belongs_to(self, account_id):
        return account_id == self.owner.account_id

    def publish(self):
        """Share this segment with all users of this account."""
        owner = User.query.get(self.owner_id)
        same_account_users = (
            User.query
            .filter_by(account_id=owner.account_id)
            # Don't share with yourself.
            .filter(User.id != self.owner.id)
            .all())

        same_account_user_ids = [user.id for user in same_account_users]

        self.share(same_account_user_ids)
        self.published = True
        db.session.commit()

    @staticmethod
    def publish_segments_to_user(segments, user):
        if not segments:
            return
        assert user, "Invalid user"

        UserSegment.add_reading_permission(
            [segment.id for segment in segments],
            [user.id]
        )

    def unpublish(self):
        """Stop sharing this segment with all users of this account."""
        owner = User.query.get(self.owner_id)
        same_account_users = (
            User.query
            .filter_by(account_id=owner.account_id)
            .all())

        same_account_user_ids = [user.id for user in same_account_users]

        self.unshare(same_account_user_ids)
        self.published = False
        db.session.commit()

    def share(self, user_ids):
        """Make segment visible to the passed in users."""
        users_to_share_with = User.query.filter(User.id.in_(user_ids)).all()

        for user in users_to_share_with:
            # If there's already a UserSegment row with a weaker permission,
            # we need to upgrade the permission. If there's an equal or stronger
            # one, we don't need to do anything.
            user_seg = UserSegment.query.filter_by(
                user_id=user.id, segment_id=self.id).first()
            if user_seg:
                user_seg.permission = UserSegment.READ_PERMISSION
                db.session.add(user_seg)

            # If there isn't a user_seg, we need to create one.
            if user_seg is None:
                user_seg = UserSegment(
                    user_id=user.id,
                    segment_id=self.id,
                    permission=UserSegment.READ_PERMISSION)
                db.session.add(user_seg)

        db.session.commit()

    def unshare(self, user_ids):
        UserSegment.query.filter_by(segment_id=self.id).filter(
            UserSegment.user_id.in_(user_ids)).delete(synchronize_session=False)
        db.session.commit()

    def publish_to_sfdc_by_user(self, user, csv_url):
        publisher = SFDCPublisher(user)
        publisher.publish_segment(self, csv_url=csv_url)

    def unpublish_from_sfdc_by_user(self, user):
        publisher = SFDCPublisher(user)
        publisher.unpublish_segment(self)

    def __repr__(self):
        return '<Segment id=%d, name=%s>' % (self.id, self.name)

    def delete_all_seeds(self):
        SeedCandidate.delete_by_seg_id(self.id)

    def upload_sf_seeds(self, user, start_date, end_date):

        accounts = NSSalesforce.query_accounts_from_opportunities(
            user,
            start_date,
            end_date,
            current_app.config.get("MAX_SEED_CSV_ROW_COUNTS"))

        # transform sf_account to seed_candidate
        candidates = _transform_2_candidate(accounts)

        source = Source.get_by_name(Source.SALESFORCE)
        self.update_seeds(source, candidates)

    def update_seeds(self, source, candidates):
        if not candidates:
            raise exceptions.BadRequest("No seeds found. Please try increasing your date range.")

        start = datetime.datetime.utcnow()
        logger.info("Begin update seeds for segment")

        # delete old seeds
        self.delete_all_seeds()

        for candidate in candidates:
            candidate.domain = parse_domain(candidate.domain)
            candidate.segment_id = self.id
            candidate.source_id = source.id

        SeedCandidate.enrich_all(candidates)

        db.engine.execute(SeedCandidate.__table__.insert(),
                          [candidate.to_dict() for candidate in candidates])

        self.status = Segment.DRAFT
        db.session.commit()

        logger.info("Update seeds for segment completed, time cost: {%s}"
                    % (datetime.datetime.utcnow() - start))

    def get_seed_attribute_values(self, key):
        field = SeedCandidate.get_field_by_key(key)
        order = asc(field)
        attribute_values = self.seed_candidates \
            .with_entities(field.label('value')) \
            .filter(and_(field.isnot(None), field != "")) \
            .group_by(field) \
            .order_by(order)

        return [row.value for row in attribute_values]

    def add_exported_companies(self, company_ids, export_type):
        if not company_ids:
            return
        is_csv = True if "CSV" in export_type else False
        is_crm = True if "CRM" in export_type else False
        values = []
        for company_id in company_ids:
            values.append((self.id, company_id, is_csv, is_crm, datetime.datetime.utcnow(), datetime.datetime.utcnow()))
        try:
            columns = ["segment_id", "company_id", "is_csv", "is_crm", "created_ts", "updated_ts"]
            RedshiftClient.instance().insert("user_exported_company", columns, values)
        except Exception as e:
            logger.error(e.message)

    def update_exported_companies(self, company_ids, export_type):
        UserExportedCompany.update_exported_companies(self.id, company_ids, export_type)

    def change_owner(self, new_owner):
        old_owner = self.owner
        if old_owner.id == new_owner.id:
            return

        if self.is_shared():
            self.owner_id = new_owner.id
            user_segment = UserSegment.get_by_user_segment(new_owner, self)
            if user_segment is None:
                user_segment = UserSegment(
                    user_id=old_owner.id,
                    segment_id=self.id,
                    permission=UserSegment.READ_PERMISSION)
                db.session.add(user_segment)
            else:
                # if new_user is a shared user of self
                # remove it from the user_segment
                user_segment.user_id = old_owner.id
            db.session.commit()
        else:
            self.owner_id = new_owner.id
            db.session.commit()

    def seeds_distinct_counts_by_field(self, total_count, key, order, limit):
        if key == "tech":
            techs = [json.loads(seed.tech) for seed in self.seed_candidates]
            return Segment.distinct_counts_tech(order, limit, techs)

        field = SeedCandidate.get_field_by_key(key)
        value_counts = self.seed_candidates \
            .with_entities(func.count(1).label('count'), field.label('value')) \
            .filter(and_(field.isnot(None), field != "")) \
            .group_by(field) \
            .order_by(order) \
            .limit(limit)

        others_cnt = total_count

        results = []
        for row in value_counts:
            results.append({
                'value': row.value,
                'count': row.count
            })
            others_cnt -= row.count

        # sorts categorical values
        categorical_fields = {
            'revenue': current_app.config.get('REVENUE_BUCKETS'),
            'employeeSize': current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
        }

        bucket_plan = categorical_fields.get(key)
        if bucket_plan:
            idxes = [item['label'] for item in bucket_plan]
            # sorts the results by the item indexes in the bucket definition
            results = sorted(
                results,
                key=lambda item: idxes.index(item['value']))

        results.append({'value': 'Others',
                        'count': others_cnt})

        return results

    @staticmethod
    def distinct_counts_tech(order, limit, techs):
        dis_count_map = distinct_count(techs)
        dis_count_list = []
        for key, value in dis_count_map.items():
            dis_count_list.append({
                "value": key,
                "count": value
            })
        dis_count_list.sort(
            cmp=lambda x, y: x.get("count") - y.get("count"),
            reverse=False if str(order).endswith("ASC") else True)

        return dis_count_list[:limit]

    def get_page_result(self, user, args, need_total_cnt=False, for_csv=False, with_company_id_only=False,
                        direct_download=False, with_recommendation_query=False):
        """
        :param recommendation_query: If true, this method only returns a SQL query string selecting the relevant
            recommendations for publish/export.
        """
        try:
            # Filters need to be placed before filtered_recommendations, otherwise
            # the total counts is wrong.
            filters = Recommendation.build_filters(args.get('where'))
            filtered_recommendations = self.latest_recommendations.filter(filters)

            order = args.get('order')
            if order:
                field = Recommendation.get_field_by_key(order.get('key'))
                criterion = (order.get('direction')(field),)
            else:
                criterion = (asc(Recommendation.similar_c_score),
                             asc(Recommendation.similar_t_score),
                             asc(Recommendation.similar_d_score),
                             desc(Recommendation.score))

            # dedupe export
            dedupe = args.get('dedupe', False)
            exported_company_ids = self.exported_company_ids
            if dedupe:
                stmt = exists([1]).where(and_(
                    Recommendation.company_id == UserExportedCompany.company_id,
                    UserExportedCompany.segment_id == self.id
                ))
                filtered_recommendations = filtered_recommendations.\
                    filter(~stmt)

            # only show exported
            only_exported = args.get('onlyExported', False)
            if only_exported:
                filtered_recommendations = \
                    filtered_recommendations.filter(
                        Recommendation.company_id.in_(exported_company_ids))

            if user.has_limit_on_ui_company_cnt():
                cnt_limit = user.account.account_quota.ui_company_limit
                offset = args.get('offset', 0)
                if offset >= cnt_limit:
                    logger.warn("Quota excess, account {%s} can only see {%s} recommendations"
                                % (user.account.id, cnt_limit))
                    return [], _get_total_count(filtered_recommendations)

                limit_arg = args.get('limit')
                limit = min((cnt_limit - offset), limit_arg) if limit_arg else (cnt_limit - offset)

                page_result = filtered_recommendations.order_by(*criterion) \
                    .limit(limit) \
                    .offset(offset)
            else:
                page_result = filtered_recommendations \
                    .order_by(*criterion) \
                    .limit(args.get('limit')) \
                    .offset(args.get('offset'))

            if with_company_id_only:
                page_result = page_result.with_entities(Recommendation.company_id)
                serialized = [recd.company_id for recd in page_result]
            else:
                if for_csv:
                    if direct_download:
                        serialized = [recd.serialized_for_csv for recd in page_result]
                    else:
                        # Generate temp file
                        tmp_csv_path, is_exceed = self.get_company_csv_file(self, user, page_result, args.get('limit'))
                        return tmp_csv_path, is_exceed
                else:
                    serialized = [recd.serialized for recd in page_result]

        except FilterBuildError, e:
            raise exceptions.BadRequest(e.message)
        except Exception, e:
            current_app.logger.exception(e)
            # Always return a list
            serialized = []

        if serialized and need_total_cnt:
            total = _get_total_count(filtered_recommendations)
        else:
            total = 0

        if with_recommendation_query:
            # Return the raw SQL query that selects the relevant recommendations.
            return page_result, total
        return serialized, total

    def check_exported_csv_permission(self, user):
        if (not user.is_es_admin()) \
                and (self.owner_id != user.id):
            raise exceptions.Forbidden("Only the segment owner can publish results.")

    def get_companies_by_sectors(self, args, user, with_company_id_only=False,
                                 direct_download=False):

        # We will have fit score if seed list size greater than 50
        # If have fit score, will order by fit score
        # If not, will use three similarity score sum to sort,
        # these three score range is 0-4, 0 represent the most similar, 4 is the opposite
        if self.seed_candidates_cnt < 50:
            order_by = asc(Recommendation.similar_c_score +
                           Recommendation.similar_t_score +
                           Recommendation.similar_d_score)
        else:
            order_by = desc(Recommendation.score)

        sectors = args.get('sectors')
        dedupe = args.get('dedupe')
        limit = args.get('limit')

        recommendations = self.latest_recommendations
        if with_company_id_only:
            recommendations = recommendations.with_entities(Recommendation.company_id)

        sector_filters = False
        for sector in sectors:
            sector_filter = Recommendation.build_sector_filter(int(sector))
            sector_filters = or_(sector_filters, sector_filter)

        companies = recommendations.filter(sector_filters)
        if dedupe:
            dedupe_filter = Recommendation.build_dedupe_filter(self)
            if dedupe_filter is not None:
                companies = companies.filter(~dedupe_filter)

        companies = companies.order_by(order_by).limit(limit)
        if with_company_id_only:
            results = [company.company_id for company in companies]
        else:
            if direct_download:
                results = [company.serialized_for_csv for company in companies]
            else:
                # Generate temp file
                tmp_csv_path, is_exceed = self.get_company_csv_file(self, user, companies, limit)
                return tmp_csv_path, is_exceed

        return results

    def check_export_quota(self, user, company_ids):
        account = self.owner.account

        if not user.is_es_admin():
            quota = account.account_quota
            if quota.expired():
                raise exceptions.Forbidden("Your quota has expired.")

            limitation = quota.remaining_csv_company_cnt
            new_company_ids_for_account = set(company_ids) - set(account.exported_companies_ids)
            if limitation < len(new_company_ids_for_account):
                raise exceptions.Forbidden(
                    "You cannot publish more than [%s] companies." % limitation)

    def generate_sql_query(self, user, args, need_total_cnt=False):
        """
        :param recommendation_query: If true, this method only returns a SQL query string selecting the relevant
            recommendations for publish/export.
        """
        try:
            # Filters need to be placed before filtered_recommendations, otherwise
            # the total counts is wrong.
            filters = Recommendation.build_filters(args.get('where'))
            filtered_recommendations = self.latest_recommendations.filter(filters)

            order = args.get('order')
            if order:
                field = Recommendation.get_field_by_key(order.get('key'))
                criterion = (order.get('direction')(field),)
            else:
                criterion = (asc(Recommendation.similar_c_score),
                             asc(Recommendation.similar_t_score),
                             asc(Recommendation.similar_d_score),
                             desc(Recommendation.score))

            # Find previously exported companies
            previously_exported_stmt = exists([1]).where(and_(
                Recommendation.company_id == UserExportedCompany.company_id,
                UserExportedCompany.segment_id == self.id
            ))

            only_exported = args.get('onlyExported', False)
            if only_exported:
                filtered_recommendations = (filtered_recommendations.filter(
                    previously_exported_stmt))

            # dedupe export
            dedupe = args.get('dedupe', False)
            if dedupe:
                filtered_recommendations = filtered_recommendations.\
                    filter(~previously_exported_stmt)

            query_result = filtered_recommendations.\
                with_entities(Recommendation.company_id, Recommendation.company_name, Recommendation.domain).\
                order_by(*criterion)

        except FilterBuildError, e:
            raise exceptions.BadRequest(e.message)
        except Exception, e:
            current_app.logger.exception(e)
            # Always return a list
        if need_total_cnt:
            total = _get_total_count(filtered_recommendations)
        else:
            total = 0

        query_result.with_entities(Recommendation.company_id, Recommendation.company_name,
                                   Recommendation.domain)

        return query_result, total

    @staticmethod
    def get_company_csv_file(self, user, page_result, limit):
        try:
            is_exceed = False
            account = self.owner.account
            quota_remaining = account.account_quota.remaining_csv_company_cnt
            companies = page_result.with_entities(Recommendation.company_id)
            company_ids = [recd.company_id for recd in companies]
            new_company_set, exist_company_ids_set = account.get_company_ids_set(account, company_ids)
            exceed_company_ids = []

            if quota_remaining >= len(new_company_set):
                pass
            else:
                if not user.is_es_admin():
                    is_exceed = True
                    temp = 0
                    limit = quota_remaining
                    for company_id in company_ids:
                        if company_id in new_company_set:
                            temp += 1
                            if temp <= limit:
                                pass
                            else:
                                exceed_company_ids.append(company_id)
            tmp_csv_path = Recommendation.generate_tmp_companies_csv(user, page_result, limit, set(exceed_company_ids))

        except FilterBuildError, e:
            raise exceptions.BadRequest(e.message)
        except Exception, e:
            current_app.logger.exception(e)
            # Always return a list
        return tmp_csv_path, is_exceed


class SegmentError(Exception):
    """Errors relating to a predictive segment."""

    def __init__(self, message, code=None):
        Exception.__init__(self, message)
        self.code = code


def _transform_2_candidate(sf_accounts):
    result = []
    for sf_account in sf_accounts:
        seed = SeedCandidate(
            domain=sf_account["Website"],
            sf_account_id=sf_account["Id"],
            sf_account_name=sf_account["Name"]
        )
        result.append(seed)
    return result
