"""
Account model
"""
import datetime
from uuid import uuid4
from flask import current_app
import inflection
import json
from ns.model import db, AnnotatedBaseQuery
from ns.model.account_quota import AccountQuota
from ns.model.account_quota_ledger import AccountQuotaLedger
from ns.model.segment import Segment
from ns.model.user import User
from sqlalchemy import and_, false, true, func
from sqlalchemy.orm import relationship
from ns.util.string_util import is_not_blank
from ns.model.user_role import UserRole
from ns.model.role import Role
from ns.model.user_invitation import UserInvitation
from ns.model.account_exported_company import AccountExportedCompany
from ns.model.job import Job
from ns.model.salesforce import NSSalesforce
from ns.model.model_seed import ModelSeed
from ns.model.model import Model
from ns.model.seed_company import SeedCompany
from ns.util.domain_util import parse_domain
from ns.model.account_exported_contact import AccountExportedContact
from ns.model.crm_token import CRMToken
from werkzeug import exceptions
from ns.model.audience import Audience

import logging

logger = logging.getLogger(__name__)


class Account(db.Model):
    __tablename__ = 'account'

    query_class = AnnotatedBaseQuery

    ES = "ES"
    PAYING = "PAYING"
    TRIAL = "TRIAL"

    account_type_enums = (ES, PAYING, TRIAL)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    name = db.Column(db.String(255), unique=True, nullable=False)
    account_type = db.Column(db.Enum(*account_type_enums), nullable=False)
    deleted = db.Column(db.Boolean, nullable=False, default=False)

    users = db.relationship('User', backref='account', cascade='all', lazy='dynamic')

    account_quota = relationship('AccountQuota', backref='account', uselist=False)
    account_quota_ledger = relationship('AccountQuotaLedger', backref='account', uselist=False)

    all_owned_segments = relationship('Segment', secondary=User.__table__, lazy='dynamic')

    exported_companies = relationship('AccountExportedCompany', backref='account', lazy='dynamic')

    exported_contacts = relationship('AccountExportedContact', backref='account', lazy='dynamic')
    crm_data_initialized = db.Column(db.Boolean, nullable=True, default=False)
    realtime_scoring_object = db.Column(db.String(255), nullable=True, default='["account"]')
    domain = db.Column(db.String(255), nullable=True)
    is_primary = db.Column(db.Boolean, nullable=True, default=False)
    realtime_scoring = db.Column(db.SmallInteger, nullable=True, default=0)

    REALTIME_SCORING_NONE = ""
    REALTIME_SCORING_GLOBAL = "global"
    REALTIME_SCORING_AUDIENCE = "audience"
    REALTIME_SCORING_MAPPING = {
        REALTIME_SCORING_NONE: 0,
        REALTIME_SCORING_GLOBAL: 1,
        REALTIME_SCORING_AUDIENCE: 2,
    }
    REALTIME_SCORING_MAPPING_NUM_TO_STRING = dict((value, key) for key, value in REALTIME_SCORING_MAPPING.iteritems())

    @property
    def realtime_scoring_mode(self):
        return self.REALTIME_SCORING_MAPPING_NUM_TO_STRING[self.realtime_scoring]

    @property
    def sfdc_org_id(self):
        crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([self.id])
        if not crm_tokens:
            return None
        return crm_tokens[0].organization_id

    @property
    def active_users(self):
        return [user for user in self.users if user.activated]

    @property
    def fit_jobs(self):
        return Job.query \
            .filter(Job.account_id == self.id, Job.type == Job.ACCOUNT_FIT) \
            .order_by(Job.id.desc())

    @property
    def latest_fit_job(self):
        return self.fit_jobs.first()

    @property
    def latest_completed_fit_job(self):
        completed_fit_jobs = self.fit_jobs.filter_by(status=Job.COMPLETED)
        return completed_fit_jobs.first()

    @property
    def latest_completed_crm_fit_model(self):
        return Model.get_latest_completed_model(self.id, Model.ACCOUNT_FIT)

    @property
    def latest_completed_fa_model(self):
        return Model.get_latest_model(self.id, Model.FEATURE_ANALYSIS, Model.COMPLETED)

    @property
    def latest_crm_fit_model(self):
        return Model.get_latest_model(self.id, Model.ACCOUNT_FIT, None)

    @property
    def exported_companies_cnt(self):
        # we should count only those companies as published towards the quota which were exported within the contract
        # period; If we use present date, then we are counting from the very scratch.
        # present_time = datetime.datetime.utcnow()

        # PS: if start_ts is None, it is set to 1970-01-01 00:00:00 in the calling function (remaining_csv_company_cnt)
        if not self.account_quota.expired_ts:
            return 0

        cnt = self.exported_companies.\
            with_entities(func.count(AccountExportedCompany.id))\
            .filter(and_(AccountExportedCompany.created_ts >= self.account_quota.start_ts,
                         AccountExportedCompany.created_ts <= self.account_quota.expired_ts))\
            .scalar()
        return cnt if cnt else 0

    @property
    def owned_segments(self):
        return self.all_owned_segments.filter(and_(Segment.is_deleted == false(),
                                                   Segment.status != Segment.EMPTY))

    @property
    def published_segments(self):
        return self.owned_segments.filter(Segment.published.is_(True))

    @property
    def fit_model_sfdc_seeds(self):
        model = self.latest_crm_fit_model
        if not model:
            return []
        return ModelSeed.get_by_id(model.model_seed_id).get_related_seed_domain_entries()

    @property
    def is_paying(self):
        return self.account_type == Account.PAYING

    @staticmethod
    def get_by_id(account_id):
        return Account.query.get(account_id)

    @staticmethod
    def get_by_crm_initialized(crm_data_initialized):
        accounts = Account.query.filter(Account.crm_data_initialized.is_(crm_data_initialized)).all()
        return [account.id for account in accounts]

    @staticmethod
    def get_datasync_initialized():
        return Account.get_by_crm_initialized(True)

    @staticmethod
    def get_datasync_uninitialized():
        return Account.get_by_crm_initialized(False)

    @staticmethod
    def get_by_name(name):
        return Account.query.filter(Account.name == name).first()

    @staticmethod
    def get_primary_account_by_domain(domain):
        return Account.query.filter(Account.domain == domain, Account.is_primary == true(),
                                    Account.deleted == false(), Account.account_type == Account.PAYING).first()

    @staticmethod
    def create_account(name, account_type, domain='', is_primary=True, with_quota=False):
        assert is_not_blank(name), "Account name invalid"
        assert account_type in Account.account_type_enums, "Account type invalid"

        account = Account.get_by_name(name)
        if account is None:
            account = Account(
                name=name,
                account_type=account_type,
                domain=domain,
                is_primary=is_primary
            )
            if with_quota:
                account.account_quota = AccountQuota.make_trial_account_quota()
            db.session.add(account)
            db.session.commit()
        return account

    @staticmethod
    def create_trial_account(account_name):
        account = Account.create_account(account_name, Account.TRIAL)
        if account.account_quota is None:
            account.account_quota = AccountQuota.make_trial_account_quota()
        db.session.add(account)
        db.session.commit()

        return account

    def set_data_sync_initialized(self):
        """

        Parameters
        ----------
        @type new_attrs: dict
        """
        setattr(self, 'crm_data_initialized', True)
        db.session.add(self)
        db.session.commit()

    def update_real_time_scoring_object(self, scoring_object):
        """
        Parameters
        ----------
        @type new_attrs: dict
        """
        setattr(self, 'realtime_scoring_object', json.dumps(scoring_object))
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def generate_name_by_email(email):
        return 'Guest_' + email

    @property
    def serialized(self):
        return {
            'id': self.id,
            'name': self.name,
            'accountType': self.account_type,
            'domain': self.domain,
            'isPrimary': self.is_primary,
            'totalCreatedAudiences': len(self.all_owned_audiences()),
            'realtimeScoring': self.realtime_scoring_mode,
            'inboundLeadsTarget': json.loads(self.realtime_scoring_object),
            'quotas': self.account_quota.serialized if self.account_quota else {}
        }

    def update(self, new_attrs):
        """

        Parameters
        ----------
        @type new_attrs: dict
        """
        permitted_fields = ['name', 'account_type', 'domain', 'realtime_scoring']

        for field in permitted_fields:
            old_value = getattr(self, field)
            new_value = new_attrs.get(inflection.camelize(field, False), old_value)
            setattr(self, field, new_value)

        db.session.add(self)
        db.session.commit()

    def add_user(self, user):

        user_invitation = UserInvitation.get_by_account_user_id(self.id, user.id)

        new_user_info = {"status": User.ACTIVE, "accountId": self.id}
        if user_invitation:
            new_user_info["firstName"] = user_invitation.first_name or user.first_name
            new_user_info["lastName"] = user_invitation.last_name or user.last_name
            new_user_info["phone"] = user_invitation.phone or user.phone

        # assign to new account
        user.update(new_user_info)

        # reset user's role to regular user.
        role = Role.get_by_id(user_invitation.role_id) if user_invitation else Role.get_by_name(Role.ACCOUNT_USER)
        UserRole.reset_role_for_user(user, [role])
        UserInvitation.delete_by_account_user(self.id, user.id)

        # re-publish published segments in this account to this new user
        Segment.publish_segments_to_user(
            self.published_segments,
            user
        )

    @staticmethod
    def delete_by_id(account_id):
        Account.query.filter(Account.id == account_id).delete()
        db.session.commit()

    def mark_to_deleted(self):
        self.deleted = True
        db.session.commit()

    @property
    def segment_cnt_quota_not_reached(self):
        quota = self.account_quota
        return quota.used_segment_cnt < quota.segment_cnt

    @property
    def audience_cnt_quota_not_reached(self):
        quota = self.account_quota
        return quota.used_audience_cnt < quota.segment_cnt

    def add_exported_companies(self, company_ids):
        AccountExportedCompany.create_exports(self.id, company_ids)

    @property
    def exported_contact_cnt(self):
        return self.exported_contacts.with_entities(func.count(AccountExportedContact.id)).scalar()

    @property
    def exported_contacts_ids(self):
        return [item[0] for item in
                self.exported_contacts.with_entities(AccountExportedContact.contact_id)]

    @staticmethod
    def get_contact_ids_set(account, contact_ids):
        contact_ids_set = set(contact_ids)
        exist_contact_ids = account.exported_contacts_ids
        exist_contact_ids_set = set(exist_contact_ids)
        new_contact_set = contact_ids_set - exist_contact_ids_set
        return new_contact_set, exist_contact_ids_set

    @staticmethod
    def get_company_ids_set(account, company_ids):
        start = datetime.datetime.utcnow()
        company_ids_set = set(company_ids)
        exist_company_ids = Account.get_exported_companies_ids(account, company_ids)
        logger.info("Get exist company ids Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))
        exist_company_ids_set = set(exist_company_ids)
        new_company_set = company_ids_set - exist_company_ids_set
        return new_company_set, exist_company_ids_set

    @staticmethod
    def get_exported_companies_ids(account, company_ids):
        exported_companies_ids = AccountExportedCompany.query\
                                                       .filter(AccountExportedCompany.account_id == account.id)\
                                                       .filter(AccountExportedCompany.company_id.in_(company_ids))\
                                                       .with_entities(AccountExportedCompany.company_id).all()
        return map(lambda id_tuple: id_tuple[0], exported_companies_ids)

    def __repr__(self):
        class_name = self.__class__.__name__

        return '<%s %s>' % (class_name, self.serialized)

    def can_run_account_fit_model(self):
        return self.account_type != Account.TRIAL and not self.deleted

    def create_crm_fit_model(self, seed_domains):
        self.batch_create_models(seed_domains, [Model.ACCOUNT_FIT])

    def create_fit_model(self, model_name, seed_companies):

        assert seed_companies, "Invalid seedDomains"
        assert model_name, "Invalid modelName"

        batch_id = str(uuid4())
        model_seed = ModelSeed.create_seed_from(self.id, seed_companies)

        model = Model.create_model(model_name, self.id,
                                   Model.FIT_MODEL, batch_id, Model.INIT, model_seed.id)
        return model

    def batch_create_models(self, seed_domains, models, owner_id=0):

        if not seed_domains:
            raise exceptions.BadRequest("No seed domains found. Please try increasing your date range.")

        min_required_domains = current_app.config.get("MIN_SEED_CSV_ROW_COUNTS", 50)
        if len(seed_domains) < min_required_domains:
            raise exceptions.BadRequest("Not enough seed domains found. Please try increasing your date range.")

        assert models, "Invalid models"

        type_set = set([item.type for item in models])
        assert not (type_set - Model.model_types), "Unsupported model types"

        batch_id = str(uuid4())
        model_seed = ModelSeed.create_seed_from(self.id, seed_domains)

        result = []
        for model in models:
            model = Model.create_model(
                model.name, self.id,
                model.type, batch_id, Model.INIT, model_seed.id,
                owner_id=owner_id)

            result.append(model)
        return result

    def fit_model_finished(self):
        return self.latest_completed_fit_job is not None

    def upload_sf_seeds(self, user, start_date, end_date):

        accounts = NSSalesforce.query_accounts_from_opportunities(
            user,
            start_date,
            end_date,
            current_app.config.get("MAX_SEED_CSV_ROW_COUNTS"))

        self.batch_create_models(
            _transform_2_seed_company(accounts),
            [Model(type=Model.ACCOUNT_FIT, name="CRM Fit"),
             Model(type=Model.FEATURE_ANALYSIS, name="account_%s_%s" % (Model.FEATURE_ANALYSIS, self.id))],
            owner_id=user.id)

        return

    def all_owned_audiences(self, meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE)):
        from ns.model.audience import Audience
        return db.session.query(Audience)\
            .select_from(Account)\
            .join(User, Account.id == User.account_id)\
            .join(Audience, User.id == Audience.user_id)\
            .filter(Account.id == self.id, Audience.is_deleted.is_(False), Audience.meta_type == meta_type)\
            .order_by(Audience.created_ts.desc())\
            .all()


def _transform_2_seed_company(sf_accounts):
    result = []
    for sf_account in sf_accounts:
        seed = SeedCompany(
            domain=parse_domain(sf_account["Website"]),
            sf_account_id=sf_account["Id"],
            sf_account_name=sf_account["Name"]
        )
        result.append(seed)
    return result
