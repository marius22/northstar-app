import datetime
import json

from ns.model import db
from ns.model.base_model import BaseModel


class AudienceKeywords(db.Model):

    __tablename__ = 'audience_keywords'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    audience_id = db.Column(db.Integer, db.ForeignKey("audience.id"), nullable=False)
    keywords = db.Column(db.Text, nullable=False)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @property
    def serialized(self):
        return {
            "id": self.id,
            "audienceId": self.audience_id,
            "keywords": self.keywords_obj
        }

    @property
    def keywords_obj(self):
        assert self.keywords, "Invalid keywords"
        return json.loads(self.keywords)

    @staticmethod
    def get_by_audience_id(audience_id):
        return AudienceKeywords.query \
            .filter(AudienceKeywords.audience_id == audience_id) \
            .first()

    @staticmethod
    def save_keywords(audience_id, keywords_list):
        keywords = AudienceKeywords.get_by_audience_id(audience_id)
        if not keywords:
            keywords = AudienceKeywords(
                audience_id=audience_id,
                keywords=json.dumps(keywords_list)
            )
            db.session.add(keywords)
        else:
            keywords.keywords = json.dumps(keywords_list)

        db.session.commit()
        return keywords
