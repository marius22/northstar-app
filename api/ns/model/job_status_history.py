"""
Job Status History model
"""
from ns.model import db
import datetime


class JobStatusHistory(db.Model):
    __tablename__ = 'job_status_history'

    STATUS_CREATED = 'JobCreated'
    STATUS_SUBMITION_ENQUEUE = "JobSubmitionInQueued"
    STATUS_SUBMITION_DEQUEUED = "JobSubmitionDeQueued"
    STATUS_SUBMITTED = "JobSubmitted"
    STATUS_RUNNING_QUEUED = "JobRunningQueued"
    STATUS_RUNNING = "JobRunning"
    SEGMENT_FINISHED = "SegmentfitFinished"
    FEATURE_ANALYSIS_FINISHED = "FeatureAnalysisFinished"
    STATUS_FINISHED = "JobFinished"
    STATUS_FAILED = "JobFailed"
    STATUS_DATA_SYNC_STARTED = "DataSyncStarted"
    STATUS_DATA_SYNC_FAILED = "DataSyncFailed"
    STATUS_DATA_SYNC_FINISHED = "DataSyncFinished"
    status_enums = (STATUS_CREATED,
                    STATUS_SUBMITION_ENQUEUE,
                    STATUS_SUBMITION_DEQUEUED,
                    STATUS_SUBMITTED,
                    STATUS_RUNNING_QUEUED,
                    STATUS_RUNNING,
                    STATUS_FINISHED,
                    STATUS_FAILED,
                    STATUS_DATA_SYNC_STARTED,
                    STATUS_DATA_SYNC_FAILED,
                    STATUS_DATA_SYNC_FINISHED)

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    job_id = db.Column(db.Integer, db.ForeignKey("job.id"), nullable=False)
    job_run_id = db.Column(db.String(255), nullable=False)
    job_run_type = db.Column(db.String(255), nullable=False)
    job_run_name = db.Column(db.String(255), nullable=False)
    status = db.Column(db.String(255), nullable=False)

    @staticmethod
    def create_job_history(job_id, status, job_run_id, job_run_name, job_run_type):
        job_history = JobStatusHistory(
            job_id=job_id,
            status=status,
            job_run_id=job_run_id,
            job_run_name=job_run_name,
            job_run_type=job_run_type)
        db.session.add(job_history)
        db.session.commit()
