import datetime
from ns.model import db
from ns.model.base_model import BaseModel
from ns.util.string_util import generate_hash_str
from ns.util.company_db_client import CompanyDBClient
from ns.util.string_util import none_to_empty
from ns.util.redshift_client import RedshiftClient
from ns.util.domain_util import parse_domain


def _valid_and_dedupe(model_seed_id, seed_companies):

    mappings = {}
    for item in seed_companies:
        item.model_seed_id = model_seed_id
        if not item.domain:
            item.domain = SeedCompany.UNKNOWN_DOMAIN_PREFIX + generate_hash_str("")
        else:
            item.domain = parse_domain(item.domain.lower())
        item.company_name = none_to_empty(item.company_name)
        item.sf_account_id = none_to_empty(item.sf_account_id)
        item.sf_account_name = none_to_empty(item.sf_account_name)

        mappings[item.domain] = item

    return mappings.values()


def _enrich_seed_domains(seed_domains):
    if seed_domains:
        domains = [item.domain for item in seed_domains if item.domain]
        db_client = CompanyDBClient.instance()
        profiles = db_client.get_enrichment_by_domains(domains)
        for seed in seed_domains:
            profile = profiles.get(seed.domain)
            if profile:
                seed.company_name = profile.get('name')

    return seed_domains


class SeedCompany(db.Model, BaseModel):

    __tablename__ = 'seed_company'
    __bind_key__ = 'redshift'

    UNKNOWN_DOMAIN_PREFIX = "unknown_"

    model_seed_id = db.Column(db.Integer, primary_key=True, nullable=False)
    domain = db.Column(db.String(100), primary_key=True, nullable=False)

    company_name = db.Column(db.String(255), nullable=True)
    sf_account_id = db.Column(db.String(255), nullable=True)
    sf_account_name = db.Column(db.String(255), nullable=True)

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @staticmethod
    def delete_by_model_seed_id(model_seed_id):
        SeedCompany.query.filter(SeedCompany.model_seed_id == model_seed_id).delete()
        db.session.commit()

    @staticmethod
    def get_by_model_seed_id(model_seed_id):
        assert model_seed_id, "Invalid modelSeedId"
        return SeedCompany.query\
            .filter(SeedCompany.model_seed_id == model_seed_id) \
            .all()

    @staticmethod
    def get_by_domains(domains):
        if not domains:
            return []
        return SeedCompany.query \
            .filter(SeedCompany.domain.in_(domains)) \
            .all()

    @staticmethod
    def _insert_seed_domains(seed_companies):

        now = datetime.datetime.utcnow()
        data = [
            {
                "model_seed_id": item.model_seed_id,
                "domain": item.domain,
                "company_name": item.company_name,
                "sf_account_id": item.sf_account_id,
                "sf_account_name": item.sf_account_name,
                "created_ts": now,
                "updated_ts": now
            } for item in seed_companies]

        keys = data[0].keys()
        values = [tuple(x.values()) for x in data]
        RedshiftClient.instance().insert("seed_company", keys, values)

        return

    @staticmethod
    def save_seed_companies(model_seed_id, seed_companies):

        assert seed_companies, "Invalid seedCompanies"
        assert model_seed_id, "Invalid modelSeedId"

        # parse domain, valid data and de-dupe
        seed_companies = _valid_and_dedupe(model_seed_id, seed_companies)
        # enrich
        seed_companies = _enrich_seed_domains(seed_companies)

        SeedCompany._insert_seed_domains(seed_companies)

        return seed_companies

    @staticmethod
    def query_seed_domains(**kwargs):
        model_seed_id = kwargs.get("model_seed_id", 0)
        limit = kwargs.get("limit", 100)
        offset = kwargs.get("offset", 0)
        criterion = kwargs.get("criterion")
        type = kwargs.get("type")

        query = SeedCompany.query.filter(SeedCompany.model_seed_id == model_seed_id)

        if type == 'unknown':
            query = query.filter(SeedCompany.domain.like(SeedCompany.UNKNOWN_DOMAIN_PREFIX + "%"))
        else:
            query = query.filter(SeedCompany.domain.notlike(SeedCompany.UNKNOWN_DOMAIN_PREFIX + "%"))

        page = query.order_by(criterion).paginate(offset / limit + 1, limit, False)
        return page.total, page.items

    @staticmethod
    def _maps(this):
        return {
            'modelSeedId': this.model_seed_id,
            'companyName': this.company_name,
            'domain': this.domain,
            "SFDCAccountId": this.sf_account_id,
            "SFDCAccountName": this.sf_account_name
        }

    @property
    def serialized(self):
        return self.__class__._maps(self)

    @classmethod
    def get_field_by_key(cls, key):
        return cls._maps(cls).get(key)
