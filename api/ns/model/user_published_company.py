import datetime
import logging

from ns.model import db
from ns.model.base_model import BaseModel
from ns.util.redshift_client import RedshiftClient

logger = logging.getLogger(__name__)


class UserPublishedCompany(db.Model, BaseModel):
    __bind_key__ = 'redshift'
    __tablename__ = 'user_published_company'

    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    audience_id = db.Column(db.Integer, primary_key=True, nullable=False)
    company_id = db.Column(db.Integer, index=True, primary_key=True, nullable=False)
    user_id = db.Column(db.Integer)
    target = db.Column(db.SmallInteger)

    ACCEPT_EXPORT_TYPE = {'CSV': 0, 'CRM': 1, 'marketo': 2, "marketo_sandbox": 3}

    @staticmethod
    def count_by_audience(audience_id):
        where_clause = {'audience_id': audience_id}
        fields = ['count(distinct company_id)']
        row = RedshiftClient.instance().select_one('user_published_company', fields, where_clause)
        return row[0]

    @staticmethod
    def get_published_company_ids(audience_id, target):
        fields = ['company_id']
        where_clause = {'audience_id': audience_id, 'target': target}
        rows = RedshiftClient.instance().select('user_published_company', fields, where_clause)
        return [row[0] for row in rows]

    @staticmethod
    def upsert_user_published_companies(user_id, audience_id, company_ids_to_download, export_type):
        export_type = set(export_type)
        assert export_type <= set(UserPublishedCompany.ACCEPT_EXPORT_TYPE.keys())
        for t in export_type:
            UserPublishedCompany.__upsert_user_published_companies(user_id, audience_id, company_ids_to_download,
                                                                   UserPublishedCompany.ACCEPT_EXPORT_TYPE[t])

    @staticmethod
    def __upsert_user_published_companies(user_id, audience_id, company_ids_to_download, target):
        company_ids = set(company_ids_to_download)
        # deduct account quota
        published_company_ids = set(UserPublishedCompany.get_published_company_ids(audience_id, target))
        new_company_ids = list(company_ids - published_company_ids)
        UserPublishedCompany.__add_published_companies(user_id, audience_id, new_company_ids, target)

        re_download_company_ids = published_company_ids.intersection(company_ids)
        UserPublishedCompany.__update_published_companies(audience_id, re_download_company_ids, target)

    @staticmethod
    def __add_published_companies(user_id, audience_id, company_ids, target):
        if not company_ids:
            return
        values = []
        for company_id in company_ids:
            values.append(
                (user_id, audience_id, company_id, target, datetime.datetime.utcnow(), datetime.datetime.utcnow())
            )
        try:
            columns = ['user_id', 'audience_id', 'company_id', 'target', 'created_ts', 'updated_ts']
            RedshiftClient.instance().insert("user_published_company", columns, values)
        except Exception as e:
            logger.error(e.message)

    @staticmethod
    def __update_published_companies(audience_id, company_ids, target):
        if company_ids:
            set_clause = {'updated_ts': str(datetime.datetime.utcnow())}
            where_clause = {'audience_id': audience_id, 'target': target, 'company_id': list(company_ids)}
            RedshiftClient.instance().update('user_published_company', set_clause, where_clause)
