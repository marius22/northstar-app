import calendar
import datetime
import json
from marshmallow.fields import Field
from ns.model import db


class MASFieldMapping(db.Model):

    __tablename__ = "mas_field_mapping"

    FIELDS_TYPE_COMMON = "standard"
    FIELDS_TYPE_AUDIENCE = "audience"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    account_id = db.Column(db.Integer, nullable=True)
    mas_type = db.Column(db.String(255), nullable=False)
    mapping = db.Column(db.String(2048), nullable=False)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    @staticmethod
    def save_mapping(account_id, mas_type, mapping):
        Mapping.check_format(mapping)
        mas_field_mapping_in_db = MASFieldMapping.get_one_by_account_id_mas_type(account_id, mas_type)
        if not mas_field_mapping_in_db:
            mas_field_mapping = MASFieldMapping(account_id=account_id,
                                                mas_type=mas_type,
                                                mapping=json.dumps(mapping))
            db.session.add(mas_field_mapping)
            db.session.commit()
            return mas_field_mapping
        else:
            mas_field_mapping_in_db.update_mapping(account_id, mas_type, json.dumps(mapping))
            return mas_field_mapping_in_db

    def update_mapping(self, account_id, mas_type, mapping):
        self.account_id = account_id
        self.mas_type = mas_type
        self.mapping = mapping
        db.session.commit()
        return self

    @staticmethod
    def get_one_by_account_id_mas_type(account_id, mas_type):
        return MASFieldMapping.query.filter(MASFieldMapping.account_id == account_id,
                                            MASFieldMapping.mas_type == mas_type).first()

    @staticmethod
    def get_by_account_id_mas_type(account_id, mas_type):
        return MASFieldMapping.query.filter(MASFieldMapping.account_id == account_id,
                                            MASFieldMapping.mas_type == mas_type).all()

    @property
    def serialized(self):
        return {
            "id": self.id,
            "account_id": self.account_id,
            "mas_type": self.mas_type,
            "mapping": json.loads(self.mapping),
            "updatedTs": calendar.timegm(self.updated_ts.utctimetuple())
        }


class Mapping(Field):
    def deserialize(self, value, attr=None, data=None):
        return value

    @staticmethod
    def check_format(mapping):
        assert isinstance(mapping, dict), "invalid format of mapping"
