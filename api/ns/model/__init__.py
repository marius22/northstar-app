"""
    Northstar app api - models
"""
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from werkzeug.exceptions import NotFound


class AnnotatedBaseQuery(BaseQuery):
    """
    Customized query class
    """

    def get_or_404(self, ident):
        """
        Raise NotFound exception with more descriptive info
        """
        rv = self.get(ident)

        if not rv:
            entity_type = self._entity_zero().type
            raise NotFound('%s(ID = %d) is not found.' % (entity_type.__name__, ident))
        else:
            return rv


db = SQLAlchemy(session_options={'expire_on_commit': False})

__all__ = [db, AnnotatedBaseQuery]
