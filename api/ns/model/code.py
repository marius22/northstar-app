"""
Code model
"""
import datetime

from ns.model import db
from ns.util.string_util import is_not_blank
from ns.util.string_util import none_to_empty


class Code(db.Model):
    __tablename__ = 'code'

    # definitions for code.code_type
    EMAIL_CODE = 1
    PASSWORD_CODE = 2
    INVITATION_CODE = 3

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    code = db.Column(db.String(255), unique=True, nullable=False)
    value = db.Column(db.String(255), nullable=False, default="")
    code_type = db.Column(db.SmallInteger, nullable=False)
    expired_ts = db.Column(db.TIMESTAMP, nullable=True)

    @staticmethod
    def get_by_code(code):
        return Code.query.filter(Code.code == code).first()

    @staticmethod
    def remove_by_email_code_type(email, code_type):
        Code.query.filter(Code.value == email, Code.code_type == code_type)\
            .delete()
        db.session.commit()

    @staticmethod
    def create_code(code_str, value, code_type, expired_ts=None):

        assert is_not_blank(code_str), "Code invalid"
        assert code_type in (Code.EMAIL_CODE, Code.PASSWORD_CODE,
                             Code.INVITATION_CODE), "CodeType invalid"

        code = Code.get_by_code(code_str)
        if code is None:
            code = Code(
                code=code_str,
                value=none_to_empty(value),
                code_type=code_type,
                expired_ts=expired_ts
            )
            db.session.add(code)
        else:
            code.value = value
            code.code_type = code_type
            code.expired_ts = expired_ts
        db.session.commit()
        return code

    def hash_expired(self):
        return self.expired_ts and datetime.datetime.utcnow() > self.expired_ts
