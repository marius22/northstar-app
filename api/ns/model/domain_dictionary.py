"""
Invalid domain model
"""
from ns.model import db


class DomainDictionary(db.Model):
    __tablename__ = 'domain_dictionary'

    # definitions for code.code_type
    FREE_EMAIL_DOMAIN = 1
    COMPETITOR_DOMAIN = 2

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    domain = db.Column(db.String(255), nullable=False, default="")
    domain_type = db.Column(db.SmallInteger, nullable=False, default=FREE_EMAIL_DOMAIN)

    @staticmethod
    def get_by_type(domain_type):
        return DomainDictionary.query \
            .filter(DomainDictionary.domain_type == domain_type).all()

    @staticmethod
    def get_by_domain_and_type(domain, domain_type):
        return DomainDictionary.query.filter(DomainDictionary.domain == domain,
                                             DomainDictionary.domain_type == domain_type).first()

    @staticmethod
    def is_free_domain(domain):
        return DomainDictionary.get_by_domain_and_type(
            domain,
            DomainDictionary.FREE_EMAIL_DOMAIN) is not None

    @staticmethod
    def is_competitor_domain(domain):
        return DomainDictionary.get_by_domain_and_type(
            domain,
            DomainDictionary.COMPETITOR_DOMAIN) is not None

    @staticmethod
    def get_valid_email_domains(domains):
        invalid_email_domains = DomainDictionary.query.filter(DomainDictionary.domain.in_(domains))\
            .with_entities(DomainDictionary.domain).all()
        invalid_email_domains = map(lambda domain_tuple: domain_tuple[0], invalid_email_domains)
        valid_email_domains = filter(lambda domain: domain not in invalid_email_domains, domains)
        return valid_email_domains
