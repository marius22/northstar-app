from ns.util.redshift_client import RedshiftClient


class CrmAccount(object):

    @staticmethod
    def count_companies_by_org_id(org_id):
        if not org_id:
            return 0

        return RedshiftClient.instance().query('''
          select count(distinct domain)
          from northstar.crm_account
          where org_id = '%s';
        ''' % org_id)[0][0]
