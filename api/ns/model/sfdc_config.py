import logging
from ns.model import db
import datetime
import time
import json
from marshmallow.fields import Field
logger = logging.getLogger(__name__)


class SFDCConfig(db.Model):

    __tablename__ = 'sfdc_config'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"), nullable=False)
    config = db.Column(db.Text, nullable=True)

    enrich_enabled = db.Column(db.Boolean, nullable=True, default=False)
    enrich_enabled_updated_status_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    enrich_start_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    enrich_score = db.Column(db.Boolean, nullable=True, default=False)
    enrich_firmographics = db.Column(db.Boolean, nullable=True, default=False)

    DEFAULT_CONFIG = {
        "showTopInsights": True,
        "allowCRMAdd": True,
        "showAudienceScore": True,
        "showCompanyKeywords": True,
        "allowKeywordsAddAccounts": True,
        # Allow the user to add a company as a Lead
        "allowCRMAddLeads": True,
        # Allow the user to add a company as an Account
        "allowCRMAddAccounts": True,

        # Allow the user to add a person as a Lead
        "allowCRMAddPeopleToLeads": True,
        # Allow the user to add a person as a Contact
        "allowCRMAddPeopleToContacts": True,

        "showInsightsKeywords": True,
        "showAudience": True,
        "insightHeadersCustom": [],
        "insightHeaders": [
            {"value": "Industry", "key": "industry"},
            {"value": "Employee Size", "key": "employeeSize"},
            {"value": "Overall Fit Score", "key": "score"}
        ],
        "showNotInCRM": True,
        "showContacts": True,
        "allowKeywordsAdd": True,
        "segmentInsightsBlackList": [],
        "showCustomInsights": True,
        "allowKeywordsAddLeads": True,
        "showInCRM": True
    }

    @property
    def config_dict(self):
        assert self.config, "Invalid config content"
        return json.loads(self.config)

    @property
    def can_add_to_crm(self):
        config_dict = self.config_dict
        return config_dict.get("showNotInCRM") and config_dict.get("allowCRMAdd")

    @property
    def scoring_config_dict(self):
        result = {"enrich_enabled": self.enrich_enabled,
                  "enrich_score": self.enrich_score,
                  "enrich_firmographics": self.enrich_firmographics,
                  "enrich_start_ts": str(self.enrich_start_ts),
                  "enrich_enabled_updated_status_ts": self.enrich_enabled_updated_status_ts
                  }
        return result

    @staticmethod
    def create_default_config_for_account(account_id):
        config = SFDCConfig(
            account_id=account_id,
            config=json.dumps(SFDCConfig.DEFAULT_CONFIG),
            enrich_enabled=False,
            enrich_score=False,
            enrich_firmographics=False,
            enrich_start_ts=datetime.datetime.utcfromtimestamp(time.time())
        )
        db.session.add(config)
        db.session.commit()
        return config

    @staticmethod
    def get_by_account_id(account_id):

        assert account_id, "Invalid accountId"
        config = SFDCConfig.query.filter(SFDCConfig.account_id == account_id).first()
        if not config:
            config = SFDCConfig.create_default_config_for_account(account_id)
        return config

    @staticmethod
    def get_enrich_enable_accounts():
        return SFDCConfig.query.filter(SFDCConfig.enrich_enabled.is_(True)).all()

    @staticmethod
    def save_config(account_id, config_dict, enrich_enabled,
                    enrich_score, enrich_firmographics, enrich_start_ts):

        assert config_dict, "Invalid SFDC config"

        config = SFDCConfig.get_by_account_id(account_id)
        if not config:
            config = SFDCConfig(
                account_id=account_id,
                config=json.dumps(config_dict),
                enrich_enabled=enrich_enabled,
                enrich_score=enrich_score,
                enrich_firmographics=enrich_firmographics,
                enrich_start_ts=enrich_start_ts
            )
            db.session.add(config)
        else:
            config.config = json.dumps(config_dict)
            config.enrich_enabled = enrich_enabled
            config.enrich_score = enrich_score
            config.enrich_firmographics = enrich_firmographics
            config.enrich_start_ts = enrich_start_ts
        db.session.commit()

        return config

    def get_black_list(self):
        result = []
        black_list = self.config_dict.get("segmentInsightsBlackList")
        if black_list:
            for item in black_list:
                result.append(item["key"])
        return result

    def get_header_custom_keys(self):
        result = []
        custom_headers = self.config_dict.get("insightHeadersCustom")
        if custom_headers:
            for item in custom_headers:
                result.append(item["key"])
        return result


class SFDCConfigField(Field):
    def deserialize(self, value, attr=None, data=None):
        return value
