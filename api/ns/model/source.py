"""
Source Model
"""
from ns.model import db
import datetime


class Source(db.Model):
    __tablename__ = 'source'

    CSV = "CSV"
    SALESFORCE = "SALESFORCE"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = db.Column(db.TIMESTAMP, default=datetime.datetime.utcnow,
                           onupdate=datetime.datetime.utcnow)
    source_name = db.Column(db.String(255), unique=True, nullable=False)

    @staticmethod
    def get_by_name(name):
        """
        Query a source by name.
        :param name:
        :return:
        """
        return Source.query.filter(Source.source_name == name).first()
