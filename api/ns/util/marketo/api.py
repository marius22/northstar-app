# -*- coding: utf-8 -*-
import requests
import logging
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)


class Marketo(object):

    def __init__(self, client_id, client_secret, endpoint):
        self.session = requests.Session()
        self.client_id = client_id
        self.client_secret = client_secret
        self.endpoint = endpoint

        self._token = None
        self._token_expires = datetime.now() - timedelta(0, 60)

    def authenticate(self):
        args = {
            'grant_type': 'client_credentials',
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }
        self.session.headers.clear()
        resp = self.session.get(self.endpoint + "/identity/oauth/token", params=args)
        return resp.json()["access_token"], resp.json()["expires_in"]

    def set_token(self):
        if not getattr(self, '_token', None) or self._token_expires < datetime.now():
            token, expires_in = self.authenticate()
            self._token = token
            self._token_expires = datetime.now() + timedelta(0, expires_in)
            self.session.headers.update({"Authorization": "Bearer {}".format(self._token)})
        return self._token

    def get_list(self, batch_size=300):
        self.set_token()
        result_list = list()
        params = {"batchSize": batch_size}
        while True:
            res = self.session.get(self.endpoint + "/rest/v1/lists.json", params=params)
            result = res.json()
            result_list.extend(result['result'])
            if len(result['result']) < batch_size or 'nextPageToken' not in result:
                break
            next_page_token = result["nextPageToken"]
            params["nextPageToken"] = next_page_token
        result_list = sorted(result_list, key=lambda record: record.get("name", "").lower())
        return result_list

    def get_lead_schema(self):
        self.set_token()
        resp = self.session.get(self.endpoint + "/rest/v1/leads/describe.json")
        return resp.json().get("result", [])

    def upsert_leads(self, leads):
        self.set_token()
        url = self.endpoint + "/rest/v1/leads.json"
        data = {
            "action": "createOrUpdate",
            "lookupField": "email",
            "input": leads
        }
        self.session.post(url, data=data)

    def add_leads_to_list(self, list_id, lead_ids):
        self.set_token()
        url = self.endpoint + "/rest/v1/lists/{listId}/leads.json".format(listId=list_id)
        ids = [{"id": one} for one in lead_ids]
        data = {"input": ids}
        self.session.post(url, data=data)
