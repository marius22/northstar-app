import math


def distinct_count(data):
    dis_count = {}
    for row in data:
        for item in row:
            n = dis_count.get(item)
            if n:
                dis_count[item] = n + 1
            else:
                dis_count[item] = 1

    return dis_count


def split_list(l, size):
    """Yield successive n-sized chunks from list l."""
    temp_l = list(l)
    for i in range(0, len(temp_l), size):
        yield temp_l[i:i + size]


def get_batch_lists(origin_list, batch_size):
    assert isinstance(batch_size, int), "Invalid value"
    result_list = list()
    if not origin_list:
        return result_list
    total = len(origin_list)
    n = int(math.ceil(total * 1.0 / batch_size))
    for i in range(n):
        start = i * batch_size
        end = min((i + 1) * batch_size, total)
        result_list.append(origin_list[start:end])
    return result_list
