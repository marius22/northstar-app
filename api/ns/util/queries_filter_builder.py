

class FilterBuilder(object):

    @staticmethod
    def build_filter(key, value):
        pass


class InBuilder(FilterBuilder):

    @staticmethod
    def build_filter(key, value):
        assert isinstance(value, (list, set)), "Invalid value"

        if not value:
            return {}

        return {key: {"$in": list(value)}}


class OrBuilder(FilterBuilder):

    @staticmethod
    def build_filter(key, value):
        assert isinstance(value, (list, set)), "Invalid value"

        if not value:
            return {}

        filters = []
        for item in value:
            sub_filter = {}
            min_value = item.get("min")
            if min_value:
                sub_filter["$gt"] = int(min_value)
            max_value = item.get("max")
            if max_value:
                sub_filter["$lte"] = int(max_value)
            filters.append({key: sub_filter})
        return {"$or": filters}
