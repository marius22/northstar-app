import datetime
import json
import logging
import psycopg2
import time
from flask import current_app
import psycopg2.extensions


FORMAT = "%(asctime)-15s %(filename)s:%(lineno)s %(message)s"
logging.basicConfig(format=FORMAT)
logger = logging.getLogger(__name__)

# https://everstring.atlassian.net/browse/NSV2-570
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)


class RedshiftClient(object):
    # TODO: handle failures
    _instance = None
    conn_info = None

    def __init__(self):
        self.aws_access_key_id = current_app.config.get("AWS_ACCESS_KEY_ID")
        self.aws_secret_access_key = current_app.config.get("AWS_SECRET_ACCESS_KEY")

    def exec_sql(func):
        """
        Decorator for functions that execute sql commands
        """
        def wrapper(self, *args, **kwargs):
            # form connection string from connection info in config
            conn_info = current_app.config.get("PSYCOPG2_CONN_INFO")
            conn_str = ' '.join("%s=%s" % (key, value)
                                for key, value in conn_info.iteritems())

            # get connection and cursor
            # TODO: keep connection open
            self.conn = psycopg2.connect(conn_str)
            result = None
            try:
                self.cursor = self.conn.cursor()
                self.cursor.execute("SET search_path TO northstar")
                time.sleep(0.1)
                # run sql command and time the execution
                start = datetime.datetime.utcnow()
                logger.info("Starting %s" % func.__name__)
                result = func(self, *args, **kwargs)
                logger.info("Finished %s. Elapsed time: %s" %
                            (func.__name__, str(datetime.datetime.utcnow() - start)))
                # close connection
            finally:
                self.conn.close()
            return result

        return wrapper

    def _get_copy_s3_to_redshift_sql(self, table_name, bucket_name, s3_url, format='csv', columns=[], delimiter=',',
                                     ignoreheader=True, gzip=False, timestamp='YYYY-MM-DDTHH:MI:SS'):
        """
        :param: table_name (String)
        :param: columns (List[String]) the order of columns values are specified in the csv
        :param: bucket_name (String)
        :param: s3_url (String) in format dev/my_file
        """
        if format == 'json':
            options = ""
            if timestamp:
                options += "%s '%s' " % ("timeformat", timestamp)
            return "copy {0} from 's3://{1}/{2}' " \
                "credentials 'aws_access_key_id={3};aws_secret_access_key={4}' " \
                "format as json 'auto' " \
                "{5}" \
                "ENCODING UTF8;".format(
                    table_name, bucket_name, s3_url, self.aws_access_key_id, self.aws_secret_access_key, options)

        elif format == 'csv':
            options = ""
            if ignoreheader:
                options += "ignoreheader 1 "
            if gzip:
                options += "gzip "
            if timestamp:
                options += "%s '%s' " % ("timeformat", timestamp)

            return "copy {0} ({1}) from 's3://{2}/{3}' " \
                "credentials 'aws_access_key_id={4};aws_secret_access_key={5}' " \
                "delimiter as '{6}' " \
                "{7}" \
                "ENCODING UTF8 csv;".format(
                    table_name, ','.join(columns), bucket_name, s3_url, self.aws_access_key_id,
                    self.aws_secret_access_key, delimiter, options)

    def _get_insert_sql(self, table_name, fields, values):
        """
        :param: table_name (String) name of table to insert to
        :param: fields (List[String]) a list of column names
        :param: values (List[Tuple[String]])
            the inner tuple of strings are a single row that would be inserted.
            supports inserting multiple rows
        """
        if not values:
            return
        fields_ = ','.join(fields)
        # if values[0] has one element, its type is the type of the element
        if not isinstance(values[0], tuple) or len(values[0]) == 1:
            values_ = ','.join(['(%s)'] * len(values))
        else:
            values_ = ','.join(['%s'] * len(values))
        return "insert into {0} ({1}) values {2};".format(
            table_name, fields_, values_)

    @exec_sql
    def insert(self, table_name, fields, values):
        """
        :param table_name: (String) name of table to insert to
        :param fields: (List[String]) a list of column names
        :param values: (List[Tuple[String]])
            the inner tuple of strings are a single row that would be inserted.
            supports inserting multiple rows
        """
        sql = self._get_insert_sql(table_name, fields, values)
        self.cursor.execute(sql, values)
        self.conn.commit()

    def _get_select_sql(self, table_name, fields, where_clause={}):
        """
        :param: table_name (String) name of table to update
        :param: fields (List[String]) fields to be included in response
        :param: where_clause (Dict[Column:Value]) translates to WHERE Column = Value
        """
        fields_ = ','.join(fields)
        if where_clause:
            sql = "select {0} from {1} where {2};".format(
                fields_, table_name, self._where_clause_to_string(where_clause))
        else:
            sql = "select {0} from {1};".format(fields_, table_name)
        return sql

    @exec_sql
    def select_one(self, table_name, fields, where_clause={}):
        """
        :param table_name: (String) name of table to update
        :param fields: (List[String]) fields to be included in response
        :param where_clause: (Dict[Column:Value]) translates to WHERE Column = Value
        """
        sql = self._get_select_sql(table_name, fields, where_clause)
        self.cursor.execute(sql)
        return self.cursor.fetchone()

    def _where_clause_to_string(self, where_clause):
        """
        :param where_clause: (Dict[Column:Value]) translates to 'Column1 = Value1 and Column2 = Value2'
        :return:
        """
        clauses = []
        for key, value in where_clause.iteritems():
            if isinstance(value, list):
                clauses.append('%s in %s' % (key, self._map_type(value)))
            else:
                clauses.append('%s = %s' % (key, self._map_type(value)))
        return ' and '.join(clauses)

    def _map_type(self, val):
        if isinstance(val, str):
            return "'%s'" % val
        if isinstance(val, int):
            return str(val)
        if isinstance(val, long):
            return str(val)
        if isinstance(val, list):
            return "(%s)" % ','.join([self._map_type(v) for v in val])
        return val

    @exec_sql
    def copy_from_s3_to_redshift(self, table_name, bucket_name, s3_url, format='csv', columns=[], delimiter=',',
                                 ignoreheader=True, gzip=False, timestamp='YYYY-MM-DDTHH:MI:SS'):
        """
        :param: table_name (String)
        :param: columns (List[String]) the order of columns values are specified in the csv
        :param: bucket_name (String)
        :param: s3_url (String) in format dev/my_file
        """
        sql = self._get_copy_s3_to_redshift_sql(
            table_name, bucket_name, s3_url, format, columns, delimiter, ignoreheader, gzip, timestamp)
        self.cursor.execute(sql)
        self.conn.commit()

    @exec_sql
    def query(self, sql):
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    @exec_sql
    def bulk_update_using_temp_table(self, temp_table_sql, temp_table_name, fields, values, update_sql):
        # create temp table
        self.cursor.execute(temp_table_sql)
        # insert values to temp table
        sql = self._get_insert_sql(temp_table_name, fields, values)
        self.cursor.execute(sql, values)
        # join update
        self.cursor.execute(update_sql)
        self.conn.commit()

    @exec_sql
    def execute(self, sql):
        self.cursor.execute(sql)
        self.conn.commit()

    def _get_update_sql(self, table_name, set_clause, where_clause={}):
        """
        :param: table_name (String) name of table to update
        :param: set_clause (Dict[Column:Value]) translates to SET Column = (Value)
        :param: where_clause (Dict[Column:Value]) translates to WHERE Column = Value
        """
        set_ = ','.join(['%s = (%s)' % (key, self._map_type(value))
                         for key, value in set_clause.iteritems()])
        if where_clause:
            sql = "update {0} set {1} where {2};".format(
                table_name, set_, self._where_clause_to_string(where_clause))
        else:
            sql = "update {0} set {1}".format(table_name, set_)
        return sql

    @exec_sql
    def update(self, table_name, set_clause, where_clause={}):
        """
        :param: table_name (String) name of table to update
        :param: set_clause (Dict[Column:Value]) translates to SET Column = (Value)
        :param: where_clause (Dict[Column:Value]) translates to WHERE Column = Value
        """
        sql = self._get_update_sql(table_name, set_clause, where_clause)
        self.cursor.execute(sql)
        self.conn.commit()

    @exec_sql
    def select(self, table_name, fields, where_clause={}):
        """
        :param: table_name (String) name of table to update
        :param: fields (List[String]) fields to be included in response
        :param: where_clause (Dict[Column:Value]) translates to WHERE Column = Value
        """
        sql = self._get_select_sql(table_name, fields, where_clause)
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    @classmethod
    def instance(cls):
        """
        Lazy singleton
        """
        # if not cls._instance:
        #     cls._instance = cls()
        # return cls._instance
        return cls()
