import json

import arrow
from marshmallow.fields import Field, DateTime
from sqlalchemy import asc, desc
from ns.util.string_util import is_blank
from webargs.flaskparser import FlaskParser
from tldextract import tldextract


class OrderField(Field):
    def _deserialize(self, value, attr, data):
        return {
            'key': value.lstrip('-'),
            'direction': desc if value.startswith('-') else asc,
            'original_value': value
        }


class DictField(Field):
    def _deserialize(self, value, attr, data):
        if (isinstance(value, dict)):
            return value
        return json.loads(value)


class UTCNaiveDateTimeField(DateTime):
    """
    Naive datetime field to convert to UTF
    """

    def _deserialize(self, value, attr, data):
        dt = super(UTCNaiveDateTimeField, self)._deserialize(value, attr, data)
        return arrow.get(dt).to('UTC').naive


class NullableUTCNaiveDateTimeField(UTCNaiveDateTimeField):
    """
    Naive datetime field to convert to UTF
    """

    def _deserialize(self, value, attr, data):
        if is_blank(value):
            return None
        return super(NullableUTCNaiveDateTimeField, self)._deserialize(value, attr, data)


class DomainField(Field):
    def _deserialize(self, value, attr=None, data=None):
        ext = tldextract.extract(value)
        return ext.registered_domain
