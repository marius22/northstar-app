import smtplib
from smtplib import SMTPDataError, \
    SMTPSenderRefused, SMTPRecipientsRefused, SMTPHeloError
from flask import current_app
import logging
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders


logger = logging.getLogger(__name__)


class EmailClient(object):
    """
    common email client
    """

    CONNECTION_OK = 250

    def __init__(self, from_addr, username, password, smtp_server_addr, enable_email):
        super(EmailClient, self).__init__()

        self.from_addr = from_addr
        self.username = username
        self.password = password
        self.smtp_server_addr = smtp_server_addr
        self.server = smtplib.SMTP()
        self.enable_email = enable_email

    def _open_connection(self):
        for i in range(3):
            try:
                # whether need to retry connect
                self.server.connect(self.smtp_server_addr)
                self.server.ehlo()
                self.server.starttls()
                self.server.login(self.username, self.password)
                break
            except Exception, e:
                logger.error("Cannot connect to the email server, e %s" % e)
                continue
        else:
            raise EmailException("Cannot get a connection from server.")

    def _get_connection_status(self):
        try:
            return self.server.noop()[0]
        except:
            return -1

    def _reconnect_if_needed(self):

        if self._get_connection_status() == self.__class__.CONNECTION_OK:
            return
        else:
            self._open_connection()

    def _make_message(self, to, subject, content, attachments=[]):
        message = MIMEMultipart()
        body = MIMEText(content, 'html')
        message.attach(body)

        for attachment in attachments:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(attachment.get('content'))
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename="%s"' % attachment.get('file_name'))
            message.attach(part)

        message['Subject'] = subject
        message['From'] = self.from_addr
        if isinstance(to, list):
            message['To'] = ', '.join(to)
        else:
            message['To'] = to
        return message

    def send_mail(self, to, subject, content, attachments=[], bcc_emails=[]):

        self._reconnect_if_needed()

        for i in range(3):
            try:
                message = self._make_message(to, subject, content, attachments)
                # This email will be sent to all the `to_addrs`, but the `to` is the only one passed
                # to `_make_message`, and thus the only one that appears as a recipient in the "To"
                # email header.
                if isinstance(to, list):
                    to_addrs = to + bcc_emails
                else:
                    to_addrs = [to] + bcc_emails
                if self.enable_email:
                    result = self.server.sendmail(self.from_addr, to_addrs, message.as_string())
                else:
                    logger.info("Send mail: \n "
                                "*************************** \n"
                                "%s \n"
                                "***************************" % message)
                    result = None

                if not result:
                    break

                logger.error("Send email failed, msg: %s" % result)
                raise EmailException("Send email to %s failed" % ','.join(result.keys()))

            except SMTPSenderRefused, e:
                logger.error("Send email failed: from: %s, e: %s" % (self.from_addr, e))
                raise EmailException("Please check your sender %s" % self.from_addr)
            except SMTPRecipientsRefused, e:
                logger.error("Send email failed: to: %s, e: %s" % (to, e))
                raise EmailException("Please check your recipients %s" % self.to)
            except Exception, e:
                logger.warn("Send email failed, tried [%s] times, e: {%s} " % (i + 1, e))
                continue
        else:
            raise EmailException("Unknown exception when sending email.")


class NSEmailClient(EmailClient):
    """
    email client for ns
    """
    client = None

    @classmethod
    def _get_client(cls):

        if cls.client is None:
            fromaddr = current_app.config.get("EMAIL_CLIENT_FROMADDR")
            username = current_app.config.get("EMAIL_CLIENT_USERNAME")
            password = current_app.config.get("EMAIL_CLIENT_PASSWORD")
            smtp_server = current_app.config.get("EMAIL_CLIENT_SMTP_SERVER")
            enable_email = current_app.config.get("EMAIL_ENABLED", False)
            cls.client = EmailClient(fromaddr, username,
                                     password, smtp_server, enable_email)
        return cls.client

    @classmethod
    def send_email(cls, to, subject, content, attachments=[], bcc_emails=[]):
        """
        If too slow, can change it to async
        :param attachments:
        :param to:
        :param subject:
        :param content:
        :return:
        """
        cls._get_client().send_mail(to, subject, content, attachments, bcc_emails)

    @classmethod
    def send_mail_quietly(cls, to, subject, content, attachments=[]):
        try:
            cls._get_client().send_mail(to, subject, content, attachments)
        except EmailException, e:
            # log error
            logger.error("%s" % e.message)
            # send this error to data dog


class EmailException(Exception):
    pass
