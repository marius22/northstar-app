"""
Specialized JSON-oriented Flask App

Ref: http://flask.pocoo.org/snippets/83/
"""


from flask import jsonify
from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException
import logging

logger = logging.getLogger(__name__)


def make_json_app(app):
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "Method Not Allowed" }
    """

    def make_json_error(ex):
        """
        Exception handler for making it as a JSON
        """
        err_dict = {}

        if hasattr(ex, 'data'):
            # A dictionary of errors that webargs adds
            err_dict = getattr(ex.data.get('exc', {}), 'messages', {})

        if isinstance(ex, HTTPException):
            if err_dict:
                response = jsonify(
                    message=ex.description,
                    err_dict=err_dict
                )
            else:
                response = jsonify(
                    message=ex.description
                )
            response.status_code = ex.code
        else:
            logger.exception(ex)
            message = "Oops, something went wrong. Please contact support if this problem persists."
            response = jsonify(
                message=message
            )
            response.status_code = 500

        return response

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = make_json_error

    app.error_handler_spec[None][None] = [(Exception, make_json_error)]
    return app
