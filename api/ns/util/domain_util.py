"""
Utils for domain
"""
import tldextract
from validators import domain
import string
from ns.model import company_name_2_domain
from ns.model.domain_dictionary import DomainDictionary
from collections import defaultdict


def get_domain_from_email(email):
    if (not email) or '@' not in email:
        return ""
    return email.split('@')[1]


def is_domain(domain_str):
    if not domain_str:
        return False
    try:
        return domain(domain_str)
    except Exception:
        return False


def parse_domain(input_domain):
    if input_domain:
        input_domain = input_domain.lower().strip(u"\t\n\x0b\x0c\r \'\"")
        tld_extract = tldextract.extract(input_domain)
        try:
            output_domain = ".".join([tld_extract.domain, tld_extract.tld])
        except:
            output_domain = ".".join([tld_extract.domain, tld_extract.suffix])

        if output_domain in ['us.com', 'uk.com']:
            arr = input_domain.split(".")
            n = len(arr)
            if n > 2:
                output_domain = arr[n - 3] + '.' + output_domain
        return output_domain
    return ""


def get_batch_domain_matches(identifying_data):
    """
    Iterate through identifying_data and build up a list of domains to return. We also modify the identifying_data
    dictionaries in-place for use in our Enrichment REST API's output.

    :param identifying_data: A list of dictionaries, each dict having up to 3 keys: 'website', 'email', and/or 'name'.
                             We try to get a domain from each dict based on 'website', 'email', and 'name', in that
                             order of priority.

    :return: A list of domains that we've derived from identifying_data
    """

    domains = set()
    # We use defaultdict(list) here to account for how multiple info_dicts
    # may have the same name or email domain.
    c2d_names_to_info_dicts = defaultdict(list)
    email_domains_to_info_dicts = defaultdict(list)

    for info_dict in identifying_data:
        info_dict["matchedOn"] = None
        info_dict["matchedDomain"] = None
        # If we find a value for 'website', we just use that as the domain.
        website = info_dict.get('website')
        if website:
            info_dict["matchedOn"] = 'website'
            domain = parse_domain(website)
            info_dict["matchedDomain"] = domain
            domains.add(domain)
            continue
        # If we find a value for 'email', we create a email_domains_to_info_dicts entry.
        # We don't do anything else yet, because we want to filter out all invalid domains
        # in a single batch call later on.
        email = info_dict.get('email')
        if email:
            email_domain = get_domain_from_email(email)
            if email_domain:
                email_domains_to_info_dicts[email_domain].append(info_dict)
                continue
        # If we find a value for 'name', we create a c2d_names_to_info_dicts entry.
        # We don't do anything else yet, because, just like with emails, we want to do
        # a single batch call later on with all the names that we've gathered.
        name = info_dict.get('name')
        if name:
            c2d_names_to_info_dicts[name].append(info_dict)

    if email_domains_to_info_dicts:
        email_domains = email_domains_to_info_dicts.keys()
        valid_domains = DomainDictionary.get_valid_email_domains(email_domains)
        for email_domain in email_domains:
            info_dicts = email_domains_to_info_dicts[email_domain]
            for info_dict in info_dicts:
                if email_domain in valid_domains:
                    info_dict["matchedOn"] = 'email'
                    info_dict["matchedDomain"] = email_domain
                    domains.add(email_domain)
                else:
                    # If email_domain is invalid, we fall back to 'name' if possible.
                    name = info_dict.get('name')
                    if name:
                        c2d_names_to_info_dicts[name].append(info_dict)

    if c2d_names_to_info_dicts:
        c2d_names = c2d_names_to_info_dicts.keys()
        c2d_names_to_domains = company_name_2_domain.fetch_domains_dict_by_company_names(c2d_names)
        for name, domain in c2d_names_to_domains.iteritems():
            info_dicts = c2d_names_to_info_dicts.get(name, [])
            for info_dict in info_dicts:
                info_dict["matchedOn"] = 'name'
                info_dict["matchedDomain"] = domain
            domains.add(domain)

    return list(domains)


def get_domain_match(info_dict):
    domain_match = get_batch_domain_matches([info_dict])
    return domain_match[0] if domain_match else None
