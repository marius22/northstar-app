"""
Some utils for file name operations
"""
import os
from datetime import datetime


def allowed_file_type(filename, allowed_suffixes):
    """
    Verify whether the suffix of filename is in allowed_suffixes
    :param filename:
    :param allowed_suffixes:
    :return:
    """
    if not allowed_suffixes:
        return False
    else:
        name, ext = os.path.splitext(filename)
        return ext.lstrip('.') in allowed_suffixes


def append_ts(filename):
    filename = filename \
        if (str(filename) not in ["", "None"]) else "file"

    return "".join([filename, '-', str(datetime.utcnow().microsecond)])


def write_file_to_local(output_path, file):
    with open(output_path, 'w') as f:
        f.writelines('\n'.join(file.read().splitlines()))
