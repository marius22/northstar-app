import datetime

import boto3


class S3Client(object):

    def __init__(self, aws_access_key_id, aws_secret_access_key,
                 bucket_name='es-yourtam', region_name='us-east-1'):
        self.client = boto3.client('s3', region_name,
                                   aws_access_key_id=aws_access_key_id,
                                   aws_secret_access_key=aws_secret_access_key)
        self.bucket_name = bucket_name

    def put_object(self, filename, key, content, content_type, expires_in,
                   with_content_disposition=True):
        expires = datetime.datetime.utcnow() + datetime.timedelta(seconds=expires_in)
        if with_content_disposition:
            self.client.put_object(Body=content, ContentType=content_type,
                                   ContentDisposition='attachment; filename="%s"' % filename,
                                   Bucket=self.bucket_name, Key=key, Expires=expires)
        else:
            self.client.put_object(Body=content, ContentType=content_type,
                                   Bucket=self.bucket_name, Key=key, Expires=expires)

        return self.client.generate_presigned_url('get_object',
                                                  Params={'Bucket': self.bucket_name,
                                                          'Key': key},
                                                  ExpiresIn=expires_in)

    def put_csv(self, filename, key, content, expires):

        return self.put_object(filename, key, content, 'text/csv', expires)

    def get_object(self, key):
        return self.client.get_object(Bucket=self.bucket_name, Key=key)


class AccountFitModelS3Client(S3Client):
    # es-data-uat/transient/model_service/input/job_id.json

    def __init__(self, aws_access_key_id, aws_secret_access_key,
                 bucket_name='account_fit_bucket', region_name='us-east-1'):
        super(AccountFitModelS3Client, self).__init__(
            aws_access_key_id,
            aws_secret_access_key,
            bucket_name,
            region_name)

    def put_text(self, key, content):

        expires = datetime.datetime.utcnow() + datetime.timedelta(days=60)

        self.client.put_object(Body=content, ContentType="text/plain",
                               Bucket=self.bucket_name,
                               Key=key, Expires=expires)

        # self.client.get_object(Bucket="es-northstar", Key="/first_test/test.json").get("Body").read()
        return self.bucket_name, key
