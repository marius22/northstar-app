import re


def fuzzy_search(text, collection):
    if not (collection and text):
        return collection

    suggestions = []
    regex = re.compile(text, flags=re.I)
    for item in sorted(collection):
        r = regex.search(item)
        if r:
            suggestions.append((r.start(), item))
    return [z for _, z in sorted(suggestions)]
