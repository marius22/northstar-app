import logging
import requests

from flask import current_app


class SFDataServiceClient(object):

    _instance = None

    def __init__(self):
        self.session = requests.Session()
        self.base_url = current_app.config.get('COMMON_SERVICE_BASE_URL')

    def query_sf_accounts(self, org_id, domains, companies):
        url = "{0}/crm/sfdc/{1}/accounts".format(self.base_url, org_id)
        request_data = {'domains': domains,
                        'companies': companies}
        resp = self.session.post(url, json=request_data)
        try:
            data = resp.json()
            accounts = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        logging.info('Result of fetching existing accounts from SFDC: ')
        logging.info(accounts)
        return accounts

    def query_sf_leads(self, org_id, domains, companies):
        url = "{0}/crm/sfdc/{1}/leads".format(self.base_url, org_id)
        request_data = {'domains': domains,
                        'companies': companies}
        resp = self.session.post(url, json=request_data)
        try:
            data = resp.json()
            leads = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        return leads

    def query_sf_accounts_by_contact(self, org_id, domains):
        url = "{0}/crm/sfdc/{1}/contacts".format(self.base_url, org_id)
        request_data = {'domains': domains}
        resp = self.session.post(url, json=request_data)
        try:
            data = resp.json()
            accounts = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        return accounts

    def query_sf_leads_by_email(self, org_id, emails):
        url = "{0}/crm/sfdc/{1}/leads".format(self.base_url, org_id)
        request_data = {'emails': emails}
        resp = self.session.post(url, json=request_data)
        try:
            data = resp.json()
            leads = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        return leads

    def query_sf_contacts_by_email(self, org_id, emails):
        url = '{0}/crm/sfdc/{1}/contacts'.format(self.base_url, org_id)
        request_data = {'emails': emails}
        resp = self.session.post(url, json=request_data)
        try:
            data = resp.json()
            contacts = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        return contacts

    @classmethod
    def instance(cls):
        if not cls._instance:
            return cls()
        else:
            return cls._instance
