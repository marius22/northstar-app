import urllib

from pprint import pprint
import datetime
import math
import logging
import requests
from flask import current_app
from ns.util.http_client import HTTPClient
from ns.util.list_util import get_batch_lists
from ns.constants.insight_constants import InsightConstants


logger = logging.getLogger(__name__)


class CompanyEnrichmentServiceError(Exception):
    def __init__(self, context):
        template = "<Request (url = %s) (payload = %s)>\n<Response (code = %d) (body = %s)>"
        (req, resp) = context['request'], context['response']

        message = template % (req.path_url, req.body, resp.status_code, resp.text)
        super(CompanyEnrichmentServiceError, self).__init__(message)


class CompanyDBClient(object):
    """
    The client talks to the Company service(author: Yi-Shiuan)
    """

    _instance = None

    def __init__(self):
        # Advantages of using `session`:
        #   - persist parameters across requests
        #   - connection pooling, reuse TCP connection which is a huge performance gain
        self.session = requests.Session()
        self.base_common_url = current_app.config.get('COMPANY_SERVICE_HOST')

        self.company_service_base_url = current_app.config.get('COMPANY_SERVICE_BASE_URL')
        self.common_service_base_url = current_app.config.get('COMMON_SERVICE_BASE_URL')

    def get_by_domain(self, domain):

        assert domain, "Invalid domain"

        domain_map = self.get_enrichment_by_domains([domain])
        return domain_map[domain] if domain_map else None

    def get_insights_by_domains(self, domains):
        url = '%s/analytics/' % self.company_service_base_url
        resp = self.session.post(url, json={'seedDomainList': domains})

        return resp.json()

    # This is an old way of enriching by domains that doesn't satisfy our current needs.
    # However, some code may still be using this in a few cases, so I'm keeping this method around
    # for now as to not break anything.
    # We use the /enrich endpoint (not to be confused with the /enrichment/ endpoint used here) for our
    # current needs.
    def get_enrichment_by_domains(self, domains):

        start_time = datetime.datetime.utcnow()
        logger.info('Start getting enrichment for domains: %s', domains)

        url = '%s/enrichment/' % self.company_service_base_url
        profiles = []

        batch_size = current_app.config.get("ENRICH_BATCH_SIZE")
        n = int(math.ceil(len(domains) / float(batch_size)))
        start_index = 0
        for i in range(n):
            payload = {'seedDomainList': domains[start_index: start_index + batch_size]}
            resp = self.session.post(url, json=payload)
            try:
                data = resp.json()
                profiles.extend(data['data']['companies'])
                start_index += batch_size
            except (ValueError, KeyError):
                raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})

        logger.info("Getting enrichment completed, time cost: {%s}"
                    % (datetime.datetime.utcnow() - start_time))

        domain_profile_maps = {profile['domain']: profile for profile in profiles}

        return domain_profile_maps

    def get_us_states(self):
        url = '%s/metadata/location/' % self.company_service_base_url
        resp = self.session.get(url)
        try:
            data = resp.json()
            states = data['data']['states']
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return states

    def get_industries(self):
        url = '%s/metadata/industry/' % self.company_service_base_url
        resp = self.session.get(url)
        try:
            data = resp.json()
            industries = data['data']['categories']
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return industries

    def get_techs(self):
        url = '%s/metadata/tech/' % self.company_service_base_url
        resp = self.session.get(url)
        try:
            data = resp.json()
            techs = data['data']['categories']
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return techs

    def get_state_zipcode(self):
        url = '%s/metadata/state_zipcode/' % self.company_service_base_url
        resp = self.session.get(url)
        try:
            data = resp.json()
            techs = data['data']
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return techs

    def get_preview(self, criteria_dict, limit, model_id):
        url = '%s/recommendations/preview' % self.common_service_base_url
        params = {
            'criteria': criteria_dict,
        }
        if limit:
            params['limit'] = limit
        if model_id is not None:
            params['modelId'] = model_id

        resp = self.session.post(url, json=params)
        try:
            # get 'data' here so we don't have to do response.data.data.data clientside
            preview_data = resp.json().get('data')
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return preview_data

    def create_audience(self, criteria_dict, audience_id, meta_type=None, exclusion_list_ids=list()):
        url = '%s/recommendations/create' % self.common_service_base_url
        params = {
            'audienceId': audience_id,
            'criteria': criteria_dict,
            'metaType': meta_type,
            'exclusionListIds': exclusion_list_ids
        }
        resp = self.session.post(url, json=params)
        try:
            create_audience_data = resp.json().get('data')
        except (ValueError, KeyError):
            raise CompanyEnrichmentServiceError({'request': resp.request, 'response': resp})
        return create_audience_data

    def get_account_fit_score(self, account_id, domains):

        from ns.model.account import Account
        assert account_id, "Invalid accountId"
        assert domains, "Invalid domain"

        account = Account.get_by_id(account_id)

        crm_fit_model = account and account.latest_completed_crm_fit_model
        if not crm_fit_model:
            return {}

        url = (self.common_service_base_url + "/model_fit_score?domains=%s&modelId=%s"
               % (','.join(domains), str(crm_fit_model.id)))

        resp = HTTPClient.get_json(
            url,
            timeout=120.0,
        )

        data = resp.get("data")
        return {item["domain"]: item["score"] for item in data}

    def get_segment_indicators(self, account_id, segment_id, domain):

        assert account_id, "Invalid accountId"
        assert segment_id, "Invalid segmentId"
        assert domain, "Invalid domain"

        url = (self.common_service_base_url + "/segment_indicators?accountId=%s&domain=%s&segmentId=%s"
               % (account_id, domain, segment_id))

        resp = HTTPClient.get_json(
            url,
            timeout=120.0,
        )

        return resp.get("data")

    def get_account_indicators(self, account_id, domain):

        from ns.model.account import Account
        assert account_id, "Invalid accountId"
        assert domain, "Invalid domain"

        account = Account.get_by_id(account_id)
        fa_model = account and account.latest_completed_fa_model
        if not fa_model:
            return []

        url = (self.common_service_base_url + "/model_indicators?domain=%s&modelId=%s"
               % (domain, fa_model.id))

        resp = HTTPClient.get_json(
            url,
            timeout=120.0,
        )

        return resp.get("data")

    def get_domain_indicators(self, domain):

        assert domain, "Invalid domain"

        url = (self.common_service_base_url + "/domain_indicators?domain=%s"
               % domain)

        resp = HTTPClient.get_json(
            url,
            timeout=120.0,
        )

        return resp.get("data")

    def get_audience_companies(self, audience_id, model_id, company_params):
        url = self.common_service_base_url + "/audiences/%s/companies" % audience_id
        if model_id:
            company_params['modelId'] = model_id
        result = HTTPClient.post_json(url, json=company_params, timeout=120.0)
        return result

    def get_audience_companies_count_with_dedupe(self, audience_id, model_id, company_params):
        if model_id:
            company_params['modelId'] = model_id
        url = self.common_service_base_url + "/audiences/%s/crm/dedupe/count" % audience_id
        result = HTTPClient.post_json(url, json=company_params, timeout=120.0)
        return result.get("data")

    def get_audience_companies_ids_with_dedupe(self, audience_id, model_id, company_params):
        if model_id:
            company_params['modelId'] = model_id
        url = self.common_service_base_url + "/audiences/%s/crm/dedupe" % audience_id
        result = HTTPClient.post_json(url, json=company_params, timeout=120.0)
        return result.get("data")

    def get_audience_companies_count(self, audience_id, model_id=None, criteria={}, dedupe=False,
                                     meta_type=100, exclusion_list_ids=list()):
        request_json = {
            'dedupe': dedupe,
            'criteria': criteria,
            'metaType': meta_type,
            'exclusionListIds': exclusion_list_ids
        }
        if model_id:
            request_json.update({'modelId': model_id})
        url = self.common_service_base_url + "/audiences/%s/companies/count" % audience_id
        result = HTTPClient.post_json(url, json=request_json, timeout=120.0)
        return result.get("total")

    def get_matched_audience_by_domain(self, domain, audience_ids):
        url = self.common_service_base_url + "/audiences"
        request_json = {
            'domain': domain,
            'audienceIds': audience_ids
        }
        result = HTTPClient.post_json(url, json=request_json, timeout=120.0)
        return result.get("data")

    def find_in_crm(self, find_on, org_id, domains):
        url = self.common_service_base_url + "/crm/data/find_in_crm"
        request_json = {
            'findOn': find_on,
            'orgId': org_id,
            'domains': domains
        }
        result = HTTPClient.post_json(url, json=request_json, timeout=120.0)
        return result.get("data")

    def proxy(self, method, url, json=None):
        assert url, "Invalid url"
        assert method, "Invalid method"
        url = self.base_common_url + url

        if method.lower() == "get":
            return HTTPClient.get_json(url, timeout=120.0)
        elif method.lower() == "post":
            return HTTPClient.post_json(
                url,
                json=json,
                timeout=120.0
            )
        else:
            raise NotImplementedError("not implemented method: %s" % method)

    def get_audience_companies_distinct_count(self, audience_id, model_id, keys, limit, order, with_others,
                                              exclusion_list_ids=list()):
        exclusion_list_ids = [str(exclusion_list_id) for exclusion_list_id in exclusion_list_ids]
        assert audience_id, "Invalid audience_id"
        if not keys:
            return {}
        url = self.common_service_base_url + "/audiences/%s/companies/distinct_counts?" \
                                             "modelId=%s&keys=%s" \
                                             "&limit=%s&order=%s&withOthers=%s" % \
                                             (audience_id,
                                              model_id if model_id else -1,
                                              ','.join(keys),
                                              limit, order, with_others)
        if exclusion_list_ids:
            url = (url + "&exclusionListIds={exclusion_list_ids}").format(
                exclusion_list_ids=",".join(exclusion_list_ids))

        result = HTTPClient.post_json(url, timeout=120.0)
        return result.get("data")

    def get_publish_data_for_domains(self, domains, insights=[], audience_ids=[], model_ids=[], batch_size=1000):
        from ns.model.publish_field_mapping import CompanyPublishData
        result = dict()
        domains_lists = get_batch_lists(domains, batch_size)
        request_data = {
            "insights": insights,
            "audienceIds": audience_ids,
            "modelIds": model_ids
        }
        for domains_list in domains_lists:
            url = '{0}/company_enrichment/enrich'.format(self.base_common_url)
            request_data["domains"] = domains_list
            resp = self.session.post(url, json=request_data)
            try:
                data = resp.json()
                companies = data.get("data", [])
                domains_to_company_data = {
                    company_data["domain"]: CompanyPublishData(company_data) for company_data in companies
                }
                result.update(domains_to_company_data)
            except Exception:
                logging.error(resp.text)
                raise
        return result

    # convenience method for publishing a single domain.
    def get_publish_data_for_domain(self, domain, insights, audience_ids, model_ids):
        publish_data = self.get_publish_data_for_domains([domain], insights, audience_ids, model_ids)
        return publish_data[domain]

    # get all the crm data from redshift
    def get_crm_data(self, org_id, find_on):
        url = self.common_service_base_url + "/crm/%s/data" % org_id
        request_json = {
            'findOn': find_on
        }
        result = HTTPClient.post_json(url, json=request_json, timeout=120.0)
        return result.get('data')

    @classmethod
    def instance(cls):
        """
        Lazy singleton

        Returns
        -------

        """
        if not cls._instance:
            return cls()
        else:
            return cls._instance


if __name__ == '__main__':
    client = CompanyDBClient.instance()

    resp = client.get_enrichments_by_domains(['alf.org'])
    pprint(resp)

    resp = client.get_insights_by_domains(['alf.org'])
    pprint(resp)
