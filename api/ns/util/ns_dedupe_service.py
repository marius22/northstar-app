import logging
from ns.util.sf_data_service import SFDataServiceClient


class NSDedupeService(object):

    _instance = None

    def __init__(self):
        self.sf_data_client = SFDataServiceClient.instance()

    def _parse_dedupe_preference(self, preference):
        self.net_new = preference['net_new']
        self.existing = preference['existing']
        self.net_new_enabled = self.net_new.get('enable')
        self.existing_enabled = self.existing.get('enable')
        if self.net_new_enabled and not self.existing_enabled:
            self.dedupe_against = self.net_new.get('against')

        elif not self.net_new_enabled and self.existing_enabled:
            self.dedupe_against = self.existing.get('against')

        elif self.net_new_enabled and self.existing_enabled:
            self.dedupe_against = self.net_new.get('against')

    def get_dedupe_results(self, org_id, company_list, preference):
        '''
        Based on your dedupe preferences, this method returns the ID's of the companies
        that match them.

        So if you publish only new companies, this method returns the ID's of all companies
        that do not exist in Salesforce.

        If you publish only existing companies, this method returns ID's of all companies already
        existing in Salesforce.
        '''
        logging.info("The dedupe process org is {}, preferences are {}".format(org_id, preference))
        self._parse_dedupe_preference(preference)
        self.merge_results = {}
        self.name2domain = {}
        self.id2domain = {}
        net_new_ids = []
        existing_ids = []
        domains, companies = self._parse_records_4_company(company_list)

        if 'account_contact' in self.dedupe_against and 'lead' in self.dedupe_against:
            self._dedupe_sf_accounts(org_id, domains, companies)
            domains_filtered = [key for key, val in self.merge_results.iteritems() if not val['accounts']]
            self._dedupe_sf_contacts(org_id, domains_filtered)
            self._dedupe_sf_leads(org_id, domains, companies)

        elif 'account_contact' in self.dedupe_against:
            self._dedupe_sf_accounts(org_id, domains, companies)
            domains_filtered = [key for key, val in self.merge_results.iteritems() if not val['accounts']]
            self._dedupe_sf_contacts(org_id, domains_filtered)

        elif 'lead' in self.dedupe_against:
            self._dedupe_sf_leads(org_id, domains, companies)

        for domain, v in self.merge_results.iteritems():
            if v['accounts'] or v['leads']:
                existing_ids.append(v['id'])
            else:
                net_new_ids.append(v['id'])

        if self.net_new_enabled and not self.existing_enabled:
            return net_new_ids

        elif not self.net_new_enabled and self.existing_enabled:
            return existing_ids

        elif self.net_new_enabled and self.existing_enabled:
            return net_new_ids + existing_ids

    def _parse_records_4_company(self, company_list):
        #   save the name 2 domain mapping
        domains = list()
        companies = list()
        for record in company_list:
            domain = record['domain']
            company_name = record['companyName']
            company_id = record['companyId']
            if domain and company_name:
                domains.append(domain)
                companies.append(company_name)

                self.merge_results[domain] = {}
                self.merge_results[domain]['accounts'] = []
                self.merge_results[domain]['leads'] = []
                self.merge_results[domain]['id'] = company_id
                self.name2domain[company_name] = domain

        return domains, companies

    def _dedupe_sf_accounts(self, org_id, domains, companies):

        accounts = self.sf_data_client.query_sf_accounts(org_id, domains, companies)
        for account in accounts:
            domain = account['domain']
            if not domain or domain not in self.merge_results:
                domain = self.name2domain.get(account['name'])
            if domain and domain in self.merge_results:
                self.merge_results[domain]['accounts'].append(account)
            else:
                self.logger.error("Missing account domain from dedupe API")
                if domain:
                    self.logger.error("Missing account domain is {}".format(domain))

    def _dedupe_sf_contacts(self, org_id, domains):

        accounts = self.sf_data_client.query_sf_accounts_by_contact(org_id, domains)

        for account in accounts:
            # The domain here is actually contact domain which we passed in to
            # query the related account, which can gurantee in the
            # merge_results dict
            domain = account.get('domain')
            if domain and domain in self.merge_results:
                self.merge_results[domain]['accounts'].append(account)
            else:
                self.logger.error("Missing contact domain from dedupe API")
                if domain:
                    self.logger.error("Missing contact domain is {}".format(domain))

    def _dedupe_sf_leads(self, org_id, domains, companies):

        leads = self.sf_data_client.query_sf_leads(org_id, domains, companies)
        for lead in leads:
            domain = lead['domain']
            email_domain = lead['emailDomain']
            company = lead['company']

            if domain and domain in self.merge_results:
                self.merge_results[domain]['leads'].append(lead)
            elif email_domain and email_domain in self.merge_results:
                self.merge_results[email_domain]['leads'].append(lead)
            elif company and company in self.name2domain:
                domain = self.name2domain.get(company)
                self.merge_results[domain]['leads'].append(lead)
            else:
                self.logger.error("Missing lead domain from dedupe API")
                if domain:
                    self.logger.error("Missing lead domain is {}".format(domain))

    @classmethod
    def instance(cls):
        if not cls._instance:
            return cls()
        else:
            return cls._instance
