import requests
from flask import current_app
import json


class QueueServiceClient(object):
    """
    The client makes requests to services that sends
    jobs to queue
    """

    _instance = None

    def __init__(self):
        # Advantages of using `session`: (Author: Yuanfei)
        #   - persist parameters across requests
        #   - connection pooling, reuse TCP connection which is a huge performance gain
        self.session = requests.Session()

        self.base_url = current_app.config.get('APP_SERVICE_BASE_URL')

    def submit_model_run_request(
            self, user_id, segment_id, job_id, domains,
            qualification_dict=None, expansion_dict=None):
        url = '{0}/jobs/{1}/recommendations/user/{2}/segment/{3}/job_submission'.format(
            self.base_url, job_id, user_id, segment_id)
        resp = self.session.post(url, json={
            "seed_list": domains,
            "qualification_criterias": qualification_dict,
            "expansion": expansion_dict
        })
        return resp.json()

    def submit_account_model(self, account_id, model_id):
        url = '%s/models/%s/submit' % (self.base_url, model_id)
        resp = self.session.post(url)

        if resp.status_code != 200:
            raise Exception("Trigger account model error, "
                            "modelId: {%s}" % model_id)
        return resp.json()

    def submit_feedback(self, job_id, domain, feedback):
        url = '{0}/jobs/{1}/recommendations/{2}/feedback/{3}'.format(
            self.base_url, job_id, domain, feedback)
        resp = self.session.post(url)
        return resp.json()

    def send_csv_export_message(self, data):
        url = '{0}/message/csv_export'.format(self.base_url)
        resp = self.session.post(url, json=data)
        return resp.status_code

    def send_export_message_with_dedupe(self, data):
        url = '{0}/message/export_with_dedupe'.format(self.base_url)
        resp = self.session.post(url, json=data)
        return resp.status_code

    def send_publish_audience_message(self, data):
        url = '{0}/message/publish_audience'.format(self.base_url)
        resp = self.session.post(url, json=data)
        print "resp", resp

        return resp.status_code

    @classmethod
    def instance(cls):
        """
        Lazy singleton

        Returns
        -------

        """
        if not cls._instance:
            return cls()
        else:
            return cls._instance
