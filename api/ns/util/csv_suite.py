"""
c csv util suite
"""

from six import StringIO
import unicodecsv
from flask import make_response


class BatchQuerier(object):
    """
    a querier, query data from data source in batches
    """
    def total(self):
        """
        total data count
        :return:
        """
        pass

    def get_batch(self, start, end):
        """
        query in batch
        :param start:
        :param end:
        :return:
        """
        pass


class BatchBasedIterator(object):

    """
    a one by one iterator base on the batch querier
    """

    def __init__(self, batch_querier, per_batch):
        """

        :param batch_querier:
        :param per_batch:        batch size
        :return:
        """
        self.batch_querier = batch_querier
        self.per_batch = per_batch

    def __iter__(self):

        start = 0
        total = self.batch_querier.total()

        while True:
            end = start + self.per_batch

            batch = self.batch_querier.get_batch(start, end)
            for ele in batch:
                yield ele

            start = start + self.per_batch

            if start >= total:
                break


class AbstractCSVDataLoader(BatchQuerier):
    """
    a abstract csv data loader, contains some common method such as: get_header
    """
    def __init__(self, per_batch):
        self.per_batch = per_batch

    def process_ele(self, ele):
        pass

    def get_headers(self):
        pass

    def __iter__(self):

        for ele in BatchBasedIterator(self, self.per_batch):
            yield self.process_ele(ele)


class SampleCSVDataLoader(AbstractCSVDataLoader):

    """
        sample code
    """

    def __init__(self, per_batch):
        super(SampleCSVDataLoader, self).__init__(per_batch)

    def process_ele(self, ele):
        return ele

    def get_headers(self):
        return ["key", "value"]

    def total(self):
        return 100

    def get_batch(self, start, end):
        result = []
        for i in range(start, end):
            index = str(i)
            result.append({"key": "key" + index, "value": "value" + index})
        return result


def write_csv_data_to_response(file_name, csv_data_loader):

    assert isinstance(csv_data_loader, AbstractCSVDataLoader), \
        "Please provide a CSV data loader"

    return write_csv_file_to_response(
        file_name,
        generate_csv_file(csv_data_loader.get_headers(), csv_data_loader))


def write_csv_file_to_response(file_name, csv_file):
    resp = make_response(csv_file)
    resp.headers['Content-Disposition'] = 'attachment; filename="%s"' % file_name
    resp.headers['Content-Type'] = 'text/csv'
    return resp


def generate_csv_file(headers, data):

    si = StringIO()
    writer = unicodecsv.DictWriter(si, headers,
                                   encoding='utf-8', extrasaction='ignore')

    writer.writeheader()
    writer.writerows(data)
    return si.getvalue()
