from flask import current_app
import httplib
import logging
import requests
import time


class DataServiceError(Exception):

    def __init__(self, context):
        template = "<Request (url = %s) (payload = %s)>\n<Response (code = %d) (body = %s)>"
        (req, resp) = context['request'], context['response']

        message = template % (req.path_url, req.body,
                              resp.status_code, resp.text)
        super(DataServiceError, self).__init__(message)


class ContactEmail:

    @staticmethod
    def is_valid(status):
        return status in ["accept_all", "valid"]


class DataAPIClient(object):
    """
    The client makes requests to Data API (owned by Andrew's team)
    """

    _instance = None

    def __init__(self):

        self._X_API_KEY = current_app.config.get('DATA_API_KEY')
        self._X_APP_KEY = current_app.config.get('DATA_APP_KEY')
        self._HOST = current_app.config.get('DATA_API_HOST')
        self._PORT = current_app.config.get('DATA_API_PORT')
        self.CONTACT_API_METHOD_NAME = current_app.config.get(
            'CONTACT_API_METHOD_NAME')
        self.logger = logging.getLogger(__name__)
        # time before the data api switches to asynchronous call
        self.wait_time = int(current_app.config.get('CONTACT_API_WAIT_TIME'))

    def __get_headers(self):
        return {"X-API-Key": self._X_API_KEY, "X-App-Key": self._X_APP_KEY}

    def __get_url(self, method_name, req_format="json"):
        method_name = method_name.replace(".", "/")
        url = "https://" + self._HOST
        if (self._PORT > 0):
            url = url + ":" + str(self._PORT)

        url = url + "/api/" + method_name + "." + req_format
        return url

    def get_request(self, method_name):
        url = self.__get_url(method_name)
        try:
            resp = requests.get(url, headers=self.__get_headers())
            if resp.status_code not in [httplib.OK, httplib.CREATED, httplib.ACCEPTED]:
                raise
            return resp.json()
        except Exception as ex:
            self.logger.error(ex)
            raise DataServiceError({'request': resp.request, 'response': resp})

    def post_request(self, method_name, params={}):
        url = self.__get_url(method_name)
        try:
            resp = requests.post(
                url, json=params, headers=self.__get_headers(), verify=True)
            if resp.status_code not in [httplib.OK, httplib.CREATED, httplib.ACCEPTED]:
                raise
            return resp.json()
        except Exception as ex:
            self.logger.error(ex)
            raise DataServiceError({'request': resp.request, 'response': resp})

    def get_task_result(self, task_id):
        """get status of long running tasks (Author: Sam Stevens with modifications by Yi-Shiuan)

        Args:
            taskId (str): the name of the task provided by call
            reqFormat (str, optional): response format, defaults to 'json'
        Returns:
            Object: if reqFormat is 'json' returns the response as a json Object, else it returns the response as text
        Raises:
            Exception: if request fails
        """
        return self.post_request("task.result", {"taskId": task_id})

    def kill_task(self, task_id):
        return self.post_request("task.cancel", {"taskId": task_id})

    def contact_list(self, payload, contact_api_method_name=None):
        # wait parameter sets how long the job can take. if query takes
        # longer than wait time, a 503 error will be thrown
        payload["wait"] = self.wait_time

        received_all_contacts = False
        contacts = []
        offset = 0
        while not received_all_contacts:
            self.logger.info("Calling contact list API with contact payload %s" % payload["contact"])
            if not contact_api_method_name:
                contact_api_method_name = self.CONTACT_API_METHOD_NAME
            resp = self.post_request(contact_api_method_name, payload)

            # check if task is still waiting for completion
            if isinstance(resp, dict):
                # if resp contains task id, we will try to kill the task
                if resp.get("taskId"):
                    self.logger.error(resp)
                    self.logger.error("Task took longer than %s seconds and will be cancelled" % self.wait_time)
                    # kill the task because it's still queued
                    resp = self.kill_task(resp["taskId"])
                else:
                    self.logger.error(resp)
                    self.logger.error("Task could not complete and could not be cancelled \
                        because the response has no task id.")
                break

            contacts += resp

            # check if another API call is required
            if len(resp) < payload["limit"]:
                received_all_contacts = True

            # update offset and store in payload
            offset += len(resp)
            payload["offset"] = offset
        return contacts

    def contact_title_expand(self, titles):
        if titles:
            payload = {
                "title": titles
            }
            return self.post_request("contact.title.expand", payload)
        return []

    @classmethod
    def instance(cls):
        """
        Lazy singleton

        Returns
        -------

        """
        if not cls._instance:
            return cls()
        else:
            return cls._instance
