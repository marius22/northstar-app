import re
import hashlib
import codecs
import random
from random import Random
from werkzeug.security import generate_password_hash
from werkzeug.security import check_password_hash
from werkzeug import exceptions

chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789'
lowercase_alphabets = "abcdefghijklmnopqrstuvwxyz"
uppercase_alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
numerical = "0123456789"


# require 3 of 4 character types (upper case, lower case, numerical, and special characters) and min length 8 (max 20)
def check_password_format(password):
    if password:
        if not (check_length(password) and
                (int(check_contain_lowercase_letter(password)) +
                    int(check_contain_uppercase_letter(password)) +
                    int(check_contain_num(password)) +
                    int(check_contain_symbol(password)) >= 3)):
            raise exceptions.BadRequest(
                "Password must be 8-20 characters long, and contain 3 of 4 character types: "
                "uppercase, lowercase, numeric, and special characters.")
        if check_contain_character_repetition(password):
            raise exceptions.BadRequest("Password is not allowed to have more than 2 identical characters in a row.")
    else:
        raise exceptions.BadRequest("Password can not be None.")


def check_length(password):
    return 8 <= len(password) <= 20


def check_contain_num(password):
    pattern = re.compile('.*[0-9]+.*')
    return pattern.match(password) is not None


def check_contain_lowercase_letter(password):
    pattern = re.compile('.*[a-z]+.*')
    return pattern.match(password) is not None


def check_contain_uppercase_letter(password):
    pattern = re.compile('.*[A-Z]+.*')
    return pattern.match(password) is not None


def check_contain_symbol(password):
    pattern = re.compile('.*([^a-z0-9A-Z])+.*')
    return pattern.match(password) is not None


# checks if same character repeats 3 or more times consecutively
def check_contain_character_repetition(password):
    pattern = re.compile(r'((\w)\2{2,})')
    return pattern.search(password) is not None


def encrypt_password_with_salt(salt, plain_pwd):

    # decode salt
    hexlify2 = codecs.getdecoder('hex')
    decode_salt = hexlify2(salt)[0]

    # encrypt password
    m = hashlib.sha1()
    m.update(decode_salt)
    m.update(plain_pwd)

    result = m.digest()
    for i in range(1, 1024):
        m = hashlib.sha1()
        m.update(result)
        result = m.digest()

    # encode encrypted password to string
    hexlify = codecs.getencoder('hex')
    return hexlify(result)[0]


def generate_salt_str():
    ints = []
    for i in range(8):
        ints.append(random.randint(0, 256))

    elements = ''.join(chr(v % 256) for v in ints)
    hexlify2 = codecs.getencoder('hex')
    return hexlify2(elements)[0]


def encrypt_password(password):
    return generate_password_hash(password)


def verify_password_hash(encrypt_pw, password):
    return check_password_hash(encrypt_pw, password)


def generate_random_pw_org(length_param=10):

    char = []

    length = len(chars)
    rd = Random()

    for i in range(length_param):
        char.append(chars[rd.randrange(0, length)])
    return "".join(char)


def generate_random_pw(length_param=10):
    if length_param < 8:
        raise exceptions.BadRequest("Password length must be 8 characters.")

    char = []

    rd = Random()

    for i in range(length_param):
        char.append(lowercase_alphabets[rd.randrange(0, len(lowercase_alphabets))])

    pwd = "".join(char)

    if not check_contain_num(password=pwd):
        for i in range(rd.randrange(1, 3)):
            replace_index = rd.randrange(0, length_param / 2)
            char[replace_index] = numerical[rd.randrange(0, len(numerical))]

    if not check_contain_uppercase_letter(password=pwd):
        for i in range(rd.randrange(1, 3)):
            replace_index = rd.randrange(length_param / 2, length_param)
            char[replace_index] = uppercase_alphabets[rd.randrange(0, len(uppercase_alphabets))]

    pwd = "".join(char)

    return pwd
