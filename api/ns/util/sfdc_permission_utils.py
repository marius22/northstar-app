# -*- coding: utf-8 -*-
import logging
from simple_salesforce import SalesforceError
from ns.model.crm_token import SFDCToken
from ns.model.salesforce import NSSalesforce
from ns.controller.service.crm_api.sfdc.utils import SFDC_FIELD_PREFIX
from ns.controller.service.crm_api.sfdc.utils import SFDC_FIELD_SUFFIX


logger = logging.getLogger(__name__)


visualforce_pages_names = ["es_account_vf_page", "es_lead_vf_page", "es_contact_vf_page", "es_opps_vf_page"]
standard_objects = ['Account', 'Contact', 'Lead', 'Opportunity']
custom_objects = ['AccountAudience', 'AccountScore',
                  'Audience',
                  'ContactAudience', 'ContactScore',
                  'LeadAudience', 'LeadScore',
                  'Model']
custom_objects = [SFDC_FIELD_PREFIX + one + SFDC_FIELD_SUFFIX for one in custom_objects]
profile_permissions = ["PermissionsApiEnabled", "PermissionsCustomizeApplication"]

object_permissions = ["PermissionsRead", "PermissionsCreate",
                      "PermissionsViewAllRecords",
                      "PermissionsEdit"]


class SalesforcePermissionError(SalesforceError):
    message = u"<b>Your Salesforce user's profile, <i>{profile_name}</i>, " \
              "must have the following permission(s):</b> <br><br>{content}"

    def __init__(self, content, profile_name):
        self.content = content
        self.profile_name = profile_name

    def __str__(self):
        return self.message.format(content=self.content, profile_name=self.profile_name)


def check_sfdc_permission(account_id, user_id, sfdc_basic_user, token_dict, crm_type):
    sfdc_token = SFDCToken(account_id, user_id, crm_type)
    sfdc_token.access_token = token_dict.get(SFDCToken.ACCESS_TOKEN_KEY)
    sfdc_token.instance_url = token_dict.get(SFDCToken.INSTANCE_URL_KEY)
    ns_salesforce = NSSalesforce(sfdc_token)
    sfdc_user = ns_salesforce.get_sfdc_info_by_url('sobjects/user/{user_id}'.
                                                   format(user_id=sfdc_basic_user.get(SFDCToken.USER_ID_KEY)))
    profile_id = sfdc_user.get(SFDCToken.PROFILE_ID_KEY)
    profile = ns_salesforce.get_sfdc_info_by_url('sobjects/profile/{profile_id}'.format(
        profile_id=profile_id))
    logger.info(profile)
    errors_from_profile = check_profile(profile)
    errors_from_objects = check_objects_permission(ns_salesforce, profile_id)

    # Stop doing the check_visualforce_pages() and check_entities_permission() checks
    # because they create a dependency on managed package during integration.
    # Keeping the commented code for reference.

    # v_ids, errors_v_pages = check_visualforce_pages(ns_salesforce)
    # v_ids = [item.encode('utf-8') for item in v_ids]
    # errors_entities = check_entities_permission(ns_salesforce, v_ids, profile_id)

    all_errors = errors_from_profile + errors_from_objects
    if all_errors:
        raise SalesforcePermissionError("<br><br>".join(all_errors), profile["Name"])

    return profile["Name"]


def check_objects_permission(ns_salesforce, profile_id):
    error_messages = list()
    all_objects = standard_objects
    soql = "SELECT Id, SobjectType, {permissions} " \
           "FROM ObjectPermissions " \
           "WHERE parentid in " \
           "(select id from permissionset where PermissionSet.Profile.Id = '{profile_id}') and " \
           "SObjectType in {objects}".format(permissions=",".join(object_permissions),
                                             profile_id=profile_id,
                                             objects=tuple(all_objects))
    records = ns_salesforce.query(soql).get("records", [])
    logger.info("{records}".format(records=records))
    permissions_result_map = dict()
    for record in records:
        permissions_result_map[record["SobjectType"]] = record

    for one_object in all_objects:
        p = permissions_result_map.get(one_object)
        if not p:
            error_messages.append(" &#8226; Standard Object Permissions > {object} Object".format(object=one_object))
        else:
            for permission in object_permissions:
                if not p.get(permission):
                    permission = format_permission_name(permission)
                    error_messages.append(" &#8226; Standard Object Permissions > {object} Object > {permission}".
                                          format(object=one_object, permission=permission))
    return error_messages


def check_profile(profile):
    error_messages = list()
    if not profile:
        error_messages.append(" &#8226; Permission to access profile information")
    else:
        for one_permission in profile_permissions:
            if not profile.get(one_permission):
                one_permission = format_permission_name(one_permission)
                error_messages.append(" &#8226; Administrative Permissions > {permission}".
                                      format(permission=one_permission))
    return error_messages


def check_visualforce_pages(ns_salesforce):
    error_messages = list()
    visualforce_pages = ns_salesforce.query('Select id from ApexPage where Name in {pages}'.format(
        pages=tuple(visualforce_pages_names)))
    logger.info("visualforce_pages: {visualforce_pages}".format(visualforce_pages=visualforce_pages))
    v_ids = [record.get("Id") for record in visualforce_pages.get("records", []) if record.get("Id")]
    if len(v_ids) != len(visualforce_pages_names):
        names_str = ', '.join(visualforce_pages_names)
        error_messages.append(" &#8226; Enabled Visualforce Page Access for the following page(s): {pages}".
                              format(pages=names_str))
    return v_ids, error_messages


def check_entities_permission(ns_salesforce, sfdc_ids, profile_id):
    error_messages = list()
    soql = "SELECT Id, SetupEntityId, SetupEntityType, ParentId FROM SetupEntityAccess where " \
           "SetupEntityId in {sfdc_ids} and " \
           "ParentId in (select id from  PermissionSet where ProfileId='{profile_id}')".\
        format(sfdc_ids=tuple(sfdc_ids), profile_id=profile_id)
    entity_accesses = ns_salesforce.query(soql).get("records", [])
    logger.info(entity_accesses)
    if len(entity_accesses) != len(sfdc_ids):
        names_str = ', '.join(visualforce_pages_names)
        error_messages.append(" &#8226; Enabled Visualforce Page Access for the following page(s): {pages}".
                              format(pages=names_str))
    return error_messages


def format_permission_name(name):
    name = name.replace('Permissions', '')
    return ''.join(' ' + x if 'A' <= x <= 'Z' else x for x in name).strip()
