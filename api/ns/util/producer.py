"""
General class to talk to message queue
"""
from flask import current_app
from kafka import KafkaProducer
from kafka.common import KafkaError
from werkzeug import exceptions
import logging
import traceback
import json


class Producer(object):
    """
        Note: The KafkaProducer is thread safe and sharing a single producer instance across threads
        will generally be faster than having multiple instances.
        More info: http://kafka-python.readthedocs.org/en/1.0.1/apidoc/KafkaProducer.html
    """
    _instance = None

    def __init__(self, kafka_server_list):

        self.brokers = kafka_server_list

        self.kafka_producer = KafkaProducer(
            bootstrap_servers=kafka_server_list,

            # convert values into bytes
            value_serializer=lambda m: json.dumps(m).encode('ascii'),

            # wait for the full set of in-sync replicas to write the record
            # strongest guarantee for message delivered to queue
            acks='all'
        )
        self.logger = logging.getLogger('ns.prodcuer')

    @classmethod
    def get_default_instance(cls):
        use_kafka = current_app.config.get('KAFKA_ENABLED')
        if use_kafka:
            brokers = current_app.config.get('KAFKA_BROKERS')
            return Producer.get_instance(brokers)
        else:
            return DummyProducer()

    @classmethod
    def get_instance(cls, kafka_server_list):
        """

        Parameters
        ----------
        kafka_server_list
            Comma separate broker server list
        kwargs
            If has `use_default` keyword arg and no
            `kafka_server_list` provided, then it'll
            use the config with key = `KAFKA_BROKERS`
            to initialize the instance.

        Returns
        -------

        """
        logger = logging.getLogger(__name__)

        instance = cls(kafka_server_list)
        logger.info("Got Kafka producer: %s", instance)

        return instance

    def job_submission(self, topic, user_id, segment_id, job_id, seed_list,
                       qualification_criterias=None, expansion=None):
        """
        DEPRECATED
        Job submission service
        :param job_id
        :param segment_id
        :param topic (string) topic the producer publishes message to
        :param user_id
        :param seed_list (list) String array containing domains
        :param qualification_criterias (dict)
        :param expansion (dict)
        :return dict object containing topic, partition, and offset of the message
        """
        assert type(topic) is str, "topic is not a string"
        assert type(user_id) is int, "user id is not integer"
        assert type(segment_id) is int, "segment id is not integer"
        assert type(job_id) is int, "job id is not integer"
        assert type(seed_list) is list, \
            "seed_list is not an array"

        # job-submission is the topic
        result = self.kafka_producer.send(topic, {
            "user_id": user_id,
            "segment_id": segment_id,
            "job_id": job_id,
            "seed_list": seed_list,
            "qualification": qualification_criterias,
            "expansion": expansion
        })

        try:
            msg_metadata = result.get(timeout=10)
        except KafkaError as e:
            self.logger.exception(e)
            self.logger.error(
                "Cannot send message to queue for job submission.")
            raise

        return {
            "topic": msg_metadata.topic,
            "partition": msg_metadata.partition,
            "offset": msg_metadata.offset
        }

    def recommendation_feedback(self, topic, job_id, domain, feedback):
        """
        DEPRECATED
        Feedback service
        :param topic (string) topic the producer publishes message to
        :param job_id (int)
        :param domain (string)
        :param feedback (boolean) true: like, false: dislike
        """
        assert type(topic) is str, "topic is not a string"
        assert type(job_id) is int, "job id is not integer"
        assert type(domain) is str, "domain is not a string"
        assert type(feedback) is bool, "feedback is not a boolean"

        # recommendation-feedback is the topic
        result = self.kafka_producer.send('recommendation-feedback', {
            "job_id": job_id,
            "domain": domain,
            "feedback": feedback
        })

        try:
            msg_metadata = result.get(timeout=5)
        except KafkaError as e:
            self.logger.exception(e)
            self.logger.error(
                "Cannot send message to queue for recommendation feedback.")
            raise

        return {
            "topic": msg_metadata.topic,
            "partition": msg_metadata.partition,
            "offset": msg_metadata.offset
        }

    def send(self, *args, **kwargs):
        """
        Delegate the call to the kafka producer
        """

        self.logger.info('Send from %s: args = %s, kwargs = %s',
                         self,
                         args,
                         kwargs)

        result = self.__retry(self.kafka_producer.send, 2, *args, **kwargs)

        if result is not None:
            try:
                msg_metadata = result.get(timeout=5)

                return {
                    "topic": msg_metadata.topic,
                    "partition": msg_metadata.partition,
                    "offset": msg_metadata.offset
                }
            except KafkaError:
                self.logger.exception(
                    "Cannot send message to kafka queue.")
                raise

    def send_message(self, message):
        if not isinstance(message, Message):
            raise TypeError('Message %s is not an instance '
                            'of Message class' % message)

        self.send(message.topic, message.payload)

    def __retry(self, function, attempts_left, *args, **kwargs):
        try:
            return function(*args, **kwargs)
        except Exception as e:
            if attempts_left == 0:
                self.logger.exception(e)
                self.logger.error("Failed to send message to Kafka")
            else:
                self.__retry(function, attempts_left - 1, *args, **kwargs)

    def __repr__(self):
        return '<KafkaProducer brokers=%s>' % self.brokers


class DummyProducer(object):
    """
    A dummy producer placeholder for testing mode.
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def send(self, *args, **kwargs):
        """
        Dummy send function doesn't really do anything.
        """
        self.logger.info('Sending from %s: args = %s, kwargs = %s',
                         self,
                         args, kwargs)
        return {
            'debug': True
        }

    def send_message(self, message):
        if not isinstance(message, Message):
            raise TypeError('Message %s is not an instance '
                            'of Message class' % message)

        self.send(message.topic, message.payload)

    def __repr__(self):
        return '<DummyProducer>'


class Message(object):
    """
    The abstract message class
    """

    topic = None
    payload = None

    def __init__(self, payload=None):
        if not self.topic:
            raise ValueError('missing `topic` attribute')

        self.payload = payload


class ModelRunMessage(Message):

    def __init__(self, account_id, user_id, segment_id,
                 job_id, seed_list, qualification_criterias, expansion):
        self.topic = current_app.config.get(
            'KAFKA_TOPIC_JOB', 'default-model-run')

        payload = {
            "account_id": account_id,
            "user_id": user_id,
            "segment_id": segment_id,
            "job_id": job_id,
            "seed_list": seed_list,
            "qualification": qualification_criterias,
            "expansion": expansion
        }

        super(ModelRunMessage, self).__init__(payload)


class ExportSegmentToCSVWithoutDedupeMessage(Message):
    """
    Produce CSV type of message without dedupe in V2
    """

    def __init__(self, payload):
        self.topic = current_app.config.get('KAFKA_TOPIC_EXPORT_CSV', 'export-csv')
        super(ExportSegmentToCSVWithoutDedupeMessage, self).__init__(payload)


class AudiencePublishMessage(Message):
    """
    Config Kafka topic for each type of message
    """

    def __init__(self, payload):
        self.topic = current_app.config.get('KAFKA_TOPIC_AUDIENCE_PUBLISH', 'ns-audience-publish')
        super(AudiencePublishMessage, self).__init__(payload)


class ExportSFDCMessage(AudiencePublishMessage):
    """
    Produce SFDC type of message
    """

    def __init__(self, payload):
        super(ExportSFDCMessage, self).__init__(payload)

    def validate(self, data):
        expected_fields = {"publish_type", "audience_request_endpoint", "audience_request_json",
                           "total_publish_account_num", "contact_limit_per_account", "titles",
                           "manage_level", "user_id", "user_email", "audience_id", "audience_name",
                           "org_id", "net_new", "existing", "account_id", "publish_fields", "models",
                           "companies_num_in_audience", "companies_num_in_audience_after_filter",
                           "contacts_num_before_dedupe"}
        if set(data.keys()) != expected_fields:
            raise exceptions.BadRequest("Missing fields in post data")


class ExportMarketoMessage(AudiencePublishMessage):
    """
    Produce Marketo type of message
    """

    def __init__(self, payload):
        super(ExportMarketoMessage, self).__init__(payload)

    def validate(self, data):
        expected_fields = {'publish_type', 'audience_request_endpoint', 'audience_request_json',
                           'total_publish_account_num', 'contact_limit_per_account', 'titles',
                           'manage_level', 'user_id', 'user_email', 'account_id', 'audience_id', 'audience_name',
                           'mas_lists', 'mas_type', "models",
                           "dedupe_mas_lists", "net_new", "existing",
                           "companies_num_in_audience", "companies_num_in_audience_after_filter",
                           "contacts_num_before_dedupe"}
        if set(data.keys()) != expected_fields:
            raise exceptions.BadRequest("Missing fields in post data")


class ExportWithDedupeMessage(AudiencePublishMessage):
    """
    Produce CSV type of message with dedupe
    """

    def __init__(self, payload):
        """
         message dict format

        :param recommendation_query: A SQL query that queries the `recommendation` table
            for the exact set of companies to publish, including filters, whether to dedupe
            against previous downloads, etc.
        :param total_publish_account_num: The maximum number of companies/accounts to publish.
        :param contact_limit_per_account: The maximum number of contacts to publish for each
            account published.
        :param publish_fields: An array of strings describing the fields to publish from `recommendation`
        :param user_id: The ID of the `user` to publish for.
        :param user_email: The email of the `user` to publish for so we can email them once the publish
            is completed.
        :param account_id: The ID of the `account` the user who's publishing belongs to.
        :param org_id: The ID of the SalesForce org of this `account`.
        :param segment_id: The ID of the segment to publish.
        :param segment_name: The name of the segment we're publishing.
        :param only_publish_account_has_contact: If true, then we only publish this account if it has
            contact information. If false, we publish this account regardless.
        :param net_new: A dict of information about whether to publish new accounts.
        :param existing: A dict of information about whether to publish existing accounts.
        """
        super(ExportWithDedupeMessage, self).__init__(payload)

    def validate(self, data):
        expected_fields = {"publish_type", "audience_request_endpoint", "audience_request_json",
                           "total_publish_account_num", "contact_limit_per_account", "titles",
                           "manage_level", "user_id", "user_email", "audience_id", "audience_name",
                           "org_id", "net_new", "existing", "account_id", "publish_fields", "models",
                           "companies_num_in_audience", "companies_num_in_audience_after_filter",
                           "contacts_num_before_dedupe"}
        if set(data.keys()) != expected_fields:
            raise exceptions.BadRequest("Missing fields in post data")


class ExportCSVMessage(AudiencePublishMessage):
    """
    Produce CSV type of message without dedupe
    """

    def __init__(self, payload):
        super(ExportCSVMessage, self).__init__(payload)

    def validate(self, data):
        expected_fields = {"publish_type", "audience_request_endpoint", "audience_request_json",
                           "total_publish_account_num", "contact_limit_per_account", "titles",
                           "manage_level", "user_id", "user_email", "audience_id", "audience_name",
                           "account_id", "publish_fields", "models",
                           "companies_num_in_audience", "companies_num_in_audience_after_filter",
                           "contacts_num_before_dedupe"}
        if set(data.keys()) < expected_fields:
            raise exceptions.BadRequest("Missing fields in post data")


class AccountModelRunMessage(Message):

    def __init__(self, topic, model_id, model_type, domains_url):

        self.topic = topic

        payload = {
            "model_id": model_id,
            "model_type": model_type,
            "domains_url": domains_url
        }

        super(AccountModelRunMessage, self).__init__(payload)


class FitModelMessage(AccountModelRunMessage):

    def __init__(self, model_id, model_type, domains_url):
        topic = current_app.config.get('KAFKA_TOPIC_ACCOUNT_FIT')
        super(FitModelMessage, self).__init__(topic, model_id, model_type, domains_url)


# class GalaxyMessage(AccountMo/delRunMessage):
#
#     def __init__(self, model_id, model_type, domains_url):
#         topic = current_app.config.get('KAFKA_TOPIC_ACCOUNT_GALAXY')
#         super(GalaxyMessage, self).__init__(topic, model_id, model_type, domains_url)


class FeatureAnalysisMessage(AccountModelRunMessage):

    def __init__(self, model_id, model_type, domains_url):
        topic = current_app.config.get('KAFKA_TOPIC_ACCOUNT_FEATURE_ANALYSIS')
        super(FeatureAnalysisMessage, self).__init__(topic, model_id, model_type, domains_url)


class SegmentFeatureAnalysis(Message):

    def __init__(self, account_id, segment_id, job_id, job_type, domains):

        self.topic = current_app.config.get(
            'KAFKA_TOPIC_ACCOUNT_FIT_MODEL', 'default_account_fit_model_run')

        payload = {
            "account_id": account_id,
            "job_id": job_id,
            "job_type": job_type,
            "domains": domains,
            "segment_id": segment_id
        }

        super(SegmentFeatureAnalysis, self).__init__(payload)


class DataSyncMessage(Message):

    def __init__(self, topic, account_ids):
        self.topic = topic
        payload = {
            "account_ids": account_ids,
        }
        super(DataSyncMessage, self).__init__(payload)
