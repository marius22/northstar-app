"""
A helper class for getting contacts from Andrew's Data API
"""

import copy
from collections import defaultdict
from functools import partial
from multiprocessing.dummy import Pool as ThreadPool
from flask import current_app
from ns.util.data_api_client import DataAPIClient


class ContactHelper(object):

    def __init__(self, args, payload):
        self.company_ids = args.get('companyIds')
        self.titles = args.get('titles')
        self.manage_levels = map(lambda x: x.lower(), args.get('manageLevels'))
        self.limit_per_company = args.get('limitPerCompany') or 1
        self.payload = payload
        self.num_workers = current_app.config['CONTACT_API_NUM_WORKERS']

        # Store final results in a dictionary key'ed by company id
        self.results = defaultdict(list)

    def get_exact_contacts(self):
        """
        This is used for API to get real contact information. In production, it'll be using
        contacts.list API where ZoomInfo will be called to get email, phone ... etc. We have
        to do this single threaded because for companies that get enough contacts already, we don't
        want to keep querying for contacts. For each API call, we filter out companies that already
        have enough contacts.
        """
        payloads = self.generate_payloads()
        for p in payloads:
            # call data api with given payload
            resp = DataAPIClient.instance().contact_list(p)
            # store contacts in self.results dictionary, remove duplicates
            self.dedupe_contacts(resp)
            # remove domains that have already gotten desired
            # number of contacts
            self.dedupe_domains()

        self.format_response()
        return self.results

    def process_get_contacts(self, (api_client, payload, index)):
        """
        Args:
            api_client: The wrapper class for calling Data API
            payload: (Dict) json object containing information for api request
            index: (Int) used to indicate priority
        """
        resp = api_client.contact_list(payload)
        return resp, index

    def get_contacts_multi_threads(self):
        """
        This is used for preview/count APIs where we want to optimize performance. We call
        the preview contacts API so no charges are incurred from ZoomInfo.
        """
        # make a pool of n workers
        pool = ThreadPool(self.num_workers)

        # store payloads for api requests
        payloads = self.generate_payloads()
        # arguments for process_get_contacts
        iterable = [(DataAPIClient(), payloads[i], i) for i in range(len(payloads))]

        results = pool.map(self.process_get_contacts, iterable)
        # close the pool and wait for workers to finish
        pool.close()
        pool.join()

        # sort results by index
        results = sorted(results, key=lambda x: x[1])

        # store contacts in self.results, remove duplicates
        for res in results:
            self.dedupe_contacts(res[0])

        self.format_response()
        return self.results

    def generate_payloads(self):
        """
        Assumptions:
            - either title or manage_level is selected or both are selected

        1. generate payloads for each title, each title has a limit so this requires
        an api call per title.
        2. generate payloads for manage levels.

        The order of the payloads correspond to its priority. The earlier the payload, the
        higher its priority.

        Returns a list of payloads for calling data api
        """
        assert self.titles is not None or self.manage_levels is not None

        payloads = []

        # set limit for results to at least 1 million in order to get good contacts
        self.payload["limit"] = max(int(1e6), int(self.limit_per_company) * len(self.company_ids))

        # only get contacts with emails
        self.payload["contact"]["hasEmail"] = 1

        # used to store criteria for each title
        groups = []
        for i in range(len(self.titles)):
            # Andrew's API only support lower case
            job_title = self.titles[i]["jobTitle"].lower()
            limit = self.titles[i]["limit"]

            if self.titles[i]["match"] == "Exact":
                fuzzy = False
            elif self.titles[i]["match"] == "Fuzzy":
                fuzzy = True

            groups.append({
                "match": [job_title],
                "fuzzy": fuzzy,
                "limit": limit
            })

        # only generate payload if groups/job titles is not empty
        if groups:
            payload = copy.deepcopy(self.payload)
            payload["contact"]["jobTitlePriority"] = {
                "domainLimit": self.limit_per_company,
                "groups": groups
            }
            payloads.append(payload)

        if self.manage_levels:
            payload = copy.deepcopy(self.payload)
            # add manage level filters
            payload["contact"]["manageLevel"] = self.manage_levels
            payload["contact"]["limit"] = self.limit_per_company
            payloads.append(payload)
        return payloads

    def dedupe_domains(self):
        """
        remove domains that have already found enough contacts
        """
        # get companies with desired number of contacts
        domains_with_enough_contacts = filter(lambda x: len(
            self.results[x]) >= self.limit_per_company, self.results)
        # get company ids for the domains with desired number of contacts
        comp_ids_with_enough_contacts = map(lambda x: self.results[x][
            0]["account.esId"], domains_with_enough_contacts)
        # get company ids that require more contacts
        self.company_ids = list(
            set(self.company_ids) - set(comp_ids_with_enough_contacts))

    def dedupe_contacts(self, new_contacts):
        """
        store new contacts into self.results, dedupe against existing contacts
        """
        for info in new_contacts:
            is_duplicate = False
            # stop adding contacts for a company if limit has reached
            if len(self.results[info["account.domain"]]) == self.limit_per_company:
                continue

            # check if contact info already exists in results
            for existing in self.results[info["account.domain"]]:
                if info["contact.esId"] == existing["contact.esId"]:
                    is_duplicate = True
                    break

            # if haven't found contact in results, add to results
            if not is_duplicate:
                self.results[info["account.domain"]].append(info)

    # removes special characters / spaces etc from job titles which are NOT allowed in data api
    # remove any special char from job title (which is treated as a separate word) director - assistant
    # remove any special char from job title from within word ??? ex: director-assistant
    # check for cases like director's and directors' and director -assistant

    def clean_contact_job_titles(self):
        for title in self.titles:

            # in case we have job title value within double quotes, then just ignore it all ex: "director & management"
            if title['jobTitle'].startswith('"') and title['jobTitle'].endswith('"'):
                title['jobTitle'] = title['jobTitle'][1:-1]
                continue

            all_words = title['jobTitle'].split()
            alpha_words = list()

            for word in all_words:
                if not word.isalnum():
                    word = ''.join([e if e.isalnum() else " " for e in word])
                    if not word or " " == word:
                        continue
                alpha_words.append(word)

            job_title = " ".join(alpha_words)
            title['jobTitle'] = " ".join(job_title.split())

    def format_response(self):
            """
            used to format keys from data api
            to keys in our response
            """
            key_mapping = {
                "account.esId": "companyId",
                "account.domain": "domain",
                "account.name": "companyName",
                "account.phone": "phone",
                "contact.esId": "id",
                "contact.firstName": "firstName",
                "contact.lastName": "lastName",
                "contact.country": "country",
                "contact.state": "state",
                "contact.email": "email",
                "contact.jobTitle": "jobTitle",
                "contact.manageLevel": "manageLevel",
                "contact.directPhone": "directPhone",
                "contact.priority": "priority"
            }

            def map_key(key):
                if key not in key_mapping:
                    return key.split(".")[1]
                return key_mapping[key]

            for domain in self.results:
                # iterate through contacts for a domain
                for i in range(len(self.results[domain])):

                    # get the contact dictionary at position i
                    contact = self.results[domain][i]
                    # remap keys according to key_mapping
                    formatted_contact = {map_key(k): v
                                         for k, v in contact.iteritems()}
                    self.results[domain][i] = formatted_contact

                # remove contacts where emailValid field is not "accept_all" or "valid"
                # the field only exists if briteverify is turned on
                self.results[domain] = filter(lambda c: c.get('emailValid') in
                                              [None, "accept_all", "valid"],
                                              self.results[domain])
