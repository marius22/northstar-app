"""
Some utils for string
"""
import hashlib
from datetime import datetime


def trim(s):
    if s:
        return unicode(s).strip()
    return s


def is_not_blank(s):
    if s:
        return bool(unicode(s).strip())
    return False


def is_blank(s):
    return not is_not_blank(s)


def none_to_empty(s):
    return s if s else ""


def generate_hash_str(seed):
    sha = hashlib.sha1(seed + str(datetime.utcnow()))
    return sha.hexdigest()
