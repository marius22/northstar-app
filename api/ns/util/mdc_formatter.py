# -*- coding: utf-8 -*-
import logging

from ns.util.auth_util import get_effective_user


class MDCFormatter(logging.Formatter):

    def format(self, record):
        result = super(MDCFormatter, self).format(record)
        user_email = ""
        user_id = ""
        account_id = ""
        try:
            user = get_effective_user()
            user_email = user.email
            user_id = user.id
            account_id = user.account.id
        except:
            pass

        if user_email:
            result = "account_id: {account_id} user_id: {user_id} user: {user_email} ".format(
                account_id=account_id, user_email=user_email, user_id=user_id) + result
        return result
