import requests
import logging

logger = logging.getLogger(__name__)


class HTTPClient(object):

    @staticmethod
    def post_json(url, json=None, **kwargs):
        logger.info("url is {}, json is {}".format(url, json))
        return HTTPClient._do_request('post', url, json=json, **kwargs)

    @staticmethod
    def get_json(url, params=None, **kwargs):
        logger.info("url is  {}, params is {}".format(url, params))
        return HTTPClient._do_request('get', url, params=params, **kwargs)

    @staticmethod
    def _is_success(response):
        return response.status_code == 200

    @staticmethod
    def _process_error(response):
        raise HTTPClientException(response.status_code, response.json())

    @staticmethod
    def _do_request(method, url, **kwargs):
        """
        a wrapper for requests.request() add exception process.
        :param url:
        :param json:
        :param kwargs:
        :return:
        """
        try:
            response = requests.request(method, url, **kwargs)

            if HTTPClient._is_success(response):
                return response.json()

        except Exception, e:
            logger.error("HTTPClient error: url: %s, params: {%s}, exception: %s"
                         % (url, kwargs, e))
            raise HTTPClientException(-1, "Unknown Exception in HTTPClient")

        HTTPClient._process_error(response)
        return


class HTTPClientException(Exception):

    def __init__(self, code, msg):
        super(HTTPClientException, self).__init__()
        self.code = code
        self.msg = msg

    def __str__(self):
        return "HTTPClientException: code: [%s], msg: [%s]" % (self.code, self.msg)
