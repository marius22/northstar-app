import logging
import requests
from werkzeug import exceptions
from flask import current_app


class NSDataServiceClient(object):
    """
    The client makes requests to services that sends
    jobs to queue
    """
    _instance = None

    def __init__(self):
        # Advantages of using `session`: (Author: Yuanfei)
        #   - persist parameters across requests
        #   - connection pooling, reuse TCP connection which is a huge performance gain
        self.session = requests.Session()
        self.base_url = current_app.config.get('APP_SERVICE_BASE_URL')

    def get_company_has_contact(self, company_ids, contact_params):
        contact_params['companyIds'] = company_ids
        url = self.base_url + "/contacts/has_contact"
        logging.info("url is  {},  parmas is {}".format(url, contact_params))
        resp = self.session.post(url, json=contact_params)
        try:
            data = resp.json()
            company_ids = data['data']
        except Exception:
            logging.error(resp.text)
            raise
        return company_ids

    def get_company_contacts_num(self, company_ids, contact_params):
        contact_params['companyIds'] = company_ids
        url = self.base_url + "/contacts/num_contact"
        logging.info("url is  {},  parmas is {}".format(url, contact_params))
        try:
            resp = self.session.post(url, json=contact_params)
            if resp.status_code == 200:
                data = resp.json()
                account_num_dict = data['data']
            else:
                raise exceptions.BadRequest(resp.content)
        except Exception:
            logging.error(resp.text)
            raise

        # get number of companies whose values are greater than 0
        # (have at least 1 contact)
        total_company_num = len(filter(lambda x: account_num_dict[x] > 0, account_num_dict))
        total_contacts_num = sum(account_num_dict.values())
        return total_company_num, total_contacts_num

    @classmethod
    def instance(cls):
        if not cls._instance:
            return cls()
        else:
            return cls._instance
