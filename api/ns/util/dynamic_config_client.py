"""
Client used for retrieving configurations from dynamic es config server
"""

from esconfig.consul_es_config import ConsulEsConfig
import imp


def get_configuration_object(env, application):
    es_config = ConsulEsConfig.get_instance(env, application)
    all_configurations_str = es_config.query("all")
    d = imp.new_module('config')
    exec(compile(all_configurations_str, "all_configurations_str", 'exec'), d.__dict__)
    return d
