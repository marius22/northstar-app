
def bucketize(num, buckets):
    if not num:
        return num

    for bucket in buckets:
        if num <= bucket.get('max'):
            return bucket.get('label')
