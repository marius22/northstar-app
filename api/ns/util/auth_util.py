from flask import g, current_app, request
from ns.constants import auth_constants
import logging
import base64
from werkzeug import exceptions

logger = logging.getLogger(__name__)


def get_effective_user():
    return g.get("ghost_user") or g.user


def get_login_cookie_key(email=None):
    key = None
    try:
        if email:
            key = str(email).lower() + current_app.config.get('SESSION_COOKIE_LOGIN_ATTEMPTS_NAME')
    except (AttributeError, Exception) as err:
        logging.warning(err.message)
    finally:
        if key:
            key = base64.b16encode(key)

        return key


def reset_login_attempts():
    set_session_ns_login_attempt(0)


def check_is_locked(login_attempts):
    if login_attempts >= auth_constants.MAX_FAILED_LOGIN_ATTEMPTS:
        raise exceptions.Unauthorized("Login Forbidden. Account is Locked. "
                                      "Please wait %d minutes before trying again."
                                      % auth_constants.USER_LOCK_TIMEOUT_MINS)


def get_session_ns_login_attempt():
    return current_app.session_interface.session_class.ns_login_attempt


def set_session_ns_login_attempt(login_attempts=0):
    current_app.session_interface.session_class.ns_login_attempt = login_attempts


def get_session_user_email():
    return current_app.session_interface.session_class.user_email


def set_session_user_email(email=None):
    current_app.session_interface.session_class.user_email = email


def get_token_str():
    auth_header = request.headers.get("Authorization")
    if not auth_header or auth_header == "":
        raise exceptions.Unauthorized("Authorization header not found")
    parts = auth_header.split()
    if parts[0].lower() != 'token':
        raise exceptions.Unauthorized("Authorization header must start with 'Token'")
    elif len(parts) == 1:
        raise exceptions.Unauthorized("Authorization token not found")

    return parts[1]
