
def get_value(d, key, default_value):
    if not d:
        return default_value

    value = d.get(key)
    if not value:
        return default_value

    return value
