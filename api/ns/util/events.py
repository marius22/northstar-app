# -*- coding: utf-8 -*-
import os
import socket
import struct
from datadog import initialize
from datadog import statsd as events

env = os.environ.get("ENV") or 'loc'


def get_gateway():
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue
            return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))

if env not in ['loc', 'test'] and not os.path.exists('/opt/datadog-agent/run/dogstatsd.pid'):
    options = {
        'statsd_host': get_gateway(),
        'statsd_port': '8125'
    }
    initialize(**options)
