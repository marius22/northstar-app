# -*- coding: utf-8 -*-

import logging

from flask import Blueprint, jsonify
from ns.model.account import Account
from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from ns.model.model import Model
from ns.model.publish_field_mapping import PublishFieldMapping
from webargs import fields
from webargs.flaskparser import use_kwargs


blueprint = Blueprint('account', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/accounts/<int:account_id>', methods=['GET'])
def get_account(account_id):
    account = Account.query.get_or_404(account_id)
    return jsonify(data=account.serialized)


@blueprint.route('/account/<int:account_id>/field/mapping', methods=['GET'])
@use_kwargs({
    "targetType": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_type_enums),
    "targetObject": fields.Str(required=False, validate=lambda x: x in PublishFieldMapping.target_object_enums)
})
def get_account_publish_field_mappings(account_id, targetType, targetObject):
    # only check that account_id exists, this API is only used by internal services
    Account.query.get_or_404(account_id)

    target_org_id = PublishFieldMapping.get_target_org_id(account_id, targetType)
    results = PublishFieldMapping.get_publish_field_mapping(account_id, targetType, target_org_id, targetObject)
    return jsonify(data=results)
