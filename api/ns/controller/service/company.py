import httplib
import logging
import datetime

from flask import Blueprint, jsonify
from ns.model.account_quota import AccountQuota
from ns.model.audience import Audience
from ns.model.user_published_company import UserPublishedCompany
from webargs import fields
from webargs.flaskparser import use_args
from ns.model.segment import Segment
from werkzeug import exceptions
from ns.model.account import Account
from ns.model.account_exported_company import AccountExportedCompany
from sqlalchemy.exc import IntegrityError
from ns.controller.app.param_validator import check_user_id
from ns.model.user_exported_company import UserExportedCompany

blueprint = Blueprint('company', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/companies/quota_remaining', methods=['GET'])
@use_args({
    'account_id': fields.Int(required=True)
})
def quota_remaining(args):
    account = _get_account(args['account_id'])
    return jsonify(data=account.account_quota.remaining_csv_company_cnt)


@blueprint.route('/companies/companies_check', methods=['POST'])
@use_args({
    'company_ids': fields.DelimitedList(fields.Int(), required=True),
    'account_id': fields.Int(required=True)
})
def companies_check(args):
    account = _get_account(args['account_id'])
    new_company_ids_set, exist_company_ids_set = account.get_company_ids_set(account, args['company_ids'])
    data = dict()
    data["new_company_ids"] = list(new_company_ids_set)
    data["exist_company_ids"] = list(exist_company_ids_set)
    return jsonify(data=data)


@blueprint.route('/companies/companies_track', methods=['POST'])
@use_args({
    'company_ids': fields.DelimitedList(fields.Int(), required=True),
    'account_id': fields.Int(required=True),
    'user_id': fields.Int(required=True)
})
def save_companies(args):
    account = _get_account(args['account_id'])
    # We check here for user_id != 0 because in our real time scoring
    # flow, it passes user_id 0 by default since the user_id for
    # real time scoring-related publishes is ambiguous.
    # This causes an exception to occur (and thus, the quota tracking
    # code in the try block doesn't run) in check_user_id because
    # a user with id 0 doesn't exist.

    # TODO: user_id is equal to 0, which is hard code in realtime scoring
    # this will be changed in future operator
    if args['user_id'] != 0:
        user = check_user_id(args['user_id'])
        if user.is_es_admin() or user.is_super_admin():
            return jsonify(status='OK')
    try:
        new_company_ids_set, exist_company_ids_set = account.get_company_ids_set(account, args['company_ids'])
        AccountExportedCompany.create_exports(args['account_id'], list(new_company_ids_set))
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(status='OK')


@blueprint.route('/companies/user_companies_track', methods=['POST'])
@use_args({
    'company_ids': fields.DelimitedList(fields.Int(), required=True),
    'segment_id': fields.Int(required=True),
    'user_id': fields.Int(required=True),
    'type': fields.DelimitedList(fields.Str(), required=True)
})
def save_user_companies(args):
    segment = _get_segment(args['segment_id'])
    user = check_user_id(args['user_id'])
    if user.is_es_admin() or user.is_super_admin():
        if segment.owner_id == user.id:
            pass
        else:
            return jsonify(status='OK')
    try:
        AccountQuota.upsert_user_companies(segment, args['company_ids'], args['type'])
        company_count = UserExportedCompany.count_by_segment(segment.id)
        segment.update_exported_num(company_count)
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(status='OK')


@blueprint.route('/companies/user_published_track', methods=['POST'])
@use_args({
    'company_ids': fields.DelimitedList(fields.Int(), required=True),
    'audience_id': fields.Int(required=True),
    'user_id': fields.Int(required=True),
    'type': fields.DelimitedList(fields.Str(), required=True)
})
def save_user_published_companies(args):
    audience = Audience.get_by_id(args['audience_id'])
    if not audience:
        raise exceptions.BadRequest("Invalid audience_id")

    user = check_user_id(args['user_id'])
    if user.is_es_admin() or user.is_super_admin():
        if audience.user_id == user.id:
            pass
        else:
            return jsonify(status='OK')
    try:
        UserPublishedCompany.upsert_user_published_companies(user.id, audience.id, args['company_ids'], args['type'])
        publish_time_ts = datetime.datetime.utcnow()
        company_count = UserPublishedCompany.count_by_audience(audience.id)
        audience.update_company_published_num(company_count, publish_time_ts)
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(status='OK')


def _get_account(account_id):
    """

    :rtype: account
    """
    account = Account.get_by_id(account_id)
    if not account:
        raise exceptions.BadRequest("Invalid account_id")
    return account


def _get_segment(segment_id):
    """

    :rtype: account
    """
    segment = Segment.get_by_id(segment_id)
    if not segment:
        raise exceptions.BadRequest("Invalid segment_id")
    return segment
