import urllib
from flask import jsonify, Blueprint, request
from werkzeug import exceptions
from ns.controller.app.param_validator import check_account_id
from ns.util.company_db_client import CompanyDBClient

blueprint = Blueprint('similar_domain', __name__)


@blueprint.route('/similar_domains/top_similarity_in_filter', methods=['POST'])
def top_similar_domains_in_filter():
    json_args = request.json.copy()

    account = check_account_id(json_args.get("accountId"))
    model = account.latest_completed_crm_fit_model
    if not model:
        raise exceptions.BadRequest(
            "Your CRM fit model is not completed. "
            "Please contact your system administrator to run the model"
        )

    json_args.pop("accountId")
    json_args["modelId"] = model.id

    result = CompanyDBClient.instance().proxy(
        'post',
        '/common/top_similarity_in_filter',
        json=json_args
    )

    return jsonify(
        result
    )


@blueprint.route('/similar_domains/search_by_tag_with_fitscore', methods=['POST'])
def search_domains_by_tags():
    json_args = request.json.copy()

    account = check_account_id(json_args.get("accountId"))
    model = account.latest_completed_crm_fit_model
    if not model:
        raise exceptions.BadRequest(
            "Your CRM fit model is not completed. "
            "Please contact your system administrator to run the model"
        )

    json_args.pop("accountId")
    json_args["modelId"] = model.id

    result = CompanyDBClient.instance().proxy(
        'post',
        '/common/ngrams/to/domains',
        json=json_args
    )

    return jsonify(
        result
    )
