"""
Job service
"""
import httplib

import datetime

from flask import Blueprint, request, jsonify, current_app, render_template
from werkzeug import exceptions
from webargs import fields
from webargs.flaskparser import use_args
from ns.model.job import Job
from ns.model.segment import Segment
from ns.util.email_client import NSEmailClient
from ns.model.job_status_history import JobStatusHistory

blueprint = Blueprint('JobStatus', __name__)


@blueprint.route('/jobs/<int:job_id>/status', methods=['POST'])
def create_job_status(job_id):
    data = request.get_json()

    # Verifies
    status = data.get('status')
    if not status:
        raise exceptions.BadRequest("`status` is required")

    job = Job.get_by_id(job_id)
    if not job:
        raise exceptions.NotFound('Job(ID = %d) is not found' % job_id)

    # Append a job status

    job_run_id = data.get('subJobId', '')
    job_run_name = data.get('subJobName', '')
    job_run_type = data.get('subJobType', '')

    if job.segment_id:
        Segment.create_job_history(job, status, job_run_id, job_run_name, job_run_type)

        if status == JobStatusHistory.SEGMENT_FINISHED:
            send_summary_email(job.segment)
    else:
        JobStatusHistory.create_job_history(
            job.id, status, job_run_id, job_run_name, job_run_type)

        if status == JobStatusHistory.STATUS_FINISHED:
            job.update_status(Job.COMPLETED)

    return jsonify(status="OK"), httplib.CREATED


@blueprint.route('/jobs', methods=['GET'])
@use_args({
    'status': fields.Str(validate=lambda s: s in ['COMPLETED', 'FAILED', 'IN PROGRESS']),
    'elapse': fields.Int()  # query jobs elapse x seconds
})
def get_jobs(args):
    status = args.get('status')
    elapse = args.get('elapse')

    jobs_query = Job.query
    if elapse:
        jobs_query = jobs_query.filter(
            Job.created_ts < datetime.datetime.utcnow() - datetime.timedelta(seconds=elapse))
    if status:
        jobs_query = jobs_query.filter(Job.status == status)
    jobs = jobs_query.join(Segment, Job.segment_id == Segment.id)\
        .filter(Job.type == Job.SEGMENT).\
        with_entities(Segment.id, Segment.name, Job.id, Job.status, Job.created_ts).all()

    res = []
    for job in jobs:
        res.append({'segment_id': job[0],
                    'segment_name': job[1],
                    'job_id': job[2],
                    'job_status': job[3],
                    'job_created_time': job[4].isoformat()})

    return jsonify(data=res), httplib.OK


def send_summary_email(segment):

    app_host = current_app.config.get("NS_STAR_APP_HOST")
    redirect_url = app_host + ("/#/segment/%s/overview" % segment.id)

    NSEmailClient.send_email(
        segment.owner.email,
        "EverString Segment Completed",
        render_template("segment_completed.html",
                        redirect_url=redirect_url)
    )
