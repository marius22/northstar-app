# -*- coding: utf-8 -*-
import httplib

from flask import Blueprint, jsonify
from webargs import fields
from webargs.flaskparser import use_args
from sqlalchemy.exc import IntegrityError
from ns.model.domain_dictionary import DomainDictionary

blueprint = Blueprint('domain_dictionary', __name__)


@blueprint.route('/domain_dictionary/email_check', methods=['GET'])
@use_args({
    "domains": fields.DelimitedList(fields.Str(), required=True,)
})
def get_valid_email_domain(args):
    """
    We normally can rely on email domains to get company information for a given contact
    (i.e nkittikul@everstring.com works at Everstring, so we can query on everstring.com
    to get Everstring data).

    However, we sometimes have contacts who use free email domains (i.e yahoo.com, gmail.com)
    that we want to ignore for the purposes of getting publish data, so we use this endpoint
    to filter out those free domains and return only the valid ones.
    """
    domains = args.get("domains")
    try:
        valid_domains = DomainDictionary.get_valid_email_domains(domains)
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(data=valid_domains)
