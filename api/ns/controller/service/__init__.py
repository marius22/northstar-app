"""
    Northstar service api - controllers
"""
import os
import logging
import logging.config

from flask import Flask
from flask_cors import CORS
from ns.util.dynamic_config_client import get_configuration_object
from os.path import dirname
from ns.util.make_json_app import make_json_app
from ns.model import db

from ns.controller.service import account as ns_account, job_status, status, recommendation, \
    feedback, job_submission, message, segment, api_token, salesforce, \
    insights, crm_data_sync, crm_token, \
    contact as contact_managerment, company, model, audience, mas, \
    metadata, similar_domain, crm_data, user, user_download, domain_dictionary


from ns.controller.service.crm_api.sfdc import account, contact, lead, segment_account, segment_contact, segment_lead, \
    account_audience, lead_audience, contact_audience, account_score, lead_score, model as sfdc_model, \
    audience as sfdc_audience


logging.config.fileConfig("logging.conf")

tmpl_dir = os.path.join(
    dirname(dirname(dirname(os.path.abspath(__file__)))), 'templates')

app = Flask("ns_service", instance_relative_config=True,
            template_folder=tmpl_dir)

env = os.environ.get('ENV', 'local')
if env in ['dev', 'uat', 'staging', 'demo', 'prod']:
    d = get_configuration_object(env, "northstar_service")
    app.config.from_object(d)
else:
    app.config.from_pyfile(env + '.config-service.py')
app.config['CURRENT_ENV'] = env

app = make_json_app(app)

db.init_app(app)

if env == 'local':
    CORS(app)


app.register_blueprint(status.blueprint)
app.register_blueprint(job_status.blueprint)
app.register_blueprint(recommendation.blueprint)
app.register_blueprint(feedback.blueprint)
app.register_blueprint(job_submission.blueprint)
app.register_blueprint(message.blueprint)
app.register_blueprint(segment.blueprint)
app.register_blueprint(api_token.blueprint)
app.register_blueprint(salesforce.blueprint)
app.register_blueprint(insights.blueprint)
app.register_blueprint(account.blueprint)
app.register_blueprint(contact.blueprint)
app.register_blueprint(lead.blueprint)
app.register_blueprint(crm_data_sync.blueprint)
app.register_blueprint(crm_token.blueprint)
app.register_blueprint(segment_account.blueprint)
app.register_blueprint(segment_contact.blueprint)
app.register_blueprint(segment_lead.blueprint)
app.register_blueprint(contact_managerment.blueprint)
app.register_blueprint(company.blueprint)
app.register_blueprint(model.blueprint)
app.register_blueprint(audience.blueprint)
app.register_blueprint(similar_domain.blueprint)
app.register_blueprint(metadata.blueprint)

app.register_blueprint(account_audience.blueprint)
app.register_blueprint(lead_audience.blueprint)
app.register_blueprint(contact_audience.blueprint)
app.register_blueprint(account_score.blueprint)
app.register_blueprint(lead_score.blueprint)
app.register_blueprint(sfdc_model.blueprint)
app.register_blueprint(sfdc_audience.blueprint)
app.register_blueprint(ns_account.blueprint)
app.register_blueprint(mas.blueprint)
app.register_blueprint(crm_data.blueprint)
app.register_blueprint(user.blueprint)
app.register_blueprint(user_download.blueprint)
app.register_blueprint(domain_dictionary.blueprint)
