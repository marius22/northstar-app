# -*- coding: utf-8 -*-
import json
from flask import Blueprint, jsonify
from ns.model.crm_token import CRMToken
from ns.model.account import Account
from werkzeug.exceptions import NotFound, BadRequest
import logging

blueprint = Blueprint('crm_token', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/crm_token/<int:_id>', methods=['GET'])
def get_token(_id):
    crm_token = CRMToken.get_by_id_refresh_if_expired(_id)
    if not crm_token:
        raise NotFound('Token %s is not found' % _id)
    result = generate_token_response(crm_token)
    return jsonify(**result)


@blueprint.route('/crm_token/data_sync_<string:_sync_type>/tokens', methods=['GET'])
def get_crm_data_sync_tokens(_sync_type):
    if _sync_type not in ['init', 'inc']:
        raise BadRequest("Wrong data sync type")
    account_ids = None
    if _sync_type == 'init':
        account_ids = Account.get_datasync_uninitialized()
    else:
        account_ids = Account.get_datasync_initialized()

    crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts(account_ids)
    records = []
    for crm_token in crm_tokens:
        record = generate_token_response(crm_token)
        records.append(record)
    return jsonify(data=records)


@blueprint.route('/crm_token/account_admin/<int:_account_id>', methods=['GET'])
def get_account_admin_token(_account_id):
    crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([_account_id])
    logger.info("got crm token for account: {0}, --> {1}".format(_account_id, crm_tokens))
    if not crm_tokens:
        raise NotFound('No admin token is not found for account %s.' % _account_id)

    crm_token = CRMToken.refresh_if_expired(crm_tokens[0])
    logger.info("token of account:{0} after refresh --> {1}".format(_account_id, crm_token))
    result = generate_token_response(crm_token)
    return jsonify(**result)


@blueprint.route('/crm_token/account_admin_no_refresh/<int:_account_id>', methods=['GET'])
def get_account_admin_token_no_refresh(_account_id):
    crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([_account_id])
    logger.info("got crm token for account: {0}, --> {1}".format(_account_id, crm_tokens))
    if not crm_tokens:
        raise NotFound('admin token is not found for account %s.' % _account_id)
    result = generate_token_response(crm_tokens[0])
    return jsonify(**result)


def generate_token_response(crm_token):
    token = json.loads(crm_token.token)
    return {"id": crm_token.id, "access_token": token["access_token"], 'account_id': crm_token.account_id,
            "instance_url": token["instance_url"],
            "organization_id": crm_token.organization_id,
            "user_id": crm_token.user_id}
