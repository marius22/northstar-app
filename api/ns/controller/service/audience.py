import logging

from flask import Blueprint, jsonify
from webargs import fields
from webargs.flaskparser import use_args

from werkzeug import exceptions
from ns.model.account import Account
from ns.model.audience import Audience
from ns.util.company_db_client import CompanyDBClient


blueprint = Blueprint('audience', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/audience/domain_match', methods=['GET'])
@use_args({
    'accountId': fields.Int(required=True),
    'domain': fields.Str(required=True)
})
def get_audience_by_domain(args):
    domain = args.get('domain')
    account = _get_account(args['accountId'])
    audiences = []
    audience_ids = []
    for user in account.active_users:
        audiences.extend(user.audiences.filter(Audience.is_deleted.is_(False)))
    audience_ids = [audience.id for audience in audiences]
    matched_dict = CompanyDBClient.instance().get_matched_audience_by_domain(domain, audience_ids)
    matched_audiences = Audience.get_matched_audiences_by_ids(
        matched_dict['matched_ids'], matched_dict['published_ids'])
    return jsonify(data=[item.serialized for item in matched_audiences])


@blueprint.route('/audience/need_score', methods=['GET'])
@use_args({
    'account_id': fields.Int(required=True),
    'all_audience': fields.Boolean(missing=False)
})
def get_need_score_audience(args):
    account = _get_account(args['account_id'])
    all_audience = args['all_audience']
    audiences = []
    for user in account.active_users:
        audiences.extend([audience for audience in user.audiences.all() if not audience.is_deleted])
    result_audiences = list()
    for item in audiences:
        if all_audience:
            result_audiences.append(item.serialized)
        else:
            if item.real_time_scoring_enabled:
                result_audiences.append(item.serialized)
    return jsonify(data=result_audiences)


@blueprint.route('/audience/insights', methods=['GET'])
@use_args({
    'account_id': fields.Int(required=True)
})
def get_audience_insights(args):
    account = _get_account(args['account_id'])
    user_exposed_fields = account.account_quota.exposed_insight_company_column_names
    user_exposed_fields = [name for name in user_exposed_fields if name]
    return jsonify(data=user_exposed_fields)


def _get_account(account_id):
    """
    :rtype: account
    """
    account = Account.get_by_id(account_id)
    if not account:
        raise exceptions.BadRequest("Invalid account_id")
    return account
