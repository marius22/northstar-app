# -*- coding: utf-8 -*-

import json
from flask import Blueprint, request, jsonify, current_app
from ns.model.account import Account
from werkzeug.exceptions import NotFound
from ns.util.producer import Producer, DataSyncMessage
import time

blueprint = Blueprint('data_sync', __name__)


@blueprint.route('/data_sync/initialized/<int:account_id>', methods=['POST'])
def finish_sync(account_id):
    account = Account.get_by_id(account_id)
    if not account:
        raise NotFound('account %s is not found' % account_id)
    account.set_data_sync_initialized()
    return jsonify(status="OK")


@blueprint.route('/data_sync/trigger/<string:account_ids>', methods=['GET'])
def trigger_data_sync(account_ids):
    brokers = current_app.config.get('KAFKA_BROKERS')
    producer = Producer.get_instance(brokers)

    current_app.logger.info("Before produce:" + str(time.time()))
    topic = current_app.config.get('KAFKA_TOPIC_CRM_DATA_SYNC')
    message = DataSyncMessage(topic, account_ids)
    producer.send_message(message)
    current_app.logger.info("After produce:" + str(time.time()))
    return jsonify(status="OK")
