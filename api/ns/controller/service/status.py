"""
Status controller for service test
"""

from flask import jsonify, Blueprint

blueprint = Blueprint('status', __name__)


@blueprint.route('/status', methods=['GET'])
def status_check():
    """
    Sanity check endpoint for monitoring
    """
    return jsonify(status='OK')
