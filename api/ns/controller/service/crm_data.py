# -*- coding: utf-8 -*-

from flask import Blueprint, jsonify
from webargs.flaskparser import use_args
from webargs import fields
from ns.util.redshift_client import RedshiftClient
from ns.model.crm_token import CRMToken
import logging
from ns.util.company_db_client import CompanyDBClient

blueprint = Blueprint('crm_data', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/crm/data/find_in_crm', methods=['GET'])
@use_args({
    'findOn': fields.String(required=True),
    'accountId': fields.String(required=True),
    'domains': fields.DelimitedList(fields.Str, required=True,
                                    validate=lambda val: len(val) > 0),
    'checkOpps': fields.Boolean(missing=True)
})
def find_in_crm(args):
    find_on = args.get("findOn")
    account_id = args.get("accountId")
    domains = args.get("domains")
    check_opps = args.get("checkOpps")
    result = {}
    org_id = get_token_by_account_id(account_id)
    if not org_id:
        logging.error("org is for account %s is not exist" % account_id)
        return jsonify(data=result)
    domains = [domain for domain in domains if domain and not domain.isspace()]
    if 'lead' == find_on:
        domains, result = search_on_lead(domains, org_id, result, check_opps)
    elif 'account' == find_on:
        domains, result = search_on_account(domains, org_id, result, check_opps)
    elif 'lead,account' == find_on:
        domains, result = search_on_lead(domains, org_id, result, False)
        domains, result = search_on_account(domains, org_id, result, False)
    return jsonify(data=result)


def search_on_lead(domains, org_id, result, check_opportunity):
    if check_opportunity:
        domains, result = find_on_leads("domain", domains, org_id, result, True)
        domains, result = find_on_leads("email_domain", domains, org_id, result, True)
    domains, result = find_on_leads("domain", domains, org_id, result, False)
    domains, result = find_on_leads("email_domain", domains, org_id, result, False)
    return domains, result


def search_on_account(domains, org_id, result, check_opportunity):
    if check_opportunity:
        domains, result = find_on_accounts(domains, org_id, result, True)
    domains, result = find_on_accounts(domains, org_id, result, False)
    return domains, result


def process_find_on_result(in_crms, find_on, result, domains):
    for in_crm in in_crms:
        if not result.get(in_crm[0]):
            result[in_crm[0]] = {"ownerId": in_crm[1], "objectId": in_crm[2], "findOn": find_on, "stage": in_crm[3]}
    domains = list(set(domains) - set(result.keys()))
    return domains, result


def find_on_leads(domain_field, domains, org_id, result, check_opportunity):
    if len(domains) == 0:
        return domains, result
    if check_opportunity:
        sql = get_find_on_leads_with_opportunity_sql(domain_field, domains, org_id)
    else:
        sql = get_find_on_leads_without_opportunity_sql(domain_field, domains, org_id)
    in_crms = RedshiftClient.instance().query(sql)
    return process_find_on_result(in_crms, "lead", result, domains)


def find_on_accounts(domains, org_id, result, check_opportunity):
    if len(domains) == 0:
        return domains, result
    if check_opportunity:
        sql = get_find_on_accounts_with_opportunity_sql(domains, org_id)
    else:
        sql = get_find_on_accounts_without_opportunity_sql(domains, org_id)
    in_crms = RedshiftClient.instance().query(sql)
    return process_find_on_result(in_crms, "account", result, domains)


def get_find_on_leads_with_opportunity_sql(domain_field, domains, org_id):
    table_name = "crm_lead"
    opportunity_table = 'crm_opportunity'
    sql = '''SELECT a.{}, a.owner_id, a.crm_id, c.stage_name
            FROM {} as a
            JOIN (SELECT {}, org_id, MAX(created_date) AS created_date
                FROM {}
                WHERE org_id = '{}'
                AND {} in('{}')
                AND converted_opportunity_id is not null
                GROUP BY org_id, {}
                ) as b
            ON a.org_id = b.org_id
            AND a.{} = b.{}
            AND a.created_date = b.created_date
            JOIN {} as c
            ON a.org_id = c.org_id
            AND c.crm_id = a.converted_opportunity_id''' \
        .format(domain_field, table_name, domain_field, table_name, org_id, domain_field,
                "','".join(domains), domain_field, domain_field, domain_field, opportunity_table)
    return sql


def get_find_on_leads_without_opportunity_sql(domain_field, domains, org_id):
    table_name = "crm_lead"
    sql = '''SELECT a.{}, a.owner_id, a.crm_id, '' as stage_name
            FROM {} as a
            JOIN (SELECT {}, org_id, MAX(created_date) AS created_date
                FROM {}
                WHERE org_id = '{}'
                AND {} in('{}')
                GROUP BY org_id, {}
                ) as b
            ON a.org_id = b.org_id
            AND a.{} = b.{}
            AND a.created_date = b.created_date
            ''' \
        .format(domain_field, table_name, domain_field, table_name, org_id, domain_field,
                "','".join(domains), domain_field, domain_field, domain_field)
    return sql


def get_find_on_accounts_with_opportunity_sql(domains, org_id):
    table_name = 'crm_account'
    opportunity_table = 'crm_opportunity'
    sql = '''SELECT a.domain, a.owner_id, a.crm_id, b.stage_name
            FROM {} as a
            JOIN {} AS b
              ON a.org_id = '{}'
              AND a.crm_id = b.account_id
              AND b.org_id = a.org_id
            JOIN (SELECT c.domain, c.org_id, MAX(d.created_date) AS created_date
                    FROM {} AS c
                    JOIN {} AS d
                      ON c.org_id = '{}'
                     AND domain in ('{}')
                     AND c.crm_id = d.account_id
                     AND d.org_id = c.org_id
                     GROUP BY c.org_id,c.domain) AS e
            ON a.domain = e.domain
            AND b.created_date = e.created_date
            '''.format(table_name, opportunity_table, org_id, table_name, opportunity_table,
                       org_id, "','".join(domains))
    return sql


def get_token_by_account_id(account_id):
    crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([account_id])
    if not crm_tokens:
        return None
    return crm_tokens[0].organization_id


def get_find_on_accounts_without_opportunity_sql(domains, org_id):
    table_name = 'crm_account'
    sql = '''SELECT a.domain, a.owner_id, a.crm_id, '' as stage_name
                FROM {} as a
                JOIN (SELECT domain, org_id, MAX(created_date) AS created_date
                    FROM {}
                    WHERE org_id = '{}'
                    AND domain in('{}')
                    GROUP BY org_id, domain
                    ) as b
                ON a.org_id = b.org_id
                AND a.domain = b.domain
                AND a.created_date = b.created_date
                ''' \
        .format(table_name, table_name, org_id, "','".join(domains))
    return sql


@blueprint.route('/crm/<int:account_id>/data', methods=['POST'])
@use_args({
    'findOn': fields.String(required=True)
})
def get_crm_data(args, account_id):
    find_on = args.get("findOn")
    org_id = get_token_by_account_id(account_id)
    res = CompanyDBClient.instance().get_crm_data(org_id, find_on)
    return jsonify(data=res)
