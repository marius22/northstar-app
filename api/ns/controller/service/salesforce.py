from flask import Blueprint, jsonify
from webargs import fields
from webargs.flaskparser import use_kwargs
from ns.model.account_quota import AccountQuota, QuotaExceedError
from ns.model.sfdc_config import SFDCConfig
from ns.model.salesforce import publish_domain_to_sfdc
from ns.model.crm_token import CRMToken
from ns.model.user import User
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.util.webargs_parser import DictField, DomainField
from ns.model.user_preference import UserPreference
from ns.constants.default_field_mapping_constants import TARGET_OBJECT_LEAD, TARGET_OBJECT_ACCOUNT
from werkzeug import exceptions


blueprint = Blueprint('sfdc_config', __name__)


@blueprint.route('/sfdc_app/add_to_crm', methods=['POST'])
@use_kwargs({
    "account_id": fields.Int(required=True),
    "domain": DomainField(required=True),
    "sf_obj_type": fields.Str(required=True, validate=lambda x: x in [TARGET_OBJECT_LEAD, TARGET_OBJECT_ACCOUNT])
})
def add_sfdc_object(account_id, domain, sf_obj_type):
    try:
        result = publish_domain_to_sfdc(account_id, domain, sf_obj_type)
    except QuotaExceedError:
        raise exceptions.BadRequest("0 account credits available")

    return jsonify(
        data=result
    )


@blueprint.route('/sfdc_app/salesforce_config', methods=['GET'])
@use_kwargs({
    "account_id": fields.Int()
})
def get_sfdc_config(account_id):
    config = SFDCConfig.get_by_account_id(account_id)
    if config:
        custom_keys = config.get_header_custom_keys()
        indicators = IndicatorWhiteList.get_by_indicators(custom_keys)
        indicator_map = {}
        for indicator in indicators:
            indicator_map[indicator.indicator] = indicator.display_name
        records = config.config_dict.get("insightHeadersCustom")
        new_records = []
        for record in records:
            new_record = record
            new_record["value"] = indicator_map.get(record["key"])
            new_records.append(new_record)
        config_dict_result = config.config_dict
        config_dict_result["insightHeadersCustom"] = new_records

    return jsonify(
        data=config_dict_result if config else None
    )


@blueprint.route('/sfdc/salesforce_config', methods=['GET'])
def get_scoring_enable_accounts():
    configs = SFDCConfig.get_enrich_enable_accounts()
    config_map = {}
    for one in configs:
        config_map[one.account_id] = one.scoring_config_dict
    return jsonify(
        data=config_map if configs else {}
    )


@blueprint.route('/sfdc_app/accounts/<org_id>/account_id', methods=['GET'])
def get_account_id_by_org_id(org_id):
    """
    # query account_id by sfdc org_id

    # Confirmed by PM, (2016-06-30)
    # Select an user from users randomly
    # and return the user's account_id as the org's account_id
    :param org_id:
    :return:
    """
    account_id = None

    crm_tokens = CRMToken.get_sfdc_tokens_by_org_id(org_id)
    if crm_tokens:
        user = User.get_by_id(crm_tokens[0].user_id)
        account_id = user.account_id

    return jsonify(
        data=account_id
    )


@blueprint.route('/sfdc_app/users/<sfdc_user_id>/preference', methods=['POST'])
@use_kwargs({
    "appName": fields.Str(required=True),
    "email": fields.Str(required=True),
    "preference": DictField(required=True, missing={})
})
def save_user_preference(sfdc_user_id, appName, email, preference):
    result = []
    for key, value in preference.items():
        result.append(
            UserPreference.save_preference(sfdc_user_id, email, appName, key, value)
        )
    return jsonify(
        data={item.type: item.val_obj for item in result}
    )


@blueprint.route('/sfdc_app/users/<sfdc_user_id>/preference', methods=['GET'])
@use_kwargs({
    "appName": fields.Str(required=True),
    "types": fields.DelimitedList(fields.Str(), missing=None, required=False)
})
def get_user_preference_by_sfdc_user_id(sfdc_user_id, appName, types):
    result = UserPreference.get_by_sfdc_user_id_app_and_type(sfdc_user_id, appName, types)

    return jsonify(
        data={item.type: item.val_obj for item in result}
    )


@blueprint.route('/sfdc_app/preferences/get_by_email', methods=['GET'])
@use_kwargs({
    "email": fields.Str(required=True),
    "appName": fields.Str(required=True),
    "types": fields.DelimitedList(fields.Str(), missing=None, required=False)
})
def get_user_preference_by_email(email, appName, types):
    result = UserPreference.get_by_email_app_types(email, appName, types)

    return jsonify(
        data={item.type: item.val_obj for item in result}
    )
