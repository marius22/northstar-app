from werkzeug import exceptions
from flask import Blueprint, request, current_app, jsonify
from ns.util.producer import Producer, DummyProducer, ExportCSVMessage, ExportWithDedupeMessage, \
    ExportSFDCMessage, ExportMarketoMessage, ExportSegmentToCSVWithoutDedupeMessage


blueprint = Blueprint('message', __name__)


# this api is used for segment publish to csv without dedupe in V2
@blueprint.route('/message/csv_export', methods=['POST'])
def create_csv_export_message_without_dedupe_v2():
    data = request.get_json()

    expected_fields = {'user_id', 'user_email', 'account_id', 'segment_id', 'num_companies', 'segment_name',
                       'titles', 'departments', 'contact_limit_per_company', 'csv_url'}
    if set(data.keys()) != expected_fields:
        raise exceptions.BadRequest("Missing fields in post data")

    message = ExportSegmentToCSVWithoutDedupeMessage(data)
    producer = get_kafka_producer()
    producer.send_message(message)

    return jsonify(status="OK")


@blueprint.route('/message/publish_audience', methods=['POST'])
def create_export_message():
    data = request.get_json()
    if not data or ('publish_type' not in data.keys()):
        raise exceptions.BadRequest("Missing fields in post data")
    type = data['publish_type']
    if type == "CSV0":
        message = ExportCSVMessage(data)
        message.validate(data)
    elif type == "CSV":
        message = ExportWithDedupeMessage(data)
        message.validate(data)
    elif type == "SFDC":
        message = ExportSFDCMessage(data)
        message.validate(data)
    elif type == "MAS":
        message = ExportMarketoMessage(data)
        message.validate(data)
    else:
        pass
    producer = get_kafka_producer()
    producer.send_message(message)

    return jsonify(status="OK")


def get_kafka_producer():
    use_kafka = current_app.config.get('KAFKA_ENABLED')
    if use_kafka:
        brokers = current_app.config.get('KAFKA_BROKERS')
        producer = Producer.get_instance(brokers)
    else:
        producer = DummyProducer()

    current_app.logger.debug("producer: %s" % producer)

    return producer
