"""
Service for submitting model run job requests
"""
import json
from flask import Blueprint, request, jsonify, current_app, copy_current_request_context
from ns.model.job import Job
from ns.model.segment import Segment
from ns.util.producer import DummyProducer, ModelRunMessage, Producer
from werkzeug.exceptions import NotFound, BadRequest
import time
from ns.util.producer import SegmentFeatureAnalysis
from webargs.flaskparser import use_kwargs
from webargs import fields
from ns.model.user import User
from ns.model.seed import Seed
from ns.model.seed_candidate import SeedCandidate

blueprint = Blueprint('JobSubmission', __name__)


@blueprint.route(
    '/jobs/<int:job_id>/recommendations/user/<int:user_id>/segment/<int:segment_id>/job_submission',
    methods=['POST'])
def create_model_run_request(job_id, user_id, segment_id):
    data = request.get_json()

    # Verifies
    seed_list = data.get('seed_list')
    if not seed_list:
        raise BadRequest("seed list is required")

    qualification_criterias = data.get('qualification_criterias', [])
    expansion = data.get('expansion', [])

    segment = Segment.get_by_id(segment_id)
    if not segment:
        raise NotFound('Segment(ID = %d) is not found' % segment_id)

    job = Job.get_by_id(job_id)
    if not job:
        raise NotFound('Job(ID = %d) is not found' % job_id)

    use_kafka = current_app.config.get('KAFKA_ENABLED')
    if use_kafka:
        brokers = current_app.config.get('KAFKA_BROKERS')
        producer = Producer.get_instance(brokers)
    else:
        producer = DummyProducer()

    current_app.logger.info("Before produce:" + str(time.time()))

    user = User.get_by_id(user_id)
    message = ModelRunMessage(user.account_id, user_id, segment_id,
                              job_id, seed_list, qualification_criterias, expansion)
    producer.send_message(message)

    current_app.logger.info("After produce:" + str(time.time()))
    return jsonify(status="OK")


@blueprint.route(
    '/jobs/segment_jobs/<int:job_id>/feature_analysis', methods=['POST'])
def segment_feature_analysis(job_id):

    # get account_id, job_type
    job = Job.get_by_id(job_id)
    assert job, "Invalid job"

    segment_id = job.segment_id
    assert segment_id, "Segment job must has a segmentId"

    segment = Segment.get_by_id(segment_id)
    account_id = segment.owner.account_id

    seed_id = job.seed_id
    seed_candidate_ids = json.loads(Seed.get_by_id(seed_id).seed_candidate_ids)
    seed_candidates = SeedCandidate.get_by_ids(seed_candidate_ids)
    domains = [seed.domain for seed in seed_candidates]

    msg = SegmentFeatureAnalysis(account_id, segment_id, job_id, Job.FEATURE_ANALYSIS, domains)
    Producer.get_default_instance().send_message(msg)

    return jsonify(
        status="OK"
    )
