
from flask import Blueprint, request, current_app, jsonify
from ns.util.producer import Producer, DummyProducer, ExportCSVMessage
from werkzeug import exceptions
from webargs import fields
from webargs.flaskparser import use_kwargs, use_args
from ns.model.api_token import ApiToken

blueprint = Blueprint('apiToken', __name__)


@blueprint.route('/apiToken', methods=['GET'])
@use_kwargs({
    "token": fields.Str(),
    "app_version": fields.Str()
})
def get_api_token(token, app_version):
    db_token = ApiToken.get_by_token_and_app_version(token, app_version)
    return jsonify(
        data=db_token.serialized if db_token else None
    )


@blueprint.route('/apiToken', methods=['POST'])
@use_args({
    'token': fields.Nested({
        'token': fields.Str(),
        'app_version': fields.Str(),
        'ip': fields.Str()
    }, required=True)
})
def create_api_token(args):
    token = args.get("token")
    saved_token = ApiToken.create_api_token(
        token.get("token"),
        token.get("app_version"),
        ip=token.get("ip")
    )
    return jsonify(
        data=saved_token.serialized
    )


@blueprint.route('/lockApiToken', methods=['POST'])
@use_args({
    'token': fields.Str()
})
def lock_api_token(args):
    token = args.get("token")

    db_tokens = ApiToken.get_by_token(token)
    if not db_tokens:
        raise exceptions.NotFound("Not Found")

    for token in db_tokens:
        token.update_lock_status(True)

    return jsonify(
        status="OK"
    )


@blueprint.route('/unlockApiToken', methods=['POST'])
@use_args({
    'token': fields.Str()
})
def unlock_api_token(args):
    token = args.get("token")

    db_tokens = ApiToken.get_by_token(token)
    if not db_tokens:
        raise exceptions.NotFound("Not Found")

    for token in db_tokens:
        token.update_lock_status(False)

    return jsonify(
        status="OK"
    )
