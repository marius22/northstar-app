# -*- coding: utf-8 -*-
from flask import Blueprint, request
from ns.util.marketo.api import Marketo

blueprint = Blueprint('mas_lead', __name__)


@blueprint.route('/mas/marketo/leads', methods=['POST'])
def upsert_leads():
    return ""


@blueprint.route('/mas/marketo/leads_to_list', methods=['POST'])
def add_leads_to_list():
    return ""
