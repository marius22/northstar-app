from flask import jsonify, Blueprint, request
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.model.company_insight import CompanyInsight
from webargs.flaskparser import use_kwargs
from webargs import fields
from webargs.flaskparser import use_args
from ns.util.company_db_client import CompanyDBClient


blueprint = Blueprint('insights', __name__)


@blueprint.route('/jobs/<int:job_id>/accountInsights/summary', methods=['POST'])
def save_account_insights(job_id):
    # need to change to get account id by jobid
    data = request.get_json()
    insights = data.get("insights", [])
    records = []
    for insight in insights:
        record = {}
        record["category"] = insight.get("category")
        record["indicator"] = insight.get("indicator")
        record["display_name"] = insight.get("displayValue")
        records.append(record)
    IndicatorWhiteList.save_indicators(records)
    return jsonify(status="ok")


@blueprint.route('/sfdc_app/domain_indicators', methods=['POST'])
@use_kwargs({
    "domain": fields.Str(),
    "indicators": fields.DelimitedList(fields.Str())
})
def domain_indicators(domain, indicators):

    domain_indicators = CompanyDBClient.instance().get_domain_indicators(domain)
    valid_indicators = set(indicators).intersection(set(domain_indicators))

    result = IndicatorWhiteList.get_by_indicators(valid_indicators)

    return jsonify(
        data=[item.serialized for item in result]
    )


@blueprint.route('/company_insights/info', methods=['GET'])
@use_args({
    'insightIds': fields.DelimitedList(fields.Int(), required=True)
})
def get_insights(args):
    insight_ids = args.get('insightIds')
    company_insights = CompanyInsight.query.filter(CompanyInsight.id.in_(insight_ids)).all()
    insights_info = list()
    if company_insights:
        insights_info = [company_insight.serialized for company_insight in company_insights]
    return jsonify(data=insights_info)


@blueprint.route('/company_insights', methods=['GET'])
def get_all_insights():
    company_insights = CompanyInsight.query.all()
    insights_info = list()
    if company_insights:
        insights_info = [company_insight.serialized for company_insight in company_insights]
    return jsonify(data=insights_info)
