import httplib
import logging
import sys
import traceback
from ns.util.events import events
from datetime import datetime
from flask import jsonify
from ns.controller.service.crm_api.sfdc.simple_salesforce import Salesforce

SFDC_VERSION = "36.0"
CHECKING_FIELDS = ["token", "instance_url", "data"]
SFDC_OBJECT_LEAD = 'Lead'
SFDC_OBJECT_ACCOUNT = 'Account'
SFDC_OBJECT_CONTACT = 'Contact'
SFDC_OBJECT_SEGMENT_LEAD = 'Segment_Lead'
SFDC_OBJECT_SEGMENT_ACCOUNT = 'Segment_Account'
SFDC_OBJECT_SEGMENT_CONTACT = 'Segment_Contact'
SFDC_OBJECT_ACCOUNT_AUDIENCE = 'AccountAudience'
SFDC_OBJECT_LEAD_AUDIENCE = 'LeadAudience'
SFDC_OBJECT_CONTACT_AUDIENCE = 'ContactAudience'
SFDC_OBJECT_ACCOUNT_SCORE = 'AccountScore'
SFDC_OBJECT_LEAD_SCORE = 'LeadScore'
SFDC_OBJECT_MODEL = 'Model'
SFDC_OBJECT_ACCOUNT_AUDIENCE_EXTERNAL_ID_NAME = 'UniqueKey'
SFDC_OBJECT_CONTACT_AUDIENCE_EXTERNAL_ID_NAME = 'UniqueKey'
SFDC_OBJECT_LEAD_AUDIENCE_EXTERNAL_ID_NAME = 'UniqueKey'
SFDC_OBJECT_ACCOUNT_SCORE_EXTERNAL_ID_NAME = 'UniqueKey'
SFDC_OBJECT_LEAD_SCORE_EXTERNAL_ID_NAME = 'UniqueKey'
SFDC_OBJECT_MODEL_EXTERNAL_ID_NAME = 'ESModelId'
SFDC_OBJECT_AUDIENCE = 'Audience'
SFDC_OBJECT_AUDIENCE_EXTERNAL_ID_NAME = 'ESAudienceId'
SFDC_FIELD_PREFIX = "ES_APP__"
SFDC_FIELD_SUFFIX = "__c"

SFDC_OBJECT_TYPES = [SFDC_OBJECT_LEAD, SFDC_OBJECT_ACCOUNT, SFDC_OBJECT_CONTACT]


logger = logging.getLogger(__name__)


def get_sfdc_column_name(field_name):
    return "".join([SFDC_FIELD_PREFIX, field_name, SFDC_FIELD_SUFFIX])


def check_api_body(data):
    try:
        check_sfdc_parameters(data)
    except TokenException as e:
        return jsonify(error_message="Exception: " + str(e)), httplib.BAD_REQUEST
    es_data_list = data.get("data")
    if not es_data_list:
        return jsonify(sfdc_accounts_id=[])


def check_sfdc_parameters(data):
    for field in CHECKING_FIELDS:
        if not data.get(field):
            raise TokenException("missing " + field)


def insert_object_into_sfdc(sfdc_object_type, data):
    return operate_sfdc(sfdc_object_type, data, 'insert')


def update_object_in_sfdc(sfdc_object_type, data):
    return operate_sfdc(sfdc_object_type, data, 'update')


def upsert_object_in_sfdc(sfdc_object_type, data, external_id_name):
    return operate_sfdc(sfdc_object_type, data, 'upsert', external_id_name)


def operate_sfdc(sfdc_object_type, data, operation_type, external_id_name=None):

    try:
        client = Salesforce(
            version=SFDC_VERSION, session_id=data.get('token'),
            instance_url=data.get('instance_url'))

        create_job_data = {
            'operation': operation_type,
            'object': sfdc_object_type,
            'contentType': 'JSON'
        }
        if external_id_name and operation_type == 'upsert':
            create_job_data["externalIdFieldName"] = external_id_name

        job = client.async_Job.create(create_job_data)

        # job.add_batches_from_json(data.get("data", []))
        data_chunks = chunks(data.get("data", []), 2000)
        for data_chunk in data_chunks:
            job.add_batches_from_json(data_chunk)

        job.close()
        job.await_completion(retry_cnt=120)
        batches_results = job.get_batches_results()
        check_result_status(batches_results)
        sfdc_object_ids = []
        for batches_result in batches_results:
            sfdc_object_ids.extend([result.get("id") for result in batches_result])
        return sfdc_object_ids
    except Exception, ex:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        error_msgs = traceback.format_exception(exc_type, exc_value, exc_traceback)
        events.event("salesforce api call failure", "\n".join(error_msgs), alert_type="error")
        logger.error("salesforce api call failure: {error}".format(error=ex))
        raise


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


def check_result_status(results):
    """
    find the failed records and log them or send email to someone
    :param results:
    :return:
    """

    records = list()
    for batches_result in results:
        records.extend(batches_result)
    records = [record for record in records if not record.get("success")]
    error_map = dict()
    for record in records:
        errors = record.get("errors", [])
        if errors:
            for one_error in errors:
                error_key = one_error.get("statusCode", "") + ": " + one_error.get("message", "")
                if error_map.get(error_key):
                    list_temp = error_map.get(error_key)
                    list_temp.extend([record["id"]])
                    error_map[error_key] = list_temp
                else:
                    error_map[error_key] = [record["id"]]

    for error_key, record_ids in error_map.iteritems():
        logger.warning("error happen when call salesforce api, error: {error}, records ids: {ids}".
                       format(error=error_key, ids=set(record_ids)))
        events.event("salesforce api call failure", "error: {error}, records ids: {ids}".format(
            error=error_key, ids=set(record_ids)), alert_type="error")


def publish_to_sfdc(operate_function, data):
    logger.info("start publishing ")
    starttime = datetime.now()
    check_result = check_api_body(data)
    if check_result:
        return check_result
    sfdc_object_ids = operate_function(data)
    endtime = datetime.now()
    logger.info("end publishing: spend %s seconds" % (endtime - starttime).seconds)
    return jsonify(sfdc_object_ids=sfdc_object_ids, status_code=httplib.OK)


class TokenException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
