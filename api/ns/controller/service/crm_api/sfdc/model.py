from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_MODEL, SFDC_OBJECT_MODEL_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_model', __name__)


@blueprint.route('/crm/sfdc/models', methods=['POST'])
def upsert_models():
    return publish_to_sfdc(upsert_models_in_sfdc, request.get_json())


def upsert_models_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_MODEL), data,
                                 get_sfdc_column_name(SFDC_OBJECT_MODEL_EXTERNAL_ID_NAME))
