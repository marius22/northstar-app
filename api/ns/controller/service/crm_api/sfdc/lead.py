from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import insert_object_into_sfdc, update_object_in_sfdc, publish_to_sfdc, \
    SFDC_OBJECT_LEAD

blueprint = Blueprint('sfdc_lead', __name__)


@blueprint.route('/crm/sfdc/leads', methods=['PUT'])
def update_leads():
    return publish_to_sfdc(update_leads_into_sfdc, request.get_json())


@blueprint.route('/crm/sfdc/leads', methods=['POST'])
def insert_leads():
    return publish_to_sfdc(insert_leads_into_sfdc, request.get_json())


def insert_leads_into_sfdc(data):
    return insert_object_into_sfdc(SFDC_OBJECT_LEAD, data)


def update_leads_into_sfdc(data):
    return update_object_in_sfdc(SFDC_OBJECT_LEAD, data)
