from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_ACCOUNT_AUDIENCE, SFDC_OBJECT_ACCOUNT_AUDIENCE_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_account_audience', __name__)


@blueprint.route('/crm/sfdc/account_audiences', methods=['POST'])
def upsert_account_audiences():
    return publish_to_sfdc(upsert_account_audiences_in_sfdc, request.get_json())


def upsert_account_audiences_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_ACCOUNT_AUDIENCE),
                                 data, get_sfdc_column_name(SFDC_OBJECT_ACCOUNT_AUDIENCE_EXTERNAL_ID_NAME))
