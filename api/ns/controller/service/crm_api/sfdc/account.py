from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import insert_object_into_sfdc, update_object_in_sfdc, publish_to_sfdc, \
    SFDC_OBJECT_ACCOUNT

blueprint = Blueprint('sfdc_account', __name__)


@blueprint.route('/crm/sfdc/accounts', methods=['PUT'])
def update_accounts():
    return publish_to_sfdc(update_accounts_into_sfdc, request.get_json())


@blueprint.route('/crm/sfdc/accounts', methods=['POST'])
def insert_accounts():
    return publish_to_sfdc(insert_accounts_into_sfdc, request.get_json())


def insert_accounts_into_sfdc(data):
    return insert_object_into_sfdc(SFDC_OBJECT_ACCOUNT, data)


def update_accounts_into_sfdc(data):
    return update_object_in_sfdc(SFDC_OBJECT_ACCOUNT, data)
