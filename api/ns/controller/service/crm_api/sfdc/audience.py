from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_AUDIENCE, SFDC_OBJECT_AUDIENCE_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_audience', __name__)


@blueprint.route('/crm/sfdc/audiences', methods=['POST'])
def upsert_audiences():
    return publish_to_sfdc(upsert_audiences_in_sfdc, request.get_json())


def upsert_audiences_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_AUDIENCE), data,
                                 get_sfdc_column_name(SFDC_OBJECT_AUDIENCE_EXTERNAL_ID_NAME))
