from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_CONTACT_AUDIENCE, SFDC_OBJECT_CONTACT_AUDIENCE_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_contact_audience', __name__)


@blueprint.route('/crm/sfdc/contact_audiences', methods=['POST'])
def upsert_contact_audiences():
    return publish_to_sfdc(insert_contact_audiences_in_sfdc, request.get_json())


def insert_contact_audiences_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_CONTACT_AUDIENCE), data,
                                 get_sfdc_column_name(SFDC_OBJECT_CONTACT_AUDIENCE_EXTERNAL_ID_NAME))
