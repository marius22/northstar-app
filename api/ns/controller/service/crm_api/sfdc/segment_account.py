from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import insert_object_into_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_SEGMENT_ACCOUNT

blueprint = Blueprint('sfdc_segment_account', __name__)


@blueprint.route('/crm/sfdc/segment_accounts', methods=['POST'])
def insert_segment_accounts():
    return publish_to_sfdc(insert_segment_accounts_into_sfdc, request.get_json())


def insert_segment_accounts_into_sfdc(data):
    return insert_object_into_sfdc(get_sfdc_column_name(SFDC_OBJECT_SEGMENT_ACCOUNT), data)
