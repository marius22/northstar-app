from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_ACCOUNT_SCORE, SFDC_OBJECT_ACCOUNT_SCORE_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_account_score', __name__)


@blueprint.route('/crm/sfdc/account_scores', methods=['POST'])
def upsert_account_scores():
    return publish_to_sfdc(upsert_account_scores_in_sfdc, request.get_json())


def upsert_account_scores_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_ACCOUNT_SCORE), data,
                                 get_sfdc_column_name(SFDC_OBJECT_ACCOUNT_SCORE_EXTERNAL_ID_NAME))
