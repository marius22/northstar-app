from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import insert_object_into_sfdc, update_object_in_sfdc, \
    publish_to_sfdc, SFDC_OBJECT_CONTACT

blueprint = Blueprint('sfdc_contact', __name__)


@blueprint.route('/crm/sfdc/contacts', methods=['PUT'])
def update_contacts():
    return publish_to_sfdc(update_contacts_into_sfdc, request.get_json())


@blueprint.route('/crm/sfdc/contacts', methods=['POST'])
def insert_contacts():
    return publish_to_sfdc(insert_contacts_into_sfdc, request.get_json())


def insert_contacts_into_sfdc(data):
    return insert_object_into_sfdc(SFDC_OBJECT_CONTACT, data)


def update_contacts_into_sfdc(data):
    return update_object_in_sfdc(SFDC_OBJECT_CONTACT, data)
