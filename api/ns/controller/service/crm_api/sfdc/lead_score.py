from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import upsert_object_in_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_LEAD_SCORE, SFDC_OBJECT_LEAD_SCORE_EXTERNAL_ID_NAME

blueprint = Blueprint('sfdc_lead_score', __name__)


@blueprint.route('/crm/sfdc/lead_scores', methods=['POST'])
def upsert_lead_scores():
    return publish_to_sfdc(upsert_lead_scores_in_sfdc, request.get_json())


def upsert_lead_scores_in_sfdc(data):
    return upsert_object_in_sfdc(get_sfdc_column_name(SFDC_OBJECT_LEAD_SCORE), data,
                                 get_sfdc_column_name(SFDC_OBJECT_LEAD_SCORE_EXTERNAL_ID_NAME))
