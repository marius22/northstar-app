from flask import Blueprint, request
from ns.controller.service.crm_api.sfdc.utils import insert_object_into_sfdc, get_sfdc_column_name, publish_to_sfdc, \
    SFDC_OBJECT_SEGMENT_CONTACT

blueprint = Blueprint('sfdc_segment_contact', __name__)


@blueprint.route('/crm/sfdc/segment_contacts', methods=['POST'])
def insert_segment_accounts():
    return publish_to_sfdc(insert_segment_contacts_into_sfdc, request.get_json())


def insert_segment_contacts_into_sfdc(data):
    return insert_object_into_sfdc(get_sfdc_column_name(SFDC_OBJECT_SEGMENT_CONTACT), data)
