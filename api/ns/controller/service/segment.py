import httplib
import logging
import uuid
import os

import time
import datetime
import unicodecsv
from flask import Blueprint, current_app, jsonify, request
from ns.model.account_quota import AccountQuota
from ns.model.s3client_default import S3ClientDefault
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from ns.util.webargs_parser import OrderField, DictField
from ns.model.user import User
from ns.model.segment import Segment
from werkzeug import exceptions
from ns.controller.app.param_validator import check_account_id, check_segment_id
from ns.model.recommendation import Recommendation
from ns.model.sfdc_config import SFDCConfig
from ns.util.company_db_client import CompanyDBClient
from ns.model.account import Account
from ns.model.indicator_white_list import IndicatorWhiteList

blueprint = Blueprint('segment', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/segments/<int:segment_id>/companies', methods=['GET'])
@use_args({
    'limit': fields.Int(),
    'offset': fields.Int(),
    'format': fields.Str(missing='csv', validate=lambda fmt: fmt in ['csv']),
    'order': OrderField(),
    'where': DictField(),
    'dedupe': fields.Boolean(),
    'onlyExported': fields.Boolean(),
    'userId': fields.Int(required=True)
})
def get_segment_companies(args, segment_id):

        user = User.get_by_id(args.get("userId"))
        segment = Segment.get_by_id(segment_id)

        segment.check_exported_csv_permission(user)

        start_time = time.time()
        # get recommendations and generate the tmp file
        tmp_csv_path, is_exceed = segment.get_page_result(user, args, False, True)
        end_time = time.time()
        logger.info("Get recommendation results: %ds" % (end_time - start_time))
        start_time = end_time

        # upload to s3
        csv_name = "%s_account.csv" % segment.name
        with open(tmp_csv_path) as csv_file:
            csv_s3_key, csv_s3_url = upload_to_s3(csv_name, csv_file)
        end_time = time.time()
        logger.info("Upload to s3 results: %ds" % (end_time - start_time))
        start_time = end_time
        delete_csv_file(tmp_csv_path)

        return jsonify(data={'company_csv_s3_key': csv_s3_key,
                             'company_csv_s3_url': csv_s3_url,
                             'is_exceed': is_exceed})


def delete_csv_file(file_path):
    try:
        os.remove(file_path)
    except:
        logger.error('Delete temp csv file failed')


@blueprint.route('/segments/<int:segment_id>/sector_companies', methods=['GET'])
@use_args({
    'sectors': fields.DelimitedList(
        fields.Str(), required=True,
        validate=lambda x: set(x).issubset(['1', '2', '3', '4', '5', '6', '7'])),
    'format': fields.Str(missing='csv', validate=lambda fmt: fmt in ['csv']),
    'dedupe': fields.Boolean(missing=False),
    'limit': fields.Int(fields.Str(), required=True),
    'userId': fields.Int(required=True)
})
def export_csv_by_sectors(args, segment_id):

    user = User.get_by_id(args.get("userId"))
    segment = Segment.get_by_id(segment_id)

    segment.check_exported_csv_permission(user)

    # generate csv path
    start_time = time.time()
    tmp_csv_path, is_exceed = segment.get_companies_by_sectors(args, user)
    end_time = time.time()
    logger.info("Get recommendation results: %ds" % (end_time - start_time))
    start_time = end_time

    # upload to s3
    csv_name = "%s_account.csv" % segment.name
    with open(tmp_csv_path) as csv_file:
        csv_s3_key, csv_s3_url = upload_to_s3(csv_name, csv_file)
    end_time = time.time()
    logger.info("Upload to s3 results: %ds" % (end_time - start_time))
    start_time = end_time
    delete_csv_file(tmp_csv_path)

    return jsonify(data={'company_csv_s3_key': csv_s3_key,
                         'company_csv_s3_url': csv_s3_url,
                         'is_exceed': is_exceed})


def upload_to_s3(company_csv_name, company_csv):
    s3client = S3ClientDefault.get_instance()
    env = current_app.config.get('CURRENT_ENV')
    company_csv_s3_key = env + '/' + uuid.uuid1().hex
    expired_time = current_app.config.get('S3_EXPIRED_TIME')
    url = s3client.put_csv(company_csv_name, company_csv_s3_key, company_csv, expired_time)
    return company_csv_s3_key, url


def extract_company_ids(company_csv_path):
    company_ids = []
    with open(company_csv_path, 'rb') as f:
        reader = unicodecsv.DictReader(f)
        if 'companyId' in set(reader.fieldnames):
            for row in reader:
                company_ids.append(int(row['companyId']))
        else:
            err_msg = "Found no 'companyId' in CSV: %s", company_csv_path
            logger.error(err_msg)
            raise exceptions.InternalServerError(err_msg)

    return company_ids


@blueprint.route('/sfdc_app/account_fit_score', methods=['GET', 'POST'])
@use_kwargs({
    "account_id": fields.Int(),
    "domains": fields.DelimitedList(fields.Str(), required=True)

})
def get_account_fit_score(account_id, domains):
    """
    Api for query account fit score of domain: domain
    :param accountId:
    :param domains:
    :return:
    """
    result = CompanyDBClient.instance().get_account_fit_score(account_id, domains)

    return jsonify(
        data=result
    )


@blueprint.route('/sfdc_app/owner_segments', methods=['GET'])
@use_kwargs({
    "account_id": fields.Int(),
    "domain": fields.Str()

})
def owner_segments(account_id, domain):

    # query segments of account: accountId -> segments
    account = check_account_id(account_id)
    segments = account.owned_segments.all()

    job_segment_mapping = {}
    for segment in segments:
        latest_job = segment.latest_completed_job
        if latest_job:
            job_segment_mapping[latest_job.id] = segment

    # query recommendation by domain and job_ids: job_id -> fitScore
    # sort by recommendation's score desc
    recommendations = Recommendation \
        .get_by_job_ids_and_domain(job_segment_mapping.keys(), domain)

    result = []

    if recommendations:
        result.extend(_build_result_for_segments(job_segment_mapping, recommendations))
    if account.fit_model_finished():
        result.extend(_build_result_for_default_fit(account_id, domain))

    return jsonify(
        data=result
    )


def _build_result_for_segments(job_segment_mapping, recommendations):

    result = []
    for recommendation in recommendations:
        segment = job_segment_mapping.get(recommendation.job_id)

        result.append(
            {
                "id": segment.id,
                "name": segment.name,
                "fitScore": recommendation.score
            }
        )

    return result


def _build_result_for_default_fit(account_id, domain):
    db_client = CompanyDBClient.instance()
    account_fit_score = db_client.get_account_fit_score(account_id, [domain]).get(domain)
    return [{
        "id": None,
        "name": "Default",
        "fitScore": account_fit_score
    }]


def _get_now():
    return datetime.datetime.utcnow()


@blueprint.route('/sfdc_app/segment_indicators', methods=['GET'])
@use_kwargs({
    "segment_id": fields.Int(),
    "domain": fields.Str()
})
def segment_indicators(segment_id, domain):
    start = _get_now()
    logger.info("segment indicators: start")

    segment = check_segment_id(segment_id)
    account_id = segment.owner.account_id

    db_client = CompanyDBClient.instance()
    insights = db_client.get_segment_indicators(account_id, segment_id, domain)
    logger.info("segment indicators: query indicators, len: {%s}, time cost: {%s}"
                % (len(insights), (_get_now() - start)))

    start = _get_now()
    result = [item.serialized for item in _filter_insights(segment.owner.account_id, insights)]
    logger.info("segment indicators: filter indicators, time cost: {%s}" % (_get_now() - start))

    return jsonify(
        data=result
    )


@blueprint.route('/sfdc_app/account_indicators', methods=['GET'])
@use_kwargs({
    "account_id": fields.Int(),
    "domain": fields.Str()
})
def account_indicators(account_id, domain):

    check_account_id(account_id)

    db_client = CompanyDBClient.instance()
    insights = db_client.get_account_indicators(account_id, domain)

    return jsonify(
        data=[item.serialized
              for item in _filter_insights(account_id, insights)]
    )


def _filter_insights(account_id, insights):

    start = _get_now()
    if not insights:
        return []
    # get all white list indicators
    white_list = IndicatorWhiteList.get_all_indicators()
    white_list_mapping = {item.indicator: item for item in white_list}
    logger.info("segment indicators: prepare white list, len: {%s} time cost: {%s}"
                % (len(white_list), (_get_now() - start)))

    start = _get_now()
    # get black list
    canvas_config = SFDCConfig.get_by_account_id(account_id)
    black_list = canvas_config.get_black_list() if canvas_config else []
    logger.info("segment indicators: prepare black list, time cost: {%s}" % (_get_now() - start))

    start = _get_now()
    indicators = [item['indicator'] for item in insights]

    valid_indicators = (set(white_list_mapping.keys()) - set(black_list))\
        .intersection(set(indicators))
    logger.info("segment indicators: intersection, time cost: {%s}" % (_get_now() - start))

    start = _get_now()
    result = [white_list_mapping.get(indicator) for indicator in indicators
              if indicator in valid_indicators]

    logger.info("segment indicators: prepare result, time cost: {%s}" % (_get_now() - start))
    return result
