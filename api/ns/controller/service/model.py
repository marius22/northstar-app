from xml.dom import NotSupportedErr
from flask import jsonify, Blueprint, current_app, request, g, render_template
from webargs.flaskparser import use_kwargs, use_args
from webargs import fields
from werkzeug import exceptions
from ns.model.model import Model
from ns.model.user import User
from ns.model.seed_company import SeedCompany
from ns.util.producer import AccountModelRunMessage, Producer,\
    FitModelMessage, FeatureAnalysisMessage
from ns.util.email_client import NSEmailClient
from ns.controller.app.param_validator import check_and_get_model
import logging

blueprint = Blueprint('model', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/models/<int:model_id>/submit', methods=["POST"])
def submit_model(model_id):

    model = check_and_get_model(model_id)

    domain_url = "/model_seeds/%s/domains" % model.model_seed_id

    model_type = Model.FIT_MODEL if model.type == Model.ACCOUNT_FIT else model.type

    if model_type == Model.FIT_MODEL:
        msg = FitModelMessage(model.id, model_type, domain_url)
    elif model_type == Model.FEATURE_ANALYSIS:
        msg = FeatureAnalysisMessage(model.id, model_type, domain_url)
    else:
        raise NotSupportedErr("Galaxy not supported")

    Producer.get_default_instance().send_message(msg)

    model.update_status(Model.IN_QUEUE)

    return jsonify(
        status="OK"
    )


@blueprint.route('/model_seeds/<int:seed_id>/domains', methods=['GET'])
def get_model_seeds(seed_id):
    seeds = SeedCompany.get_by_model_seed_id(seed_id)

    return jsonify(
        data=[item.domain for item in seeds]
    )


@blueprint.route('/models/<int:model_id>/status', methods=['POST'])
@use_kwargs({
    "status": fields.Str(required=True, ),
    "model_type": fields.Str()
})
def update_model_status(model_id, model_type, status):

    if status == "JobCompleted":

        # use try except to silently fail when model_id doesn't exist
        try:
            model = check_and_get_model(model_id)
        except exceptions.BadRequest as ex:
            logger.error(ex)
            return jsonify(
                status="OK",
                message="Model id {0} not found".format(model_id)
            )

        model.update_status(Model.COMPLETED)
        if Model.all_models_in_batch_complete(model.batch_id):
            model_owner = User.get_by_id(model.owner_id)
            if model_owner:
                NSEmailClient.send_email(
                    model_owner.email,
                    "Your Model is Ready",
                    render_template("model_run_completed.html",
                                    model_name=model.name,
                                    eap_link=current_app.config.get("NS_STAR_APP_HOST")
                                    )
                )
            customer_success_emails = current_app.config.get("CUSTOMER_SUCCESS_EMAILS")
            if customer_success_emails:
                title = "Model is Ready"
                if model_owner:
                    title = title + " for " + model_owner.email
                NSEmailClient.send_email(
                    customer_success_emails,
                    title,
                    render_template("model_run_completed.html",
                                    model_name=model.name,
                                    eap_link=current_app.config.get("NS_STAR_APP_HOST")
                                    )
                )
    else:
        model = check_and_get_model(model_id)
        model.update_status(status)

    return jsonify(
        status="OK"
    )


@blueprint.route('/models/info', methods=['GET'])
@use_args({
    'modelIds': fields.DelimitedList(fields.Int(), required=True)
})
def get_models(args):
    model_ids = args.get('modelIds')
    models = Model.query.filter(Model.id.in_(model_ids)).all()
    models_info = list()
    if models:
        models_info = [model.serialized for model in models]
    return jsonify(data=models_info)
