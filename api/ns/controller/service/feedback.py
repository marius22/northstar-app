"""
Feedback service
"""
import httplib
import time
import logging

from flask import Blueprint, request, jsonify, current_app
from ns.model.job import Job
from ns.util.producer import Producer
from werkzeug.exceptions import NotFound, BadRequest
from webargs import fields
from webargs.flaskparser import use_kwargs, use_args
from ns.util.string_util import is_not_blank
from ns.model.company_new import CompanyNew


blueprint = Blueprint('Feedback', __name__)


@blueprint.route(
    '/jobs/<int:job_id>/recommendations/<string:domain>/feedback/<string:feedback>',
    methods=['POST'])
def recommendation_feedback(job_id, domain, feedback):
    job = Job.query.get(job_id)
    if not job:
        raise NotFound('Job(Id = %d) is not found' % job_id)

    feedback = feedback.encode('utf-8')
    domain = domain.encode('utf-8')

    # convert feedback to boolean
    if feedback == "like":
        feedback_bool = True
    elif feedback == "dislike":
        feedback_bool = False
    else:
        raise BadRequest('Feedback is not like | dislike')

    # TODO save feedback in db

    if current_app.config.get("KAFKA_ENABLED"):
        # get data from config
        broker_lst_str = current_app.config.get('KAFKA_BROKER', '')
        broker_lst = broker_lst_str.split(',')
        topic = current_app.config.get('KAFKA_TOPIC_FEEDBACK', '')

        current_app.logger.info("Before produce:" + str(time.time()))
        Producer(broker_lst).recommendation_feedback(
            topic, job_id, domain, feedback_bool)
        current_app.logger.info("After produce:" + str(time.time()))

    return jsonify(status="OK")


@blueprint.route('/add_new_domain', methods=['PUT'])
@use_args({
    "domain": fields.Str(validate=is_not_blank)
})
def add_new_domain(args):
    db_domain = CompanyNew.add_new_domain(args.get("domain"))
    return jsonify(
        data=db_domain.serialized
    )
