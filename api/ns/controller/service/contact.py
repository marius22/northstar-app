from collections import defaultdict
import httplib
import logging
import uuid
import os
import time
import unicodecsv
from flask import Blueprint, current_app, jsonify, request
from ns.model.account_quota import AccountQuota
from ns.model.s3client_default import S3ClientDefault
from webargs import fields
from webargs.flaskparser import use_args
from ns.util.contact_helper import ContactHelper
from ns.util.data_api_client import DataAPIClient, ContactEmail
from ns.util.webargs_parser import OrderField, DictField
from ns.model.user import User
from ns.model.segment import Segment
from werkzeug import exceptions
from ns.model.account import Account
from ns.model.account_exported_contact import AccountExportedContact
from sqlalchemy.exc import IntegrityError
from ns.controller.app.param_validator import check_user_id

blueprint = Blueprint('contact_managerment', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/contacts/quota_remaining', methods=['GET'])
@use_args({
    'account_id': fields.Int(required=True)
})
def quota_remaining(args):
    account = _get_account(args['account_id'])

    return jsonify(data=account.account_quota.remaining_contact_cnt)


@blueprint.route('/contacts/contacts_check', methods=['POST'])
@use_args({
    'contact_ids': fields.DelimitedList(fields.Int(), required=True),
    'account_id': fields.Int(required=True)
})
def contacts_check(args):
    account = _get_account(args['account_id'])
    new_contact_set, exist_contact_ids_set = account.get_contact_ids_set(
        account, args['contact_ids'])
    data = dict()
    data["new_contact_ids"] = list(new_contact_set)
    data["exist_contact_ids"] = list(exist_contact_ids_set)
    return jsonify(data=data)


@blueprint.route('/contacts/contacts_track', methods=['POST'])
@use_args({
    'contact_ids': fields.DelimitedList(fields.Int(), required=True),
    'account_id': fields.Int(required=True),
    'user_id': fields.Int(required=True)
})
def save_contacts(args):
    account = _get_account(args['account_id'])
    user = check_user_id(args['user_id'])
    if user.is_es_admin() or user.is_super_admin():
        return jsonify(status='OK')
    new_contact_set, exist_contact_ids_set = account.get_contact_ids_set(
        account, args['contact_ids'])
    try:
        AccountExportedContact.create_exports(
            args['account_id'], list(new_contact_set))
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(status='OK')


def _get_account(account_id):
    # type: (Account) -> account
    account = Account.get_by_id(account_id)
    if not account:
        raise exceptions.BadRequest("Invalid account_id")
    return account


@blueprint.route('/contacts/admin_check', methods=['GET'])
@use_args({
    'user_id': fields.Int(required=True)
})
def admin_check(args):
    user = check_user_id(args['user_id'])
    if user.is_es_admin() or user.is_super_admin():
        return jsonify(status=True)
    else:
        return jsonify(status=False)


@blueprint.route('/contacts/num_contact', methods=['GET', 'POST'])
@use_args({
    "companyIds": fields.DelimitedList(fields.Int(), required=True,
                                       validate=lambda l: len(l) > 0),
    "titles": fields.List(DictField(), missing=[]),
    "manageLevels": fields.DelimitedList(fields.Str(), missing=[]),
    "limitPerCompany": fields.Int()
})
def num_contact(args):
    """
    return companies with the number of contacts that
    satisfy given criteria
    """
    company_ids = args.get('companyIds')
    # payload is the request params sent to Data API
    payload = defaultdict(dict)
    payload["account"]["esId"] = company_ids
    payload["select"] = ["contact.esId", "account.esId", "account.domain"]
    contact_helper = ContactHelper(args, payload)
    contact_helper.clean_contact_job_titles()
    contacts = contact_helper.get_exact_contacts()
    res = {}
    for c in contacts:
        res.update({c: len(contacts[c])})
    return jsonify(data=res)


@blueprint.route('/contacts/contact_info', methods=['GET', 'POST'])
@use_args({
    "companyIds": fields.DelimitedList(fields.Int(), required=True,
                                       validate=lambda l: len(l) > 0),
    "titles": fields.List(DictField(), missing=[]),
    "manageLevels": fields.DelimitedList(fields.Str(), missing=[]),
    "limitPerCompany": fields.Int(),
    # If True, we only return the ID's of the Contacts we need. This is important
    # for Contacts because we want to know what contacts are available without leaking all the
    # contact information to the client.
    "idsOnly": fields.Boolean(missing=False),
    # calls BriteVerify API to validate email if set to true
    "validateEmails": fields.Boolean(missing=False)
})
def contact_info(args):
    """Return companies with contact information."""
    company_ids = args.get('companyIds')
    # payload is the request params sent to Data API
    payload = defaultdict(dict)
    payload["account"]["esId"] = company_ids
    payload["select"] = [
        "contact.esId",
        "contact.firstName",
        "contact.lastName",
        "contact.email",
        "contact.jobTitle",
        "contact.manageLevel",
        "contact.directPhone",
        "account.esId",
        "account.domain",
        "account.name",
        "account.phone"
    ]

    # check esconfig and args for flag to validate email, call briteverify
    # api only when both are true
    can_validate_emails = current_app.config.get("CONTACT_API_VALIDATE_EMAIL", False)
    should_validate_emails = args.get('validateEmails')
    if can_validate_emails and should_validate_emails:
        # use BriteVerify to validate contact emails
        payload["validateEmails"] = True

    results = ContactHelper(args, payload).get_exact_contacts()

    # Remove phone and email
    if args['idsOnly']:
        for domain, contacts in results.iteritems():
            for contact in contacts:
                contact['hasDirectPhone'] = True if contact['directPhone'] else False
                del contact['directPhone']

                contact['hasEmail'] = True if contact['email'] else False
                del contact['email']

    return jsonify(data=results)


@blueprint.route('/contacts/validate', methods=['POST'])
@use_args({
    "emails": fields.DelimitedList(fields.Str(),
                                   required=True,
                                   validate=lambda l: len(l) > 0)
})
def validate_contact_email(args):
    """Call third party API to validate email.

    Note: This is only turned on in production.
    Args:
        emails (List[String])
    Return:
        Dict[String, Boolean] a boolean for each email indicating
        whether it's valid or not
    """
    emails = args.get('emails')
    # all emails are valid if briteverify is not turned on
    response = {e: True for e in emails}
    if current_app.config.get("CONTACT_API_VALIDATE_EMAIL", False):
        # payload is the request params sent to Data API
        payload = defaultdict(dict)
        payload["select"] = ["contact.email"]
        payload["validateEmails"] = True
        payload["contact"]["email"] = emails
        # reset all emails to be invalid
        response = {e: False for e in emails}
        api_results = DataAPIClient.instance().contact_list(payload)
        for res in api_results:
            if ContactEmail.is_valid(res["contact.emailValid"]):
                response[res["contact.email"]] = True
    return jsonify(data=response)


@blueprint.route('/contacts/has_contact', methods=['GET', 'POST'])
@use_args({
    "companyIds": fields.DelimitedList(fields.Int(), required=True,
                                       validate=lambda l: len(l) > 0),
    "titles": fields.List(DictField(), missing=[]),
    "manageLevels": fields.DelimitedList(fields.Str(), missing=[])
})
def has_contact(args):
    """
    return a list of company ids that have contacts
    satisfying given criteria
    """
    company_ids = args.get('companyIds')
    payload = defaultdict(dict)
    payload["account"]["esId"] = company_ids
    payload["select"] = ["contact.esId", "account.esId", "account.domain"]
    contacts = ContactHelper(args, payload).get_exact_contacts()
    res = map(lambda d: int(contacts[d][0]["companyId"]), contacts)
    return jsonify(data=res)


@blueprint.route('/contacts/title/expand', methods=['GET', 'POST'])
@use_args({
    "titles": fields.List(fields.Str(), required=True,
                          validate=lambda l: len(l) > 0)
})
def title_expand(args):
    titles = args.get('titles')
    expanded_titles = DataAPIClient.instance().contact_title_expand(titles)
    return jsonify(data=expanded_titles)
