# -*- coding: utf-8 -*-

import httplib
from flask import Blueprint, jsonify, current_app
from webargs.flaskparser import use_kwargs, use_args
from webargs import fields
from ns.model.user import User
import logging
from ns.util.string_util import is_not_blank, is_blank

logger = logging.getLogger(__name__)

blueprint = Blueprint('user', __name__)


@blueprint.route('/users/get_by_email', methods=['GET'])
@use_kwargs({
    "email": fields.Str(required=True)
})
def get_user_by_email(email):
    user = User.get_user_by_email(email)
    return jsonify(
        data=user.serialized if user else None
    )


@blueprint.route('/users/get_by_id', methods=['GET'])
@use_kwargs({
    "userId": fields.Int(required=True)
})
def get_user_by_id(userId):
    user = User.get_by_id(userId)
    return jsonify(
        data=user.serialized if user else None
    )

email_password = {
    'email': fields.Str(required=True, validate=is_not_blank),
    'password': fields.Str(required=True, validate=is_not_blank)
}


@blueprint.route('/users/auth_user', methods=['POST'])
@use_args(email_password)
def user_login(args):
    user = User.get_user_by_email(args['email'])
    if not user or not user.check_password(args['password']):
        return jsonify(data=None)
    return jsonify(data=user.serialized)
