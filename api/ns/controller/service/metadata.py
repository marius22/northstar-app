from flask import jsonify, Blueprint, current_app, request, g
from webargs.flaskparser import use_kwargs
from webargs import fields
from ns.model.metadata import Metadata

blueprint = Blueprint('metadata', __name__)


@blueprint.route('/metadata/categories/details', methods=['GET'])
@use_kwargs({
    "keys": fields.DelimitedList(fields.Str(), required=True),
    "substr": fields.Str(required=fields, missing=None)
})
def get_metadata_details(keys, substr):
    result = {}
    for key in keys:
        result[key] = Metadata.get_values_fuzzily(key, substr)

    return jsonify(
        data=result
    )
