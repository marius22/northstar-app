import httplib

from flask import Blueprint, request, jsonify
from ns.model.job import Job
from ns.model.recommendation import Recommendation
from ns.model.segment_metric import SegmentMetric
from sqlalchemy import func
from werkzeug.exceptions import NotFound, BadRequest
from ns.util import param_util

blueprint = Blueprint('Recommendation', __name__)


def check_job_id(job_id):
    job = Job.get_by_id(job_id)
    if not job:
        raise NotFound('Job(Id = %d) is not found' % job_id)
    return job


@blueprint.route('/exported_recommendations', methods=['PUT'])
def set_recommendation_exported():
    recommendations = request.get_json().get("recommendation_ids", [])
    Recommendation.set_recommendations_exported(recommendations)
    return jsonify(status_code=httplib.OK)


@blueprint.route('/jobs/<int:job_id>/recommendations', methods=['POST'])
def import_job_result(job_id):

    job = check_job_id(job_id)

    payload = request.get_json().get('recommendations')
    recommendations = job.create_recommendations(payload)

    all_jobs_for_this_segment = Job.get_all_jobs_by_segment_id(job.segment_id).all()
    for job in all_jobs_for_this_segment:
        if job.id == job_id:
            continue

        job.recommendations.delete()

    return jsonify(total=len(recommendations)), httplib.CREATED


@blueprint.route('/jobs/<int:job_id>/summary', methods=['POST'])
def import_job_result_summary(job_id):
    """
        {
            "totalCount": 1000000,
            "techDistinct": [
                                {"Java": 1000},
                                {"Python": 9000},
                                {"MySql": 8000}
                            ]   // this should be sorted, based on count, desc
            "inBoundaryCount": 100000000
        }
    """
    job = check_job_id(job_id)
    data = request.get_json()
    # total_count = get_value(data, "totalCount", 0)

    segment = job.segment
    # segment.update_recommendation_num(total_count)

    if "inBoundaryCount" in data:
        in_boundary_count = param_util.get_value(data, "inBoundaryCount", 0)
        SegmentMetric.save_in_boundary_count_metric(segment.id, in_boundary_count)
    if "techDistinct" in data:
        tech_distinct = param_util.get_value(data, "techDistinct", [])
        SegmentMetric.save_tech_metric(segment.id, tech_distinct)

    return jsonify(status="OK")


@blueprint.route('/jobs/<int:job_id>/count_sector_companies', methods=['POST'])
def count_sector_companies(job_id):
    job = check_job_id(job_id)

    recommendations = job.recommendations
    res = dict()
    """
    The venn chart from UI is comprised of three dimensions:
        similar companies (C),
        similar technologies (T),
        similar departments (D)

    The chart has 7 sectors:
        1 - Only have similar companies, no similar technologies or departments
        2 - Only have similar technologies, no similar companies or departments
        3 - Only have similar departments, no similar companies or technologies
        4 - Have similar companies and technologies, but no similar departments
        5 - Have similar companies and departments, but no similar technologies
        6 - Have similar technologies and departments, but no similar companies
        7 - Have similar companies, technologies and departments
    """
    total_company_cnt = 0
    for i in [1, 2, 3, 4, 5, 6, 7]:
        sector_filter = Recommendation.build_sector_filter(i)

        company_cnt = recommendations.filter(sector_filter). \
            with_entities(func.count(Recommendation.company_id)).scalar()
        res[i] = company_cnt
        total_company_cnt += company_cnt

    SegmentMetric.save_sector_metric(job.segment_id, res)

    # set total company count
    job.segment.update_recommendation_num(total_company_cnt)

    return jsonify(status="OK")
