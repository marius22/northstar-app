import httplib

from werkzeug import exceptions
from flask import Blueprint, jsonify
from webargs import fields
from webargs.flaskparser import use_args
from sqlalchemy.exc import IntegrityError
from ns.model.user_download import UserDownload

blueprint = Blueprint('user_download', __name__)


# this api is used for tracking audience publish csv file
@blueprint.route('/user_download/csv_track', methods=['POST'])
@use_args({
    'user_id': fields.Int(required=True),
    'audience_id': fields.Int(required=True),
    'company_csv_s3_key': fields.Str(required=True),
    'contact_csv_s3_key': fields.Str(required=True)
})
def track_csv_file(args):
    user_id = args.get('user_id')
    audience_id = args.get('audience_id')
    company_csv_s3_key = args.get('company_csv_s3_key')
    contact_csv_s3_key = args.get('contact_csv_s3_key')
    try:
        UserDownload.create_for_audience(user_id, audience_id, company_csv_s3_key, contact_csv_s3_key)
    except IntegrityError, e:
        return jsonify(status=str(e)), httplib.BAD_REQUEST
    return jsonify(status="OK")
