import logging
from flask import jsonify, g, Blueprint, \
    request, make_response, redirect, current_app
from webargs.flaskparser import use_kwargs
from webargs import fields
from werkzeug import exceptions

from ns.model.mas_token import Token, MASToken
from ns.model.mas_field_mapping import MASFieldMapping, Mapping
from ns.util.string_util import is_not_blank
from ns.util.marketo.api import Marketo
from ns.model.mas_token import PublishConfig
from ns.util.auth_util import get_effective_user
from ns.model.audience import Audience
from ns.util.queue_service_client import QueueServiceClient

blueprint = Blueprint('mas', __name__)

logger = logging.getLogger(__name__)


@blueprint.route('/mas/integration', methods=['GET'])
@use_kwargs({
    "account_id": fields.Int(),
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
def get_mas_token(account_id, mas_type):
    result_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    return jsonify(
        data=result_token.token_info if result_token else {}
    )


@blueprint.route('/mas/fields_mapping', methods=['GET'])
@use_kwargs({
    "account_id": fields.Int(),
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
def get_mas_fields_mapping(account_id, mas_type):
    mas_fields_mapping = MASFieldMapping.get_one_by_account_id_mas_type(account_id, mas_type)
    return jsonify(
        data=mas_fields_mapping.serialized if mas_fields_mapping else {}
    )
