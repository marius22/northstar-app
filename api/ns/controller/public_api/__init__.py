import os

from flask import Flask, g, current_app, request, jsonify, url_for

from ns.util.make_json_app import make_json_app
from ns.model import db
from ns.model.usage_tracking import ESTrackUsage
from ns.model.usage_tracking_storage import UsageTrackingStorage
from ns.util.dynamic_config_client import get_configuration_object
from sqlalchemy.engine import create_engine

public_api_app = Flask(
    "public_api",
    # Set config dynamically
    instance_relative_config=True)

env = os.environ.get('ENV', 'local')
if env in ['dev', 'uat', 'staging', 'demo', 'prod']:
    d = get_configuration_object(env, "northstar_application")
    public_api_app.config.from_object(d)
else:
    public_api_app.config.from_pyfile(env + '.config.py')
public_api_app.config['CURRENT_ENV'] = env

public_api_app = make_json_app(public_api_app)
db.init_app(public_api_app)

mysql_db_engine = create_engine(public_api_app.config.get("SQLALCHEMY_DATABASE_URI"))
# create an object for storage (UsageTrackingStorage inherits default impl of SQLStorage (provided by flask).
usage_tracking_storage = UsageTrackingStorage(mysql_db_engine, None, "rest_api_tracking", None)
# create object for tracking using app and storage above. This object will be imported by endpoint modules.
track = ESTrackUsage(public_api_app, usage_tracking_storage)

from . import company   # noqa: ignore=E402

public_api_app.register_blueprint(company.blueprint, url_prefix='/v1')
