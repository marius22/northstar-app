from flask import jsonify, g, Blueprint, request, current_app
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from werkzeug import exceptions
from sqlalchemy.exc import IntegrityError

from ns.constants.insight_constants import InsightConstants
from ns.controller.public_api import track
from ns.controller.app.token_mgmt import validate_token
from ns.model.account_exported_company import AccountExportedCompany
from ns.model.audience import Audience
from ns.model.model import Model
from ns.util.company_db_client import CompanyDBClient
from ns.util.bucket_util import bucketize
from ns.util import domain_util

import logging
import traceback
import datetime

blueprint = Blueprint('company', __name__)

logger = logging.getLogger(__name__)


@blueprint.route('/status', methods=['GET'])
def status_check():
    """
    Sanity check endpoint for monitoring
    """
    current_app.logger.info('Access status')
    return jsonify(status='OK')

# TODO: Move this to the appropriate place.
COMPANY_BATCH_SIZE = 1000

# Map from response parameter to CompanyInsight.
# The key is the response parameter that users should expect in the response
# and can pass in the "select" request parameter.
# The key is our corresponding company insight constant.
resp_param_insight_map = {
    "revenueRange": [InsightConstants.INSIGHT_NAME_REVENUE],
    "employeesRange": [InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE],
    "facebookUrl": [InsightConstants.INSIGHT_NAME_FACEBOOK_URL],
    "linkedinUrl": [InsightConstants.INSIGHT_NAME_LINKEDIN_URL],
    "twitterUrl": [InsightConstants.INSIGHT_NAME_TWITTER_URL],
    "industry": [InsightConstants.INSIGHT_NAME_INDUSTRY],
    "location": [InsightConstants.INSIGHT_NAME_STREET, InsightConstants.INSIGHT_NAME_CITY,
                 InsightConstants.INSIGHT_NAME_ZIPCODE, InsightConstants.INSIGHT_NAME_STATE,
                 InsightConstants.INSIGHT_NAME_COUNTRY],
    "alexaRank": [InsightConstants.INSIGHT_NAME_ALEXA_RANK],
    "phone": [InsightConstants.INSIGHT_NAME_COMPANY_PHONE],
    "audiences": [InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES],
    "audienceNames": [InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES],
    "models": [InsightConstants.INSIGHT_COMPANY_MODEL_SCORES],
    "keywords": [InsightConstants.INSIGHT_NAME_KEYWORDS],
    "keywordsStr": [InsightConstants.INSIGHT_NAME_KEYWORDS],
    "technologies": [InsightConstants.INSIGHT_NAME_TECHNOLOGY],
    "technologiesStr": [InsightConstants.INSIGHT_NAME_TECHNOLOGY],
    "employeesInMultipleLocations": [InsightConstants.INSIGHT_NAME_EMPLOYEES_IN_MULTIPLE_LOCATIONS],
    "marketingSophistication": [InsightConstants.INSIGHT_NAME_MARKETING_SOPHISTICATION],
    "businessToBusiness": [InsightConstants.INSIGHT_NAME_B2B],
    "businessToConsumer": [InsightConstants.INSIGHT_NAME_B2C],
    "facilitiesInMultipleLocations": [InsightConstants.INSIGHT_NAME_FACILITIES_IN_MULTIPLE_LOCATIONS],
}


def limit_tech_by_length(tech, tech_limit):
    # We limit the amount of tech we publish because there is potentially enough tech that
    # that the API client won't like it. We'll need to solicit preferences for tech that
    # the user is interested in and publish only tech that matches these preferences.
    tech_length_so_far = 0
    for tech_idx in xrange(len(tech)):
        if tech_length_so_far > tech_limit:
            break

        tech_length_so_far += len(tech[tech_idx])

    return tech[:tech_idx]


def transform_advanced_insight_value(value):
    if value == "Yes":
        return True
    elif value == "No":
        return False

    return value


def format_advanced_insights(advanced_insights, insight_map):
    advanced_insights_exact_name_to_name_map = {
        exact_name: name for name, exact_name in
        insight_map.iteritems()}

    formatted = {}

    for insight in advanced_insights:
        exact_name = insight["name"]
        name = advanced_insights_exact_name_to_name_map[exact_name]

        formatted[name] = {
            "name": exact_name,
            "value": transform_advanced_insight_value(insight["value"])
        }

    return formatted


def format_enriched_company(company_dict):
    audience_dicts = map(
        lambda aud: aud.serialized_public,
        Audience.get_by_ids(company_dict.get("audience_ids", []) or []))
    published_tech = []
    if company_dict.get("tech"):
        published_tech = limit_tech_by_length(
            company_dict.get("tech"),
            InsightConstants.INSIGHT_NAME_TECHNOLOGY_MAX_LENGTH)

    advanced_insights = company_dict.get("advanced_insights", [])
    keywords = company_dict.get("keywords", [])
    # All our publish flows should only return top 20 keywords.
    # Keywords are already sorted by relevance.
    top_20_keywords = keywords[:20]

    formatted_output = {
        "domain": company_dict.get(InsightConstants.INSIGHT_NAME_DOMAIN),
        "name": company_dict.get(InsightConstants.INSIGHT_NAME_COMPANY_NAME)
    }
    if InsightConstants.INSIGHT_NAME_REVENUE in company_dict:
        formatted_output["revenueRange"] = bucketize(
            company_dict.get(InsightConstants.INSIGHT_NAME_REVENUE) * 1000,
            current_app.config.get('REVENUE_BUCKETS'))

    if InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE in company_dict:
        formatted_output["employeesRange"] = bucketize(
            company_dict[InsightConstants.INSIGHT_NAME_EMPLOYEE_SIZE],
            current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
        )

    if InsightConstants.INSIGHT_NAME_FACEBOOK_URL in company_dict:
        formatted_output["facebookUrl"] = company_dict[InsightConstants.INSIGHT_NAME_FACEBOOK_URL]

    if InsightConstants.INSIGHT_NAME_TWITTER_URL in company_dict:
        formatted_output["twitterUrl"] = company_dict[InsightConstants.INSIGHT_NAME_TWITTER_URL]

    if InsightConstants.INSIGHT_NAME_LINKEDIN_URL in company_dict:
        formatted_output["linkedinUrl"] = company_dict[InsightConstants.INSIGHT_NAME_LINKEDIN_URL]

    if InsightConstants.INSIGHT_NAME_INDUSTRY in company_dict:
        formatted_output["industry"] = company_dict[InsightConstants.INSIGHT_NAME_INDUSTRY]

    if (company_dict.get(InsightConstants.INSIGHT_NAME_STREET) or
            company_dict.get(InsightConstants.INSIGHT_NAME_CITY) or
            company_dict.get(InsightConstants.INSIGHT_NAME_STATE) or
            company_dict.get(InsightConstants.INSIGHT_NAME_ZIPCODE) or
            company_dict.get(InsightConstants.INSIGHT_NAME_COUNTRY)):
        formatted_output["location"] = {}

    if audience_dicts:
        formatted_output["audiences"] = audience_dicts
        formatted_output["audienceNames"] = InsightConstants.INSIGHT_DELIMITER.join(
            map(lambda aud: aud.get('name'), audience_dicts))

    for location_key in [InsightConstants.INSIGHT_NAME_STREET, InsightConstants.INSIGHT_NAME_CITY,
                         InsightConstants.INSIGHT_NAME_STATE, InsightConstants.INSIGHT_NAME_ZIPCODE,
                         InsightConstants.INSIGHT_NAME_COUNTRY]:
        if location_key in company_dict:
            output_location_key = location_key
            if location_key == InsightConstants.INSIGHT_NAME_ZIPCODE:
                output_location_key = "postalCode"

            formatted_output["location"][output_location_key] = company_dict[location_key]

    if InsightConstants.INSIGHT_NAME_COMPANY_PHONE in company_dict:
        formatted_output["phone"] = company_dict[InsightConstants.INSIGHT_NAME_COMPANY_PHONE]

    if InsightConstants.INSIGHT_NAME_ALEXA_RANK in company_dict:
        formatted_output["alexaRank"] = company_dict[InsightConstants.INSIGHT_NAME_ALEXA_RANK]

    if "scores" in company_dict:
        formatted_output["models"] = company_dict["scores"]

    if top_20_keywords:
        formatted_output["keywords"] = top_20_keywords
        formatted_output["keywordsStr"] = InsightConstants.INSIGHT_DELIMITER.join(top_20_keywords)

    if published_tech:
        formatted_output["technologies"] = published_tech
        formatted_output["technologiesStr"] = InsightConstants.INSIGHT_DELIMITER.join(published_tech)

    advanced_insights = company_dict.get("advanced_insights", [])
    formatted_advanced_insights = format_advanced_insights(
        advanced_insights,
        InsightConstants.ADVANCED_INSIGHT_NAME_EXACT_NAME_MAP)

    formatted_output.update(formatted_advanced_insights)
    return formatted_output


def format_data_for_response(identifying_data_with_meta, company_publish_data, company_ids_exceeding_quota):
    """
    Create our final response format for the enrich API.

    :param identifying_data_with_meta: A list of single_company_enrich_params that was originally input by the API
                                       caller, with some metadata that we attached to help keep track of things.
                                       We iterate over this list to create our final output, which also preserves
                                       original input order in the response.

    :param company_publish_data: A dictionary that maps domain strings (i.e 'everstring.com') to CompanyPublishData
                                 objects.
                                 These objects have a data field that stores a dictionary of enrichment data returned by
                                 our enrichment service.

    :param company_ids_exceeding_quota: A list of company ids for which we will not return any data.

    :return: Returns a list of one or more dicts, formatted for the response based on our API docs.
    """
    formatted_publish_data = []
    for info_dict in identifying_data_with_meta:
        enriched_company_data = None
        matched_domain = info_dict.get("matchedDomain")
        matched_on = info_dict.get("matchedOn")
        del info_dict["matchedOn"]
        del info_dict["matchedDomain"]
        # Re-naming to indicate that, after deleting the keys above (which were added in our domain_util's
        # get_batch_domain_matches logic), info_dict is now back to its original form that the API caller sent.
        original_input = info_dict

        enriched_company_response_dict = {
            "meta": {
                "originalInput": original_input,
                "matchedOn": matched_on
            }
        }

        if matched_domain:
            # publish_data is an instance of CompanyPublishData, defined in publish_field_mapping.py.
            # It will be None if we don't have the matched domain in our redshift data.
            publish_data = company_publish_data.get(matched_domain)
            if publish_data:
                company_id = publish_data.data.get('id')
                if company_id in company_ids_exceeding_quota:
                    response_meta = enriched_company_response_dict["meta"]
                    response_meta["code"] = "quota_exceeded"
                    response_meta["message"] = "No quota remaining."
                else:
                    enriched_company_data = format_enriched_company(publish_data.data)

        enriched_company_response_dict["data"] = enriched_company_data
        formatted_publish_data.append(enriched_company_response_dict)

    return formatted_publish_data


# The format of the input for a single company.
single_company_enrich_params = {
    # Optional ID the user can pass in to help match an input company with an enriched output company.
    "id": fields.Str(),
    "website": fields.Str(),
    "email": fields.Str(),
    "name": fields.Str()
}

batch_companies_enrich_params = {
    "companies": fields.DelimitedList(fields.Nested(single_company_enrich_params))
}

enrich_companies_params = dict(
    single_company_enrich_params.items() +
    batch_companies_enrich_params.items() +
    ({
        'select': fields.List(fields.Str())
    }).items()
)


# ADDED for testing
@track.include
@blueprint.route('/companies/enrich/usage_tracking', methods=['POST', 'GET'])
def test_usage_tracking():
    return jsonify(data="Success")


@track.include
@blueprint.route('/companies/enrich', methods=['GET', 'POST'])
@use_args(enrich_companies_params)
@validate_token
def enrich_companies(args):
    """Company Enrichment API
    Docs: http://bit.ly/2lrD5zD
    """
    # g.account is set by the @validate_token decorator
    begin = datetime.datetime.utcnow()
    account = g.account

    # Enrich single company params
    website = args.get("website")
    email = args.get("email")
    name = args.get("name")
    select = args.get("select", [])
    valid_single_company_request = website or email or name

    # Enrich multiple companies
    companies = args.get("companies", [])
    valid_multiple_companies_request = bool(companies)

    if not (valid_single_company_request or valid_multiple_companies_request):
        raise exceptions.BadRequest("Must pass either `companies` or one of `website`, `email`, or `name`.")

    if len(companies) > COMPANY_BATCH_SIZE:
        raise exceptions.BadRequest("You can batch enrich at most %(COMPANY_BATCH_SIZE)s companies." % {
            "COMPANY_BATCH_SIZE": COMPANY_BATCH_SIZE
        })
    if valid_multiple_companies_request:
        identifying_data = companies
    elif valid_single_company_request:
        identifying_data = [args]

    start = datetime.datetime.utcnow()
    domains = domain_util.get_batch_domain_matches(identifying_data)
    logger.info("Domain Matches Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))
    # Re-naming to indicate we now have metadata on our matches
    identifying_data_with_meta = identifying_data

    model_ids = []

    # Whether the user is asking for models. If there's no select parameter,
    # they want models. If there is a "select" param, then they only
    # want models if "models" is one of the entries
    wants_models = (not select) or (select and "models" in select)
    query_for_model_ids = account.account_quota.scoring_enabled and wants_models
    if query_for_model_ids:
        models = Model.get_by_account_id(
            account.id,
            [Model.FIT_MODEL, Model.ACCOUNT_FIT],
            statuses=[Model.COMPLETED]
        )
        model_ids = [model.serialized["id"] for model in models]

    # exposed_csv_insights are actually the insights that we allow this account to publish.
    start = datetime.datetime.utcnow()
    allowed_insights = account.account_quota.exposed_csv_insights

    if select:
        selected_insights = []
        for resp_key in select:
            if resp_key in resp_param_insight_map:
                selected_insights += resp_param_insight_map[resp_key]

        allowed_insights = list(set(allowed_insights) & set(selected_insights))

    logger.info("Get allowed insights Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))

    audience_ids = []
    if InsightConstants.INSIGHT_NAME_AUDIENCE_NAMES in allowed_insights:
        audience_ids = [audience.serialized["id"] for audience in account.all_owned_audiences()]

    start = datetime.datetime.utcnow()
    company_publish_data = CompanyDBClient.instance().get_publish_data_for_domains(
        domains,
        allowed_insights,
        audience_ids,
        model_ids
    )
    logger.info("Enrich Service Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))

    company_ids = map(lambda d: d.data.get('id'), company_publish_data.values())

    start = datetime.datetime.utcnow()
    new_publish_company_ids, already_published_company_ids = account.get_company_ids_set(account, company_ids)
    logger.info("Quota Check Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))

    remaining_quota = int(account.account_quota.remaining_csv_company_cnt)
    # If remaining_quota is negative, the list slicing logic on new_publish_company_ids will be incorrect.
    # We want to make sure that it's a value >= 0.
    remaining_quota = max(remaining_quota, 0)

    new_publish_company_ids = list(new_publish_company_ids)
    # Only deduct/enforce quota for companies that this account has not yet published.
    # For deduct:
    #     company_ids_within_quota is used solely for creating export history in our
    #     account_exported_company table. We deduct quota only with these ids.
    # For enforce:
    #     company_ids_exceeding_quota are the company ids for which we will not return any data.
    #     If a company id is present in company_ids_exceeding_quota, we don't return any data
    #     for that company, and instead provide meta information indicating that there's no quota remaining.

    company_ids_within_quota = new_publish_company_ids[:remaining_quota]
    company_ids_exceeding_quota = new_publish_company_ids[remaining_quota:]
    start = datetime.datetime.utcnow()
    formatted_publish_data = format_data_for_response(
        identifying_data_with_meta,
        company_publish_data,
        company_ids_exceeding_quota
    )

    try:
        AccountExportedCompany.create_exports(account.id, company_ids_within_quota)
    except IntegrityError:
        logger.error(traceback.format_exc())
    logger.info("Quota Deduct Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))
    # If we're enriching a single company, we publish the company data at the top level.
    if valid_single_company_request and formatted_publish_data:
        logger.info("total Elapsed time: %s" % (str(datetime.datetime.utcnow() - begin)))
        return jsonify(formatted_publish_data[0])

    # Otherwise, we publish enriched data under the `companies` key.
    logger.info("total Elapsed time: %s" % (str(datetime.datetime.utcnow() - begin)))
    return jsonify({"companies": formatted_publish_data})
