import httplib
import logging
import json
import calendar

import ns.model.salesforce as SALESFORCE
from flask import jsonify, g, Blueprint, request, current_app
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from werkzeug import exceptions

from ns.controller.app.param_validator import check_and_get_audience
from ns.controller.app.permission import has_audience_access_permission, has_audience_edit_permission, \
    has_expansion_permission
from ns.model.account import Account
from ns.model.audience import Audience
from ns.model.audience_company import AudienceCompany
from ns.model.audience_exclusion_list import AudienceExclusionList
from ns.model.audience_criteria import AudienceCriteria
from ns.model.audience_keywords import AudienceKeywords
from ns.model.crm_token import CRMToken
from ns.model.metadata import Metadata
from ns.model.model import Model
from ns.model.seed_csv_reader import SeedCSVReader
from ns.model.company_insight import CompanyInsight
from ns.model.publish_field_mapping import PublishFieldMapping
from ns.constants.default_field_mapping_constants import TARGET_OBJECT_LEAD

from ns.util.auth_util import get_effective_user
from ns.util.bucket_util import bucketize
from ns.util.company_db_client import CompanyDBClient
from ns.util.data_api_client import DataAPIClient
from ns.util.ns_data_service import NSDataServiceClient
from ns.util.queue_service_client import QueueServiceClient
from ns.util.webargs_parser import DictField

blueprint = Blueprint('audience', __name__)
logger = logging.getLogger(__name__)

audience_params = {
    'type': fields.Str(required=True, validate=lambda x: x in ['found', 'enriched']),
    'name': fields.Str(required=True),
    'description': fields.Str(),
    'criteria': DictField(),
    'modelId': fields.Int(),
    'metaType': fields.Str(),
    'exclusionListIds': fields.DelimitedList(fields.Int(), missing=[])
}


PREVIEW_NUM = 100


# Can't find a good location for this endpoint, so putting it here since it's used by publish audience.
@blueprint.route('/seniority', methods=['GET'])
def get_seniorities():
    return jsonify(data=current_app.config.get('SENIORITY', []))


@blueprint.route('/audiences', methods=['POST'])
@use_args(audience_params)
def create_audience(args):
    user = get_effective_user()
    user_id = user.id
    source_type = args.get('type')
    name = args.get('name')
    meta_type = args.get('metaType', Audience.META_TYPE_AUDIENCE)
    meta_type_int = Audience.META_TYPE_MAPPING.get(meta_type)

    if meta_type == Audience.META_TYPE_AUDIENCE and not user.account.audience_cnt_quota_not_reached:
        raise exceptions.Forbidden("Insufficient audience quota!")

    # give an empty str when no description passed in
    description = args.get('description', "")
    model_id = args.get('modelId')
    audience = Audience.create_audience(name, description, user_id,
                                        source_type=source_type, meta_type=meta_type_int)
    # front-end passes modelId = -1 if there's no model selected during audience creation
    if model_id > 0:
        audience.model_id = model_id

    criteria = {}
    if source_type == "found":
        criteria = args.get('criteria')

    if source_type == "enriched":
        csv_file = request.files['file']
        domains = SeedCSVReader.read_domains_from_file(csv_file)
        criteria = {'domain': domains}
        AudienceCriteria.save_criteria(audience.id, 'domains', domains)

    exclusion_list_ids = args.get("exclusionListIds")
    if meta_type == Audience.META_TYPE_AUDIENCE:
        AudienceExclusionList.update_exclusion_lists(audience.id, exclusion_list_ids)

    db_client = CompanyDBClient.instance()
    creation_response = db_client.create_audience(criteria, audience.id, meta_type=meta_type_int,
                                                  exclusion_list_ids=exclusion_list_ids)

    # write the total num
    total_company_num = creation_response.get('created')
    audience.update_company_number(total_company_num)

    return jsonify(data=audience.serialized)


@blueprint.route('/audiences/<int:audience_id>', methods=['DELETE'])
@has_audience_access_permission
def delete_audience(audience_id):
    audience = Audience.get_by_id(audience_id)
    Audience.delete_by_id(audience_id)
    if audience.meta_type == Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION):
        update_all_related_audiences_company_number(audience_id)

    return jsonify(status="OK")


@blueprint.route('/audiences/preview', methods=['POST'])
@use_args({
    'modelId': fields.Int(),
    'justDomains': fields.Boolean()
})
def preview_audience(args):
    """
      We're having this endpoint serve two possible purposes now:
      1. Enrich My List flow: Take csv and completely enrich.
         In this case, justDomains = False.
      2. Find Similar Companies from CSV: Take CSV and return just
         domains, because maybe the CSV only has company names so
         we can't just read from it on the client side.
         In this case, justDomains = True and modelId doesn't matter.
    """
    csv_file = request.files['file']
    model_id = args.get('modelId')
    just_domains = args.get('justDomains')
    domains = SeedCSVReader.read_domains_from_file(csv_file)

    if just_domains:
        return jsonify(data=domains)

    db_client = CompanyDBClient.instance()
    criteria_dict = {'domain': domains}
    profiles = db_client.get_preview(criteria_dict, PREVIEW_NUM, model_id)
    return jsonify(data=profiles)


@blueprint.route('/audiences', methods=['GET'])
@use_args({
    'metaType': fields.Str(missing=Audience.META_TYPE_AUDIENCE)
})
def get_audiences(args):
    meta_type = args.get('metaType', Audience.META_TYPE_AUDIENCE)
    meta_type = Audience.META_TYPE_MAPPING.get(meta_type)

    user = get_effective_user()
    if user.is_es_admin() or user.is_account_admin():
        audiences = user.account.all_owned_audiences(meta_type=meta_type)
    else:
        audiences = user.owned_or_shared_audiences(meta_type)

    return jsonify(
        data=[audience.serialized for audience in audiences])


@blueprint.route('/audiences/<int:audience_id>', methods=['GET'])
@has_audience_access_permission
def get_audience(audience_id):
    audience = Audience.get_by_id(audience_id)
    return jsonify(data=audience.serialized)


@blueprint.route('/audiences/<int:audience_id>/criteria', methods=['POST'])
@use_kwargs({
    "criteria": DictField(required=True, missing={})
})
@has_audience_access_permission
def save_criteria(audience_id, criteria):
    result = []
    for key, value in criteria.items():
        result.append(
            AudienceCriteria.save_criteria(audience_id, key, value)
        )
    return jsonify(
        data={item.criteria_type: item.criteria_val_obj for item in result}
    )


@blueprint.route('/audiences/<int:audience_id>/criteria', methods=['GET'])
@use_kwargs({
    "types": fields.DelimitedList(fields.Str(), missing=None, required=False)
})
@has_audience_access_permission
def get_criteria(audience_id, types):

    result = AudienceCriteria.get_by_audience_id_and_type(audience_id, types)

    return jsonify(
        data={item.criteria_type: item.criteria_val_obj for item in result}
    )


@blueprint.route('/audiences/<int:audience_id>/keywords', methods=['POST'])
@use_kwargs({
    "keywords": fields.DelimitedList(fields.Str())
})
@has_audience_access_permission
def save_keywords(audience_id, keywords):

    result = AudienceKeywords.save_keywords(audience_id, keywords)

    return jsonify(
        data=result.serialized
    )


@blueprint.route('/audiences/<int:audience_id>/keywords', methods=['GET'])
@has_audience_access_permission
def get_keywords(audience_id):
    result = AudienceKeywords.get_by_audience_id(audience_id)

    return jsonify(
        data=result.keywords_obj if result else []
    )


@blueprint.route('/audiences/<int:audience_id>/exclusion_lists', methods=['POST'])
@use_kwargs({
    "exclusionListId": fields.Int()
})
@has_audience_access_permission
def save_audience_exclusion_list(audience_id, exclusionListId):
    result = AudienceExclusionList.create_exclusion_list(audience_id, exclusionListId)
    return jsonify(
        data=result.serialized
    )


@blueprint.route('/audiences/<int:audience_id>/switch_fit_model', methods=['POST'])
@use_kwargs({
    "fit_model_id": fields.Int(required=True)
})
@has_audience_access_permission
def switch_fit_model(audience_id, fit_model_id):
    audience = check_and_get_audience(audience_id)
    audience.update_fit_model(fit_model_id)
    return jsonify(
        status="OK"
    )


def _get_latest_completed_crm_fit_model_id(account_id):
    account = Account.get_by_id(account_id)
    crm_fit_model = account.latest_completed_crm_fit_model
    if crm_fit_model:
        return crm_fit_model.id
    raise exceptions.BadRequest("Can not find any fit model, please try later")


@blueprint.route('/audiences/<int:audience_id>/companies', methods=['GET', 'POST'])
@has_audience_access_permission
def get_companies(audience_id):

    args = request.args.copy()

    audience = check_and_get_audience(audience_id)
    exclusion_list = AudienceExclusionList.get_by_audience_id(audience_id)
    exclusion_ids = [one.exclusion_list_id for one in exclusion_list]
    args["exclusionListIds"] = exclusion_ids
    model_id = audience.model_id
    account_id = audience.user.account_id

    current_model = model_id and Model.get_by_id(model_id)
    valid_current_model = current_model and current_model.status == Model.COMPLETED

    if not valid_current_model:
        model_id = _get_latest_completed_crm_fit_model_id(account_id)

    result = CompanyDBClient.instance().get_audience_companies(audience_id, model_id, args)

    employee_size_bucket = current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
    revenue_bucket = current_app.config.get('REVENUE_BUCKETS')
    for item in result["data"]:
        fit_score = item["fitScore"]
        item["fitScore"] = fit_score * 100 if fit_score else 0
        item["employeeSize"] = bucketize(item["employeeSize"], employee_size_bucket)
        revenue = item["revenueRange"]
        item["revenueRange"] = bucketize((revenue if revenue else 0) * 1000, revenue_bucket)

    return jsonify(
        result
    )


@blueprint.route('/audiences/<int:audience_id>/companies/count', methods=['GET', 'POST'])
@has_audience_access_permission
def get_companies_count(audience_id):
    args = request.args.copy()

    audience = check_and_get_audience(audience_id)
    exclusion_list = AudienceExclusionList.get_by_audience_id(audience_id)
    exclusion_ids = [one.exclusion_list_id for one in exclusion_list]
    model_id = audience.model_id
    account_id = audience.user.account_id
    criteria = args.get('criteria')

    if not (model_id and Model.get_by_id(model_id).status == Model.COMPLETED):
        model_id = _get_latest_completed_crm_fit_model_id(account_id)

    result = CompanyDBClient.instance().get_audience_companies_count(audience_id,
                                                                     model_id,
                                                                     criteria=criteria,
                                                                     exclusion_list_ids=exclusion_ids)
    return jsonify(total=result)


@blueprint.route('/audiences/filters', methods=['GET'])
def get_audience_filters():

    result = Metadata.get_all_categories()
    result = filter(lambda x: x != Metadata.TERRITORY, result)

    return jsonify(
        data=result
    )


publish_summary_params = {}
basic_params = {
    "audienceId": fields.Int(required=True),
    "modelId": fields.Int(required=True),
    "criteria": DictField(missing={}),
    "orderBy": fields.Str(),
    "titles": fields.DelimitedList(DictField(), missing=[]),
    "manageLevels": fields.DelimitedList(fields.Str(), missing=[]),
    'masType': fields.Str(),
    'contactLimitPerCompany': fields.Int(missing=0),
    'totalPublishAccountNum': fields.Int(required=True)
}

dedupe_params = {
    'contactLimitPerCompany': fields.Int(missing=0),
    # If true, we will de-dupe against accounts.
    'compareAgainstAccounts': fields.Boolean(),
    # If true, we will de-dupe against leads.
    'compareAgainstLeads': fields.Boolean(),
    'publishType': fields.Str(validate=lambda publishType: publishType in [
        'CSV',
        'CSV0',
        'SFDC',
        'MAS'
    ]),
    'publishNew': fields.Boolean(missing=False),
    'publishExisting': fields.Boolean(missing=False),
    'dedupeAgainstPreviousPublish': fields.Boolean(missing=False),
    'new': fields.Nested({
        # If true, we only publish an account if we can find contact information
        # for it
        'publishOnlyIfContactFound': fields.Boolean(),
        'publishCompaniesTo': fields.Str(validate=lambda dest: dest in [
            SALESFORCE.ACCOUNT,
            SALESFORCE.LEAD,
        ]),
        'publishLeadsTo': fields.Str(validate=lambda dest: dest in [
            SALESFORCE.LEAD,
            SALESFORCE.CONTACT,
        ]),
    }),
    'existing': fields.Nested({
        'publishOnlyIfContactFound': fields.Boolean(),
        'foundInAccount': fields.List(fields.Str(validate=lambda sf_obj: sf_obj in [
            SALESFORCE.LEAD,
            SALESFORCE.CONTACT
        ])),
        'foundInAccountsAndLeads': fields.List(fields.Str(
            validate=lambda sf_obj: sf_obj in [
                SALESFORCE.LEAD,
                SALESFORCE.CONTACT
            ])
        )
    })
}


publish_summary_params.update(basic_params)
publish_summary_params.update(dedupe_params)


@blueprint.route('/audiences/publish_summary', methods=['POST'])
@use_args(publish_summary_params)
def get_publish_summary(args):
    if len(args["titles"]) == 1 and args["titles"][0].get('jobTitle').strip() == '':
        raise exceptions.BadRequest("Contacts field must have some value.")
    audience_id = args.get("audienceId")
    model_id = args.get("modelId")
    publish_type = args.get("publishType")
    criteria = args.get("criteria")
    dedupe = args.get("dedupeAgainstPreviousPublish")
    total_publish_account_num = args.get('totalPublishAccountNum')
    exclusion_lists = AudienceExclusionList.get_by_audience_id(audience_id)
    exclusion_ids = [one.exclusion_list_id for one in exclusion_lists]
    dedupe_preference = parse_dedupe_args(args)
    if publish_type in ["CSV", "SFDC"]:
        crm_token = CRMToken.get_sfdc_token_by_user_id(get_effective_user().id)
        org_id = crm_token.organization_id

    audience_num = CompanyDBClient.instance().get_audience_companies_count(audience_id,
                                                                           model_id=None,
                                                                           criteria={},
                                                                           dedupe=False,
                                                                           exclusion_list_ids=exclusion_ids)
    if criteria or dedupe:
        # get total company num after filtering
        filter_num = CompanyDBClient.instance().get_audience_companies_count(
            audience_id, model_id, criteria, dedupe, exclusion_list_ids=exclusion_ids)
    else:
        filter_num = audience_num

    companies_num = 0
    contacts_num = 0
    company_with_contact_num = 0
    # generate company and contact params
    company_params = gen_company_params(args)
    contact_params = gen_contact_params(args)
    company_params["exclusionListIds"] = exclusion_ids

    if publish_type in ["CSV0", "MAS"]:
        # In csv0 publish, we don't have dedupe logic, the final companies number
        # which user can publish is the minimum one between filter_num and
        # total_publish_account_num
        companies_num = min(filter_num, total_publish_account_num)

        # Check if user want to publich contact through contactLimitPerCompany.
        # If contactLimitPerCompany > 0, we need to call get_company_contact_num API.
        # This API returns a tuple, including company number which has contact and contact number
        if args.get('contactLimitPerCompany') > 0:
            company_with_contact_num, found_contacts_num = get_company_contact_num(
                audience_id, model_id, companies_num, company_params, contact_params, publish_type)
            contacts_num = found_contacts_num

    if publish_type in ["CSV", "SFDC"]:
        # Add dedupe params in company_params
        company_params['orgId'] = org_id
        company_params['dedupePreference'] = dedupe_preference

        # We need to figure out company number which has contact, because user might want to
        # publish company only if contact found
        # Here we have 16 different cases, in terms of four paramters:
        #   1. publish_new, which means ublish company not in crm
        #   2. publish_existing, which means publish company in crm
        #   3. net_new_company_has_contact, which means publish company not in crm only if contact found
        #   4. existing_company_has_contact, which means publish company in crm only if contact found
        publish_new = args.get('publishNew')
        publish_existing = args.get('publishExisting')
        net_new_company_has_contact = args['new']['publishOnlyIfContactFound']
        existing_company_has_contact = args['existing']['publishOnlyIfContactFound']

        if publish_new and publish_existing:
            if (net_new_company_has_contact and existing_company_has_contact) or \
               (not net_new_company_has_contact and not existing_company_has_contact):
                # In these two case, we only need to call get_company_contact_num one time
                # Final company number will be company number only if contact found if both
                # net_new_company_has_contact and existing_company_has_contact are true
                companies_num = CompanyDBClient.instance().get_audience_companies_count_with_dedupe(
                    audience_id, model_id, company_params)
                companies_num = min(companies_num, total_publish_account_num)

                company_with_contact_num, contacts_num = get_company_contact_num(
                    audience_id, model_id, companies_num, company_params, contact_params, publish_type)
                if net_new_company_has_contact and existing_company_has_contact:
                    companies_num = company_with_contact_num
            else:
                # we call get_company_contact_num API twice, and figure out company number only contact
                # found in crm and not in crm separately.
                dedupe_preference['net_new']['enable'] = True
                dedupe_preference['existing']['enable'] = False
                company_params['dedupePreference'] = dedupe_preference
                net_new_companies_num = CompanyDBClient.instance().get_audience_companies_count_with_dedupe(
                    audience_id, model_id, company_params)
                net_new_company_with_contact_num, net_new_contacts_num = get_company_contact_num(
                    audience_id, model_id, min(net_new_companies_num, total_publish_account_num),
                    company_params, contact_params, publish_type)

                dedupe_preference['net_new']['enable'] = False
                dedupe_preference['existing']['enable'] = True
                company_params['dedupePreference'] = dedupe_preference
                existing_companies_num = CompanyDBClient.instance().get_audience_companies_count_with_dedupe(
                    audience_id, model_id, company_params)
                existing_company_with_contact_num, existing_contacts_num = get_company_contact_num(
                    audience_id, model_id, min(existing_companies_num, total_publish_account_num),
                    company_params, contact_params, publish_type)
                if net_new_company_has_contact:
                    companies_num = min(net_new_company_with_contact_num + existing_companies_num,
                                        total_publish_account_num)
                else:
                    companies_num = min(existing_company_with_contact_num + net_new_companies_num,
                                        total_publish_account_num)
                contacts_num = net_new_contacts_num + existing_contacts_num

        else:
            companies_num = CompanyDBClient.instance().get_audience_companies_count_with_dedupe(
                audience_id, model_id, company_params)
            companies_num = min(companies_num, total_publish_account_num)
            company_with_contact_num, contacts_num = get_company_contact_num(
                audience_id, model_id, companies_num, company_params, contact_params, publish_type)
            if net_new_company_has_contact or existing_company_has_contact:
                companies_num = company_with_contact_num

    return jsonify(data={
        'companiesInAudience': audience_num,
        'totalPublishCompanies': total_publish_account_num,
        'companiesAfterFiltering': filter_num,
        'companiesToPublish': companies_num,
        'contactsToPublish': contacts_num,
    })


def get_company_contact_num(audience_id, model_id, companies_num, company_params, contact_params, publish_type):
    batch_size = current_app.config.get('AUDIENCE_COMPANY_BATCH_SIZE', 5000)
    limit = batch_size
    offset = 0
    current_cnt = 0
    contacts_num = 0
    company_with_contact_num = 0
    found_contacts_num = 0
    while True:
        company_ids = list()
        if current_cnt >= companies_num:
            logging.info("Found current count: {}".format(current_cnt))
            break

        limit = min(batch_size, companies_num - current_cnt)
        company_params['limit'] = limit
        company_params['offset'] = offset

        # get company ids, resp is a list of dictionary.
        if publish_type in ["CSV0", "MAS"]:
            resp = CompanyDBClient.instance().get_audience_companies(audience_id, model_id, company_params).get('data')
        if publish_type in ["CSV", "SFDC"]:
            resp = CompanyDBClient.instance().get_audience_companies_ids_with_dedupe(audience_id,
                                                                                     model_id,
                                                                                     company_params)
        company_ids = map(lambda c: c.get('companyId'), resp)
        if not company_ids:
            logging.error("Get empty company ids")
            break
        found_company_with_contact_num, found_contacts_num = NSDataServiceClient.instance().get_company_contacts_num(
            company_ids, contact_params)

        contacts_num += found_contacts_num
        company_with_contact_num += found_company_with_contact_num
        current_cnt += len(company_ids)
        offset += limit
    return company_with_contact_num, contacts_num


@blueprint.route('/audiences/<int:audience_id>', methods=['POST'])
@use_args({
    'audience': fields.Nested({
        'name': fields.Str(),
        'description': fields.Str(),
        'showInSfdc': fields.Boolean(),
        'isPrivate': fields.Boolean(),
        'modelId': fields.Int(missing=-1),
        "realTimeScoringEnabled": fields.Boolean(),
        "userId": fields.Int(),
        "isPublishedSfdc": fields.Boolean(),
        "exclusionListIds": fields.DelimitedList(fields.Int(), missing=None)
    }, missing={'modelId': -1})
})
@has_audience_edit_permission
def update_audience(args, audience_id):
    #  if no  model selected ,pass modelId = -1
    if args['audience'].get('modelId') < 0:
        args['audience'].pop('modelId')
    g.audience.update(get_effective_user(), args["audience"])

    audience = Audience.get_by_id(audience_id)

    exclusion_list_ids = args["audience"].get("exclusionListIds")
    if audience.meta_type == Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE):
        if exclusion_list_ids is not None:
            AudienceExclusionList.update_exclusion_lists(audience.id, exclusion_list_ids)
            result_total = CompanyDBClient.instance().get_audience_companies_count(audience_id, None, {},
                                                                                   False, audience.meta_type,
                                                                                   exclusion_list_ids)
            audience.update_company_number(result_total)

    if audience.meta_type == Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_EXCLUSION) and request.files.get('file'):
        csv_file = request.files['file']
        domains = SeedCSVReader.read_domains_from_file(csv_file)
        criteria = {'domain': domains}
        AudienceCriteria.save_criteria(audience_id, 'domains', domains)
        db_client = CompanyDBClient.instance()
        creation_response = db_client.create_audience(criteria, audience_id, meta_type=audience.meta_type)
        # write the total num
        total_company_num = creation_response.get('created')
        audience.update_company_number(total_company_num)
        update_all_related_audiences_company_number(audience_id)

    return jsonify(
        data=g.audience.serialized
    )


def update_all_related_audiences_company_number(audience_id):
    db_client = CompanyDBClient.instance()
    audiences_exclusion_lists = AudienceExclusionList.get_by_exclusion_list_id(audience_id)
    audience_ids = [one.audience_id for one in audiences_exclusion_lists]
    for one_audience_id in audience_ids:
        one_audience = Audience.get_by_id(one_audience_id)
        if not one_audience.is_deleted and one_audience.meta_type == \
                Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE):
            exclusion_list_ids = [one["id"] for one in one_audience.exclusion_lists]
            count = db_client.get_audience_companies_count(one_audience_id, exclusion_list_ids=exclusion_list_ids)
            one_audience.update_company_number(count)


publish_params = {
    'publishFields': fields.DelimitedList(fields.Str()),
    'masLists': fields.DelimitedList(DictField(), missing=[]),
    'masListsForDedupe': fields.DelimitedList(DictField(), missing=[]),
    'companiesNumInAudience': fields.Int(required=True),
    'companiesNumInAudienceAfterFilter': fields.Int(required=True),
    'contactsNumBeforeDedupe': fields.Int(required=True)
}

publish_params.update(basic_params)
publish_params.update(dedupe_params)


@blueprint.route('/audiences/<int:audience_id>/publish', methods=['POST'])
@use_args(publish_params)
@has_expansion_permission
@has_audience_access_permission
def publish(args, audience_id):
    audience = Audience.get_by_id(audience_id)
    exclusion_list = audience.exclusion_lists
    exclusion_ids = [one["id"] for one in exclusion_list]

    # Company ID is a mandatory field for publishing
    publish_fields = args.get('publishFields')
    publish_fields.append('companyId')

    models = []
    audience_request_json = {}
    criteria = args.get('criteria')
    if criteria:
        audience_request_json['criteria'] = criteria
    order_by = args.get('orderBy')
    if order_by:
        audience_request_json['orderBy'] = order_by
    audience_request_json['dedupe'] = args.get('dedupeAgainstPreviousPublish')
    if audience.model_id:
        model = Model.get_by_id(audience.model_id)
        models = [{'model_id': model.id, 'model_name': model.name}]
        audience_request_json['modelId'] = model.id
    audience_request_json["exclusionListIds"] = exclusion_ids if exclusion_ids else []
    message = {
        'publish_type': args.get('publishType'),
        'audience_request_endpoint': '/audiences/%d/companies' % audience.id,
        'audience_request_json': audience_request_json,
        'total_publish_account_num': args.get('totalPublishAccountNum'),
        'contact_limit_per_account': args.get('contactLimitPerCompany'),
        'titles': args.get('titles'),
        'manage_level': args.get('manageLevels'),
        'user_id': g.user.id,
        'user_email': g.user.email,
        'account_id': g.user.account.id,
        'audience_id': audience.id,
        'audience_name': audience.name,
        'models': models,
        'companies_num_in_audience': args.get('companiesNumInAudience'),
        'companies_num_in_audience_after_filter': args.get('companiesNumInAudienceAfterFilter'),
        # Contacts after considering titles/seniority, but before deduping.
        'contacts_num_before_dedupe': args.get('contactsNumBeforeDedupe'),
    }

    if args.get('publishType') in ['CSV', 'CSV0', 'SFDC']:
        insight_ids = CompanyInsight.get_ids_by_display_names(publish_fields)
        special_column_names = CompanyInsight.get_company_column_name_by_ids(insight_ids)
        message.update({
            'publish_fields': special_column_names,
        })

    # Add org_id if publishing to Salesforce or de-duping against Salesforce
    if args.get('publishType') in ['CSV', 'SFDC']:
        crm_token = CRMToken.get_sfdc_token_by_user_id(g.user.id)
        org_id = crm_token.organization_id

        sfdc_params = {
            'org_id': org_id,
        }
        message.update(sfdc_params)

        dedupe_message = parse_dedupe_args(args)
        message.update(dedupe_message)
    elif args.get('publishType') == 'MAS':
        mas_publish_check(args, g.user.account.id)
        message.update({
            'mas_lists': args.get('masLists'),
            'mas_type': args.get('masType'),
            'dedupe_mas_lists': args.get('masListsForDedupe'),
            'net_new': args.get('publishNew'),
            'existing': args.get('publishExisting')
        })

    import pprint
    print '********'
    print 'message: '
    pprint.pprint(message, indent=2, width=120)
    print '******'

    status_code = QueueServiceClient.instance().send_publish_audience_message(message)

    if status_code == httplib.OK:
        return jsonify(status='OK'), httplib.CREATED
    else:
        raise exceptions.InternalServerError("Publish Audience failed")


def mas_publish_check(args, account_id):
    mas_lists = args.get('masLists')
    # mas_type = args.get('masType')
    net_new = args.get('publishNew')
    existing = args.get('publishExisting')
    if net_new == existing:
        raise exceptions.ExpectationFailed("Please select only net new or enrich")

    if existing:
        if not mas_lists:
            raise exceptions.ExpectationFailed("You must select a marketo list you want to enrich")
    # we always have default mapping for contact
    # target_org_id = PublishFieldMapping.get_target_org_id(account_id, mas_type)
    # mas_field_mappings = PublishFieldMapping.get_publish_field_mapping(account_id, mas_type,
    #                                                                    target_org_id, TARGET_OBJECT_LEAD)
    # if not mas_field_mappings:
    #     raise exceptions.ExpectationFailed("You don't have fields mapping")


def parse_dedupe_args(args):

    against = []
    if args.get('compareAgainstAccounts'):
        against.append(SALESFORCE.ACCOUNT_CONTACT)

    if args.get('compareAgainstLeads'):
        against.append(SALESFORCE.LEAD)

    publish_contacts = args.get('contactLimitPerCompany') > 0

    if args.get('publishNew'):
        publish_account_to = [args['new'].get('publishCompaniesTo')]
        net_new_info_dict = dict(
            enable=True,
            only_publish_account_has_contact=args['new']['publishOnlyIfContactFound'],
            against=against,
            publish_account_to=publish_account_to,
        )
        if publish_contacts:
            publish_leads_to = []
            if 'publishLeadsTo' in args['new']:
                publish_leads_to = [args['new']['publishLeadsTo']]
            net_new_info_dict.update(dict(
                publish_contact_to=publish_leads_to
            ))
        else:
            net_new_info_dict.update(dict(
                publish_contact_to={}
            ))

    else:
        net_new_info_dict = dict(
            enable=False
        )

    if args.get('publishExisting'):
        # Comparing against the account/contact combo means we want to publish
        # to account if something is found
        publish_account_to = []
        for obj in against:
            if obj == SALESFORCE.ACCOUNT_CONTACT:
                publish_account_to.append(SALESFORCE.ACCOUNT)
            else:
                publish_account_to.append(obj)

        existing_info_dict = dict(
            enable=True,
            only_publish_account_has_contact=args['existing']['publishOnlyIfContactFound'],
            against=against,
            # Whatever we compare against is what we publish to.
            publish_account_to=publish_account_to,
        )

        if publish_contacts:
            if args.get('compareAgainstLeads'):
                found_in_lead = [SALESFORCE.LEAD]
            else:
                found_in_lead = []
            existing_info_dict.update(dict(
                publish_contact_to=dict(
                    found_in_account_lead=args.get('existing', {}).get('foundInAccountsAndLeads', []),
                    found_in_account=args.get('existing', {}).get('foundInAccount', []),
                    found_in_lead=found_in_lead
                )
            ))
        else:
            existing_info_dict.update(dict(
                publish_contact_to={}
            ))
    else:
        existing_info_dict = dict(
            enable=False
        )

    return dict(net_new=net_new_info_dict,
                existing=existing_info_dict)


def gen_company_params(args):
    company_params = {}
    company_params['criteria'] = args.get('criteria', {})
    company_params['dedupe'] = args.get('dedupeAgainstPreviousPublish', False)
    company_params['select'] = ["companyId"]
    return company_params


def gen_contact_params(args):
    contact_params = {}
    if args.get('titles'):
        contact_params['titles'] = args.get('titles')
    if args.get('manageLevels'):
        contact_params['manageLevels'] = args.get('manageLevels')
    contact_params['limitPerCompany'] = args['contactLimitPerCompany']
    return contact_params


@blueprint.route('/audiences/publish_fields_supported', methods=['GET'])
def get_publish_fields_supported():
    supported_fields = CompanyInsight.get_supported_publish_field_for_display()
    return jsonify(data=supported_fields)


def _get_model_id_for_audience(audience):
    fit_model_id = audience.model_id
    if fit_model_id:
        return fit_model_id
    crm_fit = audience.user.account.latest_completed_crm_fit_model
    return crm_fit.id if crm_fit else None


@blueprint.route('/audiences/<int:audience_id>/companies/distinct_counts', methods=['GET'])
@use_kwargs({
    'limit': fields.Int(missing=10),
    'keys': fields.DelimitedList(fields.Str(), required=True),
    'order': fields.Str(missing='-count'),
    'withOthers': fields.Boolean(missing=False)
})
@has_audience_access_permission
def get_audience_companies_distinct_count(audience_id, limit, keys, order, withOthers):
    exclusion_list = AudienceExclusionList.get_by_audience_id(audience_id)
    exclusion_ids = [one.exclusion_list_id for one in exclusion_list]

    fit_model_id = None
    if 'fitScore' in keys:
        fit_model_id = _get_model_id_for_audience(g.audience)
        if not fit_model_id:
            raise exceptions.BadRequest("Can not find any fit model")

    data = CompanyDBClient.instance().get_audience_companies_distinct_count(
        audience_id, fit_model_id, keys, limit, order, withOthers, exclusion_list_ids=exclusion_ids)

    return jsonify(
        data=data
    )


@blueprint.route('/audiences/<int:audience_id>/refresh_count', methods=['POST'])
@has_audience_access_permission
def refresh_count(audience_id):
    audience = Audience.get_by_id(audience_id)
    if audience.meta_type == Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE):
        exclusion_list_ids = [one["id"] for one in audience.exclusion_lists]
        result_total = CompanyDBClient.instance().get_audience_companies_count(audience_id,
                                                                               None,
                                                                               {}, False,
                                                                               exclusion_list_ids=exclusion_list_ids)
    else:
        result_total = CompanyDBClient.instance().get_audience_companies_count(audience_id, None, {},
                                                                               False, meta_type=200)
    result = dict()
    result["recommendationNum"] = result_total
    audience.update_company_number(result_total)
    result["companyNumberRefreshTs"] = calendar.timegm(audience.company_number_refresh_ts.utctimetuple())
    return jsonify(data=result)


@blueprint.route('/contacts/title/expand', methods=['POST'])
@use_args({
    'titles': fields.List(fields.Str())
})
def contact_title_expand_endpoint(args):
    titles = args.get('titles')

    expanded_titles = DataAPIClient.instance().contact_title_expand(titles)

    return jsonify(data=expanded_titles)
