import os
from webargs.flaskparser import use_kwargs
from webargs import fields

from flask import jsonify, Blueprint, current_app
from ns.model.metadata import Metadata

blueprint = Blueprint('metadata', __name__)


@blueprint.route('/metadata/categories', methods=['GET'])
def get_metadata_categories():

    return jsonify(
        data=Metadata.get_all_categories()
    )


@blueprint.route('/metadata/categories/details', methods=['GET'])
@use_kwargs({
    "keys": fields.DelimitedList(fields.Str(), required=True),
    "substr": fields.Str(required=fields, missing=None)
})
def get_metadata_details(keys, substr):

    result = {}
    for key in keys:
        result[key] = Metadata.get_values_fuzzily(key, substr)

    return jsonify(
        data=result
    )


@blueprint.route('/metadata/categories/subcategories', methods=['GET'])
@use_kwargs({
    "keys": fields.DelimitedList(fields.Str(), required=True),
    "substr": fields.Str(required=fields, missing=None)
})
def get_metadata_subcats(keys, substr):
    result = {}
    for key in keys:
        result[key] = Metadata.get_sub_categories(key, substr)

    return jsonify(
        data=result
    )
