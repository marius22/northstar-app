"""
    Northstar app api - controllers
"""
import concurrent.futures
import os

from flask_session import Session
from flask import Flask, g, current_app, request
from flask_cors import CORS
import logging
import logging.config
from os.path import dirname
from ns.model import db
from ns.model.usage_tracking import ESTrackUsage
from ns.model.usage_tracking_storage import UsageTrackingStorage
from ns.util.make_json_app import make_json_app
from ns.util.dynamic_config_client import get_configuration_object

from sqlalchemy.engine import create_engine

logging.config.fileConfig("logging.conf")
tmpl_dir = os.path.join(dirname(dirname(dirname(os.path.abspath(__file__)))), 'templates')

app = Flask("ns_app", instance_relative_config=True, template_folder=tmpl_dir)
env = os.environ.get('ENV', 'local')

if env in ['dev', 'uat', 'staging', 'demo', 'prod']:
    d = get_configuration_object(env, "northstar_application")
    app.config.from_object(d)
else:
    app.config.from_pyfile(env + '.config.py')
app.config['CURRENT_ENV'] = env

app = make_json_app(app)
CORS(app)

db.init_app(app)

# This logic is moved to public_api/__init__.py because we want to track that public_app; Leaving this commented in
# case we want to track any api for this app (EAP).

# mysql_db_engine = create_engine(app.config.get("SQLALCHEMY_DATABASE_URI"))
# create an object for storage (UsageTrackingStorage inherits default impl of SQLStorage (provided by flask).
# usage_tracking_storage = UsageTrackingStorage(mysql_db_engine, None, "rest_api_tracking", None)
# create object for tracking using app and storage above. This object will be imported by endpoint modules.
# track = ESTrackUsage(app, usage_tracking_storage)

from werkzeug import exceptions  # noqa: ignore=E402
from flask.ext.redis import FlaskRedis  # noqa: ignore=E402

from ns.model.account import Account  # noqa: ignore=E402
from ns.model.account_quota import AccountQuota  # noqa: ignore=E402
from ns.model.account_quota_ledger import AccountQuotaLedger    # noqa: ignore=E402
from ns.model.company_new import CompanyNew  # noqa: ignore=E402
from ns.model.crm_token import CRMToken  # noqa: ignore=E402
from ns.model.job import Job  # noqa: ignore=E402
from ns.model.job_status_history import JobStatusHistory  # noqa: ignore=E402
from ns.model.qualification import Qualification  # noqa: ignore=E402
from ns.model.recommendation import Recommendation  # noqa: ignore=E402
from ns.model.role import Role  # noqa: ignore=E402
from ns.model.seed import Seed  # noqa: ignore=E402
from ns.model.seed_candidate import SeedCandidate  # noqa: ignore=E402
from ns.model.seed_filter import SeedFilter  # noqa: ignore=E402
from ns.model.segment import Segment  # noqa: ignore=E402
from ns.model.source import Source  # noqa: ignore=E402
from ns.model.user import User  # noqa: ignore=E402
from ns.model.user_role import UserRole  # noqa: ignore=E402
from ns.model.user_segment import UserSegment  # noqa: ignore=E402
from ns.model.user_invitation import UserInvitation  # noqa: ignore=E402
from ns.model.tmp_account_segment import TmpAccountSegment  # noqa: ignore=E402
from ns.model.user_download import UserDownload  # noqa: ignore=E402
from ns.model.user_exported_company import UserExportedCompany  # noqa: ignore=E402
from ns.model.domain_dictionary import DomainDictionary  # noqa: ignore=E402
from ns.util import domain_util  # noqa: ignore=E402
from ns.model.session import RedisSessionInterface  # noqa: ignore=E402

from . import segment, status, user, auth, salesforce, insights, mas, \
    model, audience, proxy, metadata, token_mgmt  # noqa: ignore=E402

import httplib  # noqa: ignore=E402
from flask import jsonify  # noqa: ignore=E402


app.register_blueprint(status.blueprint)
app.register_blueprint(segment.blueprint)
app.register_blueprint(auth.blueprint)
app.register_blueprint(user.blueprint)
app.register_blueprint(salesforce.blueprint)
app.register_blueprint(insights.blueprint)
app.register_blueprint(mas.blueprint)
app.register_blueprint(model.blueprint)
app.register_blueprint(audience.blueprint)
app.register_blueprint(metadata.blueprint)
app.register_blueprint(proxy.blueprint)
app.register_blueprint(token_mgmt.blueprint)

redis_store = FlaskRedis(app, strict=True)

# Session
Session(app)
app.session_interface = RedisSessionInterface(redis=redis_store)


AUTH_ENABLE = app.config.get("AUTH_ENABLE")


@app.before_request
def before_request():

    anonymous_urls = [
        "/status",
        "/static/test.html",
        "/users/register",
        "/users/login",
        "/users/activate",
        "/users/forget_password",
        "/salesforce/call_back",
        "/users/confirm_invitation",
        "/users/chrome/register",
        "/v1/companies/enrich"
    ]
    path = request.path
    if AUTH_ENABLE and path not in anonymous_urls:
        if not auth.authenticated():
            raise exceptions.Unauthorized("Unauthorized")
    else:
        if request.args.get('__user_id'):
            user_id = request.args.get('__user_id')
            g.user = User.query.get(user_id)
        else:
            # Here is just for local test, we either create a user with email 'test@everstring.com'
            # Or pass in a '__user_id' like above
            g.user = User.get_user_by_email('test@everstring.com')


@app.errorhandler(httplib.UNPROCESSABLE_ENTITY)
def handle_unprocessable_entity(err):
    # webargs attaches additional metadata to the `data` attribute
    data = getattr(err, 'data')
    if data:
        messages = data['exc'].messages
    else:
        messages = ['Invalid request']
    return jsonify({
        'messages': messages,
    }), 422
