from flask import jsonify, Blueprint, current_app, request
from ns.util.bucket_util import bucketize
from ns.util.company_db_client import CompanyDBClient
from webargs.flaskparser import use_kwargs
from webargs import fields
from ns.util.webargs_parser import DictField
from werkzeug import exceptions
from ns.controller.app.permission import proxy_permission_check
blueprint = Blueprint('proxy', __name__)


@blueprint.route('/proxy', methods=['POST'])
@use_kwargs({
    "url": fields.Str(),
    "method": fields.Str(validate=lambda x: x in ['get', 'post']),
    "json": DictField(required=False, missing={})
})
@proxy_permission_check
def proxy(method, url, json):
    """
    proxy this request to company_enrichment service directly
    :param method:    request method of company_enrichment's endpoint
    :param url:       url of company_enrichment's endpoint
    :param json:      json data, for post request
    :return:
    """
    result = CompanyDBClient.instance().proxy(method, url, json)
    return jsonify(
        result
    )


@blueprint.route('/companies/profile', methods=['GET'])
@use_kwargs({
    "domains": fields.DelimitedList(fields.Str()),
    "expands": fields.DelimitedList(fields.Str(), missing='')
})
def get_company_profile(domains, expands):
    if not domains:
        raise exceptions.BadRequest("Invalid domains")

    params = {
        "domains": domains,
        "expands": expands
    }

    resp = CompanyDBClient.instance().proxy('post', '/company_enrichment/profile', params)
    company_profiles = resp["data"]["companies"]
    result = {profile["domain"]: profile for profile in company_profiles}

    employee_size_bucket = current_app.config.get('EMPLOYEE_SIZE_BUCKETS')
    revenue_bucket = current_app.config.get('REVENUE_BUCKETS')
    for item in result.values():
        item["employeeSize"] = bucketize(item["employeeSize"], employee_size_bucket)
        revenue = item["revenueRange"]
        item["revenueRange"] = bucketize((revenue if revenue else 0) * 1000, revenue_bucket)

    return jsonify(
        data=result
    )
