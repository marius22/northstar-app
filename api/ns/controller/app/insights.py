from flask import jsonify, Blueprint
from ns.model.indicator_white_list import IndicatorWhiteList

blueprint = Blueprint('insight', __name__)


@blueprint.route('/insights/categories', methods=['GET'])
def get_insight_categories():

    categories = IndicatorWhiteList.get_indicator_categories()

    return jsonify(
        data=categories
    )


@blueprint.route('/insights/<category_name>/details', methods=['GET'])
def get_insight_category_details(category_name):

    details = IndicatorWhiteList.get_indicator_category_details([category_name])

    return jsonify(
        data=[{"key": detail.indicator, "value": detail.display_name}
              for detail in details[category_name]]
    )
