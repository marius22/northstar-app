from flask import jsonify, Blueprint, current_app, request, g
from webargs.flaskparser import use_kwargs
from webargs import fields
from ns.controller.app.param_validator import check_account_id
from werkzeug import exceptions
import datetime
from ns.model.seed_csv_reader import SeedCSVReader
from ns.model.publish_field_mapping import PublishFieldMapping
from ns.model.model import Model
from ns.controller.app.permission import has_account_access
from ns.util.auth_util import get_effective_user

blueprint = Blueprint('model', __name__)


def _check_account_operation_permission(user, account):
    if not user.is_es_admin() and user.account_id != account.id:
        raise exceptions.Forbidden("Access Denied")
    return


@blueprint.route('/model/c2d', methods=['POST'])
@use_kwargs({
    "csvData": fields.List(fields.List(fields.Str()))
})
def get_seed_domains(csvData):
    seed_domains = SeedCSVReader.get_domains_from_file_data(csvData)
    return jsonify(
        data=seed_domains
    )


@blueprint.route('/accounts/<int:account_id>/fit_models/create_by_csv', methods=['POST'])
@use_kwargs({
    "modelName": fields.Str()
})
def create_fit_model(account_id, modelName):

    # every time customer upload a csv will create a new account fit model
    account = check_account_id(account_id)
    user = get_effective_user()
    user_id = user.id
    _check_account_operation_permission(user, account)

    csv_file = request.files['file']
    file_name = 'model_seed_domain_account_%d_%s.csv' \
                % (account_id, datetime.datetime.utcnow().isoformat())

    file_path = SeedCSVReader.write_seed_csv_to_local(file_name, csv_file)

    seed_companies = SeedCSVReader.get_seed_companies_from_file_path(file_path)

    models = account.batch_create_models(
        seed_companies,
        [Model(type=Model.FIT_MODEL, name=modelName),
         Model(type=Model.FEATURE_ANALYSIS, name=modelName)],
        owner_id=user_id
    )

    # submit the model run
    for model in models:
        model.submit_model_run()

    return jsonify(
        data=[model.serialized for model in models]
    )


@blueprint.route('/accounts/<int:account_id>/fit_models/<int:model_id>', methods=['DELETE'])
def delete_model(account_id, model_id):
    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)
    user = get_effective_user()
    user_id = user.id
    model = Model.get_by_id(model_id)
    # Any ES admins can delete a model. If not an ES admin, then user must be the model's owner.
    if not g.user.is_es_admin() and model.owner_id != user_id:
        raise exceptions.Forbidden("Model not owned by user. Delete not allowed.")

    PublishFieldMapping.delete_model_by_model_id(model_id)
    return jsonify(status="OK")


@blueprint.route('/accounts/<int:account_id>/fit_models', methods=['GET'])
@has_account_access
def get_fit_models(account_id):
    account = check_account_id(account_id)

    models = Model.get_by_account_id(g.account.id, [Model.FIT_MODEL],
                                     statuses=[Model.IN_PROGRESS, Model.IN_QUEUE, Model.COMPLETED, Model.FAILED])

    crm_fit_model = account.latest_completed_crm_fit_model
    if not crm_fit_model:
        crm_fit_model = account.latest_crm_fit_model

    if crm_fit_model:
        models.append(crm_fit_model)

    return jsonify(
        data=[item.serialized for item in models]
    )


@blueprint.route('/accounts/<int:account_id>/crm_fit_models', methods=['GET'])
def get_crm_fit_models(account_id):
    account = check_account_id(account_id)

    account_fit_model = Model.get_latest_completed_model(account_id, Model.ACCOUNT_FIT)
    if account_fit_model:
        status = Model.COMPLETED
    else:
        models = Model.get_account_crm_fit_model_by_statuses(account.id, [Model.IN_QUEUE, Model.IN_PROGRESS])
        if models:
            status = Model.IN_PROGRESS
        else:
            status = "NO_MODEL"

    return jsonify(
        data=status
    )
