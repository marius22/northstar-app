from functools import wraps

import logging
from flask import g, current_app
from marshmallow import missing
from webargs import fields
from webargs.flaskparser import use_args
from werkzeug import exceptions

from ns.model.segment import Segment
from ns.model.user import User
from ns.model.crm_token import CRMToken
from ns.model.user_role import UserRole
from ns.model.account import Account
from ns.model.role import Role
from ns.model.audience import Audience
from ns.model.account_quota import AccountQuota


logger = logging.getLogger(__name__)


def allow(role_list):
    def pass_on(function):
        @wraps(function)
        def wrap(*args, **kwargs):
            if current_app.config.get("ROLE_ENABLED", True):
                # if options.get('__as') and current_app.config.get('DEBUG_ROLE_PLAY_ENABLED'):
                #     user_roles = options['__as']
                # elif g.user:
                if g.user:
                    roles = UserRole.get_roles_by_user(g.user)
                    user_roles = [role.name for role in roles]
                else:
                    raise exceptions.Unauthorized("Unauthorized")

                if len(set(user_roles).intersection(role_list)) == 0:
                    raise exceptions.Forbidden("Permission denied")
            return function(*args, **kwargs)

        return wrap

    return pass_on


def deny(role_list):
    def pass_on(function):
        @wraps(function)
        def wrap(*args, **kwargs):
            if current_app.config.get("ROLE_ENABLED", True):
                if not g.user:
                    raise exceptions.Unauthorized("Unauthorized")
                roles = UserRole.get_roles_by_user(g.user)
                user_roles = [role.name for role in roles]
                if len(set(user_roles) - set(role_list)) == 0:
                    raise exceptions.Forbidden("Permission denied")
            return function(*args, **kwargs)

        return wrap

    return pass_on


def has_segment_access(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")
        segment_id = kwargs['segment_id']
        g.segment = Segment.get_by_id(segment_id)
        if not g.segment:
            raise exceptions.BadRequest("Invalid segment ID.")
        if not User.has_segment_access(g.user.id, g.segment):
            raise exceptions.Forbidden("You don't have access to this segment.")
        return function(*args, **kwargs)

    return wrap


def has_account_access(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")

        account_id = kwargs['account_id']
        g.account = Account.get_by_id(account_id)
        if not g.account:
            raise exceptions.BadRequest("Invalid account ID.")
        if not User.has_account_access(g.user.id, g.account.id):
            raise exceptions.Forbidden("You don't have access to this account.")
        return function(*args, **kwargs)

    return wrap


def has_audience_access_permission(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")

        audience_id = kwargs['audience_id']
        g.audience = Audience.get_by_id(audience_id)

        if not g.audience:
            raise exceptions.BadRequest("Invalid audience ID.")
        if not User.has_audience_access_permission(g.user.id, g.audience.id):
            raise exceptions.Forbidden("You don't have access to this audience.")
        return function(*args, **kwargs)

    return wrap


def has_audience_edit_permission(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")

        audience_id = kwargs['audience_id']
        g.audience = Audience.get_by_id(audience_id)

        if not g.audience:
            raise exceptions.BadRequest("Invalid audience ID.")
        if not User.has_audience_edit_permission(g.user.id, g.audience.id):
            raise exceptions.Forbidden("You don't have access to this audience.")
        return function(*args, **kwargs)

    return wrap


def segment_completed_required(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if not g.segment.is_completed():
            raise exceptions.Forbidden("Please wait until segment is completed.")
        return func(*args, **kwargs)
    return wrap


def has_expansion_permission(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")
        account_quota = AccountQuota.get_by_account_id(g.user.account_id)
        allowed_publish_new = account_quota.enable_not_in_crm
        publish_new = args[0]['publishNew']
        if publish_new:
            if not allowed_publish_new:
                raise exceptions.Forbidden("Not permitted to publish 'Not In CRM' companies.")
        return func(*args, **kwargs)
    return wrap


def check_change_segment_owner_permission(user, segment):
    # owner
    if segment.owner_id == user.id:
        return
    # es super admin / es admin
    if user.is_es_admin():
        return
    # account admin
    if user.is_account_admin() \
            and user.account_id == segment.owner.account_id:
        return
    raise exceptions.Forbidden("You cannot change the owner for this segment.")


def proxy_permission_check(function):

    @wraps(function)
    def wrap(*args, **kwargs):
        if not g.user:
            raise exceptions.Unauthorized("Unauthorized")
        url = kwargs['url']
        payload = kwargs['json']
        audience_check(url, payload)
        org_check(url, payload)
        return function(*args, **kwargs)
    return wrap


def audience_check(url, payload):
    url_parts = url.split('/')
    if url.startswith("/common/audiences/"):
        audience_id = url_parts[3]
        audience = Audience.get_by_id(int(audience_id))
        if not audience:
            raise exceptions.BadRequest("Invalid audience ID.")
        if not User.has_audience_access_permission(g.user.id, audience.id):
            raise exceptions.Forbidden("You don't have access to this audience.")
    audience_ids = payload.get("audienceIds")
    if audience_ids:
        for audience_id in audience_ids:
            audience = Audience.get_by_id(audience_id)
            if not audience:
                raise exceptions.BadRequest("Invalid audience ID.")
            if not User.has_audience_access_permission(g.user.id, audience.id):
                raise exceptions.Forbidden("You don't have access to this audience.")


def org_check(url, payload):
    url_parts = url.split('/')
    org_id = None
    if url.startswith("/common/crm/sfdc/"):
        org_id = url_parts[4]
    if payload.get("orgId"):
        org_id = payload.get("orgId")
    if org_id:
        crm_token = CRMToken.get_sfdc_token_by_user_id(g.user.id)
        if not crm_token:
            raise exceptions.BadRequest("Invalid org ID.")
        if org_id != crm_token.organization_id:
            raise exceptions.Forbidden("You don't have access to this sfdc org.")
