from werkzeug import exceptions
from ns.model.account import Account
from ns.model.user import User
from ns.model.segment import Segment
from ns.util.string_util import is_blank
from ns.model.model import Model
from ns.model.audience import Audience


def check_segment_id(segment_id):

    if not segment_id > 0:
        raise exceptions.BadRequest("Invalid segmentId")

    segment = Segment.get_by_id(segment_id)

    if segment is None:
        raise exceptions.BadRequest("Invalid segmentId")

    return segment


def check_account_id(account_id):

    if not account_id > 0:
        raise exceptions.BadRequest("Invalid accountId")

    account = Account.get_by_id(account_id)

    if account is None:
        raise exceptions.BadRequest("Invalid accountId")

    return account


def check_user_email(email):

    if is_blank(email):
        raise exceptions.BadRequest("Invalid email")
    user = User.get_user_by_email(email)

    if user is None:
        raise exceptions.BadRequest("Invalid email")

    return user


def check_user_id(user_id):

    if not user_id > 0:
        raise exceptions.BadRequest("Invalid userId")
    user = User.get_by_id(user_id)

    if user is None:
        raise exceptions.BadRequest("Invalid userId")

    return user


def check_and_get_model(model_id):
    if not model_id:
        raise exceptions.BadRequest("Invalid modelId")
    model = Model.get_by_id(model_id)

    if not model:
        raise exceptions.BadRequest("Invalid modelId")
    return model


def check_and_get_audience(audience_id):
    if not audience_id:
        raise exceptions.BadRequest("Invalid audienceId")
    audience = Audience.get_by_id(audience_id)

    if not audience:
        raise exceptions.BadRequest("Invalid audienceId")

    return audience
