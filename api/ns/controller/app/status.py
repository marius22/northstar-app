"""
Base controller that exhibits API endpoints for general use.
"""
import os
from flask import jsonify, Blueprint, current_app

blueprint = Blueprint('status', __name__)

version = os.getenv('VERSION')


@blueprint.route('/status', methods=['GET'])
def status_check():
    """
    Sanity check endpoint for monitoring
    """
    current_app.logger.info('Access status')
    return jsonify(status='OK', version=version)


@blueprint.route('/', methods=['GET'])
def greet():
    """
    Default greet for whoever drops by.

    TODO: add endpoint documentation(or the link) here.
    """
    return 'EverString: Hello, North Star!'
