"""
controller for SF
"""
import time
import datetime
import uuid
import jwt
import urllib

from flask import jsonify, g, Blueprint, \
    request, make_response, redirect, current_app
from webargs.flaskparser import use_kwargs
import logging
import traceback
from werkzeug import exceptions
from werkzeug.exceptions import NotFound
from ns.model.crm_token import CRMToken, SFDCToken
from ns.model.publish_field_mapping import PublishFieldMapping
from ns.model.salesforce import publish_domain_to_sfdc, NSSalesforce, get_salesforce_fields
from ns.model.user import User
from ns.model import salesforce
from param_validator import check_account_id
from ns.model.sfdc_config import SFDCConfig
from ns.controller.app.permission import allow
from ns.model.role import Role
from ns.model.sfdc_config import SFDCConfigField
from ns.model.account_quota import AccountQuota, QuotaExceedError
from ns.model.account import Account
from ns.model.indicator_white_list import IndicatorWhiteList
from ns.model.account_exported_company import AccountExportedCompany
from simple_salesforce.api import SalesforceError
from webargs import fields
from webargs.flaskparser import use_args
from ns.util.auth_util import get_effective_user
from ns.util.company_db_client import CompanyDBClient
from ns.util.webargs_parser import DomainField
from sqlalchemy.exc import IntegrityError
from ns.util.sfdc_permission_utils import check_sfdc_permission, SalesforcePermissionError

blueprint = Blueprint('salesforce', __name__)

logger = logging.getLogger(__name__)

LONG_FUTURE_TIMESTAMP = 4070880000
REAL_TIME_SCORING_TURN_OFF = []


def get_config(key):
    return current_app.config.get(key)


@blueprint.route('/salesforce/integrated', methods=['GET'])
def integrated():
    """
    whether this user has already integrated
    :return:
    """
    user = get_effective_user()
    token = CRMToken.get_sfdc_token(user.id)

    return jsonify({"data": True if token else False})


@blueprint.route('/salesforce/revoke', methods=['DELETE'])
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def revoke():

    user = get_effective_user()
    CRMToken.delete_token_by_user_id_type(user.id, CRMToken.SALESFORCE)
    CRMToken.delete_token_by_user_id_type(user.id, CRMToken.SALESFORCE_SANDBOX)
    account = Account.get_by_id(user.account_id)
    account.update_real_time_scoring_object(REAL_TIME_SCORING_TURN_OFF)
    return jsonify(
        status="OK"
    )


@blueprint.route('/salesforce/integration', methods=['GET'])
@use_args({
    'isSandbox': fields.Boolean()
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def integration(args):
    """
    redirect to SF to complete the SF integration
    :return:
    """
    user = get_effective_user()
    if args.get('isSandbox') is True:
        auth_url = get_config("AUTH_URL_SANDBOX")
        state = str(user.id) + ',sandbox'
    else:
        auth_url = get_config("AUTH_URL")
        state = user.id

    return make_response(
        redirect('%s?response_type=code'
                 '&client_id=%s&redirect_uri=%s&state=%s&display=popup'
                 % (auth_url, get_config("CLIENT_ID"),
                     get_config("CALL_BACK_URL"), state)))


@blueprint.route('/salesforce/call_back', methods=['GET'])
def auth_call_back():
    """
    call back for SF to complete the SF integration
    :return:
    """
    try:
        code = request.args.get("code")
        # state is user_id
        state = request.args.get("state")
        crm_type = CRMToken.SALESFORCE
        results = state.split(",")
        user_id = results[0]

        if len(results) == 2 and results[1] == 'sandbox':
            crm_type = CRMToken.SALESFORCE_SANDBOX
        user = User.get_by_id(user_id)

        assert user, "Can not find a user by userId: [%s]" % user_id

        token_dict = salesforce.request_access_token(code, crm_type)

        logger.info('token_dict: {token_dict}'.format(token_dict=token_dict))

        # contain the SF user email.........    user info
        sfdc_basic_user = salesforce.get_user_info(
            token_dict.get(SFDCToken.SALESFORCE_ID_KEY),
            token_dict.get(SFDCToken.ACCESS_TOKEN_KEY))
        logger.info("sfdc_user: {sfdc_user}".format(sfdc_user=sfdc_basic_user))

        # Get user profile role
        sfdc_basic_user[SFDCToken.ROLE_KEY] = check_sfdc_permission(user.account_id, user_id,
                                                                    sfdc_basic_user, token_dict, crm_type)

        token_dict.update(sfdc_basic_user)
        CRMToken.create_sfdc_token(user.account_id, user.id, token_dict, crm_type)

        logger.info("salesforce integration succeed for user: {%s}" % user.email)
    except SalesforcePermissionError, permission_error:
        return make_response("<html>%s</html>" % permission_error)
    except SalesforceError, sfError:
        return make_response("<html>%s</html>" % sfError.content[0].get("message"))
    except Exception, e:
        traceback.print_exc()
        logger.warn("Integration failed for user [%s], e: [%s]" % (user_id, e))

    return make_response("<html><script>window.close();</script></html>")


@blueprint.route('/salesforce/<int:account_id>/config', methods=['GET'])
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def get_sfdc_config(account_id):
    check_account_id(account_id)
    config = SFDCConfig.get_by_account_id(account_id)

    config_dict = config.config_dict
    config_dict["insightHeadersDef"] = current_app.config.get("SFDC_APP_COMPANY_INSIGHTS_HEADERS")
    result = {"enrich_enabled": config.enrich_enabled,
              "enrich_score": config.enrich_score,
              "enrich_firmographics": config.enrich_firmographics,
              "enrich_start_ts": str(config.enrich_start_ts),
              "config": config_dict
              }

    return jsonify(
        data=result
    )


@blueprint.route('/salesforce/<int:account_id>/config', methods=['POST'])
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
@use_kwargs({
    'config': SFDCConfigField(required=True),
    'enrich_enabled': fields.Boolean(required=False, default=False),
    'enrich_score': fields.Boolean(required=False, default=False),
    'enrich_firmographics': fields.Boolean(required=False, default=False),
    'enrich_start_ts': fields.Int()
})
def save_sfdc_config(account_id, config, enrich_enabled, enrich_score,
                     enrich_firmographics, enrich_start_ts):

    check_account_id(account_id)
    if enrich_start_ts > 0:
        enrich_start_ts = datetime.datetime.utcfromtimestamp(enrich_start_ts / 1000)
    else:
        enrich_start_ts = datetime.datetime.utcfromtimestamp(time.time())

    config = SFDCConfig.save_config(account_id, config, enrich_enabled, enrich_score,
                                    enrich_firmographics, enrich_start_ts)

    return jsonify(
        data=config.config_dict
    )


@blueprint.route('/salesforce/managed_package_link', methods=['GET'])
@use_args({
    'isSandbox': fields.Boolean()
})
def get_managed_package_link(args):
    """Return the current link to install the managed package."""
    if args.get('isSandbox') is True:
        package_link = get_config("PACKAGE_LINK_SANDBOX")
    else:
        package_link = get_config("PACKAGE_LINK")

    return jsonify({
        'link': package_link
    })


@blueprint.route('/salesforce/package_link', methods=['GET'])
@use_args({
    'isSandbox': fields.Boolean()
})
def get_package_link(args):
    if args.get('isSandbox') is True:
        package_link = get_config("PACKAGE_LINK_SANDBOX")
    else:
        package_link = get_config("PACKAGE_LINK")
    return package_link


def _go_to_zendesk_page(url):
    account = g.user.account
    user_fields = {
        "account_name": account.name,
        "account_id": account.id,
        "role": ",".join(g.user.role_names),
        "status": "inactive" if account.deleted else "active"
    }

    payload = {"iat": int(time.time()),
               "jti": str(uuid.uuid1()),
               "name": g.user.first_name or g.user.email,
               "email": g.user.email,
               "user_fields": user_fields
               }
    jwt_string = jwt.encode(payload, current_app.config['ZENDESK_API_KEY'])
    sso_url = "https://" + current_app.config['ZENDESK_SUB_DOMAIN'] + ".zendesk.com/access/jwt?jwt=" + jwt_string
    sso_url += "&return_to=" + urllib.quote(url)
    return redirect(sso_url)


@blueprint.route('/salesforce/package_guide_link', methods=['GET'])
@use_kwargs({
    'linkType': fields.Str(missing='sfdc')
})
def get_package_guide_link(linkType):
    # I'm overloading this to support other guide links in addition
    # to SFDC. The functionality is straightforward enough that I
    # don't anticipate problems with this growing unmanageable.
    link_type_cfg_key_map = {
        'sfdc': 'PACKAGE_GUIDE_LINK',
        'mkto': 'MARKETO_GUIDE_LINK',
        'sprt': 'SUPPORT_LINK'
    }
    cfg_key = link_type_cfg_key_map[linkType]
    guide_link = current_app.config.get(cfg_key, "")
    return _go_to_zendesk_page(guide_link)


@blueprint.route('/zendesk/go_to', methods=['GET'])
@use_kwargs({
    'url': fields.Str()
})
def go_to_zendesk(url):
    return _go_to_zendesk_page(url)


@blueprint.route('/salesforce/crm_type', methods=['GET'])
def get_crm_type():
    """
    whether this user has already integrated
    :return:
    """
    user = get_effective_user()
    token = CRMToken.get_sfdc_token_by_user_id(user.id)
    return jsonify({"data": token.crm_type if token else False})


@blueprint.route('/salesforce/find_in_crm', methods=['GET'])
@use_kwargs({
    'findOn': fields.DelimitedList(fields.Str, required=True),
    "accountId": fields.Int(required=True),
    'domains': fields.DelimitedList(fields.Str, required=True,
                                    validate=lambda val: len(val) > 0)
})
def find_in_crm(findOn, accountId, domains):
    crm_tokens = CRMToken.get_salesforce_admin_by_crm_type_accounts([accountId])
    if not crm_tokens:
        raise exceptions.BadRequest("Please do Salesforce integration first")

    crm_token = crm_tokens[0]
    org_id = crm_token.organization_id
    result = CompanyDBClient.instance().find_in_crm(findOn, org_id, domains)

    return jsonify(data=result)


@blueprint.route('/salesforce/add_sfdc_obj', methods=['POST'])
@use_kwargs({
    "account_id": fields.Int(required=True),
    "domain": DomainField(required=True)
})
def add_sfdc_object(account_id, domain):
    try:
        # Number of accounts created/enriched.
        num_accounts = publish_domain_to_sfdc(account_id, domain)
    except QuotaExceedError:
        raise exceptions.BadRequest("0 account credits available")

    return jsonify(
        data=num_accounts
    )


@blueprint.route('/salesforce/schema/<string:objectName>', methods=['GET'])
def get_schema(objectName):
    user = get_effective_user()
    try:
        SFDCToken.get_sfdc_crm_token(user.account_id)
    except:
        raise exceptions.BadRequest("Account is not integrated.")
    object_fields = get_salesforce_fields(user.account_id, objectName)

    return jsonify(
        data=object_fields
    )
