import logging
from flask import jsonify, g, Blueprint
from webargs.flaskparser import use_kwargs
from webargs import fields
from werkzeug import exceptions
import json

from ns.model.mas_token import Token, MASToken
from ns.model.mas_field_mapping import MASFieldMapping, Mapping
from ns.util.string_util import is_not_blank
from ns.util.marketo.api import Marketo
from ns.util.auth_util import get_effective_user
from ns.util.webargs_parser import DictField

from ns.controller.app.permission import allow
from ns.model.role import Role
from ns.model.audience import Audience
from ns.constants.default_field_mapping_constants import CONTACT_DEFAULT_FIELD_MAPPING_FOR_MAS

blueprint = Blueprint('mas', __name__)

logger = logging.getLogger(__name__)


@blueprint.route('/mas/integration', methods=['POST'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
    'token': Token(required=True)
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def save_mas_token(mas_type, token):
    marketo = Marketo(token.get("client_id"), token.get("client_secret"), token.get("endpoint"))
    marketo.authenticate()
    user = get_effective_user()
    account_id = user.account.id
    MASToken.save_token(account_id, mas_type, token)
    return jsonify(
        data="ok"
    )


@blueprint.route('/mas/revoke_integration', methods=['POST'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def revoke_mas(mas_type):
    user = get_effective_user()
    account_id = user.account.id

    existing_integration = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    existing_integration.delete()

    return jsonify(
        data="ok"
    )


@blueprint.route('/mas/integration', methods=['GET'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
def get_mas_token(mas_type):
    user = get_effective_user()
    account_id = user.account.id
    result_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    return jsonify(
        data=result_token.token_info if result_token else None
    )


@blueprint.route('/mas/fields_mapping', methods=['POST'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
    "mapping": Mapping(required=True)
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def save_mas_fields_mapping(mas_type, mapping):
    """Sets the mapping to the database to exactly the mapping passed in."""
    user = get_effective_user()
    account_id = user.account.id

    check_field_mapping(mapping)

    if "email" not in mapping.get("standard"):
        raise exceptions.BadRequestKeyError("No email field mapping in the mapping")
    MASFieldMapping.save_mapping(account_id, mas_type, mapping)

    current_mapping = MASFieldMapping.get_one_by_account_id_mas_type(account_id, mas_type)
    return jsonify(
        data=current_mapping.serialized
    )


def check_field_mapping(mapping):
    all_mkt_fields = list()
    all_es_fields = list()
    for field_type in mapping:
        if field_type == "isValid":
                continue
        one_type_fields = mapping.get(field_type)
        for es_field in one_type_fields:
            mkt_field = one_type_fields.get(es_field)
            all_mkt_fields.append(mkt_field)
            all_es_fields.append(es_field)
            if (not mkt_field) or mkt_field.lower() == 'null' or mkt_field.lower() == 'none':
                raise exceptions.ExpectationFailed("{} didn't get a good field mapping".format(es_field))
    if len(set(all_mkt_fields)) != len(set(all_es_fields)):
        raise exceptions.ExpectationFailed("You have duplicate marketo fields")


@blueprint.route('/mas/marketo_lead_fields', methods=['GET'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
def get_marketo_lead_fields(mas_type):
    user = get_effective_user()
    account_id = user.account.id
    mas_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    if not mas_token:
        raise exceptions.ExpectationFailed("Didn't have integration")
    token_info = mas_token.token_info
    marketo = Marketo(token_info.get("client_id"), token_info.get("client_secret"), token_info.get("endpoint"))
    lead_schema = marketo.get_lead_schema()
    return jsonify(
        data=lead_schema
    )


@blueprint.route('/mas/marketo_list', methods=['GET'])
@use_kwargs({
    "mas_type": fields.Str(required=True, validate=is_not_blank),
})
def get_marketo_list(mas_type):
    user = get_effective_user()
    account_id = user.account.id
    mas_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    if not mas_token:
        raise exceptions.ExpectationFailed("Didn't have integration")
    token_info = mas_token.token_info
    marketo = Marketo(token_info.get("client_id"), token_info.get("client_secret"), token_info.get("endpoint"))
    marketo_list = marketo.get_list()
    logger.info(marketo_list)
    return jsonify(
        data=filter_marketo_list(marketo_list)
    )


def filter_marketo_list(mas_list):
    result_list = list()
    for one in mas_list:
        if "_EXCEPTION" not in one.get("name"):
            result_list.append(one)
    return result_list


@blueprint.route('/mas/es_fields', methods=['GET'])
def get_es_fields_list():
    es_standard_fields = list()
    es_company_fields = ["domain", "companyName", "revenue", "employeeSize",
                         "industry", "state", "zipcode", "country", "city", "esSource", "esEnriched"]
    es_contact_fields = ["directPhone", "email", "firstName", "lastName", "manageLevel", "jobTitle"]

    es_standard_fields.extend(es_company_fields)
    es_standard_fields.extend(es_contact_fields)

    user = get_effective_user()
    if user.is_es_admin() or user.is_account_admin():
        audiences = user.account.all_owned_audiences(
            meta_type=Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE))
    else:
        audiences = user.owned_or_shared_audiences(Audience.META_TYPE_MAPPING.get(Audience.META_TYPE_AUDIENCE))

    audiences = [(audience.id, audience.name) for audience in audiences]
    es_audience_fields = audiences

    all_fields = []
    for one in es_standard_fields:
        record = dict()
        record["type"] = "standard"
        record["name"] = one
        all_fields.append(record)

    for one in es_audience_fields:
        record = dict()
        record["type"] = "audience"
        record["name"] = one[1]
        record["id"] = one[0]
        all_fields.append(record)

    return jsonify(
        data=all_fields
    )


@blueprint.route('/mas/contact_default_field_mapping', methods=['GET'])
def get_default_contact_field_mapping():
    return jsonify(
        data=CONTACT_DEFAULT_FIELD_MAPPING_FOR_MAS
    )
