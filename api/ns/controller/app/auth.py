from flask import jsonify, Blueprint, request, session, \
    g, current_app, url_for, make_response, redirect, render_template
from werkzeug import exceptions
import json
from ns.model.role import Role
from ns.model.user import User
from ns.model.account import Account
from ns.util.password_util import generate_random_pw
from ns.model.code import Code
from ns.util.string_util import generate_hash_str
from ns.util.email_client import NSEmailClient
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from ns.util.string_util import is_not_blank, is_blank
from ns.util import domain_util
from ns.util.auth_util import check_is_locked, set_session_ns_login_attempt,  \
    reset_login_attempts, get_session_ns_login_attempt
from ns.controller.app.permission import allow
from ns.model.user_invitation import UserInvitation
from ns.model.user_role import UserRole
from ns.controller.app import param_validator
from ns.model.domain_dictionary import DomainDictionary
import httplib
import tldextract
from ns.controller.app.param_validator import check_account_id
import logging
from ns.model.user_register_log import UserRegisterLog
from ns.constants import auth_constants

logger = logging.getLogger(__name__)

blueprint = Blueprint('auth', __name__)


def authenticated():
    """
    whether has a valid session in the context.
    :return:
    """
    is_valid = session.is_valid()
    if is_valid:
        g.user = User.query.get(session["userId"])
        if not User.has_eap_access(g.user):
            return False
        ghost_user_id = session.get('ghostUserId')
        if ghost_user_id:
            g.ghost_user = User.get_by_id(ghost_user_id)

    return is_valid


def login_user(user, remember_me=False):
    """
    login user,
    enrich authentication and authorization information.
    :param user:
    :param remember_me:
    :return:
    """
    if user.is_deleted:
        raise exceptions.Forbidden("Invalid user.")
    if not user.activated:
        raise exceptions.Forbidden("Only an active user can log in.")

    session.refresh()
    session.permanent = remember_me
    session["userId"] = user.id


def logout_user():
    """
    Logout user
    """
    session.logout()


def fresh_required(view_fun):
    """
    Decorator, need a fresh login status
    :param view_fun:
    :return:
    """

    def inner_fun(*args):
        if not session.is_fresh():
            raise exceptions.Unauthorized("You need to log in again.")
        return view_fun(*args)

    return inner_fun


def get_confirm_link(url, code, redirect_url, es_app=None):
    """
    common method, generate a confirm link for the confirm email.
    :param url:   specific view function url
    :param code:  a random code
    :param redirect_url:   the url which the user will redirect to
    :param es_app:   confirm is sending for wich app
    :return:
    """
    ns_host = current_app.config.get("NS_STAR_APP_API_HOST")
    if es_app:
        return "%s%s?code=%s&es_app=%s&redirect_url=%s" % (ns_host, url, code, es_app, redirect_url)
    return "%s%s?code=%s&redirect_url=%s" % (ns_host, url, code, redirect_url)


def check_user_not_exist(email):
    """
    ensure the email hadn't used by other users
    :param email:
    :return:
    """
    db_user = User.get_user_by_email(email)
    if db_user:
        raise exceptions.BadRequest("User already exists.")


email_password = {
    'email': fields.Str(required=True, validate=is_not_blank),
    'password': fields.Str(required=True, validate=is_not_blank)
}


def check_domain_for(email):
    domain = domain_util.get_domain_from_email(email)

    if is_blank(domain):
        raise exceptions.BadRequest("Invalid email")

    if DomainDictionary.is_free_domain(domain):
        raise exceptions.BadRequest("Personal email addresses (gmail, yahoo) are not valid work"
                                    "email addresses and cannot be used to sign up.")
    if DomainDictionary.is_competitor_domain(domain):
        raise exceptions.Forbidden("Personal email addresses (gmail, yahoo) are not valid work"
                                   "email addresses and cannot be used to sign up.")


def _register_user(email, password, account_id=-1):
    use_trial_account = account_id < 0
    if use_trial_account:
        account = Account.create_trial_account(Account.generate_name_by_email(email))
        account_id = account.id

    user = User.create_user(account_id, email, password)
    role = Role.get_by_name(Role.TRIAL_USER)
    UserRole.create(user, role)

    if not use_trial_account:
        account = Account.get_by_id(account_id)
        account.add_user(user)

    return user


@blueprint.route('/users/register', methods=['POST'])
@use_args({
    'email': fields.Str(required=True, validate=is_not_blank),
    'password': fields.Str(required=True, validate=is_not_blank),
    'accountId': fields.Int(),
    'sendEmail': fields.Boolean()
})
def register(args):
    email = args["email"]
    password = args["password"]
    account_id = args["accountId"] if args.get('accountId') else -1
    send_email = args["sendEmail"]

    check_domain_for(email)
    check_user_not_exist(email)

    user = _register_user(email, password, account_id)

    if send_email:
        login_user(user, remember_me=False)
        code = Code.create_code(generate_hash_str(email), email, Code.EMAIL_CODE)
        send_confirmation_email(email, password, code.code)

    return jsonify({"status": "OK", "data": user.serialized}), httplib.CREATED


@blueprint.route('/users/resend_confirmation_email', methods=['POST'])
@use_args({'email': fields.Str(required=True, validate=is_not_blank)})
def resend_confirmation_email(args):
    email = args["email"]

    Code.remove_by_email_code_type(email, Code.EMAIL_CODE)
    code = Code.create_code(generate_hash_str(email), email, Code.EMAIL_CODE)

    send_confirmation_email(email, '*********', code.code)
    return jsonify(status="OK"), httplib.CREATED


def send_confirmation_email(email, password, code):
    redirect_url = current_app.config.get("NS_STAR_APP_HOST")
    confirm_link = get_confirm_link(url_for("auth.activate_user"), code, redirect_url)
    NSEmailClient.send_email(
        email,
        "Welcome to EverString! Please Confirm Your Email to Start Predicting",
        render_template("email_confirm.html",
                        email=email,
                        password=password,
                        confirm_link=confirm_link
                        )
    )


@blueprint.route('/users/activate', methods=['GET'])
@use_args({
    'code': fields.Str(required=True, validate=is_not_blank),
    'redirect_url': fields.Str(required=True, validate=is_not_blank)
})
def activate_user(args):
    """
    the callback for user to confirm their email address
    :return:
    """
    confirm_code = args.get("code")
    redirect_url = args.get("redirect_url")

    code = Code.get_by_code(confirm_code)
    if code is None:
        raise exceptions.BadRequest("Confirmation email link invalid.")

    # set user to active after they confirmed their email
    user = User.get_user_by_email(code.value)
    user.activate()

    # force to refresh login
    login_user(user, remember_me=False)

    return make_response(redirect(redirect_url))


@blueprint.route('/users/login', methods=['POST'])
@use_args(email_password)
def login(args):
    """
    login.
    if already logged in, return directly
    :return:
    """
    user = User.get_user_by_email(args['email'])
    if user and not User.has_eap_access(user):
        logging.warning('Login Forbidden. Your credentials work only with the Chrome plugin. \t'
                        'user_email: %s \t'
                        'user_id: %d \t'
                        'account_id: %d' % (user.email, user.id, user.account_id))
        raise exceptions.BadRequest("Your credentials work only with the Chrome plugin")
    # Check if the account is expired or not based on expired_ts from account_quota ?
    account = user.account
    if account:
        if account.account_quota.expired():
            raise exceptions.BadRequest("Your account is expired. Please contact support.")
    else:
        raise exceptions.BadRequest("Account not found.")

    user_dict = _user_login(args, user)
    return jsonify(data=user_dict)


def _user_login(args, user):
    if not session.is_fresh():
        if user is None:
            logging.warning('Login Forbidden. Username / password combination is incorrect '
                            'or no such user record found. \t'
                            'user_entered_email: %s' % args['email'])
            raise exceptions.BadRequest('Username / password combination is incorrect or no such user record found.')

        login_attempts = get_session_ns_login_attempt()
        if login_attempts:
            check_is_locked(login_attempts)

        if not user.check_password(args['password']):
            login_attempts += 1
            set_session_ns_login_attempt(login_attempts)
            check_is_locked(login_attempts)

            logging.warning('Login Forbidden. Password is incorrect. \t'
                            'user_entered_email: %s' % args['email'])
            raise exceptions.Unauthorized('Login Forbidden. Password is incorrect. Failed Attempt: %d of %d'
                                          % (login_attempts, auth_constants.MAX_FAILED_LOGIN_ATTEMPTS))

        try:

            login_user(user, remember_me=False)
            # on successful login (either after cookie has expired or password has been reset in which case we have
            # deleted cookie, we need to reset login attempt
            # (session var is source of truth, so we reset that here and then update / delete cookie in save_session)
            reset_login_attempts()

            logger.info('Login successful. \t'
                        'user_email: %s \t'
                        'user_id: %d \t'
                        'account_id: %d' % (user.email, user.id, user.account_id))
        except exceptions.Forbidden as forbidden_error:
            logging.warning('Login Forbidden. ' + forbidden_error.description + ' \t' +
                            'user_email: %s \t'
                            'user_id: %d \t'
                            'account_id: %d' % (user.email, user.id, user.account_id))
            raise forbidden_error

    user_dict = user.serialized_with_quota
    user_dict['ghostUser'] = g.ghost_user.serialized_with_quota \
        if hasattr(g, 'ghost_user') else None
    old_version_user_time = current_app.config.get("TIME_TO_ALLOW_USE_PREVIOUS_VERSION")
    if user.is_es_admin() or user.created_ts < old_version_user_time:
        user_dict["canUsePreviousVersion"] = True
    else:
        user_dict["canUsePreviousVersion"] = False
    return user_dict


@blueprint.route('/users/forget_password', methods=['PUT'])
@use_args({'email': fields.Str(required=True, validate=is_not_blank)})
def forget_password(args):
    """
    send a confirm email to user so that they can reset their password
    :return:
    """
    email = args['email']
    user = User.get_user_by_email(email)
    if user is None:
        raise exceptions.BadRequest("Email doesn't exist.")
    if not user.activated:
        raise exceptions.Forbidden("Only an active user can reset password.")

    # According to Amit, we just generate the new password for the email.
    # There is no link for confirmation in email
    password = generate_random_pw(10)
    user.reset_password(password)

    # once we have told user password and reset it,
    # we should rest the cookie value (allow login on correct user / password combo)
    # (session variable is source of truth, so we reset that here and then update / delete cookie in save_session)
    reset_login_attempts()

    NSEmailClient.send_email(
        email,
        "New EverString Password",
        render_template("forget_password.html",
                        email=email,
                        password=password,
                        login_link=current_app.config.get("NS_STAR_APP_HOST")
                        )
    )
    return jsonify(status="OK"), httplib.OK


@blueprint.route('/users/change_password', methods=['POST'])
def change_password():
    """
    change password
    :return:
    """
    data = request.get_json()
    if not data or "new_password" not in data:
        raise exceptions.BadRequest("Missing new password.")
    g.user.reset_password(data['new_password'])
    # once we have forced password reset, we should rest the cookie value (allow login on correct user / password combo)
    # (session variable is source of truth, so we reset that here and then update / delete cookie in save_session)
    reset_login_attempts()

    return jsonify(status="OK"), httplib.OK


@blueprint.route('/users/logout', methods=['DELETE'])
def logout():
    logout_user()
    return jsonify(status="OK"), httplib.ACCEPTED


def send_confirm_email_for_invite(email, password, invite_code):
    template_name = "new_user_invite.html"
    redirect_url = current_app.config.get("NS_STAR_APP_HOST") + "/#/audience/console"
    confirm_link = get_confirm_link(url_for("auth.confirm_invitation"), invite_code, redirect_url)
    customer_success_emails = current_app.config.get("CUSTOMER_SUCCESS_EMAILS", [])
    NSEmailClient.send_email(
        email,
        "You Are Invited to EverString",
        render_template(template_name,
                        email=email,
                        password=password,
                        confirm_link=confirm_link,
                        inviter_email=g.user.email),
        attachments=[],
        bcc_emails=customer_success_emails
    )


def check_target_account(user, target_account):
    """
    ES admin / ES super admin can invite user for any account
    Account admin can only invite user for his own account
    :param user:
    :param target_account:
    :return:
    """
    account = user.account
    if (not Role.is_es_admin(user.role_names)) \
            and target_account.id != account.id:
        raise exceptions.Forbidden("You can only invite users to your own account.")


def check_target_email(inviter, invitee_email):
    check_domain_for(invitee_email)
    admin_domain = domain_util.get_domain_from_email(inviter.email)
    target_domain = domain_util.get_domain_from_email(invitee_email)
    # es admin can invite user with any domain for any account
    # account admin can only invite users who have the same domain with him
    if (not inviter.is_es_admin()) \
            and (admin_domain != target_domain):
        raise exceptions.Forbidden("Your invited user must have an email with the same domain as yours.")


@blueprint.route('/users/invite', methods=['POST'])
@use_args({
    'accountId': fields.Int(),
    'email': fields.Str(required=True, validate=is_not_blank),
    'roleId': fields.Int()
})
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN, Role.ACCOUNT_ADMIN])
def invite_user(args):
    """
    api for invite user
    :param args:
    :return:
    """
    account_id = args.get("accountId")
    role_id = args.get("roleId")

    roles = Role.get_by_names([Role.ACCOUNT_USER, Role.CHROME_USER])
    role_ids = [role.id for role in roles]
    if role_id not in set(role_ids):
        raise exceptions.Forbidden("You can't assign this role to user!")

    account = param_validator.check_account_id(account_id)

    email = args.get("email")

    role_id = args.get("roleId")
    if not role_id:
        role_id = Role.get_by_name(Role.ACCOUNT_USER)

    # check the permission
    check_target_account(g.user, account)
    check_target_email(g.user, email)

    user = User.get_user_by_email(email)

    if user is not None:
        if user.account_id == account_id:
            raise exceptions.BadRequest(
                "This user is already in your account.")
        # this is just a invitation record, make no change to the existed user

        UserInvitation.create_invitation(account, email, role_id)

        code_value = {"userId": user.id, "accountId": account_id}
        code = Code.create_code(
            generate_hash_str(email), json.dumps(code_value), Code.INVITATION_CODE)

        send_confirm_email_for_invite(email, user.password, code.code)
    else:
        password = generate_random_pw(10)
        # add a trial user
        user = _register_user(email, password)
        UserInvitation.create_invitation(account, email, role_id)

        code_value = {"userId": user.id, "accountId": account_id}
        code = Code.create_code(
            generate_hash_str(email), json.dumps(code_value), Code.INVITATION_CODE)

        send_confirm_email_for_invite(email, password, code.code)

    return jsonify({"status": "OK"})


@blueprint.route('/users/confirm_invitation', methods=['GET'])
@use_args({
    'code': fields.Str(required=True, validate=is_not_blank),
    'redirect_url': fields.Str(required=True, validate=is_not_blank),
    'es_app': fields.Str(required=False)
})
def confirm_invitation(args):
    """
    call back for invitation email
    :param args:
    :return:
    """
    confirm_code = args.get("code")
    redirect_url = args.get("redirect_url")
    es_app = args.get("es_app")

    code = Code.get_by_code(confirm_code)
    if code is None:
        raise exceptions.BadRequest("Confirmation email link invalid.")

    user_info = json.loads(code.value)
    account = param_validator.check_account_id(user_info.get("accountId"))

    user_id = user_info.get("userId")
    user = User.get_by_id(user_id)
    old_account = user.account

    account.add_user(user)

    if not old_account.users.all():
        old_account.mark_to_deleted()

    if es_app and es_app == "chrome_extension":
        send_chrome_admin_confirm_email(account.id, user.first_name, user.last_name, user.email)
    else:
        # force to refresh login
        login_user(user, remember_me=False)

    return make_response(redirect(redirect_url))


@blueprint.route('/users/<int:user_id>/roles', methods=['PUT'])
@use_args({
    "roleNames": fields.List(fields.Str)
})
@allow([Role.ES_SUPER_ADMIN])
def assign_roles_to_user(args, user_id):
    user = param_validator.check_account_id(user_id)

    role_names = args.get("roleNames")
    roles = Role.get_by_names(role_names)

    UserRole.reset_role_for_user(user, roles)
    return jsonify({"status": "OK"})


@blueprint.route('/roles', methods=['GET'])
@allow([Role.ES_ADMIN, Role.ES_SUPER_ADMIN, Role.ACCOUNT_ADMIN])
def get_all_roles():
    role_names = [Role.ACCOUNT_ADMIN, Role.ACCOUNT_USER, Role.CHROME_USER]

    if g.user.is_es_admin():
        role_names.append(Role.ES_ADMIN)
        role_names.append(Role.TRIAL_USER)
    if g.user.is_super_admin():
        role_names.append(Role.ES_SUPER_ADMIN)

    roles = Role.get_by_names(role_names)

    return jsonify(data=[role.serialized for role in roles])


@blueprint.route('/all_role_names', methods=['GET'])
@allow([Role.ES_ADMIN, Role.ES_SUPER_ADMIN, Role.ACCOUNT_ADMIN])
def get_role_names():
    all_roles = Role.get_all()
    return jsonify(data=[role.serialized for role in all_roles])


@blueprint.route('/users/ghost', methods=['PUT'])
@allow([Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
@use_kwargs({
    "accountId": fields.Int()
})
def ghost(accountId):
    account = check_account_id(accountId)

    users = account.active_users
    # Ghosting happens on an account-level basis, but we don't have a g.account,
    # we just use a g.user. So we try to find an account admin to ghost as.
    # If we can't find one, we just ghost as the last user we iterated over.
    for user in users:
        if user.is_account_admin():
            session['ghostUserId'] = user.id
            break
    else:
        session['ghostUserId'] = user.id

    return jsonify(
        status="OK"
    )


@blueprint.route('/users/ghost/fallback', methods=['DELETE'])
@allow([Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def fallback_ghost():
    session.pop("ghostUserId", "")

    return jsonify(
        status="OK"
    )


@blueprint.route('/users/chrome/register', methods=['POST'])
@use_kwargs({
    'firstName': fields.Str(required=True),
    'lastName': fields.Str(required=True),
    'company': fields.Str(required=True),
    'email': fields.Str(required=True),
    'password': fields.Str(required=True)
})
def register_chrome_user(firstName, lastName, company, email, password):
    try:
        domain = _parse_email_domain(email)
        user = User.get_active_user_by_email(email)
        if user:
            return jsonify(message="User already exist"), httplib.CONFLICT

        account = Account.get_primary_account_by_domain(domain)
        if not account or not account.is_paying:
            UserRegisterLog.create_register_log(firstName, lastName, company, email)
            return jsonify(message="Thanks for showing interest, Someone from EverString will contact you shortly"), \
                httplib.EXPECTATION_FAILED

        user = User.create_user(account.id, email, password, User.INACTIVE, firstName, lastName, company=company)
        role = Role.get_by_name(Role.CHROME_USER)
        UserRole.create(user, role)
        UserInvitation.create_invitation(account, email, role.id)
        code_value = {"userId": user.id, "accountId": account.id}
        code = Code.create_code(
            generate_hash_str(email), json.dumps(code_value), Code.INVITATION_CODE)
        send_chrome_confirm_email(email, invite_code=code.code)
    except:
        logger.exception("Register User Exception:")
        return jsonify(message="Oops, something went wrong. Please contact support "
                               "if this problem persists."), httplib.BAD_REQUEST

    return jsonify(
        data=user.serialized
    )


def send_chrome_confirm_email(email, invite_code):
    template_name = "chrome_register_user_confirm.html"
    redirect_url = current_app.config.get("NS_STAR_APP_HOST") + "/#/chrome-confirm"
    confirm_link = get_confirm_link(url_for("auth.confirm_invitation"), invite_code, redirect_url, "chrome_extension")
    NSEmailClient.send_email(
        email,
        "Welcome to EverString! Please Confirm Your Email",
        render_template(template_name, confirm_link=confirm_link)
    )


def send_chrome_admin_confirm_email(account_id, first_name, last_name, email):
    account_admins = User.get_account_admin_by_account_id(account_id)
    if not account_admins:
        return
    account_admin_emails = [user.email for user in account_admins]
    template_name = "chrome_register_admin_confirm.html"
    redirect_url = current_app.config.get("NS_STAR_APP_HOST") + "/#/user/login"
    NSEmailClient.send_email(
        account_admin_emails,
        "New Chrome User Alert",
        render_template(template_name,
                        first_name=first_name,
                        last_name=last_name,
                        email=email,
                        confirm_link=redirect_url
                        )
    )


def _parse_email_domain(email):
    email_parts = email.split('@')
    if len(email_parts) == 2:
        ext = tldextract.extract(email_parts[1] or '')
        domain = ext.registered_domain
    return domain
