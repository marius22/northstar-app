import httplib
import json

from flask import Blueprint, jsonify, g, render_template, request, current_app

from ns.controller.app.permission import allow, deny
from ns.model.account import Account
from ns.model.user_segment_sfdc_publisher import SFDCIntegrationError, SFDCTokenInvalidError
from ns.util.webargs_parser import UTCNaiveDateTimeField
from sqlalchemy.orm import joinedload
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs
from werkzeug import exceptions
from werkzeug.exceptions import NotFound, BadRequest, Forbidden
from ns.model.user import User
from ns.util.string_util import is_not_blank
from ns.util.email_client import NSEmailClient
from ns.model.user_role import UserRole
from ns.model.role import Role
from ns.model.user_invitation import UserInvitation
from ns.controller.app import param_validator
from ns.model.recommendation import Recommendation
from ns.controller.app.param_validator import check_account_id
from ns.model.seed_csv_reader import SeedCSVReader
import datetime
import logging
from ns.util.webargs_parser import OrderField, DictField
from ns.util.webargs_parser import NullableUTCNaiveDateTimeField
from ns.model.sf_pull_request import SFPullRequest
from ns.model.seed_company import SeedCompany
from ns.model.model_seed import ModelSeed
from ns.model.model import Model, NotEnoughDomainsError
from ns.model.account_quota import AccountQuota, InvalidAccountQuotaError
from ns.model.crm_account import CrmAccount
from ns.model.crm_lead import CrmLead
from ns.model.company_insight import CompanyInsight
from ns.model.publish_field_mapping import PublishFieldMapping, InvalidOrgId
from ns.model.account_quota_ledger import AccountQuotaLedger
from ns.util.auth_util import get_effective_user

from ns.constants.default_field_mapping_constants import TARGET_TYPE_MARKETO, TARGET_TYPE_MARKETO_SANDBOX
from ns.constants.insight_constants import InsightConstants

blueprint = Blueprint('user', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/users', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def get_users():
    """
    Get all users for the ES super admin.

    TODO:
        1. [ ] `count` -> fast_count, https://gist.github.com/hest/8798884
        2. [ ] pagination
        3. [x] eager loading to speed up account attribute access,
            http://goo.gl/civqWI

    Returns
    -------

    """
    users = User.query
    serialized = [user.serialized for user in users.options(joinedload('account')).all()]

    return jsonify(data=serialized, total=users.count())


@blueprint.route('/users/<int:user_id>', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def get_user(user_id):
    user = User.query.get_or_404(user_id)
    return jsonify(data=user.serialized)


@blueprint.route('/users/<int:user_id>', methods=['PUT'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN, Role.ACCOUNT_ADMIN])
@use_args({
    'user': fields.Nested({
        'email': fields.Email(),
        'firstName': fields.Str(),
        'lastName': fields.Str(),
        'phone': fields.Str(),
        'status': fields.Str(),
        'contactExportFlag': fields.Boolean(),
        "roleNames": fields.List(fields.Str),
        'accountId': fields.Int()
    }, required=True)
})
def update_user_info(args, user_id):

    user = param_validator.check_user_id(user_id)

    if not g.user.is_es_admin() \
            and g.user.account_id != user.account_id:
        raise exceptions.Forbidden("You cannot modify this user.")

    user_dict = args['user']

    role_names = user_dict.get("roleNames")
    if role_names:
        if not g.user.can_grant_role(role_names):
            raise exceptions.Forbidden("Access Denied")
        roles = Role.get_by_names(role_names)
        UserRole.reset_role_for_user(user, roles)

    user.update(user_dict)

    return jsonify(data=user.serialized), httplib.ACCEPTED


@blueprint.route('/invitations/<int:invitation_id>', methods=['PUT'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN, Role.ACCOUNT_ADMIN])
@use_args({
    'invitation': fields.Nested({
        'email': fields.Email(),
        'firstName': fields.Str(),
        'lastName': fields.Str(),
        'phone': fields.Str(),
        'status': fields.Str(),
        "roleNames": fields.List(fields.Str)
    }, required=True)
})
def update_invitation_info(args, invitation_id):

    user_invitation = UserInvitation.get_by_id(invitation_id)
    if user_invitation is None:
        raise exceptions.BadRequest("Invalid user ID.")

    if not g.user.is_es_admin() \
            and g.user.account_id != user_invitation.account_id:
        raise exceptions.Forbidden("You cannot modify this user.")

    invitation_arg = args['invitation']
    user_invitation.update(invitation_arg)

    if invitation_arg['status'] == User.ACTIVE:
        original_user = User.get_by_id(user_invitation.user_id)
        old_account = original_user.account

        account = Account.get_by_id(user_invitation.account_id)
        account.add_user(original_user)

        role_names = invitation_arg.get("roleNames")
        if role_names:
            if not g.user.can_grant_role(role_names):
                raise exceptions.Forbidden("Access Denied")
            roles = Role.get_by_names(role_names)
            UserRole.reset_role_for_user(original_user, roles)

        if not old_account.users.all():
            old_account.mark_to_deleted()

    return jsonify(data=user_invitation.serialized), httplib.ACCEPTED


@blueprint.route('/accounts', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def get_accounts():
    accounts = Account.query.filter(Account.deleted.is_(False))
    res = [account.serialized for account in accounts]
    return jsonify(data=res)


@blueprint.route('/accounts', methods=['POST'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
@use_args({
    'name': fields.Str(),
    'domain': fields.Str(),
    'isPrimary': fields.Boolean()
})
def create_account(args):
    name = args.get('name')
    domain = args.get('domain')
    is_primary = args.get('isPrimary')
    with_quota = True
    account = Account.create_account(
        name, Account.PAYING, domain, is_primary, with_quota)
    return jsonify(data=account.serialized)


@blueprint.route('/accounts/<int:account_id>', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def get_account(account_id):
    account = Account.query.get_or_404(account_id)
    return jsonify(data=account.serialized)


@blueprint.route('/accounts/<int:account_id>/quotas/adjust', methods=['POST'])
@use_args({
    'value': fields.Int(missing=0),
    'description': fields.Str(required=True)
})
@allow([Role.ES_SUPER_ADMIN])
def adjust_account_quota(args, account_id):

    adjustment_val = args.get('value', 0)
    desc = args.get('description', None)

    if not adjustment_val:
        return jsonify(data="Nothing to adjust. Adjustment value is 0 or None.")

    account = Account.query.get_or_404(account_id)

    if not account:
        raise exceptions.BadRequest("Could not get Account details for given account id: %s" % account_id)

    quota = account.account_quota

    if not quota:
        raise exceptions.BadRequest("Could not get Account Quota for given account id: %s" % account_id)

    if quota.expired():
        raise exceptions.BadRequest("Account is expired. Can not adjust publish quota.")

    # the value must be less than equal to  (maxLimit - publishedQuota) for that contract period
    if adjustment_val > (quota.csv_company_limit - quota.remaining_csv_company_cnt):
        raise exceptions.BadRequest(
            "Adjustment value must be less than the difference between quota limit and published quota.")

    try:
        if g is not None and g.user is not None:
            AccountQuotaLedger.create_entry(account_id=account.id, user_id=g.user.id,
                                            op_code=AccountQuotaLedger.ADJUST_REMAINING_QUOTA,
                                            operation_ts=datetime.datetime.utcnow(),
                                            value=adjustment_val,
                                            description=desc if desc else 'Adjustment for account: %s' % account.id)
        else:
            raise exceptions.BadRequest("User could not be determined / user not logged in.")

    except Exception as err:
        raise err

    res = account.serialized
    return jsonify(data=res)


@blueprint.route('/accounts/<int:account_id>', methods=['PUT'])
@use_args({
    'account': fields.Nested({
        'name': fields.Str(validate=is_not_blank),
        'accountType': fields.Str(validate=lambda t: t in Account.account_type_enums),
        'domain': fields.Str()
    }, required=True),
    'quotas': fields.Nested({
        'segmentCnt': fields.Int(),
        'UICompanyLimit': fields.Int(),
        'CSVCompanyLimit': fields.Int(),
        'contactLimit': fields.Int(),
        'exposedUIInsights': fields.DelimitedList(fields.Str()),
        'exposedCSVInsights': fields.DelimitedList(fields.Str()),
        'startTs': UTCNaiveDateTimeField(),
        'expiredTs': UTCNaiveDateTimeField(),
        'enableAds': fields.Boolean(),
        'enableApis': fields.Boolean(),
        'realTimeScoringInsights': fields.DelimitedList(fields.Str()),
        'enableInCrm': fields.Boolean(),
        'enableNotInCrm': fields.Boolean(),
        'enableFindCompany': fields.Boolean(),
        'enableScoreList': fields.Boolean(),
        'enableModelInsights': fields.Boolean(),
        'inboundLeadsTarget': fields.DelimitedList(fields.Str(
            validate=lambda x: x in [AccountQuota.INBOUND_TARGET_ACCOUNTS, AccountQuota.INBOUND_TARGET_LEADS,
                                     AccountQuota.INBOUND_TARGET_CONTACTS]),
            required=True),
        'realtimeScoring': fields.Str(
            validate=lambda x: x in [Account.REALTIME_SCORING_NONE,
                                     Account.REALTIME_SCORING_GLOBAL,
                                     Account.REALTIME_SCORING_AUDIENCE])
    }, required=False)
})
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def update_account(args, account_id):
    account = Account.query.get_or_404(account_id)
    account_arg = args['account']
    if account_arg:
        account.update(account_arg)
    quota = args.get('quotas')
    if quota:
        try:
            account.account_quota.update(quota)
        except InvalidAccountQuotaError as err:
            raise exceptions.BadRequest(err.message)
        account.update_real_time_scoring_object(
            quota.get("inboundLeadsTarget"))
        account.update({"realtimeScoring": Account.REALTIME_SCORING_MAPPING.get(
            quota.get("realtimeScoring"))})

    result = account.serialized
    result['quota'] = account.account_quota.serialized
    return jsonify(data=result), httplib.ACCEPTED


@blueprint.route('/accounts/<int:account_id>/quotas', methods=['GET'])
@use_args({
    'includes_usage': fields.Bool(missing=False)
})
def get_account_quota(args, account_id):
    account = Account.query.get_or_404(account_id)
    quota = account.account_quota

    includes_usage = args['includes_usage']
    res = quota._serialized(includes_usage)
    res['inboundLeadsTarget'] = AccountQuota.get_real_time_scoring_object(account)
    res['realtimeScoring'] = account.realtime_scoring_mode

    return jsonify(data=res)


@blueprint.route('/accounts/<int:account_id>/crm_sync_summary', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
def get_account_crm_sync_summary(account_id):
    account = Account.query.get_or_404(account_id)
    org_id = account.sfdc_org_id

    result = {}
    lead_com_count = CrmAccount.count_companies_by_org_id(org_id)
    result["countInLead"] = lead_com_count

    account_com_count = CrmLead.count_companies_by_org_id(org_id)
    result["countInAccount"] = account_com_count

    result["countInCrm"] = lead_com_count + account_com_count

    return jsonify(
        data=result
    )


@blueprint.route('/accounts/<int:account_id>/quotas', methods=['PUT'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN])
@use_args({
    'quotas': fields.Nested({
        'segmentCnt': fields.Int(),
        'UICompanyLimit': fields.Int(),
        'CSVCompanyLimit': fields.Int(),
        'contactLimit': fields.Int(),
        'exposedUIInsights': fields.DelimitedList(fields.Str()),
        'exposedCSVInsights': fields.DelimitedList(fields.Str()),
        'startTs': UTCNaiveDateTimeField(missing='1970-01-01 00:00:01'),
        'expiredTs': UTCNaiveDateTimeField(),
        'realTimeScoringInsights': fields.DelimitedList(fields.Str(), required=True),
        'enableInCrm': fields.Boolean(required=True),
        'enableNotInCrm': fields.Boolean(required=True),
        'inboundLeadsTarget': fields.DelimitedList(fields.Str(
            validate=lambda x: x in [AccountQuota.INBOUND_TARGET_ACCOUNTS, AccountQuota.INBOUND_TARGET_LEADS,
                                     AccountQuota.INBOUND_TARGET_CONTACTS]),
            required=True),
        'realtimeScoring': fields.Str(
            validate=lambda x: x in [Account.REALTIME_SCORING_NONE,
                                     Account.REALTIME_SCORING_GLOBAL,
                                     Account.REALTIME_SCORING_AUDIENCE]),
    }, required=True)
})
def update_account_quota(args, account_id):
    account = Account.query.get_or_404(account_id)
    quotas_param = args['quotas']

    quota = account.account_quota
    quota.update(quotas_param)

    account.update_real_time_scoring_object(
        quotas_param.get("inboundLeadsTarget"))
    account.update({"realtimeScoring": Account.REALTIME_SCORING_MAPPING.get(
        quotas_param.get("realtimeScoring"))})

    return jsonify(data=quota.serialized)


@blueprint.route('/accounts/<int:account_id>/scoring', methods=['PUT'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN, Role.ACCOUNT_ADMIN])
@use_args({
    'inboundLeadsTarget': fields.DelimitedList(fields.Str(
        validate=lambda x: x in [AccountQuota.INBOUND_TARGET_ACCOUNTS, AccountQuota.INBOUND_TARGET_LEADS,
                                 AccountQuota.INBOUND_TARGET_CONTACTS]),
        required=True),
    'realtimeScoring': fields.Str(validate=lambda x: x in [Account.REALTIME_SCORING_NONE,
                                                           Account.REALTIME_SCORING_GLOBAL,
                                                           Account.REALTIME_SCORING_AUDIENCE])
})
def update_scoring_settings(args, account_id):
    account = Account.query.get_or_404(account_id)
    account.update_real_time_scoring_object(args.get("inboundLeadsTarget"))
    account.update({"realtimeScoring": Account.REALTIME_SCORING_MAPPING.get(args.get("realtimeScoring"))})
    updated_to = {
        'inboundLeadsTarget': AccountQuota.get_real_time_scoring_object(account),
        'realtimeScoring': account.realtime_scoring_mode
    }
    return jsonify(data=updated_to)


def send_email_to_user(user, case_number):
    NSEmailClient.send_email(
        user.email,
        "EverString Customer Service Acknowledgment Case Number %s" % case_number,
        render_template(
            "request_confirm_customer.html",
            case_number=case_number
        )
    )


def send_email_to_admins(user, case_number):
    # query all super admins
    role = Role.get_by_name(Role.ES_SUPER_ADMIN)
    admins = UserRole.get_users_by_role(role)

    NSEmailClient.send_email(
        [admin.email for admin in admins],
        "EverString Customer Service Acknowledgment Case Number %s" % case_number,
        render_template(
            "request_confirm_admin.html",
            case_number=case_number,
            name=' '.join([user.first_name, user.last_name]),
            phone=user.phone,
            email=user.email
        )
    )


@blueprint.route('/users/me', methods=['GET'])
def current_user():
    user = g.user
    if user is None:
        raise exceptions.Unauthorized("Please log in.")

    user_dict = user.serialized_with_quota
    user_dict['ghostUser'] = g.ghost_user.serialized_with_quota \
        if hasattr(g, 'ghost_user') else None

    old_version_user_time = current_app.config.get(
        "TIME_TO_ALLOW_USE_PREVIOUS_VERSION")
    if user.is_es_admin() or user.created_ts < old_version_user_time:
        user_dict["canUsePreviousVersion"] = True
    else:
        user_dict["canUsePreviousVersion"] = False

    return jsonify(data=user_dict)


@blueprint.route('/users/request_upgrade', methods=['POST'])
@allow([Role.TRIAL_USER])
@use_args({
    "firstName": fields.Str(required=True, validate=is_not_blank),
    "lastName": fields.Str(required=True, validate=is_not_blank),
    "phone": fields.Str(required=True, validate=is_not_blank)
})
def request_upgrade(args):
    """
    upgrade request
    :param args:
    :return:
    """
    first_name = args["firstName"]
    last_name = args["lastName"]
    phone = args["phone"]

    user = g.user
    if not user.activated:
        raise exceptions.Forbidden("Please confirm your email first.")

    user.update_name_and_phone(first_name, last_name, phone)

    case_number = str(10000 + user.id)

    send_email_to_user(user, case_number)
    send_email_to_admins(user, case_number)
    return jsonify({"status": "OK"})


@blueprint.route('/users/request_more', methods=['POST'])
@allow([Role.ACCOUNT_ADMIN])
def request_more():
    """
    upgrade more
    """

    user = g.user
    if not user.activated:
        raise exceptions.Forbidden("Please confirm your email first.")

    case_number = str(10000 + user.id)

    send_email_to_user(user, case_number)
    send_email_to_admins(user, case_number)
    return jsonify({"status": "OK"})


@blueprint.route('/accounts/<int:account_id>/users', methods=['GET'])
@use_args({
    'includePending': fields.Boolean(),
})
def account_users(args, account_id):
    # check permission
    if not g.user.is_es_admin() \
            and g.user.account_id != account_id:
        raise exceptions.Forbidden("You cannot manage users of this account.")

    result = []
    result.extend(User.get_users_for_account(account_id))
    if args.get("includePending"):
        result.extend(UserInvitation.get_by_account(account_id))
    return jsonify(dict(data=[user.serialized for user in result]))


@blueprint.route('/accounts/<int:account_id>/user_invitations', methods=['GET'])
@allow([Role.ES_SUPER_ADMIN, Role.ES_ADMIN, Role.ACCOUNT_ADMIN])
def user_invitations(account_id):
    # check permission
    if not g.user.is_es_admin() \
            and g.user.account_id != account_id:
        raise exceptions.Forbidden("You cannot manage users of this account.")

    result = UserInvitation.get_by_account(account_id)
    return jsonify(
        data=[user.serialized for user in result]
    )


# This API endpoint is accessible to all roles because it's required
# for audience publish.
@blueprint.route('/accounts/quota_fields', methods=['GET'])
def get_quota_fields():
    supported_fields = CompanyInsight.get_supported_publish_field_for_display()
    return jsonify(data=supported_fields)


@blueprint.route('/users/<int:user_id>/sfdc/managed_package_status', methods=['GET'])
def get_sfdc_managed_package_status(user_id):
    if not g.user:
        raise exceptions.Forbidden()
    if not g.user.is_es_admin() and not g.user.id == user_id:
        raise exceptions.Forbidden()

    user = User.query.get_or_404(user_id)
    sfdc_token = user.sfdc_token

    try:
        if not sfdc_token:
            raise SFDCTokenInvalidError()
        sfdc_token.fetch_managed_package_status()
        return jsonify(status='OK')
    except SFDCIntegrationError as e:
        return (
            jsonify(message=e.message, code=e.error_code),
            httplib.UNPROCESSABLE_ENTITY)


def _check_account_operation_permission(user, account):
    if not user.is_es_admin() and user.account_id != account.id:
        raise exceptions.Forbidden("Access Denied")
    return


@blueprint.route('/accounts/<int:account_id>/seeds', methods=['POST'])
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def upload_account_seeds_csv(account_id):

    # every time customer upload a csv will create a new account fit model
    account = check_account_id(account_id)
    user = get_effective_user()
    user_id = user.id
    _check_account_operation_permission(g.user, account)

    csv_file = request.files['file']
    min_rows = current_app.config.get('MIN_SEED_CSV_ROW_COUNTS', 50)
    try:
        # write csv file to local and parse domains
        file_name = 'seed_candidates_account_%d_%s.csv' \
                    % (account_id, datetime.datetime.utcnow().isoformat())
        file_path = SeedCSVReader.write_seed_csv_to_local(file_name, csv_file)

        seed_companies = SeedCSVReader.get_seed_companies_from_file_path(file_path)
        num_unique_companies = len(seed_companies)
        if num_unique_companies < min_rows:
            raise NotEnoughDomainsError()

    except NotEnoughDomainsError:
        raise BadRequest({'domains': [c.domain for c in seed_companies],
                          'desc': 'Not enough domains found.'})

    account.batch_create_models(
        seed_companies,
        [Model(type=Model.ACCOUNT_FIT, name="CRM Fit"),
         Model(type=Model.FEATURE_ANALYSIS, name="account_%s_%s" % (Model.FEATURE_ANALYSIS, account.id))],
        owner_id=user_id
    )

    return jsonify(
        status="OK"
    )


def process_date_condition(start_date, end_date):
    if end_date is None:
        end_date = datetime.datetime.utcnow()
    if start_date is None:
        start_date = end_date + datetime.timedelta(days=-310)
    return start_date, end_date


def make_sfdc_seed_result(sf_pull_request):
    result = {"status": "EMPTY"}
    if sf_pull_request:
        result["startDate"] = sf_pull_request.start_ts
        result["endDate"] = sf_pull_request.end_ts
        result["status"] = sf_pull_request.status
        if sf_pull_request.status == SFPullRequest.COMPLETED:
            # fill overview
            user = get_effective_user()
            account = user.account
            sf_seeds = account.fit_model_sfdc_seeds

            matches = sum([1 for seed in sf_seeds
                           if not seed.domain.startswith(SeedCompany.UNKNOWN_DOMAIN_PREFIX)])
            total = len(sf_seeds)
            result["overview"] = {"matches": matches, "unknown": total - matches}
    return result


@blueprint.route('/accounts/<int:account_id>/seeds/sfdc', methods=['POST'])
@use_args({
    "startDate": NullableUTCNaiveDateTimeField(),
    "endDate": NullableUTCNaiveDateTimeField()
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def pull_sfdc_seed_for_account(args, account_id):

    user = get_effective_user()
    account = user.account

    check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    start_date, end_date = \
        process_date_condition(args.get("startDate"), args.get("endDate"))

    sf_pull_request = SFPullRequest.save_account_request(
        user, account, start_date, end_date)

    account.upload_sf_seeds(user, start_date, end_date)

    sf_pull_request.complete()

    return jsonify(make_sfdc_seed_result(sf_pull_request))


def get_criterion(order):
    criterion = None
    if order:
        field = SeedCompany.get_field_by_key(order.get('key'))
        if field is None:
            return None
        criterion = order.get('direction')(field)

    return criterion


@blueprint.route('/accounts/<int:account_id>/seeds/sfdc/overview', methods=['GET'])
@use_args({
    'limit': fields.Int(),
    'offset': fields.Int(),
    'order': OrderField(),
    'type': fields.Str(missing="unknown")
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def account_sfdc_seeds_overview(args, account_id):
    limit = args.get("limit")
    offset = args.get("offset")

    account = g.user.account
    model = account.latest_crm_fit_model
    assert model, "Invalid model"

    total, paging = SeedCompany.query_seed_domains(
        model_seed_id=model.model_seed_id,
        limit=limit,
        offset=offset,
        criterion=get_criterion(args.get('order')),
        type=args.get('type')
    )

    return jsonify({
        "total": total,
        "data": [recd.serialized for recd in paging],
        "offset": offset,
        "limit": limit
    })


@blueprint.route('/accounts/<int:account_id>/submit_fit_job', methods=['POST'])
@use_kwargs({
    "type": fields.Str(missing="CRM", validate=lambda x: x in ["CRM", "CSV"])
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def submit_account_fit_job(account_id, type):

    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    # a account may have many account fit models
    # we need a rule to choose one to run
    model = account.latest_crm_fit_model
    if not model:
        raise exceptions.BadRequest("No model to run")

    for model in Model.get_same_batch_models(
            model.batch_id,
            [Model.FEATURE_ANALYSIS, Model.ACCOUNT_FIT]):

        model.submit_model_run()

    return jsonify(
        status="OK"
    )


@blueprint.route('/account/<int:account_id>/field/mapping', methods=['GET'])
@use_kwargs({
    "targetType": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_type_enums),
    "targetObject": fields.Str(required=False, validate=lambda x: x in PublishFieldMapping.target_object_enums)
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def get_account_publish_field_mappings(account_id, targetType, targetObject):
    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    target_org_id = PublishFieldMapping.get_target_org_id(account_id, targetType)

    results = PublishFieldMapping.get_publish_field_mapping(account_id, targetType, target_org_id, targetObject)
    return jsonify(data=results)


@blueprint.route('/account/<int:account_id>/field/mapping', methods=['POST'])
@use_kwargs({
    "targetType": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_type_enums),
    "targetObject": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_object_enums),
    "targetFields": fields.List(
        fields.Nested({
            "targetField": fields.Str(required=True),
            "insightId": fields.Int(),
            "modelId": fields.Int()
        }), required=True)
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def upsert_account_publish_field_mappings(account_id, targetType, targetObject, targetFields):
    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    try:
        # get unique id based on type and account
        target_org_id = PublishFieldMapping.get_target_org_id(
            account_id, targetType)
    except InvalidOrgId as ex:
        raise exceptions.BadRequest(ex.message)

    for field in targetFields:
        target_field = field.get("targetField")
        insight_id = field.get("insightId")
        model_id = field.get("modelId")

        try:
            # verify that ids are valid and exist in db
            PublishFieldMapping.verify_ids(account_id, insight_id, model_id)
        except AssertionError as ex:
            raise exceptions.BadRequest(ex.message)

        PublishFieldMapping.upsert(
            account_id=account_id,
            insight_id=insight_id,
            model_id=model_id,
            target_type=targetType,
            target_object=targetObject,
            target_org_id=target_org_id,
            target_field=target_field
        )

    # return all current mappings for given targetType and targetObject
    results = PublishFieldMapping.get_publish_field_mapping(account_id, targetType, target_org_id, targetObject)
    return jsonify(data=results)


@blueprint.route('/account/<int:account_id>/field/mapping', methods=['DELETE'])
@use_kwargs({
    "targetType": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_type_enums),
    "targetObject": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_object_enums),
    "targetFields": fields.List(
        fields.Nested({
            "targetField": fields.Str(required=True),
            "insightId": fields.Int(),
            "modelId": fields.Int()
        }), required=True)
})
@allow([Role.ACCOUNT_ADMIN, Role.ES_ADMIN, Role.ES_SUPER_ADMIN])
def delete_account_publish_field_mappings(account_id, targetType, targetObject, targetFields):
    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    try:
        # get unique id based on type and account
        target_org_id = PublishFieldMapping.get_target_org_id(
            account_id, targetType)
    except InvalidOrgId as ex:
        raise exceptions.BadRequest(ex.message)

    for field in targetFields:
        target_field = field.get("targetField")
        insight_id = field.get("insightId")
        model_id = field.get("modelId")

        try:
            # verify that ids are valid and exist in db
            PublishFieldMapping.verify_ids(account_id, insight_id, model_id)
        except AssertionError as ex:
            raise exceptions.BadRequest(ex.message)

        PublishFieldMapping.delete(
            account_id=account_id,
            insight_id=insight_id,
            model_id=model_id,
            target_type=targetType,
            target_object=targetObject,
            target_org_id=target_org_id,
            target_field=target_field
        )

    # return all current mappings for given targetType and targetObject
    results = PublishFieldMapping.get_publish_field_mapping(account_id, targetType, target_org_id, targetObject)
    return jsonify(data=results)


@blueprint.route('/account/<int:account_id>/supported_publish_fields', methods=['GET'])
@use_kwargs({
    "targetType": fields.Str(required=True, validate=lambda x: x in PublishFieldMapping.target_type_enums),
})
def get_supported_publish_fields_for_account(account_id, targetType):
    from ns.constants import default_field_mapping_constants
    account = check_account_id(account_id)
    _check_account_operation_permission(g.user, account)

    supported_insights = CompanyInsight.get_supported_publish_fields(target_type=targetType)

    allowed_insight_ids = AccountQuota.get_by_account_id(account_id).allowed_insight_ids
    insights = filter(
        lambda ins: ins.id in allowed_insight_ids, supported_insights)

    fit_models = []
    # if scoring is not turned on, do not return model fields
    if AccountQuota.get_by_account_id(account_id).scoring_enabled:
        # get all fit models and the most recent account fit model (CRM FIT)
        fit_models = Model.get_by_account_id(
            account_id, [Model.FIT_MODEL], [Model.COMPLETED])
        account_fit_model = Model.get_latest_completed_model(
            account_id, Model.ACCOUNT_FIT)
        if account_fit_model:
            fit_models.append(account_fit_model)

    insights = [ins.serialized for ins in insights]
    insights = sorted(insights, key=lambda record: record.get("displayName").lower())
    models = [m.serialized for m in fit_models]
    models = sorted(models, key=lambda record: record.get("name").lower())
    return jsonify(
        data={
            "insights": insights,
            "models": models,
            "defaultMappings": default_field_mapping_constants.DEFAULT_MAPPING
        }
    )
