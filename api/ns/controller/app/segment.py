"""
Controller for the segment related endpoints
"""
import httplib
import ujson
import uuid
from flask import Blueprint, jsonify, request, g, current_app, url_for
import logging
import datetime
import time

import ns.model.salesforce as SALESFORCE

from ns.model.s3client_default import S3ClientDefault
from ns.model.account import Account
from ns.model.account_quota import QuotaExceedError
from ns.model.expansion import Expansion
from ns.model.seed_csv_reader import RowCountsExceedsLimitError
from ns.model.segment_metric import SegmentMetric
from ns.model.tmp_account_segment import TmpAccountSegment
from ns.model.tmp_segment_stage import TmpSegmentStage
from ns.model.user_download import UserDownload
from ns.util.company_db_client import CompanyDBClient
from ns.util.queue_service_client import QueueServiceClient
from ns.util.webargs_parser import OrderField, DictField
from ns.controller.app.permission import deny, \
    has_segment_access, segment_completed_required
from sqlalchemy import func, case
from webargs import fields
from webargs.flaskparser import use_args, parser
from werkzeug import exceptions
from werkzeug.exceptions import NotFound, BadRequest, Forbidden
from ns.model.role import Role
from ns.model.segment import Segment
from ns.model.segment import SegmentError
from ns.model.recommendation import Recommendation
from ns.model.user import User
from ns.model.sf_pull_request import SFPullRequest
from ns.model.source import Source
from ns.util.string_util import is_not_blank
from ns.model.seed_candidate import SeedCandidate
from ns.util.webargs_parser import NullableUTCNaiveDateTimeField
from ns.controller.app.permission import check_change_segment_owner_permission
from ns.model.csv_download_impl import SeedListLoader
from ns.model.user_exported_company import UserExportedCompany
from ns.model.qualification import Qualification
from ns.model.company_dispute import CompanyDispute
from ns.model.crm_token import CRMToken
from ns.util.queries_filter_builder import InBuilder, OrBuilder

from ns.util.csv_suite import write_csv_data_to_response, write_csv_file_to_response
from ns.util.sql_util import literalquery


blueprint = Blueprint('segment', __name__)
logger = logging.getLogger(__name__)


def check_contact_export_permission(user, contact_limit_per_company):
    if not user.can_export_contacts() and contact_limit_per_company > 0:
        raise exceptions.Forbidden("You are not allowed to export contacts")


def send_publish_message(args):
    """Send publish message with dedupe informationto Kafka"""
    against = []
    if args.get('compareAgainstAccounts'):
        against.append(SALESFORCE.ACCOUNT_CONTACT)

    if args.get('compareAgainstLeads'):
        against.append(SALESFORCE.LEAD)
    publish_type = args.get('publishType')

    publish_contacts = args.get('contactLimitPerCompany') > 0

    recommendation_query, total = g.segment.generate_sql_query(
        g.user, args, need_total_cnt=True)
    if args.get('publishNew'):
        publish_account_to = [args['new'].get('publishCompaniesTo')]
        net_new_info_dict = dict(
            enable=True,
            only_publish_account_has_contact=args['new']['publishOnlyIfContactFound'],
            against=against,
            publish_account_to=publish_account_to,
        )
        if publish_contacts:
            publish_leads_to = []
            if 'publishLeadsTo' in args['new']:
                publish_leads_to = [args['new']['publishLeadsTo']]
            net_new_info_dict.update(dict(
                publish_contact_to=publish_leads_to
            ))
        else:
            net_new_info_dict.update(dict(
                publish_contact_to={}
            ))

    else:
        net_new_info_dict = dict(
            enable=False
        )

    if args.get('publishExisting'):
        # Comparing against the account/contact combo means we want to publish
        # to account if something is found
        publish_account_to = []
        for obj in against:
            if obj == SALESFORCE.ACCOUNT_CONTACT:
                publish_account_to.append(SALESFORCE.ACCOUNT)
            else:
                publish_account_to.append(obj)

        existing_info_dict = dict(
            enable=True,
            only_publish_account_has_contact=args['existing']['publishOnlyIfContactFound'],
            against=against,
            # Whatever we compare against is what we publish to.
            publish_account_to=publish_account_to,
        )

        if publish_contacts:
            if args.get('compareAgainstLeads'):
                found_in_lead = [SALESFORCE.LEAD]
            else:
                found_in_lead = []
            existing_info_dict.update(dict(
                publish_contact_to=dict(
                    found_in_account_lead=args.get('existing', {}).get('foundInAccountsAndLeads', []),
                    found_in_account=args.get('existing', {}).get('foundInAccount', []),
                    found_in_lead=found_in_lead
                )
            ))
        else:
            existing_info_dict.update(dict(
                publish_contact_to={}
            ))
    else:
        existing_info_dict = dict(
            enable=False
        )

    # companyId's are needed for us to calculate the quota deduction once execute the publish
    required_publish_fields = ['company_id']
    if publish_type == 'SFDC':
        user_exposed_fields = [
            'revenue',
            'employee_size',
            'industry',
            'state',
            'zipcode'
        ]
    else:
        user_exposed_fields = g.user.account.account_quota.exposed_csv_insight_column_names
    publish_fields = list(set(required_publish_fields) | set(user_exposed_fields))

    token = CRMToken.get_sfdc_token_by_user_id(g.user.id)
    crm_token = CRMToken._get_by_user_crm_type(g.user.id, token.crm_type)
    org_id = crm_token.organization_id
    message = dict(
        recommendation_query=literalquery(recommendation_query).replace("\n", ""),
        total_publish_account_num=args.get('numCompanies'),
        contact_limit_per_account=args.get('contactLimitPerCompany'),
        publish_fields=publish_fields,
        user_id=g.user.id,
        user_email=g.user.email,
        account_id=g.user.account.id,
        titles=args.get('titles', []),
        publish_type=publish_type,
        org_id=org_id,
        segment_id=g.segment.id,
        segment_name=g.segment.name,
        net_new=net_new_info_dict,
        existing=existing_info_dict
    )
    import pprint
    pprint.pprint(message, indent=2, width=120)
    start_time = time.time()

    status_code = QueueServiceClient.instance().send_export_message_with_dedupe(message)
    current_time = time.time()
    logger.info("Submit export job time spent: {}s".format(current_time - start_time))

    if status_code == httplib.OK:
        return jsonify(status='OK'), httplib.CREATED
    else:
        raise exceptions.InternalServerError("Send export with dedupe message failed")


dedupe_params_validation_dict = {
    # Job titles we're interested in for contacts.
    'titles': fields.List(fields.Str, missing=[]),
    'contactLimitPerCompany': fields.Int(),
    'offset': fields.Int(),
    'order': OrderField(),
    'where': DictField(),
    'onlyExported': fields.Boolean(),
    # Number of companies we want to publish for this publish with dedupe.
    'numCompanies': fields.Int(),
    # If true, we will de-dupe against accounts.
    'compareAgainstAccounts': fields.Boolean(),
    # If true, we will de-dupe against leads.
    'compareAgainstLeads': fields.Boolean(),
    'publishType': fields.Str(validate=lambda publishType: publishType in [
        'CSV',
        'SFDC'
    ]),
    'publishNew': fields.Boolean(),
    'publishExisting': fields.Boolean(),
    'dedupeAgainstPreviousPublish': fields.Boolean(),
    'new': fields.Nested({
        # If true, we only publish an account if we can find contact information
        # for it
        'publishOnlyIfContactFound': fields.Boolean(),
        'publishCompaniesTo': fields.Str(validate=lambda dest: dest in [
            SALESFORCE.ACCOUNT,
            SALESFORCE.LEAD,
        ]),
        'publishLeadsTo': fields.Str(validate=lambda dest: dest in [
            SALESFORCE.LEAD,
            SALESFORCE.CONTACT,
        ]),
    }),
    'existing': fields.Nested({
        'publishOnlyIfContactFound': fields.Boolean(),
        'foundInAccount': fields.List(fields.Str(validate=lambda sf_obj: sf_obj in [
            SALESFORCE.LEAD,
            SALESFORCE.CONTACT
        ])),
        'foundInAccountsAndLeads': fields.List(fields.Str(
            validate=lambda sf_obj: sf_obj in [
                SALESFORCE.LEAD,
                SALESFORCE.CONTACT
            ])
        )
    })
}


publish_csv_params = {
    'limit': fields.Int(),
    'offset': fields.Int(),
    'format': fields.Str(missing='json', validate=lambda fmt: fmt in ['json', 'email']),
    'order': OrderField(),
    'where': DictField(),
    # Whether to dedupe against previous publish
    'dedupe': fields.Boolean(),
    'onlyExported': fields.Boolean(),
}
publish_csv_params.update(dedupe_params_validation_dict)


@blueprint.route('/segments/<int:segment_id>/companies', methods=['GET', 'POST'])
@use_args(publish_csv_params)
@has_segment_access
@segment_completed_required
def get_segment_companies(args, segment_id):

    format = args['format']

    if format == 'json':
        serialized, total = g.segment.get_page_result(g.user, args, True, False)
        result = remove_unaccessible_columns(g.user, serialized)
        enrich_exported_ts(result)
        enrich_disputes(result)

        # When showing the fit scores in the company list table, we need these values
        # to be numeric in order to sort by the fit score column appropriately.
        for rec in result:
            if rec['score']:
                rec['score'] = float(rec['score'])
        return jsonify(data=result, total=total)

    else:
        check_contact_export_permission(g.user, args.get("contactLimitPerCompany"))
        g.segment.check_exported_csv_permission(g.user)

        message = prepare_csv_export_message(g.user, g.segment,
                                             args, '.get_segment_companies')
        status_code = QueueServiceClient.instance().send_csv_export_message(message)

        if status_code == httplib.OK:
            return jsonify(status='OK'), httplib.OK
        else:
            raise exceptions.InternalServerError("Send export csv message failed")


def prepare_csv_export_message(user, segment, args, endpoint):
    titles = args.get('titles')
    limit_per_company = int(args.get('contactLimitPerCompany', 0))

    job = segment.latest_completed_job

    expansion_dict = {}
    if job.expansion_id:
        expansion = Expansion.query.get(job.expansion_id)
        if expansion:
            expansion_dict = ujson.loads(expansion.expansion_val)

    user = user
    account = segment.owner.account

    args = args.copy()
    args['format'] = 'csv'
    args['userId'] = user.id

    if 'order' in args:
        args['order'] = args['order'].get('original_value')
    if 'sectors' in args:
        args['sectors'] = ','.join(args['sectors'])
    if 'where' in args:
        args['where'] = ujson.dumps(args['where'])

    csv_url = url_for(endpoint,
                      segment_id=segment.id,
                      **args)

    message = {'user_id': user.id,
               'user_email': user.email,
               'account_id': account.id,
               # Num companies the user intends to publish.
               'num_companies': args['limit'],
               'segment_id': segment.id,
               'segment_name': segment.name,
               'titles': titles,
               'departments': expansion_dict.get('departments', []),
               'contact_limit_per_company': limit_per_company,
               'csv_url': csv_url}
    return message


def upload_to_s3(company_csv_name, company_csv):
    s3client = S3ClientDefault.get_instance()
    env = current_app.config.get('CURRENT_ENV')
    expired_time = current_app.config.get('S3_EXPIRED_TIME')
    company_csv_s3_key = env + '/' + uuid.uuid1().hex
    url = s3client.put_csv(company_csv_name, company_csv_s3_key, company_csv, expired_time)
    return company_csv_s3_key, url


def enrich_exported_ts(result):
    company_ids = [item.get("companyId") for item in result]

    exports = UserExportedCompany.get_by_segment_and_company_ids(g.segment.id, company_ids)
    mapping = dict([(item.company_id, item.created_ts) for item in exports])

    for item in result:
        export_ts = mapping.get(item.get("companyId"))
        item["exportedTs"] = export_ts.isoformat() if export_ts else None


def enrich_disputes(result):
    company_ids = [item.get("companyId") for item in result]

    disputes = CompanyDispute.get_by_segment_company_ids(g.segment.id, company_ids)
    mapping = get_dispute_mapping(disputes)

    for item in result:
        field_disputes = mapping.get(item.get("companyId"))
        if field_disputes:
            item["disputes"] = [dispute.serialized for dispute in field_disputes]
        else:
            item["disputes"] = []


def remove_unaccessible_columns(user, companies):
    if user.is_es_admin():
        return companies
    quota = user.account.account_quota
    accessible_columns = quota.exposed_ui_insights
    result = []
    for company in companies:
        result.append(dict([(item, company.get(item)) for item in accessible_columns]))
    return result


def get_dispute_mapping(disputes):
    result = {}
    for item in disputes:
        result.setdefault(item.company_id, []).append(item)
    return result


def generate_contact_csv(account_id, company_ids, titles, limit_per_company, contact_csv_name, env):
    client = CompanyDBClient.instance()
    contacts_file = client.get_contacts_csv_info(account_id, company_ids, titles,
                                                 limit_per_company, contact_csv_name, env)
    contact_csv_s3_key = contacts_file.get('key')
    contact_csv_s3_url = contacts_file.get('url')
    return contact_csv_s3_key, contact_csv_s3_url


def get_distinct_criterion(order):
    accept_keys = ['count', 'value']
    if not order:
        criterion = None
    elif order['key'] not in accept_keys:
        raise BadRequest('unknown order key: %s, only accept %s' % (order['key'], accept_keys))
    else:
        criterion = order['direction'](order['key'])
    return criterion


def _tech_metric_to_api_format(techs_metrics):

    result = []
    for tech in techs_metrics:
        for k, v in tech.items():
            result.append({"value": k, "count": v})
    return result


@blueprint.route('/segments/<int:segment_id>/companies/distinct_counts', methods=['GET'])
@use_args({
    'limit': fields.Int(),
    'keys': fields.DelimitedList(fields.Str(), required=True),
    'order': OrderField(),
    'with_others': fields.Boolean(missing=False)
})
@has_segment_access
@segment_completed_required
def get_companies_distinct_counts(args, segment_id):
    job = g.segment.latest_completed_job
    criterion = get_distinct_criterion(args.get('order'))

    keys = args['keys']
    limit = args.get('limit')
    with_others = args.get('with_others')
    results = dict()
    companies_cnt = g.segment.recommendation_num
    for key in keys:
        if key == 'tech':
            techs = g.segment.get_tech_distinct_count(limit, str(criterion).endswith("ASC"))
            distinct_counts = _tech_metric_to_api_format(techs)

        else:
            distinct_counts = job.recommendation_distinct_counts_by_field(companies_cnt, key, order=criterion,
                                                                          limit=limit, with_others=with_others)

        results[key] = distinct_counts

    return jsonify(data=results)


@blueprint.route('/segments/<int:segment_id>/companies/overview')
@has_segment_access
@segment_completed_required
def get_companies_overview(segment_id):
    segment = g.segment

    all_counts = segment.all_similar_counts

    return jsonify(
        inBoundaryCnt=segment.in_boundary_companies_cnt,
        companiesCnt=segment.recommendation_num,
        bizSimilarCnt=all_counts["biz"],
        techSimilarCnt=all_counts["tech"],
        depSimilarCnt=all_counts["department"],
        seedsCnt=segment.seed_candidates_cnt
    )


@blueprint.route('/seeds/dimension', methods=['GET'])
def seed_dimension():
    return jsonify(
        data=[
            "tech",
            "growth",
        ]
    )


@blueprint.route('/recommendations/dimension', methods=['GET'])
def recommendations_dimension():
    return jsonify(
        data=[
            "tech",
            "growth",
        ]
    )


@blueprint.route('/segments/<int:segment_id>/seeds/distinct_counts', methods=['GET'])
@use_args({
    'limit': fields.Int(),
    'keys': fields.DelimitedList(fields.Str(), required=True),
    'order': OrderField()
})
@has_segment_access
def get_seed_list_distinct_counts(args, segment_id):
    criterion = get_distinct_criterion(args.get('order'))

    keys = args['keys']
    results = dict()
    seeds_cnt = g.segment.seed_candidates_cnt
    for key in keys:
        distinct_counts = g.segment.seeds_distinct_counts_by_field(seeds_cnt, key,
                                                                   order=criterion, limit=args.get('limit'))
        results[key] = distinct_counts

    return jsonify(data=results)


@blueprint.route('/segments/<int:segment_id>/seed_attributes', methods=['GET'])
@use_args({
    'keys': fields.DelimitedList(fields.Str(), required=True)
})
@has_segment_access
def get_segment_seed_attributes(args, segment_id):

    keys = args['keys']
    results = dict()
    for key in keys:
        results[key] = g.segment.get_seed_attribute_values(key)

    # Sort employee size
    if 'employeeSize' in results:
        employee_size_bucket = current_app.config.get("EMPLOYEE_SIZE_BUCKETS")
        employee_size_list = [employee_size['label'] for employee_size in employee_size_bucket
                              if employee_size['label'] in results.get('employeeSize')]
        results['employeeSize'] = employee_size_list
    if 'tech' in results:
        technologies = set()
        for tech in results.get('tech'):
            for t in ujson.loads(tech):
                technologies.add(t)
        techs = list(technologies)
        techs.sort()
        results['tech'] = techs

    return jsonify(data=results)


@blueprint.route('/segments/universe_attributes', methods=['GET'])
@use_args({
    'keys': fields.DelimitedList(fields.Str(), required=True)
})
def get_segment_universe_attributes(args):
    keys = args['keys']
    results = dict()
    for key in keys:
        results[key] = []

    if 'employeeSize' in results:
        employee_size_list = current_app.config.get("EMPLOYEE_SIZE_BUCKETS")
        results['employeeSize'] = [employee_size['label'] for employee_size in employee_size_list]
    if 'revenue' in results:
        revenue_list = current_app.config.get('REVENUE_BUCKETS')
        results['revenue'] = [revenue['label'] for revenue in revenue_list]
    if 'state' in results:
        states = CompanyDBClient.instance().get_us_states()
        results['state'] = states
    if 'industry' in results:
        industries = CompanyDBClient.instance().get_industries()
        results['industry'] = industries
    if 'tech' in results:
        techs = CompanyDBClient.instance().get_techs()
        results['tech'] = techs
    if 'department' in results:
        departments = current_app.config.get('DEPARTMENT')
        department_lst = [department for department in departments]
        results['department'] = department_lst
    if 'growth' in results:
        results['growth'] = current_app.config.get('GROWTH')

    return jsonify(data=results)


@blueprint.route('/segments/<int:segment_id>/expansion', methods=['GET'])
@has_segment_access
def get_segment_expansion(segment_id):
    expansion = g.segment.expansion
    res = ujson.loads(expansion.expansion_val) if expansion else {}
    return jsonify(data=res)


@blueprint.route('/segments/<int:segment_id>/qualification', methods=['GET'])
@has_segment_access
def get_segment_qualification(segment_id):
    qualification = g.segment.qualification
    res = ujson.loads(qualification.qualification_val) if qualification else {}
    return jsonify(data=res)


@blueprint.route('/segments/<int:segment_id>/expansion', methods=['PUT'])
@has_segment_access
def update_segment_expansion(segment_id):
    data = request.get_json()
    expansion = g.segment.expansion
    expansion_dict = ujson.loads(expansion.expansion_val) if expansion else {}
    if data:
        expansion_input = data.get('expansion')
        if 'tech' in expansion_input:
            expansion_dict['tech'] = expansion_input['tech']
        if 'department' in expansion_input:
            expansion_dict['department'] = expansion_input['department']

    if expansion:
        expansion_val = ujson.dumps(expansion_dict)
        expansion.update_expansion_val(expansion_val)
    else:
        expansion_dict['seniority'] = current_app.config.get('SENIORITY')
        expansion_val = ujson.dumps(expansion_dict)
        Expansion.create(segment_id, expansion_val)
    if g.segment.status == Segment.EMPTY:
        g.segment.update_status(Segment.DRAFT)

    return jsonify(status="OK")


@blueprint.route('/segments/<int:segment_id>/qualification', methods=['PUT'])
@has_segment_access
def update_segment_qualification(segment_id):
    qualification = g.segment.qualification
    qualification_dict = ujson.loads(qualification.qualification_val) if qualification else {}

    data = request.get_json()
    if not data:
        raise exceptions.BadRequest("Invalid post data")
    qualification_input = data.get('qualification')
    if 'employeeSize' in qualification_input:
        qualification_dict['employeeSize'] = qualification_input['employeeSize']
    if 'state' in qualification_input:
        qualification_dict['state'] = qualification_input['state']
    if 'industry' in qualification_input:
        qualification_dict['industry'] = qualification_input['industry']

    qualification_val = ujson.dumps(qualification_dict)
    if qualification:
        qualification.update_qualification_val(qualification_val)
    else:
        Qualification.create(segment_id, qualification_val)
    if g.segment.status == Segment.EMPTY:
        g.segment.update_status(Segment.DRAFT)

    return jsonify(status="OK")


@blueprint.route('/segments/<int:segment_id>/stage', methods=['PUT'])
@has_segment_access
def update_segment_stage(segment_id):
    data = request.get_json()
    if not data:
        raise exceptions.BadRequest("Invalid post data")
    stage = data.get('stage')
    TmpSegmentStage.create_or_update(segment_id, stage)
    return jsonify(status="OK")


@blueprint.route('/segments/<int:segment_id>/publish', methods=['POST'])
@deny([Role.TRIAL_USER])
@has_segment_access
def publish_segment(segment_id):
    if g.segment.status != Segment.COMPLETED:
        raise BadRequest(
            "You can only publish a completed Predictive Segment. " +
            "Please try again once the model run is completed."
        )
    g.segment.publish()

    return jsonify(data=to_json(g.segment))


@blueprint.route('/segments/<int:segment_id>/unpublish', methods=['POST'])
@deny([Role.TRIAL_USER])
@has_segment_access
def unpublish_segment(segment_id):
    try:
        g.segment.unpublish()
    except SegmentError as e:
        raise BadRequest(e.message)

    return jsonify(data=to_json(g.segment))


@blueprint.route('/segments/<int:segment_id>/seeds/csv', methods=['POST'])
@has_segment_access
def upload_csv(segment_id):
    """
    upload csv file of seed list
    :param segment_id:
    :return:
    """
    # Flask handles the file missing errors
    csv_file = request.files['file']

    if not g.segment:
        raise NotFound('Segment(ID = %d) is not found' % segment_id)

    if g.segment.csv_overwrite_allowed():
        try:
            g.segment.attach_csv(csv_file)

            new_seed_candidates_count = g.segment.seed_candidates.count()
            return jsonify(total=new_seed_candidates_count), httplib.CREATED
        except RowCountsExceedsLimitError, e:
            # `RowCountsExceedsLimitError` is a business exception,
            # convert it to a HTTP exception on the API layer
            args = e.args[0]
            raise BadRequest("Please make sure your CSV doesn't have more than %d rows."
                             % (args['max_rows']))
    else:
        raise Forbidden('Segment is no longer in draft, so you can\'t upload a new seed list.')


@blueprint.route('/segments', methods=['POST'])
@use_args({
    'segmentName': fields.Str(required=True),
    'accountId': fields.Int()
}, locations=('json',))
def create_segment(args):
    """
    Segment creation API
    request body: {"segmentName": "Everstring"}
    :return: {"segmentId": 1}
    """
    segment_name = args['segmentName']
    account_id = args.get('accountId')
    try:
        segment = Segment.create_or_replace_segment(g.user, segment_name, account_id)
        return jsonify(data={"segmentId": segment.id})
    except QuotaExceedError, e:
        raise exceptions.Forbidden(e.message)


@blueprint.route('/segments', methods=['GET'])
@use_args({
    'accountId': fields.Int()
})
def get_segments(args):
    can_create_segments = False
    if g.user.is_es_admin():
        visible_segments = []
        #  no limitation for es admin
        can_create_segments = True
        account_id = args.get('accountId')
        if account_id:
            account = Account.query.get(account_id)
            if account and not account.deleted:
                visible_segments = account.owned_segments.all()
                tmp_segments = Segment.get_tmp_account_segments(account_id)
                visible_segments.extend(tmp_segments)
        else:
            visible_segments = Segment.get_all_segments(g.user.id)
    elif g.user.is_account_admin():
        # account admin can see all segments of this account
        account = g.user.account
        visible_segments = account.owned_segments
        can_create_segments = account.segment_cnt_quota_not_reached
    else:
        # Only display owned_segments that this user has permission to view
        visible_segments = Segment.get_all_segments(g.user.id)
        can_create_segments = g.user.account.segment_cnt_quota_not_reached

    segments = sorted(visible_segments, key=lambda x: x.created_ts, reverse=True)
    segments_json = [to_json(segment) for segment in segments]

    # Only allow user to publish his segment if there are people
    # to publish it to.
    num_users_in_account = len(User.get_users_for_account(g.user.account_id))
    can_publish = num_users_in_account > 1

    return jsonify(
        data=segments_json,
        canPublish=can_publish,
        canCreateSegments=can_create_segments
    )


@blueprint.route('/segments/<int:segment_id>', methods=['GET'])
@has_segment_access
def get_segment(segment_id):
    segment_dict = g.segment.to_dict()
    if g.segment.status == Segment.DRAFT:
        segment_stage = TmpSegmentStage.get_by_segment(segment_id)
        if segment_stage:
            segment_dict["segmentStage"] = segment_stage.stage

    try:
        g.segment.check_exported_csv_permission(g.user)
        segment_dict["canExport"] = True
    except:
        segment_dict["canExport"] = False

    return jsonify(data=segment_dict)


@blueprint.route('/segments/<int:segment_id>', methods=['DELETE'])
@has_segment_access
def delete_segment(segment_id):
    if g.segment.owner_id != g.user.id:
        raise exceptions.Forbidden("Segment can only be deleted by owner.")
    g.segment.delete()
    return jsonify(status="OK")


@blueprint.route('/segments/<int:segment_id>/model_run', methods=['POST'])
@has_segment_access
def model_run(segment_id):
    if not g.segment.is_in_draft():
        raise exceptions.Forbidden("Segment not in draft.")

    # check the status of user
    if not g.segment.owner.activated:
        raise exceptions.Forbidden("Please confirm your email first.")

    g.segment.submit_job()
    return jsonify(data=g.segment.to_dict()), httplib.ACCEPTED


@blueprint.route('/segments/<int:segment_id>/seed_candidates', methods=['GET'])
@use_args({
    'format': fields.Str(missing='json', validate=lambda fmt: fmt in ['json', 'csv']),
    'limit': fields.Int(),
    'offset': fields.Int(),
})
@has_segment_access
def get_seed_candidates(args, segment_id):
    seed_candidates = g.segment.seed_candidates

    page_result = seed_candidates.limit(args.get('limit')).offset(args.get('offset'))

    serialized = [sc.serialized for sc in page_result]
    if args.get("format") == "json":
        return jsonify(data=serialized, total=g.segment.seed_candidates_cnt)
    else:
        filename = "%s.csv" % g.segment.name
        loader = SeedListLoader(g.segment, 2000)
        return write_csv_data_to_response(filename, loader)


@blueprint.route('/segments/<int:segment_id>/has_seeds', methods=['GET'])
@has_segment_access
def if_segment_has_seeds(segment_id):
    segment = g.segment
    return jsonify(
        data=segment.seed_candidates_cnt > 0
    )


@blueprint.route('/segments/<int:segment_id>/seed_candidates/insights', methods=['GET'])
@has_segment_access
def get_seed_candidate_insights(segment_id):
    insights = g.segment.seed_candidate_insights()
    return jsonify(insights)


advanced_export_params = {
    'sectors': fields.List(
        fields.Str(),
        validate=lambda x: set(x).issubset(['1', '2', '3', '4', '5', '6', '7'])),
    'format': fields.Str(validate=lambda fmt: fmt in ['csv', 'email']),

    # Whether to dedupe against previous publish in the advanced export case.
    'dedupe': fields.Boolean(missing=False),
    # limit param is only for advanced publish.
    'limit': fields.Int(fields.Str(), required=False),
}

sfdc_params = {}
sfdc_params.update(advanced_export_params)
sfdc_params.update(dedupe_params_validation_dict)


@blueprint.route('/segments/<int:segment_id>/publish_with_dedupe', methods=['POST'])
@use_args(sfdc_params)
@has_segment_access
def publish_segment_to_sfdc(args, segment_id):
    g.segment.check_exported_csv_permission(g.user)
    check_contact_export_permission(g.user, args.get("contactLimitPerCompany"))

    # The present of 'sectors' means we're doing an advanced export. In the advanced export case,
    # we produce a Kafka message that passes a csv_url to the consumer, which uses it download a
    # csv of what needs to be published to Salesforce.
    #
    # For the July 2016 release, there is no de-dupe functionality here.
    if 'sectors' in args:
        endpoint = '.export_csv_by_sectors'
        company_ids = g.segment.get_companies_by_sectors(args, g.user, True)
        # check remaining quota
        g.segment.check_export_quota(g.user, company_ids)

        # Build the reference recommendation_url
        # for background job to download the
        # corresponding CSV.
        args = request.args.copy()
        args['format'] = 'csv'
        args['userId'] = g.user.id

        csv_url = url_for(
            endpoint,

            # additional parameters
            segment_id=segment_id,
            # default parameters
            **args)

        g.segment.publish_to_sfdc_by_user(
            g.user, csv_url)
        # In the basic export case, we do perform dedupe.
    else:
        send_publish_message(args)

    return jsonify(status='OK'), httplib.CREATED


def to_json(segment):
    # Takes a segment and generates a dictionary that is suitable
    # to pass to the client as a json object.
    segment_dict = segment.to_dict()

    is_owner = g.user.id == segment.owner_id
    owner = User.query.get(segment.owner_id)
    owner_name = owner.full_name

    segment_dict.update({
        'ownerName': owner_name,
        'isOwner': is_owner,
        'isShared': segment.is_shared(),
        'isESSegment': owner.is_es_admin()
    })

    return segment_dict


def make_sfdc_seed_result(sf_pull_request):
    result = {"status": "EMPTY"}
    if sf_pull_request:
        result["startDate"] = sf_pull_request.start_ts
        result["endDate"] = sf_pull_request.end_ts
        result["status"] = sf_pull_request.status
        if sf_pull_request.status == SFPullRequest.COMPLETED:
            # fill overview
            segment = g.segment
            sf_seeds = segment.seed_candidates_by_source(
                Source.get_by_name(Source.SALESFORCE)).all()

            matches = sum([1 for seed in sf_seeds if is_not_blank(seed.domain)])
            total = len(sf_seeds)
            result["overview"] = {"matches": matches, "unknown": total - matches}
    return result


@blueprint.route('/segments/<int:segment_id>/seeds/sfdc', methods=['GET'])
@has_segment_access
def get_sfdc_seed_status(segment_id):
    user = g.user
    segment = g.segment
    sf_pull_request = SFPullRequest.get_by_user_segment(user, segment)

    return jsonify(make_sfdc_seed_result(sf_pull_request))


def process_date_condition(start_date, end_date):
    if end_date is None:
        end_date = datetime.datetime.utcnow()
    if start_date is None:
        start_date = end_date + datetime.timedelta(days=-31)
    return start_date, end_date


@blueprint.route('/segments/<int:segment_id>/seeds/sfdc', methods=['POST'])
@use_args({
    "startDate": NullableUTCNaiveDateTimeField(),
    "endDate": NullableUTCNaiveDateTimeField()
})
@has_segment_access
def pull_sfdc_seed(args, segment_id):
    if not g.segment.csv_overwrite_allowed():
        raise Forbidden('Segment is no longer in draft, so you can\'t upload a new seed list.')

    start_date, end_date = process_date_condition(args.get("startDate"), args.get("endDate"))

    user = g.user
    segment = g.segment

    # save SF data pull request.
    sf_pull_request = SFPullRequest.save_segment_request(user, segment, start_date, end_date)

    segment.upload_sf_seeds(user, start_date, end_date)

    sf_pull_request.complete()

    return jsonify(make_sfdc_seed_result(sf_pull_request))


def get_criterion(order):
    criterion = None
    if order:
        field = SeedCandidate.get_field_by_key(order.get('key'))
        if field is None:
            return None
        criterion = order.get('direction')(field)

    return criterion


@blueprint.route('/segments/<int:segment_id>/seeds/sfdc/overview', methods=['GET'])
@use_args({
    'limit': fields.Int(),
    'offset': fields.Int(),
    'order': OrderField(),
    'type': fields.Str()
})
@has_segment_access
def sfdc_seeds_overview(args, segment_id):
    source = Source.get_by_name(Source.SALESFORCE)
    limit = args.get("limit")
    offset = args.get("offset")

    total, paging = SeedCandidate.query_seed_candidates(
        g.segment.id,
        source.id,
        limit=limit,
        offset=offset,
        criterion=get_criterion(args.get('order')),
        type=args.get('type')
    )

    return jsonify({
        "total": total,
        "data": [recd.serialized for recd in paging],
        "offset": offset,
        "limit": limit
    })


def check_new_owner(current_user, new_owner):
    if current_user.is_es_admin():
        return
    if current_user.account_id != new_owner.account_id:
        raise Forbidden("User [%s] does not belong to your account." % new_owner.email)


def check_account_quota(target_account):
    if target_account.segment_cnt_quota_not_reached:
        return
    raise exceptions.Forbidden("Insufficient segment quota.")


@blueprint.route('/segments/<int:segment_id>/change_owner', methods=['PUT'])
@deny([Role.TRIAL_USER])
@use_args({
    "userId": fields.Int()
})
def change_owner(args, segment_id):
    user_id = args.get("userId")
    user = User.get_by_id(user_id)
    if user is None:
        raise BadRequest("Invalid user ID.")

    segment = Segment.get_by_id(segment_id)
    if segment is None:
        raise BadRequest("Invalid segment ID.")

    # check permission
    check_change_segment_owner_permission(g.user, segment)
    check_new_owner(g.user, user)

    # if segment's old owner and new owner are in same account,
    # don't need to check the account quota.
    if segment.owner.account_id != user.account_id:
        check_account_quota(user.account)

    # change owner for segment
    segment.change_owner(user)

    # delete record from TmpAccountSegment
    if g.user.is_es_admin() and not user.is_es_admin():
        TmpAccountSegment.delete_by_account_segment(user.account_id, segment.id)

    return jsonify(status='OK')


@blueprint.route('/segments/<int:segment_id>/sector_company_cnt', methods=['GET'])
@has_segment_access
def get_company_count_by_sector(segment_id):
    sector_metric = SegmentMetric.get_by_type(segment_id, SegmentMetric.METRIC_SECTOR)
    recommendations = g.segment.latest_recommendations
    res = dict()
    """
    The venn chart from UI is comprised of three dimensions:
        similar companies (C),
        similar technologies (T),
        similar departments (D)

    The chart has 7 sectors:
        1 - Only have similar companies, no similar technologies or departments
        2 - Only have similar technologies, no similar companies or departments
        3 - Only have similar departments, no similar companies or technologies
        4 - Have similar companies and technologies, but no similar departments
        5 - Have similar companies and departments, but no similar technologies
        6 - Have similar technologies and departments, but no similar companies
        7 - Have similar companies, technologies and departments
    """
    cnt_query_res = recommendations.with_entities(
        case([(Recommendation.similar_c_score == 4, 4), ], else_=0).label('c_score'),
        case([(Recommendation.similar_t_score == 4, 4), ], else_=0).label('t_score'),
        case([(Recommendation.similar_d_score == 4, 4), ], else_=0).label('d_score'),
        func.count(Recommendation.company_id).label('count')).\
        join(UserExportedCompany,
             Recommendation.company_id == UserExportedCompany.company_id).\
        filter(UserExportedCompany.segment_id == segment_id).\
        group_by('c_score', 't_score', 'd_score')

    company_cnt_deduped = dict()
    for cnt in cnt_query_res:
        if cnt.c_score == 0 and cnt.t_score == 0 and cnt.d_score == 0:
            company_cnt_deduped[7] = cnt.count
        elif cnt.c_score == 0 and cnt.t_score == 0 and cnt.d_score == 4:
            company_cnt_deduped[4] = cnt.count
        elif cnt.c_score == 0 and cnt.t_score == 4 and cnt.d_score == 0:
            company_cnt_deduped[5] = cnt.count
        elif cnt.c_score == 4 and cnt.t_score == 0 and cnt.d_score == 0:
            company_cnt_deduped[6] = cnt.count
        elif cnt.c_score == 0 and cnt.t_score == 4 and cnt.d_score == 4:
            company_cnt_deduped[1] = cnt.count
        elif cnt.c_score == 4 and cnt.t_score == 0 and cnt.d_score == 4:
            company_cnt_deduped[2] = cnt.count
        elif cnt.c_score == 4 and cnt.t_score == 4 and cnt.d_score == 0:
            company_cnt_deduped[3] = cnt.count

    for i in [1, 2, 3, 4, 5, 6, 7]:
        sector_filter = Recommendation.build_sector_filter(i)
        if sector_metric is None:
            company_cnt = (recommendations.filter(sector_filter)
                           .with_entities(func.count(Recommendation.company_id)).scalar()
                           .with_entities(func.count(Recommendation.id)).scalar())
            res[i] = (company_cnt, company_cnt_deduped.get(i, 0))
        else:
            res[i] = (sector_metric.value[str(i)], company_cnt_deduped.get(i, 0))

    return jsonify(data=res)


@blueprint.route('/segments/<int:segment_id>/sector_companies', methods=['GET'])
@use_args({
    'sectors': fields.DelimitedList(
        fields.Str(), required=True,
        validate=lambda x: set(x).issubset(['1', '2', '3', '4', '5', '6', '7'])),
    'format': fields.Str(missing='email', validate=lambda fmt: fmt in ['csv', 'email']),
    'dedupe': fields.Boolean(missing=False),
    'limit': fields.Int(fields.Str(), required=True),
    'titles': fields.List(fields.Str, missing=[]),
    'contactLimitPerCompany': fields.Int()
})
@has_segment_access
def export_csv_by_sectors(args, segment_id):
    g.segment.check_exported_csv_permission(g.user)

    format = args.get('format')
    if format == 'email':
        check_contact_export_permission(g.user, args.get("contactLimitPerCompany"))

        message = prepare_csv_export_message(g.user, g.segment,
                                             args, '.export_csv_by_sectors')
        import pprint
        print '********'
        print 'message: '
        pprint.pprint(message, indent=2, width=120)
        print '******'
        status_code = QueueServiceClient.instance().send_csv_export_message(message)

        if status_code == httplib.OK:
            return jsonify(status='OK'), httplib.OK
        else:
            raise exceptions.InternalServerError("Send export csv message failed")


@blueprint.route('/segments/<int:segment_id>/disputes', methods=['POST'])
@use_args({
    "companyId": fields.Int(),
    "fieldName": fields.Str(required=True, validate=is_not_blank)
})
@has_segment_access
def segment_dispute(args, segment_id):

    result = CompanyDispute.create_dispute(segment_id, args.get("companyId"), args.get("fieldName"))
    return jsonify(
        data=result.serialized
    )


@blueprint.route('/segments/<int:segment_id>/disputes', methods=['DELETE'])
@use_args({
    "companyId": fields.Int(),
    "fieldName": fields.Str(required=True, validate=is_not_blank)
})
@has_segment_access
def delete_segment_dispute(args, segment_id):

    CompanyDispute.delete_dispute(segment_id, args.get("companyId"), args.get("fieldName"))
    return jsonify(
        status="OK"
    )


@blueprint.route('/accounts/<int:account_id>/datasync_initialized')
def datasync_initialized(account_id):
    account = Account.query.get(account_id)

    return jsonify(
        data=dict(
            initialized=account.crm_data_initialized
        )
    )
