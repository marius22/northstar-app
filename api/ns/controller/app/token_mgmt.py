from flask import Blueprint, request, jsonify, g
from ns.model.api_token import ApiToken
from ns.model.account import Account
from ns.model.account_quota import AccountQuota
from ns.util.auth_util import get_token_str
from werkzeug import exceptions
from webargs import fields
from webargs.flaskparser import use_kwargs
from functools import wraps

import datetime
import logging

blueprint = Blueprint('token_mgmt', __name__)
logger = logging.getLogger(__name__)


@blueprint.route('/token_mgmt/generate_token', methods=['GET'])
def generate_token():
    account = g.user.account
    if not account:
        raise exceptions.BadRequest("Account does not exist")
    api_token = ApiToken.create_api_token(account.id)
    return jsonify(
        token=api_token.token_encoded
    )


@blueprint.route('/token_mgmt/get_token', methods=['GET'])
def get_token():
    account = g.user.account
    if not account:
        raise exceptions.BadRequest("Account does not exist")
    api_token = ApiToken.get_token_by_account_id(account.id)
    return jsonify(
        token=api_token.token_encoded if api_token else None
    )


def validate_token(func):
    """
      This decorator authenticates a token passed to a REST call.
      Token is expected as part of Authorization header in the following format:
      'Authorization: Token <token>'
      A successful validation of the token will result in g.account being set to the account
      the token belongs to
    """
    start = datetime.datetime.utcnow()

    @wraps(func)
    def wrap(*args, **kwargs):
        token_str = get_token_str()
        api_token = ApiToken.validate_and_retrieve_token(token_str)
        if not api_token:
            raise exceptions.Unauthorized("Invalid token")
        quota = AccountQuota.get_by_account_id(api_token.account_id)
        if not quota.apis_enabled:
            raise exceptions.Unauthorized("Not authorized to use APIs")
        g.account = Account.get_by_id(api_token.account_id)
        return func(*args, **kwargs)
    logger.info("Token validation Elapsed time: %s" % (str(datetime.datetime.utcnow() - start)))
    return wrap
