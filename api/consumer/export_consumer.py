import logging
import logging.config
import sys
import ujson

import os
from os import path
sys.path.insert(0, path.dirname(path.dirname(path.realpath(__file__))))

from export_csv import export_csv
from threadpool import ThreadPool
from kafka import KafkaConsumer
from ns.util.dynamic_config_client import get_configuration_object

logging.config.fileConfig("logging.conf")
env = os.environ.get('ENV', 'local')

if env in ['dev', 'uat', 'staging', 'demo', 'prod']:
    config = get_configuration_object(env, "northstar_service")
else:
    from instance import config


thread_num = config.THREAD_NUM
pool = ThreadPool(thread_num)

broker_lst = config.KAFKA_BROKERS.split(',')
consumer = KafkaConsumer(config.KAFKA_TOPIC_EXPORT_CSV,
                         group_id='export_csv_consumer',
                         bootstrap_servers=broker_lst)
try:
    for message in consumer:
        logging.info("kafka message: %s", message.value)
        data = ujson.loads(message.value)
        expected_fields = {'user_id', 'user_email', 'account_id', 'num_companies', 'segment_id', 'segment_name',
                           'titles', 'departments', 'contact_limit_per_company', 'csv_url'}
        if set(data.keys()) != expected_fields:
            logging.warning("Wrong message format: %s" % message.value)

        pool.add_task(export_csv, env, config, data)

finally:
    pool.wait_completion()
