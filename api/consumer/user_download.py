import datetime

from base import Base
from sqlalchemy import Column, Integer, TIMESTAMP, String


class UserDownload(Base):
    __tablename__ = 'user_download'

    id = Column(Integer, primary_key=True, autoincrement=True)
    created_ts = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    updated_ts = Column(TIMESTAMP, default=datetime.datetime.utcnow,
                        onupdate=datetime.datetime.utcnow)

    user_id = Column(Integer, nullable=False)
    segment_id = Column(Integer, nullable=False)
    company_csv_s3_key = Column(String(255), nullable=True)
    contact_csv_s3_key = Column(String(255), nullable=True)

    @staticmethod
    def create(session, user_id, segment_id, company_csv_s3_key, contact_csv_s3_key=None):
        user_download = session.query(UserDownload).\
            filter_by(user_id=user_id, segment_id=segment_id,
                      company_csv_s3_key=company_csv_s3_key,
                      contact_csv_s3_key=contact_csv_s3_key).first()
        if user_download is None:
            user_download = UserDownload(user_id=user_id,
                                         segment_id=segment_id,
                                         company_csv_s3_key=company_csv_s3_key,
                                         contact_csv_s3_key=contact_csv_s3_key)
            session.add(user_download)
            session.commit()

        return user_download
