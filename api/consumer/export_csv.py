import json
import logging
import tempfile

import os
import threading
import uuid

import math
import time

from os import path
import requests
from consumer.db_session import DBSession
from consumer.user_download import UserDownload
import unicodecsv
from tempfile import mkstemp

from ns.util.email_client import EmailClient
from ns.util.s3client import S3Client
import datetime


class S3ClientDefault(object):

    _instance = None

    def __init__(self, config):
        self.client = S3Client(config.AWS_ACCESS_KEY_ID,
                               config.AWS_SECRET_ACCESS_KEY,
                               config.S3_CSV_BUCKET)
        self.expired_time = config.S3_EXPIRED_TIME

    @classmethod
    def instance(cls, config):
        if not cls._instance:
            cls._instance = cls(config)
        return cls._instance

    def upload_to_s3(self, env, csv_name, csv):
        csv_s3_key = env + '/' + uuid.uuid1().hex
        url = self.client.put_csv(csv_name, csv_s3_key, csv, self.expired_time)
        return csv_s3_key, url


class ContactServiceClient(object):
    _instance = None

    def __init__(self, config):
        self.session = requests.Session()
        self.base_url = config.COMPANY_SERVICE_BASE_URL

    def get_contacts(self, account_id, company_ids, titles,
                     departments, limit_per_company):
        # Sometimes company ID's get passed in as a string, but they should be int's.
        company_ids = [int(cid) for cid in company_ids]
        url = '%s/ns_contacts' % self.base_url
        post_data = {'accountId': int(account_id),
                     'companyIdList': company_ids,
                     'titles': titles,
                     'departments': departments,
                     'limitPerCompany': limit_per_company}
        resp = self.session.post(url, json=post_data)
        try:
            data = resp.json()
            contacts = data['data']['contacts']
        except Exception, e:
            logging.error(resp.text)
            raise
        return contacts

    @classmethod
    def instance(cls, config):
        if not cls._instance:
            return cls(config)
        else:
            return cls._instance


class NSServiceClient(object):
    _instance = None

    def __init__(self, config):
        self.session = requests.Session()
        self.base_url = config.NS_SERVICE_BASE_URL

    def get_company_csv(self, s3_url):
        resp = self.session.get(s3_url, stream=True)
        fd, file_path = mkstemp('_company_csv')

        chunk_size = 1024
        for chunk in resp.iter_content(chunk_size):
            os.write(fd, chunk)
        os.close(fd)

        return file_path

    def get_company_csv_s3_info(self, url):
        print "URL:", self.base_url + url
        resp = self.session.get(self.base_url + url)
        try:
            res_dict = resp.json()['data']
            return res_dict['company_csv_s3_key'], res_dict['company_csv_s3_url'], res_dict['is_exceed']
        except Exception, e:
            logging.error(resp.text)
            raise

    def quota_remaining(self, account_id):
        url = '%s/contacts/quota_remaining?account_id=%d' % (self.base_url, account_id)
        resp = self.session.get(url)
        try:
            data = resp.json()['data']
        except Exception, e:
            logging.error(resp.text)
            raise
        return data

    def admin_check(self, user_id):
        url = '%s/contacts/admin_check?user_id=%d' % (self.base_url, user_id)
        resp = self.session.get(url)
        try:
            status = resp.json()['status']
        except Exception, e:
            logging.error(resp.text)
            raise
        return status

    def contacts_check(self, account_id, contact_ids):
        url = '%s/contacts/contacts_check' % self.base_url
        post_data = {'account_id': account_id,
                     'contact_ids': contact_ids}
        resp = self.session.post(url, json=post_data)
        try:
            data = resp.json()['data']

        except Exception, e:
            logging.error(resp.text)
            raise
        return data

    def save_contacts(self, account_id, user_id, contact_ids):
        url = '%s/contacts/contacts_track' % self.base_url
        post_data = {'account_id': account_id, 'user_id':user_id, 'contact_ids': contact_ids}
        resp = self.session.post(url, json=post_data)
        try:
            data = resp.json()['status']

        except Exception, e:
            logging.error(resp.text)
            raise
        return data

    def save_companies(self, account_id, user_id, company_ids):
        url = '%s/companies/companies_track' % self.base_url
        post_data = {'account_id': account_id, 'user_id':user_id, 'company_ids': company_ids}
        resp = self.session.post(url, json=post_data)
        try:
            data = resp.json()['status']

        except Exception, e:
            logging.error(resp.text)
            raise
        return data

    def save_user_companies(self, segment_id, user_id, company_ids):
        url = '%s/companies/user_companies_track' % self.base_url
        post_data = {'segment_id': segment_id, 'user_id': user_id, 'type':['CSV'], 'company_ids': company_ids}
        resp = self.session.post(url, json=post_data)
        try:
            data = resp.json()['status']

        except Exception, e:
            logging.error(resp.text)
            raise
        return data

    @classmethod
    def instance(cls, config):
        if not cls._instance:
            return cls(config)
        else:
            return cls._instance


class ContactCSVWriter(object):

    def __init__(self, csv_file):
        fieldnames = [
            "companyName",
            "companyURL",
            "firstName",
            "lastName",
            "managementLevel",
            "jobTitle",
            "email",
            "directPhone",
            "companyPhone",
            "linkedinURL",
        ]

        self.writer = unicodecsv.DictWriter(csv_file, fieldnames, encoding='utf-8', extrasaction='ignore')
        self.writer.writeheader()

    def write_rows(self, contacts):
        self.writer.writerows(contacts)

    def complete(self):
        self.writer = None


def extract_company_ids(company_csv_path):
    company_ids = []
    with open(company_csv_path, 'rb') as f:
        reader = unicodecsv.DictReader(f)
        if 'companyId' in set(reader.fieldnames):
            for row in reader:
                company_ids.append(row['companyId'])
        else:
            logging.error("Found no 'companyId' in CSV: %s", company_csv_path)

    return company_ids


def delete_csv_file(file_path):
    try:
        os.remove(file_path)
    except:
        logging.error('Delete temp csv file failed')


def export_csv(env, config, data):
    start_time = time.time()
    thread_name = threading.current_thread().name

    user_id = data['user_id']
    user_email = data['user_email']
    segment_id = data['segment_id']
    segment_name = data['segment_name']
    account_id = data['account_id']
    num_companies = data['num_companies']
    contact_limit_per_company = data['contact_limit_per_company']
    csv_url = data['csv_url']

    company_csv_s3_key, company_csv_s3_url, is_exceed = NSServiceClient.instance(config).\
        get_company_csv_s3_info(csv_url)
    # timing company csv generation
    current_time = time.time()
    logging.info("{} - Time spent on company csv generation: {}s".format(thread_name, current_time - start_time))

    contact_csv_s3_key = None
    contact_csv_s3_url = None
    directory = path.dirname(path.dirname(path.realpath(__file__)))
    logging.info("{} - Contact_limit_per_company: {}".format(thread_name, contact_limit_per_company))

    company_ids = generate_company_ids(thread_name, config, company_csv_s3_url)

    contact_client = NSServiceClient.instance(config)
    remaining_contact_quota = contact_client.quota_remaining(account_id)
    is_admin = contact_client.admin_check(user_id)
    email_client = EmailClient(config.EMAIL_CLIENT_FROMADDR, config.EMAIL_CLIENT_USERNAME,
                               config.EMAIL_CLIENT_PASSWORD, config.EMAIL_CLIENT_SMTP_SERVER,
                               config.EMAIL_ENABLED)
    attempts = config.SEND_EMAIL_RETRY
    if contact_limit_per_company > 0 and (remaining_contact_quota >= 0 or is_admin):
        # Generate contact csv
        contact_ids_total_set = set()
        contact_ids_final = 0
        contact_csv_s3_key, contact_csv_s3_url, contact_ids_total_set, contact_ids_final = \
            generate_contact_csv(thread_name, env, config, data, company_csv_s3_url, remaining_contact_quota,
                                 contact_client, contact_ids_total_set, company_ids, is_admin)
        if contact_ids_total_set:
            contact_client.save_contacts(account_id, user_id, list(contact_ids_total_set))
        completed_time = datetime.datetime.now().strftime("%H:%M%p on %b. %d, %Z %Y")
        with open("%s/ns/templates/publish_summary_csv.html" % directory) as f:
            content = f.read()
            content = content.replace('{{company_csv}}', company_csv_s3_url)
            content = content.replace('{{contact_csv}}', contact_csv_s3_url)
            content = content.replace('{{date_time_str}}', completed_time)
            content = content.replace('{{num_selected_companies}}', str(num_companies))
            content = content.replace('{{num_companies_published}}', str(len(company_ids)))
            contact_is_exceed = False
            if remaining_contact_quota - len(contact_ids_total_set) <= 0 and not is_admin:
                contact_is_exceed = True
                content = content.replace('{{contacts_block}}', get_contacts_email_content(
                    contact_limit_per_company, str(contact_ids_final), contact_csv_s3_url))
                content = content.replace('{{quota_block}}', get_quota_email_content(contact_is_exceed, is_exceed))
            else:
                content = content.replace('{{contacts_block}}', get_contacts_email_content(
                    contact_limit_per_company, str(contact_ids_final), contact_csv_s3_url))
                content = content.replace('{{quota_block}}', get_quota_email_content(contact_is_exceed, is_exceed))

    else:
        logging.info("use new template: only company")
        completed_time = datetime.datetime.now().strftime("%H:%M%p on %b. %d, %Z %Y")
        with open("%s/ns/templates/publish_summary_csv.html" % directory) as f:
            content = f.read()
            content = content.replace('{{company_csv}}', company_csv_s3_url)
            content = content.replace('{{date_time_str}}', completed_time)
            content = content.replace('{{num_selected_companies}}', str(num_companies))
            content = content.replace('{{num_companies_published}}', str(len(company_ids)))
            content = content.replace('{{quota_block}}', '<div></div>')
            content = content.replace('{{contacts_block}}', '<div></div>')
            content = content.replace('{{quota_block}}', get_quota_email_content(False, is_exceed))

    # send email
    start_time = time.time()

    subject = "Summary of your publish results completed "

    for i in xrange(attempts):
        try:
            email_client.send_mail(user_email, subject, content, bcc_emails=config.CUSTOMER_SUCCESS_EMAILS)
            break
        except Exception as e:
            logging.error(e.message)
            logging.info("Send email retry %d time" % (i + 1))
            continue

    # deduct the company quota and upsert those company ids into user_export_company table
    contact_client.save_companies(account_id, user_id, company_ids)
    contact_client.save_user_companies(segment_id, user_id, company_ids)
    # Timing sending email
    current_time = time.time()
    logging.info("{} - Time spent on sending email: {}s".format(thread_name, current_time - start_time))
    start_time = current_time

    # tracking download
    session = DBSession.get_db_session(config.SQLALCHEMY_DATABASE_URI)
    UserDownload.create(session, user_id, segment_id, company_csv_s3_key, contact_csv_s3_key)

    # timing update db
    current_time = time.time()
    logging.info("{} - Time spent on updating table user_download: {}s".format(thread_name, current_time - start_time))

    # logging
    logging.info("%s - Company CSV url: %s" % (thread_name, company_csv_s3_url))
    logging.info("%s - Contact CSV url: %s" % (thread_name, contact_csv_s3_url))


def generate_contact_csv(thread_name, env, config, data, company_csv_s3_url, remaining_contact_quota,
                         contact_client, contact_ids_total_set, company_ids, is_admin):
    start_time = time.time()
    account_id = data['account_id']
    segment_name = data['segment_name']
    titles = data['titles']
    departments = data['departments']
    contact_limit_per_company = data['contact_limit_per_company']

    # generate contact csv
    client = ContactServiceClient.instance(config)
    bach_size = config.CONTACT_REQUEST_SIZE
    attempts = config.CONTACT_REQUEST_RETRY
    n = int(math.ceil(len(company_ids) / float(bach_size)))
    start = 0

    contact_csv = tempfile.TemporaryFile()
    writer = ContactCSVWriter(contact_csv)
    need_break = False
    contact_ids_final = 0
    for i in xrange(n):
        if not need_break:
            for j in xrange(attempts):
                if is_admin:
                    try:
                        contacts_batch = client.get_contacts(account_id,
                                                             company_ids[start:start+bach_size],
                                                             titles, departments,
                                                             contact_limit_per_company)
                        writer.write_rows(contacts_batch)
                        start += bach_size
                        contact_ids_final += len(contacts_batch)
                        break
                    except Exception as e:
                        logging.warning(e.message)
                        logging.info("Get contact part %d retry %d time" % (i + 1, j + 1))
                        continue
                else:
                    try:
                        contacts_batch = client.get_contacts(account_id,
                                                             company_ids[start:start+bach_size],
                                                             titles, departments,
                                                             contact_limit_per_company)
                        contact_ids = [contact["id"] for contact in contacts_batch]
                        contact_ids_set = set(contact_ids)
                        quota_duct = contact_client.contacts_check(account_id, list(contact_ids_set))
                        contact_ids_total_set_temp = contact_ids_total_set | set(quota_duct.get('new_contact_ids'))
                        if remaining_contact_quota >= len(contact_ids_total_set_temp):
                            contact_ids_total_set = contact_ids_total_set_temp
                            writer.write_rows(contacts_batch)
                            start += bach_size
                            contact_ids_final += len(contacts_batch)
                            break
                        else:
                            temp = 0
                            limit = remaining_contact_quota - len(contact_ids_total_set)
                            contact_temp = []
                            for index, contact in enumerate(contacts_batch):
                                if contact["id"] in set(quota_duct.get('new_contact_ids')):
                                    temp += 1
                                    if temp <= limit:
                                        contact_ids_total_set.add(contact["id"])
                                    else:
                                        contact_temp.append(contact)
                            contacts_batch = [item for item in contacts_batch if item not in contact_temp]
                            writer.write_rows(contacts_batch)
                            contact_ids_final += len(contacts_batch)
                            start += bach_size
                            need_break = True
                            break
                    except Exception as e:
                        logging.warning(e.message)
                        logging.info("Get contact part %d retry %d time" % (i + 1, j + 1))
                        continue

    # timing contact API call
    current_time = time.time()
    logging.info("{} - Time spent on contact API and generating CSV: {}s".format(thread_name, current_time - start_time))
    start_time = current_time

    contact_csv.seek(0)

    # upload contact csv to s3
    attempts = config.S3_UPLOAD_RETRY
    contact_csv_name = "%s_contact.csv" % segment_name
    for i in xrange(attempts):
        try:
            contact_csv_s3_key, contact_csv_s3_url = S3ClientDefault.instance(config). \
                upload_to_s3(env, contact_csv_name, contact_csv)
            break
        except Exception as e:
            logging.warning(e.message)
            logging.info("Upload to S3 retry %d time" % (i + 1))
            continue

    # timing uploading to s3
    current_time = time.time()
    logging.info("{} - Time spent on uploading to s3: {}s".format(thread_name, current_time - start_time))

    writer.complete()
    contact_csv.close()

    return contact_csv_s3_key, contact_csv_s3_url, contact_ids_total_set, contact_ids_final


def generate_company_ids(thread_name, config, company_csv_s3_url):
    start_time = time.time()

    # download company csv
    company_csv_path = NSServiceClient.instance(config).get_company_csv(company_csv_s3_url)
    logging.info("Download company csv path: %s" % company_csv_path)

    # timing generating company csv
    current_time = time.time()
    logging.info("{} - Time spent on downloading company csv: {}s".format(thread_name, current_time - start_time))
    start_time = current_time

    # extract company ids from company csv
    company_ids = extract_company_ids(company_csv_path)
    delete_csv_file(company_csv_path)
    # timing extract company ids
    current_time = time.time()
    logging.info("{} - Time spent on extract company ids: {}s".format(thread_name, current_time - start_time))
    return company_ids


def get_contacts_email_content(selected_contacts_per_company, num_contacts_published, contact_csv):
    return '<p> <div> Number of contacts per company that you selected to publish: ' \
           '<strong> %s </strong> </div>' \
           '<div> Number of contacts that were published: <strong> %s </strong>' \
           '<a href="%s" style="color:rgb(81, 191, 221); ' \
           'font-weight:400; text-decoration:none">Download</a></div><br></p>' % (selected_contacts_per_company,
                                                                                  num_contacts_published, contact_csv)
           
def get_quota_email_content(contact_is_exceed, is_exceed):
    content = '<div></div>'
    if contact_is_exceed and is_exceed:
        content = 'Your Account and Contact quotas have been exceeded.'
    elif contact_is_exceed and not is_exceed:
        content = 'Your Contact quota has been exceeded.'
    elif not contact_is_exceed and is_exceed:
        content = 'Your Account quota has been exceeded.'
    elif not contact_is_exceed and not is_exceed:
        return content
    return '<div> <strong>Note: %s </strong> ' % content + 'Would you like to raise your quota now?' \
           '<a href="mailto:support@everstring.com?subject=Request%20for%20raising%20quota%20" ' \
           'style="color:rgb(81, 191, 221); margin-left:5px">Contact us.</a></div>'

