from ns.controller.app import app

# This is only used by developers. In production, the server is run from ns/controller/__init__.py
if __name__ == '__main__':
    # EAP Server
    app.run(host='0.0.0.0', port=app.config['PORT'],
            # Without this, the dev server only handles a single request at a time, which is super slow.
            threaded=True)
