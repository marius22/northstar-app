from ns.controller.public_api import public_api_app

# This is only used by developers. In production, the server is run from ns/controller/__init__.py
if __name__ == '__main__':
    # Public API server
    public_api_app.run(
        host='0.0.0.0',
        port=public_api_app.config.get('PUBLIC_API_PORT', 9003),
        # Without this, the dev server only handles a single request at a time, which is super slow.
        debug=public_api_app.config.get('DEBUG', False),
        threaded=True)
