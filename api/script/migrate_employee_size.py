# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import datetime
import json
from ns.model.audience_criteria import AudienceCriteria
from ns.model.user_preference import UserPreference
from ns.controller.app import app
from ns.model import db

import logging
logger = logging.getLogger(__name__)

ranges_changes_map = {'"11-50"': '"11-20", "21-50"',
                      '"51-200"': '"51-100", "101-200"',
                      '"501-1000"': '"501-1,000"',
                      '"1001-5000"': '"1,001-2,000", "2,001-5,000"',
                      '"5001-10000"': '"5,001-10,000"',
                      '"10000+"': '"10,000+"'
                      }


def ranges_switch(ranges_str):
    for key, value in ranges_changes_map.iteritems():
        ranges_str = ranges_str.replace(key, value)
    return ranges_str


def fix_audience_criteria_table():
    audience_criteria_records = AudienceCriteria.query.filter(AudienceCriteria.criteria_type == "employeeSize").all()
    for record in audience_criteria_records:
        ranges_str = record.criteria_val
        new_ranges_str = ranges_switch(ranges_str)
        if ranges_str != new_ranges_str:
            record.criteria_val = json.dumps(json.loads(new_ranges_str))
            db.session.commit()


def fix_user_preference_table():
    user_preference_records = UserPreference.query.filter(UserPreference.type == "employeeSize").all()
    for record in user_preference_records:
        ranges_str = record.val
        new_ranges_str = ranges_switch(ranges_str)
        if ranges_str != new_ranges_str:
            record.val = json.dumps(json.loads(new_ranges_str))
            db.session.commit()


def backup_table(table_name):
    now_str = datetime.datetime.now().strftime("%y_%m_%d_%H_%M")
    sql_create_table = ("CREATE TABLE {table_name}"+"_" + now_str + " LIKE {table_name};").format(table_name=table_name)
    sql_backup = ("INSERT {table_name}"+"_" + now_str + " SELECT * FROM {table_name};").format(table_name=table_name)
    logger.info(sql_create_table)
    logger.info(sql_backup)
    db.engine.execute(sql_create_table)
    db.engine.execute(sql_backup)


with app.app_context():
    backup_table("audience_criteria")
    backup_table("user_preference")
    fix_audience_criteria_table()
    fix_user_preference_table()
