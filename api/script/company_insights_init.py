import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from ns.model.recommendation import Recommendation
from ns.model.company_insight import CompanyInsight


with app.app_context():
    field_display_names = Recommendation._maps(Recommendation)

    already_existed_names = CompanyInsight.get_all_display_names()

    for display_name, field in field_display_names.items():
        if display_name not in already_existed_names:
            CompanyInsight.create_insights(field.name, display_name)

