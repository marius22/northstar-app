/*
 * advanced_insights_corrections.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/************************************************
 * SQL for update advanced insights descriptions*
 ************************************************/
--12/09/2016
update northstar.advanced_insight set name = 'Business-to-Business (B2B)' where name = 'Has Business Customers';
update northstar.advanced_insight set name = 'Business-to-Consumer (B2C)' where name = 'Has Consumer Customers';
update northstar.advanced_insight set name = 'Facilities in Multiple Locations', description = 'Company has offices or other facilities located at more than one address based on web presence.' where name = 'Has Multiple Locations';
update northstar.advanced_insight set name = 'Employees in Multiple Locations', description = 'Company has employees located in more than one state or country based on contact information.' where name = 'Has Presence in Multiple Locations';
update northstar.advanced_insight set description = 'Level of use of marketing technology by company, presence of senior marketing employees at company, and execution of marketing programs across multiple channels by company.' where name = 'Marketing Sophistication';
