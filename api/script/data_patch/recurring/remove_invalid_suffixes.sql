/*
 * remove_invalid_suffixes.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

select domain into #records_to_delete
from northstar.company
where domain not like '%.com'
and domain not like '%.org'
and domain not like '%.tv'
and domain not like '%.me'
and domain not like '%.co'
and domain not like '%.aero'
and domain not like '%.biz'
and domain not like '%.net'
and domain not like '%.us'
and domain not like '%.io'
and domain not like '%.gov'
and domain not like '%.edu'
and domain not like '%.info' 
and domain not like '%.today'
and domain not like '%.aero'
and domain not like '%.cc'
and domain not like '%.careers'
and domain not like '%.fm'
and domain not like '%.coop'
and domain not like '%.life'
and domain not like '%.mil'
and domain not like '%.pro'
and domain not like '%.media'
and domain not like '%.consulting'
and domain not like '%.club'
and domain not like '%.boutique'
and domain not like '%.marketing'
and domain not like '%.mobi'
and domain not like '%.jobs'
and domain not like '%.ai'
and domain not like '%.ly'
and domain not like '%.digital'
and domain not like '%.design'
and domain not like '%.center'
and domain not like '%.care'
and domain not like '%.agency'
and domain not like '%.academy'
and domain not like '%.company'
and domain not like '%.guru'
and domain not like '%.world'
and domain not like '%.bio'
and domain not like '%.vc'
and domain not like '%.church'
and domain not like '%.travel'
and domain not like '%.solutions'
and domain not like '%.nyc'
and domain not like '%.services'
and domain not like '%.support';

--cascading delete
delete from northstar.business_tags where domain in (select domain from #records_to_delete);
delete from northstar.business_tags_keyword where domain in (select domain from #records_to_delete);
delete from northstar.company_tech where company_id in (select id from northstar.company join #records_to_delete on company.domain = #records_to_delete.domain);
delete from northstar.function where contact_id in (select con.id from northstar.contact con join northstar.company com on con.company_id = com.id join #records_to_delete r on com.domain = r.domain);
delete from northstar.contact where domain in (select domain from #records_to_delete);
delete from northstar.contact_summary where domain in (select domain from #records_to_delete);
delete from northstar.domain_indicator where domain in (select domain from #records_to_delete);
delete from northstar.funding_round where company_id in (select id from northstar.company join #records_to_delete on company.domain = #records_to_delete.domain);
delete from northstar.relevance where domain in (select domain from #records_to_delete);
delete from northstar.similar_domain where domain in (select domain from #records_to_delete) or match in (select domain from #records_to_delete);
delete from northstar.company_advanced_insights where domain in (select domain from #records_to_delete);
--company delete run last
delete from northstar.company where domain in (select domain from #records_to_delete);
