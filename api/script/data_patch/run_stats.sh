#! /bin/sh
#
# delete_backup_tables.sh
# Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
#
# Distributed under terms of the EverString license.
#



for table in $(<tables); do
    echo $table
    psql -w -U dataadmin company_contact -c "select count(*) from $table;"
done
