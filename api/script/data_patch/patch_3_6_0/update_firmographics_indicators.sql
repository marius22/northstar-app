/*
 * update_firmographics_indicators.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/***********************************************************
 * SQL for Cascading update indicators for specific domain *
 ***********************************************************/
--12/09/2016
update northstar.domain_indicator set indicator = 'state=Massachusetts' where domain = 'bullhorn.com' and indicator like 'state=%';
update northstar.domain_indicator set indicator = 'industry=Computer Software' where domain = 'marketo.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'state=Maryland' where domain = 'merkleinc.com' and indicator like 'state=%';
update northstar.domain_indicator set indicator = 'industry=Computer Software' where domain = 'infusionsoft.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'industry=Computer Software' where domain = 'steamboatunbridled.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'employees=11 to 50' where domain = 'natsoonline.com' and indicator like 'employees=%';
update northstar.domain_indicator set indicator = 'revenue=0M to 1M' where domain = 'natsoonline.com' and indicator like 'revenue=%';
