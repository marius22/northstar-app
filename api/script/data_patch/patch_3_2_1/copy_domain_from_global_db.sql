/*
 * copy_domain_from_global_db.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/******************************************************************
 * SQL for copying domain related data from other database to eap *
 ******************************************************************/
\set domain 'walkme.com'
--11/15/2016
insert into northstar.company (select * from company_contact.public.company where domain = :'domain');
insert into northstar.company_tech (select t.* from company_contact.public.company_tech t join company_contact.public.company c on t.company_id = c.id where c.domain = :'domain');
insert into northstar.competitor (select c.* from company_contact.public.competitor c join company_contact.public.company com on c.company_id = com.id where com.domain = :'domain');
insert into northstar.domain_indicator (select * from company_contact.public.domain_indicator where domain = :'domain');
insert into northstar.function (select f.* from company_contact.public.function f join company_contact.public.contact con on f.contact_id = con.id where con.domain = :'domain');
insert into northstar.funding_round (select f.* from company_contact.public.funding_round f join company_contact.public.company com on f.company_id = com.id where com.domain = :'domain');
--insert into northstar.contact (select * from company_contact.public.contact where domain = :'domain');
--insert into northstar.contact_summary (select * from company_contact.public.contact_summary where domain = :'domain');
--different schema
--insert into northstar.relevance (select * from company_contact.public.relevance where domain = :'domain');
--insert into northstar.business_tags (select * from company_contact.public.business_tags where domain = :'domain');
--insert into northstar.business_tags_keyword (select * from company_contact.public.business_tags_keyword where domain = :'domain');
--no similar domain for global domains
--insert into northstar.similar_domain (select * from company_contact.public.similar_domain where domain = :'domain' or match = :'domain')
