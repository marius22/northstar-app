/*
 * unset_mismatched_zipcodes.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/************************************************
 * SQL for unset mismatched zipcodes in company *
 ************************************************/

UPDATE northstar.company
   SET zipcode = ''
WHERE zipcode != ''
AND   ((state = 'Alaska' AND (zipcode < '99501' OR zipcode > '99950')) 
        OR (state = 'Alabama' AND (zipcode < '35004' OR zipcode > '36925')) 
        OR (state = 'Arkansas' AND (zipcode < '71601' OR zipcode > '72959') AND zipcode != '75502') 
        OR (state = 'Arizona' AND (zipcode < '85001' OR zipcode > '86556')) 
        OR (state = 'California' AND (zipcode < '90001' OR zipcode > '96162')) 
        OR (state = 'Colorado' AND (zipcode < '80001' OR zipcode > '81658')) 
        OR (state = 'Connecticut' AND (zipcode < '06001' OR zipcode > '06389') AND (zipcode < '06401' OR zipcode > '06928')) 
        OR (state = 'District of Columbia' AND (zipcode < '20001' OR zipcode > '20799')) 
        OR (state = 'Delaware' AND (zipcode < '19701' OR zipcode > '19980')) 
        OR (state = 'Florida' AND (zipcode < '32004' OR zipcode > '34997')) 
        OR (state = 'Georgia' AND (zipcode < '30001' OR zipcode > '31999') AND zipcode != '39901') 
        OR (state = 'Hawaii' AND (zipcode < '96701' OR zipcode > '96898')) 
        OR (state = 'Iowa' AND (zipcode < '50001' OR zipcode > '52809') AND (zipcode < '68119' OR zipcode > '68120')) 
        OR (state = 'Idaho' AND (zipcode < '83201' OR zipcode > '83876')) 
        OR (state = 'Illinois' AND (zipcode < '60001' OR zipcode > '62999')) 
        OR (state = 'Indiana' AND (zipcode < '46001' OR zipcode > '47997')) 
        OR (state = 'Kansas' AND (zipcode < '66002' OR zipcode > '67954')) 
        OR (state = 'Kentucky' AND (zipcode < '40003' OR zipcode > '42788')) 
        OR (state = 'Louisiana' AND (zipcode < '70001' OR zipcode > '71232') AND (zipcode < '71234' OR zipcode > '71497')) 
        OR (state = 'Massachusetts' AND (zipcode < '01001' OR zipcode > '02791') AND (zipcode < '05501' OR zipcode > '05544')) 
        OR (state = 'Maryland' AND (zipcode < '20335' OR zipcode > '20797') AND (zipcode < '20812' OR zipcode > '21930') AND zipcode != '20331') 
        OR (state = 'Maine' AND (zipcode < '03901' OR zipcode > '04992')) 
        OR (state = 'Michigan' AND (zipcode < '48001' OR zipcode > '49971')) 
        OR (state = 'Minnesota' AND (zipcode < '55001' OR zipcode > '56763')) 
        OR (state = 'Missouri' AND (zipcode < '63001' OR zipcode > '65899')) 
        OR (state = 'Mississippi' AND (zipcode < '38601' OR zipcode > '39776') AND zipcode != '71233') 
        OR (state = 'Montana' AND (zipcode < '59001' OR zipcode > '59937')) 
        OR (state = 'North Carolina' AND (zipcode < '27006' OR zipcode > '28909')) 
        OR (state = 'North Dakota' AND (zipcode < '58001' OR zipcode > '58856')) 
        OR (state = 'Nebraska' AND (zipcode < '68001' OR zipcode > '68118') AND (zipcode < '68122' OR zipcode > '69367')) 
        OR (state = 'New Hampshire' AND (zipcode < '03031' OR zipcode > '03897')) 
        OR (state = 'New Jersey' AND (zipcode < '07001' OR zipcode > '08989')) 
        OR (state = 'New Mexico' AND (zipcode < '87001' OR zipcode > '88441')) 
        OR (state = 'Nevada' AND (zipcode < '88901' OR zipcode > '89883')) 
        OR (state = 'New York' AND (zipcode < '10001' OR zipcode > '14975') AND zipcode != '06390') 
        OR (state = 'Ohio' AND (zipcode < '43001' OR zipcode > '45999')) 
        OR (state = 'Oklahoma' AND (zipcode < '73001' OR zipcode > '73199') AND (zipcode < '73401' OR zipcode > '74966')) 
        OR (state = 'Oregon' AND (zipcode < '97001' OR zipcode > '97920')) 
        OR (state = 'Pennsylvania' AND (zipcode < '15001' OR zipcode > '19640')) 
        OR (state = 'Rhode Island' AND (zipcode < '02801' OR zipcode > '02940')) 
        OR (state = 'South Carolina' AND (zipcode < '29001' OR zipcode > '29948')) 
        OR (state = 'South Dakota' AND (zipcode < '57001' OR zipcode > '57799')) 
        OR (state = 'Tennessee' AND (zipcode < '37010' OR zipcode > '38589')) 
        OR (state = 'Texas' AND (zipcode < '75001' OR zipcode > '75501') AND (zipcode < '75503' OR zipcode > '79999') AND (zipcode < '88510' OR zipcode > '88589') AND zipcode != '73301') 
        OR (state = 'Utah' AND (zipcode < '84001' OR zipcode > '84784')) 
        OR (state = 'Virginia' AND (zipcode < '20040' OR zipcode > '20167') AND (zipcode < '22001' OR zipcode > '24658')) 
        OR (state = 'Vermont' AND (zipcode < '05001' OR zipcode > '05495') AND (zipcode < '05601' OR zipcode > '05907')) 
        OR (state = 'Washington' AND (zipcode < '98001' OR zipcode > '99403')) 
        OR (state = 'Wisconsin' AND (zipcode < '53001' OR zipcode > '54990')) 
        OR (state = 'West Virginia' AND (zipcode < '24701' OR zipcode > '26886')) 
        OR (state = 'Wyoming' AND (zipcode < '82001' OR zipcode > '83128')))

