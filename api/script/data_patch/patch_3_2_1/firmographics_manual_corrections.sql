/*
 * firmographics_manual_corrections.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/**************************************************************************
 * SQL for one-off update specific company attributes for specific domain *
 **************************************************************************/
update northstar.company set name = 'Marketo, Inc.' where domain = 'marketo.com';
update northstar.company set state = 'Utah', city = 'South Jordan', street = '698 West 10000 South Suite 500', zipcode = '84095' where domain = 'appsense.com';
