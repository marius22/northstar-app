/*
 * backup_tables.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

\set role1 'user'
\set role2 'editor'
\set user1 :env:role1 
\set user2 :env:role2
\set backup '_back'
\set tablename_back :tablename:backup

drop table if exists northstar.:tablename_back;
create table if not exists northstar.:tablename_back (like northstar.:tablename including defaults);
insert into northstar.:tablename_back (select * from northstar.:tablename);
grant select on northstar.:tablename_back to :user1;
grant all on northstar.:tablename_back to :user2;
