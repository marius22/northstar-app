INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=LinkedIn Platform API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'employees=11 to 50'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=CSS Media Queries'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Name Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=yepnope'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Webmaster'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=financial'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=banking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Live Stream / Webcast'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_peru_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Pixel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Live Writer Support'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=PHP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=cash flow'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Wordpress Monthly Activity'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Masked Input'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Charting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Compatibility'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Social Video Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery UI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SEO_META_DESCRIPTION'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'industry=Computer Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_social_twitter_following_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=bank'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_peru_10_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'employees_and_revenue=11 to 50&&5M to 10M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_comp_profile_office_multiple'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Programming Language'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Yoast WordPress SEO Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=A/B Testing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_peru_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Audience Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Widgets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=bill pay'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=CrazyEgg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Linode'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Moment JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=New Relic'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Business Email Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_pricing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=IFrame'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Audio / Video Media'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=1and1 DNS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_web_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_peru_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=html5shiv'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SEO_H1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Web Master Registration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_web_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Bing Universal Event Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery Validate'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=payroll'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_web_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Web Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Lucky Orange'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Content Management System'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Amazon'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=JavaScript Libraries and Functions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Advertising'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_web_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=bookkeeping'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Canonical Content Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Wordpress Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=time tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'industry_and_state=Computer Software&&Georgia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=LinkedIn Follow Company Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=small'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'state=Georgia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_domain_suffix_com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Analytics and Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Forms and Surveys'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_social_twitter_followers_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=SEO Meta Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Wysija'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'revenue=5M to 10M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Max Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=easyXDM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Extended Validation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_web_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=GStatic Google Static Content'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=chat software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_contact'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=JetPack'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Akamai'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=CryptoJS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_peru_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=WordPress Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'industry_and_employees=Computer Software&&11 to 50'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Like'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Resolution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_peru_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Modernizr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_web_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Apple Mobile Web Clips Icon'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Site Optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Cascading Style Sheets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'revenue_and_state=5M to 10M&&Georgia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Marketing Performance Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Javascript'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=SEO Header Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'country=United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SEO_TITLE'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Marketing Performance Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=financials'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Orientation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_about'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=1and1 Email Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Pingback Support'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Rack Cache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Open Graph Protocol'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=jQuery Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Feedback Forms and Surveys'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Page Administration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=invoicing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Web Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Meta Tags'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_perm_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_rank_delta_100k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Conditional Comments'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook for Websites'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Domain Insights'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Media'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'employees_and_country=11 to 50&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=management platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=WordPress Stats'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '4_grams=financial management and accounting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Custom Audiences'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '3_grams=live chat software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_perm_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_help'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Blog'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'employees_and_state=11 to 50&&Georgia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Ruby on Rails'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Device Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=YouTube'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Meta Description'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SEO_H2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Mandrill'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Social SDK'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Min Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=GeoTrust SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=payment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Font API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=MailPoet'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=small business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=SharpSpring'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Application Performance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=HTML 5 Specific Tags'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Chrome IE Frame'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=bill payment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Sitelinks Search Box'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery 1.7.2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Yoast Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook SDK'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_web_rank_delta_-100k-'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_web_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Olark'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=SEO Title Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Friends Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=WordPress Grid'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=accounting software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=business intelligence'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Viewport Meta'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_login'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Apache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Fonts'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=cloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Cowboy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=financial management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Live Chat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Open Source'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Heroku Proxy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=accounting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=FitVids.JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Symantec VeriSign'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Verisign EV'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Font Awesome'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '3_grams=management and accounting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Transactional Email'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Really Simple Discovery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Wordpress 3.9'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=US hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Web Hosting Provider Email'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_social_twitter_tweets_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery Masonry'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=chat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Gravatar Profiles'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Email Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Document Standards'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=JavaScript Library'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Rackspace'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Ruby on Rails Token'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Universal Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=CDN JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=pay'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=one platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=D3 JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery Form'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Analytics Event Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Amazon Virginia Region'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_webpage_homepage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_pageview_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_social_twitter'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'revenue_and_country=5M to 10M&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=UI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Sharer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=RapidSSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=manage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Strict Transport Security'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=YouTube IFrame Embed'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Elegant Builder'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=VPS Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'industry_and_revenue=Computer Software&&5M to 10M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=accountant'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Content Experiments'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=jQuery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=GSAP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Data Archiving, Back-Up & Recovery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Visualization Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Photoshop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_revenue_5b+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Barracuda Networks (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Name Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_youtube_account_age_6_to_7'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=yepnope'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=TrueCrypt'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Avada'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=management solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SAP ERP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=IT Governance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=XPath'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_type_venture'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=UltraDNS neustar'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce.com Jigsaw'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server 2008'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_web_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=learning'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=talent'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Conversion Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.insurance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Paychex'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Citrix (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Network Management (Hardware)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Compatibility'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Cloud Infrastructure Computing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=hire'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=acquisition'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=human capital management software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Drupal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=hiring'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'industry=Computer Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.hospitality'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server 2000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Programming Language'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_comp_profile_office_multiple'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Yoast WordPress SEO Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Productivity Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=data management and reporting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Remote Computer/Server Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_netsuite.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=global talent'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Hardware (Basic)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_perm_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Oracle Database'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Visual Studio'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Widgets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hrms solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=time and attendance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=recruiting and hiring'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Business Email Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=jQuery prettyPhoto'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=OpenSSL 1.0.1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Office 365 Mail'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise Resource Planning (ERP)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apple Desktop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Database Management Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=talent acquisition strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Watir'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'revenue=50M to 100M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Productivity Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Linux'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apple (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_comp_unisys.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Data Management & Storage (Hardware)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Dedicated Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Social Media Systems'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ZoomInfo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Web Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_youtube_views_100001_to_1000000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Mantis'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=JavaScript Libraries and Functions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_news'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_web_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.bizservice'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent portal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=management and reporting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Wordfence'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Hosted Web Font Loader'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Slider'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=IIS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=BlackBerry Enterprise Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_revenue_1b_to_5b'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Limiter Modules'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=hr software solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=performance management solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=CRM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Hyper-V Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ADP (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=employee engagement strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Pardot'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Perl'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Analytics and Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_domain_suffix_com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=CallidusCloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WP Engine'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=HR Management Systems (HRMS)/Human Capital Management (HCM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apache (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'state=Illinois'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Travel and Expense Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=F5 Networks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP Unified Functional Testing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=compliance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Mobile'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=FlexSlider'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hiring strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_contact'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ASP.NET'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Marketo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Product Lifecycle Management (PLM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Dreamweaver'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=learning and talent management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce.com CRM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft System Center Virtual Machine Manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=ats software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Change Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=YouTube IFrame Upload'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=WordPress Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.software.mfg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Facebook Like'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_new york'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'industry_and_revenue=Computer Software&&50M to 100M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=hrms'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Articulate Storyline'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Windows Server 2008'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise Business Solutions (EBS)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server 2005'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_peru_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_revenue_500m_to_1b'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=management change'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_web_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Mac OS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=portal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Marketing Performance Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Atlassian Greenhopper'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise Performance Management (EPM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=training and education'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Symantec (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=talent management strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=OpenSSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Web Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Quality Management System'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=recruiter'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=OpenSSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Citrix GoToMeeting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP WinRunner'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=employee experience'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce.com Marketing Cloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=new hire'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'employees_and_country=501 to 1000&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SharePoint'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_virginia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Media'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=employee data management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe LiveCycle'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_twitter_following_101_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=social recruiting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.bizservice.accounting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Payment Acceptance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_new york'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_tour'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Blog'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Web Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_new jersey'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Dynamics GP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Device Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Plumtree Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Operating Systems and Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Mandrill'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=global talent acquisition'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=recruit'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Hosted Libraries'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=learning management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Min Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Selenium'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=travel and expense management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Middleware Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=AJAX Libraries API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=resource management system'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_perm_1000+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Concur (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_rank_delta_-1000_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_job'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Enterprise Applications'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetApp SnapShot'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Sitelinks Search Box'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_latest_round_age_less_than_1yr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Yoast SEO Premium'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Yoast Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise Learning'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WP Super Cache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Olark'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_team'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_revenue_250m_to_500m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SAP Crystal Reports'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Ceridian'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=cloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_youtube_subscribers_101_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Windows 7'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_perm_1000+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=human'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=FitVids.JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Mapping'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=IBM Rational ClearCase'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP Application Lifecycle Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_legal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Marketing Automation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=employee performance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Transactional Email'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft PowerPoint'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=recruiting metric'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=strategic talent management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Really Simple Discovery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=US hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_concur.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_washington'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=jQuery Masonry'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'employees_and_state=501 to 1000&&Illinois'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Software - Other'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=BootstrapCDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=JavaScript Library'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Rackspace'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_latest_round_amt_2_to_10m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server 2012'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Office'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Windows XP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=PostgreSQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=VMware ESX'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_perm_1000+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=recruiting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Windows Server 2003'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=performance management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=BlackBerry'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Target'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft System Center'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Business Intelligence'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=human resource management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SAP R/3'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=job'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hr solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_twitter'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Telerik'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=UI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Skype'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Tag Manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'industry_and_state=Computer Software&&Illinois'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Java'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_investors_total_5_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=learning management system'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Reporting Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Atlassian (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_youtube'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=jQuery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Solaris'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Hootsuite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=InsideSales.com PowerDialer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_comp_accenture.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=GSAP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce.com Data.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'industry_and_country=Computer Software&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=nginx'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hr software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Barracuda Spam & Virus Firewall'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Application Development & Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.energy.energy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_total_amt_100_to_500m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Contact Center Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adaptive Insights'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_texas'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_adp.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=RSS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_learnmore'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=System Security Services'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Unix'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Network Computing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.mfg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Server Technologies (Software)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=employee engagement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Mobile'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=SSL Certificates'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WordPress'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=PayPal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Froogaloop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=recruitment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apple iPhone'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Exchange'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP Quality Center'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Root Authority'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Visitor Count Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=GoDaddy SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=CSS Media Queries'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=talent management software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_microsoft.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.software.mfg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Customer Relationship Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Live Writer Support'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=PHP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.mfg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.transportation.moving'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Ecommerce'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Hypervisor'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_blog'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=integrated'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Quantcast Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Marketing Cloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Application'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_webinar'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Atlassian JIRA'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WebFont Loader'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=TechSmith Camtasia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Collaboration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_web_rank_delta_-1000_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=career portal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Audience Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hr manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_oklahoma'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Azure US East Region'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=IBM Rational ClearQuest'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WP Video Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Web-Oriented Architecture'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Omniture (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.bizservice'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.mfg.chemicals.chemicals'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=onboarding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=form i - 9'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_event'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Consumer Electronics, Personal Computers & Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_washington'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.energy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent acquisition'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ExactTarget'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Transact-SQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=screening'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=jQuery Mousewheel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Content Management System'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=AngularJS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Advertising'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=talent development'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'industry_and_employees=Computer Software&&501 to 1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_rank_delta_-1000_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_missouri'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Mapping SaaS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=engagement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'revenue_and_state=50M to 100M&&Illinois'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_web_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category= Server & Data Center'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft SQL Server Reporting Services'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=employee engagement program'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_peru_delta_10_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=job distribution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Cisco WebEx'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=IT Infrastructure & Operations Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_revenue_250m_to_500m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Salesforce Desk Link'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Operating Systems and Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Prophix'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Financial Analytical Applications'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hr activity'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_twitter_followers_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_web_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Max Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetApp Storage Systems'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft System Center OpsMgr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=acquisition strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=service and consulting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Exchange Online'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_comp_tenneco.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_comp_starbucks.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_peru_delta_10_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_pennsylvania'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SuccessFactors'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=employee'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=performance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_revenue_5b+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Modernizr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_fund_investors_top_firms'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Data Center Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Marketing Performance Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ArcGIS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=employee onboarding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=training'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_perm_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Software Configuration Management (SCM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'country=United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_monster.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetApp SnapDrive'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Orientation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_about'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Multi-Channel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.hospitality.restaurant'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=jQuery Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=StoneRiver'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe ColdFusion'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Dynatrace'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Enterprise Content'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=social medium innovation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_perm_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=learning management solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetApp (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Silverlight'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Web & Portal Technology'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_connecticut'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_help'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=WordPress Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Gainsight'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Network Management (Software)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Customer Relationship Management (CRM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft.NET'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=GeoTrust SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Font API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=QuickBooks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Online Video Platform (OVP)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category= Platform Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Corel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Application Performance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'employees=501 to 1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_product'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Tag Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=carouFredSel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Qvidian'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apache 2.2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Progress Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_perm_1000+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Enterprise Content Management (ECM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Payment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=integrated talent management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_revenue_500m_to_1b'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_paychex.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetSuite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=W3 Total Cache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Software (Basic)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Viewport Meta'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=management and hr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Cloud Services'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Fonts'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=IIS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Live Chat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Open Source'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'employees_and_revenue=501 to 1000&&50M to 100M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=MySQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Windows Server 2000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP LoadRunner'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_perm_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Verified Link'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Font Awesome'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_deltek.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.mfg.chemicals'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Xactly'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Node.js'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Joomla'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Informatica'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_peru_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_texas'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_social_twitter_tweets_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Operating Systems & Computing Languages'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=WordPress 4.0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=PrestaShop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Email Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Sage (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Conversion Optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Sage 100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_revenue_100m_to_250m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Windows Web Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Illustrator'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=InsideView'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_pageview_peru_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Apache JMeter'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google Universal Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_overview'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Dynamics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Dell EqualLogic Networked Storage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_state_california'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_homepage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_pageview_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_revenue_1b_to_5b'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Disaster Recovery (DR)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=RapidSSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=SilkRoad Recruiting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'revenue_and_country=50M to 100M&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=iCIMS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=hr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '1_grams=manage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=hr professional'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Adobe Creative Suite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=HP QuickTest Professional'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '2_grams=strategic service'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=ESRI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Microsoft Azure'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Lawson Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=NetApp SAN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Google AdWords'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_month_reach_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Perforce'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Visualization Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe Photoshop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=DemDex'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Name Server'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=supplier'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=yepnope'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Bookmarking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Live Stream / Webcast'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=IT Governance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=ZeroClipboard'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Video Players'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_fund_type_venture'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=digital platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MaxCDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Yahoo Small Business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_raised_growth_funding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Lightbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_comp_profile_acquired_other_comp'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Conversion Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_release_new_products'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Basho Riak'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe InDesign'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Citrix (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Keen IO'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=reducing risk'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=apps'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Compatibility'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Cloud Infrastructure Computing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe Premiere'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=cloud technology'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=faster payment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Programming Language'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_comp_profile_office_multiple'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Yoast WordPress SEO Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Retargeting / Remarketing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest_pincount_11_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Productivity Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=collaboration platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon EC2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=cloud based technology'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=change management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=cloud - based'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Hardware (Basic)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=PL/SQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Widgets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=OpenID'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Linode'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Telecitygroup International'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Business Email Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery prettyPhoto'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_expand_team_members'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=IFrame'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Audio / Video Media'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SSL by Default'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Enterprise Resource Planning (ERP)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Database Management Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=build your own'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=html5shiv'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Infor CRM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Productivity Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Linux'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apple (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=nginx 1.4'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'employees_and_revenue=201 to 500&&25M to 50M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Optimizely'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Social Media Systems'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SEO_META_KEYWORDS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Web Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_youtube_views_100001_to_1000000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=JavaScript Libraries and Functions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_news'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_web_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Plus One Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Dstillery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Slider'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=vCard'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Canonical Content Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery 1.9.1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=ap'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SSL.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Intercom'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=buyer and supplier'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=CRM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Wordpress Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Wordpress Daily Activity'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=transportation and logistics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Analytics and Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_domain_suffix_com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_web_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Forms and Surveys'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=WP Engine'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=HR Management Systems (HRMS)/Human Capital Management (HCM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Plus Embedded Posts'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Passive Localization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Travel and Expense Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=bxSlider'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MediaElement.js'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Analytics with Ad Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GStatic Google Static Content'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Mobile'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter CDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=FlexSlider'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner_comp_wipro.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_contact'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Marketo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Salesforce.com CRM'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Akamai'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Docs'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=procurement strategy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=medium company'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GratisDNS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=WordPress Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=automation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner_industry_industry.software.mfg'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=project manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Vimeo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Meta Keywords'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Like'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_web_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Cascading Style Sheets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Enterprise Business Solutions (EBS)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Jobscore'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Javascript'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_peru_delta_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Qualaroo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Reinvigorate'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SEO_TITLE'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Epicor (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=open platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_youtube_account_age_4_to_5'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Graph API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Pingback Support'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Customer.io'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Retina JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rack Cache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Open Graph Protocol'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=virtual credit card'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Mixpanel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Web Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Meta Tags'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=capital optimisation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_customer_expansion'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Citrix GoToMeeting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Code Prettify'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Conditional Comments'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Openads/OpenX'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook for Websites'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Media'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '4_grams=training and change management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Payment Acceptance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Wistia'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=DMARC'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Blog'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GitHub'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Device Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Browser Specific'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=electronic invoicing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Login Button'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SEO_H2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Operating Systems and Servers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=developer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Hosted Libraries'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Social SDK'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Min Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '4_grams=cloud - based platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Fancybox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Document Encoding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AJAX Libraries API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Mailgun'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=HTML 5 Specific Tags'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=integration and testing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_job'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Enterprise Applications'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Sitelinks Search Box'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Crosswise'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Salesforce SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Yoast Plugins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Olark'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SWFObject'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Custom Content Scroller'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_team'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'employees_and_country=201 to 500&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=SEO Title Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=WordPress Grid'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Zendesk'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Dynamic Creative Optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_youtube_subscribers_101_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Shadowbox JS for WordPress'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Resonate Insights'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Marketing Automation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Transactional Email'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_peru_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Microsoft PowerPoint'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery CDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Really Simple Discovery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=US hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Autocomplete'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Jenkins'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SuperFish'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Geolify'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=BootstrapCDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_web_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Document Standards'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=JavaScript Library'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rackspace'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'industry=Information Technology and Services'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Microsoft Office'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=PostgreSQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=IponWeb BidSwitch'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Ruby on Rails Token'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=CDN JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Chartbeat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Data Management Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=D3 JS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery UI Tabs'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Form'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Analytics Event Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=FreeBSD'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Gist'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Vertical Markets'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon Virginia Region'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_twitter'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Smartling'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=enterprise'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=UI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Tag Manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=DigiCert SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Shared Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Sharer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Advertiser Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=business travel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Fontdeck'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache Hbase'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Java'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_fund_investors_total_5_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache 2.4'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Social Sharing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=master data'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_youtube'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Edge Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=The Trade Desk'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rhinoceros D'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=nginx'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rapleaf'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_comp_profile_years_age_3_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Application Development & Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Zendesk Dropbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Verified CDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=online purchase'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'industry_and_state=Information Technology and Services&&California'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_fund_total_amt_100_to_500m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=RSS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Unix'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=QuickBooks Online'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Server Technologies (Software)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner_industry_industry.software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Mobile'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=SSL Certificates'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=WordPress'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=PrettyPrint'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Search Engine'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=PayPal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Exchange FBX'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=accounting and finance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=procure to pay'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter Tweet Button'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Eventbrite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=brand and marketing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon AWS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Like Button'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe After Effects'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Root Authority'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_web_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Visitor Count Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GoDaddy SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=CSS Media Queries'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=nginx 1.2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GoDaddy'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery 1.8.2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Webmaster'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Siebel (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Modernizr 2.6'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=automated process'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Pixel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Customer Relationship Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Live Writer Support'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=PHP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Easing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=invoice'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_perm_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Perfect Audience'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=svp'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=process and technology'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Ecommerce'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Magento'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Charting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=UTF-8'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_blog'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Contextual Advertising'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_peru_delta_10_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter Ads'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=WordPress Weekly Activity'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Cookie Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Social Video Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery UI'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=ap automation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SEO_META_DESCRIPTION'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Enterprise'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_webinar'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_perm_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_twitter_following_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Document Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=virtual'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_getstarted'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=BlueKai'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=A/B Testing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Expensify'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Video Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=business and finance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Collaboration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twitter Timeline'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Apps'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=supply chain'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Audience Measurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'industry_and_country=Information Technology and Services&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=spend management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=J2EE'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_web_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Web-Oriented Architecture'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=procurement'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_perm_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'revenue=25M to 50M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=New Relic'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=onboarding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Dedicated Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'employees=201 to 500'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=supplier financing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Ad Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_event'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Consumer Electronics, Personal Computers & Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Mobile Non Scaleable Content'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Analytics Multiple Trackers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=StatusPage IO'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=WP PageNavi'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SEO_H1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Web Master Registration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Device Height'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Bing Universal Event Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Customer.io Mail'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=business case'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_open_new_offices'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Mousewheel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=payable'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=social technology'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=German HREF LANG'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Content Management System'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AngularJS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=HTML5 History API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Advertising'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Segment.io'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=working capital optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Meta Robot'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=comScore'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest_following_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Image Tool Bar'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Ezakus'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Cisco WebEx'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest_followers_11_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=IT Infrastructure & Operations Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Website Optimizer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=kicking as'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '4_grams=connecting buyer and supplier'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=process'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Financial Analytical Applications'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Analytics Classic'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_fund_latest_round_amt_50_to_100m'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_twitter_followers_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=SalesLogix'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=SEO Meta Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Max Width'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Dropbox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Jetty'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_peru_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=JetPack'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_expand_the_executive_team'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache Hadoop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=CryptoJS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_perm_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'state=California'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_peru_delta_10_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=innovate'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'employees_and_state=201 to 500&&California'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Spanish HREF LANG'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=global'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Modernizr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=capital'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Ubuntu'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apple Mobile Web Clips Icon'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Data Center Solutions'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Marketo Forms'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Site Optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Marin Software'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=account payable'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=social medium company'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_comp_profile_crunchbase_competitors'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Tag API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Prototype'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=SEO Header Tag'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Software Configuration Management (SCM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'country=United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AppNexus'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Orientation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_about'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_web_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Multi-Channel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'revenue_and_state=25M to 50M&&California'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=collaborate'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MediaMath'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_web_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_product_character_cloud'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_rank_10k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_perm_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=jQuery Plugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Feedback Forms and Surveys'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=invoicing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Vidyard'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest_boards_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=DNS Prefetch'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Enterprise Content'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=app'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_perm_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Domain Insights'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AddThis'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Contact Form 7'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=TrackJS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Plus One Button'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook Custom Audiences'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Sage 300'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_help'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'industry_and_employees=Information Technology and Services&&201 to 500'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=WordPress Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Adsense'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_rank_10k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Ruby on Rails'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=YouTube'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Meta Description'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=collaboration'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Customer Relationship Management (CRM)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=roll - out'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=finance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=LinkedIn Ads'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Bizo'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=GeoTrust SSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Font API'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=QuickBooks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Ad Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=geoPlugin'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=HTML5 DocType'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Bizo Insights'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Microsoft Yammer'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Tag Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_product'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Application Performance'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=e - invoice'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache 2.2'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Chrome IE Frame'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'revenue_and_country=25M to 50M&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=selectivizr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=improve process'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Oracle E-Business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Digital Video Ads'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=manager'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_establish_partnership'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rubicon Project'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Payment'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_peru_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Facebook SDK'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'industry_and_revenue=Information Technology and Services&&25M to 50M'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Design & Publishing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=timeago'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Ticketing System'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=NetSuite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Demand-side Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Software (Basic)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Viewport Meta'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_login'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Puppet Labs'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Cloud Services'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Fonts'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_domain_length_10+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apple MacBook Air'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Live Chat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Open Source'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MailChimp SPF'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=national health service'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=webinars and event'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Sage Accpac'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MySQL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Audience Targeting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_pinterest_likes_0_to_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Font Awesome'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Elasticsearch'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_perm_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=event and webinars'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Node.js'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Turn'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Joomla'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=supplier onboarding'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_rank_delta_-100k_to_-1k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=script.aculo.us'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_social_twitter_tweets_1001_to_10000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Operating Systems & Computing Languages'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Email Hosting Providers'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Gravity Forms'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Apps for Business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=CloudFront'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Device Pixel Ratio'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_quarter_pageview_rank_1000k+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Wordpress 4.1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'industry_and_country=Computer Software&&United States'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=nginx'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Google Analytics Enhanced Link Attribution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '3_grams=integrated financial management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_month_pageview_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=fully integrated'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Online Video Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=DoubleClick.Net'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Classie'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=RSS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Handlebars'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Twemoji'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Mobile'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=SSL Certificates'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=WordPress'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=payroll processing'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_reach_perm_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Frameworks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=HSTS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Social Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '2_grams=cloud bookkeeping'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  '1_grams=intelligence'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_day_web_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Twitter Cards'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_name=Facebook Like Button'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_traffic_week_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=CSS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_category=Syndication Techniques'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'has_domain_length_7_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Root Authority'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'finsync.com',
  'tech_sub_category=Visitor Count Tracking'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Web Content Management System (WCMS)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=pcAnywhere'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_industry_industry.transportation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=mod_ssl'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '4_grams=human resource management system'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=Virtualization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category= Application & Desktop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=DoubleClick.Net'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Blackboard'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=IIS 8'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=assessment and survey'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_web_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_sub_category=WordPress Theme'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Twemoji'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=VMware (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_comp_indeed.com'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=Isotope'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_day_web_rank_1k_to_10k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Frameworks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_partner_industry_industry.bizservice.hr'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_rank_delta_0_to_1000'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_week_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=COBOL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_category=Syndication Techniques'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_traffic_quarter_reach_perm_delta_-100_to_-10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  '3_grams=talent management solution'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_domain_length_7_10'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'tech_name=DevExpress'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'silkroad.com',
  'has_webpage_client_state_illinois'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apache Tomcat'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Sage (Unspecified Product)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Conversion Optimization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon Route 53'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Marketo Mail'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Easy Fancybox'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_perm_1_to_100'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Shutterstock'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe Illustrator'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Apple Laptop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Ad Exchange'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Universal Analytics'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_overview'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Animation'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AdRoll'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Placeholder'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_web_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=chain'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner_state_california'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Hosted jQuery'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Star Rating'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Python'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_homepage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_rank_delta_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Waypoints'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=AppNexus Segment Pixel'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '3_grams=supplier master data'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=virtual assistant'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_perm_delta_-10_to_0'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Datalogix'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_webpage_partner_revenue_1b_to_5b'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=RapidSSL'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=BlueKai DMP'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=supplier management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=manage'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Adobe Creative Suite'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_attend_marketing_conferences'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=UK hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Cookie'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=VPS Hosting'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Pubmatic'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Index Exchange'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_comp_profile_crunchbase'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_news_event_attend_international_conferences'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Google Apps for Business'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Amazon CloudFront'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=CloudFront'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=jQuery Metadata'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Device Pixel Ratio'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Perforce'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Web Content Management System (WCMS)'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Docker'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Campaign Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Enterprise DNS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Drip'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=webinars'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_day_pageview_peru_delta_100+'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Virtualization'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category= Application & Desktop'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Online Video Platform'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_pageview_peru_0_to_1'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_pageview_rank_delta_1k_to_100k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=DoubleClick.Net'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_month_reach_rank_100k_to_1000k'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Content Delivery Network'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Zendesk Portal'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '1_grams=connect'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Twemoji'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  '2_grams=working capital'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Frameworks'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Social Management'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=EnterpriseDB'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=MongoDB'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=NWMatcher'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Rackspace CDN'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=Facebook Exchange'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_name=Chango'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_sub_category=CSS'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_week_reach_rank_has'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'tech_category=Syndication Techniques'
);

INSERT INTO northstar.domain_indicator
(
  DOMAIN,
  indicator
)
VALUES
(
  'tradeshift.com',
  'has_traffic_quarter_reach_perm_delta_-100_to_-10'
);


COMMIT;
