/*
 * update_firmographics_indicators.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/***********************************************************
 * SQL for Cascading update indicators for specific domain *
 ***********************************************************/
update northstar.domain_indicator set indicator = 'state=Illinois' where domain = 'silkroad.com' and indicator like 'state=%';
update northstar.domain_indicator set indicator = 'state=Washington' where domain = 'concur.com' and indicator like 'state=%';
update northstar.domain_indicator set indicator = 'industry=Computer Software' where domain = 'concur.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'industry=Internet' where domain = 'formstack.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'industry=Information Technology and Services' where domain = 'tradeshift.com' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'industry=Computer Software' where domain = 'greenhouse.io' and indicator like 'industry=%';
update northstar.domain_indicator set indicator = 'employees=201 to 500' where domain = 'silkroad.com' and indicator like 'employees=%';
update northstar.domain_indicator set indicator = 'employees=10000+' where domain = 'glazersbeer.com' and indicator like 'employees=%';
update northstar.domain_indicator set indicator = 'employees=11 to 50' where domain = 'silksf.com' and indicator like 'employees=%';
update northstar.domain_indicator set indicator = 'revenue=1B to 5B' where domain = 'glazersbeer.com' and indicator like 'revenue=%';
update northstar.domain_indicator set indicator = 'revenue=1M to 5M' where domain = 'silksf.com' and indicator like 'revenue=%';

COMMIT;
