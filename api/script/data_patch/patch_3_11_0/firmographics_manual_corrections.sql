/*
 * firmographics_manual_corrections.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/**************************************************************************
 * SQL for one-off update specific company attributes for specific domain *
 **************************************************************************/
update northstar.company set state = 'Illinois', city = 'Chicago', street = '100 S. Wacker Drive Suite 425', country = 'United States', employee_size = 463, linkedin_url = 'https://www.linkedin.com/company/silkroad' where domain = 'silkroad.com';
update northstar.company set name = 'Concur', state = 'Washington', city = 'Bellevue', street = '601 108th Ave NE #1000', industry = 'Computer Software', linkedin_url = 'www.linkedin.com/company/concur-technologies' where domain = 'concur.com';
update northstar.company set name = 'Formstack', industry = 'Internet', linkedin_url = 'www.linkedin.com/company/formstack' where domain = 'formstack.com';
update northstar.company set name = 'Tradeshift', image_url = 'https://media.licdn.com/mpr/mpr/shrink_200_200/AAEAAQAAAAAAAAiJAAAAJGY5OTZhZjdkLTgzNzctNDMwZC1iY2M5LTk3ODk0MzY4ZjQxZQ.png', industry = 'Information Technology and Services', street = '612 Howard Street, Suite 100', zipcode = '94105', linkedin_url = 'www.linkedin.com/company/tradeshift' where domain = 'tradeshift.com';
update northstar.company set name = 'Greenhouse Software', street = '110 Fifth Avenue', zipcode = '10011', industry = 'Computer Software', linkedin_url = 'www.linkedin.com/company/greenhouse-inc-' where domain = 'greenhouse.io';
update northstar.company set status = 'valid' where domain = 'finsync.com';

update northstar.company set city = 'Dallas', street = '2730 Irving Blvd', zipcode = '75207' where domain = 'andrewsdistributing.com';
update northstar.company set employee_size = 20000, revenue = 4300000 where domain = 'glazersbeer.com';
update northstar.company set employee_size = 25, revenue = 1000 where domain = 'silksf.com';

COMMIT;
