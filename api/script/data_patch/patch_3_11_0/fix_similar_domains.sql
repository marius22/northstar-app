/*
 * fix_similar_domains.sql
 * Copyright (C) 2017 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

/******************************************
 * SQL for deleting wrong similar domains *
 ******************************************/

delete from northstar.similar_domain where domain = 'abercrombie.com' and match = 'hollister.com';
delete from northstar.similar_domain where domain = 'hollister.com' and match = 'abercrombie.com';
update northstar.similar_domain set score = 1.00 where domain = 'abercrombie.com' and match = 'hollisterco.com';
update northstar.similar_domain set score = 1.00 where domain = 'hollisterco.com' and match = 'abercrombie.com';
