INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'acquisition strategies',
  'acquisition strategies',
  0.486165,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'agile performance management',
  'agile performance management',
  0.509028,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automated processes',
  'automated processes',
  0.450395,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automation',
  'automation',
  0.270148,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaborate',
  'collaborate',
  0.477303,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'events and webinars',
  'events and webinars',
  0.490160,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrated',
  'integrated',
  0.213547,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'pay',
  'pay',
  0.203700,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social talent management',
  'social talent management',
  0.444168,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cloud bookkeeping',
  'cloud bookkeeping',
  0.493161,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connecting buyers and suppliers',
  'connecting buyers and suppliers',
  0.268434,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global',
  'global',
  0.299386,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job distribution',
  'job distribution',
  0.521755,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'online purchase',
  'online purchase',
  0.453134,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payroll processing',
  'payroll processing',
  0.300985,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'brand and marketing',
  'brand and marketing',
  0.155846,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'electronic invoicing',
  'electronic invoicing',
  0.626192,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement',
  'employee engagement',
  0.626406,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial management and accounting',
  'financial management and accounting',
  0.583269,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'fully integrated',
  'fully integrated',
  0.237089,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'invoicing',
  'invoicing',
  0.286390,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'invoicing',
  'invoicing',
  0.696631,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruitment',
  'recruitment',
  0.213572,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier financing',
  'supplier financing',
  0.761064,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent acquisition strategies',
  'talent acquisition strategies',
  0.515639,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training',
  'training',
  0.193499,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'webinars',
  'webinars',
  0.319440,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based',
  'cloud-based',
  0.268094,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrated talent management',
  'integrated talent management',
  0.789497,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job',
  'job',
  0.195241,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management and hr',
  'management and hr',
  0.511474,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'portal',
  'portal',
  0.192192,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiters',
  'recruiters',
  0.289891,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accounting software',
  'accounting software',
  0.367849,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'buyers and suppliers',
  'buyers and suppliers',
  0.531831,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based technology',
  'cloud-based technology',
  0.448363,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee experience',
  'employee experience',
  0.511596,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'innovate',
  'innovate',
  0.201495,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruit',
  'recruit',
  0.203196,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategies',
  'strategies',
  0.257660,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'time and attendance',
  'time and attendance',
  0.302058,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'time tracking',
  'time tracking',
  0.327112,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'assessments and surveys',
  'assessments and surveys',
  0.307882,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee performance',
  'employee performance',
  0.540115,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engagement',
  'engagement',
  0.285090,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'form i-9',
  'form i-9',
  0.434111,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr activities',
  'hr activities',
  0.436812,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning',
  'learning',
  0.309012,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social recruiting strategy',
  'social recruiting strategy',
  0.491240,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'travel and expense management',
  'travel and expense management',
  0.160528,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business travel',
  'business travel',
  0.430718,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'career portal',
  'career portal',
  0.489131,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based platform',
  'cloud-based platform',
  0.421404,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr',
  'hr',
  1.000000,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human',
  'human',
  0.203792,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'kicking ass',
  'kicking ass',
  0.344814,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'media company',
  'media company',
  0.342832,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social recruiting',
  'social recruiting',
  0.654194,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent portal',
  'talent portal',
  0.826749,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement strategies',
  'employee engagement strategies',
  0.379553,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr managers',
  'hr managers',
  0.482834,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr software solutions',
  'hr software solutions',
  0.419344,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management',
  'learning management',
  0.802125,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance management solutions',
  'performance management solutions',
  0.370480,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social media innovations',
  'social media innovations',
  0.289691,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent strategies',
  'talent strategies',
  0.514239,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tracking',
  'tracking',
  0.199234,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual assistant',
  'virtual assistant',
  0.492018,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'accounts payable',
  'accounts payable',
  0.456203,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaboration',
  'collaboration',
  0.262858,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaboration platform',
  'collaboration platform',
  0.482642,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance',
  'compliance',
  0.255820,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'reducing risk',
  'reducing risk',
  0.417672,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'small',
  'small',
  0.165349,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'small business',
  'small business',
  0.464653,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic talent management',
  'strategic talent management',
  0.510016,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent',
  'talent',
  0.480383,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'chain',
  'chain',
  0.199089,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data management and reporting',
  'data management and reporting',
  0.156403,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement programs',
  'employee engagement programs',
  0.355749,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hiring',
  'hiring',
  0.303021,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'one platform',
  'one platform',
  0.623645,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'processes',
  'processes',
  0.225445,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procure to pay',
  'procure to pay',
  0.262688,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting metrics',
  'recruiting metrics',
  0.460477,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic services',
  'strategic services',
  0.452342,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier onboarding',
  'supplier onboarding',
  0.671771,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supply chain',
  'supply chain',
  0.381261,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent development',
  'talent development',
  0.580636,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management',
  'talent management',
  1.000000,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training and education',
  'training and education',
  0.298310,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'working capital optimization',
  'working capital optimization',
  0.498583,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'app',
  'app',
  0.233031,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud invoicing',
  'cloud invoicing',
  0.345398,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee data management',
  'employee data management',
  0.478633,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial',
  'financial',
  0.306003,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'integrated financial management',
  'integrated financial management',
  0.614763,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management systems',
  'learning management systems',
  0.452102,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'onboarding',
  'onboarding',
  0.576753,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'onboarding',
  'onboarding',
  0.274614,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payments',
  'payments',
  0.152288,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement strategy',
  'procurement strategy',
  0.506171,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resource management systems',
  'resource management systems',
  0.413927,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ap',
  'ap',
  0.268946,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bill pay',
  'bill pay',
  0.692828,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'build your own',
  'build your own',
  0.193591,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'business intelligence',
  'business intelligence',
  0.549093,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial management',
  'financial management',
  1.000000,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global talent',
  'global talent',
  0.595438,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource management',
  'human resource management',
  0.335614,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management solutions',
  'management solutions',
  0.552412,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'network',
  'network',
  0.185682,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance',
  'performance',
  0.222540,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier management',
  'supplier management',
  1.000000,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent analytics',
  'talent analytics',
  0.593337,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transportation and logistics',
  'transportation and logistics',
  0.160693,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'acquisition',
  'acquisition',
  0.258847,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'apps',
  'apps',
  0.332163,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'chat software',
  'chat software',
  0.355990,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cloud',
  'cloud',
  0.204161,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud',
  'cloud',
  0.264544,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'e-invoice',
  'e-invoice',
  0.169682,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'faster payments',
  'faster payments',
  0.467372,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'invoices',
  'invoices',
  0.384785,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'process and technology',
  'process and technology',
  0.250744,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'project manager',
  'project manager',
  0.377047,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'svp',
  'svp',
  0.190507,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'training and change management',
  'training and change management',
  0.254640,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'change management',
  'change management',
  0.350239,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connect',
  'connect',
  0.209680,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global talent acquisition',
  'global talent acquisition',
  0.528667,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr solutions',
  'hr solutions',
  0.534902,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hrms solutions',
  'hrms solutions',
  0.680448,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'improve processes',
  'improve processes',
  0.593550,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management and accounting',
  'management and accounting',
  0.546931,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management and reporting',
  'management and reporting',
  0.312455,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management platform',
  'management platform',
  0.412008,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manager',
  'manager',
  0.192435,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'payables',
  'payables',
  0.241206,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payroll',
  'payroll',
  0.483317,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting',
  'recruiting',
  0.474708,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting and hiring',
  'recruiting and hiring',
  0.370372,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'roll out',
  'roll out',
  0.184827,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'services and consulting',
  'services and consulting',
  0.444107,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social technology',
  'social technology',
  0.503648,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier master data',
  'supplier master data',
  0.772443,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'suppliers',
  'suppliers',
  0.499379,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management solutions',
  'talent management solutions',
  0.686764,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'accounting and finance',
  'accounting and finance',
  0.188340,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'banks',
  'banks',
  0.178140,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bill payments',
  'bill payments',
  0.598186,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'business',
  'business',
  0.206462,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business',
  'business',
  0.224246,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business case',
  'business case',
  0.539240,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hiring strategy',
  'hiring strategy',
  0.548371,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr software',
  'hr software',
  0.605764,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'webinars and events',
  'webinars and events',
  0.391407,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accountants',
  'accountants',
  0.452943,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'capital',
  'capital',
  0.188084,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning and talent management',
  'learning and talent management',
  0.189012,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'live chat software',
  'live chat software',
  0.304054,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'open platform',
  'open platform',
  0.632206,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social media company',
  'social media company',
  0.443517,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'software',
  'software',
  0.207732,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software',
  'software',
  0.223024,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ats software',
  'ats software',
  0.496757,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human capital management software',
  'human capital management software',
  0.174990,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'integration and testing',
  'integration and testing',
  0.177895,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance management',
  'performance management',
  0.738763,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'platform',
  'platform',
  0.281894,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'platform',
  'platform',
  0.380770,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'solutions',
  'solutions',
  0.197442,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'spend management',
  'spend management',
  0.883915,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management strategy',
  'talent management strategy',
  0.414496,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual',
  'virtual',
  0.214609,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accounting',
  'accounting',
  0.706482,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ap automation',
  'ap automation',
  0.814613,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bookkeeping',
  'bookkeeping',
  0.399519,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business and finance',
  'business and finance',
  0.256031,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'capital optimization',
  'capital optimization',
  0.539872,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud technology',
  'cloud technology',
  0.369521,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'digital platform',
  'digital platform',
  0.370480,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financials',
  'financials',
  0.558662,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hire',
  'hire',
  0.303781,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource management systems',
  'human resource management systems',
  0.437549,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management solutions',
  'learning management solutions',
  0.593947,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'manage',
  'manage',
  0.308542,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'manage',
  'manage',
  0.284053,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manage',
  'manage',
  0.282698,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'master data',
  'master data',
  0.444968,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'national health service',
  'national health service',
  0.389765,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new hire',
  'new hire',
  0.449362,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent acquisition',
  'talent acquisition',
  0.692872,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'working capital',
  'working capital',
  0.423189,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'chat',
  'chat',
  0.159737,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'developers',
  'developers',
  0.212329,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee onboarding',
  'employee onboarding',
  0.743655,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'enterprise',
  'enterprise',
  0.330279,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr professionals',
  'hr professionals',
  0.626387,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hrms',
  'hrms',
  0.266323,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management change',
  'management change',
  0.668043,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'banking',
  'banking',
  0.309216,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cash flow',
  'cash flow',
  0.214743,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee',
  'employee',
  0.314483,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'finance',
  'finance',
  0.277891,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'intelligence',
  'intelligence',
  0.241655,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management',
  'management',
  0.240708,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management',
  'management',
  0.273848,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'management',
  'management',
  0.233559,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'managers',
  'managers',
  0.224173,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement',
  'procurement',
  1.000000,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'screening',
  'screening',
  0.221725,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management software',
  'talent management software',
  0.506707,
  0.00
);

INSERT INTO northstar.business_tags_keyword
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual credit cards',
  'virtual credit cards',
  0.252769,
  0.00
);


COMMIT;
