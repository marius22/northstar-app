INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'others',
  'others',
  0.000000,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ats',
  'ats',
  0.000000,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'email',
  'email',
  0.002916,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'contact',
  'contact',
  0.003042,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'well',
  'well',
  0.004198,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'home',
  'home',
  0.004388,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'page',
  'page',
  0.004580,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'care',
  'care',
  0.005250,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'small',
  'small',
  0.005338,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'details',
  'details',
  0.005430,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'us',
  'us',
  0.005470,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'questions',
  'questions',
  0.005622,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'used',
  'used',
  0.005717,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'map',
  'map',
  0.005830,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cities',
  'cities',
  0.005877,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'schedule',
  'schedule',
  0.006540,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'rate',
  'rate',
  0.006621,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'quick',
  'quick',
  0.006800,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hands',
  'hands',
  0.006947,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'requirements',
  'requirements',
  0.006948,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'class',
  'class',
  0.006977,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'maintenance',
  'maintenance',
  0.007060,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'purchase',
  'purchase',
  0.007077,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'serving',
  'serving',
  0.007087,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'things',
  'things',
  0.007099,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'happy',
  'happy',
  0.007213,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'lot',
  'lot',
  0.007372,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'insurance',
  'insurance',
  0.007381,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'certified',
  'certified',
  0.007399,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'article',
  'article',
  0.007614,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'packages',
  'packages',
  0.007637,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'friends',
  'friends',
  0.007698,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'american',
  'american',
  0.007710,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'test',
  'test',
  0.007781,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'protection',
  'protection',
  0.007916,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reach',
  'reach',
  0.008034,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'profile',
  'profile',
  0.008152,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'documents',
  'documents',
  0.008311,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'release',
  'release',
  0.008410,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'clear',
  'clear',
  0.008436,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'stand',
  'stand',
  0.008459,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fill',
  'fill',
  0.008532,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'copyright',
  'copyright',
  0.008541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'worldwide',
  'worldwide',
  0.008561,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'rights',
  'rights',
  0.008726,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'superior',
  'superior',
  0.008765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'proven',
  'proven',
  0.008769,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'storage',
  'storage',
  0.008775,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'starting',
  'starting',
  0.008829,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fresh',
  'fresh',
  0.008958,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fees',
  'fees',
  0.009026,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'enhance',
  'enhance',
  0.009055,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'upgrades',
  'upgrades',
  0.009221,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'premier',
  'premier',
  0.009237,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'visitors',
  'visitors',
  0.009259,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'conference',
  'conference',
  0.009321,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'walks',
  'walks',
  0.009345,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hosts',
  'hosts',
  0.009382,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'specialized',
  'specialized',
  0.009391,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dynamic',
  'dynamic',
  0.009434,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'finish',
  'finish',
  0.009480,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'kit',
  'kit',
  0.009482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'representative',
  'representative',
  0.009531,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'accurate',
  'accurate',
  0.009541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'stress',
  'stress',
  0.009672,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reference',
  'reference',
  0.009738,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'target',
  'target',
  0.009765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'infrastructure',
  'infrastructure',
  0.009819,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'virtual',
  'virtual',
  0.009827,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'speak',
  'speak',
  0.009841,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'australia',
  'australia',
  0.009845,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'il',
  'il',
  0.009848,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fl',
  'fl',
  0.009950,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'regional',
  'regional',
  0.009969,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'housing',
  'housing',
  0.010048,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sales',
  'sales',
  0.010056,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ecommerce',
  'ecommerce',
  0.010077,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'set-up',
  'set-up',
  0.010079,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'spot',
  'spot',
  0.010084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'flow',
  'flow',
  0.010155,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'numbers',
  'numbers',
  0.010193,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'scale',
  'scale',
  0.010344,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'economic',
  'economic',
  0.010396,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'remote',
  'remote',
  0.010404,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'flat',
  'flat',
  0.010426,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ease',
  'ease',
  0.010443,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'address',
  'address',
  0.010465,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'staff',
  'staff',
  0.010626,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compare',
  'compare',
  0.010636,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'identity',
  'identity',
  0.010649,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ma',
  'ma',
  0.010657,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'entry',
  'entry',
  0.010735,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'view',
  'view',
  0.010748,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'extension',
  'extension',
  0.010834,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'approval',
  'approval',
  0.011051,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'privacy',
  'privacy',
  0.011084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'discussion',
  'discussion',
  0.011130,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'switches',
  'switches',
  0.011174,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'france',
  'france',
  0.011276,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'free',
  'free',
  0.011376,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'germany',
  'germany',
  0.011413,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'putting',
  'putting',
  0.011425,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'creativity',
  'creativity',
  0.011426,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'framework',
  'framework',
  0.011596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'standard',
  'standard',
  0.011597,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'illinois',
  'illinois',
  0.011606,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'elegant',
  'elegant',
  0.011637,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'signs',
  'signs',
  0.011640,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'intelligence',
  'intelligence',
  0.011702,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'robust',
  'robust',
  0.011846,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'nc',
  'nc',
  0.011865,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'nj',
  'nj',
  0.011907,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'metro',
  'metro',
  0.011913,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expansion',
  'expansion',
  0.011979,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'upload',
  'upload',
  0.012010,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'name',
  'name',
  0.012108,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'high',
  'high',
  0.012124,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'seamless',
  'seamless',
  0.012179,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tailor',
  'tailor',
  0.012233,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mass',
  'mass',
  0.012289,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'conventional',
  'conventional',
  0.012301,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customizable',
  'customizable',
  0.012423,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'commercial',
  'commercial',
  0.012463,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'japan',
  'japan',
  0.012546,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'area',
  'area',
  0.012648,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'paris',
  'paris',
  0.012727,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tape',
  'tape',
  0.012781,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'brokerage',
  'brokerage',
  0.012794,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'user-friendly',
  'user-friendly',
  0.012969,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'wonder',
  'wonder',
  0.013178,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'marriage',
  'marriage',
  0.013186,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'singapore',
  'singapore',
  0.013280,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sydney',
  'sydney',
  0.013326,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'transparent',
  'transparent',
  0.013417,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tm',
  'tm',
  0.013474,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'demonstration',
  'demonstration',
  0.013583,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'updates',
  'updates',
  0.013733,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'orientation',
  'orientation',
  0.013763,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'drains',
  'drains',
  0.013764,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'champion',
  'champion',
  0.013873,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sharp',
  'sharp',
  0.013919,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'step',
  'step',
  0.013999,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'salary',
  'salary',
  0.014094,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'inc.',
  'inc.',
  0.014201,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'simplicity',
  'simplicity',
  0.014322,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'calculations',
  'calculations',
  0.014353,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'card',
  'card',
  0.014520,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'benchmark',
  'benchmark',
  0.014535,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'day',
  'day',
  0.014573,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'passes',
  'passes',
  0.014584,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'advanced',
  'advanced',
  0.014598,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'styles',
  'styles',
  0.014616,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'specialists',
  'specialists',
  0.014655,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'xml',
  'xml',
  0.014682,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'aggregates',
  'aggregates',
  0.014743,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'model',
  'model',
  0.014849,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'quiz',
  'quiz',
  0.014861,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'inexpensive',
  'inexpensive',
  0.014932,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ibm',
  'ibm',
  0.015029,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technical',
  'technical',
  0.015055,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'choice',
  'choice',
  0.015058,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'logging',
  'logging',
  0.015103,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'agencies',
  'agencies',
  0.015104,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'jacksonville',
  'jacksonville',
  0.015291,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'routers',
  'routers',
  0.015377,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'intranets',
  'intranets',
  0.015430,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ebay',
  'ebay',
  0.015440,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'uptime',
  'uptime',
  0.015449,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dive',
  'dive',
  0.015527,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'laws',
  'laws',
  0.015538,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'competitive',
  'competitive',
  0.015649,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'login',
  'login',
  0.015911,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'good',
  'good',
  0.016043,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tokyo',
  'tokyo',
  0.016047,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'school',
  'school',
  0.016112,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'risk',
  'risk',
  0.016125,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'multinational',
  'multinational',
  0.016189,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'twitter',
  'twitter',
  0.016212,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'devices',
  'devices',
  0.016254,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'variety',
  'variety',
  0.016267,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'multilingual',
  'multilingual',
  0.016270,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'info',
  'info',
  0.016275,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'vulnerabilities',
  'vulnerabilities',
  0.016334,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'base',
  'base',
  0.016348,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'restructuring',
  'restructuring',
  0.016445,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'satisfaction',
  'satisfaction',
  0.016485,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'uncertainty',
  'uncertainty',
  0.016556,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'foundation',
  'foundation',
  0.016581,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'impact',
  'impact',
  0.016831,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'browser',
  'browser',
  0.016867,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'unique',
  'unique',
  0.016930,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'taxes',
  'taxes',
  0.016952,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'children',
  'children',
  0.016990,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'future',
  'future',
  0.017116,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'intrusion',
  'intrusion',
  0.017123,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'apis',
  'apis',
  0.017270,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'facebook',
  'facebook',
  0.017292,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'edmonton',
  'edmonton',
  0.017351,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'san',
  'san',
  0.017406,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'red',
  'red',
  0.017511,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'internet',
  'internet',
  0.017530,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'blog',
  'blog',
  0.017583,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'range',
  'range',
  0.017651,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'contractors',
  'contractors',
  0.018153,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'stage',
  'stage',
  0.018169,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'give',
  'give',
  0.018258,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'auckland',
  'auckland',
  0.018286,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ideas',
  'ideas',
  0.018392,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'group',
  'group',
  0.018468,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'improve',
  'improve',
  0.018521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'balance',
  'balance',
  0.018640,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'legal',
  'legal',
  0.018697,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'estate',
  'estate',
  0.018736,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'federal',
  'federal',
  0.018760,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'medical',
  'medical',
  0.018793,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'diverse',
  'diverse',
  0.018864,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'safe',
  'safe',
  0.018941,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interest',
  'interest',
  0.018946,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'full',
  'full',
  0.018969,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'seismic',
  'seismic',
  0.019021,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'less',
  'less',
  0.019240,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'directory',
  'directory',
  0.019305,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'middleware',
  'middleware',
  0.019363,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-site',
  'on-site',
  0.019366,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interactive',
  'interactive',
  0.019624,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'downsizing',
  'downsizing',
  0.019706,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'long-term',
  'long-term',
  0.019743,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'affordable',
  'affordable',
  0.019827,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'install',
  'install',
  0.019841,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'press',
  'press',
  0.019843,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'demand',
  'demand',
  0.019978,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'institution',
  'institution',
  0.020127,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'including',
  'including',
  0.020198,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'flexibility',
  'flexibility',
  0.020200,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'current',
  'current',
  0.020296,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'manual',
  'manual',
  0.020333,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'self',
  'self',
  0.020352,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'points',
  'points',
  0.020361,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'commitment',
  'commitment',
  0.020377,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'travel',
  'travel',
  0.020679,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'returns',
  'returns',
  0.020732,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dhs',
  'dhs',
  0.020866,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'much',
  'much',
  0.020910,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'outplacement',
  'outplacement',
  0.020947,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'adp',
  'adp',
  0.020951,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'load',
  'load',
  0.020954,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software-as-a-service',
  'software-as-a-service',
  0.020975,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'love',
  'love',
  0.021037,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'failover',
  'failover',
  0.021053,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'progress',
  'progress',
  0.021054,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'backups',
  'backups',
  0.021060,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'accessible',
  'accessible',
  0.021133,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'local',
  'local',
  0.021167,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'addition',
  'addition',
  0.021202,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'skills',
  'skills',
  0.021410,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'liability',
  'liability',
  0.021422,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'relations',
  'relations',
  0.021425,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'revenue',
  'revenue',
  0.021626,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'retail',
  'retail',
  0.021649,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'drug',
  'drug',
  0.021655,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'labor',
  'labor',
  0.021657,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'healthcare',
  'healthcare',
  0.021677,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'complex',
  'complex',
  0.021697,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reliability',
  'reliability',
  0.021710,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'techniques',
  'techniques',
  0.021773,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'experienced',
  'experienced',
  0.021796,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'microsoft',
  'microsoft',
  0.021845,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ca',
  'ca',
  0.021882,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'gauging',
  'gauging',
  0.021982,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'chicago',
  'chicago',
  0.022015,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'click',
  'click',
  0.022062,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'manufacturing',
  'manufacturing',
  0.022151,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'unions',
  'unions',
  0.022183,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'simple',
  'simple',
  0.022187,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'bank',
  'bank',
  0.022300,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'features',
  'features',
  0.022337,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'explore',
  'explore',
  0.022516,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'individual',
  'individual',
  0.022612,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'investment',
  'investment',
  0.022630,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'low',
  'low',
  0.022786,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'youtube',
  'youtube',
  0.022821,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'conversation',
  'conversation',
  0.022893,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'trail',
  'trail',
  0.022960,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'asset',
  'asset',
  0.023106,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'uk',
  'uk',
  0.023108,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'component',
  'component',
  0.023131,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'direct',
  'direct',
  0.023150,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'transform',
  'transform',
  0.023154,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'servers',
  'servers',
  0.023166,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'call',
  'call',
  0.023172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interactions',
  'interactions',
  0.023396,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'configuration',
  'configuration',
  0.023539,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'legacy',
  'legacy',
  0.023540,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'accredited',
  'accredited',
  0.023590,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'anywhere',
  'anywhere',
  0.023593,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'passion',
  'passion',
  0.023716,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dvd',
  'dvd',
  0.023721,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'issues',
  'issues',
  0.023756,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'subscription',
  'subscription',
  0.023769,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'edge',
  'edge',
  0.023782,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'match',
  'match',
  0.023825,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'eforms',
  'eforms',
  0.023875,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'incentives',
  'incentives',
  0.023958,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'shows',
  'shows',
  0.023964,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'achievement',
  'achievement',
  0.023993,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'trusted',
  'trusted',
  0.024048,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'health',
  'health',
  0.024103,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'add',
  'add',
  0.024112,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'response',
  'response',
  0.024166,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strength',
  'strength',
  0.024182,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'classroom',
  'classroom',
  0.024213,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hospital',
  'hospital',
  0.024378,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'meet',
  'meet',
  0.024444,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'high-performance',
  'high-performance',
  0.024462,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'involvement',
  'involvement',
  0.024823,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hardware',
  'hardware',
  0.024946,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'import',
  'import',
  0.025036,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'led',
  'led',
  0.025040,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'feed',
  'feed',
  0.025295,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'options',
  'options',
  0.025405,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'exciting',
  'exciting',
  0.025435,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'focus',
  'focus',
  0.025488,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'basis',
  'basis',
  0.025507,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'profit',
  'profit',
  0.025917,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'google',
  'google',
  0.025936,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'project',
  'project',
  0.026076,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'workflow',
  'workflow',
  0.026084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'pet',
  'pet',
  0.026117,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cutting',
  'cutting',
  0.026189,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'exit',
  'exit',
  0.026248,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'all sizes',
  'all sizes',
  0.026542,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tips',
  'tips',
  0.026571,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'back',
  'back',
  0.026598,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'pay-for-performance',
  'pay-for-performance',
  0.026620,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'place',
  'place',
  0.026759,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'face',
  'face',
  0.026949,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'designing',
  'designing',
  0.027021,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'canada',
  'canada',
  0.027179,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'paperwork',
  'paperwork',
  0.027587,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hong kong',
  'hong kong',
  0.027809,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'transforming',
  'transforming',
  0.027812,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consistency',
  'consistency',
  0.028085,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'boards',
  'boards',
  0.028176,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strong',
  'strong',
  0.028193,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'wealth',
  'wealth',
  0.028393,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ceos',
  'ceos',
  0.028394,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'better',
  'better',
  0.028573,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'optimization',
  'optimization',
  0.028620,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'economy',
  'economy',
  0.028776,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'vacancies',
  'vacancies',
  0.028792,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'office',
  'office',
  0.028820,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'real estate',
  'real estate',
  0.028921,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'rewards',
  'rewards',
  0.028945,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'french',
  'french',
  0.028957,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'annual',
  'annual',
  0.029062,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'generation',
  'generation',
  0.029064,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fast',
  'fast',
  0.029230,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'watch',
  'watch',
  0.029460,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'offers',
  'offers',
  0.029506,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'projections',
  'projections',
  0.029718,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'saas',
  'saas',
  0.029783,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'around the world',
  'around the world',
  0.029817,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'present',
  'present',
  0.029818,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reform',
  'reform',
  0.029867,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'behaviors',
  'behaviors',
  0.029951,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'english',
  'english',
  0.030003,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'work',
  'work',
  0.030037,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'feedback',
  'feedback',
  0.030081,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'linkedin',
  'linkedin',
  0.030082,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'firewalls',
  'firewalls',
  0.030174,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'configure',
  'configure',
  0.030221,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'honor',
  'honor',
  0.030226,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'workplace',
  'workplace',
  0.030317,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'alliances',
  'alliances',
  0.030356,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'location',
  'location',
  0.030439,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'abuse',
  'abuse',
  0.030484,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'optimize',
  'optimize',
  0.030591,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'score',
  'score',
  0.030613,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'disaster',
  'disaster',
  0.030643,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'real',
  'real',
  0.030716,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'outsource',
  'outsource',
  0.030722,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'leading',
  'leading',
  0.030765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'san francisco',
  'san francisco',
  0.030870,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'night',
  'night',
  0.030945,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expanding',
  'expanding',
  0.031080,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'record',
  'record',
  0.031116,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'run',
  'run',
  0.031172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'secure',
  'secure',
  0.031191,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reliable',
  'reliable',
  0.031249,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'editor',
  'editor',
  0.031274,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'criminal',
  'criminal',
  0.031295,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'authentication',
  'authentication',
  0.031344,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'emerging',
  'emerging',
  0.031354,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'export',
  'export',
  0.031456,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'download',
  'download',
  0.031561,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'potential',
  'potential',
  0.031578,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fastest',
  'fastest',
  0.031755,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'readiness',
  'readiness',
  0.031763,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'growing',
  'growing',
  0.031840,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'comparison',
  'comparison',
  0.031958,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'analyze',
  'analyze',
  0.032019,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'driving',
  'driving',
  0.032119,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'web',
  'web',
  0.032192,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'international',
  'international',
  0.032375,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'accounting',
  'accounting',
  0.032461,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'quality',
  'quality',
  0.032521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'active',
  'active',
  0.032573,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliant',
  'compliant',
  0.032579,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'still',
  'still',
  0.032615,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'complete',
  'complete',
  0.032635,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'verification',
  'verification',
  0.032638,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'table',
  'table',
  0.032677,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'path',
  'path',
  0.032685,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'personal',
  'personal',
  0.032984,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dish',
  'dish',
  0.033074,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sites',
  'sites',
  0.033192,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'organizational',
  'organizational',
  0.033193,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'predictive',
  'predictive',
  0.033224,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'knowledgeable',
  'knowledgeable',
  0.033499,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hedge',
  'hedge',
  0.033562,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'files',
  'files',
  0.033617,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'credit card',
  'credit card',
  0.033727,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'running',
  'running',
  0.033766,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'excellence',
  'excellence',
  0.033831,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'initiatives',
  'initiatives',
  0.034042,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'comprehensive',
  'comprehensive',
  0.034050,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'counsel',
  'counsel',
  0.034069,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ranking',
  'ranking',
  0.034118,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'measure',
  'measure',
  0.034278,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'japanese',
  'japanese',
  0.034397,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'build',
  'build',
  0.034428,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'critical',
  'critical',
  0.034480,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'complexity',
  'complexity',
  0.034617,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'effective',
  'effective',
  0.035350,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'flexible',
  'flexible',
  0.035393,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'identify',
  'identify',
  0.035555,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'discover',
  'discover',
  0.035794,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'motivation',
  'motivation',
  0.036057,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'first',
  'first',
  0.036334,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'line',
  'line',
  0.036336,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'leverage',
  'leverage',
  0.036460,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engineers',
  'engineers',
  0.036533,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'electronic',
  'electronic',
  0.036555,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'faster',
  'faster',
  0.036563,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'smart',
  'smart',
  0.036596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'category',
  'category',
  0.036725,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'deliver',
  'deliver',
  0.036796,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interoperability',
  'interoperability',
  0.036886,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'standardization',
  'standardization',
  0.036965,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'personalized',
  'personalized',
  0.037088,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'delivering',
  'delivering',
  0.037094,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'substance',
  'substance',
  0.037099,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'processing',
  'processing',
  0.037259,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'it staff',
  'it staff',
  0.037336,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'badge',
  'badge',
  0.037545,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'screen',
  'screen',
  0.037797,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tie',
  'tie',
  0.037939,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tap',
  'tap',
  0.038088,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'globe',
  'globe',
  0.038097,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'opportunities',
  'opportunities',
  0.038452,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'task',
  'task',
  0.038596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'productivity',
  'productivity',
  0.038617,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'make',
  'make',
  0.038687,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'insight',
  'insight',
  0.038744,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tactical',
  'tactical',
  0.038891,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'maintaining',
  'maintaining',
  0.038917,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'credit',
  'credit',
  0.038971,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'networking',
  'networking',
  0.038995,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'guide',
  'guide',
  0.039227,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'posting',
  'posting',
  0.039325,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'evaluation',
  'evaluation',
  0.039472,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'book',
  'book',
  0.039720,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'format',
  'format',
  0.039829,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ladders',
  'ladders',
  0.039938,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'linking',
  'linking',
  0.039948,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'open',
  'open',
  0.040047,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'podcasts',
  'podcasts',
  0.040075,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'communicate',
  'communicate',
  0.040119,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dip',
  'dip',
  0.040227,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'public',
  'public',
  0.040341,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'transitions',
  'transitions',
  0.040384,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'responsiveness',
  'responsiveness',
  0.040550,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'making',
  'making',
  0.041107,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'retain',
  'retain',
  0.041116,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'pre-sales',
  'pre-sales',
  0.041265,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'maintain',
  'maintain',
  0.041394,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'access',
  'access',
  0.041512,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'award-winning',
  'award-winning',
  0.041709,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'referrals',
  'referrals',
  0.041968,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hcm',
  'hcm',
  0.042229,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'prestige',
  'prestige',
  0.042749,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'frontier',
  'frontier',
  0.042765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'selecting',
  'selecting',
  0.042937,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'courseware',
  'courseware',
  0.043050,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrity',
  'integrity',
  0.043067,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'action',
  'action',
  0.043568,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'community',
  'community',
  0.043790,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'staffing',
  'staffing',
  0.043970,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'define',
  'define',
  0.044227,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'best',
  'best',
  0.044237,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'boost',
  'boost',
  0.044583,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'state',
  'state',
  0.044618,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'contribution',
  'contribution',
  0.044628,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'level',
  'level',
  0.044943,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'operating',
  'operating',
  0.045083,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'netsuite',
  'netsuite',
  0.045132,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'create',
  'create',
  0.045263,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'state and federal',
  'state and federal',
  0.045321,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'easy-to-use',
  'easy-to-use',
  0.045400,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'selling',
  'selling',
  0.045596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'commercial real estate',
  'commercial real estate',
  0.046172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'buying',
  'buying',
  0.046412,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hris',
  'hris',
  0.046502,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'forging',
  'forging',
  0.046654,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'enterprise',
  'enterprise',
  0.047026,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reduction',
  'reduction',
  0.047047,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer service',
  'customer service',
  0.047172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'operations',
  'operations',
  0.047293,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'programming',
  'programming',
  0.047390,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'funds',
  'funds',
  0.047547,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'helping',
  'helping',
  0.047560,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'central location',
  'central location',
  0.047606,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mutual funds',
  'mutual funds',
  0.047726,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'club',
  'club',
  0.047912,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'value',
  'value',
  0.048007,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'checking',
  'checking',
  0.048158,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'billable hours',
  'billable hours',
  0.048212,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'committed',
  'committed',
  0.048426,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'concur',
  'concur',
  0.048432,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'next generation',
  'next generation',
  0.048608,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engineered',
  'engineered',
  0.049574,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'needs',
  'needs',
  0.049689,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'certification',
  'certification',
  0.049708,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dedicated',
  'dedicated',
  0.049894,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'bartender',
  'bartender',
  0.049899,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'post',
  'post',
  0.049933,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'size',
  'size',
  0.050108,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'internal',
  'internal',
  0.050482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'public relations',
  'public relations',
  0.050541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'building',
  'building',
  0.050596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'lms',
  'lms',
  0.051026,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'communications',
  'communications',
  0.051029,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recovery',
  'recovery',
  0.051046,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'aspiration',
  'aspiration',
  0.051242,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'pto',
  'pto',
  0.051458,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'communicating',
  'communicating',
  0.051810,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sharing',
  'sharing',
  0.051856,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'thinking',
  'thinking',
  0.051871,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'practical',
  'practical',
  0.052616,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'attracting',
  'attracting',
  0.052646,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'money',
  'money',
  0.052826,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fits',
  'fits',
  0.052937,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'information',
  'information',
  0.052967,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'set',
  'set',
  0.053135,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'spirit',
  'spirit',
  0.053251,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'develops',
  'develops',
  0.053410,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'research',
  'research',
  0.053486,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'single sign-on',
  'single sign-on',
  0.053648,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'think',
  'think',
  0.053650,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'suites',
  'suites',
  0.053659,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'guest',
  'guest',
  0.053881,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'import and export',
  'import and export',
  0.053883,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial institution',
  'financial institution',
  0.054027,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'media',
  'media',
  0.054370,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'disaster recovery',
  'disaster recovery',
  0.054406,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'credit unions',
  'credit unions',
  0.054420,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'power',
  'power',
  0.054684,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'library',
  'library',
  0.054787,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ultimate',
  'ultimate',
  0.054838,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'center',
  'center',
  0.054966,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'old school',
  'old school',
  0.055183,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'clients',
  'clients',
  0.055186,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interviewing',
  'interviewing',
  0.055297,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cycle',
  'cycle',
  0.055595,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'selection',
  'selection',
  0.055749,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'increase efficiency',
  'increase efficiency',
  0.055779,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'core',
  'core',
  0.055901,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'deltek',
  'deltek',
  0.055974,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'content',
  'content',
  0.056009,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'audit',
  'audit',
  0.056058,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fast and easy',
  'fast and easy',
  0.056154,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'help',
  'help',
  0.056228,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'single',
  'single',
  0.056251,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'partnerships',
  'partnerships',
  0.056393,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'service providers',
  'service providers',
  0.056642,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'partner',
  'partner',
  0.056646,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'top',
  'top',
  0.056806,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'world',
  'world',
  0.057034,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'checklist',
  'checklist',
  0.057035,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reduce',
  'reduce',
  0.057061,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'showing',
  'showing',
  0.057063,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'operational',
  'operational',
  0.057181,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'working',
  'working',
  0.057201,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'buyers',
  'buyers',
  0.057365,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new zealand',
  'new zealand',
  0.057573,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'search',
  'search',
  0.058527,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'share',
  'share',
  0.058696,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'form',
  'form',
  0.059027,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'channels',
  'channels',
  0.059032,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'basic',
  'basic',
  0.059080,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'workforce',
  'workforce',
  0.059100,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'saving',
  'saving',
  0.059114,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hard drive',
  'hard drive',
  0.059117,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'latest technology',
  'latest technology',
  0.059188,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-demand',
  'on-demand',
  0.059260,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'activation',
  'activation',
  0.059273,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'budgets',
  'budgets',
  0.059462,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'custom',
  'custom',
  0.059563,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'knowledge and experience',
  'knowledge and experience',
  0.059690,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fully accredited',
  'fully accredited',
  0.059739,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'substance abuse',
  'substance abuse',
  0.059857,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'banking',
  'banking',
  0.059898,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'powerful',
  'powerful',
  0.059907,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'language',
  'language',
  0.060014,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'experience',
  'experience',
  0.060272,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'save time and money',
  'save time and money',
  0.060470,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'buy',
  'buy',
  0.060812,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'events',
  'events',
  0.061041,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud-based',
  'cloud-based',
  0.061579,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hedge funds',
  'hedge funds',
  0.061630,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'labor laws',
  'labor laws',
  0.061782,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'team of experts',
  'team of experts',
  0.061960,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'last minute',
  'last minute',
  0.062137,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'roi',
  'roi',
  0.062167,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recording',
  'recording',
  0.062388,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'in-depth knowledge',
  'in-depth knowledge',
  0.062854,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global leader',
  'global leader',
  0.063027,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-premise',
  'on-premise',
  0.063381,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'challenges',
  'challenges',
  0.063539,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'government',
  'government',
  0.063838,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'decisions',
  'decisions',
  0.063925,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ebooks',
  'ebooks',
  0.064079,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'architecture',
  'architecture',
  0.064116,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'results',
  'results',
  0.064162,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'managed',
  'managed',
  0.064603,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'find',
  'find',
  0.065050,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'brochures',
  'brochures',
  0.065351,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new',
  'new',
  0.065374,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'client relationships',
  'client relationships',
  0.065555,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'benefits',
  'benefits',
  0.065817,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'creating',
  'creating',
  0.065826,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'market',
  'market',
  0.066136,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'audit trail',
  'audit trail',
  0.067131,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performers',
  'performers',
  0.067354,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'user',
  'user',
  0.067807,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'sourcing',
  'sourcing',
  0.067939,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consultative',
  'consultative',
  0.068021,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'goals and objectives',
  'goals and objectives',
  0.068041,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'rapid',
  'rapid',
  0.068134,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'roles',
  'roles',
  0.068171,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'accounting firm',
  'accounting firm',
  0.068172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tools',
  'tools',
  0.068388,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'messaging',
  'messaging',
  0.068416,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'build relationships',
  'build relationships',
  0.068534,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'products',
  'products',
  0.068574,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'progressive',
  'progressive',
  0.068949,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'relationship',
  'relationship',
  0.069034,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'exchange',
  'exchange',
  0.069119,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'module',
  'module',
  0.069566,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'evaluate',
  'evaluate',
  0.069888,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'appraisals',
  'appraisals',
  0.069933,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'innovative',
  'innovative',
  0.070551,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'department of homeland security',
  'department of homeland security',
  0.070830,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'load balancers',
  'load balancers',
  0.070995,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'marketplace',
  'marketplace',
  0.071246,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online',
  'online',
  0.071487,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'fully integrated',
  'fully integrated',
  0.071959,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'knowledge',
  'knowledge',
  0.071995,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'predictive analytics',
  'predictive analytics',
  0.072111,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'source',
  'source',
  0.072305,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'network',
  'network',
  0.072841,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'webinars',
  'webinars',
  0.072890,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'moving',
  'moving',
  0.072957,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'professional development',
  'professional development',
  0.072977,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'key',
  'key',
  0.073099,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'basic needs',
  'basic needs',
  0.073607,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social networking',
  'social networking',
  0.073693,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'roll',
  'roll',
  0.073984,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'measurement',
  'measurement',
  0.074214,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'government contractors',
  'government contractors',
  0.074224,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'completion',
  'completion',
  0.074444,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate clients',
  'corporate clients',
  0.074787,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'growth',
  'growth',
  0.075217,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tradition',
  'tradition',
  0.075588,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'webinar',
  'webinar',
  0.075817,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'journey',
  'journey',
  0.075823,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'research report',
  'research report',
  0.075830,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'disaster recovery plan',
  'disaster recovery plan',
  0.075897,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'metrics',
  'metrics',
  0.076036,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'vendor',
  'vendor',
  0.076195,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'team',
  'team',
  0.076508,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'generate revenue',
  'generate revenue',
  0.076663,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'efficiency',
  'efficiency',
  0.076822,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ecosystem',
  'ecosystem',
  0.076963,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job opportunities',
  'job opportunities',
  0.077059,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'save',
  'save',
  0.077325,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'entrepreneurial',
  'entrepreneurial',
  0.077332,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'services',
  'services',
  0.077381,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'stories',
  'stories',
  0.077893,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'distribution channels',
  'distribution channels',
  0.077921,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'c-suite',
  'c-suite',
  0.077958,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'homeland security',
  'homeland security',
  0.078443,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'millenials',
  'millenials',
  0.078761,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'control',
  'control',
  0.078801,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'end-to-end',
  'end-to-end',
  0.079628,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'leadership roles',
  'leadership roles',
  0.079741,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'complex needs',
  'complex needs',
  0.079789,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'best people',
  'best people',
  0.079860,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'app',
  'app',
  0.079951,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'medical education',
  'medical education',
  0.080027,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'organizations',
  'organizations',
  0.080258,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'time',
  'time',
  0.080445,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'active directory',
  'active directory',
  0.080543,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mobile',
  'mobile',
  0.080702,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial sector',
  'financial sector',
  0.080705,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'public accounting firm',
  'public accounting firm',
  0.081160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'brand',
  'brand',
  0.081543,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consistent',
  'consistent',
  0.081830,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'distribution network',
  'distribution network',
  0.082207,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'plan',
  'plan',
  0.083156,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'easy',
  'easy',
  0.083161,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customized',
  'customized',
  0.083171,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'public accounting',
  'public accounting',
  0.083338,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'move',
  'move',
  0.083514,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consultative approach',
  'consultative approach',
  0.083672,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'practices',
  'practices',
  0.083733,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'firm',
  'firm',
  0.083954,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'one platform',
  'one platform',
  0.083959,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'actionable information',
  'actionable information',
  0.084145,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'roll out',
  'roll out',
  0.084349,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'web service',
  'web service',
  0.084361,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'criminal background checks',
  'criminal background checks',
  0.084800,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'set goals',
  'set goals',
  0.085119,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'deployment',
  'deployment',
  0.085295,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job site',
  'job site',
  0.085451,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social networking sites',
  'social networking sites',
  0.085489,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'for hire',
  'for hire',
  0.085725,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'payroll and taxes',
  'payroll and taxes',
  0.085880,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'updating',
  'updating',
  0.085880,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'companies',
  'companies',
  0.085972,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business solutions',
  'business solutions',
  0.086124,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'clo',
  'clo',
  0.086136,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'selection process',
  'selection process',
  0.086880,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engine',
  'engine',
  0.087339,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'saving time',
  'saving time',
  0.087418,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'industry',
  'industry',
  0.087491,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'culture',
  'culture',
  0.087724,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'education courses',
  'education courses',
  0.087867,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial services industry',
  'financial services industry',
  0.087966,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporation',
  'corporation',
  0.088285,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'white',
  'white',
  0.088362,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'select',
  'select',
  0.088439,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'functionality',
  'functionality',
  0.088830,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'competition',
  'competition',
  0.089651,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'provider',
  'provider',
  0.089740,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'adoption',
  'adoption',
  0.089823,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technical consulting',
  'technical consulting',
  0.089857,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'socal media',
  'socal media',
  0.090391,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'regulation',
  'regulation',
  0.090408,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'socialization',
  'socialization',
  0.090924,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'app marketplace',
  'app marketplace',
  0.090990,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'benefits administration',
  'benefits administration',
  0.091103,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consultants',
  'consultants',
  0.091158,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'professionals',
  'professionals',
  0.091416,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'life',
  'life',
  0.091523,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'content management',
  'content management',
  0.091858,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'helpful',
  'helpful',
  0.091936,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reporting',
  'reporting',
  0.091989,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job posting',
  'job posting',
  0.092089,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'blended',
  'blended',
  0.092160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic business',
  'strategic business',
  0.093098,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'criminal background',
  'criminal background',
  0.093129,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'network devices',
  'network devices',
  0.093167,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'attendance',
  'attendance',
  0.093455,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'services industry',
  'services industry',
  0.093488,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'outsource payroll',
  'outsource payroll',
  0.093596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'courses',
  'courses',
  0.093877,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'planning',
  'planning',
  0.094115,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resume',
  'resume',
  0.094267,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'exit interview',
  'exit interview',
  0.094521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'education',
  'education',
  0.094560,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connected',
  'connected',
  0.095216,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software products',
  'software products',
  0.095319,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'computing',
  'computing',
  0.095500,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expense',
  'expense',
  0.096123,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'review',
  'review',
  0.096250,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'change',
  'change',
  0.096403,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'traditional',
  'traditional',
  0.096419,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'industry certification',
  'industry certification',
  0.096440,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'practical help',
  'practical help',
  0.096524,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud services',
  'cloud services',
  0.096902,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'collaboration',
  'collaboration',
  0.097326,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software company',
  'software company',
  0.097729,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connecting people',
  'connecting people',
  0.097816,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tracking system',
  'tracking system',
  0.098118,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training and development',
  'training and development',
  0.098316,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'workflow processes',
  'workflow processes',
  0.098558,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer service and support',
  'customer service and support',
  0.098828,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job vacancies',
  'job vacancies',
  0.100190,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'support',
  'support',
  0.100336,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technology services',
  'technology services',
  0.100471,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'bottom line',
  'bottom line',
  0.100807,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-site training',
  'on-site training',
  0.101125,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'leaders',
  'leaders',
  0.101127,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expert',
  'expert',
  0.101523,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'applicant tracking system',
  'applicant tracking system',
  0.102258,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engaging',
  'engaging',
  0.102360,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'capital',
  'capital',
  0.102572,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'custom development',
  'custom development',
  0.102691,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'product experts',
  'product experts',
  0.102884,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-time',
  'on-time',
  0.102938,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'automated',
  'automated',
  0.103161,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'security controls',
  'security controls',
  0.103194,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'check',
  'check',
  0.103234,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'goal achievement',
  'goal achievement',
  0.103666,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'video',
  'video',
  0.103820,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engaged',
  'engaged',
  0.103859,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate',
  'corporate',
  0.104082,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'functions',
  'functions',
  0.104100,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruitment agencies',
  'recruitment agencies',
  0.105126,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'case',
  'case',
  0.105618,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'people',
  'people',
  0.105701,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'distribution',
  'distribution',
  0.105756,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'automation',
  'automation',
  0.106230,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrated solution',
  'integrated solution',
  0.106792,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'paperless',
  'paperless',
  0.107067,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'increase engagement',
  'increase engagement',
  0.107160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'export data',
  'export data',
  0.107165,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ebook',
  'ebook',
  0.107329,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate brand',
  'corporate brand',
  0.107743,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'demo',
  'demo',
  0.108095,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'enterprise software',
  'enterprise software',
  0.108461,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management tools',
  'management tools',
  0.108735,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategy development',
  'strategy development',
  0.109089,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'local employment',
  'local employment',
  0.109125,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training options',
  'training options',
  0.109432,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data integrity',
  'data integrity',
  0.109630,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connections',
  'connections',
  0.109831,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dedicated team',
  'dedicated team',
  0.109936,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'e-verify',
  'e-verify',
  0.110233,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'dvd or hard drive',
  'dvd or hard drive',
  0.110374,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'buying and selling',
  'buying and selling',
  0.110377,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning and development',
  'learning and development',
  0.110507,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business',
  'business',
  0.110789,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implementation specialists',
  'implementation specialists',
  0.110867,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connect',
  'connect',
  0.110883,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'retention',
  'retention',
  0.111047,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'next generation leadership',
  'next generation leadership',
  0.111081,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'administration',
  'administration',
  0.111121,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'trained',
  'trained',
  0.111172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'travel and expense',
  'travel and expense',
  0.111179,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'web 2.0',
  'web 2.0',
  0.111374,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implementing',
  'implementing',
  0.111383,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'banking reform',
  'banking reform',
  0.111738,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'solutions and services',
  'solutions and services',
  0.112115,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'managed services',
  'managed services',
  0.112556,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'enterprise agility',
  'enterprise agility',
  0.112566,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud architecture',
  'cloud architecture',
  0.112672,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'paper',
  'paper',
  0.112893,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud security',
  'cloud security',
  0.112984,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'drives',
  'drives',
  0.114255,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee satisfaction',
  'employee satisfaction',
  0.114320,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'costs',
  'costs',
  0.114440,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'no software',
  'no software',
  0.114467,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'effective training',
  'effective training',
  0.114478,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business management',
  'business management',
  0.114971,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'substance abuse screening',
  'substance abuse screening',
  0.115677,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'change management',
  'change management',
  0.115777,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'applications',
  'applications',
  0.115965,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'people and processes',
  'people and processes',
  0.116447,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'development',
  'development',
  0.116556,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connecting',
  'connecting',
  0.116776,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'consulting',
  'consulting',
  0.117092,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'apps',
  'apps',
  0.117427,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'video training',
  'video training',
  0.117579,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance regulations',
  'compliance regulations',
  0.117745,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social and mobile',
  'social and mobile',
  0.118174,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cost per hire',
  'cost per hire',
  0.118232,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'check out',
  'check out',
  0.118269,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'retention programs',
  'retention programs',
  0.118367,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'activities',
  'activities',
  0.119591,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software systems',
  'software systems',
  0.119997,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'life cycle',
  'life cycle',
  0.120068,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'studies',
  'studies',
  0.120156,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee retention',
  'employee retention',
  0.120406,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'secure authentication',
  'secure authentication',
  0.120732,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'payroll company',
  'payroll company',
  0.121194,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrating',
  'integrating',
  0.121471,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate moves',
  'corporate moves',
  0.121528,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training solution',
  'training solution',
  0.122831,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'professional services companies',
  'professional services companies',
  0.123010,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'save time',
  'save time',
  0.123098,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software as a service',
  'software as a service',
  0.123577,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'channel communications',
  'channel communications',
  0.123657,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'administrative',
  'administrative',
  0.123727,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial',
  'financial',
  0.123734,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance concerns',
  'compliance concerns',
  0.123913,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customers',
  'customers',
  0.123956,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic',
  'strategic',
  0.124198,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'career',
  'career',
  0.124495,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compensation',
  'compensation',
  0.124588,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business management software',
  'business management software',
  0.124836,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resume verification',
  'resume verification',
  0.124849,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'programs',
  'programs',
  0.125109,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning activities',
  'learning activities',
  0.125132,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'rapid growth',
  'rapid growth',
  0.125279,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technology help',
  'technology help',
  0.125338,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'security',
  'security',
  0.125374,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning platform',
  'learning platform',
  0.125482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software selection',
  'software selection',
  0.125809,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'process',
  'process',
  0.126419,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'branding',
  'branding',
  0.126529,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'perform',
  'perform',
  0.126571,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'managing',
  'managing',
  0.126782,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global',
  'global',
  0.126925,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'first impressions',
  'first impressions',
  0.127033,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'video interviewing',
  'video interviewing',
  0.127837,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employment background screening',
  'employment background screening',
  0.127962,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mergers',
  'mergers',
  0.128285,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'career community',
  'career community',
  0.128653,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee communication',
  'employee communication',
  0.128972,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online medical education',
  'online medical education',
  0.129603,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'brand and messaging',
  'brand and messaging',
  0.129828,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'finding',
  'finding',
  0.129847,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mobile learning',
  'mobile learning',
  0.130138,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'life events',
  'life events',
  0.130680,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'school technology',
  'school technology',
  0.131559,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'assess',
  'assess',
  0.131570,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'changing',
  'changing',
  0.132236,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource software',
  'human resource software',
  0.132782,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'response tracking',
  'response tracking',
  0.132909,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reduce costs',
  'reduce costs',
  0.133442,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruitment solutions',
  'recruitment solutions',
  0.133572,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social media',
  'social media',
  0.135221,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tracking solutions',
  'tracking solutions',
  0.135224,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'innovations',
  'innovations',
  0.136329,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technology optimization',
  'technology optimization',
  0.136395,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'leadership',
  'leadership',
  0.136552,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'survey',
  'survey',
  0.136959,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'video library',
  'video library',
  0.137295,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'no software to install',
  'no software to install',
  0.137403,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'background screening services',
  'background screening services',
  0.137911,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'successful',
  'successful',
  0.139013,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'search engine',
  'search engine',
  0.139465,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customized courses',
  'customized courses',
  0.139692,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'analytic',
  'analytic',
  0.139783,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new hire onboarding',
  'new hire onboarding',
  0.139991,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance tracking',
  'compliance tracking',
  0.140551,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'verification services',
  'verification services',
  0.140569,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'background checking',
  'background checking',
  0.141035,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'time and money',
  'time and money',
  0.141072,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'assessment',
  'assessment',
  0.141197,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'aligning technology',
  'aligning technology',
  0.141211,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new hire training',
  'new hire training',
  0.141341,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management processes',
  'management processes',
  0.142633,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software deployments',
  'software deployments',
  0.142832,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'systems',
  'systems',
  0.143745,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implement',
  'implement',
  0.145171,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social',
  'social',
  0.145325,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'company messaging',
  'company messaging',
  0.145717,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'goals',
  'goals',
  0.146728,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate learning',
  'corporate learning',
  0.147160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training and learning',
  'training and learning',
  0.147312,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learn',
  'learn',
  0.147495,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'legacy software',
  'legacy software',
  0.148003,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'higher education',
  'higher education',
  0.148110,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource technology',
  'human resource technology',
  0.148140,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mergers and acquisition',
  'mergers and acquisition',
  0.148301,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new employee onboarding',
  'new employee onboarding',
  0.148836,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance management',
  'compliance management',
  0.149676,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employment',
  'employment',
  0.149944,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'report',
  'report',
  0.150257,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'millennials',
  'millennials',
  0.150300,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cost reduction',
  'cost reduction',
  0.150504,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'c suite',
  'c suite',
  0.151739,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer experience',
  'customer experience',
  0.151906,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'quality of hire',
  'quality of hire',
  0.152886,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr compliance',
  'hr compliance',
  0.154090,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'develop',
  'develop',
  0.154114,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'agile',
  'agile',
  0.154401,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee commitment',
  'employee commitment',
  0.154493,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expense management',
  'expense management',
  0.154540,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'self-service',
  'self-service',
  0.155140,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technology',
  'technology',
  0.155610,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data management and reporting',
  'data management and reporting',
  0.156403,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'capital expenditure',
  'capital expenditure',
  0.156771,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management controls',
  'management controls',
  0.156888,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'entrepreneurial spirit',
  'entrepreneurial spirit',
  0.156977,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employment compliance',
  'employment compliance',
  0.157119,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'track',
  'track',
  0.157256,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'assessment solutions',
  'assessment solutions',
  0.158712,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr management',
  'hr management',
  0.159177,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implementation',
  'implementation',
  0.159443,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'people data',
  'people data',
  0.160030,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'travel and expense management',
  'travel and expense management',
  0.160528,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'background',
  'background',
  0.160947,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online recruitment solutions',
  'online recruitment solutions',
  0.161185,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employers',
  'employers',
  0.161801,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management system',
  'talent management system',
  0.162038,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'interviews',
  'interviews',
  0.162763,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'candidates',
  'candidates',
  0.163069,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'platform',
  'platform',
  0.163442,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integration',
  'integration',
  0.163780,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'smart hiring',
  'smart hiring',
  0.164299,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expense management solutions',
  'expense management solutions',
  0.164324,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resources',
  'resources',
  0.164427,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management costs',
  'management costs',
  0.164504,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'analytics',
  'analytics',
  0.164516,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data',
  'data',
  0.164522,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business objectives',
  'business objectives',
  0.164810,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee performance management',
  'employee performance management',
  0.165192,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data exchange',
  'data exchange',
  0.165294,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'knowledge sharing',
  'knowledge sharing',
  0.167135,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting platform',
  'recruiting platform',
  0.167202,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruitment system',
  'recruitment system',
  0.167582,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'deploy',
  'deploy',
  0.167627,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engage',
  'engage',
  0.167877,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr leadership',
  'hr leadership',
  0.168773,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'payroll',
  'payroll',
  0.169306,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'big data',
  'big data',
  0.169524,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'administrator',
  'administrator',
  0.169817,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'success',
  'success',
  0.170098,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent selection',
  'talent selection',
  0.170954,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'onboarding software',
  'onboarding software',
  0.171069,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'book club',
  'book club',
  0.173154,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'health screening',
  'health screening',
  0.173376,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer support',
  'customer support',
  0.173508,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hcm solutions',
  'hcm solutions',
  0.173750,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'innovative thinking',
  'innovative thinking',
  0.174463,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human capital management software',
  'human capital management software',
  0.174990,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'systems integration',
  'systems integration',
  0.176290,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting and onboarding',
  'recruiting and onboarding',
  0.177150,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr applications',
  'hr applications',
  0.177169,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'agility',
  'agility',
  0.177921,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'succession',
  'succession',
  0.179379,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business value',
  'business value',
  0.180787,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee onboarding software',
  'employee onboarding software',
  0.181046,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human capital',
  'human capital',
  0.181409,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job boards',
  'job boards',
  0.183084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new technologies',
  'new technologies',
  0.183537,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'selling online',
  'selling online',
  0.184623,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'productive',
  'productive',
  0.186050,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrate',
  'integrate',
  0.186821,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr systems integration',
  'hr systems integration',
  0.187544,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compensation management',
  'compensation management',
  0.188977,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning and talent management',
  'learning and talent management',
  0.189012,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr administrators',
  'hr administrators',
  0.189329,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online training',
  'online training',
  0.189552,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'portal',
  'portal',
  0.192192,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training',
  'training',
  0.193499,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'guest post',
  'guest post',
  0.194858,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job',
  'job',
  0.195241,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resume review',
  'resume review',
  0.196121,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'government regulation',
  'government regulation',
  0.197418,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'solutions',
  'solutions',
  0.197442,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud solution',
  'cloud solution',
  0.197957,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'tracking',
  'tracking',
  0.199234,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent branding',
  'talent branding',
  0.199564,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new world',
  'new world',
  0.199663,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'goal setting',
  'goal setting',
  0.199708,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'connect people',
  'connect people',
  0.199908,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruit',
  'recruit',
  0.203196,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human',
  'human',
  0.203792,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'electronic form i-9',
  'electronic form i-9',
  0.207558,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training courses',
  'training courses',
  0.207688,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'find jobs',
  'find jobs',
  0.208878,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'background checks',
  'background checks',
  0.209280,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer stories',
  'customer stories',
  0.209669,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'career sites',
  'career sites',
  0.211243,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'more customers',
  'more customers',
  0.212519,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrated',
  'integrated',
  0.213547,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruitment',
  'recruitment',
  0.213572,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'collaboration and content',
  'collaboration and content',
  0.213929,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online training courses',
  'online training courses',
  0.216228,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'company culture',
  'company culture',
  0.217655,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'demo video',
  'demo video',
  0.218549,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting management',
  'recruiting management',
  0.220842,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'screening',
  'screening',
  0.221725,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compensation planning',
  'compensation planning',
  0.222100,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance',
  'performance',
  0.222540,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software',
  'software',
  0.223024,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'managers',
  'managers',
  0.224173,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job search engine',
  'job search engine',
  0.225270,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'success stories',
  'success stories',
  0.226803,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training programs',
  'training programs',
  0.227023,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business decisions',
  'business decisions',
  0.229512,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cost control',
  'cost control',
  0.229934,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management team',
  'management team',
  0.231507,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'white paper',
  'white paper',
  0.231559,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'technology solution',
  'technology solution',
  0.232099,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'work events',
  'work events',
  0.232200,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial industry',
  'financial industry',
  0.232487,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'user adoption',
  'user adoption',
  0.232915,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human capital management',
  'human capital management',
  0.234756,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business strategy',
  'business strategy',
  0.235266,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance tools',
  'compliance tools',
  0.236993,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'open jobs',
  'open jobs',
  0.237521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'case studies',
  'case studies',
  0.238231,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'on-time performance',
  'on-time performance',
  0.238647,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employment background checks',
  'employment background checks',
  0.238694,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud platform',
  'cloud platform',
  0.239905,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social collaboration',
  'social collaboration',
  0.240604,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'succession planning',
  'succession planning',
  0.242843,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'reducing costs',
  'reducing costs',
  0.243484,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning programs',
  'learning programs',
  0.246151,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance',
  'compliance',
  0.255820,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business growth',
  'business growth',
  0.255848,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'media innovations',
  'media innovations',
  0.255906,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategies',
  'strategies',
  0.257660,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'acquisition',
  'acquisition',
  0.258847,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'candidate experience',
  'candidate experience',
  0.259809,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud computing',
  'cloud computing',
  0.260292,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job search',
  'job search',
  0.261668,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud',
  'cloud',
  0.264544,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implementation and support',
  'implementation and support',
  0.265782,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hrms',
  'hrms',
  0.266323,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'capital management',
  'capital management',
  0.266788,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'i-9',
  'i-9',
  0.269394,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'corporate training',
  'corporate training',
  0.269882,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer success',
  'customer success',
  0.272773,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management',
  'management',
  0.273848,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business goals',
  'business goals',
  0.274231,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'software solutions',
  'software solutions',
  0.274691,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud software',
  'cloud software',
  0.277160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'online recruitment',
  'online recruitment',
  0.279230,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'screening services',
  'screening services',
  0.281296,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'cloud technology',
  'cloud technology',
  0.281657,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resource center',
  'resource center',
  0.281748,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'information solutions',
  'information solutions',
  0.283750,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'manage',
  'manage',
  0.284053,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'professional services firms',
  'professional services firms',
  0.284530,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engagement',
  'engagement',
  0.285090,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr needs',
  'hr needs',
  0.285340,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance programs',
  'compliance programs',
  0.285757,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'professional services',
  'professional services',
  0.288112,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business results',
  'business results',
  0.289228,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social media innovations',
  'social media innovations',
  0.289691,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiters',
  'recruiters',
  0.289891,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'measurement solutions',
  'measurement solutions',
  0.290203,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee referrals',
  'employee referrals',
  0.290778,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'world-class service',
  'world-class service',
  0.295947,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'training and education',
  'training and education',
  0.298310,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'implementation services',
  'implementation services',
  0.301389,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'time and attendance',
  'time and attendance',
  0.302058,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'customer onboarding',
  'customer onboarding',
  0.302434,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hiring',
  'hiring',
  0.303021,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hire',
  'hire',
  0.303781,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr information',
  'hr information',
  0.304689,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'financial services',
  'financial services',
  0.307054,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'assessments and surveys',
  'assessments and surveys',
  0.307882,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'people strategy',
  'people strategy',
  0.308747,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning',
  'learning',
  0.309012,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'core hr',
  'core hr',
  0.309714,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management applications',
  'management applications',
  0.311484,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management and reporting',
  'management and reporting',
  0.312455,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee',
  'employee',
  0.314483,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'paperless solutions',
  'paperless solutions',
  0.321296,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employer brand',
  'employer brand',
  0.325788,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'easy to deploy',
  'easy to deploy',
  0.328545,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource management',
  'human resource management',
  0.335614,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'blended learning',
  'blended learning',
  0.335808,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management strategy',
  'management strategy',
  0.338422,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'company performance',
  'company performance',
  0.341186,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee information',
  'employee information',
  0.341743,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning supports',
  'learning supports',
  0.341989,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engagement strategies',
  'engagement strategies',
  0.347352,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resources',
  'human resources',
  0.353392,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job seekers',
  'job seekers',
  0.355522,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement programs',
  'employee engagement programs',
  0.355749,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'engagement programs',
  'engagement programs',
  0.357359,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'traditional training',
  'traditional training',
  0.358304,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr automation',
  'hr automation',
  0.358439,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'applicant tracking',
  'applicant tracking',
  0.360463,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'millennial leadership',
  'millennial leadership',
  0.362541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'data management',
  'data management',
  0.362707,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resource management',
  'resource management',
  0.363541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'compliance systems',
  'compliance systems',
  0.369011,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting and hiring',
  'recruiting and hiring',
  0.370372,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance management solutions',
  'performance management solutions',
  0.370480,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'background screening',
  'background screening',
  0.373014,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'service and support',
  'service and support',
  0.376482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employment branding',
  'employment branding',
  0.376762,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement strategies',
  'employee engagement strategies',
  0.379553,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance review',
  'performance review',
  0.384754,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr systems',
  'hr systems',
  0.387039,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business performance',
  'business performance',
  0.387588,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management software',
  'management software',
  0.388552,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hiring process',
  'hiring process',
  0.389440,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'expert services',
  'expert services',
  0.391609,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'top talent',
  'top talent',
  0.395139,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management platform',
  'management platform',
  0.397209,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hire employee',
  'hire employee',
  0.400182,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'mobile recruiting',
  'mobile recruiting',
  0.401243,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent measurement',
  'talent measurement',
  0.404121,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr strategies',
  'hr strategies',
  0.409024,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'resource management systems',
  'resource management systems',
  0.413927,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management strategy',
  'talent management strategy',
  0.414496,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job market',
  'job market',
  0.414608,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr software solutions',
  'hr software solutions',
  0.419344,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr resource',
  'hr resource',
  0.420360,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management program',
  'management program',
  0.424526,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting strategy',
  'recruiting strategy',
  0.425534,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'business success',
  'business success',
  0.425928,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social talent',
  'social talent',
  0.426638,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'form i-9',
  'form i-9',
  0.434111,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr activities',
  'hr activities',
  0.436812,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'human resource management systems',
  'human resource management systems',
  0.437549,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'services and consulting',
  'services and consulting',
  0.444107,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social talent management',
  'social talent management',
  0.444168,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'new hire',
  'new hire',
  0.449362,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management systems',
  'learning management systems',
  0.452102,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic services',
  'strategic services',
  0.452342,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting metrics',
  'recruiting metrics',
  0.460477,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'recruiting',
  'recruiting',
  0.474708,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee data management',
  'employee data management',
  0.478633,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent',
  'talent',
  0.480383,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr managers',
  'hr managers',
  0.482834,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'acquisition strategies',
  'acquisition strategies',
  0.486165,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'career portal',
  'career portal',
  0.489131,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social recruiting strategy',
  'social recruiting strategy',
  0.491240,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'ats software',
  'ats software',
  0.496757,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management software',
  'talent management software',
  0.506707,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'agile performance management',
  'agile performance management',
  0.509028,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'strategic talent management',
  'strategic talent management',
  0.510016,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management and hr',
  'management and hr',
  0.511474,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee experience',
  'employee experience',
  0.511596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent strategies',
  'talent strategies',
  0.514239,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent acquisition strategies',
  'talent acquisition strategies',
  0.515639,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'job distribution',
  'job distribution',
  0.521755,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global talent acquisition',
  'global talent acquisition',
  0.528667,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr solutions',
  'hr solutions',
  0.534902,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee performance',
  'employee performance',
  0.540115,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr',
  'hr',
  0.546944,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hiring strategy',
  'hiring strategy',
  0.548371,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management solutions',
  'management solutions',
  0.552412,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'onboarding',
  'onboarding',
  0.576753,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent development',
  'talent development',
  0.580636,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent analytics',
  'talent analytics',
  0.593337,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'contact',
  'contact',
  0.006666,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'time',
  'time',
  0.007401,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'home',
  'home',
  0.007482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'clients',
  'clients',
  0.009326,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'including',
  'including',
  0.009924,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'group',
  'group',
  0.011288,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'staff',
  'staff',
  0.011642,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'complete',
  'complete',
  0.011918,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'plan',
  'plan',
  0.012188,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'questions',
  'questions',
  0.012320,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'us',
  'us',
  0.012594,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'opportunity',
  'opportunity',
  0.012682,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'commercial',
  'commercial',
  0.013654,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'review',
  'review',
  0.013818,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'history',
  'history',
  0.014420,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'focus',
  'focus',
  0.014669,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'add',
  'add',
  0.014738,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'effective',
  'effective',
  0.014982,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'relationships',
  'relationships',
  0.015178,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'board',
  'board',
  0.015435,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'agency',
  'agency',
  0.016548,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'advice',
  'advice',
  0.016688,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'consulting',
  'consulting',
  0.016847,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'secure',
  'secure',
  0.017086,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'air',
  'air',
  0.017122,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'track',
  'track',
  0.017189,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'needs',
  'needs',
  0.017407,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'guarantees',
  'guarantees',
  0.017432,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'attention',
  'attention',
  0.017455,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'america',
  'america',
  0.017638,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'growth',
  'growth',
  0.017785,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'watch',
  'watch',
  0.018007,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'modern',
  'modern',
  0.018032,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'documents',
  'documents',
  0.018211,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'tour',
  'tour',
  0.018238,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'series',
  'series',
  0.018349,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'whole',
  'whole',
  0.018357,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'well',
  'well',
  0.018401,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'independent',
  'independent',
  0.018413,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'dream',
  'dream',
  0.018683,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'copyright',
  'copyright',
  0.018717,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'active',
  'active',
  0.018746,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'night',
  'night',
  0.018914,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'feedback',
  'feedback',
  0.019842,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'walk',
  'walk',
  0.020478,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'click',
  'click',
  0.020569,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'web',
  'web',
  0.021113,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'exchange',
  'exchange',
  0.021123,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'van',
  'van',
  0.021136,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'tasks',
  'tasks',
  0.021143,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'day',
  'day',
  0.021288,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'forum',
  'forum',
  0.021504,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'tech',
  'tech',
  0.021512,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'investors',
  'investors',
  0.021868,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'call',
  'call',
  0.021875,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'handling',
  'handling',
  0.021916,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'thinking',
  'thinking',
  0.021985,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'quality',
  'quality',
  0.022087,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sponsor',
  'sponsor',
  0.022145,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'rules',
  'rules',
  0.022326,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'community',
  'community',
  0.022348,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'new',
  'new',
  0.022573,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'expenses',
  'expenses',
  0.022728,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'winning',
  'winning',
  0.022737,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'bridge',
  'bridge',
  0.022861,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'joint',
  'joint',
  0.023022,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'order',
  'order',
  0.023031,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'mean',
  'mean',
  0.023084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sustainable',
  'sustainable',
  0.023425,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'first',
  'first',
  0.023809,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'instant',
  'instant',
  0.023886,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'nice',
  'nice',
  0.023996,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'approvals',
  'approvals',
  0.024216,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'individuals',
  'individuals',
  0.024774,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'rapid',
  'rapid',
  0.024882,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'best',
  'best',
  0.024925,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'enabling',
  'enabling',
  0.025176,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'stream',
  'stream',
  0.025499,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'find',
  'find',
  0.025522,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'entrepreneurs',
  'entrepreneurs',
  0.025746,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'saw',
  'saw',
  0.026115,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'joining',
  'joining',
  0.026273,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'training',
  'training',
  0.026365,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'make',
  'make',
  0.026457,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'benefits',
  'benefits',
  0.026759,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'fastest',
  'fastest',
  0.026918,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'offices',
  'offices',
  0.027207,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'international',
  'international',
  0.027444,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'back',
  'back',
  0.027596,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'blog',
  'blog',
  0.027781,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'options',
  'options',
  0.027835,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'mission',
  'mission',
  0.027928,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'brand',
  'brand',
  0.028254,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'help',
  'help',
  0.028269,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'knowledge',
  'knowledge',
  0.028591,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'arena',
  'arena',
  0.029011,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'information',
  'information',
  0.029070,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'meet',
  'meet',
  0.029456,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collective',
  'collective',
  0.029484,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'private',
  'private',
  0.029502,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'high-tech',
  'high-tech',
  0.029598,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'adoption',
  'adoption',
  0.029624,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'internet',
  'internet',
  0.029720,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'open-source',
  'open-source',
  0.029728,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'money',
  'money',
  0.029853,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'power',
  'power',
  0.029956,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'operations',
  'operations',
  0.031017,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'check out',
  'check out',
  0.031074,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'things',
  'things',
  0.031114,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'give',
  'give',
  0.031133,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'privacy',
  'privacy',
  0.031212,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'erp',
  'erp',
  0.031461,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'story',
  'story',
  0.031669,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'metrics',
  'metrics',
  0.032227,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'product',
  'product',
  0.032429,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'select',
  'select',
  0.032545,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'support',
  'support',
  0.032841,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'better',
  'better',
  0.032983,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'size',
  'size',
  0.033053,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'still',
  'still',
  0.033072,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'gm',
  'gm',
  0.033911,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'wired',
  'wired',
  0.033933,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'director',
  'director',
  0.033986,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'experts',
  'experts',
  0.034026,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'policy',
  'policy',
  0.034067,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'top',
  'top',
  0.034204,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'competitive',
  'competitive',
  0.034291,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'jobs',
  'jobs',
  0.034384,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'learn',
  'learn',
  0.034667,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'bet',
  'bet',
  0.034739,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'appstore',
  'appstore',
  0.034974,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'play',
  'play',
  0.034992,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'reach',
  'reach',
  0.035211,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manufacturer',
  'manufacturer',
  0.035427,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'eng',
  'eng',
  0.035714,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cfo',
  'cfo',
  0.035998,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'circular',
  'circular',
  0.036069,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'government',
  'government',
  0.036076,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'asp',
  'asp',
  0.036102,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'savings',
  'savings',
  0.036132,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'york',
  'york',
  0.036298,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'complex',
  'complex',
  0.036785,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'trade',
  'trade',
  0.037126,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'future',
  'future',
  0.037505,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'place',
  'place',
  0.037675,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'send',
  'send',
  0.037908,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'testing',
  'testing',
  0.038190,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'guide',
  'guide',
  0.039369,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cto',
  'cto',
  0.039887,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'methods',
  'methods',
  0.040704,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cost-effective',
  'cost-effective',
  0.040932,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'people',
  'people',
  0.041195,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'workshop',
  'workshop',
  0.041208,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'single',
  'single',
  0.041842,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'integration',
  'integration',
  0.042274,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'insights',
  'insights',
  0.042449,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'maintaining',
  'maintaining',
  0.042638,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'choice',
  'choice',
  0.042647,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'download',
  'download',
  0.042871,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'short',
  'short',
  0.043408,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'profit',
  'profit',
  0.043938,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'leading',
  'leading',
  0.044110,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'owner',
  'owner',
  0.044137,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'goals',
  'goals',
  0.044175,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'tungsten',
  'tungsten',
  0.044180,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'vision',
  'vision',
  0.044322,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'london',
  'london',
  0.044342,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manual',
  'manual',
  0.044556,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'efficient',
  'efficient',
  0.045082,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'smbs',
  'smbs',
  0.046463,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'implement',
  'implement',
  0.046611,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'hosted',
  'hosted',
  0.046896,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'check',
  'check',
  0.047055,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'contract',
  'contract',
  0.047488,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'adm',
  'adm',
  0.047592,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'talk',
  'talk',
  0.047884,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ceo',
  'ceo',
  0.048139,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'personal',
  'personal',
  0.048184,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'french',
  'french',
  0.049093,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ventures',
  'ventures',
  0.049658,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'creating',
  'creating',
  0.050060,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'press',
  'press',
  0.050461,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'garage',
  'garage',
  0.050613,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'systems',
  'systems',
  0.050679,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'configuration',
  'configuration',
  0.051580,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'anywhere',
  'anywhere',
  0.051698,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'e-procurement',
  'e-procurement',
  0.052724,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'equity',
  'equity',
  0.052936,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'wind',
  'wind',
  0.053529,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'hr',
  'hr',
  0.053673,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'productive',
  'productive',
  0.054542,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'marketing',
  'marketing',
  0.054586,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automobile',
  'automobile',
  0.054635,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transportation',
  'transportation',
  0.055054,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transactions',
  'transactions',
  0.055139,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'paris',
  'paris',
  0.055778,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'deliver',
  'deliver',
  0.055910,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'europe',
  'europe',
  0.055991,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'new york',
  'new york',
  0.056114,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'journey',
  'journey',
  0.056401,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sharing',
  'sharing',
  0.056814,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ariba',
  'ariba',
  0.057003,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'signing',
  'signing',
  0.057452,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'e-invoicing',
  'e-invoicing',
  0.057514,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'answers',
  'answers',
  0.057594,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'shared',
  'shared',
  0.057675,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'innovation',
  'innovation',
  0.057840,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'plug',
  'plug',
  0.058068,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'flexible',
  'flexible',
  0.058165,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'functionality',
  'functionality',
  0.058594,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'purchasing',
  'purchasing',
  0.058901,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'buy',
  'buy',
  0.059082,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transit',
  'transit',
  0.060063,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'financial',
  'financial',
  0.060503,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'f&a',
  'f&a',
  0.060704,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'telecom',
  'telecom',
  0.061260,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'steps',
  'steps',
  0.061352,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'professional',
  'professional',
  0.063862,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'industry',
  'industry',
  0.064172,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'vp',
  'vp',
  0.064757,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'capture',
  'capture',
  0.065310,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'configure',
  'configure',
  0.066221,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'logistics',
  'logistics',
  0.066311,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'web based',
  'web based',
  0.067378,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'europa',
  'europa',
  0.067987,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'health',
  'health',
  0.068264,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'videos',
  'videos',
  0.068931,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sailing',
  'sailing',
  0.069132,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'tokyo',
  'tokyo',
  0.070327,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'team',
  'team',
  0.073135,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'growing',
  'growing',
  0.073298,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'online',
  'online',
  0.073823,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'workflow',
  'workflow',
  0.073874,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'roles',
  'roles',
  0.074689,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'compliance',
  'compliance',
  0.074881,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'center',
  'center',
  0.075161,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'engineering',
  'engineering',
  0.075735,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'san',
  'san',
  0.076283,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'development',
  'development',
  0.076461,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'reports',
  'reports',
  0.076465,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'national',
  'national',
  0.076733,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'services',
  'services',
  0.076851,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'danish',
  'danish',
  0.077567,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'engine',
  'engine',
  0.077781,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connecting',
  'connecting',
  0.077998,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'accounting',
  'accounting',
  0.078762,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'powerful',
  'powerful',
  0.079033,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'empowering',
  'empowering',
  0.079170,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'customer',
  'customer',
  0.080260,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'delivery',
  'delivery',
  0.081109,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'improve',
  'improve',
  0.081169,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'agile',
  'agile',
  0.081317,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business-to-business',
  'business-to-business',
  0.081817,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'smb',
  'smb',
  0.082040,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cards',
  'cards',
  0.082249,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'competitive advantage',
  'competitive advantage',
  0.082772,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'copenhagen',
  'copenhagen',
  0.083108,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'payments',
  'payments',
  0.083547,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transform',
  'transform',
  0.084271,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'dhl',
  'dhl',
  0.084809,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'processing',
  'processing',
  0.085112,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'events',
  'events',
  0.085852,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'studies',
  'studies',
  0.086458,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'open',
  'open',
  0.087751,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'simple',
  'simple',
  0.087812,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'world',
  'world',
  0.087941,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'run',
  'run',
  0.088285,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'create your own',
  'create your own',
  0.088290,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'christian',
  'christian',
  0.090246,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sign',
  'sign',
  0.091441,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'apac',
  'apac',
  0.092165,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'free',
  'free',
  0.092819,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'working',
  'working',
  0.094006,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'startups',
  'startups',
  0.094635,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'project',
  'project',
  0.094905,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'change',
  'change',
  0.095385,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'running',
  'running',
  0.095630,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'work',
  'work',
  0.095886,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'success',
  'success',
  0.097476,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'small',
  'small',
  0.097555,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'demo',
  'demo',
  0.098769,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'credit',
  'credit',
  0.099105,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'nhs',
  'nhs',
  0.099415,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'live',
  'live',
  0.099556,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'simple to use',
  'simple to use',
  0.100229,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'awards',
  'awards',
  0.101476,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaborative',
  'collaborative',
  0.102752,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'thought leaders',
  'thought leaders',
  0.102951,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'it and telecom',
  'it and telecom',
  0.102977,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'purchase',
  'purchase',
  0.103631,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'build',
  'build',
  0.104626,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'resources',
  'resources',
  0.105579,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'financial services',
  'financial services',
  0.105911,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'private equity',
  'private equity',
  0.107087,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'go',
  'go',
  0.108079,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'co-founder',
  'co-founder',
  0.111699,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'project management',
  'project management',
  0.111754,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'around the world',
  'around the world',
  0.112593,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'data',
  'data',
  0.112599,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'implementation',
  'implementation',
  0.112982,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'software',
  'software',
  0.113964,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'executives',
  'executives',
  0.114349,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'digital',
  'digital',
  0.114785,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'faster',
  'faster',
  0.115541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'scoping',
  'scoping',
  0.119944,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'companies',
  'companies',
  0.120526,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cash',
  'cash',
  0.123643,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'leaders',
  'leaders',
  0.124385,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'roll',
  'roll',
  0.125429,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'travel',
  'travel',
  0.125688,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'party',
  'party',
  0.126033,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connected',
  'connected',
  0.127195,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'master',
  'master',
  0.129604,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'plug and play',
  'plug and play',
  0.129904,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social',
  'social',
  0.130057,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'value',
  'value',
  0.131179,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business needs',
  'business needs',
  0.132334,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'flow',
  'flow',
  0.133521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'pay',
  'pay',
  0.133794,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'case',
  'case',
  0.134330,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'delivery services',
  'delivery services',
  0.134725,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'san francisco',
  'san francisco',
  0.135286,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaborating',
  'collaborating',
  0.135548,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'create',
  'create',
  0.137690,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'maximizing profit',
  'maximizing profit',
  0.138501,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'solution',
  'solution',
  0.139010,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supply',
  'supply',
  0.139431,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'buyers',
  'buyers',
  0.139989,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'media',
  'media',
  0.142368,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'customer success',
  'customer success',
  0.142445,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'eprocurement',
  'eprocurement',
  0.144915,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'strategy',
  'strategy',
  0.144995,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'optimization',
  'optimization',
  0.145564,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'financing',
  'financing',
  0.145738,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automated',
  'automated',
  0.146357,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'rolling',
  'rolling',
  0.146804,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'assistant',
  'assistant',
  0.152563,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'electronic',
  'electronic',
  0.155293,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'optimize',
  'optimize',
  0.155588,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'brand and marketing',
  'brand and marketing',
  0.155846,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'managing director',
  'managing director',
  0.156071,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'risk',
  'risk',
  0.157575,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'transportation and logistics',
  'transportation and logistics',
  0.160693,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procure',
  'procure',
  0.160729,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'partner',
  'partner',
  0.160937,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'technology',
  'technology',
  0.161981,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business processes',
  'business processes',
  0.163748,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'enterprise software',
  'enterprise software',
  0.165190,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'sharing knowledge',
  'sharing knowledge',
  0.165249,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'innovative',
  'innovative',
  0.166251,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'personal information',
  'personal information',
  0.166576,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social media',
  'social media',
  0.167810,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud',
  'cloud',
  0.168007,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'credit cards',
  'credit cards',
  0.169409,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'e-invoice',
  'e-invoice',
  0.169682,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'creating value',
  'creating value',
  0.170131,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'integration and testing',
  'integration and testing',
  0.177895,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'empowering individuals',
  'empowering individuals',
  0.180299,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'account',
  'account',
  0.181852,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'managing',
  'managing',
  0.181963,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automobile manufacturer',
  'automobile manufacturer',
  0.182913,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'roll out',
  'roll out',
  0.184827,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'empower',
  'empower',
  0.184965,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'network',
  'network',
  0.185682,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'capital',
  'capital',
  0.188084,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'international media',
  'international media',
  0.188108,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'accounting and finance',
  'accounting and finance',
  0.188340,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'free enterprise',
  'free enterprise',
  0.188875,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'logistics company',
  'logistics company',
  0.190257,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'svp',
  'svp',
  0.190507,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'wind systems',
  'wind systems',
  0.192003,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manager',
  'manager',
  0.192435,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'build your own',
  'build your own',
  0.193591,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'growing business',
  'growing business',
  0.195539,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'chain',
  'chain',
  0.199089,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'managing partner',
  'managing partner',
  0.199437,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'innovate',
  'innovate',
  0.201495,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'development partner',
  'development partner',
  0.201895,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business start-up',
  'business start-up',
  0.206404,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connect',
  'connect',
  0.209680,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'online network',
  'online network',
  0.210690,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'developers',
  'developers',
  0.212329,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'people operations',
  'people operations',
  0.213460,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'case studies',
  'case studies',
  0.213541,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual',
  'virtual',
  0.214609,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global business',
  'global business',
  0.217577,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business network',
  'business network',
  0.218135,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business software',
  'business software',
  0.218972,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business',
  'business',
  0.224246,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'processes',
  'processes',
  0.225445,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'third party',
  'third party',
  0.226163,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'app',
  'app',
  0.233031,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'management',
  'management',
  0.233559,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'network strategy',
  'network strategy',
  0.240122,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'payables',
  'payables',
  0.241206,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global transportation',
  'global transportation',
  0.242844,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global supplier',
  'global supplier',
  0.243397,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement process',
  'procurement process',
  0.245279,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'software startups',
  'software startups',
  0.245445,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'electronic invoice',
  'electronic invoice',
  0.247874,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'innovative solution',
  'innovative solution',
  0.248610,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement solutions',
  'procurement solutions',
  0.248671,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business apps',
  'business apps',
  0.250219,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'process and technology',
  'process and technology',
  0.250744,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual credit cards',
  'virtual credit cards',
  0.252769,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'training and change management',
  'training and change management',
  0.254640,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global hr',
  'global hr',
  0.254918,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business and finance',
  'business and finance',
  0.256031,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual credit',
  'virtual credit',
  0.257501,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'trade platform',
  'trade platform',
  0.261166,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procure-to-pay',
  'procure-to-pay',
  0.261963,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procure to pay',
  'procure to pay',
  0.262688,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaboration',
  'collaboration',
  0.262858,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business platform',
  'business platform',
  0.266652,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based',
  'cloud-based',
  0.268094,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'connecting buyers and suppliers',
  'connecting buyers and suppliers',
  0.268434,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ap',
  'ap',
  0.268946,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'resource center',
  'resource center',
  0.270100,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automation',
  'automation',
  0.270148,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'go live',
  'go live',
  0.270202,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cash flow',
  'cash flow',
  0.273453,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'onboarding',
  'onboarding',
  0.274614,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'invoice processing',
  'invoice processing',
  0.274850,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'health service',
  'health service',
  0.275884,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'finance',
  'finance',
  0.277891,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'manage',
  'manage',
  0.282698,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global collaboration',
  'global collaboration',
  0.284374,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global procurement',
  'global procurement',
  0.287658,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'app development',
  'app development',
  0.288237,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global',
  'global',
  0.299386,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'industry reports',
  'industry reports',
  0.303475,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'webinars',
  'webinars',
  0.319440,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'small business',
  'small business',
  0.324828,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'professional services',
  'professional services',
  0.327950,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'enterprise',
  'enterprise',
  0.330279,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'apps',
  'apps',
  0.332163,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier collaboration',
  'supplier collaboration',
  0.333372,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'global network',
  'global network',
  0.337282,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'media company',
  'media company',
  0.342832,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'kicking ass',
  'kicking ass',
  0.344814,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud invoicing',
  'cloud invoicing',
  0.345398,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'change management',
  'change management',
  0.350239,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud technology',
  'cloud technology',
  0.369521,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'digital platform',
  'digital platform',
  0.370480,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'project manager',
  'project manager',
  0.377047,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'platform',
  'platform',
  0.380770,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supply chain',
  'supply chain',
  0.381261,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'invoices',
  'invoices',
  0.384785,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'national health service',
  'national health service',
  0.389765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'webinars and events',
  'webinars and events',
  0.391407,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'reducing risk',
  'reducing risk',
  0.417672,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management solutions',
  'learning management solutions',
  0.593947,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'global talent',
  'global talent',
  0.595438,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr software',
  'hr software',
  0.605764,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hr professionals',
  'hr professionals',
  0.626387,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee engagement',
  'employee engagement',
  0.626406,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'social recruiting',
  'social recruiting',
  0.654194,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'management change',
  'management change',
  0.668043,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'hrms solutions',
  'hrms solutions',
  0.680448,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management solutions',
  'talent management solutions',
  0.686764,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent acquisition',
  'talent acquisition',
  0.692872,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'performance management',
  'performance management',
  0.738763,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'employee onboarding',
  'employee onboarding',
  0.743655,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'integrated talent management',
  'integrated talent management',
  0.789497,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'learning management',
  'learning management',
  0.802125,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent portal',
  'talent portal',
  0.826749,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'silkroad.com',
  'talent management',
  'talent management',
  1.000000,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based platform',
  'cloud-based platform',
  0.421404,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'working capital',
  'working capital',
  0.423189,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business travel',
  'business travel',
  0.430718,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social media company',
  'social media company',
  0.443517,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'master data',
  'master data',
  0.444968,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'cloud-based technology',
  'cloud-based technology',
  0.448363,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'automated processes',
  'automated processes',
  0.450395,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'online purchase',
  'online purchase',
  0.453134,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'accounts payable',
  'accounts payable',
  0.456203,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'faster payments',
  'faster payments',
  0.467372,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaborate',
  'collaborate',
  0.477303,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'collaboration platform',
  'collaboration platform',
  0.482642,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'events and webinars',
  'events and webinars',
  0.490160,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'virtual assistant',
  'virtual assistant',
  0.492018,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'working capital optimization',
  'working capital optimization',
  0.498583,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'suppliers',
  'suppliers',
  0.499379,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'social technology',
  'social technology',
  0.503648,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement strategy',
  'procurement strategy',
  0.506171,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'buyers and suppliers',
  'buyers and suppliers',
  0.531831,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'business case',
  'business case',
  0.539240,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'capital optimization',
  'capital optimization',
  0.539872,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'procurement',
  'procurement',
  0.558510,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'improve processes',
  'improve processes',
  0.593550,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'electronic invoicing',
  'electronic invoicing',
  0.626192,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'open platform',
  'open platform',
  0.632206,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier onboarding',
  'supplier onboarding',
  0.671771,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'invoicing',
  'invoicing',
  0.696631,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier financing',
  'supplier financing',
  0.761064,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier master data',
  'supplier master data',
  0.772443,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'ap automation',
  'ap automation',
  0.814613,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'spend management',
  'spend management',
  0.883915,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'tradeshift.com',
  'supplier management',
  'supplier management',
  1.000000,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'contact',
  'contact',
  0.012151,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'email',
  'email',
  0.015053,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'information',
  'information',
  0.017122,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'rights',
  'rights',
  0.017426,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'phone',
  'phone',
  0.023041,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'full',
  'full',
  0.023484,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'name',
  'name',
  0.024182,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'inc.',
  'inc.',
  0.028361,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'menu',
  'menu',
  0.028435,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'questions',
  'questions',
  0.029025,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'better',
  'better',
  0.030061,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'state',
  'state',
  0.030248,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'city',
  'city',
  0.030340,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'company',
  'company',
  0.030950,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'faq',
  'faq',
  0.033041,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'time',
  'time',
  0.034873,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'make',
  'make',
  0.037527,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'single',
  'single',
  0.038135,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'size',
  'size',
  0.038934,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'free',
  'free',
  0.039154,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'present',
  'present',
  0.042938,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'picture',
  'picture',
  0.044164,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'solution',
  'solution',
  0.046449,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'feedback',
  'feedback',
  0.046747,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accurate',
  'accurate',
  0.049256,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'decisions',
  'decisions',
  0.049670,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'help',
  'help',
  0.049950,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'real-time',
  'real-time',
  0.055044,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'interface',
  'interface',
  0.056060,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'complete',
  'complete',
  0.056157,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'sign',
  'sign',
  0.060092,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'tools',
  'tools',
  0.061252,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'login',
  'login',
  0.063554,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'profitable',
  'profitable',
  0.065729,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'holistic',
  'holistic',
  0.066786,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'projects',
  'projects',
  0.067307,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'mid',
  'mid',
  0.068565,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'pricing',
  'pricing',
  0.081975,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'maintain',
  'maintain',
  0.082667,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'live',
  'live',
  0.084560,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'grow',
  'grow',
  0.085974,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'processing',
  'processing',
  0.096173,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cash',
  'cash',
  0.097097,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'confidence',
  'confidence',
  0.101391,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'run',
  'run',
  0.103995,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'flow',
  'flow',
  0.104854,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'lockbox',
  'lockbox',
  0.105920,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'tracking',
  'tracking',
  0.107590,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'trial',
  'trial',
  0.109257,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'automatic',
  'automatic',
  0.110176,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'entrepreneurs',
  'entrepreneurs',
  0.121311,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'integrated',
  'integrated',
  0.121765,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payments',
  'payments',
  0.152288,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'chat',
  'chat',
  0.159737,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'small',
  'small',
  0.165349,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'banks',
  'banks',
  0.178140,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'pay',
  'pay',
  0.203700,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cloud',
  'cloud',
  0.204161,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'business',
  'business',
  0.206462,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'software',
  'software',
  0.207732,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cash flow',
  'cash flow',
  0.214743,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'free trial',
  'free trial',
  0.230025,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'fully integrated',
  'fully integrated',
  0.237089,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management',
  'management',
  0.240708,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'intelligence',
  'intelligence',
  0.241655,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'complete solution',
  'complete solution',
  0.247408,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'live chat',
  'live chat',
  0.262483,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'platform',
  'platform',
  0.281894,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'invoicing',
  'invoicing',
  0.286390,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payroll processing',
  'payroll processing',
  0.300985,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'live chat software',
  'live chat software',
  0.304054,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial',
  'financial',
  0.306003,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'manage',
  'manage',
  0.308542,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'banking',
  'banking',
  0.309216,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'time tracking',
  'time tracking',
  0.327112,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'chat software',
  'chat software',
  0.355990,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accounting software',
  'accounting software',
  0.367849,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bookkeeping',
  'bookkeeping',
  0.399519,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management platform',
  'management platform',
  0.412008,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accountants',
  'accountants',
  0.452943,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'small business',
  'small business',
  0.464653,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'payroll',
  'payroll',
  0.483317,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'cloud bookkeeping',
  'cloud bookkeeping',
  0.493161,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'management and accounting',
  'management and accounting',
  0.546931,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'business intelligence',
  'business intelligence',
  0.549093,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financials',
  'financials',
  0.558662,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial management and accounting',
  'financial management and accounting',
  0.583269,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bill payments',
  'bill payments',
  0.598186,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'integrated financial management',
  'integrated financial management',
  0.614763,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'one platform',
  'one platform',
  0.623645,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'bill pay',
  'bill pay',
  0.692828,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'accounting',
  'accounting',
  0.706482,
  0.00
);

INSERT INTO northstar.relevance
(
  DOMAIN,
  grams,
  lemma,
  score,
  scaled_score
)
VALUES
(
  'finsync.com',
  'financial management',
  'financial management',
  1.000000,
  0.00
);


COMMIT;
