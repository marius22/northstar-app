/*
 * stats.sql
 * Copyright (C) 2017 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

select count(*) from northstar.company where domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(t.*) from northstar.company_tech t join northstar.company c on t.company_id = c.id where c.domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(c.*) from northstar.competitor c join northstar.company com on c.company_id = com.id where com.domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(*) from northstar.domain_indicator where domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(f.*) from northstar.function f join northstar.contact con on f.contact_id = con.id where con.domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(f.*) from northstar.funding_round f join northstar.company com on f.company_id = com.id where com.domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(*) from northstar.relevance where domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(*) from northstar.business_tags where domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
select count(*) from northstar.business_tags_keyword where domain in ('silkroad.com', 'tradeshift.com', 'finsync.com');
