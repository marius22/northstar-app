INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'debt_financing',
  7500000,
  2015,
  5,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  14000000,
  2014,
  7,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  16000000,
  2013,
  8,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  35000000,
  2012,
  5,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  40000000,
  2010,
  11,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  54000000,
  2008,
  5,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  10000000,
  2008,
  2,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074498028,
  'venture',
  23000000,
  2003,
  1,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'venture',
  75000000,
  2016,
  6,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'venture',
  75000000,
  2014,
  2,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'venture',
  17000000,
  2011,
  10,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'venture',
  7000000,
  2011,
  5,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'seed',
  12000000,
  2010,
  10,
  NULL
);

INSERT INTO northstar.funding_round
(
  company_id,
  funding_round,
  funding_round_amount,
  funding_year,
  funding_month,
  funding_day
)
VALUES
(
  1074684921,
  'angel',
  NULL,
  2010,
  4,
  NULL
);


COMMIT;
