/*
 * rollback_tables.sql
 * Copyright (C) 2016 Zhuo Peng <zhuopeng@everstring.com>
 *
 * Distributed under terms of the EverString license.
 */

\set backup '_before_migration'
\set tablename_back :tablename:backup
drop table if exists northstar.:tablename;
alter table northstar.:tablename_back rename to :tablename;
