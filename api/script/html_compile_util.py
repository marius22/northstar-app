"""
a util for jinling to transform .source.html files to .html files
run "python html_compile_util.py" please
"""
from os import listdir
from os.path import join
from premailer import Premailer
import re

base_path = "../ns/templates"
template_mark = ".source"
navigation_html = ['<html><head><meta charset="UTF-8">'
                   '<style>a{color:rgb(81, 191, 221)}</style>'
                   '<title>Title</title>'
                   '</head><body>'
                   '<h3>EverString email templates</h3>']

r = re.compile("href=\"%7B%7B(.*)%7D%7D\"")
rdash = re.compile("--")

for file_name in [f for f in listdir(base_path)]:
    if file_name.find(template_mark) != -1:
        template_name = file_name
        target_file = file_name.replace(template_mark, '')
        print target_file

        with open(join(base_path, template_name), 'r') as f, \
                open(join(base_path, target_file), 'w') as tar:
            # convert -- to &mdash;
            content = Premailer(f.read(), base_path=base_path).transform()
            content = rdash.sub(r'&mdash;', content)
            tar.write(r.sub(r'href="{{\1}}"', content))

        # navigation_html.append("<p><a href='" + file_name + "' target='content'>" + file_name + "</a></p>")
        navigation_html.append("<p><a href='" + target_file + "' target='content'>" + target_file + "</a></p>")

navigation_html.append('</body></html>')
with open(join(base_path, 'navigation.html'), 'w') as nav:
    nav.write(''.join(navigation_html))
