import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from ns.util.redshift_client import RedshiftClient

import tldextract as tldextract


class DomainUtils(object):

    @staticmethod
    def parse_domain(website, email):
        ext = tldextract.extract(website or '')
        domain = ext.registered_domain
        if not domain and email:
            email_parts = email.split('@')
            if len(email_parts) == 2:
                ext = tldextract.extract(email_parts[1] or '')
                domain = ext.registered_domain
        return domain


with app.app_context():
    client = RedshiftClient.instance()
    update_sql = "update crm_lead set email_domain = 'email_domain' where email_domain is null or email_domain = ''"
    try:
        client.execute(update_sql)
        sql = "select org_id, crm_id, email, email_domain from crm_lead " \
              "where email_domain = 'email_domain' limit 100000"
        result = client.query(sql)
        result = [list(one) for one in list(result)]
        while len(result) > 0:
            print len(result)
            for one in result:
                one[3] = DomainUtils.parse_domain(None, one[2])
            result = [tuple(one) for one in list(result)]
            tmp_table_name = "temp_crm_lead_email_domain"
            sql_create_temp_table = """create temp table {}
                                            (org_id varchar(18),
                                             crm_id varchar(18),
                                             email varchar(120),
                                             email_domain varchar(120))
                                        """.format(tmp_table_name)
            tmp_table_columns = ["org_id", "crm_id", "email", "email_domain"]

            update_sql = "update crm_lead set email_domain = b.email_domain " \
                         "from temp_crm_lead_email_domain as b " \
                         "where crm_lead.org_id = b.org_id and crm_lead.crm_id = b.crm_id"
            client.bulk_update_using_temp_table(sql_create_temp_table, tmp_table_name, tmp_table_columns, result, update_sql)
            result = client.query(sql)
            result = [list(one) for one in list(result)]
    finally:
        update_sql = "update crm_lead set email_domain = null where email_domain='email_domain'"
        client.execute(update_sql)



