import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from ns.model.role import Role
from ns.model import db


with app.app_context():
    role = Role(name=Role.CHROME_USER)
    db.session.add(role)
    db.session.commit()