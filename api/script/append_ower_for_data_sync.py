import sys
import os
import json

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from ns.util.redshift_client import RedshiftClient


objects = ["lead", "contact", "account", "opportunity"]


def handle_org(org_id):
    for object in objects:
        handle_object(object, org_id)


def handle_object(object_name, org_id):
    raw_sql = '''
                SELECT org_id, crm_id, data_raw_json
                FROM crm_{}_raw_data a
                WHERE EXISTS (SELECT *
                                FROM crm_{} b
                                WHERE a.org_id = b.org_id
                                AND   a.crm_id = b.crm_id
                                AND   b.owner_id IS NULL
                                AND   b.org_id = '{}')
                            limit 100000
        '''.format(object_name, object_name, org_id)
    raw_results = client.query(raw_sql)
    while len(raw_results) > 0:
        list_temp_records = []
        for raw_result in raw_results:
            owner_id = json.loads(raw_result[2]).get("OwnerId")
            temp_record = (raw_result[0], raw_result[1], owner_id)
            list_temp_records.append(temp_record)
        tmp_table_name = "temp_crm_owner_id"
        sql_create_temp_table = """create temp table {}
                                            (org_id varchar(18),
                                             crm_id varchar(18),
                                             owner_id varchar(18))
                                        """.format(tmp_table_name)
        tmp_table_columns = ["org_id", "crm_id", "owner_id"]

        update_sql = '''update crm_{} set owner_id = b.owner_id
                        from temp_crm_owner_id as b
                        where crm_{}.org_id = b.org_id and crm_{}.crm_id = b.crm_id'''.format(object_name, object_name, object_name)
        client.bulk_update_using_temp_table(sql_create_temp_table, tmp_table_name, tmp_table_columns, list_temp_records,
                                            update_sql)
        raw_results = client.query(raw_sql)


with app.app_context():
    client = RedshiftClient.instance()
    org_sql = "select org_id from crm_lead where owner_id is null group by org_id"
    org_ids = client.query(org_sql)
    org_ids = [list(one)[0] for one in list(org_ids)]
    for org_id in org_ids:
        handle_org(org_id)



