#!/usr/bin/env python
from shutil import copy

import os

src_hook_filename = 'pre-push.py.example'
src = os.path.join(os.path.dirname(__file__), src_hook_filename)
src = os.path.abspath(src)

dest_hook_filename = 'pre-push'
dest = os.path.join(os.path.dirname(__file__), '..', '..', '.git', 'hooks', dest_hook_filename)
dest = os.path.abspath(dest)

print('=== Installing pre-push script ===\nFrom:\t%s\nDest:\t%s' % (src, dest))
copy(src, dest)
