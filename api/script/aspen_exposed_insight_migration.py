import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.model.account_quota import AccountQuota
from ns.model.company_insight import CompanyInsight
from ns.constants.insight_constants import InsightConstants
from ns.controller.app import app

import logging
logger = logging.getLogger(__name__)


#
# This function updates exposed_csv_insight_ids for all customers.
# The requirement is to use the default published insights without
# technology and update the existing list for every customer.
# 
def _update_exposed_account_ids():
    supported_insights = InsightConstants.SUPPORTED_PUBLISH_INSIGHTS
    supported_insights.remove(InsightConstants.INSIGHT_NAME_TECHNOLOGY)
    ids = CompanyInsight.get_ids_by_display_names(supported_insights)
    AccountQuota.bulk_update_exposed_csv_insight_ids(ids)

with app.app_context():
    logger.info("Migration started")
    _update_exposed_account_ids()
    logger.info("Migration ended")
