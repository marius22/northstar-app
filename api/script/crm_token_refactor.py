import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from sqlalchemy import create_engine
import json
from ns.model import salesforce
from ns.model.crm_token import SFDCToken
from ns.model.salesforce import NSSalesforce
from ns.util.http_client import HTTPClient


db_uri = app.config.get("SQLALCHEMY_DATABASE_URI")
print db_uri


def request_refresh_token(refresh_token):

    request_url = '%s?grant_type=refresh_token' \
                  '&client_id=%s&client_secret=%s&refresh_token=%s' \
                  % (app.config.get("TOKEN_URL"), app.config.get("CLIENT_ID"),
                     app.config.get("CLIENT_SECRET"), refresh_token)
    print "RequestURL: ", request_url

    json_result = HTTPClient.post_json(request_url)

    return dict([(k, json_result.get(k)) for k in
                 [SFDCToken.ACCESS_TOKEN_KEY, SFDCToken.INSTANCE_URL_KEY,
                  SFDCToken.SALESFORCE_ID_KEY]])

engine = create_engine(db_uri, encoding='utf-8')

query = "select id, token, user_id, account_id from crm_token"
result = engine.execute(query)
for row in result:
    row_id = row[0]
    token_dict = json.loads(row[1])

    token_new = request_refresh_token(token_dict[SFDCToken.REFRESH_TOKEN_KEY])
    token_dict[SFDCToken.ACCESS_TOKEN_KEY] = token_new[SFDCToken.ACCESS_TOKEN_KEY]
    token_dict[SFDCToken.INSTANCE_URL_KEY] = token_new[SFDCToken.INSTANCE_URL_KEY]
    token_dict[SFDCToken.SALESFORCE_ID_KEY] = token_new[SFDCToken.SALESFORCE_ID_KEY]
    print 'REFRESH', token_new

    profile = salesforce.get_user_info(token_dict[SFDCToken.SALESFORCE_ID_KEY],
                                       token_dict[SFDCToken.ACCESS_TOKEN_KEY])
    sfdc_token = SFDCToken(row[2], row[3])
    sfdc_token.access_token = token_dict.get(SFDCToken.ACCESS_TOKEN_KEY)
    sfdc_token.instance_url = token_dict.get(SFDCToken.INSTANCE_URL_KEY)
    ns_salesforce = NSSalesforce(sfdc_token)
    profile_name = ns_salesforce.query_profile_name(profile.get(SFDCToken.USER_ID_KEY))

    sql = "update crm_token set organization_id='{}', email='{}', role='{}', token='{}' where id={}".format(
        token_dict.get('organization_id'), token_dict.get('email'), profile_name, json.dumps(token_dict), row_id)
    print sql
    engine.execute(sql)


