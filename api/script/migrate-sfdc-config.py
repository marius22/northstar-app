# -*- coding: utf-8 -*-
import sys
import os
import json

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.model.sfdc_config import SFDCConfig
from ns.controller.app import app
from ns.model import db


def add_sfdc_config():
    all_configs = SFDCConfig.query.all()
    for sfdc_config in all_configs:
        original_config = sfdc_config.config_dict

        original_config.update({
            "allowCRMAddPeopleToLeads": True,
            "allowCRMAddPeopleToContacts": True
        })

        sfdc_config.config = json.dumps(original_config)
        db.session.commit()


if __name__ == '__main__':
    with app.app_context():
        add_sfdc_config()
