"""
usage:
python script/upload_ui_img.py ../client/src/images/email/logo2.png
"""
import getopt
import uuid

import os
import sys


sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.util.s3client import S3Client
from instance import config


def main(argv):
    if len(argv) < 1:
        print "Missing input path file"
        exit()

    aws_access_key_id = config.AWS_ACCESS_KEY_ID
    aws_secret_access_key = config.AWS_SECRET_ACCESS_KEY
    bucket_name = config.S3_CSV_BUCKET
    client = S3Client(aws_access_key_id, aws_secret_access_key, bucket_name)
    fid = uuid.uuid1().hex

    image = open(argv[0], 'r')
    link = client.put_object(fid, 'ui-img/' + fid, image.read(), 'image',
                             5 * 365 * 24 * 3600, False)
    image.close()
    print link

if __name__ == "__main__":
    main(sys.argv[1:])
