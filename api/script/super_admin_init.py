import sys, os
import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.model import db
from ns.controller.app import app
from mixer.backend.flask import mixer
from ns.util.password_util import generate_salt_str, encrypt_password_with_salt

from ns.model.account import Account
from ns.model.role import Role
from ns.model.user import User
from ns.model.user_role import UserRole
from ns.model.account_quota import AccountQuota

with app.app_context():
    account = Account(name='ES_SUPER_ADMIN', account_type=Account.ES)
    account_quota = AccountQuota(segment_cnt=1000000,
                                 exposed_ui_insight_ids=str([]),
                                 exposed_csv_insight_ids=str([]),
                                 ui_company_limit=100000000,
                                 csv_company_limit=100000000,
                                 contact_limit=0,
                                 account=account,
                                 enable_ads=True,
                                 expired_ts=datetime.datetime.utcnow() + datetime.timedelta(365 * 2))
    salt = generate_salt_str()
    user = User(email='pu@everstring.com', password=encrypt_password_with_salt(salt, 'superPasswd123'),
                salt=salt, activated=True, account=account, contact_export_flag=True)
    role = Role.get_by_name(Role.ES_SUPER_ADMIN)
    user_role = UserRole(user=user, role=role)
    
    db.session.add_all([account, account_quota, user, user_role])
    db.session.commit()
