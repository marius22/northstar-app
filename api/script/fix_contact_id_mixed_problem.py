# -*- coding: utf-8 -*-

import sys
import os
import math
import datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.service import app
from ns.model.account_exported_contact import AccountExportedContact
from ns.util.data_api_client import DataAPIClient
from sqlalchemy import and_
from ns.model import db

import logging
logger = logging.getLogger(__name__)


def get_batch_lists(origin_list, batch_size):
    assert isinstance(batch_size, int), "Invalid value"
    result_list = list()
    if not origin_list:
        return result_list
    total = len(origin_list)
    n = int(math.ceil(total*1.0/batch_size))
    for i in range(n):
        start = i*batch_size
        end = min((i+1)*batch_size, total)
        result_list.append(origin_list[start:end])
    return result_list


def get_es_contacts(contact_ids):
    contact_api_method_name = "contact.list.preview"
    payload = {"contact": {"id": contact_ids}, "limit": len(contact_ids), "select": ["contact.id", "contact.esId"]}
    contacts = DataAPIClient.instance().contact_list(payload, contact_api_method_name)
    id_to_es_id_map = dict()
    for one in contacts:
        id_to_es_id_map[one["contact.id"]] = one["contact.esId"]
    return id_to_es_id_map


with app.app_context():
    # backup data
    now_str = datetime.datetime.now().strftime('%s')
    sql_create_table = "CREATE TABLE account_exported_contact_" + now_str + " LIKE account_exported_contact"
    sql_backup = "INSERT account_exported_contact_" + now_str + " SELECT * FROM account_exported_contact;"
    db.engine.execute(sql_create_table)
    db.engine.execute(sql_backup)

    # get all contacts with not everstring contact id
    all_contacts = AccountExportedContact.filter_by(and_(AccountExportedContact.contact_id <= 2 * 10000 * 10000))
    print len(all_contacts)

    # delete all contacts with not everstring contact id
    AccountExportedContact.delete_by_filter(and_(AccountExportedContact.contact_id <= 2 * 10000 * 10000))

    # batch process the contacts with not everstring contact id
    contacts_batches = get_batch_lists(all_contacts, 200)
    batch_count = len(contacts_batches)
    i = 0
    for one_batch_contacts in contacts_batches:
        logger.info("process {i}/{batch_count}".format(i=i, batch_count=batch_count))
        contact_ids = [contact.contact_id for contact in one_batch_contacts]
        id_to_es_id_map = get_es_contacts(contact_ids)
        for contact in one_batch_contacts:
            account_exported_contact = AccountExportedContact.filter_by(and_(
                AccountExportedContact.account_id == contact.account_id,
                AccountExportedContact.contact_id == int(id_to_es_id_map.get(str(contact.contact_id)))))
            if not account_exported_contact:
                contact_with_es_contact_id = AccountExportedContact(id=contact.id,
                                                                    created_ts=contact.created_ts,
                                                                    updated_ts=contact.updated_ts,
                                                                    account_id=contact.account_id,
                                                                    contact_id=
                                                                    int(id_to_es_id_map.get(str(contact.contact_id))))
                db.session.add(contact_with_es_contact_id)
        db.session.commit()
        i += 1
