import os
import copy
import random
import sys

import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.model.job import Job
from ns.model.preference import Preference
from ns.model.recommendation import Recommendation
from ns.model.seed import Seed
from ns.model.qualification import Qualification
from ns.model import db
from ns.model.segment import Segment
from ns.model.user import User
from ns.model.user_role import UserRole
from ns.model.role import Role
from mixer.backend.sqlalchemy import Mixer
from ns.model.account import Account
from ns.controller.app import app


def es_email_generator(domain):
    i = 1
    while True:
        email = 'test%(i)s@%(domain)s' % locals()
        yield email
        i += 1


def segment_name_gen():
    i = 1
    while True:
        yield 'Segment %(i)s' % locals()
        i += int(100 * random.random())

with app.app_context():
    mixer = Mixer(session=db.session, commit=True)

    everstring = mixer.blend(Account, name='Everstring', domain='everstring.com')

    # Hardcoding a user with this email since it's required while we don't have log in.
    es_user = mixer.blend(User, account=everstring, email='test@everstring.com',
                          activated=True)
    # Since alembic creates the roles already, we grab it and use it here
    account_user_role = Role.query.filter_by(name='ACCOUNT_USER').first()
    es_user_user_role = mixer.blend(UserRole, user=es_user,
                                    role=account_user_role)

    other_es_users = mixer.cycle(2).blend(
        User, account=everstring, email=es_email_generator(everstring.domain),
        activated=True, role_id=account_user_role.id)

    infer = mixer.blend(Account, name='Infer', domain='infer.com')
    mixer.cycle(3).blend(User, account=infer, email=es_email_generator(infer.domain))

    # Create segments for our ES users.
    segment_args = {
        'account': everstring,
        'is_deleted': False,
        'name': segment_name_gen()
    }
    es_users = [es_user] + other_es_users
    for user in es_users:
        segment_args.update(owner=user)
        for status in Segment.status_enums:
            this_segment_args = copy.copy(segment_args)
            this_segment_args.update(status=status)
            mixer.cycle(100).blend(Segment, **this_segment_args)
