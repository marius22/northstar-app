from mixer.backend.flask import mixer
from ns.controller.app import app

from ns.model.account import Account
from ns.model.account_quota import AccountQuota
from ns.model.company_new import CompanyNew
from ns.model.crm_token import CRMToken
from ns.model.job import Job
from ns.model.job_status_history import JobStatusHistory
from ns.model.qualification import Qualification
from ns.model.recommendation import Recommendation
from ns.model.role import Role
from ns.model.seed import Seed
from ns.model.seed_candidate import SeedCandidate
from ns.model.seed_filter import SeedFilter
from ns.model.segment import Segment
from ns.model.source import Source
from ns.model.user import User
from ns.model.user_role import UserRole
from ns.model.user_segment import UserSegment
from ns.model.user_exported_company import UserExportedCompany
from ns.model.domain_dictionary import DomainDictionary
from ns.model.session import RedisSessionInterface

app.app_context().push()
mixer.init_app(app)
