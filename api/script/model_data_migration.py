import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import psycopg2
from ns.util.dynamic_config_client import get_configuration_object
from ns.model.model import Model
from ns.model.model_seed import ModelSeed
from ns.model.seed_domain import SeedDomain
from ns.model.seed_company import SeedCompany
from ns.util.string_util import none_to_empty
from ns.model import db
from ns.model.job import Job
from ns.model.account import Account
from ns.controller.app import app
from uuid import uuid4

import logging
import json
logger = logging.getLogger(__name__)


def _get_a_redshift_session():
    conn_info = json.loads(os.getenv("DB_CONFIG"))
    conn_str = ' '.join("%s=%s" % (key, value)
                        for key, value in conn_info.iteritems())

    conn = psycopg2.connect(conn_str)
    cursor = conn.cursor()
    cursor.execute("SET search_path TO northstar")
    return conn, cursor


def _figure_out_model_status(account_id, seed_id):
    job = Job.get_by_account_type_seed(account_id, Job.ACCOUNT_FIT, seed_id)
    if job and job.status == Job.COMPLETED:
        return Model.COMPLETED
    return Model.IN_PROGRESS


def _transform_2_domain_company(model_seed_id, seed_domains):
    if not seed_domains:
        return []
    result = []
    for seed_domain in seed_domains:
        result.append(
            SeedCompany(
                model_seed_id=model_seed_id,
                domain=seed_domain.domain,
                company_name=none_to_empty(seed_domain.company_name),
                sf_account_id=none_to_empty(seed_domain.sf_account_id),
                sf_account_name=none_to_empty(seed_domain.sf_account_name),
                created_ts=seed_domain.created_ts,
                updated_ts=seed_domain.updated_ts
            )
        )
    return result


def _migration_for_model():

    # delete all feature analysis models
    Model.query.filter(Model.type == Model.FEATURE_ANALYSIS).delete()
    db.session.commit()

    fit_models = Model.get_all_models()
    account_ids = set([item.owner_id for item in fit_models])

    total_count = len(account_ids)
    logger.info("v3 migration -- model data migration, start. [%s] accounts to migrate" % total_count)
    completed_counter = 0

    for account_id in account_ids:

        account = Account.get_by_id(account_id)
        if not account:
            logger.warn("v3 migration -- model data migration, can not find account: [%s]"
                        % account_id)
            continue

        fit_model = Model.query\
            .filter(Model.owner_id == account_id,
                    Model.type == Model.ACCOUNT_FIT)\
            .order_by(Model.id.desc())\
            .first()

        if not fit_model:
            logger.warn("v3 migration -- model data migration, can not any crm fit model for account: [%s]"
                        % account_id)
            continue

        # we only need one account fit model, delete others
        Model.query.filter(Model.owner_id == account_id,
                           Model.type == Model.ACCOUNT_FIT,
                           Model.id != fit_model.id).delete()
        db.session.commit()

        batch_id = str(uuid4())

        # migration for model
        fit_model.name = "CRM Fit"
        fit_model.account_id = fit_model.owner_id
        fit_model.model_seed_id = fit_model.seed_id
        fit_model.batch_id = batch_id
        fit_model.status = _figure_out_model_status(fit_model.owner_id, fit_model.seed_id)

        # migration for model_seed
        model_seed = ModelSeed.get_by_id(fit_model.seed_id)
        if not model_seed:
            logger.warn("v3 migration -- model data migration, "
                        "can find a model_seed for model [%s], model_seed_id [%s]"
                        % (fit_model.id, fit_model.seed_id))
            continue

        model_seed.account_id = fit_model.account_id
        # delete the existed seeds first, for re-run
        SeedCompany.delete_by_model_seed_id(model_seed.id)

        # migration for seed_company
        seed_domains = SeedDomain.get_by_ids(json.loads(model_seed.seed_domain_ids))
        SeedCompany._insert_seed_domains(_transform_2_domain_company(model_seed.id, seed_domains))

        # create feature analysis model for old account fit model
        Model.create_model('account_%s' % Model.FEATURE_ANALYSIS, fit_model.account_id,
                           Model.FEATURE_ANALYSIS, batch_id,
                           fit_model.status, fit_model.model_seed_id)

        db.session.commit()

        completed_counter += 1
        logger.info("v3 migration -- model data migration, migration for model [%s] completed,"
                    " %s/%s completed" % (fit_model.id, completed_counter, total_count))

    logger.info("v3 migration -- model data migration, %s/%s completed. " % (completed_counter, total_count))


def _create_temp_table(session, temp_table_name):
    session.execute('''
        CREATE temp TABLE %s (
            account_id   integer NOT NULL,
            model_id     integer NOT NULL,
            domain       varchar(100) NOT NULL,
            score        numeric(8,6)   NOT NULL,
            is_seed      integer
         );
    ''' % temp_table_name)
    logger.info("v3 migration -- fit score migration, create temporary table completed")


def _drop_model_fitscore_data(session):
    session.execute('''
        delete from model_fitscore;
    ''')
    logger.info("v3 migration -- fit score migration, delete model_fitscore data completed")


def _copy_data_from_fitscore_to_temp(session, temp_table_name):
    logger.info("v3 migration -- fit score migration, insert data into temporary table start")
    session.execute('''
        insert into %s(account_id, model_id, domain, score, is_seed)
        select account_id, 0, domain, score, is_seed from fitscore
    ''' % temp_table_name)
    logger.info("v3 migration -- fit score migration, insert data into temporary table completed")


def _copy_data_from_temp_table_to_model_fitscore(session, temp_table_name):
    logger.info("v3 migration -- fit score migration, insert data into model_fitscore table start")
    session.execute('''
        insert into model_fitscore(model_id, domain, score, is_seed)
        select model_id, domain, score, is_seed from %s
    ''' % temp_table_name)
    logger.info("v3 migration -- fit score migration, insert data into model_fitscore table completed")


def _migration_for_fit_score():

    logger.info("v3 migration -- fit score migration, start")

    conn, redshift_session = _get_a_redshift_session()
    temp_fit_score_table_name = "temp_fit_score"

    _drop_model_fitscore_data(redshift_session)
    _create_temp_table(redshift_session, temp_fit_score_table_name)
    _copy_data_from_fitscore_to_temp(redshift_session, temp_fit_score_table_name)

    fit_models = Model.get_all_models()
    account_ids = set([item.account_id for item in fit_models])
    total_count = len(account_ids)
    logger.info("v3 migration -- fit score migration, start, [%s] accounts to migrate" % total_count)
    completed_counter = 0

    for account_id in account_ids:
        account = Account.get_by_id(account_id)

        if not account:
            logger.warn("v3 migration -- fit score migration, can not find account: [%s]"
                        % account_id)
            continue

        latest_model = account.latest_completed_crm_fit_model
        if not latest_model:
            logger.warn("v3 migration -- fit score migration, "
                        "can not find a latest completed crm fit model for account: [%s]" % account_id)
            continue

        redshift_session.execute('''
            update %s set model_id = %s where account_id = %s
        ''' % (temp_fit_score_table_name, latest_model.id, account_id))

        completed_counter += 1
        logger.info("v3 migration -- fit score migration, migration for account [%s] completed,"
                    " %s/%s completed" % (account_id, completed_counter, total_count))

    logger.info("v3 migration -- fit score migration, %s/%s completed. " % (completed_counter, total_count))

    _copy_data_from_temp_table_to_model_fitscore(redshift_session, temp_fit_score_table_name)
    redshift_session.execute("drop table %s" % temp_fit_score_table_name)

    conn.commit()
    conn.close()
    logger.info("v3 migration -- fit score migration, completed")


def _create_temp_table_for_feature_analysis(session, temp_table_name):
    session.execute('''
        CREATE temp TABLE %s (
            account_id   integer NOT NULL,
            model_id     integer NOT NULL,
            indicator    varchar(160) not null,
            coeff        numeric (8,6) not null
         );
    ''' % temp_table_name)
    logger.info("v3 migration -- feature analysis migration, create temporary table completed")


def _drop_model_coefficient_data(session):
    session.execute('''
        delete from model_coefficient;
    ''')
    logger.info("v3 migration -- feature analysis migration, delete model_fitscore data completed")


def _copy_data_from_model_coeff_to_temp_table(session, temp_table_name):
    logger.info("v3 migration -- feature analysis migration, insert data into temporary table start")
    session.execute('''
        insert into %s(account_id, model_id, indicator, coeff)
        select account_id, 0, indicator, coeff from model_coeff where account_id != 0 and segment_id = 0
    ''' % temp_table_name)
    logger.info("v3 migration -- feature analysis migration, insert data into temporary table completed")


def _copy_data_from_temp_table_to_model_coefficient(session, temp_table_name):
    logger.info("v3 migration -- feature analysis migration, insert data into model_fitscore table start")
    session.execute('''
        insert into model_coefficient(model_id, indicator, coeff)
        select model_id, indicator, coeff from %s
    ''' % temp_table_name)
    logger.info("v3 migration -- feature analysis migration, insert data into model_fitscore table completed")


def _migration_for_feature_analysis():
    logger.info("v3 migration -- feature analysis migration, start")
    conn, redshift_session = _get_a_redshift_session()
    temp_model_coeff_table_name = "temp_model_coeff"

    _drop_model_coefficient_data(redshift_session)
    _create_temp_table_for_feature_analysis(redshift_session, temp_model_coeff_table_name)
    _copy_data_from_model_coeff_to_temp_table(redshift_session, temp_model_coeff_table_name)

    fit_models = Model.get_all_models()
    account_ids = set([item.account_id for item in fit_models])
    total_count = len(account_ids)

    logger.info("v3 migration -- feature analysis migration, start, [%s] accounts to migrate" % total_count)
    completed_counter = 0

    for account_id in account_ids:
        account = Account.get_by_id(account_id)

        if not account:
            logger.warn("v3 migration -- feature analysis migration, can not find account: [%s]"
                        % account_id)
            continue

        latest_model = Model.get_latest_model(account.id, Model.FEATURE_ANALYSIS, Model.COMPLETED)
        if not latest_model:
            logger.warn("v3 migration -- feature analysis migration, "
                        "can not find a latest completed feature analysis model for account: [%s]" % account_id)
            continue

        redshift_session.execute('''
                update %s set model_id = %s where account_id = %s
            ''' % (temp_model_coeff_table_name, latest_model.id, account_id))

        completed_counter += 1
        logger.info("v3 migration -- feature analysis migration, migration for account [%s] completed,"
                    " %s/%s completed" % (account_id, completed_counter, total_count))

    logger.info("v3 migration -- feature analysis migration, %s/%s completed. " % (completed_counter, total_count))

    _copy_data_from_temp_table_to_model_coefficient(redshift_session, temp_model_coeff_table_name)
    redshift_session.execute("drop table %s" % temp_model_coeff_table_name)

    conn.commit()
    conn.close()
    logger.info("v3 migration -- feature analysis migration, completed")


with app.app_context():

    logger.info("v3 migration --  model, start!")
    _migration_for_model()
    logger.info("v3 migration --  model, completed")

    logger.info("v3 migration --  fit score, start")
    _migration_for_fit_score()
    logger.info("v3 migration --  fit score, completed")

    logger.info("v3 migration --  feature analysis, start")
    _migration_for_feature_analysis()
    logger.info("v3 migration --  feature analysis, completed")
