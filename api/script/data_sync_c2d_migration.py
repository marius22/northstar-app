import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from ns.controller.app import app
from ns.util.redshift_client import RedshiftClient
from ns.util.string_util import is_blank, is_not_blank
from ns.util.http_client import HTTPClient
from flask import current_app

import json
import httplib


def get_domains_by_names(names):
    name_to_domain_endpoint = current_app.config.get("COMPANY_NAME_TO_DOMAIN_HOST") + "/name_domain/c2d/"

    c2d_results = []
    resp = HTTPClient.post_json(
        name_to_domain_endpoint,
        json={
            "name": "||".join(names),
            'source': "datasync",
            "enable_crawl": "1",
            "max_wait_seconds": "0"
        }
    )
    c2d_results.extend(resp["result"])
    results = {}
    for item in c2d_results:
        if is_not_blank(item.get("domain")):
            results[item.get("name")] = item.get("domain")
    return results


def handle_object_c2d(object_name):
    client = RedshiftClient.instance()
    update_sql = "update crm_{} set domain = 'company_domain' where domain is null or domain=''".format(object_name)
    company_name_field = "company"
    if object_name == 'account':
        company_name_field = "name"
    try:
        client.execute(update_sql)
        sql = "select org_id, crm_id, domain, {} from crm_{} " \
              "where domain = 'company_domain' limit 2000".format(company_name_field, object_name)
        result = client.query(sql)
        result = [list(one) for one in list(result)]
        while len(result) > 0:
            print len(result)
            names = set()
            for one in result:
                if is_not_blank(one[3]):
                    names.add(one[3])

            name_domain_map = get_domains_by_names(names)

            for one in result:
                one[2] = None
                if name_domain_map.get(one[3]):
                    one[2] = name_domain_map.get(one[3])

            result = [tuple(one) for one in list(result)]
            tmp_table_name = "temp_crm_company_domain"
            sql_create_temp_table = """create temp table {}
                                                (org_id varchar(18),
                                                 crm_id varchar(18),
                                                 domain varchar(120),
                                                 company varchar(255))
                                            """.format(tmp_table_name)
            tmp_table_columns = ["org_id", "crm_id", "domain", "company"]

            update_sql = """update crm_{} set domain = b.domain
                            from temp_crm_company_domain as b
                            where crm_{}.org_id = b.org_id and crm_{}.crm_id = b.crm_id
                        """.format(object_name, object_name, object_name)
            client.bulk_update_using_temp_table(sql_create_temp_table, tmp_table_name, tmp_table_columns, result,
                                                update_sql)
            result = client.query(sql)
            result = [list(one) for one in list(result)]
    finally:
        update_sql = "update crm_{} set domain = null where domain='company_domain'".format(object_name)
        client.execute(update_sql)


with app.app_context():
    for object_name in ["lead", "account"]:
        handle_object_c2d(object_name)





