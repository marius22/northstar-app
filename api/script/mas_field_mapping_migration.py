# -*- coding: utf-8 -*-
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import datetime
import json

from ns.controller.app import app
from ns.model.publish_field_mapping import PublishFieldMapping
from ns.model.company_insight import CompanyInsight
from ns.model.mas_field_mapping import MASFieldMapping
from ns.model.mas_token import MASToken
from ns.model import db
from ns.model.audience import Audience
from ns.model.model import Model
from sqlalchemy import desc
import logging
logger = logging.getLogger(__name__)

keys = []


def backup_table(table_name):
    now_str = datetime.datetime.now().strftime("%y_%m_%d_%H_%M")
    sql_create_table = ("CREATE TABLE {table_name}"+"_" + now_str + " LIKE {table_name};").format(table_name=table_name)
    sql_backup = ("INSERT {table_name}"+"_" + now_str + " SELECT * FROM {table_name};").format(table_name=table_name)
    logger.info(sql_create_table)
    logger.info(sql_backup)
    db.engine.execute(sql_create_table)
    db.engine.execute(sql_backup)


def mas_field_mapping_record_to_publish_field_mapping_records(mas_field_mapping_record):
    records = []

    target_object = "lead"

    account_id = mas_field_mapping_record.account_id
    mas_type = mas_field_mapping_record.mas_type
    mapping = json.loads(mas_field_mapping_record.mapping)
    standard_field_map = mapping.get("standard", {})
    audience_field_map = mapping.get("audience", {})

    created_ts = mas_field_mapping_record.created_ts
    updated_ts = mas_field_mapping_record.updated_ts

    mas_token = MASToken.get_by_account_id_and_mas_type(account_id, mas_type)
    if mas_token:
        target_org_id = mas_token.mas_id
        mappings_for_company_fields = generate_company_mapping(account_id, mas_type,
                                                               target_org_id, target_object,
                                                               created_ts, updated_ts, standard_field_map)
        mappings_for_models = generate_model_mapping(account_id, mas_type,
                                                     target_org_id, target_object,
                                                     created_ts, updated_ts,
                                                     audience_field_map)
        records.extend(mappings_for_company_fields)
        records.extend(mappings_for_models)

    return records


def generate_company_mapping(account_id, mas_type,
                           target_org_id, target_object,
                           created_ts, updated_ts, standard_field_map):
    records = list()
    company_insight_records = CompanyInsight.get_supported_publish_fields(target_type=mas_type)
    insight_name_to_insight_record_map = dict([(one.display_name, one) for one in company_insight_records])
    for es_field, target_field in standard_field_map.iteritems():
        insight_record = insight_name_to_insight_record_map.get(es_field)
        new_key = str(account_id) + ":" + mas_type + ":" + target_object + ":"+target_org_id + ":"+target_field
        if insight_record and (new_key not in keys):
            insight_id = insight_record.id
            publish_field_mapping = PublishFieldMapping(
                account_id=account_id,
                insight_id=insight_id,
                model_id=None,
                target_type=mas_type,
                target_object=target_object,
                target_org_id=target_org_id,
                target_field=target_field,
                created_ts=created_ts,
                updated_ts=updated_ts
            )
            records.append(publish_field_mapping)
            keys.append(new_key)
            logger.info("account: {account_id}, key: {key}".format(account_id=account_id, key=new_key))
    return records


def generate_model_mapping(account_id, mas_type,
                           target_org_id, target_object,
                           created_ts, updated_ts, audience_field_map):
    records = list()

    audience_ids = [int(audience_id) for audience_id in audience_field_map]
    audiences = Audience.get_by_ids(audience_ids)
    audience_id_audience_map = dict([(audience.id, audience) for audience in audiences])

    model_ids = list()
    model_id_target_field_map = dict()

    for audience_id, target_field in audience_field_map.iteritems():
        audience = audience_id_audience_map.get(int(audience_id))
        if audience and audience.model_id:
            model_id = audience.model_id
            model_ids.append(model_id)
            model_id_target_field_map[model_id] = target_field

    account_fit_models = Model.query.filter(Model.id.in_(model_ids), Model.type == Model.ACCOUNT_FIT).all()
    custom_models = Model.query.filter(Model.id.in_(model_ids),
                                       Model.type == Model.FIT_MODEL,
                                       Model.is_deleted.is_(False)).order_by(desc(Model.id)).all()

    all_models_need_mapping = list()

    account_model = None
    if account_fit_models:
        account_model = Model.get_latest_completed_model(account_id, Model.ACCOUNT_FIT)
    if account_model:
        all_models_need_mapping.append(account_model)
    all_models_need_mapping.extend(custom_models)

    for one_model in all_models_need_mapping:
        model_id = one_model.id
        target_field = model_id_target_field_map.get(model_id)
        new_key = str(account_id) + ":" + mas_type + ":" + target_object + ":"+target_org_id + ":"+target_field
        if new_key not in keys:
            publish_field_mapping = PublishFieldMapping(
                    account_id=account_id,
                    insight_id=None,
                    model_id=model_id,
                    target_type=mas_type,
                    target_object=target_object,
                    target_org_id=target_org_id,
                    target_field=target_field,
                    created_ts=created_ts,
                    updated_ts=updated_ts
                )
            records.append(publish_field_mapping)
            keys.append(new_key)
            logger.info("account: {account_id}, key: {key}".format(account_id=account_id, key=new_key))
    return records


def migrate():
    mas_field_mapping_records = MASFieldMapping.query.filter().order_by(MASFieldMapping.updated_ts.desc()).all()
    num = len(mas_field_mapping_records)
    logger.info("{num} mas_field_mapping_records need to migrate".format(num=num))
    for index, mas_field_mapping_record in enumerate(mas_field_mapping_records):
        records = mas_field_mapping_record_to_publish_field_mapping_records(mas_field_mapping_record)
        logger.info("processing {current}/{num}".format(current=index+1, num=num))
        if records:
            db.session.add_all(records)
            db.session.commit()

with app.app_context():
    backup_table("publish_field_mapping")
    # in case on dev, uat, staging, already deploy newest code(already use the new field mapping table),
    # but didn't run the this migration script
    delete_marketo_mapping_sql = 'delete from ' \
                                 'publish_field_mapping where target_type in ("marketo", "marketo_sandbox")'
    db.engine.execute(delete_marketo_mapping_sql)
    migrate()
