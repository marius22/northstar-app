"""
echo 'python api/script/compare_config.py' > .git/hooks/post-merge & chmod +x .git/hooks/post-merge
"""
from os.path import join

config_name = 'local.config.py'
example_config_name = 'local.config.py.example'


def get_key_from_line(line):
    return line.split('=')[0]


def get_keys_from_config(file_path):
    keys = set()
    with open(file_path, 'r') as f:
        for line in f:
            if line.strip() and line[0].isupper():
                keys.add(get_key_from_line(line))
    return keys

if __name__ == "__main__":
    base = join('api', 'instance')

    config_in_config = \
        get_keys_from_config(join(base, config_name))

    config_in_example = \
        get_keys_from_config(join(base, example_config_name))

    print("\033[93m only in config", config_in_config - config_in_example)
    print("\033[93m only in config example", config_in_example - config_in_config)
