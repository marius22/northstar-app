set @accountName := 'Guest_mcglynn.cullen@mayert.net';
/*
select d.id from account a, user b, segment c, job d where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id and c.id=d.segment_id;
select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id;
select b.id from account a, user b where a.name = @accountName and a.id=b.account_id;
select a.id from account a where a.name=@accountName;
*/

delete from job_status_history where job_id in (select d.id from account a, user b, segment c, job d where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id and c.id=d.segment_id);
delete from recommendation where job_id in (select d.id from account a, user b, segment c, job d where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id and c.id=d.segment_id);
delete from job where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);

delete from qualification where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from expansion where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from sf_pull_request where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from seed_filter where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from seed_candidate where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from seed where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from user_exported_company where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from tmp_segment_stage where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from tmp_account_segment where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from user_download where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from segment_metric where segment_id in (select c.id from account a, user b, segment c where a.name = @accountName and a.id=b.account_id and b.id=c.owner_id);
delete from segment where owner_id in (select b.id from account a, user b where a.name = @accountName and a.id=b.account_id);

delete from user_role where user_id in (select b.id from account a, user b where a.name = @accountName and a.id=b.account_id);
delete from user_segment where user_id in (select b.id from account a, user b where a.name = @accountName and a.id=b.account_id);
delete from crm_token where user_id in (select b.id from account a, user b where a.name = @accountName and a.id=b.account_id);
delete from user_invitation where account_id in (select a.id from account a where a.name = @accountName);
delete from user_invitation where user_id in (select id from user where account_id in (select a.id from account a where a.name = @accountName));
delete from user where account_id in (select a.id from account a where a.name = @accountName);

delete from account_exported_company where account_id in (select a.id from account a where a.name=@accountName);
delete from account_quota where account_id in (select a.id from account a where a.name=@accountName);
delete from account where name=@accountName;
