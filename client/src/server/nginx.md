Our client should support ssl so that salesforce's callback can work

Our client should be view in a domain ended with .everstring.com so that login can work

following is about configure for ssl and domain, using nginx as the proxy server.

## step 1 config /etc/hosts
```
127.0.0.1 ns.everstring.com
```
## step 2 config ssl for nginx
```
cd ~ && mkdir .ssl && sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout .ssl/nginx.key -out .ssl/nginx.crt
```

##  step 3 config proxy for client and api
add following config to nginx(replace /Users/xijinling/.ssh to your own)

```
http {
    # for socket-io
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }
}
```

```
server
{
	listen 443;
    server_name ns.everstring.com;

    ssl on;
    ssl_certificate /Users/xijinling/.ssl/nginx.crt;
    ssl_certificate_key /Users/xijinling/.ssl/nginx.key;

    location ~ ^(.*)\/\.git\/{
          return 404;
    }
    location = /config/server/nginx.conf{
        return 404;
    }
    location / {
        proxy_pass http://localhost:3000;
        proxy_redirect     off;
        proxy_set_header   Host   $http_host;
        proxy_set_header  X-Real-IP         $remote_addr;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
    }
    location /api/ {
        proxy_pass http://localhost:9000/;
        proxy_redirect     off;
        proxy_set_header   Host   $http_host;
        proxy_set_header  X-Real-IP         $remote_addr;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
    }
    location /browser-sync/socket.io/ {
        proxy_pass http://localhost:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
    }
}
server
{
    listen 80;
    server_name ns.everstring.com;
    location / {
        rewrite ^(.*) https://ns.everstring.com$1 permanent;
    }
}
```