import React from 'react'
import ReactSelect from 'react-select'
import 'react-select/dist/react-select.css'

export default class Select extends ReactSelect {
  render() {
    return <ReactSelect {...this.props} isLoading={false} />
  }
}
