import React from 'react'

/**
 * AudienceListCrudText
 *
 * A component that shows the table crud text for an audience/exclusion list.
 */
export default class AudienceListCrudText extends React.Component {
  static propTypes = {
    // The kind of list we're showing crud text for.
    listType: React.PropTypes.oneOf([
      'AUDIENCE',
      'EXCLUSION_LIST',
    ])
  }

  getCreateBtnText() {
    if (this.props.listType === 'AUDIENCE') {
      return 'Create Audience'
    } else {
      return 'Create Exclusion List'
    }
  }

  goToState(stateName) {
    return () => {
      this.props.$state.go(stateName)
    }
  }

  getFindCompaniesElem() {
    if (!this.props.userService.canFindCompanies()) {
      return ''
    }

    return (
      <div className="dropdown-item extender" >
        Find Target Companies Using
        <i className="btr bt-angle-right"></i>
        <div className="choose-find-method-dd extension">
          <div className="dropdown-item" onClick={this.props.findAudienceByKeywords}>
            Keywords
          </div>
          <div className="dropdown-item" onClick={this.props.findAudienceByUrls}>
            Preferred Companies (URLs)
          </div>
          <div className="dropdown-item" onClick={this.props.findAudienceByCsv}>
            Preferred Companies (List)
          </div>
          <div className="dropdown-item" onClick={this.props.findAudienceByFilters}>
            Criteria
          </div>
        </div>
      </div>
    )
  }

  getScoreListElem() {
    if (!this.props.userService.canScoreList()) {
      return ''
    }

    return (
      <div
        className="dropdown-item"
        onClick={this.props.enrichAudience}
      >
        Score List
      </div>
    )
  }

  getAudienceDropdown() {
    return (
      <div
        className="create-audience-dd"
      >
        { this.getFindCompaniesElem() }
        { this.getScoreListElem() }
      </div>
    )
  }

  getExclusionListDropdown() {
    return (
      <div
        className="create-exclusion-dd"
      >
        <div
          className="dropdown-item"
          onClick={this.goToState('exclusion.enrich')}
        >
          By Uploading a List
        </div>
        <div
          className="dropdown-item"
          onClick={this.goToState('exclusion.find.keywords')}
        >
          Using Keywords
        </div>
      </div>
    )
  }

  render() {
    if (!this.props.inited) {
      return <div></div>
    }

    let createAudienceDropdown
    if (this.props.listType === 'AUDIENCE') {
      createAudienceDropdown = this.getAudienceDropdown()
    } else {
      createAudienceDropdown = this.getExclusionListDropdown()
    }

    return (
      <div className="table-crud-text create-audience">
        <i className="btr bt-plus-circle bt-2x"></i>
        <span>{ this.getCreateBtnText() }</span>
        { createAudienceDropdown }
      </div>
    )
  }
}
