import React, { PropTypes } from 'react'
import _ from 'lodash'

import MappingRow, { MappingPropType, OneSidePropType, } from '../MappingRow'

/**
 * Mapping - A component that handles mapping one set of things to another set
 *   of things. For example, we can specify how we map company fields into fields
 *   of an external service, such as Salesforce or Marketo.
 */
export default class Mapping extends React.Component {
  static propTypes = {
    mappings: PropTypes.arrayOf(MappingPropType),
    mapFrom: PropTypes.string.isRequired,
    mapTo: PropTypes.string.isRequired,

    onAddRow: PropTypes.func,
    onSave: PropTypes.func,

    // To indicate loading, we'll show blank rows. This is the number of blank rows to show.
    // If 0 is passed, we'll show real data.
    numLoadingRows: PropTypes.number
  }

  static defaultProps = {
    isLoading: false,
  }

  getAddRow() {
    if (!this.props.onAddRow) {
      return null
    }

    const rowStyle = {
      cursor: 'pointer',
    }

    return (
      <tr className="add-row" onClick={this.props.onAddRow} style={rowStyle}>
        <td colSpan="5" className="align-center add-row-cell">
          <i className="add-row-plus btr bt-plus-circle"></i>
        </td>
      </tr>
    )
  }

  getRows() {
    if (this.props.numLoadingRows) {
      return _.fill(Array(this.props.numLoadingRows), {
        leftSide: {
          choices: [],
          selectedChoice: null,
        },
        rightSide: {
          choices: [],
          selectedChoice: null,
        },
      }).map((props) => {
        return <MappingRow {...props} />
      })
    }

    return this.props.mappings.map((mapping) => <MappingRow {...mapping} />)
  }

  getSaveButton() {
    if (!this.props.onSave) {
      return null
    }

    return (
      <div className="btn-wrapper">
        <div className="save" onClick={this.props.onSave}>
          Save
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="mappings-wrapper">
        <table className="table">
          <thead>
            <tr>
              <th className="align-center">
                {this.props.mapFrom}
              </th>
              <th className="align-center">
                Maps To
              </th>
              <th className="align-center">
                {this.props.mapTo}
              </th>
              <th className="align-center">
                Delete
              </th>
              <th className="align-center">
                Last Updated
              </th>
            </tr>
          </thead>

          <tbody>
            { this.getRows() }
            { this.getAddRow() }
          </tbody>
        </table>

        { this.getSaveButton() }
      </div>
    )
  }
}
