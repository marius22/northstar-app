import _ from 'lodash'
import moment from 'moment'

import React, { PropTypes } from 'react'

import Select from 'components/Select'

export const ChoicePropType =
  PropTypes.shape({
    value: PropTypes.any.isRequired,
    label: PropTypes.string.isRequired,
    // Whether we *must* make a selection for this choice
    isRequired: PropTypes.bool,
  })

const OneSidePropType = PropTypes.shape({
  choices: PropTypes.arrayOf(ChoicePropType),
  selectedChoice: ChoicePropType,

  // If we don't want the user to be able to choose a value for a side, then
  // we pass this in.
  isFixedChoice: PropTypes.bool,

  // Set to true if we don't consider it an error to let this side to be blank.
  allowBlank: PropTypes.bool,

  // If present, we'll show an info icon with a tooltip for this side of the mapping.
  tooltipContent: PropTypes.string,
})


export const MappingPropType = PropTypes.shape({
  leftSide: OneSidePropType,
  rightSide: OneSidePropType,
})

export const getChoiceFromSide = (side) => {
  return side.selectedChoice
}


/**
 * A component that allows you to map one thing to another thing.
 */
export default class MappingRow extends React.Component {
  static propTypes = {
    // The stuff on the left side of the mapping, i.e. the stuff on our
    // end that is being mapped
    leftSide: OneSidePropType.isRequired,
    rightSide: OneSidePropType.isRequired,

    onSelectLeft: PropTypes.func,
    onSelectRight: PropTypes.func,

    // Called once there's a left and a right value.
    onMappingComplete: PropTypes.func,

    // Callback called when this mapping is deleted.
    onDelete: PropTypes.func,
    // Flag set if mapping is currently saved to the db.
    // If true, then we prompt the user with an "are you sure?"
    // upon clicking the delete button.
    needsServerDelete: PropTypes.bool,

    // Timestamp of the last time this row was updated
    updatedTimestamp: PropTypes.number,
  }

  static defaultProps = {
    onSelectLeft: () => {},
    onSelectRight: () => {},
    onMappingComplete: () => {},
    onMappingIncomplete: () => {},
  }

  constructor(props) {
    super(props)

    this.state = {
      // Whether to show the alert icon the right side mapping cell.
      showRightSideMissingMapping: false,
    }
  }

  /**
   * Get the cell for a mapping element for one side.
   * @param side - The side of the mapping we're getting a cell for,
   *   either 'left' or 'right'.
   * @param onSelect - The callback called after a selection is made.
   */
  getOneSideCell(side, onSelect = () => {}) {
    if (side.isFixedChoice) {
      return (
        <td className="center">
          { side.selectedChoice ? side.selectedChoice.label : ''}
          { side.selectedChoice && side.selectedChoice.isRequired ? <span className="text-required"> (required) </span> : ''}
          { this.getExplanationTooltip(side) }
        </td>
      )
    } else {
      const shouldShowRightSideMissingMapping = () => {
        const leftSideChoice = getChoiceFromSide(this.props.leftSide)
        const rightSideChoice = getChoiceFromSide(this.props.rightSide)
        const onlyHasLeftChoice = leftSideChoice && !rightSideChoice && side === this.props.rightSide

        return onlyHasLeftChoice && !side.allowBlank
      }

      const onChange = (value) => {
        const oldChoice = getChoiceFromSide(side)
        side.selectedChoice = _.find(side.choices, (choice) => {
          return choice === value || choice.value === value
        })
        const leftSideChoice = getChoiceFromSide(this.props.leftSide)
        const rightSideChoice = getChoiceFromSide(this.props.rightSide)
        if (leftSideChoice && rightSideChoice) {
          this.props.onMappingComplete(leftSideChoice, rightSideChoice)
        } else {
          this.props.onMappingIncomplete(leftSideChoice, rightSideChoice)
        }

        if (shouldShowRightSideMissingMapping()) {
          this.setState({showRightSideMissingMapping: true})
        } else {
          this.setState({showRightSideMissingMapping: false})
        }

        onSelect(oldChoice, getChoiceFromSide(side))
      }

      this.state.showRightSideMissingMapping = shouldShowRightSideMissingMapping()

      return (
        <td className="text-center">
          <div className="cell-content-wrapper">
            {
              this.state.showRightSideMissingMapping ?
              <i className="fa fa-exclamation-circle"></i> :
              ''
            }
            <Select
              options={side.choices}
              value={side.selectedChoice}
              onChange={onChange}
              clearable={false}
            />
            { this.getExplanationTooltip(side) }
          </div>
        </td>
      )
    }
  }

  getExplanationTooltip(side) {
    const choice = getChoiceFromSide(side)
    const isLeftSide = side === this.props.leftSide
    const shouldShow = choice && isLeftSide && side.tooltipContent

    if (shouldShow) {
      return (
        <i className="fa fa-info-circle">
          <div className="tooltip-window">
            { side.tooltipContent }
          </div>
        </i>
      )
    } else {
      return ''
    }
  }

  getDeleteCell() {
    const blankCell = (<td className="align-center"></td>)
    if (!this.props.onDelete) {
      return blankCell
    }

    if (_.get(this.props.leftSide, 'selectedChoice.isRequired') ||
        _.get(this.props.rightSide, 'selectedChoice.isRequired')) {
      return blankCell
    }

    return (
      <td className="align-center">
        <i className="btr bt-trash"
          onClick={() => {this.props.onDelete()}}>
        </i>
      </td>
    )
  }

  getTimestampCell() {
    if (!this.props.updatedTimestamp) {
      return (
        <td className="align-center">
          --
        </td>
      )
    }

    return (
      <td className="align-center">
        { moment(this.props.updatedTimestamp * 1000).format('lll') }
      </td>
    )
  }

  render() {
    return (
      <tr>
        { this.getOneSideCell(this.props.leftSide, this.props.onSelectLeft) }

        <td className="align-center">
          <i className="btr bt-long-arrow-right bt-2x color-pri1"></i>
        </td>

        { this.getOneSideCell(this.props.rightSide, this.props.onSelectRight) }

        { this.getDeleteCell() }

        { this.getTimestampCell() }
      </tr>
    )
  }
}
