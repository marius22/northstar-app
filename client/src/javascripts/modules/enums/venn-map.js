import _ from 'lodash'

const vennMap = {
  1: {
    name: 'Businesses',
    components: [1, -2, -4], // means section1 - section2 - section4
    backEnd: 1 // used as fontEnd -> backEnd mapping
  },
  2: {
    name: 'Technologies',
    components: [2, -1, -4],
    backEnd: 2
  },
  4: {
    name: 'Departments',
    components: [-1, -2, 4],
    backEnd: 3
  },
  3: {
    components: [1, 2, -4],
    backEnd: 4
  },
  6: {
    components: [-1, 2, 4],
    backEnd: 6
  },
  5: {
    components: [1, -2, 4],
    backEnd: 5
  },
  7: {
    components: [1, 2, 4],
    backEnd: 7
  }
}

/*
 * convert components to description
 * @return array
 * */
function componentsDetail(section) {
  if (!(section in vennMap)) {
    return []
  }
  return _.map(vennMap[section].components, (component) => {
    const selected = component > 0
    return {
      name: vennMap[Math.abs(component)].name,
      selected
    }
  })
}

module.exports = {vennMap, componentsDetail}
