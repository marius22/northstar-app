const CLOSE = 'Close'
const SFDCLabels = {
  NO_OAUTH: {
    title: 'Error',
    messageClass: 'error',
    message: 'We have detected that you have not integrated with SFDC. <br>Would you like to do it now?'
  },
  WAITING_OAUTH: {
    title: 'Error',
    messageClass: 'error',
    message: 'Waiting for you to integrate with SFDC.'
  },
  OAUTH_FAIL: {
    title: 'Error',
    messageClass: 'error',
    message: 'Oh Snap! <br>There was a problem connecting to SFDC. <br>Would you like to try again?'
  },
  HAVE_PERMISSION_INSTALL_PACKAGE: {
    title: 'Error',
    messageClass: 'error',
    message: 'Salesforce package is not installed. <br />Would you like to do that now?'
  },
  PACKAGE_NOT_INSTALLED: {
    title: 'Error',
    messageClass: 'error',
    message: 'Salesforce package is not installed. <br />Would you like to do that now?'
  },
  WAITING_INSTALL: {
    title: 'Error',
    messageClass: 'error',
    message: 'Waiting for you to install Salesforce package.'
  },
  INSTALL_PACKAGE_FAIL: {
    title: 'Error',
    messageClass: 'error',
    message: 'Oh Snap! <br>There was a problem installing Salesforce package. ' +
    '<br>Would you like to try again?'
  },
  NO_PERMISSION_INSTALL_PACKAGE: {
    title: 'Error',
    messageClass: 'error',
    message: 'You need administrative permissions to complete this process. <br>We have contacted customer success ' +
    'and they will be in touch soon.',
    btnConfirm: CLOSE,
    hideDenyButton: true
  },
  INSTALL_PACKAGE_SUCCESS: {
    title: 'Info',
    // we don't want mislead user that everything is done, so give an info message instead of success message here
    messageClass: 'info',
    message: 'Package Installed Successfully!<br>Continue to export by clicking button below'
  },
  DEFAULT: {
    title: 'Error',
    messageClass: 'error',
    message: 'There was a problem with your export. <br>We have contacted customer success ' +
    'and they will be in touch soon.',
    btnConfirm: CLOSE,
    hideDenyButton: true
  }
}

module.exports = {SFDCLabels, CLOSE}
