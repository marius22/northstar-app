// These enums match the ones in northstar-app/api/ns/model/salesforce.py
const SALESFORCE = {
  ACCOUNT_CONTACT: 'account_contact',
  ACCOUNT: 'account',
  CONTACT: 'contact',
  LEAD: 'lead',
}

export {
  SALESFORCE
}
