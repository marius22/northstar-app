const SegmentStatus = {
  EMPTY: 'EMPTY',
  COMPLETED: 'COMPLETED',
  DRAFT: 'DRAFT',
  IN_PROGRESS: 'IN PROGRESS'
}

module.exports = {SegmentStatus}
