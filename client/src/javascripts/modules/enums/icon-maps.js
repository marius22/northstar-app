const iconMaps = {
  error: 'bt-ban',
  warning: 'bt-exclamation-circle',
  info: 'bt-info-circle',
  success: 'bt-check-circle'
}

const getIconName = (type) => {
  return iconMaps[type] || iconMaps.info
}

module.exports = {iconMaps, getIconName}
