// public Pages don't need authentication
const publicPages = [
  '/user/login',
  '/user/sign-up',
  '/user/pass-sent',
  '/404',
  '/chrome-confirm',
  '/timeout'
]

module.exports = {publicPages}
