import _ from 'lodash'

class Color {
  constructor({red, green, blue}) {
    this.red = red
    this.green = green
    this.blue = blue
  }

  toString() {
    return `rgb(${this.red}, ${this.green}, ${this.blue})`
  }

  darkenBy(darkenFactor) {
    return new Color({
      red: this.red * darkenFactor,
      blue: this.blue * darkenFactor,
      green: this.green * darkenFactor,
    })
  }
}

const GALAXY_COLORS = {
  astronaut: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 36, green: 48, blue: 102}),
      new Color({red: 51, green: 68, blue: 147}),
      new Color({red: 63, green: 83, blue: 178}),
      new Color({red: 111, green: 126, blue: 204})
    ],
    businessTagColor: new Color({red: 173, green: 182, blue: 226})
  },
  elm: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 36, green: 106, blue: 121}),
      new Color({red: 47, green: 139, blue: 159}),
      new Color({red: 55, green: 164, blue: 188}),
      new Color({red: 105, green: 193, blue: 211})
    ],
    businessTagColor: new Color({red: 170, green: 219, blue: 230})
  },
  forestGreen: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 40, green: 136, blue: 43}),
      new Color({red: 47, green: 159, blue: 50}),
      new Color({red: 56, green: 189, blue: 58}),
      new Color({red: 121, green: 215, blue: 123})
    ],
    businessTagColor: new Color({red: 160, green: 226, blue: 163})
  },
  purple: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 92, green: 37, blue: 122}),
      new Color({red: 130, green: 52, blue: 171}),
      new Color({red: 153, green: 70, blue: 198}),
      new Color({red: 190, green: 136, blue: 219})
    ],
    businessTagColor: new Color({red: 218, green: 186, blue: 235})
  },
  venice: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 10, green: 92, blue: 160}),
      new Color({red: 13, green: 126, blue: 219}),
      new Color({red: 42, green: 151, blue: 242}),
      new Color({red: 119, green: 189, blue: 247})
    ],
    businessTagColor: new Color({red: 179, green: 218, blue: 251})
  },
  eucalyptus: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 37, green: 118, blue: 98}),
      new Color({red: 46, green: 146, blue: 121}),
      new Color({red: 56, green: 178, blue: 149}),
      new Color({red: 108, green: 208, blue: 184})
    ],
    businessTagColor: new Color({red: 176, green: 230, blue: 217})
  },
  claret: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 120, green: 29, blue: 63}),
      new Color({red: 159, green: 38, blue: 84}),
      new Color({red: 191, green: 46, blue: 99}),
      new Color({red: 217, green: 94, blue: 140})
    ],
    businessTagColor: new Color({red: 233, green: 160, blue: 188})
  },
  meteor: {
    lastUsedNodeColorIdx: -1,
    nodeColors: [
      new Color({red: 206, green: 123, blue: 11}),
      new Color({red: 242, green: 145, blue: 13}),
      new Color({red: 245, green: 168, blue: 63}),
      new Color({red: 249, green: 199, blue: 132})
    ],
    businessTagColor: new Color({red: 251, green: 217, blue: 170})
  }
}

/**
 * The colors used in different parts of the galaxy app
 */
class GalaxyColorPalette {
  constructor() {
    this.paletteNames = _.keys(GALAXY_COLORS)
    this.lastUsedPaletteIdx = -1
  }

  /**
   * Add color information to clusters
   *
   * @param {Object[]} clusters - The cluster to add a color to.
   * @param {string} clusters[].clusterID - The cluster's ID. Each cluster has a single color.
   */
  colorClusters(clusters) {
    return _.map(clusters, cluster => {
      const colorObj = this.getNextNodeColor()
      const businessTagColor = GALAXY_COLORS[colorObj.palette].businessTagColor
      const nodeColor = colorObj.nodeColor

      return _.extend({}, cluster, {businessTagColor, nodeColor})
    })
  }

  /**
   * Get next color based on what colors we've already used.
   */
  getNextNodeColor() {
    // We'll use the first color of the first palette. Then we'll use the first color of the next
    // palette, etc. Once each palette has had a single color used, we'll re-use palettes.
    const nextPaletteIdx = (this.lastUsedPaletteIdx + 1) % this.paletteNames.length
    const numColors = GALAXY_COLORS[this.paletteNames[nextPaletteIdx]].nodeColors.length

    const nextPalette = this.paletteNames[nextPaletteIdx]
    const paletteObj = GALAXY_COLORS[nextPalette]
    const nextColorIdx = (paletteObj.lastUsedNodeColorIdx + 1) % numColors

    paletteObj.lastUsedNodeColorIdx = nextColorIdx
    this.lastUsedPaletteIdx = nextPaletteIdx

    return {
      palette: nextPalette,
      nodeColor: paletteObj.nodeColors[nextColorIdx]
    }
  }

  /**
   * used in audience word cloud
   */
  getColorList() {
    const colors = []
    _.map(GALAXY_COLORS, item => {
      _.map(item.nodeColors, color => {
        colors.push(color.toString())
      })
    })
    return colors
  }
}

module.exports = {GalaxyColorPalette}
