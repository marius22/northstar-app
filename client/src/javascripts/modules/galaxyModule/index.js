import * as colors from './colors'

import $ from 'npm-zepto'
import Handlebars from 'handlebars'
import Promise from 'bluebird'
import URI from 'urijs'
import _ from 'lodash'
import csv from 'csv'
import cytoscape from 'cytoscape'
import moment from 'moment'
import request from 'superagent'

const colorPalette = new colors.GalaxyColorPalette()

// Size, in px, of the border on company nodes.
const companyBorderWidth = 1


const DEBUG = true


// START GALAXY_DEMO_CODE
const currentDataSource = 'apttus_v1_density0_1'
// END GALAXY_DEMO_CODE

// Parse csv data describing the graph.
const parseDataCSVAsync = (csvData, columnNames) => {
  return new Promise((resolve, reject) => {
    csv.parse(csvData, (err, data) => {
      if (err) {
        reject(err)
      }
      // const headerRow = _.slice(data, 0, 1)
      const dataValues = _.slice(data, 1)

      const jsonData = _.map(dataValues, (row) => {
        const datum = {}
        _.forEach(row, (entry, idx) => {
          datum[columnNames[idx]] = entry
        })
        return datum
      })
      resolve(jsonData)
    })
  })
}

// Convert a node into a cy node.
const createCyNode = ({node, nodeType, classes = '', coloredClusters = []}) => {
  // Scale factor depends on the aspect ratio the non-overlap algorithm was fed. It's
  // 1:1 right now.

  const xScaleFactor = document.documentElement.clientWidth
  const yScaleFactor = document.documentElement.clientWidth

  // const x = nodeType === 'company' ? node.nonOverlapX : node.x
  // const y = nodeType === 'company' ? node.nonOverlapY : node.y
  const x = node.nonOverlapX
  const y = node.nonOverlapY

  const position = {
    x: x * xScaleFactor,
    y: y * yScaleFactor
  }

  let width = 0
  let height = 0
  if (nodeType === 'company') {
    width = `${xScaleFactor * node.radius * 2 - companyBorderWidth}px`
    height = width
  }

  const label = (() => {
    switch (nodeType) {
      case 'company':
        return node.domain
      case 'cluster':
        return node.label
      default:
        throw new Error(`Unknown nodeType: ${nodeType}`)
    }
  })()

  const id = (() => {
    switch (nodeType) {
      case 'company':
        return node.id
      case 'cluster':
        return `cluster${node.clusterID}`
      default:
        return ''
    }
  })()

  const color = (() => {
    const curCluster = _.find(coloredClusters, {clusterID: node.clusterID})
    switch (nodeType) {
      case 'company':
        return curCluster.nodeColor
      case 'cluster':
        return curCluster.businessTagColor
      default:
        return ''
    }
  })()

  const borderColor = color.darkenBy(0.8)

  return {
    data: {
      id,
      clusterID: node.clusterID,
      nodeType,
      color: color.toString(),
      borderColor: borderColor.toString(),
      position: `(${position.x}, ${position.y})`,
      domain: node.domain,
      label,
      width,
      height
    },
    classes,
    position,
    locked: true,
    // Don't allow users to select and move nodes around.
    grabbable: false,
    selectable: false,
    autolock: true
  }
}

// Convert an edge into a cy edge.
const createCyEdge = (edgeObj) => {
  return {
    data: {
      source: edgeObj.srcID,
      target: edgeObj.destID
    },
    locked: true,
    // Don't allow users to select and move edges around.
    grabbable: false,
    selectable: false
  }
}

// Get node data from server.
const fetchNodesAsync = () => {
  const nodesP = new Promise((resolve, reject) => {
    request.get(`/${currentDataSource}/nodes.csv`)
      .end((err, res) => {
        if (err) {
          reject(err)
        }

        const csvData = res.text
        const columnNames = [
          'id',
          'domain',
          // String ID of the cluster this belongs to.
          'clusterID',
          // Size normalized from 0 to 1.
          'radius',
          // Color (string) specified as a hex string like #0ff0022
          'color',
          // x and y coordinates
          'x',
          'y',
          'nonOverlapX',
          'nonOverlapY'
        ]

        resolve(parseDataCSVAsync(csvData, columnNames))
      })
  })

  return nodesP
}

// Get edge data from server.
const fetchEdgesAsync = () => {
  const edgesP = new Promise((resolve, reject) => {
    request.get(`/${currentDataSource}/edges.csv`)
      .end((err, res) => {
        if (err) {
          reject(err)
        }

        const csvData = res.text

        const columnNames = [
          // ID of the src and dest node of this edge.
          'srcID',
          'destID',
          // Weight from 0 to 1 of this edge.
          'weight',
          // Type is "Undirected" or "Directed"
          'type'
        ]

        resolve(parseDataCSVAsync(csvData, columnNames))
      })
  })
  return edgesP
}

// Get cluster data from server.
const fetchClusterDataAsync = () => {
  const clusterP = new Promise((resolve, reject) => {
    request.get(`/${currentDataSource}/cluster_data.csv`).end(
      (err, res) => {
        if (err) {
          reject(err)
        }

        const csvData = res.text

        const columnNames = [
          'clusterID',
          // Number of nodes in cluster
          'size',
          'sampleDomains',
          '1Grams',
          '2Grams',
          '3Grams',
          '4Grams',
          '1GramTags',
          '2GramTags',
          '3GramTags',
          '4GramTags',
          // Positioning for cluster tag
          'x',
          'y',
          'nonOverlapX',
          'nonOverlapY'
        ]

        resolve(parseDataCSVAsync(csvData, columnNames))
      })
  })

  return clusterP.then((clusters) => {
    const clustersWithLabels = _(clusters).map((cluster) => {

      // const justPotentialNames = _(cluster).pick([
      //   '1Grams',
      //   '3Grams',
      //   'sampleDomains',
      //   '4Grams',
      //   '1GramTags',
      //   '2GramTags',
      //   '3GramTags',
      //   '4GramTags'
      // ]).mapValues((commaDelimitedValues) => {
      //   return commaDelimitedValues.split(',')
      // }).value()

      // const label = _(_.values(justPotentialNames))
      //         .flatten().take(3).value().join(', ')
      const label = cluster['2Grams'].split(',')[0]

      return _.extend(cluster, {label})
    }).value()

    return clustersWithLabels
  })
}

const loadEvents = (cy) => {
  const $galaxyCont = $('.galaxy-cont')
  const $galaxy = $('.galaxy')

  // Show galaxy, hide loading screen.
  $galaxy.addClass('show')
  $('.loading').addClass('hide')
  cy.fit()

  // Toggle cursor visual grabbing state.
  $galaxy.on('mousedown', () => {
    $galaxy.addClass('grabbing')
  }).on('mouseup', () => {
    $galaxy.removeClass('grabbing')
  })

  // Hide panel when clicking left-caret
  $galaxyCont.on('click', '.company-info-panel .hide-panel', () => {
    const $companyInfoPanel = $galaxyCont.find('.company-info-panel')
    $companyInfoPanel.removeClass('show')
    cy.$('node.company').removeClass('dim')
    cy.$('node.clusterLabel').addClass('show')
  })


  // Dim other clusters on hover.
  // cy.on('mouseover', 'node.company', (evt) => {
  //   const activeNodeClusterID = evt.cyTarget.data('clusterID')
  //   const otherClusterNodes = cy.filter((idx, node) => {
  //     return node.data('nodeType') === 'company' && node.data('clusterID') !== activeNodeClusterID
  //   })
  //   const otherClusterLabels = cy.filter((idx, node) => {
  //     return node.data('nodeType') === 'cluster' && node.data('clusterID') !== activeNodeClusterID
  //   })
  //   otherClusterNodes.addClass('dim')
  //   otherClusterLabels.removeClass('show')
  // })
  // cy.on('mouseout', 'node.company', (evt) => {
  //   cy.$('node.company').removeClass('dim')
  //   cy.$('node.clusterLabel').addClass('show')
  // })

  // Show panel on click
  cy.on('click', (evt) => {
    if (evt.cyTarget === cy) {
      // Clicked on background
      // cy.$('node.company').removeClass('dim')
      // cy.$('node.clusterLabel').addClass('show')
    } else {
      const zoomLevel = cy.zoom()
      if (zoomLevel < 1) {
        return
      }
      if (evt.cyTarget.hasClass('dim')) {
        return
      }

      const $existingPanel = $galaxyCont.find('.company-info-panel')
      const alreadyOpened = $existingPanel.length > 0 && $existingPanel.hasClass('show')
      $existingPanel.remove()

      const hbInfoPanelTmpl = Handlebars.compile($('#company-info-panel-template').html())
      const infoPanelHTML = hbInfoPanelTmpl({
        domain: evt.cyTarget.data('domain'),
        alreadyOpened
      })
      $galaxyCont.append(infoPanelHTML)
      const $panelToShow = $galaxyCont.find('.company-info-panel')
      // Force reflow. Without this, the `left` won't `transition`, it'll just change immediately.
      /* eslint-disable */
      $panelToShow[0].offsetHeight
      /* eslint-enable */
      $panelToShow.addClass('show')

      const activeNodeClusterID = evt.cyTarget.data('clusterID')
      const otherClusterNodes = cy.filter((idx, node) => {
        return node.data('nodeType') === 'company' && node.data('clusterID') !== activeNodeClusterID
      })
      const otherClusterLabels = cy.filter((idx, node) => {
        return node.data('nodeType') === 'cluster' && node.data('clusterID') !== activeNodeClusterID
      })
      otherClusterNodes.addClass('dim')
      otherClusterLabels.removeClass('show')
    }
  })

  // cy.on('zoom', (evt) => {
  //   const zoomLevel = cy.zoom()
  //   console.log("zoomLevel: ", zoomLevel);
  //   const newFontSize = 60 * zoomLevel
  //   cy.style()
  //     .selector('node.clusterLabel')
  //     .style({
  //       'font-size': (ele) => {
  //         return `${newFontSize}px`
  //       }
  //     })
  //     .update()
  // })
}


const renderGraph = () => {
  // START GALAXY_DEMO_CODE
  const queryObj = URI.parseQuery(URI.parse(location.href).query)
  const showDemoGalaxy = queryObj && queryObj.showDemoGalaxy === '1'
  if (showDemoGalaxy) {
    // Show real galaxy
    $('.galaxy-cont').removeClass('hide')
  } else {
    // Show request tab
    $('.request-galaxy-cont').removeClass('hide')
  }
  // END GALAXY_DEMO_CODE

  const startTime = moment()
  Promise.join(
    fetchNodesAsync(), fetchEdgesAsync(), fetchClusterDataAsync(),
    (nodes, edges, clusters) => {
      if (DEBUG) {
        console.info('Number of nodes: ', nodes.length)
        console.info('Number of edges: ', edges.length)
      }

      const coloredClusters = colorPalette.colorClusters(clusters)
      const companyNodes = _.map(nodes, (node) => {
        return createCyNode({node, nodeType: 'company', classes: 'company',
                             coloredClusters})
      })

      const clusterLabelCyNodes = _.map(clusters, (cluster) => {
        return createCyNode({node: cluster, nodeType: 'cluster',
                             classes: 'clusterLabel show',
                             coloredClusters})
      })

      const cyNodes = _.concat(companyNodes, clusterLabelCyNodes)

      const cy = cytoscape({
        container: $('.galaxy')[0],
        elements: {
          nodes: cyNodes
        },
        style: [
          {
            selector: 'node.company',
            style: {
              'background-color': 'data(color)',
              color: '#fff',
              width: 'data(width)',
              height: 'data(height)',
              'border-width': `${companyBorderWidth}px`,
              'border-color': 'data(borderColor)'
            }
          },
          {
            selector: 'node.dim',
            style: {
              opacity: '0.2'
            }
          },
          {
            selector: 'node.hide',
            style: {
              display: 'none'
              // label: 'asdf'
            }
          },
          {
            selector: 'node.clusterLabel',
            style: {
              color: 'data(color)',
              'background-color': '#f00',
              label: 'data(label)',
              display: 'none',
              'font-size': 16,
              'text-transform': 'uppercase',
              // Give label node no size so that it can't be interacted with.
              width: 0,
              height: 0
            }
          },
          {
            selector: 'node.clusterLabel.show',
            style: {
              display: 'element'
            }
          },
          {
            selector: 'edge',
            style: {
              // Haystack edges are straight lines, which are less expensive to
              // render than bezier edges.
              'curve-style': 'haystack',
              display: 'none'
            }
          },
          {
            selector: 'edge.show',
            style: {
              display: 'element'
            }
          }
        ],
        layout: {
          // The 'preset' layout respects the positions you assign to your nodes.
          // Other layout values will layout your nodes for you.
          name: 'preset'
        },
        // Makes panning smoother by using a texture during panning and zooming
        // instead of drawing the elements
        textureOnViewport: true,

        // Hiding stuff while panning and zooming increases performance but
        // is a little jarring. Set these to `true` to hide what you want to hide.
        hideEdgesOnViewport: false,
        hideLabelsOnViewport: false,
        // Improves perceived performance to make transitions between frames smoother.
        motionBlur: true,
        minZoom: 1e-1
      })
      loadEvents(cy)

      const endTime = moment()
      const runTime = endTime.diff(startTime, 'seconds')
      if (DEBUG) {
        console.info(`Galaxy loaded in ${runTime} seconds`)
      }
    })

}

const loadCachedData = () => {
  const cy = cytoscape({container: $('.galaxy')[0]})

  request.get(`${currentDataSource}/graph_data.json`)
    .end((err, res) => {
      const data = res.text

      cy.json(_.extend({}, data, {
        container: $('.galaxy')[0],
      }))
      loadEvents(cy)
      console.log('cached data load done');
    })
}

module.exports = {
  renderGraph, loadCachedData
}
