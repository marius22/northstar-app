const config = (() => {
  if (process.env.ENV === 'prod') {
    return require('./config.prod')
  } else if (process.env.ENV === 'uat') {
    return require('./config.uat')
  } else if (process.env.ENV === 'dev') {
    return require('./config.dev')
  } else if (process.env.ENV === 'demo') {
    return require('./config.demo')
  } else if (process.env.ENV === 'local') {
    return require('./config.local')
  } else if (process.env.ENV === 'staging') {
    return require('./config.staging')
  }

  throw new Error(`Invalid ENV value: ${process.env.ENV}`)
})()

config.notProduction = () => {
  return process.env.ENV !== 'prod'
}

module.exports = config
