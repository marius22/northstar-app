module.exports = {
  apiBaseURL: 'http://dockerhost:9000/app/',
  nsServiceBaseUrl: 'http://dockerhost.com:9000/service/',
  googleAnalyticsKey: 'UA-76352342-1',
  intercom: {
    appId: 'tnb0is1b'
  },
  rollbar: {
    accessToken: 'a2167c7e10cc419384b4cefcae303eed',
  },
  // timeout should be equal to backend setting for session timeout
  timeout: 30 * 60 * 1000, // milliseconds
}
