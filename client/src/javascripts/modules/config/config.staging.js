module.exports = {
  apiBaseURL: '/app/',
  nsServiceBaseUrl: '/service/',
  googleAnalyticsKey: 'UA-76352342-1',
  intercom: {
    appId: 'tnb0is1b',
  },
  rollbar: {
    accessToken: 'a2167c7e10cc419384b4cefcae303eed',
  },
  timeout: 30 * 60 * 1000,
}
