// docs http://stackoverflow.com/questions/16427636/check-if-localstorage-is-available
const hasStorage = (() => {
  try {
    const mod = 'mod'
    localStorage.setItem(mod, mod)
    localStorage.removeItem(mod)
    return true
  } catch (exception) {
    return false
  }
})()

const GHOST_CONFIG = 'ghostConfig'

class NsStorage {
  constructor() {
    this.hasStorage = hasStorage
    this.store = localStorage
    this.data = {}
    this.GHOST_CONFIG = GHOST_CONFIG
  }

  config(flag) {
    this.hasStorage = !!flag
  }

  useStorage() {
    return this.hasStorage
  }

  get(key) {
    return this.hasStorage ? this.store.getItem(key) : this.data[key]
  }

  set(key, value) {
    if (this.hasStorage) {
      this.store.setItem(key, JSON.stringify(value))
    } else {
      this.data[key] = value
    }
  }

  remove(key) {
    if (this.hasStorage) {
      this.store.removeItem(key)
    } else {
      delete this.data[key]
    }
  }
}

const nsStorage = new NsStorage()
export default nsStorage
