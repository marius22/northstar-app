import 'velocity-animate'
import $ from 'npm-zepto'

/**
 * Animates a tooltip to translate/scale into the associated info icon.
 * @param tooltipSelector - The selector string for the tooltip we're animating
 * @param infoIconSelector - The selector string of the icon to translate/scale the tooltip into.
 */
const animateTooltip = (tooltipSelector, infoIconSelector) => {
  return new Promise((resolve, reject) => {
    const $introWindow = $(tooltipSelector)
    const $infoCircle = $(infoIconSelector)
    const duration = 200
    // For tooltips without an info icon, just scale them to 0
    if (!infoIconSelector) {
      $introWindow.velocity({
        scale: 0
      }, {
        duration,
        complete: () => {
          resolve()
        }
      })
      return
    }

    // The x and y coordinates of the center of the info icon
    const x = $infoCircle[0].getBoundingClientRect().left + $infoCircle.width() / 2
    const y = $infoCircle[0].getBoundingClientRect().top + $infoCircle.height() / 2

    const infoWindowOriginX = $introWindow[0].getBoundingClientRect().left
    const infoWindowOriginY = $introWindow[0].getBoundingClientRect().top
    const width = $introWindow.width()
    const height = $introWindow.height()

    // The x and y coordinates of the center of the info windows
    const infoWindowX = infoWindowOriginX + width * 0.5
    const infoWindowY = infoWindowOriginY + height * 0.5

    // The amounts needed to translate the info windows into the info icons.
    const transXAmount = x - infoWindowX
    const transYAmount = y - infoWindowY

    $introWindow
      .velocity({
        translateX: `${transXAmount}px`,
        translateY: `${transYAmount}px`,
        scale: 0,
      }, {
        duration,
        complete: () => {
          resolve()
        }
      })
  })
}

module.exports = {animateTooltip}
