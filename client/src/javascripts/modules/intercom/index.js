import $ from 'npm-zepto'
import _ from 'lodash'
import * as config from 'config'

export const CONSTANTS = {
  INVITE_USER: 'INVITE_USER',
  AUDIENCE_CREATED: 'AUDIENCE_CREATED',
}

// True if there is an Intercom instance corresponding to a user
// already
let booted = false

// `window.Intercom` comes from a <script> tag we load in <head>
// In case intercom isn't available for whatever reason, we have intercom be a function
// that does nothing.
const intercom = window.Intercom || _.noop

/**
 * Module that handles integration with Intercom.
 * Docs: http://bit.ly/1VWMgof
 */
export class Intercom {
  /**
   * We need to boot on every page. Calling boot more than once
   * does nothing.
   */
  static boot(user) {
    if (booted) {
      return
    }

    intercom("boot", {
      // Unique Intercom identifier.
      app_id: config.intercom.appId,
      // User's full name.
      name: [user.firstName, user.lastName].join(' '),
      // Email address.
      email: user.email,
      // Signup date as a Unix timestamp.
      created_at: user.createdTs,
      company: {
        id: user.accountId,
        name: user.accountName,
      },
      // Custom attributes. We define these on the Intercom app. If a value is missing, we set is as null.
      Status: user.status || null,
      "Account ID": user.accountId || null,
      "Account Name": user.accountName || null,
      Role: _.get(user, 'roles[0]', null)
    })
    booted = true
  }

  /**
   * for unit test
   * @returns {boolean}
   */
  static isBoot() {
    return booted
  }

  /**
   * Intercom wants us to call this whenever a user signs out.
   */
  static shutDown() {
    intercom('shutdown')
    booted = false
  }

  /**
   * We need to call this every time the user's route changes
   */
  static update() {
    intercom('update')
  }

  /**
   * Hide all intercom elements.
   */
  static hide() {
    const $intercom = $('#intercom-container')
    $intercom.hide()
  }

  /**
   * Show all intercom elements.
   */
  static show() {
    const $intercom = $('#intercom-container')
    $intercom.show()
  }

  /**
   * Open intercom window pre-populated by msg
   */
  static showNewMessage(msg) {
    intercom('showNewMessage', msg)
  }

  // Event handling
  static on(evt, cb) {
    intercom(evt, cb)
  }

  static track(evtName) {
    intercom('trackEvent', evtName)
  }
}
