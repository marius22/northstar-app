/**
 * Rollbar is an error dashboard. It'll detect uncaught exceptions in its UI
 * at rollbar.com. Ask @jasoncyu if you want access.
 */
// Rollbar browser js docs: http://bit.ly/1YAdqzx
import * as config from 'config'
const rollbarModule = require('vendors/rollbar.umd.nojson.js')

let Rollbar = undefined

if (process.env.ENV === 'prod') {
  const rollbarConfig = {
    accessToken: config.rollbar.accessToken,
    // Capture uncaught exceptions.
    captureUncaught: true,
    payload: {
      // Errors for each ENV will be available in separate views.
      environment: process.env.ENV,
    }
  }

  Rollbar = rollbarModule.init(rollbarConfig)
}

module.exports = {Rollbar}
