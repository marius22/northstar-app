const add = (x, y) => {
  return x + y
}

const subtract = (x, y) => {
  return x - y
}

const multiply = (x, y) => {
  return x * y
}

const magic = (n) => {
  if (n % 2 === 0) {
    return 2 * n + 1
  }
  return 3 * n
}

module.exports = {
  add, subtract, multiply, magic
}
