import ExtendableError from 'es6-error'

const USER_FACING_ERROR_MSG = 'Oops, something went wrong. Please contact support if this problem persists.'

class BaseError extends ExtendableError {
  /**
   * @param {message} - The human-readable message we expose to the ser.
   * @param {developerMesssage} - The message developers use to figure out what's wrong
   */
  constructor(message = USER_FACING_ERROR_MSG, developerMessage = null) {
    super(message)

    this.developerMessage = developerMessage
  }
}

/**
 * Whenever we make an HTTP request with $http, we can passs it's response
 * to a new instance of this object for debugging.
 */
class HttpError extends BaseError {
  constructor(res) {
    super(res.data.message)
  }
}

export {
  HttpError
}
