/**
 * Convers a camel-case string to a human readable one.
 */
export const camelToHumanReadable = (text) => {
  const result = text.replace(/([A-Z])/g, " $1")
  return result.charAt(0).toUpperCase() + result.slice(1)
}

export const guid = () => {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1)
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
         s4() + '-' + s4() + s4() + s4()
}

export const insightDisplayName = (key) => {
  const DISPLAY_NAME_MAP = {
    'companyName': 'Company Name',
    'domain': 'Company Website',
    'revenue': 'Revenue',
    'employeeSize': 'Employee Size',
    'industry': 'Industry',
    'alexaRank': 'Alexa Rank',
    'tech': 'Technology',
    'keywords': 'Keywords',
    'country': 'Country',
    'state': 'State',
    'city': 'City',
    'zipcode': 'Zipcode',
    'companyPhone': 'Company Phone',
    'growth': 'Growth Trend',
    'linkedinUrl': 'LinkedIn URL',
    'facebookUrl': 'Facebook URL',
    'twitterUrl': 'Twitter URL',
    'audienceNames': 'Audiences',
  }
  const dispName = DISPLAY_NAME_MAP[key]
  return dispName ? dispName : camelToHumanReadable(key)
}

// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
export const getParameterByKey = (key) => {
  const match = RegExp('[?&]' + key + '=([^&]*)').exec(window.location.search)
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
}
