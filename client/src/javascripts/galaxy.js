import $ from 'npm-zepto'
import URI from 'urijs'

import {renderGraph} from 'galaxyModule'

$(() => {
  // const queryObj = URI.parseQuery(URI.parse(location.href).query)
  // const showRequestTab = queryObj && queryObj.in_iframe === '1'

  // $('.request-galaxy-cont').removeClass('hide')
  renderGraph()
})
