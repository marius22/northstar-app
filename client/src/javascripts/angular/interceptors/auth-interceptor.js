import * as config from 'config'
import SlideController from '../controllers/modals/SlideController'

import _ from 'lodash'
import {publicPages} from 'enums'

class AuthInterceptor {
  /* @ngInject */
  constructor($q, $timeout, $location, $injector) {
    ['request', 'requestError', 'response', 'responseError']
      .forEach((method) => {
        if (this[method]) {
          this[method] = this[method].bind(this)
        }
      })
    this.$q = $q
    this.$timeout = $timeout
    this.$location = $location
    this.$injector = $injector

    this.timeoutIns = null
  }

  isPublic() {
    return _.includes(publicPages, this.$location.path())
  }

  toTimeoutPage() {
    if (!this.isPublic()) {
      // Close all slides on timeout
      SlideController.closeAllSlides()
      this.$location.path('/timeout').search({})
    }
  }

  responseError(rejection) {
    if (rejection.status === 401) {
      if (!this.isPublic()) {
        const refer = this.$location.absUrl()
        this.$state = this.$injector.get('$state')
        this.$state.go('user.login', {
          refer,
          message: 'Please log in to do that.'
        })
      }
    }
    return this.$q.reject(rejection)
  }
}

export default AuthInterceptor
