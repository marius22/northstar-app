import _ from 'lodash'
import angular from 'angular'

/* Note about semantics: keywords <=> tags; the terms are interchangeable here. */

// Which grams are taken into consideration during the queries.
// Currently, we care about bi/tri/quad grams
const DEFAULT_GRAMS = [2, 3, 4]

// Default limit for autocomplete on Prefix matches.
// Currently 50 for performance reasons.
const DF_AUTOCMP_LIMIT = 50

class KeywordService {
  /* @ngInject */
  constructor(proxyService) {
    this.proxyService = proxyService
  }

  /*
      Get top business tags from a list of domains.
      This one uses multi-domain tagging, meaning
      that it will take multiple domains and return
      20 keywords derived from considering EVERY
      domain in the input.

      domains - An array formatted like so: ['everstring.com', 'microsoft.com']
  */
  getTagsFromDomains(domains, type = DEFAULT_GRAMS) {
    const formattedType = type.join(',')
    const domainSelections = domains.join(',')
    const url = `/common/domains/to/clustertags?domains=${domainSelections}&type=${formattedType}`
    return this.proxyService.get(url)
  }


  /*
      Get the top 20 tags for EACH domain,
      so if you input 4 domains you will get 80 tags.

      domains - An array formatted like so: ['everstring.com', 'microsoft.com']
  */
  getTagsForDomainBatch(domains, type = DEFAULT_GRAMS) {
    const formattedType = type.join(',')
    const domainSelections = domains.join(',')
    const url = `/common/domains/to/ngrams?domains=${domainSelections}&type=${formattedType}`
    return this.proxyService.get(url)
  }

  /*
     These should be self-explanatory. Later, these
     may need to be tweaked to do prefix instead of
     Prefix matching.
  */

  getTagsFromPrefix(tagPrefix, type = DEFAULT_GRAMS, limit = DF_AUTOCMP_LIMIT) {
    const formattedType = type.join(',')
    tagPrefix = angular.lowercase(tagPrefix)
    const url = `/company_enrichment/search/ngram?name=${tagPrefix}&type=${formattedType}&limit=${limit}`
    return this.proxyService.get(url)
  }

  getDomainsFromPrefix(domainPrefix, limit = DF_AUTOCMP_LIMIT) {
    domainPrefix = angular.lowercase(domainPrefix)
    const url = `/company_enrichment/search/domain?name=${domainPrefix}&limit=${limit}`
    return this.proxyService.get(url)
  }

  getKeywordsForCompanies(batchSize, numToShow, companies, offset = 0) {
    const resultBatch = _.slice(companies, offset, offset + batchSize)
    const domainBatch = _.map(resultBatch, (compInfo) => {
      return compInfo.domain
    })
    this.getTagsForDomainBatch(domainBatch).then(
      (response) => {
        const domainTagBatch = response.data.data
        // iterate through our search results batch, and set its keywords
        for (let i = offset; i < Math.min(offset + batchSize, companies.length); i++) {
          const compInfo = companies[i]
          compInfo.keywords = _.map(domainTagBatch[compInfo.domain], (item) => {
            return item.grams
          }).join(', ')
        }

        const continueToNextBatch = offset + batchSize < Math.min(numToShow, companies.length)
        if (continueToNextBatch) {
          this.getKeywordsForCompanies(batchSize, numToShow, companies, offset + batchSize)
        }
      }
    )
  }

  /**
   * @param keywords - Array of keywords for which to get lemmas
   *
   * Returns an array consisting of input keywords and their lemmas
   */
  getLemmasFromKeywords(keywords) {
    const params = {ngrams: keywords}
    return this.proxyService
      .post('/common/similar_ngrams', params)
      .then((response) => {
        const kwsAndLemmas = response.data.data
        return kwsAndLemmas
      })
  }

}

module.exports = {KeywordService, DEFAULT_GRAMS}
