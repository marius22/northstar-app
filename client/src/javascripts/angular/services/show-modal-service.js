import _ from 'lodash'

/*
 @ngdoc service
 @name ShowModalService
 @description
 ShowModalService is intended to be a central location for logic involved
 in showing modals. All methods to show modals should take the "params" argument.
 See processModalParams() in ModalController for the various options.
 */

class ShowModalService {
  /* @ngInject */
  constructor(ModalService,
              userService,
              $state,
              segmentService) {
    this.modalService = ModalService
    this.userService = userService
    this.$state = $state
    this.segmentService = segmentService
  }

  requestUpgrade(params = {}) {
    let url = ''
    if (this.userService.isTrial()) {
      url = './modals/requestUpgradeModal.html'
    } else {
      url = './modals/requestMoreModal.html'
    }
    return this.modalService.showModal({
      templateUrl: url,
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })

  }

  changePassword(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/changePassModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  createSegment(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/createSegmentModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  /**
   * Slide for company export
   */
  exportCompany(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/predictive-segment-overview/segment-export.html',
      controller: 'ExportCompanyController',
      controllerAs: 'ecc',
      inputs: {params}
    })
  }

  /**
   * Slide for contacts export
   */
  exportContacts(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/predictive-segment-overview/export-contacts.html',
      controller: 'ExportContactsController',
      controllerAs: 'ecoc',
      inputs: {params}
    })
  }

  /**
   * Slide for choosing to export new or existing companies.
   */
  exportNewOrExisting(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/predictive-segment-overview/export-new-or-existing.html',
      controller: 'ExportNewOrExistingController',
      controllerAs: 'enoec',
      inputs: {params}
    })
  }

  /**
   * Slide for publishing an audience.
   */
  publishAudience(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/audience/publish-audience.html',
      controller: 'PublishAudienceController',
      controllerAs: 'pubAudCtrl',
      inputs: {params}
    })
  }

  /**
   * Slide for mapping for marketo.
   */
  mappingMarketo(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/crm/modal-mapping-marketo.html',
      controller: 'MappingMarketoController',
      controllerAs: 'mapMarketoCtrl',

      inputs: {params}
    })
  }
  /**
   * Slide for view mapping for marketo.
   */
  viewMappingMarketo(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/crm/modal-view-mapping-marketo.html',
      controller: 'ViewMappingMarketoController',
      controllerAs: 'vmapCtrl',
      inputs: {params}
    })
  }

  /**
   * Slide for instance for marketo.
   */
  instanceOfMarketo(params = {}) {
    return this.modalService.showModal({
      templateUrl: '/crm/modal-instance-of-marketo.html',
      controller: 'InstanceOfMarketoController',
      controllerAs: 'insMktCtrl',
      inputs: {params}
    })
  }

  showInviteUserModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/inviteUser.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  barChart(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/barChartModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  columnChart(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/colChartModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  // This is for Terms & Conditions during signup
  showAgreementModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/agreementModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  confirmationModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/confirmationModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  handleNewCriteriaModal(criteriaFromSeed, prevQualChoices, segmentId) {
    const toInsights = () => {
      this.$state.go('segment.create.insights', {segmentId})
    }
    const displayNameMap = {
      'industry': 'Industry',
      'employeeSize': 'Employee Size',
      'state': 'Location'
    }

    const displayName = (category) => {
      return displayNameMap[category]
    }

    let newCriteriaDiv = `<div class="new-seed-criteria">`

    _.forEach(criteriaFromSeed, (critList, category) => {
      if (!_.isEmpty(critList)) {
        newCriteriaDiv += `<div class="category">
                            <div class="name">
                              ${displayName(category)}
                            </div>
                            <div class="values">
                              ${critList.join('<br>')}
                            </div>
                          </div>`
      }
    })

    newCriteriaDiv += `</div>`

    const modalMessage = `<div>
                            We also found in your seed list:
                          </div>
                          ${newCriteriaDiv}
                          <div>
                            Add these selections as well?
                          </div>
                          `
    const modalParams = {
      title: 'Include these Qual Criteria?',
      message: modalMessage,
      onConfirm: (result, close) => {
        this.segmentService.addNewSeedQualCriteria(criteriaFromSeed, prevQualChoices, segmentId).then(
          () => {
            toInsights()
            close()
          }
        )
      },
      onDeny: (result, close) => {
        toInsights()
        close()
      }
    }
    this.confirmationModal(modalParams)
  }

  createUser(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/createUserModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  createAccount(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/createAccountModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  dataViewerModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/dataViewerModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  messageModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/messageModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  handleSFDCErrorInModal(params) {
    this.modalService.showModal({
      templateUrl: './predictive-segment-overview/export-sfdc-process.html',
      controller: 'ExportSFDCProcessController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  /**
   * Slide for finding or viewing keywords from domains.
   * params.purpose = 'view' or 'find' to determine
   * which mode the slide is in
   */
  findKeywords(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/find-keywords.html',
      controller: 'FindKeywordsController',
      controllerAs: 'fkc',
      inputs: {params}
    })
  }

  buildNewModel(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/scoring-new-model.html',
      controller: 'ScoringModelController',
      controllerAs: 'smc',
      inputs: {params}
    })
  }

  showConfirmContactsModal(params) {
    this.modalService.showModal({
      templateUrl: './modals/confirmContactsModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  customModelModal(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/custom-model.html',
      controller: 'CustomModelController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  filterAudience(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/filter-audience.html',
      controller: 'AudienceFilterController',
      controllerAs: 'afc',
      inputs: {params}
    })
  }

  audienceSummary(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/audience-summary.html',
      controller: 'AudienceSummaryController',
      controllerAs: 'asc',
      inputs: {params}
    })
  }

  audienceSettings(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/audience-settings.html',
      controller: 'AudienceSettingsController',
      controllerAs: 'asc',
      inputs: {params}
    })
  }

  audienceNameAndDesc(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/save-audience.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  modelFromList(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/build-model-with-list.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  needCRMFitModel(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/need-model.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  notEnoughDomains(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/not-enough-domains.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  /**
   * Show a focus slide with the specified inputs.
   */
  openFocusSlide({templateUrl, controller, controllerAs, params}) {
    return this.modalService.showModal({
      templateUrl,
      controller,
      controllerAs,
      inputs: {params}
    })
  }

  chooseExclusion(params = {}) {
    return this.modalService.showModal({
      templateUrl: './audience/choose-exclusion.html',
      controller: 'AudienceExclusionController',
      controllerAs: 'aec',
      inputs: {params}
    })
  }

  confirmDeleteMapping(mappingRowToDelete, onConfirm) {
    const label = _.get(mappingRowToDelete, 'leftSide.selectedChoice.label', 'this field')
    const params = {
      title: 'Confirm Deletion',
      message: `<div>
                    This will <b>permanently</b> delete the mapping for <b>${label}</b>.
                    </br>
                    <b>${label}</b> will not be included in future publishes until you create a new mapping for it.
                    </br>
                    Are you sure?
                  </div>`,
      messageType: 'warning',
      buttonText: {
        confirm: 'Yes',
        deny: 'Cancel'
      },
      onConfirm
    }
    return this.confirmationModal(params)
  }

  warnNoIntegration({ message }) {
    const params = {
      title: 'Integration Required',
      message,
      buttonText: {
        confirm: 'Ok',
      },
      onConfirm: () => {
        this.$state.go('crm.integration')
      },
    }

    this.messageModal(params)
  }

  showUnsavedChangesWarning({ onConfirm }) {
    const params = {
      title: 'Unsaved Changes',
      message: 'Changes will not be saved. Do you want to continue?',
      buttonText: {
        confirm: 'Yes',
        deny: 'No'
      },
      onConfirm,
    }

    this.confirmationModal(params)
  }

  showAdjustQuota(params = {}) {
    return this.modalService.showModal({
      templateUrl: './modals/adjustQuotaModal.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }
}

module.exports = {ShowModalService}
