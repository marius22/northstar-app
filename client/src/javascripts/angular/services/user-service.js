import {Intercom} from 'intercom'
import _ from 'lodash'
import { insightDisplayName } from 'utils'
import nsStore from 'stores'

const emptyUser = {
  // User's first name.
  firstName: "",
  // User's surname/last name.
  lastName: "",
  // Users phone number.
  phone: '',
  // User's email address.
  email: '',
  // The roles that this users acts as. This controls what UI elements this
  // user can use.
  roles: [],

  // We impose quotas on how much information we expose to the user.
  quota: {
    exposedUIInsights: [],
    exposedCSVInsights: [],
    // The number of companies this user can export in a CSV.
    CSVCompanyLimit: 0,
    // The number of companies this user is allowed to see in our app on the UI,
    // such as in a table.
    UICompanyLimit: 0,
    // The number of segments this user has in their current quota.
    segmentCnt: 0
  },
  contactExportFlag: false
}

/*
 @ngdoc service
 @name UserService
 @description
 UserService is intended to handle login/logout logic, then act as the
 central source of information for a user and its permissions
 */
class UserService {
  /* @ngInject */
  constructor(apiBaseUrl, $http, segmentService) {
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.segmentService = segmentService

    this.roles = ['ES_SUPER_ADMIN', 'ES_ADMIN', 'ACCOUNT_ADMIN', 'ACCOUNT_USER', 'TRIAL_USER']

    // The currently logged in user.
    this.user = _.extend({}, emptyUser)
  }

  /**
   * Sets the currently signed in user of this app.
   * When logging in, use this method to update this.user, instead of calling this.fetchData again
   */
  setUser(user) {
    _.extend(this.user, user)
    Intercom.boot(this.user)
  }

  // return a copy of this.user
  getUser() {
    return _.extend({}, this.user)
  }

  logout() {
    this.accountInfo = {}
    return this.$http.delete(`${this.apiBaseUrl}users/logout`)
      .then(() => {
        this.user = _.extend({}, emptyUser)
        Intercom.shutDown()
      })
  }

  atSegmentLimit() {
    return !this.segmentService.canCreateSegments
  }

  isLogin() {
    return this.user.email !== ''
  }

  isTrial() {
    return _.includes(this.user.roles, 'TRIAL_USER')
  }

  isRegular() {
    return _.includes(this.user.roles, 'ACCOUNT_USER')
  }

  isAcctAdmin() {
    return _.includes(this.user.roles, 'ACCOUNT_ADMIN')
  }

  isGstAdmin() {
    return _.includes(this.user.roles, 'ES_ADMIN')
  }

  isSuper() {
    return _.includes(this.user.roles, 'ES_SUPER_ADMIN')
  }

  isInternalAdmin() {
    return this.isGstAdmin() || this.isSuper()
  }

  firstName() {
    return this.user.firstName
  }

  lastName() {
    return this.user.lastName
  }

  fullName() {
    return `${this.firstName()} ${this.lastName()}`
  }

  accountId() {
    return this.user.accountId
  }

  accountName() {
    return this.user.accountName
  }

  publishQuotaTotal() {
    return this.user.quota.CSVCompanyLimit
  }

  publishQuotaRemaining() {
    return this.user.quota.remainingCompanyQuota
  }

  contactQuotaTotal() {
    return this.user.quota.contactLimit
  }

  contactQuotaRemaining() {
    return this.user.quota.remainingContactQuota
  }

  exportByDate() {
    return this.user.quota.expiredTs
  }

  segmentQuota() {
    return this.user.quota.segmentCnt
  }

  userId() {
    return this.user.id
  }

  isAdsUser() {
    return this.user.quota.enableAds
  }

  canExportContact() {
    return !!this.user.contactExportFlag
  }

  getUnlockedInsights() {
    return this.user.quota.exposedUIInsights
  }

  canUsePreviousVersion() {
    return this.user.canUsePreviousVersion
  }

  scoringEnabled() {
    return this.user.enableInCrm
  }

  expansionEnabled() {
    return this.user.enableNotInCrm
  }

  publicApisEnabled() {
    return this.user.quota.enableApis
  }

  canFindCompanies() {
    return this.user.quota.enableFindCompany
  }

  canScoreList() {
    return this.user.quota.enableScoreList
  }

  modelInsightsEnabled() {
    return this.user.quota.enableModelInsights
  }

  fetchData() {
    return this.$http.get(`${this.apiBaseUrl}users/me`)
      .success((response) => {
        const user = response.data
        this.setAppropriateUser(user)
      })
  }

  setAppropriateUser(user) {
    if (user.ghostUser) {
      this.setUser(user.ghostUser)
    } else {
      this.setUser(user)
      // If we're in this 'else' block, we know that
      // there's no server-side ghosting active, so we
      // want to update our local storage appropriately.
      const ghostConfigKey = nsStore.GHOST_CONFIG
      if (nsStore.get(ghostConfigKey)) {
        nsStore.set(ghostConfigKey, {
          ghostAcctId: -1,
          ghostAcctName: '',
          ghostActive: false
        })
      }
    }
  }

  getUsersByAccountId(accountId) {
    return this.$http.get(`${this.apiBaseUrl}accounts/${accountId}/users`)
  }

  getUsersByMyAccount() {
    return this.getUsersByAccountId(this.accountId())
  }

  /*
    Real Time Scoring
  */
  getInboundLeadsTargets(source = this.user.quota.inboundLeadsTarget) {
    return {
      account: _.includes(source, 'account'),
      lead: _.includes(source, 'lead'),
      contact: _.includes(source, 'contact')
    }
  }

  getRealtimeScoringMode() {
    return this.user.quota.realtimeScoring
  }

  // Whether or not real-time scoring is turned off globally.
  // Differs from scoringEnabled(), which is an account permission
  // that only ES Admins can control.
  scoringOff() {
    return this.getRealtimeScoringMode() === ''
  }

  // Whether or not real-time scoring is turned on globally
  scoringGlobally() {
    return this.getRealtimeScoringMode() === 'global'
  }

  // Whether or not real-time scoring happens per audience
  scoringPerAudience() {
    return this.getRealtimeScoringMode() === 'audience'
  }

  formatScoringInsightsForMultiSelect() {
    const insightsAllowedForExport = this.user.quota.exposedCSVInsights
    const currentScoringInsights = this.user.quota.realTimeScoringInsights
    const msData = _.map(insightsAllowedForExport, (insight) => {
      return {
        text: insightDisplayName(insight),
        key: insight,
        selected: _.includes(currentScoringInsights, insight)
      }
    })

    return msData
  }

  updateScoringSettings(inboundLeadsTarget, realtimeScoring) {
    const url = `${this.apiBaseUrl}accounts/${this.accountId()}/scoring`
    const params = {
      inboundLeadsTarget, realtimeScoring
    }
    return this.$http.put(url, params)
  }

}

module.exports = {UserService}
