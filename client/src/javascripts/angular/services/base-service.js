class BaseService {
  constructor($http, apiBaseUrl) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
  }

  fetch(api, method, params) {
    const url = `${this.apiBaseUrl}${api}`
    const payload = {
      method,
      url
    }

    if (method === 'GET') {
      payload.params = params
    } else {
      payload.data = params
    }
    return this.$http(payload)
  }
}

export default BaseService
