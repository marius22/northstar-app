import angular from 'angular'
const _alertIcons = Symbol('alertIcons')
const _alertButtons = Symbol('alertButtons')

/*
  @ngdoc service
  @name AlertService
  @description
      AlertService handles error/success/info/warning alert banners.
  @param {service} $timeout - angular's $timeout for removing the alert after a duration
  @example
    class MyController() {
      constructor(alertService) {
        this.alertService = alertService
      }

      showErrorAlert() {
        const errorMessage = 'You dun goofed!'

        //this will cause the alert banner to appear below element with id #myContainer
        this.alertService.addAlert('error', errorMessage, '#myContainer', 3)
      }

    }
 */
class AlertService {
  /* @ngInject */
  constructor($timeout,
              $animate,
              $compile,
              $rootScope,
              showModalService) {
    this.$animate = $animate
    this.$timeout = $timeout
    this.$compile = $compile
    this.$rootScope = $rootScope
    this.alertScope = $rootScope.$new()
    this.activeAlerts = new Set()
    this.alertRemovers = []

    this.alertScope.close = (msg) => {
      $animate.leave(angular.element(document.querySelector(`#${msg}`)))
      this.activeAlerts.delete(msg)
    }

    this.alertScope.showUpgradeModal = function () {
      showModalService.requestUpgrade()
    }

    this[_alertIcons] = {
      error: '<i class="btr bt-big bt-ban alert-icon"></i>',
      success: '<i class="btr bt-big  bt-check-circle alert-icon"></i>',
      info: '<i class="btr bt-big  bt-info-circle alert-icon"></i>',
      warning: '<i class="btr bt-big  bt-exclamation-circle alert-icon"></i>'
    }

    this[_alertButtons] = {
      none: '',
      upgrade: '<button ng-click="showUpgradeModal()">Upgrade</button>'
    }
  }

  /*
    @memberof AlertService
    @param {string} type - Type of the alert. Valid types are 'error', 'success', 'info', and 'warning'.
    @param {string} message - Message of the alert banner. If no message is provided, nothing will happen.
    @param {string} container - CSS selector that specifies container(s) that the alert will appear under.
                                Defaults to '#topAlert'.
    @param {number} duration - Number of seconds the banner is shown. 0 will make the banner appear indefinitely.
    @param {string} button - The type of button we want to show in the banner. You can add more types under
                              _alertButtons.
   */

  addAlert(type, message, container = '#topAlert', duration = 0, btn = 'none') {
    const encodeMessage = message.replace(/[^a-z0-9]/g, '')
    if (this.activeAlerts.has(encodeMessage)) {
      return
    }
    const validType = (type in this[_alertIcons])

    if (validType && message) {
      let alertClass = `alert-${type}`
      let containerElem = document.querySelector(container)

      if (container === 'fixed') {
        alertClass = `${alertClass} fixed-to-top`
        containerElem = document.body
      }

      const div = this.$compile(`<div class="es-alert ${alertClass} fx-fade-normal fx-speed-200" id=${encodeMessage}>
                    <span class="icon-wrap">${this[_alertIcons][type]}</span>
                    <a class="alert-message">${message} </a>
                    ${this[_alertButtons][btn]}
                    <i class="btr bt-big bt-times alert-close" ng-click="close('${encodeMessage}')"></i>
                  </div>`)(this.alertScope)

      this.$animate.enter(div, angular.element(containerElem))
      this.adjustWidth(div[0])
      this.activeAlerts.add(encodeMessage)

      const removeAlert = () => {
        this.$animate.leave(div)
        this.activeAlerts.delete(encodeMessage)
      }

      this.alertRemovers.push(removeAlert)

      if (type === 'success') {
        duration = 3
      }

      if (duration > 0) {
        this.$timeout(() => {
          removeAlert()
        }, duration * 1000)
      }

      // remove the alert if we go to a different page
      this.$rootScope.$on('$stateChangeStart', () => {
        removeAlert()
      })

      // remove the alert if the alert's container is removed.
      // $watch() returns a function that you can call to remove the watch.
      if (container !== 'fixed') {
        const removeWatch = this.$rootScope.$watch(() => {
          if (!document.querySelector(container)) {
            removeAlert()
            removeWatch()
          }
        })
      }
    }
  }

  adjustWidth(div) {
    /* eslint-disable no-param-reassign */
    let siblingsWidth = 0
    const children = div.childNodes
    for (let i = 0; i < children.length; i++) {
      const child = children[i]
      const isNode = child.nodeType === 1 || child.nodeType === 3
      const className = child.className || ''
      if (isNode &&
        className.indexOf('alert-message') === -1) {
        siblingsWidth += child.offsetWidth || 0
      }
    }
    const alertWidth = div.offsetWidth - siblingsWidth - 50
    div.querySelector('.alert-message').style.maxWidth = `${alertWidth}px`
    /* eslint-enable no-param-reassign */
  }

  addUpgradeAlert() {
    const msg = 'You have exceeded the maximum account allowance. Please click here to upgrade'
    this.addAlert('error', msg, '#topAlert', 0, 'upgrade')
  }

  addPublishSuccessAlert() {
    const message = [
      'Your publish is in progress! You\'ll receive an email once it\'s completed.'
    ].join('')
    this.addAlert('success', message, '#topAlert', 0)
  }

  addGenericErrorAlert(container = '#topAlert') {
    this.addAlert(
      'error',
      "Oops, something went wrong. Please contact support if this problem persists.",
      container,
      0)
  }

  clearAlerts() {
    _.forEach(this.alertRemovers, (removeAlertFn) => {
      removeAlertFn()
    })
    this.alertRemovers = []
  }

}

module.exports = {AlertService}
