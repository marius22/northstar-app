import * as config from 'config'

import Promise from 'bluebird'
import _ from 'lodash'
import nsStore from 'stores'

const CREATED_AUDIENCE = 'CREATED_AUDIENCE'

class AudienceService {
  /* @ngInject */
  constructor(apiBaseUrl, $http, Upload, $q, userService, proxyService, $state, alertService, keywordService) {
    this.audiences = []
    this.apiBaseUrl = apiBaseUrl
    this.uploader = Upload
    this.$http = $http
    this.$q = $q
    this.$state = $state
    this.alertService = alertService

    this.userService = userService
    this.proxyService = proxyService
    this.keywordService = keywordService
    this.init()
  }

  init() {
    this.audienceCompanies = {}
    this.currentSimilarityItems = []
  }

  getAudienceById(id) {
    return this
      .$http({
        method: 'GET',
        url: `${this.apiBaseUrl}audiences/${id}`
      })
      .then((res) => {
        const audience = res.data.data
        return audience
      })
  }

  getAudiences() {
    return this
      .$http({
        method: 'GET',
        url: `${this.apiBaseUrl}audiences`
      })
  }

  getExcLists() {
    return this
      .$http({
        method: 'GET',
        url: `${this.apiBaseUrl}audiences`,
        params: {
          metaType: 'exclusion'
        }
      })
  }

  getAudienceIndicators(id, modelId, limit, order) {
    return this
      .$http({
        method: 'GET',
        url: `${this.apiBaseUrl}audiences/${id}/indicators`,
        params: {
          modelId, limit, order
        }
      })
  }

  /**
   * @param id - The ID of the audience to fetch.
   * @param offset - The offset to pass to the company SQL query.
   * @param limit - The limit to pass to the company SQL query.
   * @param modelID - The model ID to sort our results by
   * @param criteria - The filter criteria to filter our results by
   * @param orderBy - Specifies order in which results are returned.
   *
   */
  getAudienceCompanies(id, offset, limit, criteria, orderBy) {
    const select = 'name,domain,state,revenueRange,industry,employeeSize,numContactsWithEmail'
    return this
      .$http({
        method: 'POST',
        url: `${this.apiBaseUrl}audiences/${id}/companies`,
        params: {
          offset, limit, criteria, orderBy, select
        }
      })
      .then((res) => {
        const companies = res.data.data.map((c) => {
          // Deal w/ inconsistent API's
          // Round fit score up for consistency
          c.fitScorePercent = c.fitScore !== undefined ? Math.round(c.fitScore) : '--'
          c.contactCount = c.numContactsWithEmail
          return c
        })
        return companies
      }, (err) => {
        this.alertService.addAlert('error', 'Unable to fetch companies. Please contact support and try again later.')
      })
  }

  getAudienceCompaniesCount(audienceId, criteria) {
    return this
      .$http({
        method: 'POST',
        url: `${this.apiBaseUrl}audiences/${audienceId}/companies/count`,
        params: {
          criteria
        }
      })
      .then((response) => {
        return response.data.total
      })
  }

  /**
   * Essentially the same as getAudienceCompanies
   */
  getExclusionCompanies(excListId, offset, limit, criteria, orderBy) {
    const getExcCompaniesUrl = `/common/exclusion/${excListId}/companies`
    const params = {
      limit,
      offset,
      criteria,
      orderBy
    }
    return this
      .proxyService
      .post(getExcCompaniesUrl, params)
      .then((response) => {
        return response.data.data
      }, (err) => {
        this.alertService.addAlert('error', 'Unable to fetch companies. Please contact support and try again later.')
      })
  }

  getExclusionCompaniesCount(excListId, criteria) {
    const getCountUrl = `/common/exclusion/${excListId}/companies/count`
    const params = {criteria}
    return this
      .proxyService
      .post(getCountUrl, params)
      .then((response) => {
        return response.data.total
      })
  }

  createAudience(name, description, criteria, type, file, modelId, metaType, exclusionListIds) {
    const createAudienceUrl = `${this.apiBaseUrl}audiences`
    const successHandler = (response) => {
      if (metaType === 'audience') {
        nsStore.set(CREATED_AUDIENCE, true)
      }
      return response
    }
    // type can be 'found' or 'enriched'
    if (type === 'found') {
      return this
        .$http({
          method: 'POST',
          url: createAudienceUrl,
          data: {
            // description can not be null for backend
            name, description: description || '', criteria, type, modelId, metaType, exclusionListIds
          }
        })
        .then(successHandler)
    } else {
      return this
        .uploader.upload({
          url: createAudienceUrl,
          data: {
            name, description: description || '', criteria, type, file, modelId, metaType, exclusionListIds
          }
        })
        .then(successHandler)
    }
  }

  /**
   * @param audience - Object representing the audience state that we want to update to
   */
  updateAudience(audience) {
    const updateAudienceUrl = `${this.apiBaseUrl}audiences/${audience.id}`

    const updateFields = ['name', 'description', 'showInSfdc', 'isPrivate',
      'realTimeScoringEnabled', 'userId', 'modelId', 'isPublishedSfdc',
      'exclusionListIds']

    audience = _.pick(audience, updateFields)
    if (!audience.modelId) {
      // default modelId should be -1
      audience.modelId = -1
    }
    // TODO this original problem is caused by the relaxed schema of is_published_sfdc,
    // which should not be a null value in the database
    if (!_.isBoolean(audience.isPublishedSfdc)) {
      audience.isPublishedSfdc = false
    }
    const params = {audience}
    return this.$http.post(updateAudienceUrl, params)
  }

  /**
   * @param excListId - Id of exclusion list whose companies we want to update
   * @param csvFile - csv file of companies with which we will overwrite existing
  *                   ExclusionCompanies of an exclusion list
   */
  updateExclusionCompanies(excListId, csvFile) {
    const updateUrl = `${this.apiBaseUrl}audiences/${excListId}`
    return this
      .uploader.upload({
        url: updateUrl,
        data: {
          file: csvFile
        }
      })
  }

  /*
    We have two endpoints to save audience criteria: one for filter criteria and
    one for keywords. This method uses both those endpoints.
  */
  saveAudienceCriteria(audienceId, audienceCriteria, saveKeywords) {
    const saveFilterCriteriaUrl = `${this.apiBaseUrl}audiences/${audienceId}/criteria`
    const saveKeywordsUrl = `${this.apiBaseUrl}audiences/${audienceId}/keywords`

    const filterCriteriaParams = {criteria: _.omit(audienceCriteria, 'grams')}
    const keywordParams = {keywords: audienceCriteria.grams}
    const saveFilterCriteriaPromise = this.$http.post(saveFilterCriteriaUrl, filterCriteriaParams)
    const saveKeywordsPromise = saveKeywords ? this.$http.post(saveKeywordsUrl, keywordParams) : this.$q.resolve()

    return this.$q.all([saveFilterCriteriaPromise, saveKeywordsPromise])
  }

  /*
    Combine both keywords + criteria endpoints for Creation Summary slide
  */
  getCreationSummary(audienceId) {
    const getCriteriaPromise = this.getAudienceFilterCriteria(audienceId)
    const getKeywordsPromise = this.getAudienceKeywords(audienceId)
    return this.$q.all([getCriteriaPromise, getKeywordsPromise])
  }

  /*
    Get just the keywords used to create Audience.
  */
  getAudienceKeywords(audienceId) {
    const getKeywordsUrl = `${this.apiBaseUrl}audiences/${audienceId}/keywords`
    return this.$http.get(getKeywordsUrl)
  }

  /*
    Get just the filter criteria used to create Audience
  */
  getAudienceFilterCriteria(audienceId) {
    const getCriteriaUrl = `${this.apiBaseUrl}audiences/${audienceId}/criteria`
    return this.$http.get(getCriteriaUrl)
  }

  /*
    Get the latest total company count and count refresh timestamp for audience
  */
  refreshAudienceCount(audienceId) {
    return this
      .$http({
        method: 'POST',
        url: `${this.apiBaseUrl}audiences/${audienceId}/refresh_count`
      })
  }

  deleteAudience(audienceId) {
    return this
      .$http({
        method: 'DELETE',
        url: `${this.apiBaseUrl}audiences/${audienceId}`
      })
  }

  stopShowingTooltips() {
    nsStore.set(CREATED_AUDIENCE, true)
  }

  hasCreatedAudience() {
    // Lightweight way of checking if the user has created audiences.
    // If they haven't, we show them tutorial tooltips.
    return nsStore.get(CREATED_AUDIENCE)
  }

  updateFitModel(audienceId, modelId) {
    return this
      .$http({
        method: 'POST',
        url: `${this.apiBaseUrl}audiences/${audienceId}/switch_fit_model`,
        data: {
          fit_model_id: modelId
        }
      })
  }

  /**
   * Get the fields of a company that we can publish to.
   */
  getPublishFieldsSupported() {
    // TODO: Should be configured from a python config file
    const canPublishToAudienceFields = [
      'domain',
      'companyPhone',
      'companyName',
      'revenue',
      'employeeSize',
      'industry',
      'state',
      'street',
      'zipcode',
      'country',
      'city',
      'score'
    ]

    const availableFieldsP = (
      this
        .$http({
          method: 'GET',
          url: `${this.apiBaseUrl}audiences/publish_fields_supported`,
        })
        .then(res => {
          return res.data.data
        }))

    const configuredAvailableFieldsP = (
      this
        .$http({
          method: 'GET',
          url: `${this.apiBaseUrl}accounts/${this.userService.accountId()}/quotas`,
        })
        .then(res => {
          const exposed = res.data.data.exposedCSVInsights
          return exposed
        })
    )

    return Promise
      .all([availableFieldsP, configuredAvailableFieldsP])
      .spread((availableFields, configuredAvailableFields) => {
        return _.intersection(
          canPublishToAudienceFields, availableFields, configuredAvailableFields)
      })
  }

  /**
   * Get the in CRM/not in CRM statuses of the domains passed in
   */
  getInCrmStatus(domains) {
    return this.$http({
      url: `${this.apiBaseUrl}salesforce/find_in_crm`,
      params: {
        domains: domains.join(','),
        findOn: ['lead', 'account'].join(','),
        accountId: this.userService.accountId()
      }
    })
    .then(res => {
      return res.data.data
    })
  }

  /*
    Stuff for the Similarities tab in company card details state
  */
  setCurrentSimilarityItems(items) {
    if (Array.isArray(items)) {
      this.currentSimilarityItems = items.slice(0)
      this.lemmaTags = []

      if (!this.isSimilarDomainsState()) {
        this
          .keywordService
          .getLemmasFromKeywords(this.currentSimilarityItems)
          .then((kwsAndLemmas) => {
            this.lemmaTags = kwsAndLemmas
          })
      }
    }
  }

  setSimilarityItemsFromCreationSummary(audienceId) {
    this.currentSimilarityItems = []
    this.simItemType = ''
    return this
      .getCreationSummary(audienceId)
      .then((result) => {
        const criteria = result[0].data.data
        const keywords = result[1].data.data
        const similarDomains = criteria.similarDomains

        if (!_.isEmpty(keywords)) {
          this.setCurrentSimilarityItems(keywords)
          this.simItemType = 'keywords'
        }
        else if (!_.isEmpty(similarDomains)) {
          this.setCurrentSimilarityItems(similarDomains)
          this.simItemType = 'domains'
        }
      })
  }

  isSimilarDomainsState() {
    const similarDomsInFindAud = this.$state.is('audience.find.csv') || this.$state.is('audience.find.urls')
    const similarDomsInCreatedAud = this.isCompanyListState() && this.simItemType === 'domains'
    return similarDomsInFindAud || similarDomsInCreatedAud
  }

  isCompanyListState() {
    return this.$state.is('audience.company')
  }

  showSimilaritiesTab() {
    const isAppropriateState = (this.$state.is('audience.find.keywords') ||
                                this.isSimilarDomainsState() ||
                                this.isCompanyListState())
    const hasSimilarityItems = !_.isEmpty(this.currentSimilarityItems)

    return isAppropriateState && hasSimilarityItems
  }

  /*
    Distinct counts stuff for audience insights charts
  */
  getAudienceCountsAsync(audienceId, keys, withOthers) {
    return this.$http({
      url: `${this.apiBaseUrl}audiences/${audienceId}/companies/distinct_counts`,
      params: {keys, withOthers, limit: 50}
    }).then((response) => {
      this.audienceCounts = response.data.data
    })
  }

  getAudienceCounts() {
    return this.audienceCounts
  }
}

module.exports = {AudienceService}
