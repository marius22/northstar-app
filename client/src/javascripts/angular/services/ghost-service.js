import * as config from 'config'

import nsStore from 'stores'

/*
   @ngdoc service
   @name GhostService
   @description
   GhostService keeps track of data needed for an ES admin to ghost. Uses local storage
   to keep ghosting active upon refresh or logging out and back in.
 */

class GhostService {
  /* @ngInject */
  constructor(userService, $state, $http) {
    this.userService = userService
    this.$state = $state
    this.ghostConfig = nsStore.GHOST_CONFIG
    this.$http = $http
  }

  setGhostAcct(id, name, userId) {
    return this.$http({
      method: 'PUT',
      url: `${config.apiBaseURL}users/ghost`,
      data: {
        accountId: id
      }
    }).then((res) => {
      nsStore.set(this.ghostConfig, {
        ghostAcctId: id,
        ghostAcctName: name,
        ghostActive: true,
        ghostUserId: userId
      })
      return this.userService.fetchData()
    }).then(() => {
      this.$state.go('audience.console')
    })
  }

  getGhostConfig() {
    const ghostConfig = nsStore.get(this.ghostConfig)
    let ret = ghostConfig || {}
    if (nsStore.useStorage()) {
      try {
        ret = JSON.parse(ghostConfig) || {}
      } catch (e) {
        ret = {}
      }
    }
    return ret
  }

  ghostId() {
    const ghostConfig = this.getGhostConfig()
    return ghostConfig.ghostAcctId || -1
  }

  ghostName() {
    const ghostConfig = this.getGhostConfig()
    return ghostConfig.ghostAcctName || ''
  }

  stopGhosting() {
    return this.$http({
      method: 'DELETE',
      url: `${config.apiBaseURL}users/ghost/fallback`
    }).then(() => {
      nsStore.set(this.ghostConfig, {
        ghostAcctId: -1,
        ghostAcctName: '',
        ghostActive: false
      })
      return this.userService.fetchData()
    }).then(() => {
      this.$state.go('admin.license')
    })
  }

  isGhostActive() {
    const ghostConfig = this.getGhostConfig()
    return ghostConfig.ghostActive || false
  }

  isGhosting() {
    return this.isGhostActive()
  }

  canGhost() {
    return this.userService.isGstAdmin() || this.userService.isSuper()
  }

  /**
   * return ghost accountId or current accountId
   */
  accountId() {
    return this.isGhosting() ? this.ghostId() : this.userService.accountId()
  }

  /**
   * return ghost userId or current userId
   */
  userId() {
    const user = this.getGhostConfig()
    return this.isGhosting() ? user.ghostUserId : this.userService.userId()
  }
}

module.exports = {GhostService}
