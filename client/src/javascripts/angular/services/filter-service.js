import _ from 'lodash'

const FILTER_CATEGORIES = [
  "city",
  "withContact",
  "notInCrm",
  "employeeSize",
  "country",
  "industry",
  "revenue",
  "zipcode",
  "fitScore",
  "publishState",
  "state",
  "growth",
  "tech",
  "social",
  "department",
  "inCrm",
  "territory",
  "departmentStrength"
]

const FILTERS = [
  {name: 'Location', icon: 'bt-map-marker', key: 'state'},
  {name: 'Industry', icon: 'bt-industry', key: 'industry'},
  {name: 'Revenue', icon: 'bt-money', key: 'revenue'},
  {name: 'Employees', icon: 'bt-user', key: 'employeeSize'},
  {name: 'Technology', icon: 'bt-tv', key: 'tech'},
  // {name: 'Growth Trend', icon: 'bt-bolt', key: 'growth'},
  {name: 'Department', icon: 'bt-bolt', key: 'department'},
  // {name: 'Dept Strength', icon: 'bt-bolt', key: 'departmentStrength'}
]


// These filters are only usable when filtering on the Company List
// of an existing audience.
const FILTERS_FOR_ONLY_EXISTING = [
  {name: 'Fit Score', key: 'fitScore'},
  // {name: 'In CRM', key: 'inCrm'},
  // {name: 'Not In CRM', key: 'notInCrm'},
  {name: 'Published', key: 'publishState'},
  {name: 'Zipcode', icon: 'bt-map-marker', key: 'zipcode'}
]

class FilterService {
  /* @ngInject */
  constructor($http, apiBaseUrl, $q, $state, audienceService, userService) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.$q = $q
    this.$state = $state
    this.audienceService = audienceService

    this.filterData = {}
    this.subFilterData = {}
    this.subFilterItems = {}
    this.audienceCriteria = {}
  }

  appropriateInit() {
    if (this.existingAudienceContext()) {
      this.initWithAudienceCriteria()
    } else if (this.isCreationContext()) {
      this.resetData()
      this.init()
    }
  }

  resetData() {
    this.filterData = {}
    this.subFilterData = {}
    this.subFilterItems = {}
    this.audienceCriteria = {}
    this.inited = false
  }

  isCreationContext() {
    const isAudienceCreateState = this.$state.includes('audience.find') || this.$state.includes('audience.enrich')
    const isExclusionCreateState = this.$state.includes('exclusion.find') || this.$state.includes('exclusion.enrich')
    const isExclusionUpdateState = this.$state.includes('exclusion.update')
    return isAudienceCreateState || isExclusionCreateState || isExclusionUpdateState
  }

  existingAudienceContext() {
    return this.$state.is('audience.company')
  }

  /*
    Initialize filterService with all filters and filter options in the universe
  */
  init() {
    const primaryFilters = this.getPrimaryFilters()
    const categories = _.map(primaryFilters, (filter) => {
      return filter.key
    })
    // When we have all categories, we can then use then to query
    // for the options within each category
    const p1 = this.getFilterDataAsync(categories)
    const p2 = this.getSubcategoriesAsync(categories)
    this.$q.all([p1, p2])
      .then(() => {
        // once we have all the category options for primary
        // and sub filters, we now can add all subFilters
        // to this.filterData and populate our subFilterItems
        _.forEach(primaryFilters, (item) => {
          this.subFilterItems[item.key] = this.getSubFilters(item.key, item.name)
        })
      })
      .finally(() => {
        this.inited = true
      })
  }

  /*
    Initialize filterService with filters and filter options relative to the
    existing audience
  */
  initWithAudienceCriteria() {
    const audienceId = this.$state.params.audienceId
    if (this.mostRecentAudienceId === audienceId && !_.isEmpty(this.filterData)) {
      return
    } else {
      this.resetData()
      this.mostRecentAudienceId = audienceId
    }
    return this.audienceService.getAudienceFilterCriteria(audienceId).then(
      (response) => {
        this.flattenAudienceFilters(response.data.data)
        this.init()
      }
    )
  }

  hasAudienceCriteria() {
    return !_.isEmpty(this.audienceCriteria)
  }

  /*
    Get display name from key for a primary filter.
  */
  dispName(key) {
    const primaryFilterItem = _.find(this.getPrimaryFilters(), (filterItem) => {
      return filterItem.key === key
    })
    if (primaryFilterItem) {
      return primaryFilterItem.name
    }
  }

  /*
    Transforms data into a format that multi-select uses, like so:
    ['a', 'b'] => [{text: 'a', selected: false}, {text: 'b', selected: false}]
  */
  transformForMultiSelect(data) {
    return _.map(data, (text) => {
      const selected = false
      return {text, selected}
    })
  }

  addToFilterData(category, allCategoryChoices, subFilter = false) {
    if (this.hasAudienceCriteria && !_.isEmpty(this.audienceCriteria[category])) {
      const onlyChoicesInAudience = this.audienceCriteria[category]
      this.filterData[category] = this.transformForMultiSelect(onlyChoicesInAudience)
    } else {
      // if this is for a subfilter, then allCategoryChoices are already transformed
      if (subFilter) {
        this.filterData[category] = allCategoryChoices
      } else {
        const choicesAndUnknown = this.addNullChoice(category, allCategoryChoices)
        this.filterData[category] = this.transformForMultiSelect(choicesAndUnknown)
      }
    }
  }

  addNullChoice(category, choices) {
    if (category === 'publishState') {
      // no 'Unknown' choice
      return choices
    }
    if (category === 'fitScore') {
      // this is going to change to be 'Not Scored' soon
      return _.concat(choices, ['Unknown'])
    }
    return _.concat(choices, ['Unknown'])
  }

  /*
    Get all possible data values for categories in our universe
  */
  getFilterDataAsync(categories) {
    const url = `${this.apiBaseUrl}metadata/categories/details?keys=${encodeURIComponent(categories.join(','))}`
    return this.$http.get(url).then(
      (response) => {
        const responseData = response.data.data
        _.forEach(categories, (category) => {
          this.addToFilterData(category, responseData[category])
        })
        return this.filterData
      }
    )
  }

  /*
     Get all subcategories of a primary category.
     For example, the subcategories of "departmentStrength" are ["Engineering", "HR", "Finance", ...]
   */
  getSubcategoriesAsync(subcategories) {
    const url = `${this.apiBaseUrl}metadata/categories/subcategories?keys=${encodeURIComponent(subcategories.join(','))}`
    return this.$http.get(url).then(
      (response) => {
        this.subFilterData = response.data.data
        return this.subFilterData
      }
    )
  }

  /*
    Get an array of subfilters for a given primary category key. Also initializes
    an appropriate filterData key if necessary, for every subfilter.
  */
  getSubFilters(primaryCtgKey, primaryCtgName) {
    if (this.subFilterData && !_.isEmpty(this.subFilterData[primaryCtgKey])) {
      const subFilters = this.subFilterData[primaryCtgKey]
      // add subfilter category and possible choices for that category
      // as a k/v pair on filterData if it doesn't exist yet
      _.forEach(subFilters, (subItem) => {
        if (!this.filterData[subItem]) {
          const subFilterOptions = _.cloneDeep(this.filterData[primaryCtgKey])
          this.addToFilterData(subItem, subFilterOptions, true)
        }
      })

      return _.map(subFilters, (subItem) => {
        // front-end only gets subFilters as human-readable values, so we're just going
        // to use that as both the name and the key
        return {
          name: subItem,
          key: subItem,
          summaryName: primaryCtgName ? `${primaryCtgName}: ${subItem}` : subItem
        }
      })
    }
  }

  hasSubFilters(primaryCtgKey) {
    return !_.isEmpty(this.subFilterData[primaryCtgKey])
  }

  getFiltersForExistingAudience() {
    return _.cloneDeep(FILTERS_FOR_ONLY_EXISTING)
  }

  getPrimaryFilters() {
    let primaryFilters = _.cloneDeep(FILTERS)
    if (this.existingAudienceContext()) {
      primaryFilters = _.union(primaryFilters, this.getFiltersForExistingAudience())
    }

    return primaryFilters
  }

  clearAllFilters() {
    _.forEach(this.filterData, (categoryItems) => {
      _.forEach(categoryItems, (item) => {
        item.selected = false
      })
    })
  }

  /*
     For a given category key, returns an array of all the selected item texts in that category.
     this.filterData is flat; in other words, it contains filter selections for both primary
     filters AND sub filters for simplicity.
  */

  selectedInCategory(key, customData) {
    const ctgFilterData = customData ? customData[key] : this.filterData[key]
    return _(ctgFilterData)
      .filter(item => {
        return item.selected
      })
      .map(item => {
        return item.text
      })
      .value()
  }

  totalSelected() {
    let total = 0
    _.forEach(this.filterData, (choices, key) => {
      total += this.selectedInCategory(key).length
    })
    return total
  }

  totalActiveCategories() {
    let total = 0
    _.forEach(this.filterData, (choices, key) => {
      if (!_.isEmpty(this.selectedInCategory(key))) {
        total += 1
      }
    })
    return total
  }

  atLeastOneFilter() {
    return this.totalSelected() > 0
  }

  /*
     Returns a function that checks if a filter category has any items selected.
     Implemented this way because filteringOnCategory() is intended to be used as a filter
     function of ng-repeat in the summary section of filter-audience.html
  */
  filteringOnCategory() {
    return (category) => {
      return this.selectedInCategory(category.key).length > 0
    }
  }

  /*
    Uses this.filterData or customData, and returns an object representing the filter choices
    in a nested format that APIs expect.

    For example, this.filterData is the following:

    {
      'Medical & Health': ['Low'],
      'Engineering': ['Medium', 'High'],
      'industry': ['Computer Software'],
      'state': ['California']
    }

    The formatted version of this that gets returned is the following:

    {
      'departmentStrength': {'Medical & Health': ['Low'], 'Engineering': ['Medium', High']},
      'industry': ['Computer Software'],
      'state': ['California']
    }
  */
  formatFiltersForAPIs(customData) {
    const formattedFilters = {}

    const addIfAnySelections = (obj, key) => {
      const selectedItems = this.selectedInCategory(key, customData)
      if (!_.isEmpty(selectedItems)) {
        obj[key] = selectedItems
      }
    }

    _.forEach(this.getPrimaryFilters(), (filterItem) => {
      /*
        Two cases here.
        Case 1: If a filter has subcategories i.e departmentStrength, then
                we want the value of departmentStrength to be another object with
                subfilters as keys and choices for each respective subfilter as values.
                Example: {departmentStrength: {'Medical & Health': ['Low', 'Medium'], 'HR': ['High']}}

        Case 2: If a filter has no subcategories, then we just create a key value pair of the filter key
                and choices made for that filter i.e {state: ['California', 'Nevada', 'New York']}
      */
      const key = filterItem.key
      if (this.hasSubFilters(key)) {
        // Case 1
        const subFilters = this.getSubFilters(key)
        const formattedSubFilters = {}
        _.forEach(subFilters, (subFilterItem) => {
          const sfKey = subFilterItem.key
          addIfAnySelections(formattedSubFilters, sfKey)
        })
        const hasSubFilterSelections = !_.isEmpty(formattedSubFilters)
        if (hasSubFilterSelections) {
          formattedFilters[key] = formattedSubFilters
        }
      } else {
        // Case 2
        addIfAnySelections(formattedFilters, key)
      }
    })
    return formattedFilters
  }

  /*
    This does the opposite of formatFiltersForAPIs().
    It takes data in its nested format and returns a flattened
    version of it.
  */
  flattenAudienceFilters(criteria, parentKey) {
    _.forEach(criteria, (value, key) => {
      if (Array.isArray(value)) {
        this.audienceCriteria[key] = value
      } else {
        if (!parentKey) {
          this.flattenAudienceFilters(value, key)
        }
      }
    })
  }

}

module.exports = {FilterService}
