class FieldMappingService {
  /* @ngInject */
  constructor(apiBaseUrl, $http, userService) {
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.userService = userService
  }

  fetchPossibleFields({ targetType }) {
    const url = `${this.apiBaseUrl}account/${this.userService.accountId()}/supported_publish_fields`
    return this
      .$http({
        method: 'GET',
        url,
        params: {
          targetType,
        }
      })
      .then((response) => {
        return response.data.data
      })
  }

  /**
   * Get the mappings for the given information
   *
   * @param targetType - The service we're publishing data to. 'csv', 'salesforce', 'marketo', or 'marketo_sandbox'
   * @param targetObject - The kind of object we're publishing to. 'account', 'lead', or 'contact' for Salesforce.
   *   For publishing to CSV, this should just be 'company'. For Marketo, this is always 'lead' because Marketo
   *   only contains leads.
   */
  _getCurrentMappings(targetType, targetObject) {
    const url = `${this.apiBaseUrl}account/${this.userService.accountId()}/field/mapping`
    const params = {
      targetType,
      targetObject
    }
    const request = {
      method: 'GET',
      url,
      params
    }
    return this.$http(request)
      .then((response) => {
        return response.data.data
      })
  }

  _upsertMappings(targetType, targetObject, targetFields) {
    const url = `${this.apiBaseUrl}account/${this.userService.accountId()}/field/mapping`
    const params = {
      targetType,
      targetObject,
      targetFields
    }
    return this.$http
      .post(url, params)
      .then((response) => {
        return response.data.data
      })
  }

  _deleteMappings(targetType, targetObject, targetFields) {
    const url = `${this.apiBaseUrl}account/${this.userService.accountId()}/field/mapping`
    const params = {
      targetType,
      targetObject,
      targetFields
    }
    const request = {
      method: 'DELETE',
      url: url,
      headers: {
        'Content-Type': 'application/json'
      },
      data: params
    }
    return this.$http(request)
      .then((response) => {
        return response.data.data
      })
  }

  // For CSVs, we create a field mapping to effectively 'select' the field.
  selectCsvFields(targetFields) {
    return this._upsertMappings('csv', 'company', targetFields)
  }

  // For CSVs, we delete a field mapping to effectively 'deselect' the field.
  deselectCsvFields(targetFields) {
    return this._deleteMappings('csv', 'company', targetFields)
  }

  getSelectedCsvFields() {
    return this._getCurrentMappings('csv', 'company')
  }

  saveSFDCMapping({targetObject, targetFields}) {
    return this._upsertMappings('salesforce', targetObject, targetFields)
  }

  deleteSFDCMapping({targetObject, targetFields}) {
    return this._deleteMappings('salesforce', targetObject, targetFields)
  }

  getSFDCMapping({targetObject}) {
    return this._getCurrentMappings('salesforce', targetObject)
  }

  getMarketoMapping({ targetType }) {
    return this._getCurrentMappings(targetType, 'lead')
  }

  saveMarketoMapping({ targetType, targetFields }) {
    return this._upsertMappings(targetType, 'lead', targetFields)
  }

  deleteMarketoMapping({ targetType, targetFields }) {
    return this._deleteMappings(targetType, 'lead', targetFields)
  }
}

module.exports = {FieldMappingService}
