import _ from 'lodash'

const AUDIENCE_SORT_OPTIONS = [
  {
    text: 'Score',
    key: 'fitScore'
  },
  {
    text: 'Company Name',
    key: 'companyName'
  },
  {
    text: 'Location',
    key: 'state'
  },
  {
    text: 'Industry',
    key: 'industry'
  },
  {
    text: 'Revenue',
    key: 'revenue'
  },
  {
    text: 'Employees',
    key: 'employeeSize'
  },
  {
    text: 'Alexa Rank',
    key: 'alexaRank'
  },
  {
    text: 'URL',
    key: 'domain',
    // hide this sort option if we're sorting with a dropdown menu in lazy-cards view
    tableOnly: true
  }
]

const KEYWORD_RELEVANCE_SCORE = {
  text: 'Keyword Relevance',
  key: 'relevanceScore'
}

const DOMAIN_RELEVANCE_SCORE = {
  text: 'Domain Relevance',
  key: 'relevanceScoreDomain'
}

const DESCENDING = 'desc'
const ASCENDING = 'asc'

class SortService {
  constructor() {
    this.sortOptions = _.cloneDeep(AUDIENCE_SORT_OPTIONS)
  }

  // Return possible sort options.
  getSortOptions({includeKwRelevance = false, includeDmnRelevance = false} = {}) {
    const sortOptions = this.sortOptions
    // If we want to include relevance and it's not in sortOptions yet, then
    // add it to sortOptions and set the current option to be the relevance option
    if (includeKwRelevance && !_.includes(sortOptions, KEYWORD_RELEVANCE_SCORE)) {
      this.sortOptions = _.concat(KEYWORD_RELEVANCE_SCORE, sortOptions)
      this.sortByDesc(0, _.noop)
    }

    if (includeDmnRelevance && !_.includes(sortOptions, DOMAIN_RELEVANCE_SCORE)) {
      this.sortOptions = _.concat(DOMAIN_RELEVANCE_SCORE, sortOptions)
      this.sortByDesc(0, _.noop)
    }

    // If we don't want to include relevance but it's present in sortOptions,
    // remove it and replace the current sort option if needed
    if (!includeKwRelevance && _.includes(sortOptions, KEYWORD_RELEVANCE_SCORE)) {
      this.sortOptions = _.without(sortOptions, KEYWORD_RELEVANCE_SCORE)
      if (this.currentSortOption === KEYWORD_RELEVANCE_SCORE) {
        this.sortByDesc(0, _.noop)
      }
    }

    if (!includeDmnRelevance && _.includes(sortOptions, DOMAIN_RELEVANCE_SCORE)) {
      this.sortOptions = _.without(sortOptions, DOMAIN_RELEVANCE_SCORE)
      if (this.currentSortOption === DOMAIN_RELEVANCE_SCORE) {
        this.sortByDesc(0, _.noop)
      }
    }

    return this.sortOptions
  }

  getOrderVal() {
    return this.orderByValue
  }

  getCurrSortOptn() {
    return this.currentSortOption || {}
  }

  getSortDir() {
    const orderVal = this.getOrderVal()
    if (orderVal && !_.startsWith(orderVal, '-')) {
      return ASCENDING
    } else {
      return DESCENDING
    }
  }

  isAscSort(key) {
    return this.getOrderVal() === key
  }

  isDescSort(key) {
    return this.getOrderVal() === `-${key}`
  }

  sortByAsc(key, cb) {
    let selectedSortOption
    if (typeof key === 'number') {
      selectedSortOption = this.sortOptions[key]
    } else {
      selectedSortOption = _.find(this.sortOptions, (option) => {
        return option.key === key
      })
    }

    this.currentSortOption = selectedSortOption
    this.orderByValue = selectedSortOption.key
    cb()
  }

  sortByDesc(key, cb) {
    let selectedSortOption
    if (typeof key === 'number') {
      selectedSortOption = this.sortOptions[key]
    } else {
      selectedSortOption = _.find(this.sortOptions, (option) => {
        return option.key === key
      })
    }
    this.currentSortOption = selectedSortOption
    this.orderByValue = `-${selectedSortOption.key}`
    cb()
  }

  cancelSort(cb = _.noop) {
    this.orderByValue = undefined
    this.currentSortOption = {}
    cb()
  }

  // For viewing companies in cards. Our UI implies that some sort, either ascending or descending,
  // is always active. Therefore, one can only switch between the two orders.
  changeSortDir(cb) {
    const key = this.currentSortOption.key
    if (this.isAscSort(key)) {
      this.sortByDesc(key, cb)
    } else if (this.isDescSort(key)) {
      this.sortByAsc(key, cb)
    }
  }

  // For viewing companies in a table. Table sort UI imples that sort can be ascending, descending,
  // or off. Rotation goes like this: off -> ascending -> descending -> off
  rotateSortDir(key, cb) {
    if (!this.isAscSort(key) && !this.isDescSort(key)) {
      this.sortByAsc(key, cb)
    } else if (this.isAscSort(key)) {
      this.sortByDesc(key, cb)
    } else if (this.isDescSort(key)) {
      this.cancelSort(cb)
    }
  }

  currentSortDisp() {
    const dispName = this.getCurrSortOptn().text || this.sortOptions[0].text
    const direction = this.getSortDir()
    return `${dispName} (${direction})`
  }
}

module.exports = {SortService}