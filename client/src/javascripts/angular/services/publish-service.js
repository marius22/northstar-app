import * as config from 'config'
import * as errors from 'errors'
import {SALESFORCE} from 'enums'
import Promise from 'bluebird'

import _ from 'lodash'

// Ways in which we can publish a segment.
const PUBLISH_METHODS = [
  {
    key: 'CSV',
    dispName: 'CSV'
  },
  {
    key: 'SFDC',
    dispName: 'Salesforce'
  }
]

const removeEmptyParams = (data) => {
  const newData = {};

  (Object.keys(data)).forEach((key) => {
    const value = data[key]

    if (_.isNull(value) || (_.isArray(value) && _.isEmpty(value))) {
      // Remove null's and empty arrays
      return
    } else if (_.isArray(value)) {
      // If there's a non-empty array value, we keep it
      newData[key] = value
    } else if (_.isObject(value)) {
      // Otherwise, if there's an object value, we also need to clear its empty params
      newData[key] = removeEmptyParams(value)
    } else {
      // Otherwise, we do nothing.
      newData[key] = value
    }
  })

  return newData
}


/**
 * Holds state and has functionality related to publishing the companies
 * and people of a segment.
 */
class PublishService {
  /* @ngInject */
  constructor(segmentService, userService, salesforceService, $http,
              $q, ghostService) {
    this.segmentService = segmentService
    this.$q = $q
    this.userService = userService
    this.salesforceService = salesforceService
    this.ghostService = ghostService
    this.$http = $http

    // `true` once this service has been initialized, including async work.
    this.inited = false
    // Cache the check for whether a user can publish
    this._canPublish = null

    // `this.state` holds the application state of the current export.
    // We're putting this in its own variable to distinguish it from
    // other instance variables.
    this.state = {}
  }

  getCurrentPublishMethod() {
    const curMethod = this.state[this.state.exportType].publishMethod
    return curMethod
  }

  isPublishingToSalesforce() {
    const method = this.getCurrentPublishMethod()
    return method.key === 'SFDC'
  }

  isPublishingToCsv() {
    const method = this.getCurrentPublishMethod()
    return method.key === 'CSV'
  }

  /**
   * Perform async work to set up this service
   */
  initialize(exportType, compTable) {
    this.startNewPublish(exportType, compTable)

    const publishP = this.canPublishToSalesforceAsync()

    return Promise.all([publishP]).then(() => {
      this.basicPublishMethods = this.getAvailablePublishMethods('basic')
      this.advancedPublishMethods = this.getAvailablePublishMethods('advanced')

      this.inited = true
    })
  }

  /**
   * Returns `true` if a publish is allowed, otherwise `false`
   */
  canPublishToSalesforce() {
    // disable for September release
    return false
    // Ghost admins can't publish to Salesforce because it would
    // use their own SF credentials.
    if (this.ghostService.isGhosting()) {
      return false
    }

    if (!_.isNull(this._canPublish)) {
      return this._canPublish
    }

    throw new Error(`Not sure if publishing to Salesforce is allowed`)
  }

  canPublishToSalesforceAsync() {
    const userId = this.userService.user.id
    const packageInstalledP = this.salesforceService.packageInstalled(userId)
    const sfIntegratedP = this.salesforceService.isIntegrated().then((res) => {
      return res.data.data === true
    })
    const dataSyncCompletedP = this.salesforceService.dataSyncCompleted()

    return Promise.all([packageInstalledP, sfIntegratedP, dataSyncCompletedP])
      .spread((packageInstalled, sfIntegrated, dataSyncCompleted) => {
        this._canPublish = Boolean(packageInstalled && sfIntegrated && dataSyncCompleted)
        return this._canPublish
      })
      .catch(() => {
        this._canPublish = false
        return this._canPublish
      })
  }

  /**
   * Get the publish methods available to the user.
   *
   * @param exportType - The export type ('basic' or 'advanced') that we want to do
   */
  getAvailablePublishMethods(exportType) {
    // CSV export is available to both 'basic' and 'advanced'. For the July release,
    // SFDC export is only available for 'basic' because 'advanced' export will
    // be axed soon.
    let methods = PUBLISH_METHODS.filter(method => method.key === 'CSV')
    if (exportType === 'basic' && this.canPublishToSalesforce()) {
      methods = methods.concat(PUBLISH_METHODS.filter(method => method.key === 'SFDC'))
    }

    return methods
  }

  isPublishingContacts() {
    return this.state.numContacts > 0
  }

  /**
   * Whether we're publishing contacts for the given `publishType`.
   */
  isPublishingContactsFor(publishType) {
    return (
      this.state[publishType] &&
      this.isPublishingContacts()
    )
  }

  publishTypeChosen() {
    return (
      this.state.publishNew || this.state.publishExisting
    )
  }

  publishContactsChosen() {
    return (
      this.isPublishingContacts() &&
      (this)
    )
  }

  comparisonChosen(publishType) {
    return (
      this.state.compareAgainstAccounts ||
      this.state.compareAgainstLeads)
  }

  setPublishCompaniesCallback(publishType, value) {
    const key = `${publishType}Details`

    // If we're publishing companies to leads, we can only publish contacts
    // to leads.
    if (value === SALESFORCE.LEAD) {
      this.state[key].publishLeadsTo = SALESFORCE.LEAD
    }
  }

  addTitle(title) {
    const data = [title].concat(this.state.titles)
    this.state.titles = _.uniq(data)
  }

  removeTitle(title) {
    this.state.titles = _.filter(this.state.titles, item => title !== item)
  }

  willPublishCompaniesToLeads(publishType) {
    const key = `${publishType}Details`

    return this.state[key].publishCompaniesTo === SALESFORCE.LEAD
  }

  willPublishCompaniesToAccounts(publishType) {
    const key = `${publishType}Details`

    return this.state[key].publishCompaniesTo === SALESFORCE.ACCOUNT
  }

  publishWithDedupe() {
    const publishMethod = this.getCurrentPublishMethod()
    const payload = {
      format: 'email',
      dedupe: this.state.dedupeAgainstPreviousPublish,
      numCompanies: this.state.numCompanies,
      contactLimitPerCompany: this.state.numContacts,
      publishNew: this.state.publishNew,
      publishExisting: this.state.publishExisting,
      offset: 0,
      onlyExported: this.state.onlyExported,
      onlyPublishAccountHasContact: this.state.onlyPublishAccountHasContact,
      compareAgainstAccounts: this.state.compareAgainstAccounts,
      compareAgainstLeads: this.state.compareAgainstLeads,
      order: this.state.order,
      where: this.state.where,
      publishType: publishMethod.key,
    }

    if (this.isPublishingContacts()) {
      payload.titles = this.state.titles
    }

    if (this.state.publishNew) {
      payload['new'] = this.state.publishNewDetails
    }

    if (this.state.publishExisting) {
      const existingDetailsForServer = _.pick(
        this.state.publishExistingDetails,
        ['publishOnlyIfContactFound'])
      if (this.isPublishingContacts()) {
        if (this.state.compareAgainstAccounts) {
          existingDetailsForServer.foundInAccount = [
            this.state
                .publishExistingDetails
                .foundInAccountsOnly
                .publishLeadsTo
          ]
          if (this.state.compareAgainstLeads) {
            existingDetailsForServer
              .foundInAccountsAndLeads = [
                this.state
                    .publishExistingDetails
                    .foundInBothAccountsAndLeads
                    .publishLeadsTo
              ]
          }
        }
      }

      payload['existing'] = existingDetailsForServer
    }

    const finalPayload = removeEmptyParams(payload)
    return this
      .$http({
        url: `${config.apiBaseURL}segments/${this.segmentService.segment.segmentId}/publish_with_dedupe`,
        method: 'POST',
        data: finalPayload,
      })
      .catch((res) => {
        throw new errors.HttpError(res)
      })
  }

  /**
   * Publish to CSV *without* de-dupe.
   */
  publishToCsv() {
    const payload = {
      limit: this.state.numCompanies,
      offset: 0,
      format: 'email',
      order: this.state.order,
      where: this.state.where,
      titles: this.state.titles,
      dedupe: this.state.dedupeAgainstPreviousPublish,
      contactLimitPerCompany: this.state.numContacts,
      onlyExported: this.state.onlyExported,
      dedupeAgainstCrm: this.state.dedupeAgainstCrm,
    }

    const finalPayload = removeEmptyParams(payload)

    const url = [
      `${config.apiBaseURL}segments/${this.segmentService.segment.segmentId}`,
      `/companies`,
    ].join('')

    return this
      .$http({
        method: 'POST',
        url,
        data: finalPayload,
      })
      .catch((res) => {
        throw new errors.HttpError(res)
      })
  }

  publishToCsvAdvanced() {
    const payload = {
      sectors: this.state.advanced.sectors.join(","),
      format: 'email',
      dedupe: this.state.dedupeAgainstPreviousPublish,
      limit: this.state.numCompanies,
      titles: this.state.titles,
      contactLimitPerCompany: this.state.numContacts,
    }
    const url = [
      `${config.apiBaseURL}segments/${this.segmentService.segment.segmentId}/sector_companies`,
    ].join('')

    return this
      .$http({
        method: 'GET',
        url,
        params: removeEmptyParams(payload)
      })
      .catch((res) => {
        throw new errors.HttpError(res)
      })
  }

  /**
   * Start a new publish for the current segment
   */
  startNewPublish(exportType, compTable) {
    const tableParams = compTable ? compTable.getCurrParams() : {}

    // gather filter information
    const totalFiltered = compTable ? compTable.totalFilteredItems : undefined
    const filterActive = totalFiltered && totalFiltered !== "--"
    const useFilterTotal = exportType === 'basic' && filterActive

    const totalInSegment = this.segmentService.segment.recommendationNum
    const initialNumCompanies = useFilterTotal ? totalFiltered : totalInSegment

    this.state = Object.assign(this.state, {
      // Name of the segment to publish.
      segmentName: this.segmentService.segment.segmentName,

      canPublishContacts: this.userService.canExportContact(),

      // Number of contacts to export for this export
      numContacts: 0,

      // Number of companies to export for this export
      numCompanies: initialNumCompanies,

      // Job titles to publish
      titles: [],

      // The way we'll publish the current set of companies.
      /* currentPublishMethod: null,*/

      // Either 'basic' or 'advanced'.
      exportType,

      basic: {
        publishMethod: PUBLISH_METHODS.filter(method => method.key === 'CSV')[0],
      },
      advanced: {
        publishMethod: PUBLISH_METHODS.filter(method => method.key === 'CSV')[0],
        // The venn diagram sectors to export
        sectors: [],
      },

      // If true, compare companies you're publishing against SF Accounts.
      compareAgainstAccounts: true,

      // Whether to compare against SF Leads when publishing.
      compareAgainstLeads: false,

      // Whether to dedupe against previous publishes.
      dedupeAgainstPreviousPublish: false,

      // Whether to publish new companies,
      publishNew: false,
      publishNewDetails: {

        // Only publish a company if there is at least one contact found for it.
        publishOnlyIfContactFound: false,

        // Which salesforce Object to publish companies to.
        publishCompaniesTo: SALESFORCE.ACCOUNT,

        // Which salesforce Object to publish leads to.
        publishLeadsTo: SALESFORCE.CONTACT,
      },

      // Whether to publish to existing companies, enriching them.
      publishExisting: false,
      publishExistingDetails: {
        // Only publish a company if there is at least one contact found for it.
        publishOnlyIfContactFound: false,

        foundInAccountsOnly: {
          // Which SalesForce Object to publish leads to in the case where
          // companies are found only in Account/Contact
          publishLeadsTo: SALESFORCE.CONTACT,
        },

        foundInBothAccountsAndLeads: {
          // Which SalesForce Object to publish leads to in the case where
          // companies are found in both Account and Lead.
          publishLeadsTo: SALESFORCE.CONTACT
        }
      },

      // Params from a company table indicating what to export
      order: tableParams.order ? tableParams.order : null,
      where: filterActive ? JSON.stringify(tableParams.where) : null,
      onlyExported: tableParams.onlyExported ? 1 : 0,
    })
  }

  publish() {
    if (this.state.exportType === 'advanced') {
      if (this.isPublishingToCsv()) {
        return this.publishToCsvAdvanced()
      }
    } else if (this.state.exportType === 'basic') {
      if (this.canPublishToSalesforce()) {
        return this.publishWithDedupe()
      } else {
        return this.publishToCsv()
      }
    }

    return this.$q.reject(errors.BaseError())
  }
}

module.exports = {PublishService, PUBLISH_METHODS}
