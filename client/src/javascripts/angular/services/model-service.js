import BaseService from './base-service'
import _ from 'lodash'
import Papa from 'papaparse'

const MODEL_NONE = 'NO_MODEL'
const MODEL_IN_PROGRESS = 'IN_PROGRESS'
const MODEL_COMPLETE = 'COMPLETED'

const ACTUAL_MIN_DOMAINS = 50

class ModelService extends BaseService {
  /* @ngInject */
  constructor($http, apiBaseUrl, ghostService, showModalService) {
    super($http, apiBaseUrl)
    this.ghostService = ghostService
    this.sms = showModalService
  }

  getList(accountId) {
    return this.fetch(`accounts/${accountId}/fit_models`, 'GET').then((response) => {
      return response.data.data
    })
  }

  delete(accountId, modelId) {
    const deleteUrl = `accounts/${accountId}/fit_models/${modelId}`
    return this.fetch(deleteUrl, 'DELETE')
  }

  _checkStatus(modelOrStatus, statusEnum) {
    const isStatus = _.isString(modelOrStatus)
    const comparator = isStatus ? modelOrStatus : modelOrStatus.status
    return comparator === statusEnum
  }

  isNone(modelOrStatus) {
    return this._checkStatus(modelOrStatus, MODEL_NONE)
  }

  isProgressing(modelOrStatus) {
    return this._checkStatus(modelOrStatus, MODEL_IN_PROGRESS)
  }

  isComplete(modelOrStatus) {
    return this._checkStatus(modelOrStatus, MODEL_COMPLETE)
  }

  isAccountFit(model) {
    return model.type === 'account_fit'
  }

  httpCRMFitModels() {
    const accountId = this.ghostService.accountId()
    const url = `accounts/${accountId}/crm_fit_models`
    return this.fetch(url, 'GET', {})
  }

  /**
   * Pass csvData to our c2d service, find all the seed domains that we can,
   * and check if it's enough.
   *
   * @param csvData - Array of rows from parsing the CSV
   * @param min - User-facing minimum number of required companies
   * @param fileName - Name of revised CSV file if user chooses to download it
   *
   */
  c2dCheckSeedDomains(csvData, min, fileName) {
    const url = 'model/c2d'
    return this.fetch(url, 'POST', {csvData})
      .then((response) => {
        const domains = response.data.data
        const notEnoughDomains = domains.length < ACTUAL_MIN_DOMAINS
        if (notEnoughDomains) {
          this.handleNotEnoughSeedDomains(domains, min, fileName)
          return false
        } else {
          return true
        }
      })
  }

  /**
   * If we don't find enough seed domains, we show a modal window telling the user what
   * to do next.
   *
   * @param domains - domains that we found from a CSV after going through c2d service
   * @param min - User-facing minimum number of required companies
   * @param fileName - Name of revised CSV file if user chooses to download it
   *
   */
  handleNotEnoughSeedDomains(domains, min, fileName) {
    const totalFound = domains.length
    const addMore = min - totalFound
    const csv = this.generateRevisedCsv(domains, addMore)
    const params = {totalFound, addMore, csv, fileName}
    this.sms.notEnoughDomains(params)
  }

  generateRevisedCsv(domains, addMore) {
    // format fields and data for csv
    const fields = ['company', 'url']
    const data = []
    _.forEach(domains, (domain) => {
      const rowData = ['', domain]
      data.push(rowData)
    })
    for (let i = 0; i < addMore*2; i++) {
      data.push(['',''])
    }
    return Papa.unparse({fields, data})
  }

  downloadSampleCsv() {
    const fields = ['company', 'url']
    const data = [
      ['Visa', 'visa.com'],
      ['Master card', 'https://www.mastercard.us/'],
      ['america express', 'www.americanexpress.com']
    ]
    // sample CSV with 100 blank rows
    for (let i = 0; i < 100; i++) {
      data.push(['',''])
    }
    const sample = Papa.unparse({fields, data})

    const blob = new Blob([sample]);
    const a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
    a.download = 'eap_sample.csv'
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

module.exports = {ModelService}
