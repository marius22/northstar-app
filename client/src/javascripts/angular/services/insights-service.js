class InsightsService {
  /* @ngInject */
  constructor($http,
              apiBaseUrl) {
    this.$http = $http
    this.base = `${apiBaseUrl}insights/`
  }

  categories() {
    return this.$http.get(`${this.base}categories`)
  }

  /**
   * doc: https://docs.google.com/document/d/1N7mQSu9Inl3Hx2fYijI4mrAYF1XXkFKL712J2PBfi64/edit#heading=h.o6uyn5izdoc4
   * @param name
   */
  details(name) {
    return this.$http.get(`${this.base}${name}/details`)
  }
}

module.exports = {InsightsService}
