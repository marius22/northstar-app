class TokenService {
  /* @ngInject */
  constructor(apiBaseUrl, $http, alertService) {
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.alertService = alertService
  }

  getCurrentToken() {
    const url = `${this.apiBaseUrl}token_mgmt/get_token`
    return this.$http
      .get(url)
      .then((response) => {
        const token = response.data.token
        return token
      }, () => {
        this.alertService.addAlert('error', 'Unable to retrieve token.')
      })
  }

  generateNewToken() {
    const url = `${this.apiBaseUrl}token_mgmt/generate_token`
    return this.$http
      .get(url)
      .then((response) => {
        const token = response.data.token
        return token
      }, () => {
        this.alertService.addAlert('error', 'Unable to generate token.')
      })
  }
}

module.exports = {TokenService}