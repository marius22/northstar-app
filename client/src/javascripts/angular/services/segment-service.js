import _ from 'lodash'

class SegmentServiceError extends Error {
  constructor({message}) {
    super()
    this.message = message
    this.name = 'SegmentServiceError'
  }
}

/*
    @ngdoc service
    @name SegmentService
    @description
        SegmentService handles segment-related api calls and provides
        various segment information.
 */

class SegmentService {
  /* @ngInject */
  constructor(apiBaseUrl,
              $http,
              $q,
              $httpParamSerializer) {
    this.apiBaseUrl = apiBaseUrl
    this.segmentBaseUrl = `${apiBaseUrl}segments`
    this.$http = $http
    this.$q = $q
    this.$httpParamSerializer = $httpParamSerializer

    // The segment currently being viewed.
    this.segment = null

    // The current segments that the user can see on their segment console.
    this.segments = []
    this.modelRunParams = {}
    this.previousChoices = {}

    // Whether the current ui-router state for this segment is loading.
    this.isLoading = false
  }

  // Map server-side to client-side data
  _convert(segment) {
    const visibility = this.getVisibility(segment)

    return _.extend(segment, {
      url: `#/segment/${segment.segmentId}/overview`,
      progress: this.calcProgress(segment),
      dispStatus: this.getDispStatus((segment.status)),
      recommendationNum: segment.recommendationNum,
      exportedNum: segment.exportedNum,
      visibility,
      galaxyBgImageNumber: this.getGalaxyBgImageNumber(),
    })
  }

  // Figure out which galaxy image to show in the header of this segment's card.
  getGalaxyBgImageNumber() {
    return Math.ceil(Math.random() * 10)
  }

  /**
   * Returns the human-readable status from a status value.
   * @param status - the status value to generate a human-readable status from.
   * @returns {*}
   */
  getDispStatus(status) {
    const mapping = {
      EMPTY: 'Empty',
      COMPLETED: 'Completed',
      DRAFT: 'Draft',
      'IN PROGRESS': 'In Progress'
    }

    return mapping[status]
  }

  getAllAsync(ghostId = -1) {
    let url = this.segmentBaseUrl
    if (ghostId && ghostId > 0) {
      url += `?accountId=${ghostId}`
    }
    return this.$http.get(url).then(
      (response) => {
        this.segments = _.map(response.data.data, _.bind(this._convert, this))
        this.canPublishTo = response.data.canPublishTo

        this.canCreateSegments = response.data.canCreateSegments
        return response
      },
      (response) => {
        return this.$q.reject(response)
      }
    )
  }

  getByIdAsync(segmentId) {
    return this.$http.get(`${this.segmentBaseUrl}/${segmentId}`).then(
      (response) => {
        const segment = response.data.data
        this.segment = segment
        return segment
      },
      (response) => {
        return this.$q.reject(response)
      })
  }

  publishAsync(segment) {
    const url = `${this.segmentBaseUrl}/${segment.segmentId}/publish`

    return this.$http.post(url)
      .then((res) => {
        return this._convert(res.data.data)
      })
      .catch((res) => {
        return this.$q.reject(res)
      })
  }

  unpublishAsync(segment) {
    const url = `${this.segmentBaseUrl}/${segment.segmentId}/unpublish`

    return this.$http.post(url)
      .then((res) => {
        return this._convert(res.data.data)
      })
      .catch((res) => {
        return this.$q.reject(res)
      })
  }

  /**
   * Visibility is whether this segment is visible to others.
   */
  getVisibility(segmentObj) {
    if (segmentObj.isShared && segmentObj.isOwner) {
      return 'published'
    } else if (segmentObj.isShared && !segmentObj.isOwner) {
      return 'shared'
    }

    return 'private'
  }

  calcProgress(segment) {
    // convert to local timeStamp
    const startTimeStamp = +new Date(segment.lastRunTime || segment.createdTime)
    const duration = (+new Date - startTimeStamp) / 1000 // second
    const expectedDuration = 20 * 60 // 20 minutes
    let percent = 0
    let result
    if (duration <= 0) {
      result = '0%'
    } else if (duration < expectedDuration) {
      // if not completed, max percent is 95%
      percent = duration * 0.95 * 100 / expectedDuration
      percent = percent.toFixed(0)
      result = `${percent}%`
    } else {
      const maxDuration = 2 * 365 * 24 * 3600 // two years
      // if duration exceeds 30 minutes, the exceeded part will account 5% at most
      percent = (0.95 + 0.05 * (duration - expectedDuration) / maxDuration) * 100
      percent = percent.toFixed(0)
      result = `${percent}%`
    }
    return result
  }

  /* payload is expected like {companyId: 123,fieldName: "companyName"}
  * @method can be post or delete, post for adding a record, delete for removing the recode
  * */

  dispute(segmentId, payload, method) {
    let url = `${this.segmentBaseUrl}/${segmentId}/disputes`
    if (method === 'delete') {
      url += `?${this.$httpParamSerializer(payload)}`
    }
    return this.$http[method](url, payload)
  }

  segmentCount() {
    return this.segments.length
  }

  /**
   * Get frequency data about properties of the companies in this segment.
   * Caches the values so we don't need to constantly fetch the same data
   * as we change between tabs.
   *
   * segmentId - The ID of the segment you're interested in.
   * keys - an array of strings of facets that you're interested in, such as ['industry']
   */
  getDistinctCountsByKeys(segmentId, keys, companyType) {
    let path
    if (companyType === 'seed') {
      path = 'seeds'
    } else if (companyType === 'recommendation') {
      path = 'companies'
    } else {
      throw new SegmentServiceError({message: `Invalid companyType: ${companyType}`})
    }
    const baseUrl = `${this.apiBaseUrl}segments/${segmentId}/${path}/distinct_counts`
    const search = `?keys=${keys.join()}`
    const finalUrl = [baseUrl, search].join('')
    return this.$http.get(finalUrl).then((response) => {
      return response.data.data
    })
  }

  /**
   * Get dimensions that are available that we can find insights for.
   *
   * e.g. If we know that tech stack is an available dimension,
   * we can see what tech stacks are present.
   */
  getDimensions(companyType) {
    let path
    // TODO: Make these enums
    if (companyType === 'seed') {
      path = 'seeds'
    } else if (companyType === 'recommendation') {
      path = 'recommendations'
    } else {
      throw new SegmentServiceError({message: `Invalid companyType: ${companyType}`})
    }

    const url = `${this.apiBaseUrl}${path}/dimension`
    return this.$http.get(url).then((res) => {
      const dimensions = res.data.data
      return dimensions
    })
  }

  getInsights(segmentId, companyType) {
    const dimensions = [
      'revenue',
      'employeeSize',
      'industry',
      'state'
    ]

    let advancedDimensions = []
    const advancedDimensionsP = this.getDimensions(companyType)
    return advancedDimensionsP.then((serverAdvancedDimensions) => {
      advancedDimensions = serverAdvancedDimensions
      const allDimensions = dimensions.concat(advancedDimensions)
      return this.getDistinctCountsByKeys(
        segmentId, allDimensions, companyType
      )
    }).then((overviewData) => {
      return {
        basicInsights: _.pick(overviewData, dimensions),
        advancedInsights: _.pick(overviewData, advancedDimensions)
      }
    })
  }

  /**
   * Get the summary data of the companies found. Used in the venn diagram of the segment
   * overview page.
   */
  getCompanySummary(segmentId) {
    return this.$http.get(`${this.apiBaseUrl}segments/${segmentId}/companies/overview`)
      .then((res) => {
        return res.data
      })
  }

  /* Creation Flow Helpers */


  /*
    Return the qual criteria found in seed list that were not previously
    selected for the given segmentId
  */
  newCriteriaFromSeed(segmentId) {
    const prevChoicesPromise = this.getPrevChoices('qualification', segmentId).then(
      (response) => {
        return response.data.data
      }
    )
    const categories = ['state', 'industry', 'employeeSize']
    const seedAttrPromise = this.getSeedAttrData(categories, segmentId).then(
      (response) => {
        return response.data.data
      }
    )
    const promises = [prevChoicesPromise, seedAttrPromise]
    return this.$q.all(promises).then(
      (result) => {
        const prevChoices = result[0]
        const seedAttrs = result[1]

        const seedAttrsNotInPrevChoices = {}

        _.forEach(categories, (category) => {
          const prevCategoryItems = prevChoices[category]
          const seedCategoryItems = seedAttrs[category]
          seedAttrsNotInPrevChoices[category] = _.filter(seedCategoryItems, (seedItem) => {
            return !_.includes(prevCategoryItems, seedItem)
          })
        })
        return seedAttrsNotInPrevChoices
      }
    )
  }

  /*
    Set the qual criteria to be the union of criteriaFromSeed and prevQualChoices
    for the given segmentId
  */
  addNewSeedQualCriteria(criteriaFromSeed, prevQualChoices, segmentId) {
    const qualification = {}
    _.forEach(criteriaFromSeed, (critList, category) => {
      qualification[category] = _.union(prevQualChoices[category], critList)
    })

    const url = `${this.apiBaseUrl}segments/${segmentId}/qualification`
    const params = {qualification}
    return this.$http.put(url, params)
  }

  /* Get the qualification and expansion selections the user made for this segmentId */
  getPrevChoices(type, segmentId) {
    const url = `${this.apiBaseUrl}segments/${segmentId}/${type}`
    return this.$http.get(url)
  }

  /* Get all qualification and expansion criteria found in the uploaded seed list */
  getSeedAttrData(categories, segmentId) {
    const url = `${this.apiBaseUrl}segments/${segmentId}/seed_attributes?keys=${encodeURIComponent(categories.join(','))}`
    return this.$http.get(url)
  }

  /* Get all possible data values for categories in our universe */
  getUniverseCategoryData(categories) {
    const url = `${this.apiBaseUrl}segments/universe_attributes?keys=${encodeURIComponent(categories.join(','))}`
    return this.$http.get(url)
  }

  /* Check if user has uploaded seed list already for the given segmentId */
  hasSeedList(segmentId) {
    const url = `${this.apiBaseUrl}segments/${segmentId}/has_seeds`
    return this.$http.get(url)
  }
}

module.exports = {SegmentService}
