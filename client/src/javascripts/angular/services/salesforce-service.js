import * as config from 'config'

import _ from 'lodash'

// enums the back-end uses for the type of integration
const SALESFORCE_PRODUCTION = 'SALESFORCE'
const SALESFORCE_SANDBOX = 'SALESFORCE_SANDBOX'

class SalesforceService {
  /* @ngInject */
  constructor($http,
              apiBaseUrl,
              $interval,
              $timeout,
              userService,
              showModalService,
              alertService,
              $state) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.$interval = $interval
    this.$timeout = $timeout
    this.userService = userService
    this.alertService = alertService
    this.$state = $state
    this.sms = showModalService

    // Cache whether a user is integrated with Salesforce
    this._isIntegrated = null
  }

  canIntegrate() {
    return this.userService.isAcctAdmin() || this.userService.isGstAdmin() || this.userService.isSuper()
  }

  fetchData() {
    return this
      .getIntegrationTypeAsync()
      .then((response) => {
        const integrationType = response.data.data
        // Only use sandbox if user is already integrated with sandbox,
        // otherwise use standard production package link and logic.
        // integrationType === False if the user has not integrated, so we go with
        // the standard production logic in that case.
        const alreadyIntegratedWithSandbox = integrationType === SALESFORCE_SANDBOX
        let packageLinkFn
        if (alreadyIntegratedWithSandbox) {
          return this.useSandbox()
        } else {
          return this.useProduction()
        }
      })
  }

  /**
   * Whether the current user's account has been OAuth-integrated with Salesforce
   */
  isIntegrated({forceCheck = false} = {}) {
    if (!_.isNull(this._isIntegrated) && !forceCheck) {
      return new Promise(resolve => {
        resolve({
          data: {
            data: this._isIntegrated
          }
        })
      })
    }

    return this.$http.get(`${this.apiBaseUrl}salesforce/integrated`).then(res => {
      this._isIntegrated = res.data.data
      // Need to return `res`
      return res
    })
  }

  /**
   * Get managed package link for sandbox. Also sets useSandbox to true, which will be checked
   * to drive certain logic in other parts of the code.
   */
  useSandbox() {
    return this.$http.get(`${this.apiBaseUrl}salesforce/managed_package_link?isSandbox=true`).then(
      (response) => {
        this.packageInstallLink = response.data.link
        this.sandboxMode = true
        return this.packageInstallLink
      }
    )
  }

  /**
   * Use standard prod salesforce.
   */
  useProduction() {
    return this.$http.get(`${this.apiBaseUrl}salesforce/managed_package_link`).then(
      (response) => {
        this.packageInstallLink = response.data.link
        this.sandboxMode = false
        return this.packageInstallLink
      }
    )
  }

  /**
   * Whether the current user's account has integrated with production or sandbox
   * salesforce. False if no integration is found.
   */
  getIntegrationTypeAsync() {
    return this.$http.get(`${this.apiBaseUrl}salesforce/crm_type`)
  }

  /**
   * Whether the specified user's account has a managed package installed.
   *
   * @param userId - The ID of the user that we're checking
   */
  packageInstalled(userId) {
    return this.$http.get(`${this.apiBaseUrl}users/${userId}/sfdc/managed_package_status`)
  }

  /**
   * Whether the Salesforce account associated with this user's EAP account has completed
   * a data sync.
   *
   * A data sync is the process of mirroring a Salesforce account's data into EAP's database.
   */
  dataSyncCompleted() {
    const accountId = this.userService.user.accountId

    return this.$http
               .get(`${this.apiBaseUrl}accounts/${accountId}/datasync_initialized`)
               .then((res) => {
                 return res.data.data.initialized
               })
  }

  getData(segmentId) {
    return this.$http.get(`${this.apiBaseUrl}segments/${segmentId}/seeds/sfdc`)
  }

  /**
   * @param type - 'accounts' or 'segments', just used in the url.
   * @param typeId - accountId or segmentId depending on type
   * @param payload - object with startDate and endDate to define
   *                  the date range with which to pull from SFDC
   */
  postData(typeId, type, payload) {
    return this.$http.post(`${this.apiBaseUrl}${type}/${typeId}/seeds/sfdc`, _.extend({
      startDate: null,
      endDate: null
    }, payload))
  }

  getConfig(accountId) {
    const url = `${this.apiBaseUrl}salesforce/${accountId}/config`
    return this.$http.get(url)
  }

  /**
   * docs: https://docs.google.com/document/d/1N7mQSu9Inl3Hx2fYijI4mrAYF1XXkFKL712J2PBfi64/edit#heading=h.1lv6e275nu75
   * @param payload
   */
  saveConfig(accountId, payload) {
    const url = `${this.apiBaseUrl}salesforce/${accountId}/config`
    return this.$http.post(url, payload)
  }

  removeToken() {
    return this.$http.delete(`${this.apiBaseUrl}salesforce/revoke`)
  }

  // type can be unknown or matches
  getCompanies(segmentId, params) {
    const payload = _.extend({
      offset: 0,
      limit: 10,
      order: '-company_name',
      type: 'matches'
    }, params)
    return this.$http.get(`${this.apiBaseUrl}${segmentId}/seeds/overview`, payload)
  }

  openWindow(url, title, onClose, fullScreen) {
    const winWidth = 700
    const winHeight = 500
    const left = (screen.width - winWidth) / 2
    const top = (screen.height - winHeight) / 2
    const features = fullScreen ? '' :
      `location=1,status=1,scrollbars=1,width=${winWidth},height=${winHeight},left=${left},top=${top}`
    const win = window.open(url, title, features)

    let cancel = () => {}
    if (onClose && win && !_.isUndefined(win.closed)) {
      const promise = this.$interval(() => {
        if (win && win.closed) {
          this.$interval.cancel(promise)
          onClose()
        }
      }, 500)
      cancel = () => {
        this.$interval.cancel(promise)
        win.close()
      }
    }

    // expose cancel method
    return {cancel}
  }

  /**
   * open the Salesforce authentication window
   * @param callback
   * @returns {cancel: fn}
   */
  openSFDCOauthWindow(callback) {
    if (!this.canIntegrate()) {
      const modalMessage = 'Please contact your account administrator to access this feature.'
      const modalParams = {
        title: 'Error',
        message: modalMessage,
        messageType: 'error'
      }
      this.sms.messageModal(modalParams)
      if (_.isFunction(callback)) {
        callback()
      }
      return {
        cancel: () => {}
      }
    }
    let url = `${this.apiBaseUrl}salesforce/integration`
    if (this.usingSandbox()) {
      url = url + '?isSandbox=true'
    }
    return this.openWindow(url, 'Integrate with Salesforce', callback)
  }

  openInstallPackageWindow(callback) {
    const fullScreen = true
    return this.openWindow(this.packageInstallLink, 'Install EverString Package', callback, fullScreen)
  }

  isCRMContext() {
    return this.$state.includes('crm')
  }

  triggerAcctFitModelRun(type = 'CRM') {
    this.accountId = this.userService.accountId()
    const url = `${this.apiBaseUrl}accounts/${this.accountId}/submit_fit_job`
    const params = {type}
    const successHandler = () => {
      this.$state.go('audience.console')
      const msg = `Your model is in progress. You'll get an email when it's ready.`
      this.alertService.addAlert('success', msg)
    }
    const errorHandler = (response) => {
      this.alertService.addAlert('error', response.data.message)
    }
    this.$http.post(url, params).then(
      successHandler,
      errorHandler
    )
  }

  getManagedPackageUrl() {
    return this.packageInstallLink
  }

  usingSandbox() {
    return this.sandboxMode
  }

  httpAddSfdcObj(domain, accountId) {
    const api = `${this.apiBaseUrl}salesforce/add_sfdc_obj`
    const params = {
      account_id: accountId,
      domain
    }
    return this.$http.post(api, params)
  }

}

module.exports = {SalesforceService}
