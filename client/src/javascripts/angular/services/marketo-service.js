import _ from 'lodash'

class MarketoService {
  /* @ngInject */
  constructor($http,
              apiBaseUrl,
              $interval,
              $timeout,
              userService,
              showModalService,
              alertService,
              $state) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.$interval = $interval
    this.$timeout = $timeout
    this.userService = userService
    this.alertService = alertService
    this.$state = $state
    this.sms = showModalService
    this.integrationStatus = {}

    this.init()
  }

  init() {

  }

  isIntegrated(type) {
    return this.$http.get(`${this.apiBaseUrl}mas/integration?mas_type=${type}`)
  }

  revokeIntegration({ integrationType}) {
    return this.$http({
      method: 'POST',
      url: `${this.apiBaseUrl}mas/revoke_integration`,
      data: {
        mas_type: integrationType,
      }
    })
  }

  getAudiences() {
    return this.$http.get(`${this.apiBaseUrl}audiences`)
  }

  getMapping(masType) {
    return this.$http.get(`${this.apiBaseUrl}mas/fields_mapping`, {
      params: {mas_type: masType}
    }).then(res => res.data.data)
  }

  deleteMapping({ masType, mappingToDelete }) {
    return this
      .$http({
        url: `${this.apiBaseUrl}mas/fields_mapping`,
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        data: {
          mas_type: masType,
          mappings_to_delete: mappingToDelete
        }
      })
      .then((res) => {
        // The entire set of new mappings data
        const newMappings = res.data.data.mapping

        return newMappings
      })
  }

  /**
   * Get the contact mappings which are fixed and unchangeable. These are hard-coded mappings that
   * aren't saved in the `publish_field_mapping` table.
   */
  getFixedMappings() {
    return this
      .$http({
        method: 'GET',
        url: `${this.apiBaseUrl}mas/contact_default_field_mapping`
      })
      .then((res) => {
        return res.data.data
      })
  }

  getMarketoFieldsAsync({ integrationType }) {
    return this.$http.get(`${this.apiBaseUrl}mas/marketo_lead_fields`, {
      params: {mas_type: integrationType.value}
    }).then((res) => {
      if (res.data.data) {
        const marketoFields = _(res.data.data)
          .map(item => {
            return {
              value: item.rest ? item.rest.name : null,
              type: item.dataType,
              label: item.displayName
            }
          })
        // Only keep items with values.
          .filter(item => item.value).value()

        return marketoFields
      }

      this.isLoading = false
      this.integrationStatus = true
    }).catch((resp) => {
      this.alertService.addAlert(
        'error', resp.message || "Failed to get mapping data.")
      this.integrationStatus = false
      this.isLoading = false
    })
  }
}

module.exports = {MarketoService}
