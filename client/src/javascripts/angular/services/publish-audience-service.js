import * as config from 'config'

import {SALESFORCE} from 'enums'

// This import needed for Promise.spread and other promise addons.
import Promise from 'bluebird'
import _ from 'lodash'

// The max job inputTitles you can give as input.
const MAX_TITLES = 10


class JobTitle {
  constructor() {
    this.jobTitle = ''
    this.match = 'Fuzzy'
    this.limit = 1
  }

  /**
   * We're not using ng-model for `jobTitle` because it strips the quotation marks around
   * exact titles.
   */
  updateJobTitle(evt) {
    this.jobTitle = evt.target.value
  }
}

// Ways in which we can publish a segment.
const audiencePublishMethods = [
  {
    key: 'CSV',
    dispName: 'CSV',
    // CSV Export is always enabled
    enabled: true
  },
  {
    key: 'SFDC',
    dispName: 'Salesforce',
    // SFDC export is enabled if the user has integrated with Salesforce
    enabled: false
  },
  {
    key: 'MAS',
    dispName: 'Marketo',
    // Marketo export is enabled if the user has integrated with Marketo
    enabled: false,
    productionEnabled: false,
    sandboxEnabled: false
  }
]


/**
 * Service to publish audiences (as opposed to segments, which are published by
 * PublishService.)
 */
class PublishAudienceService {
  /* @ngInject */
  constructor(audienceService, salesforceService, $http, ghostService,
              userService, marketoService, sortService) {
    this.audienceService = audienceService
    this.salesforceService = salesforceService
    this.$http = $http
    this.ghostService = ghostService
    this.userService = userService
    this.marketoService = marketoService
    this.sortService = sortService

    // `true` once this service has been initialized, including async work.
    this.inited = false

    // `this.state` holds the application state of the current export.
    // We're putting this in its own variable to distinguish it from
    // other instance variables.
    this.state = {}
  }

  setPublishMethod(method) {
    if (method.key === 'MAS') {
      // Marketo is a list of people, so you're publishing contacts
      this.state.isPublishingContacts = true
    }

    this.state.currentPublishMethod = method
  }

  getCurrentPublishMethod() {
    return this.state.currentPublishMethod
  }

  isPublishingToSalesforce() {
    const method = this.getCurrentPublishMethod()
    return method.key === 'SFDC'
  }

  isPublishingToCsv() {
    const method = this.getCurrentPublishMethod()
    return method.key === 'CSV'
  }

  isPublishingToCsvWithoutDedupe() {
    return this.isPublishingToCsv() && !this.canPublishToSalesforce()
  }

  isPublishingToCsvWithDedupe() {
    return this.isPublishingToCsv() && this.canPublishToSalesforce()
  }

  isPublishingToMarketo() {
    const method = this.getCurrentPublishMethod()
    return method.key === 'MAS'
  }

  isPublishingNewToMarketo() {
    return this.isPublishingToMarketo() && this.state.publishNew
  }

  /**
   * Perform async work to set up this service
   */
  initialize(audience, maxCompanies, criteria) {
    this.state = {
      // The fields of companies that we're going to publish for SFDC/CSV.
      publishFields: [],
      audience,
      companiesNumInAudience: audience.recommendationNum,
      companiesNumInAudienceAfterFilter: maxCompanies,

      contactsNumBeforeDedupe: 0,

      audienceId: audience.id,
      audienceRequestJson: null,
      // Either 'marketo' or 'sandbox'. For marketo only
      masType: null,

      // The marketo list objects for the multiselect
      masList: [],
      // The MAS List for scoring.
      selectedMasListForScoring: {},
      // List of marketo list IDs
      masListIds: [],

      // The MAS list objects for dedupe that we can select from
      masListsForDedupe: [],
      // The list we selected for dedupe
      masListForDedupe: {},

      // Whether we're publishing contacts. If we're publishing to
      // Marketo, we must publish contacts.
      _isPublishingContacts: false,
      set isPublishingContacts(val) {
        if (val) {
          this.numContacts = 1
        } else {
          this.numContacts = 0
        }
        this._isPublishingContacts = val
      },
      get isPublishingContacts() {
        return this._isPublishingContacts
      },

      // The filter criteria of the companies to publish
      criteria,

      // Name of the segment to publish.
      name: audience.name,

      canPublishContacts: this.userService.canExportContact(),

      // Number of contacts per company to export for this audience
      numContacts: 0,

      // Number of companies to export for this audience
      //
      // We're starting this number at 0 to force the user to pick how many companies they want
      // to publish. We could start at a higher number, but we found that users tend
      // to just click through all the publish steps without reading, so it's better to
      // start at 0.
      numCompanies: 0,

      // The maximum number of companies we can publish
      maxCompanies,

      // Job inputTitles to publish
      inputTitles: _.times(5, () => {
        return new JobTitle()
      }),

      seniority: [],

      // The way we'll publish the current set of companies.
      currentPublishMethod: null,

      // If true, compare companies you're publishing against SF Accounts.
      compareAgainstAccounts: true,

      // Whether to compare against SF Leads when publishing.
      compareAgainstLeads: true,

      // Whether to dedupe against previous publishes.
      dedupeAgainstPreviousPublish: false,

      // Whether to publish new companies,
      publishNew: false,
      publishNewDetails: {

        // Only publish a company if there is at least one contact found for it.
        publishOnlyIfContactFound: false,

        // Which salesforce Object to publish companies to.
        publishCompaniesTo: SALESFORCE.ACCOUNT,

        // Which salesforce Object to publish leads to.
        publishLeadsTo: SALESFORCE.CONTACT,
      },

      // Whether to publish to existing companies, enriching them.
      publishExisting: false,
      publishExistingDetails: {
        // Only publish a company if there is at least one contact found for it.
        publishOnlyIfContactFound: false,

        foundInAccountsOnly: {
          // Which SalesForce Object to publish leads to in the case where
          // companies are found only in Account/Contact
          publishLeadsTo: SALESFORCE.CONTACT,
        },

        foundInBothAccountsAndLeads: {
          // Which SalesForce Object to publish leads to in the case where
          // companies are found in both Account and Lead.
          publishLeadsTo: SALESFORCE.CONTACT
        }
      },
    }

    this.$http({
      method: 'GET',
      url: `${config.apiBaseURL}seniority`
    }).then((res) => {
      this.state.seniority = res.data.data.map((s) => {
        return {
          name: s,
          selected: false
        }
      })
    })

    const canPublishSalesforceP = this.canPublishToSalesforceAsync()
    const marketoPs = [];

    (['marketo', 'marketo_sandbox']).forEach(masType => {
      marketoPs.push(this.canPublishToMarketoAsync(masType))
    })

    return Promise.all([canPublishSalesforceP].concat(marketoPs)).then(() => {
      this.publishMethods = audiencePublishMethods

      this.inited = true
    })
  }

  /**
   * Returns `true` if a publish is allowed, otherwise `false`
   */
  canPublishToSalesforce() {
    // Ghost admins can't publish to Salesforce because it would
    // use their own SF credentials.
    if (this.ghostService.isGhosting()) {
      return false
    }

    const sfPubMethod = audiencePublishMethods.find(m => m.key === 'SFDC')

    return sfPubMethod.enabled
  }

  canPublishToSalesforceAsync() {
    const userId = this.userService.user.id
    const packageInstalledP = this.salesforceService.packageInstalled(userId)
    const sfIntegratedP = this.salesforceService.isIntegrated().then((res) => {
      return res.data.data === true
    })
    const dataSyncCompletedP = this.salesforceService.dataSyncCompleted()
    const sfPubMethod = audiencePublishMethods.find(m => m.key === 'SFDC')

    return Promise.all([packageInstalledP, sfIntegratedP, dataSyncCompletedP])
      .spread((packageInstalled, sfIntegrated, dataSyncCompleted) => {
        sfPubMethod.enabled = Boolean(packageInstalled && sfIntegrated && dataSyncCompleted)

        return sfPubMethod.enabled
      })
      .catch(() => {
        sfPubMethod.enabled = false
        return sfPubMethod.enabled
      })
  }

  /**
   * Check whether we can publish to a specific Marketo type
   */
  canPublishToMarketo(masType) {
    const marketoPubMethod = audiencePublishMethods.find(m => m.key === 'MAS')

    if (masType === 'marketo') {
      return marketoPubMethod.enabled && marketoPubMethod.productionEnabled
    } else if (masType === 'marketo_sandbox') {
      return marketoPubMethod.enabled && marketoPubMethod.sandboxEnabled
    }
  }

  /**
   * Tests whether we have production or sandbox marketo integration
   * @param masType - Either 'marketo' for production marketo for 'marketo_sandbox'
   *     for sandbox accounts
   */
  canPublishToMarketoAsync(masType) {
    // If you can't export contacts, then Marketo publish is not an option for you.
    if (!this.state.canPublishContacts) {
      return
    }

    const marketoPubMethod = audiencePublishMethods.find(m => m.key === 'MAS')
    const integratedP = this
      .marketoService.isIntegrated(masType)
      .then(res => {
        return res.data.data
      })

    return Promise.all([integratedP])
      .spread((isIntegrated) => {
        if (isIntegrated) {
          if (masType === 'marketo') {
            marketoPubMethod.productionEnabled = true
            marketoPubMethod.enabled = true
          } else if (masType === 'marketo_sandbox') {
            marketoPubMethod.sandboxEnabled = true
            marketoPubMethod.enabled = true
          }
        }

        return false
      })
      .catch(err => {
        marketoPubMethod.enabled = false
        return marketoPubMethod.enabled
      })
  }

  isPublishingContacts() {
    return this.state.isPublishingContacts
  }

  /**
   * Whether we're publishing contacts for the given `publishType`.
   */
  isPublishingContactsFor(publishType) {
    return (
      this.state[publishType] &&
        this.isPublishingContacts()
    )
  }

  publishTypeChosen() {
    return (
      this.state.publishNew || this.state.publishExisting
    )
  }

  publishContactsChosen() {
    return (
      this.isPublishingContacts() &&
        (this)
    )
  }

  comparisonChosen(publishType) {
    return (
      this.state.compareAgainstAccounts ||
        this.state.compareAgainstLeads)
  }

  setPublishCompaniesCallback(publishType, value) {
    const key = `${publishType}Details`

    // If we're publishing companies to leads, we can only publish contacts
    // to leads.
    if (value === SALESFORCE.LEAD) {
      this.state[key].publishLeadsTo = SALESFORCE.LEAD
    }
  }

  willPublishCompaniesToLeads(publishType) {
    const key = `${publishType}Details`

    return this.state[key].publishCompaniesTo === SALESFORCE.LEAD
  }

  willPublishCompaniesToAccounts(publishType) {
    const key = `${publishType}Details`

    return this.state[key].publishCompaniesTo === SALESFORCE.ACCOUNT
  }

  /**
   * Get the job titles that we will publish. We must select at least
   * one contact per title.
   *
   * @returns {Array[JobTitle]}
   */
  getValidTitles() {
    const selectedTitles = _(this.state.inputTitles)
      .filter((item) => {
        return item.limit > 0 && item.jobTitle
      }).value()

    const finalTitles = selectedTitles.map((item) => {
      const exactTitlePattern = /"(.+)"/
      const matches = exactTitlePattern.exec(item.jobTitle)
      const newTitle = new JobTitle()
      newTitle.limit = item.limit

      if (matches) {
        const exactTitle = matches[1]
        newTitle.match = 'Exact'
        newTitle.jobTitle = exactTitle
      } else {
        newTitle.match = 'Fuzzy'
        newTitle.jobTitle = item.jobTitle
      }

      return newTitle
    })

    return finalTitles
  }

  getSelectedSeniority() {
    return this.state.seniority.filter(item => {
      return item.selected
    }).map(item => {
      return item.name
    })
  }

  getSelectedMasLists() {
    if (this.state.publishNew) {
      return this.state.masList.filter(item => {
        return item.selected
      })
        .map(item => {
          return {
            id: item.id,
            name: item.name,
          }
        })
    } else if (this.state.publishExisting) {
      return [_.pick(this.state.selectedMasListForScoring, ['id', 'name'])]
    }

    return []
  }

  getSelectedMasDedupeLists() {
    if (this.state.masListForDedupe.id) {
      const list = this.state.masListForDedupe
      return [{
        id: list.id,
        name: list.name,
      }]
    }
    return []
  }

  /**
   * Get the `publishType` for publish/publish_summary
   */
  getPublishType() {
    let publishType

    if (this.state.currentPublishMethod.key === 'CSV') {
      if (this.canPublishToSalesforce()) {
        publishType = 'CSV'
      } else {
        publishType = 'CSV0'
      }
    } else {
      publishType = this.state.currentPublishMethod.key
    }

    return publishType
  }

  getDedupeParams() {
    const existingDetailsForServer = _.pick(
      this.state.publishExistingDetails,
      ['publishOnlyIfContactFound'])
    if (this.isPublishingContacts()) {
      if (this.state.compareAgainstAccounts) {
        existingDetailsForServer.foundInAccount = [
          this.state
            .publishExistingDetails
            .foundInAccountsOnly
            .publishLeadsTo
        ]
        if (this.state.compareAgainstLeads) {
          existingDetailsForServer
            .foundInAccountsAndLeads = [
              this.state
                .publishExistingDetails
                .foundInBothAccountsAndLeads
                .publishLeadsTo
            ]
        }
      }
    }

    return {
      contactLimitPerCompany: this.state.numContacts,
      compareAgainstAccounts: this.state.compareAgainstAccounts,
      compareAgainstLeads: this.state.compareAgainstLeads,
      publishType: this.getPublishType(),
      publishNew: this.state.publishNew,
      publishExisting: this.state.publishExisting,
      dedupeAgainstPreviousPublish: this.state.dedupeAgainstPreviousPublish,
      new: this.state.publishNewDetails,
      existing: existingDetailsForServer,
    }
  }

  getPublishSummary() {
    const payload = {
      audienceId: this.state.audienceId,
      modelId: this.state.audience.modelId,
      totalPublishAccountNum: this.state.numCompanies,
      titles: this.getValidTitles(),
      manageLevels: this.getSelectedSeniority(),
      contactLimitPerCompany: this.state.numContacts,
      criteria: this.state.criteria,
      publishType: this.state.publishType
    }
    Object.assign(payload, this.getDedupeParams())

    return this.$http({
      url: `${config.apiBaseURL}audiences/publish_summary`,
      method: 'POST',
      data: payload,
      // The intent here is to never time out this request because it might be quite slow.
      // There are a couple of caveats for this timeout value. It doesn't accept values that
      // are very large, like Number.MAX_VALUE, so I'm forced to use a smaller number.
      timeout: 99999999
    }).then((res) => {
      return res.data.data
    })
  }

  getSelectedPublishFields() {
    return this.state.publishFields.filter(field => field.selected)
  }

  /* Pull the trigger on publishing methods */
  publish() {
    const payload = {
      totalPublishAccountNum: this.state.numCompanies,
      publishFields: _.map(this.getSelectedPublishFields(), 'key'),
      audienceId: this.state.audienceId,
      modelId: this.state.audience.modelId,
      criteria: this.state.criteria,
      companiesNumInAudience: this.state.companiesNumInAudience,
      companiesNumInAudienceAfterFilter: this.state.companiesNumInAudienceAfterFilter,
      contactsNumBeforeDedupe: this.state.contactsNumBeforeDedupe
    }
    payload.publishType = this.getPublishType()

    const orderVal = this.sortService.getOrderVal()
    // If orderBy is not specified to our common service's /audiences/<audience_id>/companies endpoint,
    // it will default to ordering by descending fitscore.
    // I am explicitly specifying this here anyway, to hopefully prevent confusion from otherwise
    // relying on implicit default logic.
    payload.orderBy = orderVal ? orderVal : '-fitScore'

    if (this.state.currentPublishMethod.key === 'MAS') {
      Object.assign(payload, {
        masType: this.state.masType,
        masLists: this.getSelectedMasLists(),
        masListsForDedupe: this.getSelectedMasDedupeLists(),
        publishNew: this.state.publishNew,
        publishExisting: this.state.publishExisting,
      })
    }

    if (this.canPublishToSalesforce()) {
      Object.assign(payload, this.getDedupeParams())
    }

    if (this.state.isPublishingContacts) {
      Object.assign(payload, {
        contactLimitPerCompany: this.state.numContacts,
        titles: this.getValidTitles(),
        manageLevels: this.getSelectedSeniority(),
      })
    }

    return this.$http({
      url: `${config.apiBaseURL}audiences/${this.state.audienceId}/publish`,
      method: 'POST',
      data: payload
    })
  }

  addTitle() {
    if (!this.canAddTitles()) {
      return
    }

    this.state.inputTitles.push(new JobTitle())
  }

  canAddTitles() {
    return this.state.inputTitles.length < MAX_TITLES
  }
}

module.exports = {PublishAudienceService, audiencePublishMethods}
