const GET = 'get'
const POST = 'post'

class ProxyService {
  /* @ngInject */
  constructor(proxyUrl, $http, $q) {
    this.$http = $http
    this.$q = $q
    this.proxyUrl = proxyUrl
    this.cancelers = {}
  }

  // create a key for our unique url+method pair.
  generateCancelKey(url, method) {
    const urlWithoutParams = url.split('?')[0]
    return `${method}|${urlWithoutParams}`
  }

  // For our url and method, cancel a previous request
  // if it's still pending
  cancelSameEndpointRequestIfNeeded(url, method) {
    const key = this.generateCancelKey(url, method)
    const canceler = this.cancelers[key]
    if (canceler) {
      canceler.resolve()
      this.cancelers[key] = false
    }
  }

  // Creates a key-value pair to map a unique url+method pair
  // to a canceler
  saveCanceler(url, method, canceler) {
    const key = this.generateCancelKey(url, method)
    this.cancelers[key] = canceler
  }

  // Maybe you're intentionally firing off a bunch of proxy requests
  // in quick succession in some loop
  getWithoutCancel(url) {
    return this.get(url, false)
  }

  get(url, useCancel = true) {
    if (useCancel) {
      this.cancelSameEndpointRequestIfNeeded(url, GET)
    }

    const params = {
      url,
      method: GET
    }

    const canceler = this.$q.defer()

    const request = {
      method: POST,
      url: this.proxyUrl,
      data: params,
      timeout: canceler.promise,
      cancel: canceler
    }

    if (useCancel) {
      this.saveCanceler(url, GET, canceler)
    }

    const onSuccess = (response) => {
      return response
    }

    const onError = (result) => {
      const wasCanceled = result.status === -1
      result.wasCanceled = wasCanceled
      return this.$q.reject(result)
    }

    return this.$http(request).then(
      onSuccess,
      onError
    )
  }

  // Maybe you're intentionally firing off a bunch of proxy requests
  // in quick succession in some loop
  postWithoutCancel(url, json) {
    return this.post(url, json, false)
  }

  post(url, json, useCancel = true) {
    if (useCancel) {
      this.cancelSameEndpointRequestIfNeeded(url, POST)
    }

    const params = {
      url,
      method: POST,
      json
    }

    const canceler = this.$q.defer()

    const request = {
      method: POST,
      url: this.proxyUrl,
      data: params,
      timeout: canceler.promise,
      cancel: canceler
    }

    if (useCancel) {
      this.saveCanceler(url, POST, canceler)
    }

    const onSuccess = (response) => {
      return response
    }

    const onError = (result) => {
      const wasCanceled = result.status === -1
      result.wasCanceled = wasCanceled
      return this.$q.reject(result)
    }

    return this.$http(request).then(
      onSuccess,
      onError
    )
  }
}

module.exports = {ProxyService}
