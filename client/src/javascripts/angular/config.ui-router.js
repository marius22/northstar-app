/* @ngInject */
function routing($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/404')

  $urlRouterProvider.when('', '/user/login')
  $urlRouterProvider.when('/segment/:segmentId/overview', '/segment/:segmentId/overview/graphs')

  $stateProvider.state('galaxy', {
    url: '/galaxy',
    templateUrl: '/galaxy.html'
  })

    .state('segment', {
      url: '/segment',
      abstract: true,
      template: '<div ui-view class="segment-view"></div>',
      data: {
        displayName: 'Segment'
      }
    })
    .state('user', {
      url: '/user',
      abstract: true,
      templateUrl: '/user/index.html',
      controller: 'LoginSignUpController as lsu'
    })
    .state('user.login', {
      url: '/login',
      templateUrl: '/user/login.html',
      reloadOnSearch: false,
      params: {
        refer: '',
        message: '',
      },
    })
    .state('user.sign-up', {
      url: '/sign-up',
      templateUrl: '/user/sign-up.html'
    })
    .state('user.pass-sent', {
      url: '/pass-sent',
      templateUrl: '/user/pass-sent.html'
    })
    .state('ads', {
      url: '/ads',
      controller: 'AdsController as ac',
      templateUrl: '/others/ads.html'
    })

    /* Segment Console */

    .state('segment.console', {
      url: '/console',
      templateUrl: '/segments-console/console.html',
      controller: 'SegmentListController as slc'
    })
    .state('segment.welcome', {
      url: '/welcome',
      templateUrl: '/user/welcome.html',
      controller: 'SegmentWelcomeController as swc'
    })

    /* Segment Creation Flow */

    .state('segment.create', {
      url: '/create',
      abstract: true,
      templateUrl: '/segment-creation/layout.html',
      controller: 'SegmentCreateController as scc',
      data: {
        displayName: 'Create'
      }
    })
    .state('segment.create.name', {
      url: '/upload',
      /* notice: share the same template with QualCriteriaController */
      templateUrl: '/segment-creation/qualification.criteria.html',
      controller: 'SegmentNameController as uc'
    })
    .state('segment.create.qualification', {
      url: '/qualification/:segmentId',
      /* notice: this template is share by SegmentNameCtroller which used it as a background */
      templateUrl: '/segment-creation/qualification.criteria.html',
      controller: 'QualCriteriaController as qcc'
    })
    .state('segment.create.upload', {
      url: '/upload/:segmentId',
      templateUrl: '/segment-creation/upload.html',
      controller: 'UploadController as uc',
    })
    .state('segment.create.csv', {
      url: '/upload/:segmentId/csv',
      templateUrl: '/segment-creation/upload.csv.html',
      controller: 'UploadCSVController as ucc',
    })
    .state('segment.create.sfdc', {
      url: '/upload/:segmentId/sfdc',
      templateUrl: '/segment-creation/upload.sfdc.html',
      controller: 'UploadSFDCController as usc'
    })
    .state('segment.create.insights', {
      url: '/insights/:segmentId',
      // we're reusing this template in two different places, which is why it's in
      // a different folder from the other templates in this flow
      templateUrl: '/predictive-segment-overview/seed-list-insights.html',
      controller: 'SeedInsightsController as sic'
    })
    .state('segment.create.reach', {
      url: '/reach/:segmentId',
      templateUrl: '/segment-creation/reach.html',
      controller: 'SegmentReachController as src'
    })
    .state('segment.create.summary', {
      url: '/summary/:segmentId',
      templateUrl: '/segment-creation/summary.html',
      controller: 'CreationSummaryController as csc'
    })
    .state('segment.create.success', {
      url: '/success',
      controller: 'SegmentSuccessController as ss',
      templateUrl: '/segment-creation/success.html'
    })

    /* Overview for a specific segment */

    .state('segment.overviewTabs', {
      url: '/:segmentId/overview',
      abstract: true,
      templateUrl: '/predictive-segment-overview/layout.html',
      controller: 'LayoutController as lc'
    })
    .state('segment.overviewTabs.overview', {
      url: '',
      template: '<div class="overview-cont" ui-view></div>',
      controller: 'SegmentOverviewController as soc'
    })
    .state('segment.overviewTabs.overview.segmentGraphs', {
      url: '/graphs',
      templateUrl: '/predictive-segment-overview/segment-graphs.html',
      controller: 'SegmentGraphsController as sgc'
    })
    .state('segment.overviewTabs.overview.companyList', {
      url: '/company-list',
      templateUrl: '/predictive-segment-overview/company-list.html',
      controller: 'CompanyListController as clc',
    })
    .state('segment.overviewTabs.galaxyView', {
      url: '/galaxy-view',
      templateUrl: '/predictive-segment-overview/galaxy-view.html'
    })
    .state('segment.overviewTabs.seedCompanies', {
      url: '/seed-companies',
      templateUrl: '/predictive-segment-overview/seed-list-insights.html',
      controller: 'SeedInsightsController as sic'
    })
    .state('segment.overviewTabs.seedCompanies.creationSummary', {
      url: '/creation-summary',
      templateUrl: '/segment-creation/summary.html',
      controller: 'CreationSummaryController as csc'
    })

    /* Super Admin Console */

    .state('admin', {
      url: '/admin',
      abstract: true,
      template: '<div ui-view class="admin-view"/>'
    })
    .state('admin.license', {
      url: '/accounts',
      templateUrl: '/admin-console/admin-console.html',
      controller: 'AdminConsoleController as acc'
    })

    /* others */
    .state('404', {
      url: '/404',
      templateUrl: '/404.html'
    })
    .state('timeout', {
      url: '/timeout',
      templateUrl: '/timeout.html'
    })
    .state('chrome-confirm', {
      url: '/chrome-confirm',
      templateUrl: '/chrome-confirm.html'
    })

    .state('crm', {
      url: '/crm',
      abstract: true,
      template: '<div ui-view class="integration-view"/>',
    })
    .state('crm.integration', {
      url: '/integration',
      templateUrl: '/crm/integration.html',
      controller: 'IntegrationController as ic',
      css: ['/stylesheets/audience/mapping-marketo.css',
            '/stylesheets/bs-body/bs-body.css']
    })
    .state('crm.configuration', {
      url: '/configuration',
      templateUrl: '/crm/configuration.html',
      controller: 'ConfigurationController as cc'
    })
    /* Onboarding Flow */
    // note: we're reusing a lot of segment creation templates/logic here
    .state('crm.welcome', {
      url: '/welcome',
      templateUrl: '/crm/onboard-welcome.html'
    })
    .state('crm.uploadChoice', {
      url: '/upload',
      templateUrl: '/segment-creation/upload.html',
      controller: 'UploadController as uc'
    })
    .state('crm.uploadCSV', {
      url: '/csv',
      templateUrl: '/segment-creation/upload.csv.html',
      controller: 'UploadCSVController as ucc'
    })
    .state('crm.uploadSFDC', {
      url: '/sfdc',
      templateUrl: '/segment-creation/upload.sfdc.html',
      controller: 'UploadSFDCController as usc'
    })

    /* V3 STATES */
    .state('audience', {
      url: '/audience',
      templateUrl: '/audience/audience-view.html',
      controller: 'AudienceTopController as atc'
    })
    .state('audience.dashboard', {
      url: '/:audienceId/dashboard',
      abstract: true,
      templateUrl: '/audience/audience-dashboard.html',
      controller: 'DashboardController as dc'
    })
    .state('audience.dashboard.model-insights', {
      url: '/model-insights',
      templateUrl: '/audience/audience-model-insights.html',
      controller: 'AudienceModelInsightsController as amic',
      css: {
        href: '/stylesheets/audience/audience-model-insights.css'
      }
    })
    .state('audience.dashboard.audience-insights', {
      url: '/audience-insights',
      templateUrl: '/audience/audience-insights.html',
      controller: 'AudienceInsightsController as aic'
    })
    // Audiences Tab
    .state('audience.console', {
      url: '/console',
      templateUrl: '/audience/audience-console.html',
      controller: 'AudienceListController as alc',
      css: {
        href: '/stylesheets/audience/audience-console.css',
      }
    })
    // Audience list companies
    .state('audience.company', {
      url: '/:audienceId/audience-company',
      templateUrl: '/audience/audience-company.html',
      controller: 'AudienceCompanyController as acc',
      css: [
        '/stylesheets/audience/audience-company.css',
        '/stylesheets/audience/publish-audience.css',
        '/stylesheets/bs-body/bs-body.css'
      ]
    })
    // Galaxy Tab
    .state('audience.galaxy', {
      url: '/galaxy',
      templateUrl: '/audience/crm-galaxy.html',
      controller: ''
    })
    // Find audience, for various cases
    .state('audience.find', {
      url: '/find',
      // templateUrl: '/audience/find-audience.html',
      template: '<div ui-view></div>',
      abstract: true,
      controller: 'AudienceCreateController as acc'
    })
    .state('audience.find.keywords', {
      url: '/keywords',
      templateUrl: '/audience/find-audience.html',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    .state('audience.find.urls', {
      url: '/urls',
      templateUrl: '/audience/find-audience.html',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    .state('audience.find.csv', {
      url: '/csv',
      templateUrl: '/audience/find-audience.html',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    .state('audience.find.criteria', {
      url: '/criteria',
      templateUrl: '/audience/find-audience.html',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    // Enrich audience
    // shares html/css/js with find audience
    .state('audience.enrich', {
      url: '/score',
      templateUrl: '/audience/find-audience.html',
      controller: 'AudienceCreateController as acc',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    // Dewalt loading
    .state('progress', {
      url: '/progress',
      controller: 'ProgressViewController as pvc',
      templateUrl: '/others/progress.html',
      css: {
        href: '/stylesheets/others/progress.css'
      }
    })
    // Exclusion list states. Note that they're all very similar to various
    // audience states, so going to reuse most of/all audience templates and controllers
    .state('exclusion', {
      url: '/exclusion',
      templateUrl: '/audience/audience-view.html',
      controller: 'AudienceTopController as atc'
    })
    .state('exclusion.console', {
      url: '/console',
      templateUrl: '/audience/audience-console.html',
      controller: 'AudienceListController as alc',
      css: {
        href: '/stylesheets/audience/audience-console.css',
      }
    })
    .state('exclusion.find', {
      url: '/find',
      template: '<div ui-view></div>',
      abstract: true,
      controller: 'AudienceCreateController as acc'
    })
    .state('exclusion.find.keywords', {
      url: '/keywords',
      templateUrl: '/audience/find-audience.html',
      controller: 'AudienceCreateController as acc',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    // Enrich is perhaps not the right term here, but it's still the most
    // conceptually similar to creating an Audience by enriching/scoring a CSV:
    // The user uploads a CSV, we take exactly the contents of the CSV and create
    // with those contents.
    .state('exclusion.enrich', {
      url: '/csv',
      templateUrl: '/audience/find-audience.html',
      controller: 'AudienceCreateController as acc',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    .state('exclusion.update', {
      url: '/:excListId/update',
      templateUrl: '/audience/find-audience.html',
      controller: 'AudienceCreateController as acc',
      css: {
        href: '/stylesheets/audience/find-audience.css'
      }
    })
    .state('exclusion.company', {
      url: '/:audienceId/exclusion-company',
      templateUrl: '/audience/audience-company.html',
      controller: 'AudienceCompanyController as acc',
      css: [
        '/stylesheets/audience/audience-company.css',
        '/stylesheets/audience/publish-audience.css',
        '/stylesheets/bs-body/bs-body.css'
      ]
    })
    // Settings root.
    .state('settings', {
      url: '/settings',
      templateUrl: '/settings/settings-base.html',
      controller: 'SettingsController as sc',
    })
    // Marketo settings
    .state('settings.mkto', {
      url: '/marketo',
      templateUrl: '/settings/publish-settings-layout.html',
      controller: 'PublishSettingsController as psc',
      redirectTo: 'settings.mkto.fields'
    })
    .state('settings.mkto.fields', {
      url: '/field-mapping',
      templateUrl: '/settings/marketo-settings.html',
      controller: 'FieldMappingController as fmc',
      css: ['/stylesheets/bs-body/bs-body.css'],
    })
    .state('settings.csv', {
      url: '/csv',
      templateUrl: '/settings/publish-settings-layout.html',
      controller: 'PublishSettingsController as psc',
      redirectTo: 'settings.csv.fields'
    })
    .state('settings.csv.fields', {
      url: '/field-select',
      templateUrl: '/settings/csv-field-selection.html',
      controller: 'CSVFieldController as cfc'
    })
    // Salesforce
    .state('settings.sfdc', {
      url: '/sfdc',
      templateUrl: '/settings/publish-settings-layout.html',
      controller: 'PublishSettingsController as psc',
      redirectTo: 'settings.sfdc.fields.account'
    })
    .state('settings.sfdc.fields', {
      url: '/field-mapping',
      templateUrl: '/settings/sfdc-settings.html',
      controller: 'FieldMappingController as fmc',
      redirectTo: 'settings.sfdc.fields.account',
    })
    .state('settings.sfdc.fields.account', {
      url: '/account',
      templateUrl: '/settings/sfdc-field-mapping.html',
      params: {
        targetObject: 'account',
      },
      css: ['/stylesheets/bs-body/bs-body.css'],
    })
    .state('settings.sfdc.fields.lead', {
      url: '/lead',
      templateUrl: '/settings/sfdc-field-mapping.html',
      params: {
        targetObject: 'lead',
      },
      css: ['/stylesheets/bs-body/bs-body.css'],
    })
    .state('settings.sfdc.fields.contact', {
      url: '/contact',
      templateUrl: '/settings/sfdc-field-mapping.html',
      params: {
        targetObject: 'contact',
      },
      css: ['/stylesheets/bs-body/bs-body.css'],
    })
    .state('settings.sfdc.rts', {
      url: '/real-time-scoring',
      templateUrl: '/settings/real-time-settings.html',
      controller: 'RealTimeScoringController as rtsc'
    })
    // User Settings.
    .state('settings.users', {
      url: '/users',
      templateUrl: '/admin-console/user-management.html',
      controller: 'UserManagementController as umc'
    })
    .state('settings.api', {
      url: '/api',
      templateUrl: '/settings/publish-settings-layout.html',
      controller: 'PublishSettingsController as psc',
      redirectTo: 'settings.api.instructions'
    })
    .state('settings.api.instructions', {
      url: '/instructions',
      templateUrl: '/settings/api-instructions.html',
      controller: 'RestApiController as rac'
    })
    .state('settings.api.modelRef', {
      url: '/models',
      templateUrl: '/settings/model-reference.html',
      controller: 'ModelReferenceController as mrc'
    })

}

export default routing
