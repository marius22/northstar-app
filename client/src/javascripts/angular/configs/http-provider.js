/* @ngInject */
function configHttpProvider($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor')
}

export default configHttpProvider
