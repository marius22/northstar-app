/* eslint-disable */
// Need to import this once to init Rollbar. We import this as early as possible to catch as
// man yerrors as possible with Rollbar.
import {Rollbar} from 'rollbar'

// import none
// this angular is for no-eslint-controlled directive
import 'angular'
/* eslint-enable */
import '@iamadamjowett/angular-click-outside/clickoutside.directive.js'
import 'angularjs-datetime-picker'
import 'angular/services/angular-modal-service'
import 'angular-loading-bar'
import 'angulartics'
import 'angular-xeditable'
import 'angular-css'
import 'rx-angular'
// import all members
import * as config from 'config'
import * as controllers from './controllers'
import * as directives from './directives'
import * as services from './services'
import * as tableHelpers from './tables'
// import single member
import AuthInterceptor from 'angular/interceptors/auth-interceptor.js'
import angular from 'angular'
import angularticsGoogleAnalytics from 'angulartics-google-analytics'
import configHttpProvider from 'angular/configs/http-provider.js'
import ngAnimate from 'angular-animate'
import ngDropdowns from './dropdowns/dropdowns.js'
import ngFileUpload from 'ng-file-upload'
import ngFx from 'ng-fx'
import ngResource from 'angular-resource'
import ngSanitize from 'angular-sanitize'
import register from './register'
import routing from './config.ui-router'
import uiRouter from 'angular-ui-router'
import uiSelect from 'ui-select'
import uiValidate from 'angular-ui-validate'
// import filters
import capitalize from './filters/capitalize'
import formatPhone from './filters/format-phone'
import handleLink from './filters/handle-link'
import strToDate from './filters/str-to-date'


// ngReact dependencies
import React from 'react'
import 'ngreact'

const nsApp = angular.module('northstar', [
  tableHelpers.SmartTable,
  uiRouter,
  ngResource,
  ngFileUpload,
  ngSanitize,
  // Injecting this loading bar module automatically shows a browser progress bar
  // underneath the URL bar for any http request made.
  // 'angular-loading-bar',
  'angularModalService',
  'angular-click-outside',
  'angularjs-datetime-picker',
  'angulartics',
  angularticsGoogleAnalytics,
  ngDropdowns,
  ngAnimate,
  ngFx,
  'ngTextTruncate',
  'xeditable',
  uiSelect,
  'ui.validate',
  'easypiechart',
  // Imported from 'angular-css'
  'angularCSS',
  // RxJS angular integration http://bit.ly/2bQPJ8T
  'rx',
  // ngReact: https://github.com/ngReact/ngReact
  'react'
])

// services
nsApp.constant('apiBaseUrl', config.apiBaseURL)
nsApp.constant('proxyUrl', `${config.apiBaseURL}proxy`)
nsApp.service('authInterceptor', AuthInterceptor)
nsApp.service('tableService', tableHelpers.TableService)
for (const key in services) {
  nsApp.service(key[0].toLowerCase() + key.slice(1), services[key])
}

// configs
nsApp.config(configHttpProvider)

// filters
nsApp.filter('capitalize', capitalize)
nsApp.filter('formatPhone', formatPhone)
nsApp.filter('handleLink', handleLink)
nsApp.filter('strToDate', strToDate)

// controllers
for (const key in controllers) {
  nsApp.controller(key, controllers[key])
}

// When trying to register a component in its own file, i.e. with
// angular.module('northstar').value('Component', Component), it fails
// because this code is somehow running *before* the code that initializes
// the angular module. So we'll just register the components here.
import AudienceListCrudText from 'components/AudienceListCrudText'
nsApp.value('AudienceListCrudText', AudienceListCrudText)
import Select from 'components/Select'
nsApp.value('Select', Select)
import MappingRow from 'components/MappingRow'
nsApp.value('MappingRow', MappingRow)
import Mapping from 'components/Mapping'
nsApp.value('Mapping', Mapping)

/* Directives */
/* We need to use something like register.js so our dependency injection works properly with ES6-style directives.
 http://stackoverflow.com/questions/28620479/using-es6-classes-as-angular-1-x-directives */
register('northstar').directive('pagination', directives.Pagination)
register('northstar').directive('multiSelect', directives.MultiSelect)
register('northstar').directive('barChart', directives.BarChart)
register('northstar').directive('usaMap', directives.UsaMap)
register('northstar').directive('donutChart', directives.DonutChart)
register('northstar').directive('columnChart', directives.ColumnChart)
register('northstar').directive('loadingSpinner', directives.LoadingSpinner)
register('northstar').directive('titlePane', directives.TitlePane)
register('northstar').directive('gSwitch', directives.GSwitch)
register('northstar').directive('numberPicker', directives.NumberPicker)
register('northstar').directive('keywords', directives.Keywords)
register('northstar').directive('csvUploader', directives.CSVUploader)
register('northstar').directive('lazyCards', directives.LazyCards)
register('northstar').directive('audienceCompanyInfo', directives.AudienceCompanyInfo)
register('northstar').directive('wordCloud', directives.WordCloud)
register('northstar').directive('summaryBox', directives.SummaryBox)


// Exceptions thrown in a `resolve` block in UI router are silent
// by default. We need this to see what went wrong while developing.
if (config.notProduction()) {
  nsApp.run(['$rootScope', ($rootScope) => {
    $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
      throw new Error(JSON.stringify(error))
    })
  }])
}

// Need to wrap this in some kind of block or nsApp.run() causes an error
if (true) {
  nsApp.run(['$rootScope', '$state', ($rootScope, $state) => {
    $rootScope.$on('$stateChangeStart', (evt, toState, toParams) => {
      if (toState.redirectTo) {
        evt.preventDefault()
        $state.go(toState.redirectTo, toParams)
      }
    })
  }])
}


/* Google Analytics*/
/* eslint-disable */
(function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r;
  i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
  a = s.createElement(o),
    m = s.getElementsByTagName(o)[0];
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', config.googleAnalyticsKey, 'auto');
/* eslint-enable */

nsApp.config(routing)
