/**
 * filter: capitalize, just support english strings
 * usage: {{string | capitalize}}
 * transform "abc" to "Abc"
 */
import _ from 'lodash'
module.exports = () => {
  return (input) => {
    if (_.isString(input)) {
      return !!input ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : ''
    } else {
      return input
    }
  }
}
