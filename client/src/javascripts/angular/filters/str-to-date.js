/**
 * UTC time str to timestamp
 * default: return 0
 */
module.exports = () => {
  return (utcStr) => {
    return utcStr ? +new Date(utcStr) : 0
  }
}
