import libphonenumber from 'google-libphonenumber'

const PNF = libphonenumber.PhoneNumberFormat
const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance()

// format phone number.
// 5101234567 -> +1 510-123-4567
module.exports = () => {
  return (phoneNum) => {
    if (typeof phoneNum === 'number') {
      phoneNum = phoneNum.toString()
    }

    try {
      // If phoneNum begins with a '+', then this lib is capable
      // of respecting the country code and formatting it accordingly.
      // Otherwise, the code will assume a US phone number.
      // The difference is illustrated here, with a Japanese phone number:
      // +81352928000 -> +81 3-5292-8000 (Correct!)
      // 81352928000 -> +1 81352928000 (Incorrect! It assumes it's a US number and prepends a '+1'.)
      const parsedNum = phoneUtil.parse(phoneNum, 'US')
      const formattedNum = phoneUtil.format(parsedNum, PNF.INTERNATIONAL)
      return formattedNum
    } catch (e) {
      // phoneNum is something weird that libphonenumber can't handle, so just return the original input
      return phoneNum
    }
  }
}