// detect urls in strings and make them clickable links
module.exports = () => {
  const linkUrlPattern = /(^\S[\-\w\.']+\.(?:com|cc|net|co|org|biz|info|io|us)[\/\w\-]*[^,\s]*$)/
  return (text, target) => {
    return text.replace(linkUrlPattern, `<a target="${target}" href="http://${text}">${text}</a>`)
  }
}