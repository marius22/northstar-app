/* eslint-disable */
/**
 * @license MIT http://jseppi.mit-license.org/license.html
 * https://github.com/jseppi/angular-dropdowns/blob/master/README.md
 */
(function (window, angular) {
  'use strict'

  var dd = angular.module('ngDropdowns', [])

  dd.run(['$templateCache', function ($templateCache) {
    $templateCache.put('ngDropdowns/templates/dropdownSelect.html',
      `<div ng-class="{\'disabled\': dropdownDisabled,\'invalid\':dropdownInvalid}" class="wrap-dd-select" tabindex="0">
        <span class="selected">{{dropdownModel[labelField]}}
          <i class="btr bt-angle-down"></i>
          <i class="btr bt-angle-up"></i>
        </span>
        <ul class="dropdown">
          <li ng-repeat="item in dropdownSelect"
           class="dropdown-item"
           dropdown-select-item="item"
           dropdown-item-label="labelField"
           dropdown-model-key="dropdownModel.key || dropdownModel[labelField]">
          </li>
        </ul>
      </div>`
    )

    $templateCache.put('ngDropdowns/templates/dropdownSelectItem.html',
      `<li ng-class="{divider: (dropdownSelectItem.divider && !dropdownSelectItem[dropdownItemLabel]),
      'divider-label': (dropdownSelectItem.divider && dropdownSelectItem[dropdownItemLabel])}">
        <a href="" class="dropdown-item"
         ng-if="!dropdownSelectItem.divider"
         ng-href="{{dropdownSelectItem.href}}"
         ng-class="{'currently-selected': currentlySelected()}"
         ng-click="selectItem()">
          {{dropdownSelectItem[dropdownItemLabel]}}
        </a>
        <span ng-if="dropdownSelectItem.divider">
          {{dropdownSelectItem[dropdownItemLabel]}}
        </span>
      </li>`
    )

  }])

  dd.directive('dropdownSelect', ['DropdownService',
    function (DropdownService) {
      return {
        restrict: 'A',
        replace: true,
        scope: {
          dropdownSelect: '=',
          dropdownModel: '=',
          dropdownItemLabel: '@',
          dropdownOnchange: '&',
          dropdownDisabled: '=',
          dropdownInvalid: '='
        },

        controller: ['$scope', '$element', function ($scope, $element) {
          $scope.labelField = $scope.dropdownItemLabel || 'text'

          DropdownService.register($element)

          this.select = function (selected) {
            if (!angular.equals(selected, $scope.dropdownModel)) {
              $scope.dropdownModel = selected
            }
            $scope.dropdownOnchange({
              selected: selected
            })
            $element[0].blur() // trigger blur to clear active
          }

          $element.bind('click', function (event) {
            event.stopPropagation()
            if (!$scope.dropdownDisabled) {
              DropdownService.toggleActive($element)
            }
          })

          $scope.$on('$destroy', function () {
            DropdownService.unregister($element)
          })
        }],
        templateUrl: 'ngDropdowns/templates/dropdownSelect.html'
      }
    }
  ])

  dd.directive('dropdownSelectItem', [
    function () {
      return {
        require: '^dropdownSelect',
        replace: true,
        scope: {
          dropdownItemLabel: '=',
          dropdownModelKey: '=',
          dropdownSelectItem: '=',
        },

        link: function (scope, element, attrs, dropdownSelectCtrl) {
          scope.selectItem = function () {
            if (scope.dropdownSelectItem.href) {
              return
            }
            dropdownSelectCtrl.select(scope.dropdownSelectItem)
          }

          scope.currentlySelected = function () {
            if (!scope.dropdownModelKey) {
              return false
            }
            const keyMatch = scope.dropdownModelKey === scope.dropdownSelectItem.key
            const labelMatch = scope.dropdownModelKey === scope.dropdownSelectItem[scope.dropdownItemLabel]
            return keyMatch || labelMatch
          }
        },

        templateUrl: 'ngDropdowns/templates/dropdownSelectItem.html'
      }
    }
  ])

  dd.factory('DropdownService', ['$document',
    function ($document) {
      var body = $document.find('body'),
        service = {},
        _dropdowns = []

      body.bind('click', function () {
        angular.forEach(_dropdowns, function (el) {
          el.removeClass('active')
        })
      })

      service.register = function (ddEl) {
        _dropdowns.push(ddEl)
      }

      service.unregister = function (ddEl) {
        var index
        index = _dropdowns.indexOf(ddEl)
        if (index > -1) {
          _dropdowns.splice(index, 1)
        }
      }

      service.toggleActive = function (ddEl) {
        angular.forEach(_dropdowns, function (el) {
          if (el !== ddEl) {
            el.removeClass('active')
          }
        })

        ddEl.toggleClass('active')
      }

      service.clearActive = function () {
        angular.forEach(_dropdowns, function (el) {
          el.removeClass('active')
        })
      }

      service.isActive = function (ddEl) {
        return ddEl.hasClass('active')
      }

      return service
    }
  ])
})(window, window.angular)

module.exports = 'ngDropdowns'
