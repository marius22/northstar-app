import _ from 'lodash'
import * as intercom from 'intercom'

class UserManagementController {
  /* @ngInject */
  constructor(apiBaseUrl,
              $http,
              userService,
              tableService,
              showModalService,
              alertService,
              audienceService,
              $scope) {
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.userService = userService
    this.tableService = tableService
    this.showModalService = showModalService
    this.alertService = alertService
    this.audienceService = audienceService

    // for convenience.
    this.roleNameMap = {
      'ACCOUNT_ADMIN': 'Account Admin',
      'ACCOUNT_USER': 'Account User',
      'CHROME_USER': 'Chrome User'
    }

    // Role options in the edit dropdown menu of the "User Role" column.
    // An account admin can grant any of these roles to an account user.
    this.roles = [
      {
        text: 'Account Admin',
        key: 'ACCOUNT_ADMIN'
      },
      {
        text: 'Account User',
        key: 'ACCOUNT_USER'
      },
    ]

    // Status options in the edit dropdown menu of the "Status" column.
    this.statuses = [
      {
        text: 'ACTIVE',
        key: 'ACTIVE'
      },
      {
        text: 'INACTIVE',
        key: 'INACTIVE'
      }
    ]

    this.initializeTable()
    this.initInviteUserRoles()
    this.getAudienceCount()
  }

  getAudienceCount() {
    this.audienceService.getAudiences().then(
      (response) => {
        this.currentAudienceCount = response.data.data.length
      }
    )
  }

  initInviteUserRoles() {
    const rolesUrl = this.apiBaseUrl + 'roles'
    this.$http.get(rolesUrl).then(
      (response) => {
        const roles = response.data.data
        // Just Account User or Chrome User
        this.inviteUserRoles = _(roles)
          .filter(roleObj => {
            return roleObj.name === 'ACCOUNT_USER'
          })
          .map(roleObj => {
            return {
              key: roleObj.name,
              text: this.roleNameMap[roleObj.name],
              id: roleObj.id
            }
          })
          .value()
      }
    )
  }

  /* Table Logic */
  initializeTable() {
    const tableParams = {
      subject: 'Users',
      header: [
        {
          text: 'Last Name',
          key: 'lastName'
        },
        {
          text: 'First Name',
          key: 'firstName'
        },
        {
          text: 'Email',
          key: 'email'
        },
        {
          text: 'Phone',
          key: 'phone'
        },
        {
          text: 'User Role',
          key: 'role'
        },
        {
          text: 'Status',
          key: 'status'
        }
      ],
      defaultQueryParams: {includePending: true}
    }

    this.userTable = this.tableService.createTable(tableParams)
    const crudConfig = this.userTable.getResourceConfig()
    const accountId = this.userService.accountId()
    crudConfig.url = `${this.apiBaseUrl}accounts/${accountId}/users`
    this.userTable.configureCrudResource(crudConfig)
  }

  inviteUser() {
    const params = {
      onConfirm: (result, close) => {
        if (!_.isEmpty(result.email)) {
          this.$http.post(`${this.apiBaseUrl}users/invite`, {
            accountId: this.userService.accountId(),
            email: result.email,
            roleId: result.role.id
          }).then(() => {
            close()
            this.alertService.addAlert('success', 'Invite successful')
            this.userTable.reload({limit: 10, includePending: true})
          }, (response) => {
            this.alertService.addAlert('error', `${response.data.message}`, '#modal-error')
          })
        } else {
          this.alertService.addAlert('error', 'Invalid Email', '#modal-error')
        }
      },
      dataViewItems: this.inviteUserRoles
    }

    intercom.Intercom.track(intercom.CONSTANTS.INVITE_USER)
    this.showModalService.showInviteUserModal(params)
  }

  canEditUsers() {
    return this.userService.isAcctAdmin() || this.userService.isInternalAdmin()
  }

  isTempUser(userInfo) {
    return _.has(userInfo, 'invitationId')
  }

  saveUser(editData, userInfo) {
    let url
    let params = {}
    if (this.isTempUser(userInfo)) {
      url = `${this.apiBaseUrl}invitations/${userInfo.invitationId}`
      params['invitation'] = {
        firstName: editData.firstName,
        lastName: editData.lastName,
        email: editData.email,
        phone: editData.phone,
        status: 'PENDING'
      }
    } else {
      url = `${this.apiBaseUrl}users/${userInfo.id}`
      params['user'] = {
        firstName: editData.firstName,
        lastName: editData.lastName,
        email: editData.email,
        phone: editData.phone,
        roleNames: [editData.role],
        status: editData.status
      }
    }

    return this.$http.put(url, params).then(
      (response) => {
        this.alertService.addAlert('success', 'User info successfully updated.')
        // angular-xeditable will update the local model if saveUser() returns true.
        return true
      },
      (response) => {
        this.alertService.addAlert('error',
          'Sorry, something went wrong with updating this user. Please try again or contact support.')
        // angular-xeditable will not update the local model if saveUser() returns a string.
        return 'error'
      }
    )
  }
}


export default UserManagementController
