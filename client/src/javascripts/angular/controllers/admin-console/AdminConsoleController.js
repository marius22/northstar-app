import _ from 'lodash'
import angular from 'angular'

class AdminConsoleController {
  /* @ngInject */
  constructor(apiBaseUrl,
              tableService,
              $http,
              alertService,
              ghostService,
              $state,
              ModalService,
              showModalService,
              userService,
              $window,
              $scope) {
    this.apiBaseUrl = apiBaseUrl
    this.tableService = tableService
    this.$http = $http
    this.$state = $state
    this.$window = $window
    this.$scope = $scope
    this.alertService = alertService
    this.ghostService = ghostService
    this.modalService = ModalService
    this.sms = showModalService

    this.pendingAccount = {}
    this.enabled = userService.isInternalAdmin()
    if (this.enabled) {
      this.initializeAcctTable()
    }

    this.filterQuery = ''
  }

  /* Create new user logic, reused from LoginSignUpController */

  openCreateAccountModal() {
    const createAccount = (result, close) => {
      const params = {
        name: result.name,
        domain: result.domain,
        isPrimary: result.isPrimary
      }

      this.$http.post(`${this.apiBaseUrl}accounts`, params).success((res) => {
        this.alertService.addAlert('success', `Account ${result.name} created successfully.`)
        this.accountTable.reload({})
        close()
      }).error((response) => {
        this.alertService.addAlert('error', `${response.message}`, '#modal-error')
      })
    }

    const modalParams = {
      onConfirm: createAccount
    }

    this.sms.createAccount(modalParams)
  }

  /* Table Logic */
  initializeAcctTable() {
    const tableParams = {
      subject: 'Accounts',
      header: [
        {
          text: 'Account Name',
          key: 'name'
        },
        {
          text: 'Audience Quota'
        },
        {
          text: 'Company Quota'
        },
        {
          text: 'Contacts Quota'
        },
        {
          text: 'Domain'
        },
        {
          text: 'Primary Account'
        }
      ]
    }

    this.accountTable = this.tableService.createTable(tableParams)
  }

  /* Focus Slide/Title Pane logic */
  openSlide(accountInfo) {
    this.currentIndex = this.accountTable.data.indexOf(accountInfo)
    this.accountInfo = {}
    angular.copy(accountInfo, this.accountInfo)

    this.modalService.showModal({
      controller: 'AdminSlideController',
      controllerAs: 'acc',
      inputs: {
        accountInfo: this.accountInfo,
        accountTable: this.accountTable,
        currentIndex: this.currentIndex
      },
      templateUrl: '/templates/user-slide.html'
    })
  }
}


export default AdminConsoleController
