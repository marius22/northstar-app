import * as config from 'config'

import {Intercom} from 'intercom'
import SlideController from '../modals/SlideController'
import _ from 'lodash'
import { insightDisplayName } from 'utils'

class AdminSlideController extends SlideController {
  /* @ngInject */
  constructor(close, accountInfo, accountTable, currentIndex,
              apiBaseUrl, $http, $document, $timeout, alertService,
              userService, ModalService, showModalService, tableService, ghostService,
              $scope, $q) {
    super(close, $document, $timeout)
    this.accountInfo = accountInfo
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.$timeout = $timeout
    this.alertService = alertService
    this.userService = userService
    this.modalService = ModalService
    this.tableService = tableService
    this.ghostService = ghostService
    this.sms = showModalService
    this.$scope = $scope
    this.$q = $q
    this.accountTable = accountTable
    this.currentIndex = currentIndex
    this.roleDropdownOptions = []

    this.openSlide()
  }

  init() {
    this.initializeUserTable()
    this.initializeTentativeData()
    this.initializeDropdowns()
    this.getPendingEmails()
  }

  initializeUserTable() {
    const tableParams = {
      subject: 'Users',
      header: [
        {
          text: 'Last Name',
          key: 'lastName'
        },
        {
          text: 'First Name',
          key: 'firstName'
        },
        {
          text: 'Email',
          key: 'email'
        },
        {
          text: 'Phone',
          key: 'phone'
        },
        {
          text: 'User Role',
          key: 'role'
        },
        {
          text: 'Status',
          key: 'status'
        },
        {
          text: 'Can Export Contacts',
          key: 'contactExportFlag'
        }
      ],
      defaultQueryParams: {id: this.accountInfo.id}
    }

    this.userTable = this.tableService.createTable(tableParams)
    const crudConfig = this.userTable.getResourceConfig()
    crudConfig.url = `${this.apiBaseUrl}accounts/:id/users`
    this.userTable.configureCrudResource(crudConfig)
  }

  /* Ghost Logic */

  ghostAsAcct(userInfo) {
    this.ghostService.setGhostAcct(this.accountInfo.id, this.accountInfo.name, userInfo.id)
    this.$scope.$emit('ghosting')
  }

  ghostable(userInfo) {
    const isSuperAdmin = _.includes(userInfo.roles, 'ES_SUPER_ADMIN')
    const isGhostAdmin = _.includes(userInfo.roles, 'ES_ADMIN')
    return !(isSuperAdmin || isGhostAdmin) && userInfo.activated
  }

  /**
   * Both ghost admins and super admins can edit account-specific
   * information (quotas, exposed insights, etc.)
   *
   * No one can edit quotas.
   */
  canEditFeature(featureName) {
    const isSuper = this.userService.isSuper()
    const isGhost = this.userService.isGstAdmin()
    const isSomeKindOfAdmin = isSuper || isGhost

    // Only admins can change user features
    if (!(isSomeKindOfAdmin)) {
      return false
    }
    // Super admins can change any user features
    if (isSuper) {
      return true
    }

    // These features can't be changed by ghost admins
    const readOnlyFeatures = [
      'accountName',
      'quota',
      'publishExpirationDate',
      'numPredictiveSegments',
      'ads',
    ]
    if (_.includes(readOnlyFeatures, featureName)) {
      return false
    }

    return true
  }

  canEditAccounts() {
    return this.userService.isSuper()
  }

  canEditUser(userInfo) {
    // super admin can edit any user
    if (this.userService.isSuper()) {
      return true
    }
    // ghost admin can edit any user that isn't a super admin
    if (this.userService.isGstAdmin()) {
      return !_.includes(userInfo.roles, 'ES_SUPER_ADMIN')
    }
    return false
  }

  /* API Call Logic */

  /*
    Query for an account's exposed UI and CSV insights,
    and configure the UI and CSV multi-selects' Selected items
    appropriately
  */
  getQuotasForAccount() {
    this.quotas = _.cloneDeep(this.accountInfo.quotas)
    this.quotas.expiredTs = new Date(this.quotas.expiredTs)
    this.quotas.startTs = new Date(this.quotas.startTs)

    // set inbound leads targets info
    this.inboundLeadsTargets = this.userService.getInboundLeadsTargets(this.quotas.inboundLeadsTarget)

    // set ui insight as exposed if quotas.exposedUIInsights contains it.
    for (const insight of this.quotas.exposedUIInsights) {
      for (const cfgInsight of this.insights.ui) {
        if (insight === cfgInsight.key) {
          cfgInsight.selected = true
        }
      }
    }

    // set a publish insight as exposed if quotas.exposedCSVInsights contains it.
    for (const insight of this.quotas.exposedCSVInsights) {
      for (const cfgInsight of this.insights.csv) {
        if (insight === cfgInsight.key) {
          cfgInsight.selected = true
        }
      }
    }
  }

  /*
    Use changes in local quotas model to generate PUT params, and
    make API call to update a given account with those params.
    Updating other account info also happens here.
  */
  updateAccountQuotas(accountId) {
    const url = this.apiBaseUrl + `accounts/${accountId}`
    const quotas = {}
    if (!this.quotas) {
      return
    }
    quotas['segmentCnt'] = this.quotas.segmentCnt
    quotas['UICompanyLimit'] = this.quotas.UICompanyLimit
    quotas['CSVCompanyLimit'] = this.quotas.CSVCompanyLimit
    quotas['startTs'] = this.quotas.startTs
    quotas['expiredTs'] = this.quotas.expiredTs
    quotas['enableAds'] = this.quotas.enableAds
    quotas['contactLimit'] = this.quotas.contactLimit
    quotas['exposedUIInsights'] = []
    quotas['exposedCSVInsights'] = []
    quotas['enableInCrm'] = this.quotas.enableInCrm
    quotas['enableNotInCrm'] = this.quotas.enableNotInCrm
    quotas['enableFindCompany'] = this.quotas.enableFindCompany
    quotas['enableScoreList'] = this.quotas.enableScoreList
    quotas['enableModelInsights'] = this.quotas.enableModelInsights
    quotas['realtimeScoring'] = this.quotas.realtimeScoring
    quotas['enableApis'] = this.quotas.enableApis
    quotas['inboundLeadsTarget'] = _.keys(_.pickBy(this.inboundLeadsTargets))

    for (const uiInsight of this.insights.ui) {
      if (uiInsight.selected) {
        const dbName = uiInsight.key
        quotas['exposedUIInsights'].push(dbName)
      }
    }

    for (const csvInsight of this.insights.csv) {
      if (csvInsight.selected) {
        const dbName = csvInsight.key
        quotas['exposedCSVInsights'].push(dbName)
      }
    }

    if (!this.accountInfo.name) {
      return this.$q.reject('Account name is a required field.')
    }
    if (!this.accountInfo.domain) {
      return this.$q.reject('Account domain is a required field.')
    }

    const account = {
      name: this.accountInfo.name,
      domain: this.accountInfo.domain
    }

    const params = {quotas, account}

    this.saving = true

    return this.$http.put(url, params).then(
      (response) => {
        this.alertService.addAlert('success', 'Account changes successful!', '#topAlert')
        this.accountTable.reload({})
      },
      (res) => {
        if (res.data.message) {
          return this.$q.reject(res.data.message)
        }

        return this.$q.reject("Error saving account changes. Make sure you don't have any blank fields.")
      }
    )
  }

  /* ng-model initialization logic */
  initializeTentativeData() {
    const quotaFieldsUrl = this.apiBaseUrl + 'accounts/quota_fields'

    this.insights = {
      ui: [],
      csv: []
    }

    this.inboundLeadsTargets = {
      accounts: false,
      leads: false,
      contacts: false
    }

    this.rtsTypeOptions = [
      {
        dispName: 'Never',
        value: '',
        tooltip: 'We will never automatically publish any data to Salesforce.'
      },
      {
        dispName: 'Globally',
        value: 'global',
        tooltip: 'We will automatically publish data to Salesforce for all inbound prospects.'
      },
      {
        dispName: 'Per audience',
        value: 'audience',
        tooltip: `We will automatically publish data to Salesforce for all inbound prospects
                  that exist in an Audience which has Real Time Scoring enabled.`
      }
    ]

    return this.$http.get(quotaFieldsUrl).then(
      (response) => {
        const fieldNames = response.data.data
        for (const fieldName of fieldNames) {
          const uiFieldObj = {
            text: insightDisplayName(fieldName),
            key: fieldName,
            selected: false
          }
          this.insights.ui.push(uiFieldObj)

          const csvFieldObj = {
            text: insightDisplayName(fieldName),
            key: fieldName,
            selected: false
          }
          this.insights.csv.push(csvFieldObj)
        }
        this.getQuotasForAccount()
      }
    )
  }

  /* Dropdown Logic */

  /*
    We have 2 dropdowns: One for user activation and one for user roles.
    We define the options for each dropdown here.
  */
  initializeDropdowns() {
    // activation dropdown stuff
    this.activationDropdownOptions = [
      {
        text: 'Inactive',
        key: 'INACTIVE'
      },
      {
        text: 'Active',
        key: 'ACTIVE'
      }
    ]

    this.contactExportDropdownOptions = [
      {
        text: 'True',
        key: true
      },
      {
        text: 'False',
        key: false
      }
    ]

    this.selectedActivationOption = {}

    // role dropdown stuff
    this.roleNameMap = {
      'TRIAL_USER': 'Trial User',
      'CHROME_USER': 'Chrome User',
      'ACCOUNT_USER': 'Account User',
      'ACCOUNT_ADMIN': 'Account Admin',
      'ES_ADMIN': 'Ghost Admin',
      'ES_SUPER_ADMIN': 'Super Admin'
    }

    const rolesUrl = this.apiBaseUrl + 'all_role_names'
    this.roleDropdownOptions = []

    this.dropdownSelectedRole = {}

    this.$http.get(rolesUrl).then(
      (response) => {
        const roles = response.data.data
        for (const roleObj of roles) {
          const dropdownOption = {
            text: this.roleNameMap[roleObj.name],
            key: roleObj.name,
            id: roleObj.id
          }
          this.roleDropdownOptions.push(dropdownOption)
        }
        this.roleDropdownOptions = _.filter(this.roleDropdownOptions, (option) => {
          return !_.includes(['ES_ADMIN', 'ES_SUPER_ADMIN'], option.key) || this.canEditAccounts()
        })
      }
    )
  }

  /* User Invite Logic */
  inviteUser() {
    // Just Account User or Chrome User
    const inviteUserRoles = _.filter(this.roleDropdownOptions, (option) => {
      return option.key === 'ACCOUNT_USER' || option.key === 'CHROME_USER'
    })
    const params = {
      onConfirm: (result, close) => {
        this.$http.post(`${this.apiBaseUrl}users/invite`, {
          accountId: this.accountInfo.id,
          email: result.email,
          roleId: result.role.id
        }).then(() => {
          close()
          this.getPendingEmails()
          this.alertService.addAlert('success', 'Invite successful', '#slide-alert')
        }, (response) => {
          this.alertService.addAlert('error', `${response.data.message}`, '#modal-error')
        })
      },
      dataViewItems: inviteUserRoles
    }
    this.sms.showInviteUserModal(params)
  }

  getPendingEmails() {
    const url = `${this.apiBaseUrl}accounts/${this.accountInfo.id}/users?includePending=true`
    this.$http.get(url).then(
      (response) => {
        const users = response.data.data
        this.pendingUsers = _.filter(users, (userObj) => {
          return userObj.status === 'PENDING'
        })
      }
    )
  }

  /* Create new user logic, reused from LoginSignUpController.
     Essentially the same as invite, but skips the step where
     the invitee must click a link in the invite email. */

  openCreateUserModal() {
    const createUser = (result, close) => {
      const message = this.checkUserAndPass(result.email, result.password, result.confirmPassword)
      if (message !== '') {
        this.alertService.addAlert('error', message, '#modal-error')
        return
      }

      const params = {
        email: result.email,
        password: result.password,
        sendEmail: false,
        accountId: this.accountInfo.id
      }

      this.$http.post(`${this.apiBaseUrl}users/register`, params).success((res) => {
        this.alertService.addAlert('success', `User ${result.email} created successfully.`, 'fixed')
        this.userTable.reload({id: this.accountInfo.id})
        close()
      }).error((response) => {
        this.alertService.addAlert('error', `${response.message}`, '#modal-error')
      })
    }

    const modalParams = {
      onConfirm: createUser,
      requiredDomain: this.accountInfo.domain
    }

    this.sms.createUser(modalParams)
  }

  checkUserAndPass(email, pass, confirmPass) {
    // check User
    const message = this.checkUser(email)
    if (message !== '') {
      return message
    }

    // check password
    if (pass === '') {
      return 'Password should not be empty.'
    }

    if (pass !== confirmPass) {
      return 'Passwords do not match.'
    }

    return ''
  }

  checkUser(email) {
    if (!this.isValidEmail(email)) {
      return 'Please use a valid email.'
    }
    if (!this.isValidEmailDomain(email)) {
      return "Email domain must match this account's domain."
    }
    return ''
  }

  isValidEmail(email) {
    // send user an email and use confirmation as the validation, in front end, we just need add a simple check
    // see more: "https://davidcel.iss/posts/stop-validating-email-addresses-with-regex/"
    return /.+@.+\..+/i.test(email)
  }

  isValidEmailDomain(email) {
    const emailDomain = angular.lowercase(email.split('@')[1])
    const accountDomain = angular.lowercase(this.accountInfo.domain)
    return emailDomain === accountDomain
  }

  isTempUser(userInfo) {
    return _.has(userInfo, 'invitationId')
  }

  editUser(userForm, userInfo) {
    userForm.$show()
    this.userEditActive = true
    this.editUserId = userInfo.id
  }

  cancelEdit(userForm) {
    if (userForm) {
      userForm.$cancel()
    }
    this.userEditActive = false
    this.editUserId = undefined
  }

  disableEdit(userInfo) {
    return this.userEditActive && this.editUserId !== userInfo.id
  }

  /* Save user changes */

  saveUser(editData, userInfo) {
    let url
    let params = {}
    if (this.isTempUser(userInfo)) {
      url = `${this.apiBaseUrl}invitations/${userInfo.invitationId}`
      params['invitation'] = {
        firstName: editData.firstName,
        lastName: editData.lastName,
        email: editData.email,
        phone: editData.phone,
        status: 'PENDING'
      }
    } else {
      url = `${this.apiBaseUrl}users/${userInfo.id}`
      params['user'] = {
        firstName: editData.firstName,
        lastName: editData.lastName,
        email: editData.email,
        phone: editData.phone,
        roleNames: [editData.role],
        status: editData.status,
        contactExportFlag: editData.contactExportFlag
      }
    }

    this.updatingUserInfo = true

    return this.$http.put(url, params)
      .then(
        (response) => {
          this.alertService.addAlert('success', 'User info successfully updated.', 'fixed')
          // angular-xeditable will update the local model if saveUser() returns true.
          this.updatingUserInfo = false
          return true
        },
        (response) => {
          this.alertService.addAlert('error',
            'Sorry, something went wrong with updating this user.', 'fixed')
          // angular-xeditable will not update the local model if saveUser() returns a string.
          this.updatingUserInfo = false
          return 'error'
        }
      )
      .finally(
        () => {
          this.cancelEdit()
        }
      )
  }

  currentlyEditingUserRowWarning() {
    const params = {
      messageType: 'warning',
      title: 'User changes will not be saved',
      message: `You currently have an active edit on a user in the User Management table.
                  </br>
                  <div>
                    To save changes for a user, make sure to click
                    <span class='user-edit-save'>Save</span>
                    in the user's respective table row.
                  </div>`
    }

    this.sms.messageModal(params)
  }

  saveSlide() {
    if (this.userEditActive) {
      this.currentlyEditingUserRowWarning()
      return
    }

    if (this.canEditFeature()) {
      this.updateAccountQuotas(this.accountInfo.id)
        .then(() => {
          this.closeSlide()
        })
        .catch((errMsg) => {
          this.alertService.addAlert('error', errMsg, '#slide-alert')
        })
        .finally(() => {
          this.saving = false
        })
    }
  }

  openAdjustQuotaModal() {
    if (!this.quotas) {
      return
    }

    const data = {
      amount: null,
      description: null,
      // Max Quota - Remaining Quota. We can only adjust up to the maximum quota.
      maxAmount: this.quotas.CSVCompanyLimit - this.quotas.remainingCompanyQuota,
      // We can only decrease the quota to 0 at the minimum.
      minAmount: -this.quotas.remainingCompanyQuota,
    }

    const onConfirm = (result, close) => {
      if (!(data.amount && data.description)) {
        return
      }

      return this
        .$http({
          method: 'POST',
          url: `${config.apiBaseURL}accounts/${this.accountInfo.id}/quotas/adjust`,
          data: {
            value: parseInt(data.amount, 10),
            description: data.description,
          }
        })
        .then((res) => {
          const account = res.data.data

          this.quotas = account.quotas
          this.quotas.expiredTs = new Date(this.quotas.expiredTs)
          this.quotas.startTs = new Date(this.quotas.startTs)

          close()
          this.alertService.addAlert('success', 'Adjustment made!', '#slide-alert')
        })
        .catch((res) => {
          this.alertService.addAlert('error', `${res.data.message}`, '#modal-error')
        })
    }

    this.sms.showAdjustQuota({
      data,
      onConfirm,
    })
  }
}


export default AdminSlideController
