import ModalBaseController from '../modals/ModalBaseController.js'
import _ from 'lodash'

const CUSTOM_INSIGHTS_LIMIT = 3

class BlackListController extends ModalBaseController {
  /* @ngInject */
  constructor(close, $scope, params, insightsService) {
    super(close, $scope)
    this.$scope = $scope
    this.params = params
    this.insightsService = insightsService
    this.context = this.params.context
    this.dropdownSelect = _.cloneDeep(this.params.insightCategory)
    this.dropdownModel = _.cloneDeep(this.params.selectedCategory)
    this.msDataSrc = _.cloneDeep(this.params.categoryData)
    this.msDataType = _.cloneDeep(this.params.categoryType)
    this.apiBlackList = _.cloneDeep(this.params.selectedItems)
    this.title = this.params.title
    this.leftTitle = this.params.leftTitle
    this.rightTitle = this.params.rightTitle
    this.dropdownTips = this.params.dropdownTips || ''
    this.categoryInited = false
    this.templateType = this.params.templateType || ''
    this.init()
  }

  init() {
    // for $scope.$watch, refer to https://www.timroes.de/2015/07/29/using-ecmascript-6-es6-with-angularjs-1-x/
    this.$scope.$watch(() => this.dropdownModel, (newValue) => {
      this.selectCategory(newValue || {})
    })
    if (this.params.allSelections) {
      this.allSelections = () => {
        return this.params.allSelections(this.msDataSrc, this.apiBlackList)
      }
    }
  }

  customInsightsLimit() {
    return CUSTOM_INSIGHTS_LIMIT
  }

  limitExceeded() {
    if (this.allSelections) {
      return this.allSelections().length > CUSTOM_INSIGHTS_LIMIT
    } else {
      return false
    }
  }

  selectCategory(dropdownSelect) {
    const name = dropdownSelect.name || ''
    if (!name) {
      return
    }
    this.insightsService.details(name).then((response) => {
      this.msDataType = name
      // blackList1 is from api
      const blackList1 = _.map(this.apiBlackList, 'key')
      // blackList2 is from memory
      const blackList2 = _.flattenDeep(_.map(this.msDataSrc, (data) => {
        return _.map(_.filter(data, item => item.selected), 'key')
      }))
      const isSet = !!this.msDataSrc[name]
      this.msDataSrc[name] = _.map(response.data.data, (item) => {
        const selected = isSet ? _.includes(blackList2, item.key) :
          _.includes(blackList1, item.key)
        return {
          text: item.value,
          key: item.key,
          staged: false,
          selected
        }
      })
      this.otherCateSelected()
      this.categoryInited = true
    }, () => {
      this.msDataSrc[name] = []
      this.categoryInited = true
    })
  }

  otherCateSelected() {
    this.otherSelected = _.reduce(this.msDataSrc, (sum, items, key) => {
      const total = key === this.msDataType ? 0 :
        _.reduce(items, (sum2, item) => {
          return sum2 + (item.selected ? 1 : 0)
        }, 0)
      return sum + total
    }, 0)
  }

  totalSelected() {
    const total = this.allSelections().length
    return `(${total}/${CUSTOM_INSIGHTS_LIMIT})`
  }

  onConfirm() {
    this.close()
    this.params.onSave({
      insightCategory: this.dropdownSelect,
      selectedCategory: this.dropdownModel,
      categoryData: this.msDataSrc,
      categoryType: this.msDataType
    })
  }
}

export default BlackListController
