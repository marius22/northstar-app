import _ from 'lodash'
import moment from 'moment'

class ConfigurationController {
  /* @ngInject */
  constructor(salesforceService,
              userService,
              alertService,
              showModalService,
              insightsService,
              ModalService,
              $timeout,
              $state,
              $scope,
              $q,
              ghostService) {
    this.salesforceService = salesforceService
    this.userService = userService
    this.alertService = alertService
    this.showModalService = showModalService
    this.accountId = this.userService.accountId()
    this.userId = this.userService.userId()
    this.insightsService = insightsService
    this.modalService = ModalService
    this.$timeout = $timeout
    this.$state = $state
    this.$scope = $scope
    this.$q = $q
    this.ghostService = ghostService
    this.saving = false
    this.init()
  }

  init() {
    if (this.ghostService.isGhosting()) {
      this.accountId = this.ghostService.ghostId()
    }
    this.openHeader = false
    // set a timeout so that init-open-flag of title-pane directive take effects
    this.$timeout(() => {
      this.openHeader = true
    }, 0)
    this.categoryInited = false
    this.headerList = [] // extract data from api and shows in Header section

    // for custom insights of Header section
    // dropdown data
    /*
     this.insightsCategory1 = [
     {
     name: 'Tech'
     }
     ]
     */
    this.insightCategory1 = []
    this.selectedCategory1 = {}
    // multi-select
    /*
     this.categoryData = {
     tech: [
     {
     text: 'cate1',
     staged: false,
     selected: false
     }
     ]
     }
     */
    this.categoryData1 = {}
    this.categoryType1 = ''
    this.categoryInited1 = false
    this.leftTitle = 'Whitelisted'
    this.rightTitle = 'Blacklisted'

    // for blacklist of Insights section
    this.insightCategory2 = []
    this.selectedCategory2 = {}
    this.categoryData2 = {}
    this.categoryType2 = ''
    this.categoryInited2 = false

    // for detail structure please refer to
    // https://docs.google.com/document/d/1N7mQSu9Inl3Hx2fYijI4mrAYF1XXkFKL712J2PBfi64/edit#heading=h.ku6o1rm6d4yc
    this.config = {
      // Header
      insightHeadersDef: [],
      insightHeaders: [],
      insightHeadersCustom: [],

      // Insights
      segmentInsightsBlackList: [],
      showTopInsights: true,
      showCompanyKeywords: true,

      // Similar Companies
      showInCRM: true,
      showNotInCRM: true,
      showInsightsKeywords: true,
      allowCRMAdd: true,
      allowKeywordsAdd: true,
      allowCRMAddLeads: true,
      allowCRMAddAccounts: true,
      allowKeywordsAddAccounts: true,
      allowKeywordsAddLeads: true,

      showAudience: true,
      showAudienceScore: true,
      // Get Contacts
      showContacts: true,

      // Custom Insights
      showCustomInsights: true
    }
    this.enrich_enabled = false
    this.enrich_firmographics = false
    this.enrich_score = false
    this.enrich_start_ts = moment().format("MM/DD/YYYY");
    // data for header section and several true/false variable
    this.salesforceService.getConfig(this.accountId).then((response) => {
      if (response.data.data) {
        this.setConfig(response.data.data)
        this.headerList = this.getHeadersList()
        this.enrich_enabled = response.data.data.enrich_enabled
        this.enrich_firmographics = response.data.data.enrich_firmographics
        this.enrich_score = response.data.data.enrich_score
        this.enrich_start_ts = this.convertLocalToUtc(response.data.data.enrich_start_ts)
      }
    })
    // data for segment insights dropdown
    this.insightsService.categories().then((response) => {
      this.insightCategory1 = _.map(response.data.data, (item) => {
        return {name: item}
      })
      this.insightCategory2 = this.insightCategory1.slice()
      if (this.insightCategory1.length > 0) {
        this.selectedCategory1 = this.insightCategory1[0]
        this.selectedCategory2 = this.insightCategory2[0]
      }
      this.categoryInited = true
    })

    // package installed status
    this.packageInstalled = false
    this.salesforceService.packageInstalled(this.userId).then(() => {
      this.packageInstalled = true
    })
  }

  convertLocalToUtc(dateStr) {
    if (dateStr && dateStr !== 'None') {
      return moment(dateStr).format('MM/DD/YYYY');
    } else {
      return null
    }
  }

  setConfig(data) {
    this.config = _.extend(this.config, data.config)
    this.oldConfig = _.cloneDeep(this.config)
  }

  getHeadersList() {
    const result = []
    const selectedListKeys = _.map(this.config.insightHeaders, item => item.key)
    _.each(this.config.insightHeadersDef, (item) => {
      result.push({
        key: item.key,
        value: item.value,
        checked: _.includes(selectedListKeys, item.key)
      })
    })
    return result
  }

  onCancel() {
    // if there are no changes, go to crm.integration directly
    const payload = this.getPayload()
    const dirty = this.checkDirty(payload)
    if (!dirty) {
      this.$state.go('crm.integration')
      return
    }
    const modalMessage = `Do you want to leave without saving your configuration?`
    const modalParams = {
      title: 'Warning',
      messageType: 'warning',
      message: modalMessage,
      buttonText: {
        confirm: 'Yes',
        deny: 'No'
      },
      onConfirm: (result, close) => {
        this.$state.go('crm.integration')
        close()
      }
    }
    this.showModalService.confirmationModal(modalParams)
  }

  /**
   *
   * @param type header/insights
   */
  mergeMultiSelectData(type) {
    const categoryData = type === 'header' ?
      this.categoryData1 : this.categoryData2
    const original = type === 'header' ?
      this.config.insightHeadersCustom : this.config.segmentInsightsBlackList
    return this.mergeMultiSelectDataBase(categoryData, original)
  }

  mergeMultiSelectDataBase(categoryData, originalData) {
    let original = originalData
    let whiteList = []
    let blackList = []
    for (const key in categoryData) {
      blackList = _.concat(blackList, _.filter(categoryData[key], item => item.selected))
      whiteList = _.concat(whiteList, _.filter(categoryData[key], item => !item.selected))
    }
    original = _.differenceBy(original, whiteList, 'key')
    original = _.unionBy(original, blackList, 'key')
    original = _.map(original, item => {
      return item.key ? {
        key: item.key,
        // item.text is from user selection
        // item.value is from previous-saved data
        //value: item.text || item.value
        //value will change sometimes, so we do not offer value, will get value from list of insights
      } : false
    })
    original = _.filter(original, item => item !== false)
    return original
  }

  checkDirty(data) {
    let dirty = false
    const config = this.oldConfig
    for (const key in data) {
      if (!_.isEqual(data[key], config[key])) {
        dirty = true
        break
      }
    }
    return dirty
  }

  getInsightHeaders() {
    const insightHeaders = _.map(
      _.filter(this.headerList, item => item.checked),
      (item) => {
        return {key: item.key, value: item.value}
      })
    return insightHeaders
  }

  getPayload() {
    const insightHeaders = this.getInsightHeaders()
    let payload = {}
    payload.config = {
      insightHeaders,
      insightHeadersCustom: this.mergeMultiSelectData('header'),
      segmentInsightsBlackList: this.mergeMultiSelectData('insights'),
      // Insights
      showTopInsights: this.config.showTopInsights,
      showCompanyKeywords: this.config.showCompanyKeywords,
      // Similar Companies
      showInCRM: this.config.showInCRM,
      showNotInCRM: this.config.showNotInCRM,
      showInsightsKeywords: this.config.showInsightsKeywords,
      allowCRMAdd: this.config.allowCRMAdd,
      allowKeywordsAdd: this.config.allowKeywordsAdd,
      allowCRMAddLeads: this.config.allowCRMAddLeads,
      allowCRMAddAccounts: this.config.allowCRMAddAccounts,
      // Allow the user to add a person as a Lead
      allowCRMAddPeopleToLeads: this.config.allowCRMAddPeopleToLeads,
      // Allow the user to add a person as a Contact
      allowCRMAddPeopleToContacts: this.config.allowCRMAddPeopleToContacts,
      allowKeywordsAddAccounts: this.config.allowKeywordsAddAccounts,
      allowKeywordsAddLeads: this.config.allowKeywordsAddLeads,

      showAudience: this.config.showAudience,
      showAudienceScore: this.config.showAudienceScore,
      // Get Contacts
      showContacts: this.config.showContacts,
      // Custom Insights
      showCustomInsights: this.config.showCustomInsights
    }
    payload.enrich_enabled = this.enrich_enabled
    payload.enrich_firmographics = this.enrich_firmographics
    payload.enrich_score = this.enrich_score
    payload.enrich_start_ts = this.enrich_start_ts ? moment(this.enrich_start_ts, "MM/DD/YYYY").utc().valueOf() : -1
    return payload
  }

  onSave() {
    const payload = this.getPayload()

    /*if (payload.config.insightHeadersCustom.length > 3) {
     this.alertService.addAlert('error', 'Please select up to 3 Custom Insights', '#topAlert')
     return
     }*/
    // if there are no changes, go to crm.integration directly
    const modalMessage = this.packageInstalled ?
      `Updates to the Salesforce App take place immediately. <br/><b>Save Changes?</b>` :
      `Your changes will be saved but they will not take effect until <br/>the managed package is installed.`
    const buttonText = this.packageIntalled ? {
      confirm: 'Yes',
      deny: 'No'
    } : {
      confirm: 'Ok',
      deny: 'Cancel'
    }
    const modalParams = {
      title: 'Warning',
      messageType: 'warning',
      message: modalMessage,
      buttonText,
      onConfirm: (result, close) => {
        close()
        this.saving = true
        this.salesforceService.saveConfig(this.accountId, this.getPayload()).then((response) => {
          this.saving = false
          this.setConfig(response.data.data)
          this.alertService.addAlert('success', 'Changes saved successfully.', '#topAlert')
        }, () => {
          this.alertService.addAlert('error', 'Changes failed to save.', '#topAlert')
          this.saving = false
        })
      }
    }
    this.showModalService.confirmationModal(modalParams)
  }

  editInsightHeadersCustom() {
    this.openBlackListWindow({
      context: 'header',
      allSelections: this.mergeMultiSelectDataBase,
      insightCategory: this.insightCategory1,
      selectedCategory: this.selectedCategory1,
      categoryData: this.categoryData1,
      categoryType: this.categoryType1,
      selectedItems: this.config.insightHeadersCustom,
      title: 'Custom Header Insights',
      leftTitle: 'Not visible in SFDC App',
      rightTitle: 'Will be visible in SFDC App',
      onSave: (data) => {
        this.insightCategory1 = data.insightCategory
        this.selectedCategory1 = data.selectedCategory
        this.categoryData1 = data.categoryData
        this.categoryType1 = data.categoryType
      }
    })
  }

  editSegmentInsightsBlackList() {
    this.openBlackListWindow({
      context: 'insights',
      insightCategory: this.insightCategory2,
      selectedCategory: this.selectedCategory2,
      categoryData: this.categoryData2,
      categoryType: this.categoryType2,
      selectedItems: this.config.segmentInsightsBlackList,
      title: 'Blacklist Signals & Insights',
      leftTitle: 'Currently visible in SFDC App',
      rightTitle: 'Will NOT be visible in SFDC App',
      dropdownTips: `By default, the top 20 insights per company will show. If you wish NOT to expose some,
                please select from the list below to blacklist. If a blacklisted insight happens to
                be in the Top 20 in a segment, we will automatically skip to the next best one that is ok to expose.`,
      templateType: 'reverse',
      onSave: (data) => {
        this.insightCategory2 = data.insightCategory
        this.selectedCategory2 = data.selectedCategory
        this.categoryData2 = data.categoryData
        this.categoryType2 = data.categoryType
      }
    })
  }

  openBlackListWindow(params) {
    if (!this.categoryInited) {
      return
    }
    this.modalService.showModal({
      templateUrl: './modals/blacklist.html',
      controller: 'BlackListController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  disableInsightCheckbox(item) {
    const totalSelected = this.getInsightHeaders().length
    return !item.checked && totalSelected >= 3
  }

  isGhosting() {
    return this.ghostService.isGhosting()
  }
}

export default ConfigurationController
