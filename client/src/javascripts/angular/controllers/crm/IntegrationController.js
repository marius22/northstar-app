export default class IntegrationController {
  /* @ngInject */
  constructor(salesforceService,
              marketoService,
              ghostService,
              alertService,
              modelService,
              $q,
              showModalService,
              apiBaseUrl,
              $state) {
    this.salesforceService = salesforceService
    this.marketoService = marketoService
    this.ghostService = ghostService
    this.alertService = alertService
    this.modelService = modelService
    this.$q = $q
    this.sms = showModalService
    this.$state = $state
    this.apiBaseUrl = apiBaseUrl
    this.packageInstalled = false
    this.integrated = false
    this.packageInstallPending = false
    this.integratePending = false
    this.userId = this.ghostService.userId()
    this.inited = false
    this.init()
  }

  init() {
    this.$q.all([
      this.checkOauthAsync(),
      this.checkPackageAsync()
    ]).then(() => {
      this.inited = true
    }, () => {
      this.inited = true
    })

    this.packageLinkInited = !!this.salesforceService.getManagedPackageUrl()
    if (!this.packageLinkInited) {
      this.salesforceService.fetchData().then(() => {
        this.packageLinkInited = true
      })
    }

    this.checkMarketoIntegration('marketo')
    this.checkMarketoIntegration('marketo_sandbox')
    this.getCRMFitModels()
  }

  switchToSandbox() {
    this.salesforceService.useSandbox()
  }

  switchToProduction() {
    this.salesforceService.useProduction()
  }

  isLoadingSFDC() {
    return !this.inited || this.packageInstallPending || this.integratePending
  }

  isLoadingMarketo() {
    return this.checkingMarketoIntegration
  }

  checkPackageAsync() {
    return this.salesforceService.packageInstalled(this.userId)
      .then(() => {
        this.packageInstalled = true
      })
  }

  checkOauthAsync() {
    return this.salesforceService.isIntegrated({forceCheck: true})
      .then((response) => {
        this.integrated = response.data.data
      })
  }

  getPackageBtnStatus() {
    return this.packageInstalled ? 'Uninstall Package' : 'Install Package'
  }

  getIntegrateStatus() {
    return this.integrated ? 'Revoke OAuth' : 'CRM OAuth'
  }

  clickPackage() {
    if (this.packageInstallPending) {
      return
    }
    if (this.packageInstalled) {
      const link = this.salesforceService.usingSandbox() ? 'https://test.salesforce.com' : 'https://login.salesforce.com'
      window.open(link)
      return
    }
    this.upstallPackage()
  }

  upstallPackage() {
    this.packageInstallPending = true
    this.salesforceService.openInstallPackageWindow(() => {
      this.checkPackageAsync().then(() => {
        this.packageInstallPending = false
      }, () => {
        this.packageInstallPending = false
      })
    })
  }

  clickIntegrate() {
    if (this.integratePending) {
      return
    }
    this.integratePending = true
    if (this.integrated) {
      this.salesforceService.removeToken().then((data) => {
        this.integratePending = false
        this.integrated = false
        this.packageInstalled = false
      }, () => {
        this.integratePending = false
      })
    } else {
      this.salesforceService.openSFDCOauthWindow(() => {
        this.checkOauthAsync().then(() => {
          this.integratePending = false
          if (this.integrated) {
            this.checkPackageAsync()
            this.alertService.clearAlerts()
            this.alertService.addAlert('success', 'Salesforce integration successful.', '#topAlert')
          } else {
            this.alertService.addAlert('error', 'Salesforce integration failed.', '#topAlert')
          }
        }, () => {
          this.integratePending = false
          this.alertService.addAlert('warning', 'Salesforce integration failed.', '#top-alert')
        })
      })
    }
  }

  // Go to support page for integrating with Salesforce and installing managed package
  clickSFDCGuide() {
    window.open(`${this.apiBaseUrl}salesforce/package_guide_link`, '_blank');
  }

  // Go to support page for integrating with Marketo
  clickMarketoGuide() {
    window.open(`${this.apiBaseUrl}salesforce/package_guide_link?linkType=mkto`, '_blank')
  }

  allowIntegrationContextSwitch() {
    return !this.integrated && !this.packageInstalled
  }

  clickMappingMarketo() {
    const integrations = _.pickBy(this.marketoService.integrationStatus)
    if (_.isEmpty(integrations)) {
      this.alertService.addAlert('error', 'You must integrate first.')
      return
    }
    this.$state.go('settings.mkto.fields')
  }

  clickMappingSFDC() {
    if (!this.integrated) {
      this.alertService.addAlert('error', 'You must integrate first.')
      return
    }
    this.$state.go('settings.sfdc.fields.account')
  }

  clickInstance(type) {
    this.sms.instanceOfMarketo({'type': type, onRevoke: () => {
      this.checkMarketoIntegration('marketo')
      this.checkMarketoIntegration('marketo_sandbox')
    }})
  }

  checkMarketoIntegration(type) {
    this.checkingMarketoIntegration = true
    return this.marketoService.isIntegrated(type).then((resp) => {
      if (resp.data.data) {
        this.marketoService.integrationStatus[type] = true
      } else {
        this.marketoService.integrationStatus[type] = false
      }
      this.checkingMarketoIntegration = false
    })
  }

  getCRMFitModels() {
    const accountId = this.ghostService.accountId()
    const maps = {
      IN_PROGRESS: '(In Progress)',
      COMPLETED: '(Done)',
      NO_MODEL: '(Not Started)'
    }
    this.modelService.httpCRMFitModels(accountId).then((response) => {
      this.crmStatus = response.data.data
      this.crmStatusText = maps[this.crmStatus]
    })
  }
}
