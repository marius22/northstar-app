import SlideController from '../modals/SlideController'

export default class InstanceOfMarketoController extends SlideController {
  /* @ngInject */
  constructor($scope, $document, $timeout, userService, alertService,
              marketoService, params, $q, $http, apiBaseUrl, showModalService) {

    super(close, $document, $timeout)

    this.$scope = $scope
    this.userService = userService
    this.alertService = alertService
    this.marketoService = marketoService
    this.objType = params.type
    this.onRevoke = params.onRevoke

    this.objTypeMap = new Map([['Production', 'marketo'], ['Sandbox', 'marketo_sandbox']])
    this.$q = $q
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.showModalService = showModalService

    this.integration = {}
    this.userId = this.userService.userId()
    this.openSlide()

    this.getInstanceStatus()
  }

  revokeIntegration(evt) {
    evt.preventDefault()

    const onConfirm = (result, close) => {
      close()

      return this
        .marketoService.revokeIntegration({
          integrationType: this.objTypeMap.get(this.objType)
        })
        .then(() => {
          this.closeSlide()
          this.onRevoke()
          this.alertService.addAlert('success', 'Marketo integration revoked')
        })
    }

    this.showModalService.confirmationModal({
      title: 'Warning',
      message: 'This will revoke your Marketo integration. You will no longer be able to publish to Marketo. <br /> Do you want to continue?',
      messageType: 'warning',
      onConfirm,
    })
  }

  submit() {
    const that = this
    this.$http.post(`${this.apiBaseUrl}mas/integration`, {
      "mas_type": this.objTypeMap.get(this.objType),
      "token": {
        "client_id": this.integration.client_id,
        "client_secret": this.integration.client_secret,
        "endpoint": this.integration.endpoint
      }
    }).success((resp) => {
      that.marketoService.integrationStatus[this.objTypeMap.get(this.objType)] = true
      this.alertService.clearAlerts()
      that.closeSlide()
    }).error((resp) => {
      this.alertService.addAlert('error', "We can not integrate with your input, please check then try again.", '#slide-alert')
    })
  }

  getInstanceStatus() {
    this
      .marketoService.isIntegrated(this.objTypeMap.get(this.objType))
      .then((resp) => {
        if (resp.data.data) {
          this.integration = resp.data.data
          this.integration.fromServer = true
        }
      })
  }
}
