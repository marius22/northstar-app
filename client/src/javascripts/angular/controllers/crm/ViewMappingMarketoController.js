import SlideController from '../modals/SlideController'

export default class ViewMappingMarketoController extends SlideController {
  /* @ngInject */
  constructor(close, $scope,
              $document,
              $timeout,
              userService,
              alertService,
              marketoService,
              $q,
              params,
              $http,
              apiBaseUrl, publishAudienceService) {
    super(close, $document, $timeout)

    this.$scope = $scope
    this.$timeout = $timeout
    this.userService = userService
    this.alertService = alertService
    this.marketoService = marketoService
    this.$q = $q
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.params = params
    this.pas = publishAudienceService

    this.marketo = {}
    this.mapping = {}
    this.mappingAudience = []
    this.userId = this.userService.userId()
    this.marketoFields = []
    this.isLoading = true
    this.getMapping()

    this.openSlide()
  }


  getMapping() {
    this.$http.get(`${this.apiBaseUrl}mas/fields_mapping`, {
      params: {mas_type: this.pas.state.masType}
    }).success((resp) => {
      this.mapping = resp.data.mapping
      /*if (this.mapping) {
       for (const key of Object.keys(this.mapping.audience)) {
       this.mappingAudience.push({key, value: this.mapping.audience[key]})
       }
       this.isLoading = false
       }*/
      this.getAudienceFields()
    }).error((resp) => {
      this.alertService.addAlert('error', resp.message || "Failed to get data", '#slide-alert')
      this.isLoading = false
    })
  }

  getAudienceFields() {
    this.marketoService.getAudiences()
      .success((resp) => {
        this.audienceFields = resp.data;

        let tempMappingAudience = [];
        if (this.mapping) {
          for (var key of Object.keys(this.mapping.audience)) {

            tempMappingAudience.push({'key': {id: key}, 'value': this.mapping.audience[key]})
          }
        }
        this.mappingAudience = []
        for (let index in tempMappingAudience) {
          let targetItem = this.audienceFields.find(item2 => {
            return item2.id + '' === tempMappingAudience[index].key.id + ''
          })
          this.mappingAudience.push({
              key: targetItem, value: tempMappingAudience[index].value
            }
          )
        }
        this.isLoading = false
        console.log('this.audifields=', this.audienceFields, this.mappingAudience)
      })
      .error((resp) => {
        this.alertService.addAlert('error', resp.message || "Failed to get data", '#slide-alert')
        this.isLoading = false
      })
  }

  findMapping(arr, key) {
    return arr.find((item) => item.key === key)
  }

}
