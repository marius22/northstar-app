const ES_ADMIN_CONSOLE_MENU_ITEM = {
  text: 'EverString Admin',
  icon: 'btr bt-crown',
  sref: 'admin.license'
}

const ACCOUNT_SETTINGS_MENU_ITEM = {
  text: 'Account',
  icon: 'btr bt-gear',
  sref: 'settings.users'
}

const SF_APP_CONFIG_MENU_ITEM = {
  text: 'Configuration',
  icon: 'btr bt-wrench',
  sref: 'crm.configuration'
}

const INTEGRATION_SETTINGS_MENU_ITEM = {
  text: 'Integrations',
  icon: 'btr bt-sync',
  sref: 'crm.integration'
}

class HeaderNavController {
  /* @ngInject */
  constructor(userService,
              alertService,
              showModalService,
              $http,
              apiBaseUrl,
              $state,
              ghostService,
              salesforceService,
              $scope) {
    this.userService = userService
    this.alertService = alertService
    this.sms = showModalService
    this.ghostService = ghostService
    this.salesforceService = salesforceService
    this.apiBaseUrl = apiBaseUrl
    this.salesforceService = salesforceService
    this.$http = $http
    this.$state = $state

    $scope.$on('g_ghosting', e => {
      this.setSettingsDropdownItems()
    });

    const init = () => {
      this.settingsDropdownItems = []
      this.showProfileDropdown = false
      this.showSettingsDropdown = false
      this.isAdsUser = false
      this.isTrial = userService.isTrial()
      this.isRegular = userService.isRegular()
      this.user = this.userService.getUser()
      this.isAdsUser = this.userService.isAdsUser()
      this.setSettingsDropdownItems()
    }
    init()

    $scope.$watch(() => this.userService.user, () => {
      init()
    }, true)
  }

  toggleSettingsDropdown() {
    this.showSettingsDropdown = !this.showSettingsDropdown
  }

  toggleProfileDropdown() {
    this.showProfileDropdown = !this.showProfileDropdown
  }

  integrate() {
    this.$state.go('crm.integration')
  }

  settingsDropdownHasItems() {
    return this.settingsDropdownItems.length > 0
  }

  // don't show the settings portion of the header to non-admin users to avoid confusion
  // since they have nothing in their settings dropdown
  shouldShowSettings() {
    return this.settingsDropdownHasItems()
  }

  shouldShowSettingsDropdown() {
    const dropdownToggledOnAndHasItems = this.showSettingsDropdown && this.settingsDropdownHasItems()
    return dropdownToggledOnAndHasItems
  }

  setSettingsDropdownItems() {
    if (this.userService.isSuper() || this.userService.isGstAdmin()) {
      this.settingsDropdownItems = [
        ES_ADMIN_CONSOLE_MENU_ITEM,
        ACCOUNT_SETTINGS_MENU_ITEM
      ]
    } else if (this.userService.isAcctAdmin()) {
      this.settingsDropdownItems = [
        ACCOUNT_SETTINGS_MENU_ITEM,
        INTEGRATION_SETTINGS_MENU_ITEM
      ]
    }
  }

  showChangePassModal() {
    const validatePassword = (result, close) => {
      if (result && result.password) {
        if (result.password !== '' &&
          result.password === result.repeatPassword) {
          this.changePass(result.password)
            .then(() => {
              close()
              this.alertService.addAlert('success', 'Password Changed.', '#topAlert')
            }, (response) => {
              this.alertService.addAlert('error', response.data.message, '#change-pass-error')
            })
        } else {
          this.alertService.addAlert('error', 'Password is not the same as repeat password', '#change-pass-error')
        }
      } else {
        this.alertService.addAlert('error', 'Password can not be empty', '#change-pass-error')
      }
    }

    this.sms.changePassword({
      onConfirm: validatePassword
    })
  }

  logout() {
    return this.userService.logout()
      .then(() => {
        this.$state.go('user.login')
      })
  }

  stopGhosting() {
    this.ghostService.stopGhosting()
    this.setSettingsDropdownItems()
  }

  changePass(password) {
    return this.$http.post(`${this.apiBaseUrl}users/change_password`, {
      new_password: password
    })
  }

  /* Whether or not we show "Trial User" with a lock icon on the right side of the nav bar */
  shouldShowTrialTag() {
    return this.userService.isTrial()
  }

  /* Whether or not we show ghosting status on the right side of the nav bar */
  shouldShowGhostTag() {
    return this.ghostService.isGhosting()
  }

  /* Whether or not we show link to V1 on the right side of the nav bar */
  shouldShowOldSwitch() {
    //TODO: Appropriate logic when we know more
    return this.$state.includes('audience') && this.userService.canUsePreviousVersion()
  }

  isAudState() {
    return this.$state.includes('audience')
  }

  isExcState() {
    return this.$state.includes('exclusion')
  }

  /* Whether or not we show link to V3 on the right side of the nav bar */
  shouldShowNewSwitch() {
    return this.$state.includes('segment')
  }

  toOld() {
    this.$state.go('segment.console')
  }

  toNew() {
    this.$state.go('audience.console')
  }

  clickSupport() {
    window.open(`${this.apiBaseUrl}salesforce/package_guide_link?linkType=sprt`, '_blank');
  }
}


export default HeaderNavController
