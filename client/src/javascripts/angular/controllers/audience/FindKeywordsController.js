import * as animations from 'animations'

import SlideController from '../modals/SlideController'

class FindKeywordsController extends SlideController {
  /* @ngInject */
  constructor(close, $document, $timeout, params, audienceService, $state) {
    super(close, $document, $timeout)
    this.openSlide()
    this.$state = $state
    this.purpose = params.purpose
    this.kwModel = this.isViewing() ? params.kwModel : {items: []}
    this.onSave = params.onSave
    this.placeholderText = 'Enter company URL(s)'
    this.tableCrudItems = ['galaxy', 'clearAll']

    this.tutorialMessages = [
      'Enter a company URL and we will find the keywords that describe what the company does.',
      'For example, you can enter salesforce.com or marketo.com'
    ]
    this.showTutorialMessage = !audienceService.hasCreatedAudience() && this.findingByKeywords() && this.isFinding()
  }

  isFinding() {
    return !this.isViewing()
  }

  isViewing() {
    return this.purpose === 'view'
  }

  findingByKeywords() {
    return this.$state.is('audience.find.keywords') || this.$state.is('exclusion.find.keywords')
  }

  headerText() {
    // We only need to account for URLs in the headerText for the isViewing() case.
    const keywordsOrUrls = this.findingByKeywords() ? 'Keywords' : 'URLs'
    return this.isFinding() ? 'Find Keywords' : `${keywordsOrUrls} Used in Search`
  }

  appropriateSearchType() {
    if (this.findingByKeywords() && this.isFinding()) {
      return 'keywordsByDomains'
    }
    if (this.findingByKeywords() && this.isViewing()) {
      return 'companiesByKeywords'
    }
    if (!this.findingByKeywords()) {
      return 'companiesByDomains'
    }
  }

  cancelBtnText() {
    return this.isFinding() ? 'Cancel' : 'Close'
  }

  saveAndClose() {
    const savedKws = _(this.kwModel.items)
      .filter(kwObj => {
        return kwObj.selected
      })
      .map(kwObj => {
        return kwObj.text
      })
      .value()

    this.onSave({items: savedKws})
    this.closeSlide()
  }

  atLeastOneKeywordSelected() {
    const selectedItems = _.filter(this.kwModel.items, (item) => {
      return item.selected
    })
    return !_.isEmpty(selectedItems)
  }

  getTableCrudItems() {
    return this.isFinding() ? this.tableCrudItems : undefined
  }

  getTagAreaHeader() {
    return this.isFinding() ? 'Click on the keywords you wish to include in your search' : undefined
  }

  hideTutorialMessage() {
    animations
      .animateTooltip('.find-keywords .find-keywords-intro',
                      '.find-keywords .main .fa-info-circle')
      .then(() => {
        this.showTutorialMessage = false
      })
  }
}

export default FindKeywordsController
