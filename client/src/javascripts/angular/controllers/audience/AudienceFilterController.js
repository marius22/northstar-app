import _ from 'lodash'
import SlideController from '../modals/SlideController'

const INITIAL_RENDER_LIMIT = 100

class AudienceFilterController extends SlideController {
  /* @ngInject */
  constructor(close, $document, $timeout, params, filterService, $filter, $scope, showModalService) {
    super(close, $document, $timeout)
    this.openSlide()
    this.$filter = $filter
    this.$scope = $scope
    this.filterService = filterService
    this.sms = showModalService
    this.purpose = params.purpose
    this.onClose = params.onClose ? params.onClose : _.noop

    // set up category dropdown data and initially selected item
    this.primaryFilterItems = filterService.getPrimaryFilters()
    this.currentPrimaryFilterItem = params.key ? _.find(this.primaryFilterItems, {'key': params.key}) : this.primaryFilterItems[0]

    // set up data structures for summary boxes
    this.summaryFilters = {}
    this.summaryRenderLimit = {}
    _.forEach(this.primaryFilterItems, (item) => {
      this.summaryFilters[item.key] = {text: ''}
      this.summaryRenderLimit[item.key] = INITIAL_RENDER_LIMIT
    })

    if (filterService.inited) {
      this.handleFiltersInitState()
    }

    this.changeDetected = false
    $scope.$on('selectionChanged', () => {
      this.changeDetected = true
    })

    // Re-run some constructor logic if filterService isn't inited
    // yet when the filter audience slide opens
    $scope.$watch('afc.filterService.inited', (newVal, oldVal) => {
      const changedToInited = newVal && !oldVal
      if (changedToInited) {
        this.handleFiltersInitState()
      }
    })

    $scope.$watch('afc.currentPrimaryFilterItem', (newVal) => {
      this.setActiveSubFilter()
    })
  }

  // If getting filter data failed, alert user and allow for retry in the UI.
  // Otherwise, do the rest of the intended controller init logic.
  handleFiltersInitState() {
    if (_.isEmpty(this.filterService.filterData)) {
      this.initFailed = true
    } else {
      this.filterData = this.filterService.filterData
      this.subFilterItems = this.filterService.subFilterItems
      this.setActiveSubFilter()
      this.setAllFilterItems()
    }
  }

  retryFiltersInit() {
    this.initFailed = false
    this.filterService.appropriateInit()
  }

  summaryPaneTitle() {
    const total = this.filterService.totalActiveCategories()
    return `Summary (${total})`
  }

  /*
    Set this.subFilterCategory which respresents the currently selected
    subfilter in the subfilter dropdown
  */
  setActiveSubFilter() {
    const prmKey = this.currentPrimaryFilterItem.key
    if (this.hasSubFilters(prmKey) && this.subFilterItems[prmKey]) {
      this.subFilterCategory = this.subFilterItems[prmKey][0]
    } else {
      this.subFilterCategory = undefined
    }
  }

  hasSubFilters(primaryFilterKey = this.currentPrimaryFilterItem.key) {
    return this.filterService.hasSubFilters(primaryFilterKey)
  }

  /*
    Set this.allFilterItems to be an array of filter and subfilter category
    objects. This array is iterated over to show the filter summary.
  */
  setAllFilterItems() {
    let eventuallyAllFilterItems = _.cloneDeep(this.primaryFilterItems)
    _.forEach(this.subFilterItems, (subFilterGroup) => {
      if (subFilterGroup) {
        eventuallyAllFilterItems = _.concat(eventuallyAllFilterItems, subFilterGroup)
      }
    })
    this.allFilterItems = eventuallyAllFilterItems
  }

  initialOpenFilter() {
    return this.purpose === 'filter'
  }

  initialOpenSummary() {
    return this.purpose === 'summary'
  }

  clearAllOnclick() {
    const message = 'This will clear all criteria in each category. </br> Do you want to continue?'
    const title = 'Warning'
    const messageType = 'warning'
    const onConfirm = (result, close) => {
      this.filterService.clearAllFilters()
      this.changeDetected = true
      close()
    }
    const params = {message, onConfirm, title, messageType}
    this.sms.confirmationModal(params)
  }

  closeFilterSlide() {
    this.onClose(this.changeDetected)
    this.closeSlide()
  }

  filteringOnTechnology() {
    return this.currentPrimaryFilterItem.key === 'tech'
  }
}

export default AudienceFilterController
