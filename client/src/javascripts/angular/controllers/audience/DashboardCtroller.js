class DashboardController {
  /* @ngInject */
  constructor($scope,
              audienceService,
              $stateParams,
              userService,
              $state) {
    this.audienceService = audienceService
    this.userService = userService
    this.$state = $state

    const audienceId = $stateParams.audienceId
    this.audienceId = audienceId
    this.audienceService.getAudienceById(audienceId).then((audience) => {
      this.audience = audience
    })

    // These $stateChange* events are documented here: http://bit.ly/20CHcVw
    $scope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
      this.audienceService.isLoading = true
    })
    $scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
      this.audienceService.isLoading = false
    })
  }

  shouldDisableContents() {
    return this.$state.is('audience.dashboard.model-insights') && !this.userService.modelInsightsEnabled()
  }
}

export default DashboardController
