import SlideController from '../modals/SlideController'
import _ from 'lodash'

class CustomModelController extends SlideController {
  /* @ngInject */
  constructor(close,
              $document,
              $timeout,
              $http,
              $scope,
              apiBaseUrl,
              userService,
              alertService,
              params) {
    super(close, $document, $timeout)
    this.$http = $http
    this.$scope = $scope
    this.apiBaseUrl = apiBaseUrl
    this.accountId = userService.accountId()
    this.alertService = alertService
    this.customFitValidationRules = {
      maxLines: 100000,
      minLines: 70,
      numColumns: [1, 2]
    }
    this.uploader = {
      validated: false,
      url: `${this.apiBaseUrl}accounts/${this.accountId}/fit_models/create_by_csv`,
      methods: {}
    }
    this.model = {
      modelName: '',
      file: null
    }
    this.params = {modelName: ''}
    this.saving = false
    this.onSaveClose = params.onSaveClose
    this.currentModelNames = params.currentModelNames
    this.openSlide()
  }

  init() {
    this.$scope.$watch(() => {
      return this.canSave()
    }, (canSave) => {
      if (canSave) {
        this.alertService.addAlert('warning', 'Clicking "Save & Close" will kick off a ' +
          'new model that may take a while before it can be used.', '#slide-alert')
      }
    })
  }

  canSave() {
    const canSave = this.params.modelName && this.uploader.validated && !this.saving && !this.hasSameName()
    return canSave || this.alertService.clearAlerts()
  }

  saveAndClose() {
    this.saving = true
    this.uploader.methods.upload()
  }

  onSaveSuccess() {
    this.saving = false
    this.closeSlide()
    this.onSaveClose()
  }

  hasSameName() {
    return _.includes(this.currentModelNames, angular.lowercase(this.params.modelName))
  }

  onSaveFailed() {
    this.alertService.addAlert(
      'error',
      'There was a problem with kicking off your model run. Please retry, and contact Customer Support if problems persist.',
      '#slide-alert'
    )
    this.saving = false
  }
}

export default CustomModelController
