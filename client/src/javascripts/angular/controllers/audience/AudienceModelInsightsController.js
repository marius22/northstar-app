import _ from 'lodash'
import Papa from 'papaparse'

class AudienceModelInsightsController {
  /* @ngInject */
  constructor(audienceService,
              $timeout,
              ModalService,
              proxyService,
              $q,
              alertService,
              modelService,
              userService,
              $stateParams) {
    this.alertService = alertService
    this.audienceService = audienceService
    this.modalService = ModalService
    this.modelService = modelService
    this.userService = userService
    this.$timeout = $timeout
    this.proxyService = proxyService
    this.$q = $q

    this.audienceId = $stateParams.audienceId
    this.init()
  }

  init() {
    this.loadingInsightsDependencies = true
    this.audienceService.getAudienceById(this.audienceId).then((audience) => {
      this.audience = audience
      this.getFitModels().then(
        () => {
          this.loadingInsightsDependencies = false
          this.initModelInsights()
        }
      )
    })
  }

  initModelInsights() {
    this.indicatorData = {}
    this.indicatorCategories = []

    if (!this.currentFitModel) {
      this.alertService.addAlert('warning', 'No fit model set for this audience. Unable to fetch insights.')
      return
    }

    if (this.currentFitModel.faModelId) {
      const topDataPrm = this.getTopIndicatorData()
      const bottomDataPrm = this.getBottomIndicatorData()
      this.loadingInsights = true
      this.$q.all([topDataPrm, bottomDataPrm]).then(
        (indicatorData) => {
          this.indicatorData.top = indicatorData[0]
          this.indicatorData.bottom = indicatorData[1]
          // categories should match in top and bottom, so we just
          // arbitrarily use top here
          this.indicatorCategories = Object.keys(this.indicatorData.top)

          // open title panes
          this.openedIndex = -1
          // add 50 milliseconds delay to show animations for the first title-pane
          // it's inaccurate to set the value 50 though, but i don't find a better way
          // to show the first animation
          this.$timeout(() => {
            this.openedIndex = 0
          }, 50)
        }
      ).finally(
        () => {
          this.loadingInsights = false
        }
      )
    } else {
      this.alertService.addAlert('error', 'Current fit model has no feature analysis model id. Unable to fetch insights.')
    }

    this.loadingKeywords = true
    this.getKeywordCloudData().then(
      (keywordsData) => {
        this.$timeout(() => {
          this.tags = _.map(keywordsData, (datum) => {
            return {
              key: datum.grams,
              value: datum.score
            }
          })
        }, 0)
      }
    ).finally(
      () => {
        this.loadingKeywords = false
      }
    )
  }

  getFitModels() {
    return this
      .modelService.getList(this.userService.accountId())
      .then((fitModels) => {
        this.fitModels = fitModels
        this.currentFitModel = this.fitModels.filter((m) => m.id === this.audience.modelId)[0]
      })
  }

  getOtherFitModels() {
    if (!this.currentFitModel) {
      return this.fitModels
    }
    if (this.fitModels) {
      return this.fitModels.filter((model) => {
        return model.id !== this.currentFitModel.id
      })
    }
  }

  updateFitModel(modelId) {
    return this
      .audienceService
      .updateFitModel(this.audience.id, modelId)
      // After updating the fit model, we need to reload the keywords cloud and model insights
      // on the page
      .then(() => {
        this.init()
      })
  }

  rememberOpenedIndex(index) {
    this.openedIndex = index
  }

  showWordsCloudWindow() {
    const params = {
      tags: this.tags,
      modelName: this.currentFitModel.name
    }
    this.modalService.showModal({
      templateUrl: './modals/audienceModelCloud.html',
      controller: 'ModalController',
      controllerAs: 'mdc',
      inputs: {params}
    })
  }

  getTopIndicatorData() {
    const faModelId = this.currentFitModel.faModelId
    const url = `/common/audiences/${this.audienceId}/indicators?modelId=${faModelId}&limit=5&order=desc`
    return this.proxyService.getWithoutCancel(url).then(
      (response) => {
        return response.data.data
      }
    )
  }

  getBottomIndicatorData() {
    const faModelId = this.currentFitModel.faModelId
    const url = `/common/audiences/${this.audienceId}/indicators?modelId=${faModelId}&limit=5&order=asc`
    return this.proxyService.getWithoutCancel(url).then(
      (response) => {
        return response.data.data
      }
    )
  }

  getKeywordCloudData() {
    const url = `/common/model/to/pooltags?modelId=${this.currentFitModel.id}&limit=100`
    return this.proxyService.get(url).then(
      (response) => {
        return response.data.data
      }
    )
  }

  getIndicatorDataForCSVDownload() {
    const faModelId = this.currentFitModel.faModelId
    const url = `/common/audiences/${this.audienceId}/indicators?modelId=${faModelId}&order=desc`
    return this.proxyService.get(url).then(
      (response) => {
        return response.data.data
      }
    )
  }

  downloadAllInsights() {
    this.downloadInProgress = true
    this.getIndicatorDataForCSVDownload().then(
      (insightsData) => {
        // format fields and data for csv
        const fields = ['Insight Category', 'Indicator', 'Seed-to-Audience Coverage Ratio']
        const data = []
        _.forEach(insightsData, (indicators, insightCategory) => {
          _.forEach(indicators, (indItem) => {
            const rowData = [insightCategory, indItem.indicator, indItem.lift]
            data.push(rowData)
          })
        })
        // create csv
        const csv = Papa.unparse({fields, data})
        // kind of janky logic to download the CSV to the user's machine
        const blob = new Blob([csv]);
        const a = window.document.createElement("a");
        a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
        a.download = `insights.csv`;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        this.downloadInProgress = false
      }
    )
  }

  allowDownloadInsights() {
    return this.currentFitModel && this.currentFitModel.faModelId
  }

  getWidth(datum) {
    // the full bar width is only 42% of its container,
    // so we want our width calculation to be relative to that.
    const cssWidthFactor = 0.42
    return `${datum.norm*cssWidthFactor}%`
  }
}

export default AudienceModelInsightsController
