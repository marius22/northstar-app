import SlideController from '../modals/SlideController'
import _ from 'lodash'

const SIMILAR_DOMAINS = 'similarDomains'
const ENRICHED_DOMAINS = 'domains'

class AudienceSummaryController extends SlideController {
  /* @ngInject */
  constructor(close, $document, $timeout, params, audienceService, filterService, alertService) {
    super(close, $document, $timeout)
    this.audience = params.audience
    this.alertService = alertService
    this.filterService = filterService
    this.kwModel = {items: []}
    this.filterCriteria = {}
    this.summaryFilters = {}

    audienceService.getCreationSummary(this.audience.id).then(
      (result) => {
        const criteria = result[0].data.data
        const keywords = result[1].data.data
        this.similarDomains = criteria.similarDomains
        this.domains = criteria.domains
        // If this audience was created with URLs via similar companies by csv or enriched list,
        // the below line allows us to show URLs in the same way we show keywords in the summary.
        this.kwModel.items = this.similarDomains || this.domains ||  keywords
        this.formatFilterCriteria(criteria)
        this.openSlide()
      },
      () => {
        this.alertService.addAlert('error', `Unable to retrieve creation summary data for ${this.audience.name}`)
        this.closeSlide()
      }
    )
  }

  formatFilterCriteria(criteria, parentKey) {
    _.forEach(criteria, (value, key) => {
      const shouldIgnoreNonFilterKey = _.includes([SIMILAR_DOMAINS, ENRICHED_DOMAINS], key)
      if (!shouldIgnoreNonFilterKey) {
        if (Array.isArray(value)) {
          let dispName
          if (parentKey) {
            dispName = `${this.filterService.dispName(parentKey)}: ${key}`
          } else {
            dispName = this.filterService.dispName(key)
          }
          this.filterCriteria[dispName] = value
          this.summaryFilters[dispName] = ''
        } else {
          if (!parentKey) {
            this.formatFilterCriteria(value, key)
          }
        }
      }
    })
  }

  keywordsOrUrlsWereSelected() {
    return !_.isEmpty(this.kwModel.items) || !_.isEmpty(this.similarDomains)
  }

  audienceWasFoundByUrls() {
    return this.similarDomains !== undefined
  }

  audienceWasFromEnrichedList() {
    return this.domains !== undefined
  }

  keywordsOrUrlsTitle() {
    if (this.audienceWasFoundByUrls() || this.audienceWasFromEnrichedList()) {
      return 'URL(s)'
    }
    return 'Keyword(s)'
  }

  hasFilterCriteria() {
    return !_.isEmpty(this.filterCriteria)
  }
}

export default AudienceSummaryController