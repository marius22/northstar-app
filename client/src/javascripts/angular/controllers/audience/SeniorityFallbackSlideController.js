import SlideController from '../modals/SlideController'

import _ from 'lodash'

export default class SeniorityFallbackSlideController extends SlideController {
  /* @ngInject */
  constructor($document, $timeout, publishAudienceService) {
    super(_.noop, $document, $timeout)

    this.pas = publishAudienceService
    this.openSlide()
  }
}
