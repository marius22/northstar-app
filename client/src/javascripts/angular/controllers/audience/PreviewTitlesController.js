import SlideController from '../modals/SlideController'

import _ from 'lodash'
import * as config from 'config'

export default class PreviewTitlesController extends SlideController {
  /* @ngInject */
  constructor($document, $timeout, publishAudienceService, $http) {
    super(_.noop, $document, $timeout)

    this.$http = $http
    this.pas = publishAudienceService

    this.loading = true

    this.messages = {
      inputTitleMsg: `We'll find contacts with titles that match all forms of the titles you've input.`
    }

    const allTitles = this.pas.getValidTitles()
    const exactTitles = allTitles.filter(t => t.match === 'Exact')
    const fuzzyTitles = allTitles.filter(t => t.match === 'Fuzzy')

    this
      .$http({
        method: 'POST',
        url: `${config.apiBaseURL}contacts/title/expand`,
        data: {
          titles: _.map(fuzzyTitles, 'jobTitle'),
        }
      })
      .then(res => {
        this.$timeout(() => {
          this.titles = res.data.data.concat(_.map(exactTitles, 'jobTitle'))
        })
      })
      .finally(() => {
        this.loading = false
      })

    this.openSlide()
  }
}
