import * as animations from 'animations'
import _ from 'lodash'
import $ from 'npm-zepto'
import scroll from 'scroll'
import * as intercom from 'intercom'


const keywordContent = {
  'enter-keywords-intro': 'If you are looking for companies that do predictive marketing, enter "Predictive Marketing".',
  'find-keywords-intro': 'If you are not sure what keywords you want, we will help you find keywords.'
}

class AudienceCreateController {
  /* @ngInject */
  constructor($state,
              showModalService,
              apiBaseUrl,
              $scope,
              audienceService,
              alertService,
              keywordService,
              filterService,
              $filter,
              proxyService,
              userService,
              modelService,
              Upload,
              $http,
              sortService,
              $q) {
    this.sms = showModalService
    this.$state = $state
    this.apiBaseUrl = apiBaseUrl
    this.uploader = Upload
    this.$http = $http
    this.$filter = $filter
    this.$q = $q

    this.audienceService = audienceService
    this.alertService = alertService
    this.$scope = $scope
    this.keywordService = keywordService
    this.filterService = filterService
    this.proxyService = proxyService
    this.userService = userService
    this.modelService = modelService
    this.sortService = sortService
    this.sortService.cancelSort()
    this.accountId = this.userService.accountId()

    // only do init logic and show page if user's permissions allow
    this.enabled = false
    if (this.userService.isInternalAdmin()) {
      this.enabled = true
    } else if (this.creatingExclusion()) {
      this.enabled = true
    } else if (this.isFinding() && this.userService.canFindCompanies()) {
      this.enabled = true
    } else if (this.isEnriching() && this.userService.canScoreList()) {
      this.enabled = true
    } else {
      return
    }

    // We want to limit the csv input to 1000 rows in the similar domains search context
    // for performance.
    if (this.findingByCsv()) {
      this.validationRules = {maxLines: 1000}
    } else {
      this.validationRules = undefined
    }

    this.noResultsFromSearchAttempt = false

    // init filters
    this.filterService.appropriateInit()
    this.filters = this.filterService.getPrimaryFilters()

    // init sort
    this.sortOptions = sortService.getSortOptions({includeRelevance: false})

    // init fit models
    this.selectedModel = {}
    $scope.$watch('acc.selectedModel', () => {
      this.enrichListParams = {
        modelId: this.selectedModel.id,
        justDomains: this.findingByCsv() || this.isEnriching()
      }
    })
    // Should only set model for audience creation, and not exclusion list creation.
    if (this.creatingAudience()) {
      this.setFitModelIfAvailable()
    }

    // init for exclusion list
    this.selectedExcListIds = []
    if (this.viewingResultsAsTable()) {
      this.initResultTable()
    }

    // init sidebar
    this.sbOpen = !this.excludingByCsv() // excludingByCsv() is the only case where there's nothing relevant in the sidebar


    // init company results
    this.searchResults = []

    this.searchLoading = false
    this.pieChartOptions = {
      barColor: '#a0e2a3', // rgb(160,226,163) from our VDL 'Data Visualization' page
      size: 65,
      scaleColor: false,
    }
    this.companyNameFilter = ''

    // init keywords
    this.kwModel = {
      items: []
    }

    this.searchType = () => {
      if (this.findingByKeywords()) {
        return 'companiesByKeywords'
      }
      if (this.findingByUrls()) {
        return 'companiesByDomains'
      }
    }

    this.placeholderText = () => {
      if (this.findingByKeywords() || this.excludingByKeywords()) {
        return 'Search by Keywords or click Find Keywords to discover Keywords'
      }
      if (this.findingByUrls()) {
        return 'Enter URLs of preferred companies'
      }
    }

    this.keywordContent = keywordContent
    this.kwInputModifiedSinceLastSearch = false
    this.filtersModifiedSinceLastSearch = false
    // To help with search information display logic, accounting
    // for the search input being modified after there are already
    // search results from a previous search.
    $scope.$watch('acc.kwModel.items', () => {
      if (this.searchHasConcluded()) {
        this.kwInputModifiedSinceLastSearch = true
      }
    })

    // init score/enrich
    this.readyToEnrich = false
    this.uploadCSVUrl = `${this.apiBaseUrl}audiences/preview`


    // init tutorial
    this.curTutorialStep = 1
    this.totalSteps = 3


    // init listeners for if user leaves page while having work that could be lost
    this.initUserAlertListeners($scope)

    // init selector for scroll-back-to-top button logic
    this.mainContentSelector = '.main-content'

    // init listeners for our infinite scroll logic with lazy-cards
    this.$scope.$on('LazyCards:first_cards', (evt, params) => {
      const {limit, offset} = params
      this.fetchFirstCompanies({limit, offset})
    })

    this.$scope.$on('LazyCards:more_cards', (evt, params) => {
      const {limit, offset} = params
      this.fetchMoreCompanies({limit, offset})
    })
  }

  // This should always be the first call for the first page of a set of companies.
  fetchFirstCompanies({limit, offset}) {
    const params = {
      limit,
      offset
    }
    angular.extend(params, this.loadParams)
    this.getPreviewCompanies(params)
  }

  // After fetchFirstCompanies(), this method is called to fetch subsequent pages.
  // We don't show any loading indicators in this case to simulate an infinite scroll.
  fetchMoreCompanies({limit, offset}) {
    const params = {
      useCriteriaFromCurrentResults: true,
      updateTotal: false,
      useSpinner: this.viewingResultsAsTable(),
      limit,
      offset
    }
    this.getPreviewCompanies(params)
  }

  // Alert user with browser pop-ups on refresh or tab close if the user
  // has done enough unsaved work.
  initUserAlertListeners($scope) {
    const alertUserOnUnsavedChanges = (e) => {
      if (this.hasSearchResults() || this.searchLoading) {
        let confirmationMessage = "";
        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage;                            //Webkit, Safari, Chrome
      }
    }
    window.addEventListener('beforeunload', alertUserOnUnsavedChanges)
    $scope.$on('$destroy', () => {
      window.removeEventListener('beforeunload', alertUserOnUnsavedChanges)
    })

    // logic to alert user with EAP pop-ups on state change
    this.unsubPopups = $scope.$on('$stateChangeStart', (event, toState, toParams) => {
      const timingOut = toState.name === 'timeout'
      const hasEnoughUnsavedWork = this.hasSearchResults() || this.searchLoading
      if (!timingOut && hasEnoughUnsavedWork) {
        event.preventDefault()
        const message = 'You have unsaved changes. </br> Are you sure you want to continue?'
        const title = 'Warning'
        const messageType = 'warning'
        const onConfirm = (result, close) => {
          this.unsubPopups()
          this.$state.go(toState.name, toParams)
          close()
        }
        const params = {message, title, messageType, onConfirm}
        this.sms.confirmationModal(params)
      }
    })
  }

  initResultTable() {
    this.resultTable = {
      header: [
        {text: 'Company Name', dataKey: 'name', sortKey: 'companyName'},
        {text: 'URL', dataKey: 'domain', sortKey: 'domain'}
      ]
    }
    this.rowsPerPage = 100
  }

  // Right now we only use a table for exclusion lists, but that could change.
  viewingResultsAsTable() {
    return this.creatingExclusion()
  }

  shouldShowLoading() {
    if (this.viewingResultsAsTable()) {
      return this.searchLoading || this.pageCountLoading
    } else {
      return this.searchLoading
    }
  }

  selectPageHandler(pageNum) {
    const limit = this.rowsPerPage
    const offset = limit * (pageNum - 1)
    this.prevPage = this.currentPage
    this.currentPage = pageNum

    // We need to differentiate between the user clicking "1" in the pagination controls
    // (in which case we don't want to re-calculate the totalCount) and other contexts
    // in which selectPageHandler(1) is triggered.
    const isManuallyGoingToFirstPage = this.prevPage >= 1 && !this.filtersModifiedSinceLastSearch
    if (pageNum === 1 && !isManuallyGoingToFirstPage) {
      this.fetchFirstCompanies({limit, offset})
    } else {
      this.fetchMoreCompanies({limit, offset})
    }
  }

  reloadTable() {
    this.currentPage = undefined
    if (!this.hasSearchResults()) {
      this.selectPageHandler(1)
    } else {
      this.toFirstPage()
    }
  }

  sortTable(key) {
    const cb = () => {
      this.getPreviewCompaniesWithoutCount()
    }
    this.sortService.rotateSortDir(key, cb)
  }

  fitModelDispName() {
    return this.selectedModel.name || 'NO MODEL'
  }

  excListDispName() {
    const amount = _.isEmpty(this.selectedExcListIds) ? 'None' : this.selectedExcListIds.length
    return `${amount} Selected`
  }

  setFitModelIfAvailable() {
    this
      .modelService.getList(this.userService.accountId())
      .then((fitModels) => {
        const completeModels = _.filter(fitModels, (model) => {
          return this.modelService.isComplete(model)
        })
        const accountFitModel = _.find(completeModels, (model) => {
          return this.modelService.isAccountFit(model)
        })
        // set our fit model to the account fit, and if there isn't one, set it to the first
        // complete model. If there are no complete models then currentFitModel remains set to
        // its initial value of {}
        if (accountFitModel) {
          this.selectedModel = accountFitModel
        } else if (!_.isEmpty(completeModels)) {
          this.selectedModel = completeModels[0]
        }
      })
  }

  showAudienceTutorialStep(step) {
    return (this.findingByKeywords() &&
            this.creatingAudience() &&
            step === this.curTutorialStep &&
            !this.audienceService.hasCreatedAudience())
  }

  nextTutorialStep(tooltipSelector, infoIconSelector) {
    animations.animateTooltip(tooltipSelector, infoIconSelector).then(() => {
      this.curTutorialStep += 1
      this.$scope.$apply()
      if (this.stopTooltipsFlag) {
        this.audienceService.stopShowingTooltips()
      }
    })
  }

  // Are we finding an audience/exclusion list?
  isFinding() {
    return this.$state.includes('audience.find') || this.excludingByKeywords()
  }

  // Are we finding an audience/exclusion by keywords?
  findingByKeywords() {
    return this.$state.is('audience.find.keywords') || this.excludingByKeywords()
  }

  // Are we finding an audience by URLs?
  findingByUrls() {
    return this.$state.is('audience.find.urls')
  }

  // Are we finding an audience by similar domains via CSV?
  findingByCsv() {
    return this.$state.is('audience.find.csv')
  }

  // Are we finding an audience with only filter criteria?
  findingByFilters() {
    return this.$state.is('audience.find.criteria')
  }

  // Are we creating an audience/exclusion list by providing a CSV and
  // using the exact contents of the CSV?
  isEnriching() {
    return this.$state.is('audience.enrich') || this.excludingByCsv()
  }

  // Are we creating an exclusion list by keywords?
  excludingByKeywords() {
    return this.$state.is('exclusion.find.keywords')
  }

  // Are we creating an exclusion list by exact CSV contents?
  excludingByCsv() {
    return this.$state.is('exclusion.enrich') || this.updatingExclusion()
  }

  updatingExclusion() {
    return this.$state.is('exclusion.update')
  }

  creatingAudience() {
    return this.$state.includes('audience')
  }

  creatingExclusion() {
    return this.$state.includes('exclusion')
  }

  // Do we have a big search input where the user searches for stuff and selects "tags" from the
  // search matches?
  findingByTagSearch() {
    return this.findingByKeywords() || this.findingByUrls() || this.excludingByKeywords()
  }

  uploadCsvContext() {
    return this.findingByCsv() || this.isEnriching()
  }

  titleText() {
    if (this.findingByKeywords() && this.creatingAudience()) {
      return 'Find companies using keywords'
    }
    if (this.findingByUrls()) {
      return 'Find similar companies based on preferred companies (URLs)'
    }
    if (this.findingByCsv()) {
      return 'Find similar companies based on preferred companies (list)'
    }
    if (this.findingByFilters()) {
      return 'Find companies using criteria'
    }
    if (this.isEnriching() && this.creatingAudience()) {
      return 'Score List'
    }
    if (this.excludingByKeywords()) {
      return 'Exclusion List using keywords'
    }
    if (this.excludingByCsv()) {
      return 'Upload Exclusion List'
    }
  }

  saveBtnText() {
    if (this.creatingAudience()) {
      return 'Save Audience'
    }
    if (this.creatingExclusion()) {
      return 'Save Exclusion List'
    }
  }

  emptyResultsText() {
    if (this.noResultsFromSearchAttempt) {
      return 'Your search returned no results.'
    }
    if (this.findingByKeywords() || this.excludingByKeywords()) {
      return 'Enter keywords above to see results'
    }
    if (this.findingByUrls()) {
      return 'Enter URLs above to see results'
    }
    if (this.findingByFilters()) {
      return 'Start applying criteria from the categories on the left'
    }
  }

  toggleSidebar() {
    this.sbOpen = !this.sbOpen
  }

  displayPreSearchInformation() {
    if (this.kwInputModifiedSinceLastSearch || this.filtersModifiedSinceLastSearch) {
      return true
    }
    if (this.loadingDueToNameFltrChange || this.nameFilterActive) {
      return false
    }

    return !this.searchHasConcluded()
  }

  hasResultsButChangedSearchInput() {
    return this.displayPreSearchInformation() && this.searchHasConcluded()
  }

  hasAbsolutelyNothing() {
    if (this.isFinding()) {
      return !this.hasSearchResults() && !this.atLeastOneSearchParameter()
    }
    if (this.isEnriching()) {
      return !this.searchHasConcluded()
    }
  }

  atLeastOneSearchParameter() {
    return this.atLeastOneFilter() || this.atLeastOneKeyword()
  }

  hasSearchResults() {
    return !_.isEmpty(this.allCompanies) || !_.isEmpty(this.searchResults)
  }

  searchHasConcluded() {
    return this.hasSearchResults() || this.noResultsFromSearchAttempt
  }

  appropriateText() {
    return this.displayPreSearchInformation() ? 'Your search will use' : `We found ${this.dispTotal()} companies using`
  }

  // this.completeTotal is either a string '(calculating...)', or a number.
  dispTotal() {
    if (typeof this.completeTotal === "number") {
      return this.$filter('number')(this.completeTotal)
    } else {
      return this.completeTotal
    }
  }

  atLeastOneKeyword() {
    return !_.isEmpty(this.kwModel.items)
  }

  clearAllKeywords() {
    const keywordsOrUrls = this.findingByKeywords() ? 'keywords' : 'URLs'
    const message = `This will clear all selected ${keywordsOrUrls}. </br> Do you want to continue?`
    const title = 'Warning'
    const messageType = 'warning'
    const onConfirm = (result, close) => {
      this.kwModel.items = []
      close()
    }
    const params = {message, onConfirm, title, messageType}
    this.sms.confirmationModal(params)
  }

  atLeastOneFilter() {
    return this.filterService.atLeastOneFilter()
  }

  numKeywordsStr() {
    const numKws = this.kwModel.items.length
    const keywordsOrUrls = this.findingByKeywords() ? 'Keyword(s)' : 'URL(s)'
    return `${numKws} ${keywordsOrUrls}`
  }

  findKeywords() {
    const onSave = (kwModelToSave) => {
      const currentKws = this.kwModel.items
      const newKws = kwModelToSave.items
      this.kwModel.items = _.union(currentKws, newKws)
    }

    this.sms.findKeywords({
      kwModel: _.cloneDeep(this.kwModel),
      purpose: 'find',
      onSave
    })
  }

  viewKeywords() {
    this.sms.findKeywords({
      kwModel: _.cloneDeep(this.kwModel),
      purpose: 'view'
    })
  }

  filterAudience(key) {
    const purpose = 'filter'
    const onClose = (changeDetected) => {
      this.filtersModifiedSinceLastSearch = changeDetected
      if (changeDetected) {
        if (this.atLeastOneSearchParameter()) {
          this.getPreviewCompaniesDefault()
        } else {
          this.clearPreviewCompanies()
        }
      }
    }

    this.sms.filterAudience({
      key,
      purpose,
      onClose
    })
  }

  filterSummary() {
    const purpose = 'summary'
    const onClose = (changeDetected) => {
      this.filtersModifiedSinceLastSearch = changeDetected
      if (changeDetected) {
        if (this.atLeastOneSearchParameter()) {
          this.getPreviewCompaniesDefault()
        } else {
          this.clearPreviewCompanies()
        }
      }
    }

    this.sms.filterAudience({
      purpose,
      onClose
    })
  }

  filteringOnCategory(category) {
    return this.filterService.selectedInCategory(category).length > 0
  }

  cancelNameFilter({reload = true}) {
    this.companyNameFilter = ''
    // currentNameFilter is the active company name filter that
    // is influencing the currently displayed search results.
    // Not to be confused with companyNameFilter, which
    // is the ng-model that the search input is connected to.
    this.currentNameFilter = ''
    this.nameFilterActive = false
    if (reload) {
      this.loadingDueToNameFltrChange = true
      // When we cancel the name filter while viewing as table, we already have the
      // count of the full set of result companies from this.completeTotal, so
      // we can instantly update our totalPages here without having to query for count again.
      if (this.viewingResultsAsTable()) {
        this.totalPages = Math.ceil(this.completeTotal/this.rowsPerPage)
      }
      this.getPreviewCompaniesWithoutCount()
    }
  }

  nameSearchOnEnter(e) {
    const keyCode = e.which || e.keyCode
    const enterPressed = keyCode === 13
    if (enterPressed && this.searchHasConcluded()) {
      this.nameFilterActive = true
      this.loadingDueToNameFltrChange = true
      this.getPreviewCompaniesWithoutCount()
    }
  }

  nameSearchPlaceholder() {
    if (this.nameFilterActive && this.currentNameFilter) {
      return `Searching on "${this.currentNameFilter}"`
    } else {
      return 'Search on company name'
    }
  }

  sortByAsc($index) {
    const cb = () => {
      if (this.hasSearchResults()) {
        this.getPreviewCompaniesWithoutCount()
      }
    }
    this.sortService.sortByAsc($index, cb)
  }

  sortByDesc($index) {
    const cb = () => {
      if (this.hasSearchResults()) {
        this.getPreviewCompaniesWithoutCount()
      }
    }
    this.sortService.sortByDesc($index, cb)
  }

  changeSortDir() {
    const cb = () => {
      if (this.hasSearchResults()) {
        this.getPreviewCompaniesWithoutCount()
      }
    }
    this.sortService.changeSortDir(cb)
  }

  getPreviewCompaniesWithoutCount() {
    this.loadParams = {useCriteriaFromCurrentResults: true, updateTotal: false}
    if (this.viewingResultsAsTable()) {
      this.reloadTable()
    } else {
      this.$scope.$broadcast('LazyCards:reset')
    }
  }

  getPreviewCompaniesDefault() {
    this.loadParams = {}
    if (this.viewingResultsAsTable()) {
      this.reloadTable()
    } else {
      this.$scope.$broadcast('LazyCards:reset')
    }
  }

  clearPreviewCompanies() {
    this.noResultsFromSearchAttempt = false
    this.searchResults = []
    this.allCompanies = []
    this.$scope.$broadcast('LazyCards:reset', {
      withoutReload: true
    })
  }

  // If there are already companies in our searchResults, that means that we've used some combination
  // of search criteria (keywords + filters) to find those companies. The ng-models representing those
  // criteria might have changed since the search was initially triggered, so if we want to immediately
  // reload the same results sorted by fit scores from a different fit model, we need to remember the
  // criteria that we used for the current results. This is achieved through this.kwsOfCurrentResults and
  // this.filtersOfCurrentResults, and we can use those instead of the current ng-models if
  // useCriteriaFromCurrentResults = true.
  getPreviewCompanies({
    useCriteriaFromCurrentResults = false,
    updateTotal = true,
    useSpinner = true,
    limit, offset
  }) {
    let formattedCriteria = {}

    if (useCriteriaFromCurrentResults) {
      formattedCriteria = this.criteriaOfCurrentResults
    } else {
      // Add keywords or domains selected in search bar to formatted criteria.
      // In the findingByCsv case, there's no search bar in the UI, but the
      // search bar's ng-model is used out of convenience.
      const usingKwModelToStoreCriteria = this.findingByTagSearch() || this.findingByCsv() || this.isEnriching()
      if (usingKwModelToStoreCriteria) {
        const keywordsOrDomains = this.kwModel.items
        let appropriateKey
        if (this.findingByKeywords()) {
          appropriateKey = 'grams'
        } else if (this.findingByCsv() || this.findingByUrls()) {
          appropriateKey = 'similarDomains'
        } else if (this.isEnriching()) {
          appropriateKey = 'domain'
        }

        formattedCriteria[appropriateKey] = _.cloneDeep(keywordsOrDomains)
      }

      // If our search context allows for searching by keywords or by similar domains, and the user is actually
      // including keyword/similar domain inputs, then we add the appropriate relevance sort option.
      const includeKwRelevance = this.findingByKeywords() && !_.isEmpty(this.kwModel.items)
      const includeDmnRelevance = (this.findingByUrls() || this.findingByCsv()) && !_.isEmpty(this.kwModel.items)
      this.sortOptions = this.sortService.getSortOptions({includeKwRelevance, includeDmnRelevance})

      // add filters to formatted criteria
      const formattedFilters = this.filterService.formatFiltersForAPIs()
      _.extend(formattedCriteria, formattedFilters)
    }

    if (updateTotal) {
      this.cancelNameFilter({reload: false})
    }

    if (this.nameFilterActive) {
      formattedCriteria['companyNameSearch'] = this.companyNameFilter
    }

    const params = {
      criteria: formattedCriteria,
      modelId: this.selectedModel.id,
      exclusionListIds: this.selectedExcListIds,
      limit,
      offset
    }

    const orderValue = this.sortService.getOrderVal()
    if (orderValue) {
      params['orderBy'] = orderValue
    }

    if (useSpinner) {
      this.searchLoading = true
    }

    const getCompaniesUrl = '/common/recommendations/preview'
    const getCountUrl = '/common/recommendations/preview/count'

    if (updateTotal) {
      this.completeTotal = '(calculating...)'
      this.pageCountLoading = true
      this.proxyService.post(getCountUrl, params).then(
        (response) => {
          this.completeTotal = response.data.data.completeTotal
          this.totalPages = Math.ceil(response.data.data.completeTotal/this.rowsPerPage)
          this.pageCountLoading = false
        }
      )
    } else if (this.viewingResultsAsTable() && this.nameFilterActive) {
      // This else if block is to handle the case where we don't want to update this.completeTotal,
      // but we still need to get an accurate total for our table pagination.
      this.pageCountLoading = true
      this.proxyService.post(getCountUrl, params).then(
        (response) => {
          this.totalPages = Math.ceil(response.data.data.completeTotal/this.rowsPerPage)
          this.pageCountLoading = false
        }
      )
    }

    this.proxyService.post(getCompaniesUrl, params).then(
      (response) => {
        this.setSearchResultsWithExpectedFields(response.data.data.companies)
        this.kwInputModifiedSinceLastSearch = false
        this.filtersModifiedSinceLastSearch = false

        this.currentNameFilter = formattedCriteria['companyNameSearch']
        this.criteriaOfCurrentResults = _.cloneDeep(_.omit(formattedCriteria, 'companyNameSearch'))
        // for easily giving audience-company-info access to current search tags
        const currentSearchTags = formattedCriteria['grams'] || formattedCriteria['similarDomains']
        this.audienceService.setCurrentSimilarityItems(currentSearchTags)

        this.searchLoading = false
        // for comp name filter edge case
        this.loadingDueToNameFltrChange = false
      },
      (err) => {
        if (!err.wasCanceled) {
          this.searchLoading = false
          // for comp name filter edge case
          this.loadingDueToNameFltrChange = false
          this.alertService.addAlert('error', 'Unable to fetch search results. Please contact support and try again later.')
        }
      }
    )
  }

  setSearchResultsWithExpectedFields(companies) {
    const possiblyEmptyResults = companies.map((c) => {
      // Hardcode some parameters to account for API format differences
      // Round fit score up for consistency
      c.fitScorePercent = c.score ? Math.round(c.score * 100) : '--'
      c.contactCount = c.numContacts
      c.revenueRange = c.revenue

      return c
    })

    this.noResultsFromSearchAttempt = _.isEmpty(possiblyEmptyResults)
    this.searchResults = this.viewingResultsAsTable() ? companies : possiblyEmptyResults
  }

  saveOnclick() {
    if (this.updatingExclusion()) {
      this.openUpdateModal()
    } else {
      this.openSaveModal()
    }
  }

  openSaveModal() {
    const onConfirm = (results, close) => {
      close()
      this.saveAudience(results.name, results.description)
    }
    this.sms.audienceNameAndDesc({onConfirm})
  }

  openUpdateModal() {
    const onConfirm = (results, close) => {
      close()
      this.updateExcList()
    }
    const message = `Uploading a new list will overwrite and replace this list.
                     <span>If you have audiences using this list, it may take some time
                     for their total company counts to update.</span>
                     <b>Are you sure you want to continue?</b>`
    const params = {
      title: 'Warning',
      message,
      messageType: 'warning',
      onConfirm
    }

    this.sms.confirmationModal(params)
  }

  updateExcList() {
    this.saveAudienceInProgress = true
    const excListId = this.$state.params.excListId
    const file = this.csvFile
    this.audienceService
      .updateExclusionCompanies(excListId, file)
      .then((response) => {
        const excList = response.data.data
        this.unsubPopups()
        this.$state.go('exclusion.console')
        this.alertService.addAlert('success', `Exclusion List ${excList.name} updated successfully.`)
        this.saveAudienceInProgress = false
      })
  }

  saveAudience(name, description) {
    this.saveAudienceInProgress = true

    const criteria = this.isFinding() ? this.criteriaOfCurrentResults : {}
    const modelId = this.selectedModel.id || -1

    const type = this.isFinding() ? 'found' : 'enriched'
    const metaType = this.creatingAudience() ? 'audience' : 'exclusion'
    const exclusionListIds = this.selectedExcListIds
    const file = this.csvFile

    this
      .audienceService
      .createAudience(name, description, criteria, type, file, modelId, metaType, exclusionListIds)
      .then((response) => {
        const audienceId = response.data.data.id

        // There are only criteria for audiences made through the "find" flow.
        if (this.isFinding()) {
          const saveKeywords = this.findingByKeywords()
          this.audienceService.saveAudienceCriteria(audienceId, criteria, saveKeywords)
        }
        this.unsubPopups()
        if (this.creatingAudience()) {
          this.$state.go('audience.console')
          this.alertService.addAlert('success', `Audience ${name} created.`)
        }
        if (this.creatingExclusion()) {
          this.$state.go('exclusion.console')
          this.alertService.addAlert('success', `Exclusion List ${name} created.`)
        }

        intercom.Intercom.track(intercom.CONSTANTS.AUDIENCE_CREATED)
      })
      .catch((err) => {
        this.alertService.addAlert('error', `${err.data.message}`)
      })
      .finally(() => {
        this.saveAudienceInProgress = false
      })
  }

  getFitPercentage(fitscore) {
    if (fitscore) {
      return Math.floor(fitscore * 100)
    } else {
      return '--'
    }
  }

  shouldDisplayUploader() {
    return this.uploadCsvContext() && !this.searchHasConcluded() && !this.searchLoading
  }

  reuploadCSV() {
    const onConfirm = (result, close) => {
      this.completeTotal = undefined
      this.clearPreviewCompanies()
      this.readyToEnrich = false
      this.csvFile = undefined
      this.kwModel.items = []
      close()
    }
    const message = 'Your results will be lost. </br> Are you sure you want to continue?'
    const title = 'Warning'
    const messageType = 'warning'

    const params = {
      onConfirm,
      message,
      title,
      messageType
    }
    this.sms.confirmationModal(params)
  }

  /*
    This method handles two cases: Similar Companies by CSV, and Enrich My List.
    For Similar Companies by CSV, response contains an array of domains. We check
    to see if there's 50
  */
  handleUploadCSVResponse(response, csvFile) {
    if (this.isEnriching()) {
      this.csvFile = csvFile
      this.kwModel.items = response.data.data || []
      this.getPreviewCompaniesDefault()
    }
    if (this.findingByCsv()) {
      // use the same field that we'd use to hold urls in
      // Find Companies by URLs; the CSV is just a way to
      // input many URLs quickly
      this.kwModel.items = response.data.data || []
      if (this.kwModel.items.length >= 50) {
        this.openOfferModelRunModal(csvFile)
      }
      this.getPreviewCompaniesDefault()
    }
  }

  openOfferModelRunModal(csv) {
    const onConfirm = (result, close) => {
      if (result.buildAModel) {
        this.uploader.upload({
          url: `${this.apiBaseUrl}accounts/${this.accountId}/fit_models/create_by_csv`,
          data: {
            file: csv,
            modelName: result.name
          }
        })
        .then(() => {
          this.alertService.addAlert('success', 'Model build in progress.')
        })
      }
      close()
    }
    this.sms.modelFromList({onConfirm})
  }

  buildNewModel() {
    const onFinish = (selectedModel) => {
      this.selectedModel = selectedModel
      if (this.searchHasConcluded()) {
        this.getPreviewCompaniesDefault()
      }
    }

    this.sms.buildNewModel({
      onFinish,
      currentModelId: this.selectedModel.id
    })
  }

  chooseExcLists() {
    const onFinish = (selectedExcListIds) => {
      this.selectedExcListIds = selectedExcListIds
      if (this.searchHasConcluded()) {
        this.getPreviewCompaniesDefault()
      }
      return this.$q.resolve()
    }

    this.audienceService
      .getExcLists()
      .then((response) => {
        const allLists = response.data.data
        this.sms.chooseExclusion({
          onFinish,
          allLists,
          selectedExcListIds: this.selectedExcListIds
        })
      })
  }

  stopProp(ev) {
    ev.stopPropagation()
  }

  toExcListCompanies() {
    const excListId = this.$state.params.excListId
    this.$state.go('exclusion.company', {audienceId: excListId})
  }
}

export default AudienceCreateController
