import SlideController from '../modals/SlideController'
import _ from 'lodash'

const inboundTargetsDispMap = {
  'account': 'Accounts',
  'lead': 'Leads',
  'contact': 'Contacts'
}

class AudienceSettingsController extends SlideController {
  /* @ngInject */
  constructor(close,
              $document,
              $timeout,
              params,
              userService,
              modelService,
              audienceService,
              alertService,
              apiBaseUrl,
              $http) {
    super(close, $document, $timeout)
    this.audience = _.cloneDeep(params.audience)
    this.refreshConsole = params.refreshConsole
    this.apiBaseUrl = apiBaseUrl
    this.userService = userService
    this.accountId = userService.accountId()
    this.modelService = modelService
    this.audienceService = audienceService
    this.alertService = alertService
    this.$http = $http
    this.editingAudience = false
    this.setCharactersLeft()
    this.initChangeOwnerData()
    this.initChangeDefaultModelData()
    this.initRealTimeScoringData()
    this.openSlide()
  }

  toggleEditing() {
    this.editingAudience = !this.editingAudience
  }

  setCharactersLeft() {
    // Number of characters left in the description
    Object.defineProperty(this, 'charactersLeft', {
      get() {
        return 1000 - this.audience.description.length
      }
    })
  }

  saveAndClose() {
    if (!this.userService.scoringEnabled() && this.audience.realTimeScoringEnabled) {
      this.alertService.addAlert('error', 'Not allowed to enable Real Time Scoring.', '#audience-settings-slide-alert')
      return
    }

    const audienceUpdateSuccess = (response) => {
      const updatedAudience = response.data.data
      const changingDefaultModel = this.selectedModel && (this.audience.modelId !== this.selectedModel.id)

      const refreshAndClose = () => {
        this.refreshConsole()
          .then(() => {
            this.saving = false
            this.alertService.addAlert('success', `Audience ${updatedAudience.name} updated.`)
            this.closeSlide()
          })
      }

      if (changingDefaultModel) {
        // Make extra api call to update fit model
        this.audienceService.updateFitModel(this.audience.id, this.selectedModel.id).then(
          () => {
            // Everything updated, so we're entirely successful!
            refreshAndClose()
          },
          () => {
            // Everything but fit model updated successfully
            this.refreshConsole()
              .then(() => {
                this.saving = false
                const msg = `Audience ${updatedAudience.name} updated, but could not update default model.`
                this.alertService.addAlert('warning', msg)
                this.closeSlide()
              })
          }
        )
      } else {
        // Didn't make an extra api call to update fit, so we're entirely successful!
        refreshAndClose()
      }
    }

    const audienceUpdateFailure = () => {
      this.alertService.addAlert('error', 'Audience changes failed to save.', '#audience-settings-slide-alert')
      this.saving = false
    }

    this.audience.userId = this.selectedOwner.id
    this.saving = true
    this.audienceService
      .updateAudience(this.audience)
      .then(
        audienceUpdateSuccess,
        audienceUpdateFailure
      )
  }

  initChangeOwnerData() {
    this.userService.getUsersByMyAccount().then(
      (response) => {
        const usersInAccount = response.data.data
        this.possibleOwners = _.map(usersInAccount, (user) => {
          let name
          if (user.lastName && user.firstName) {
            name = `${user.lastName}, ${user.firstName} (${user.email})`
          } else if (user.firstName) {
            name = `${user.firstName} (${user.email})`
          } else {
            name = user.email
          }
          return {
            name,
            id: user.id,
            // dropdown directive relies on the 'key' field to remove the currently selected item
            // from the dropdown options
            key: `${user.id}`
          }
        })

        this.selectedOwner = _.find(this.possibleOwners, (possibleOwner) => {
          return possibleOwner.id === this.audience.userId
        })
      }
    )
  }

  initChangeDefaultModelData() {
    this.modelService.getList(this.accountId)
      .then((fitModels) => {
        // allow only completed models to appear in the dropdown, and format them
        // for the dropdown directive
        this.possibleModels = _(fitModels)
          .filter(model => {
            return model.status === 'COMPLETED'
          })
          .map(model => {
            return {
              name: model.name,
              id: model.id,
              key: `${model.id}`
            }
          })
          .value()

        this.selectedModel = _.find(this.possibleModels, (possibleModel) => {
          return possibleModel.id === this.audience.modelId
        })
      })
  }

  initRealTimeScoringData() {
    this.currentInboundLeadsTargets = _.keys(_.pickBy(this.userService.getInboundLeadsTargets()))
    this.realtimeScoring = this.userService.getRealtimeScoringMode()
  }

  dispInboundTargets() {
    let dispStr = ''
    let count = 0
    _.forEach(this.currentInboundLeadsTargets, (target) => {
      const targetDisp = inboundTargetsDispMap[target]
      if (count === 0) {
        dispStr = targetDisp
      } else if (count === this.currentInboundLeadsTargets.length - 1) {
        // account for oxford comma.
        if (count === 1) {
          dispStr = `${dispStr} and ${targetDisp}`
        } else {
          dispStr = `${dispStr}, and ${targetDisp}`
        }
      } else {
        dispStr = `${dispStr}, ${targetDisp}`
      }
      count++
    })
    return dispStr
  }

  // Note that disabled doesn't necessarily mean that the switch is "off", just unchangeable.
  // If we're scoringGlobally(), then real-time scoring is on but controls here are disabled.
  shouldDisableControls() {
    return this.userService.scoringGlobally() || this.userService.scoringOff() || !this.userService.scoringEnabled()
  }

  disabledReason() {
    if (!this.userService.scoringEnabled()) {
      return 'Contact EverString support to enable.'
    }
    if (this.userService.scoringOff()) {
      return 'Real Time Scoring disabled globally. This can be changed by an account admin.'
    }
    if (this.userService.scoringGlobally()) {
      return 'Real Time Scoring enabled globally. This can be changed by an account admin.'
    }
  }

  scoringSwitchOn() {
    if (this.userService.scoringGlobally()) {
      return true
    }
    if (this.userService.scoringOff()) {
      return false
    }
    return this.audience.realTimeScoringEnabled
  }

  // showInSfdc and isPublishedSfdc are exclusive settings
  showInSfdcChanged() {
    if (this.audience.showInSfdc) {
      this.audience.isPublishedSfdc = false
    }
  }

  isPublishedSfdcChanged() {
    if (this.audience.isPublishedSfdc) {
      this.audience.showInSfdc = false
    }
  }

}

export default AudienceSettingsController