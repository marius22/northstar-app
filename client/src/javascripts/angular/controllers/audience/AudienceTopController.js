class AudienceTopController {
  /* @ngInject */
  constructor($state) {
    this.$state = $state
  }

  shouldShowTabs() {
    return this.$state.is('audience.console') || this.$state.is('audience.galaxy')
  }
}

export default AudienceTopController
