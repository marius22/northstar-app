import _ from 'lodash'
import angular from 'angular'
import {audiencePublishMethods} from '../../services/publish-audience-service'
import Papa from 'papaparse'

class AudienceCompanyController {
  /* @ngInject */
  constructor($http, apiBaseUrl, keywordService, audienceService,
              $state, $scope, userService, $filter, observeOnScope,
              showModalService, filterService, modelService,
              publishAudienceService, $q, $timeout, sortService) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.companiesLoading = false
    this.keywordService = keywordService
    this.$state = $state
    this.audienceService = audienceService
    this.sortService = sortService
    this.sortService.cancelSort()
    this.userService = userService
    this.modelService = modelService
    this.$scope = $scope
    this.$filter = $filter
    this.$timeout = $timeout
    this.observeOnScope = observeOnScope
    this.sms = showModalService
    this.filterService = filterService
    this.filterService.appropriateInit()
    this.pas = publishAudienceService
    this.$q = $q

    /* State */
    // Search bar query text.
    this.$scope.companyQuery = ''

    // Array of a single company batch that we give to the lazy-cards directive for rendering.
    // Includes all filters.
    this.filteredCompanies = []

    // The current fit model associated with this audience. This is the fit model saved in our
    // database and the fit model we use when publishing scores.
    this.currentFitModel = {}

    // All fit models that we could save to this audience
    this.fitModels = []

    // Current set of things we're filtering on.
    this.currentFilterCriteria = {}

    // The set of things we can sort on. (Score, Name, etc.)
    this.sortOptions = this.sortService.getSortOptions()

    // The parameters we pass to `getAudienceCompanies()` to fetch more cards
    this.loadParams = {}

    // When lazy-cards makes a request for the first set of companies, we fetch them.
    this.$scope.$on('LazyCards:first_cards', (evt, params) => {
      const {limit, offset} = params
      this.fetchFirstCompanies({limit, offset})
    })

    // When lazy-cards makes a request for another page of companies, we fetch them here.
    this.$scope.$on('LazyCards:more_cards', (evt, params) => {
      const {limit, offset} = params
      this.fetchMoreCompanies({limit, offset})
    })

    if (this.isAudCompanies()) {
      this.initAudCompanies()
    } else if (this.isExcCompanies()) {
      this.initExcCompanies()
    }
  }

  /**
   * Init async information
   */
  initAudCompanies() {
    // Get fit models so we can save a new fit model to this audience if we want.
    this.companiesLoading = true
    const audienceId = this.$state.params.audienceId
    const audienceP = this.audienceService
                      .getAudienceById(audienceId)
                      .then((audience) => {
                        this.audience = audience
                      })

    const fitModelsP = audienceP.then(() => {
      return this.getFitModels()
    })

    const audienceServiceP = audienceP.then(() => {

      this.pas.initialize(
        this.audience,
        this.getMaxCompaniesToPublish(),
        this.filterService.formatFiltersForAPIs())
    })

    Promise.all([fitModelsP, audienceServiceP]).then(() => {
      this.inited = true
      this.$scope.$broadcast('LazyCards:reset')
    })

    this.audienceService.setSimilarityItemsFromCreationSummary(audienceId)
  }

  initExcCompanies() {
    this.companiesTable = {
      header: [
        {text: 'Company Name', dataKey: 'companyName', sortKey: 'companyName'},
        {text: 'URL', dataKey: 'domain', sortKey: 'domain'}
      ]
    }

    this.rowsPerPage = 100

    this.audienceService
      .getAudienceById(this.$state.params.audienceId)
      .then((audience) => {
          this.inited = true
          this.audience = audience
          this.totalPages = Math.ceil(this.audience.recommendationNum/this.rowsPerPage)
          this.companiesLoading = true
          this.reloadTable()
        })
  }

  viewingResultsAsTable() {
    return this.isExcCompanies()
  }

  reloadTable() {
    // toFirstPage comes from the pagination directive, which may not be fully initialized
    // when reloadTable is called for the first time
    if (this.toFirstPage) {
      this.toFirstPage()
    } else {
      this.selectPageHandler(1)
    }
  }

  sortTable(key) {
    const cb = () => {
      this.getAudienceCompaniesWithoutCount()
    }
    this.sortService.rotateSortDir(key, cb)
  }

  selectPageHandler(pageNum) {
    const limit = this.rowsPerPage
    const offset = limit * (pageNum - 1)
    if (pageNum === 1) {
      this.fetchFirstCompanies({limit, offset})
    } else {
      this.fetchMoreCompanies({limit, offset})
    }
  }

  isExcCompanies() {
    return this.$state.is('exclusion.company')
  }

  isAudCompanies() {
    return this.$state.is('audience.company')
  }

  canReupload() {
    return this.isExcCompanies() && this.inited && this.audience.sourceType === 'enriched'
  }

  /**
   * Get the maximum number of companies we're allowed to publish
   */
  getMaxCompaniesToPublish() {
    let maxCompaniesToPublish
    if (this.totalFiltered) {
      maxCompaniesToPublish = this.totalFiltered
    } else {
      maxCompaniesToPublish = this.audience.recommendationNum
    }

    return maxCompaniesToPublish
  }

  fetchFirstCompanies({limit, offset}) {
    const params = {limit, offset}
    angular.extend(params, this.loadParams)
    this.getAudienceCompanies(params)
  }

  fetchMoreCompanies({limit, offset}) {
    const params = {
      limit,
      offset,
      useCriteriaOfCurrentResults: true,
      useSpinner: this.viewingResultsAsTable(),
      updateTotal: false
    }
    this.getAudienceCompanies(params)
  }

  getAudienceCompaniesDefault() {
    this.loadParams = {}
    this.$scope.$broadcast('LazyCards:reset')
  }

  getAudienceCompaniesWithoutCount() {
    this.loadParams = {useCriteriaOfCurrentResults: true, updateTotal: false}
    if (this.viewingResultsAsTable()) {
      this.reloadTable()
    } else {
      this.$scope.$broadcast('LazyCards:reset')
    }
  }

  /**
   * Fetch more companies
   *
   * @param limit - The limit parameter passed in when fetching companies. The number of companies
   *   per page.
   * @param offset - The offset parameter passed in when fetching companies.
   * @param useCriteriaOfCurrentResults - Whether to use `this.currentFilterCriteria`. If false,
   *   we use the filter service's criteria.
   * @param useSpinner - Whether to use the loading spinner that covers all the cards. If true,
   *   the cards are covered by the spinner. We only want to use this for fetching the first page.
   * @param updateTotal - Whether to update the total number of filtered companies.
   */
  getAudienceCompanies({
    limit,
    offset,
    useCriteriaOfCurrentResults = false,
    useSpinner = true,
    updateTotal = true
  }) {
    if (useSpinner) {
      this.companiesLoading = true
    }

    const audienceId = this.$state.params.audienceId
    const criteria = useCriteriaOfCurrentResults ? this.currentFilterCriteria : this.filterService.formatFiltersForAPIs()

    if (updateTotal) {
      this.cancelNameFilter({reload: false})
      if (!_.isEmpty(criteria)) {
        this
          .audienceService
          .getAudienceCompaniesCount(audienceId, criteria)
          .then((count) => {
            this.totalFiltered = count
          })
      } else {
        this.totalFiltered = undefined
      }
    } else if (this.viewingResultsAsTable() && this.nameFilterActive) {
      this.pageCountLoading = true
      this
        .audienceService
        .getExclusionCompaniesCount(audienceId, criteria)
        .then((count) => {
          // can't let this get set to 0 here. Otherwise, it screws up
          // the pagination directive by preventing it from
          // triggering this.selectPageHandler()
          this.totalPages = Math.ceil(count/this.rowsPerPage) || 1
          this.pageCountLoading = false
        })
    }

    if (this.nameFilterActive) {
      criteria['companyNameSearch'] = this.companyNameFilter
    }

    const orderVal = this.sortService.getOrderVal()

    let getCompsP
    if (this.isAudCompanies()) {
      getCompsP = this.audienceService.getAudienceCompanies(audienceId, offset, limit, criteria, orderVal)
    }
    if (this.isExcCompanies()) {
      getCompsP = this.audienceService.getExclusionCompanies(audienceId, offset, limit, criteria, orderVal)
    }

    getCompsP
      .then((companies) => {
        this.filteredCompanies = companies
        this.currentNameFilter = criteria['companyNameSearch']
        this.currentFilterCriteria = _.omit(criteria, 'companyNameSearch')
      })
      .finally(() => {
        this.companiesLoading = false
      })
  }

  getFitModels() {
    return this
      .modelService.getList(this.userService.accountId())
      .then((fitModels) => {
        this.fitModels = fitModels
        this.currentFitModel = this.fitModels.filter((m) => m.id === this.audience.modelId)[0] || {}
      })
  }

  updateFitModel(modelId) {
    // Models that are incomplete can't be used
    const model = _.find(this.fitModels, (m) => {
      return m.id === modelId
    })
    const currentlySelectedOrIncomplete = model.id === this.currentFitModel.id || model.status !== 'COMPLETED'
    if (currentlySelectedOrIncomplete) {
      return false
    }

    this.companiesLoading = true
    return this
      .audienceService
      .updateFitModel(this.audience.id, modelId)
      // After updating the fit model, we need to reload the fit models/companies of this page
      .then(() => {
        this.initAudCompanies()
      })
  }

  filterAudience() {
    const purpose = 'filter'
    const onClose = (changeDetected) => {
      if (changeDetected) {
        this.getAudienceCompaniesDefault()
      }
    }

    const params = {purpose, onClose}
    this.sms.filterAudience(params)
  }

  selectPublishMethod(method) {
    if (!_.find(audiencePublishMethods, (m) => m.key === method.key && m.enabled)) {
      return false
    }

    this.sms.publishAudience({
      audience: this.audience,
      numCompanies: this.getMaxCompaniesToPublish(),
      publishMethod: method,
      criteria: this.filterService.formatFiltersForAPIs()
    })
  }

  dispFilterTotal() {
    if (this.filterService.atLeastOneFilter()) {
      if (this.totalFiltered) {
        return this.$filter('number')(this.totalFiltered)
      } else {
        return 'Calculating...'
      }
    }

    return '--'
  }

  sortByAsc($index) {
    const cb = () => {
      this.getAudienceCompaniesWithoutCount()
    }
    this.sortService.sortByAsc($index, cb)
  }

  sortByDesc($index) {
    const cb = () => {
      this.getAudienceCompaniesWithoutCount()
    }
    this.sortService.sortByDesc($index, cb)
  }

  changeSortDir() {
    const cb = () => {
      this.getAudienceCompaniesWithoutCount()
    }
    this.sortService.changeSortDir(cb)
  }

  cancelNameFilter({reload = true}) {
    this.companyNameFilter = ''
    // currentNameFilter is the active company name filter that
    // is influencing the currently displayed search results.
    // Not to be confused with companyNameFilter, which
    // is the ng-model that the search input is connected to.
    this.currentNameFilter = ''
    this.nameFilterActive = false
    if (reload) {
      if (this.viewingResultsAsTable()) {
        this.totalPages = Math.ceil(this.audience.recommendationNum/this.rowsPerPage)
      }
      this.getAudienceCompaniesWithoutCount()
    }
  }

  nameSearchOnEnter(e) {
    const keyCode = e.which || e.keyCode
    const enterPressed = keyCode === 13
    if (enterPressed) {
      this.nameFilterActive = true
      this.getAudienceCompaniesWithoutCount()
    }
  }

  nameSearchPlaceholder() {
    if (this.nameFilterActive && this.currentNameFilter) {
      return `Searching on "${this.currentNameFilter}"`
    } else {
      return 'Search on company name'
    }
  }

  updateExclusionList() {
    const excListId = this.$state.params.audienceId
    this.$state.go('exclusion.update', {excListId})
  }

  downloadExclusionList() {
    const excListId = this.$state.params.audienceId
    const offset = 0
    const limit = this.audience.recommendationNum
    const criteria = {}
    const orderBy = 'companyName'
    this.downloadingList = true
    this
      .audienceService
      .getExclusionCompanies(excListId, offset, limit, criteria, orderBy)
      .then((companies) => {
        const fields = ['company', 'url']
        const data = _.map(companies, (c) => {
          return [c.companyName, c.domain]
        })
        const excList = Papa.unparse({fields, data})
        const blob = new Blob([excList]);
        const a = window.document.createElement("a");
        a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
        a.download = `${this.audience.name}.csv`
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        this.downloadingList = false
      })
  }

  refreshCount() {
    const audienceId = this.audience.id
    this.refreshingCount = true
    this
      .audienceService
      .refreshAudienceCount(audienceId)
      .then((response) => {
        const countData = response.data.data
        _.extend(this.audience, countData)
      })
      .finally(() => {
        this.refreshingCount = false
      })
  }
}

export default AudienceCompanyController
