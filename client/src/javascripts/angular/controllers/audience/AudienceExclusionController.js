import _ from 'lodash'
import SlideController from '../modals/SlideController'

class AudienceExclusionController extends SlideController {
  /* @ngInject */
  constructor(close, $document, $timeout, params) {
    super(close, $document, $timeout)
    this.allLists = params.allLists
    this.selectedExcListIds = params.selectedExcListIds
    this.onFinish = params.onFinish
    this.openSlide()
  }

  // format multi-select data.
  // init() is called in SlideController's implementation of openSlide()
  init() {
    const msData = _.map(this.allLists, (list) => {
      return {
        id: list.id,
        text: list.name,
        selected: _.includes(this.selectedExcListIds, list.id)
      }
    })

    this.msDataSrc = {
      'exclusion': msData
    }
  }

  saveAndClose() {
    const selectedIds = _(this.msDataSrc.exclusion)
                          .filter(datum => {
                            return datum.selected
                          })
                          .map(datum => {
                            return datum.id
                          })
                          .value()
    this.saving = true
    this.onFinish(selectedIds)
      .then(() => {
        this.saving = false
        this.closeSlide()
      })
  }
}

export default AudienceExclusionController