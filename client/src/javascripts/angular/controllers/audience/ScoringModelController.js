import SlideController from '../modals/SlideController'
import _ from 'lodash'

class ScoringModelController extends SlideController {
  /* @ngInject */
  constructor(close,
              $document,
              $timeout,
              params,
              userService,
              showModalService,
              modelService) {
    super(close, $document, $timeout)
    this.onFinish = params.onFinish
    this.currentModelId = params.currentModelId
    this.accountId = userService.accountId()
    this.userId = userService.userId()
    this.userService = userService
    this.sms = showModalService
    this.modelService = modelService
    this.type = 'custom'
    this.data = []
    this.selected = {}
    this.openSlide()
    this.init()
  }

  init() {
    this.getList()
  }

  getList() {
    this.loading = true
    this.modelService.getList(this.accountId).then((data) => {
      this.loading = false
      this.data = data
      this.data.forEach((item) => {
        if (this.currentModelId === item.id) {
          this.selected = item
        }
      })
    }, () => {
      this.loading = false
    })
  }

  select(item) {
    if (item.status === 'COMPLETED') {
      this.selected = item
    }
  }

  showTab(type) {
    this.type = type
  }

  customModel() {
    this.sms.customModelModal({
      onSaveClose: this.getList.bind(this),
      currentModelNames: _.map(this.data, (model) => {
        return angular.lowercase(model.name)
      })
    })
  }

  saveAndClose() {
    this.closeSlide()
    this.onFinish(this.selected)
  }

  canSaveAndClose() {
    return this.selected && this.selected.status === 'COMPLETED' && this.selected.id !== this.currentModelId
  }

  deleteModelConfirm($event, model) {
    // prevent click event from selecting the model
    $event.stopPropagation()
    const modalMessage = `You are about to permanently delete this model. <br />
        <b>Are you sure you want to delete ${model.name}?</b>`
    const modalParams = {
      title: 'Warning',
      message: modalMessage,
      messageType: 'warning',
      buttonText: {
        confirm: 'Confirm',
        deny: 'Cancel'
      },
      onConfirm: (result, close) => {
        this.deleteModel(model)
        close()
      }
    }
    this.sms.confirmationModal(modalParams)
  }

  canDelete(model) {
    // allow internal admins to delete models regardless of status
    if (this.userService.isInternalAdmin()) {
      return true
    }
    // disallow account users/admins from deleting any incomplete models
    if (model.status !== 'COMPLETED') {
      return false
    }
    return this.userId === model.ownerId
  }

  deleteModel(model) {
    this.modelService.delete(this.accountId, model.id).then(
      (response) => {
        this.getList()
      }
    )
  }
}

export default ScoringModelController
