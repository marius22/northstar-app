class AudienceInsightsController {
  /* @ngInject */
  constructor(audienceService,
              $state) {
    this.audienceService = audienceService
    this.audienceId = $state.params.audienceId
    this.init()
  }

  init() {
    this.location = []
    this.industries = []
    this.employees = []
    this.revenue = []
    this.fitScore = []
    this.loading = true

    const keys = 'industry,state,employeeSize,revenue,fitScore'
    this.audienceService.getAudienceCountsAsync(this.audienceId, keys, false).then(() => {
      const data = this.audienceService.getAudienceCounts()
      this.location = data['state']
      this.industry = data['industry']
      this.employees = data['employeeSize']
      this.revenue = data['revenue']
      this.fitScore = data['fitScore']
    })['finally'](() => {
      this.loading = false
    })
  }

}

export default AudienceInsightsController
