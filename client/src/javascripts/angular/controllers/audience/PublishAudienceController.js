import {SALESFORCE} from 'enums'

import SlideController from '../modals/SlideController'
import _ from 'lodash'

const MAX_COMPANIES_SHOW_SUMMARY = 10000

const PUBLISH_AUDIENCE_STEPS = {
  // Companies or companies and contacts
  WHAT_TO_PUBLISH: 'WHAT_TO_PUBLISH',
  // Marketo-only step. Choose what marketo environment to publish to (production or sandbox)
  // and whether to publish new or existing.
  SELECT_CRITERIA: 'SELECT_CRITERIA',
  // Select what fields on a company to publish (SFDC/CSV only)
  SELECT_FIELDS: 'SELECT_FIELDS',
  // Choose what marketo list to publish (marketo only)
  CHOOSE_LIST: 'CHOOSE_LIST',
  // Choose the Marketo list to dedupe against.
  APPLY_DEDUPE_LIST: 'APPLY_DEDUPE_LIST',
  SELECT_SCORING_LIST: 'SELECT_SCORING_LIST',
  // Dedupe preferences (SFDC/CSV only)
  PUBLISH_PREFERENCES: 'PUBLISH_PREFERENCES',
  // What contacts and job titles to publish (Only if we choose to publish contacts)
  TITLES_CONTACTS: 'TITLES_CONTACTS',
  // Summary of what you're about to publish.
  SUMMARY: 'SUMMARY'
}


// Mapping of step name to data to show in the nav bar.
const AUDIENCE_NAV_CONFIG = {
  // Companies or companies and contacts
  [PUBLISH_AUDIENCE_STEPS.WHAT_TO_PUBLISH]: {
    title: 'What to Publish',
    tip: 'What and how many?'
  },
  [PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA]: {
    title: 'Select Criteria',
    tip: 'Where and how many?'
  },
  // Select what fields on a company to publish (SFDC/CSV only)
  [PUBLISH_AUDIENCE_STEPS.SELECT_FIELDS]: {
    title: 'Select Fields',
    tip: 'Select what fields to publish'
  },
  // Choose what marketo list to publish (marketo only)
  [PUBLISH_AUDIENCE_STEPS.CHOOSE_LIST]: {
    title: 'Choose List',
    tip: 'Select a list',
  },
  [PUBLISH_AUDIENCE_STEPS.APPLY_DEDUPE_LIST]: {
    title: 'Apply Dedupe List',
    tip: 'How shall we dedupe?'
  },
  [PUBLISH_AUDIENCE_STEPS.SELECT_SCORING_LIST]: {
    title: 'Select List to Score',
    tip: 'How shall we score?'
  },
  // Dedupe preferences (SFDC/CSV only)
  [PUBLISH_AUDIENCE_STEPS.PUBLISH_PREFERENCES]: {
    title: 'Publish Preferences',
    tip: 'Choose what and where to publish'
  },
  // What contacts and job titles to publish (Only if we choose to publish contacts)
  [PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS]: {
    title: 'Titles & Contacts',
    tip: 'Enter contacts & titles'
  },
  // Summary of what you're about to publish.
  [PUBLISH_AUDIENCE_STEPS.SUMMARY]: {
    title: 'Summary',
    tip: 'Publish summary'
  }
}

const DEDUPE_STEP_NAMES = {
  COMPARE_AGAINST_WHAT: 'COMPARE_AGAINST_WHAT',
  PUBLISH_COMPANIES: 'PUBLISH_COMPANIES',
  PUBLISH_CONTACTS: 'PUBLISH_CONTACTS'
}


class PublishAudienceController extends SlideController {
  /* @ngInject */
  constructor($document, $timeout, $http, apiBaseUrl, close, alertService,
              showModalService, ModalService, params, $q,
              publishAudienceService, userService, observeOnScope, $scope,
              audienceService) {

    super(close, $document, $timeout)

    this.$timeout = $timeout
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.alertService = alertService
    this.showModalService = showModalService
    this.sms = showModalService
    this.modalService = ModalService
    this.$q = $q
    this.userService = userService
    this.observeOnScope = observeOnScope
    this.audienceService = audienceService

    this.pas = publishAudienceService
    $scope.pas = this.pas
    this.PUBLISH_AUDIENCE_STEPS = PUBLISH_AUDIENCE_STEPS
    this.DEDUPE_STEP_NAMES = DEDUPE_STEP_NAMES
    this.AUDIENCE_NAV_CONFIG = AUDIENCE_NAV_CONFIG
    this.SALESFORCE = SALESFORCE

    // Set initial state of publish audience service.
    this.pas.initialize(
      params.audience,
      params.numCompanies,
      params.criteria
    )
    this.pas.setPublishMethod(params.publishMethod)

    this.publishMethod = params.publishMethod
    this.openSlide()
    // Whether we've loaded the publish summary.
    this.loadedSummary = false
    this.summary = {}
    this.mktList = {default: []}

    // Mapping from step name to bool which is true if we've seen the
    // step.
    this.hasSeenStep = _.mapValues(PUBLISH_AUDIENCE_STEPS, () => {
      return false
    })

    this.setSteps()

    $scope.$watchGroup([
      'pas.state.publishNew',
      'pas.state.publishExisting',
      'pas.state.isPublishingContacts'
    ], () => {
      this.setSteps()
    })

    this.goToStep(0)

    // Whether to show the publish fields accordion
    this.showFieldsAccordion = false
    // Whether to show the publish preferences accordion
    this.showPublishPreferencesAccordion = false
    // Whether to show the list we're scoring in the summary step.
    this.showListScoreAccordion = false
    // Whether to show the accordion for deduping lists
    this.showDedupeListAccordion = false
    this.publishFieldsSupportedInited = false

    this.MAX_COMPANIES_SHOW_SUMMARY = MAX_COMPANIES_SHOW_SUMMARY
  }

  /* Wizard step logic */
  isFirstStep(step) {
    return this.steps.indexOf(this.currentStep) === 0
  }

  isLastStep(step) {
    return this.steps.indexOf(this.currentStep) === this.steps.length - 1
  }

  /**
   * Determine what steps are present in this wizard.
   */
  setSteps() {
    if (this.publishMethod.key === 'MAS') {
      if (this.pas.state.publishNew) {
        this.steps = [
          PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA,
          PUBLISH_AUDIENCE_STEPS.CHOOSE_LIST,
          PUBLISH_AUDIENCE_STEPS.APPLY_DEDUPE_LIST,
          PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS,
          PUBLISH_AUDIENCE_STEPS.SUMMARY
        ]
      } else {
        this.steps = [
          PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA,
          PUBLISH_AUDIENCE_STEPS.SELECT_SCORING_LIST,
          PUBLISH_AUDIENCE_STEPS.SUMMARY
        ]
      }
    } else if (_.includes(['CSV', 'SFDC'], this.publishMethod.key)) {
      this.steps = [
        PUBLISH_AUDIENCE_STEPS.WHAT_TO_PUBLISH,
        // PUBLISH_AUDIENCE_STEPS.SELECT_FIELDS,
      ]

      if (this.pas.canPublishToSalesforce()) {
        this.steps.push(PUBLISH_AUDIENCE_STEPS.PUBLISH_PREFERENCES)
      }

      if (this.pas.state.isPublishingContacts) {
        this.steps.push(PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS)
      }

      this.steps.push(PUBLISH_AUDIENCE_STEPS.SUMMARY)
    }

    // If you don't have permission to publish contacts, you shouldn't show this step.
    if (!this.pas.state.canPublishContacts) {
      _.remove(this.steps, PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS)
    }
  }

  nextStep(evt) {
    evt.stopPropagation()

    const stepIdx = this.steps.indexOf(this.currentStep)
    this.goToStep(stepIdx + 1)
  }

  preStep(evt) {
    evt.stopPropagation()

    const stepIdx = this.steps.indexOf(this.currentStep)
    this.goToStep(stepIdx - 1)
  }

  goToStep(idx) {
    if (idx < 0 || idx >= this.steps.length) {
      return
    }

    // Only allow going to a step if all the steps before it have been completed
    if (this.currentStep) {
      const allStepsUpToFilled = _.every(_.range(0, idx), (stepIdx) => {
        return this.isFilled(this.steps[stepIdx])
      })

      if (!allStepsUpToFilled) {
        return
      }
    }

    // Update current step
    this.currentStep = this.steps[idx]

    // Actions to take the first time a step is visited
    if (!this.hasSeenStep[this.currentStep]) {
      if (this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_FIELDS) {
        this.setPublishFields()
      } else if (this.currentStep === PUBLISH_AUDIENCE_STEPS.CHOOSE_LIST ||
                 this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_SCORING_LIST) {
        this.getMktList()
      } else if (this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA) {
      } else if (this.currentStep === PUBLISH_AUDIENCE_STEPS.APPLY_DEDUPE_LIST) {
        this.getMktListForDedupe()
      }
    }

    // Only show publish summary here if we're publishing less than 10k.
    if (this.currentStep === PUBLISH_AUDIENCE_STEPS.SUMMARY &&
        this.calculatePublishSummary()) {
      this.preview()
    }

    // If we're changing steps away from publish summary, our summary is no longer loaded
    if (this.currentStep !== PUBLISH_AUDIENCE_STEPS.SUMMARY) {
      this.loadedSummary = false
    }

    // Mark step as seen
    this.hasSeenStep[this.currentStep] = true
  }

  /**
   * Returns true if the given `step` has been completed and we can
   * move on to the next step.
   */
  isFilled(step) {
    // Only show green checkmark after we've visited the step.
    if (!this.hasSeenStep[step]) {
      return false
    }

    if (step === PUBLISH_AUDIENCE_STEPS.WHAT_TO_PUBLISH) {
      return this.pas.state.numCompanies > 0
    } else if (step === PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA) {
      // Have to choose an MAS type.
      if (!this.pas.state.masType) {
        return false
      }

      // Have to choose either 'new' or 'existing'
      if (!(this.pas.state.publishNew || this.pas.state.publishExisting)) {
        return false
      }

      if (this.pas.state.publishNew) {
        return this.pas.state.numCompanies > 0
      }

      return true
    } else if (step === PUBLISH_AUDIENCE_STEPS.SELECT_FIELDS) {
      // Must select some fields.
      return this.pas.getSelectedPublishFields().length > 0
    } else if (step === PUBLISH_AUDIENCE_STEPS.PUBLISH_PREFERENCES) {
      // We can continue if the dedupe form passes validation.
      return this.dedupeForm.$valid
    } else if (step === PUBLISH_AUDIENCE_STEPS.SELECT_SCORING_LIST) {
      return this.pas.getSelectedMasLists().length > 0
    } else if (step === PUBLISH_AUDIENCE_STEPS.APPLY_DEDUPE_LIST) {
      // The dedupe list is the only choice we make here and it's optional. The user
      // can choose to publish new without selecting a list to dedupe against.
      return true
    } else if (step === PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS) {
      // If you can see this step, you are publishing contacts. You obviously can only publish
      // a non-zero number of contacts. You are also required to give titles or seniorities to
      // narrow things down.
      return this.pas.state.numContacts > 0 && (
        this.pas.getValidTitles().length > 0 ||
          this.pas.getSelectedSeniority().length > 0
      ) && this.contactsForm.$valid
    } else if (step === PUBLISH_AUDIENCE_STEPS.SUMMARY) {
      const stepsBeforeCompleted = _.every(
        this.steps.filter(s => s !== PUBLISH_AUDIENCE_STEPS.SUMMARY),
        s => this.isFilled(s))
      const onSummaryStep = this.currentStep === PUBLISH_AUDIENCE_STEPS.SUMMARY

      // The publish summary is completed if we've done all the previous steps,
      // we're on the summary step, and we've loaded the summary if we need to.
      return stepsBeforeCompleted && onSummaryStep &&
        (!this.calculatePublishSummary() || this.loadedSummary)
    }

    return true
  }

  canSubmit() {
    const stepsFilled = this.steps
            .map(this.isFilled.bind(this))
    const allStepsFilled = _.reduce(stepsFilled, (x, y) => {
      return x && y
    })

    return allStepsFilled
  }

  getNavNgClass(stepName) {
    return {
      active: this.currentStep === stepName,
      filled: this.isFilled(stepName)
    }
  }

  getMktList() {
    this.loadingMktList = true
    this.$http.get(`${this.apiBaseUrl}mas/marketo_list`, {
      params: {
        mas_type: this.pas.state.masType
      }
    }).success((resp) => {
      this.pas.state.masList = resp.data.map(item => {
        item.text = item.name
        item.staged = false
        item.selected = false
        return item
      })
    }).error((resp) => {
      this.alertService.addAlert('error', resp.message || "Couldn't fetch lists", '#slide-alert')
    }).finally(resp => {
      this.loadingMktList = false
    })
  }

  getMktListForDedupe() {
    this.loadingMktListForDedupe = true
    this.$http.get(`${this.apiBaseUrl}mas/marketo_list`, {
      params: {
        mas_type: this.pas.state.masType
      }
    }).success((resp) => {
      this.pas.state.masListsForDedupe = resp.data.map(item => {
        item.text = item.name
        return item
      })
      this.pas.state.masListForDedupe = {}
    }).error((resp) => {
      this.alertService.addAlert('error', resp.message || "Couldn't fetch lists", '#slide-alert')
    }).finally(resp => {
      this.loadingMktListForDedupe = false
    })
  }

  submit() {
    const doPublish = () => {
      this.isPublishPending = true
      this.pas
        .publish()
        .then((res) => {
          SlideController.closeAllSlides()
          this.alertService.addPublishSuccessAlert()
        })
        .catch((res) => {
          this.alertService.addGenericErrorAlert('#slide-alert')
        })
        .finally(() => {
          this.isPublishPending = false
        })
    }

    if (this.calculatePublishSummary() && this.pas.isPublishingContacts()) {
      this.showModalService.confirmationModal({
        title: 'Just so you know',
        buttonText: {
          confirm: 'Ok',
          deny: 'Cancel'
        },
        onConfirm: (result, close) => {
          doPublish()
          close()
        },
        onDeny: (result, close) => {
          close()
        },
        message: `We will only give you contacts whose email addresses are deliverable.
                  </br>
                  As a result, we may publish fewer contacts than the summary indicates.`,
      })
    } else {
      doPublish()
    }
  }

  preview() {
    // Need to show/hide spinner w/ zepto, otherwise the spinners aren't hidden immediately
    // when `loadingSummary` is set to false.
    this.loadingSummary = true
    this.pas
      .getPublishSummary()
      .then((summary) => {
        this.summary = summary
        this.loadedSummary = true
        this.pas.state.contactsNumBeforeDedupe = summary.contactsToPublish
      })
      .catch((err) => {
        this.alertService.addGenericErrorAlert('#slide-alert')
      })
      .finally(() => {
        this.loadingSummary = false
      })
  }

  viewMappingMarketo() {
    this.sms.viewMappingMarketo()
  }

  /**
   * @param publishType - 'new' or 'existing'
   */
  updateMarketoPublish(publishType) {
    if (publishType === 'new') {
      this.pas.state.publishNew = true
      this.pas.state.publishExisting = false
    } else if (publishType === 'existing') {
      this.pas.state.publishNew = false
      this.pas.state.publishExisting = true
      // We don't de-dupe against previous publish when scoring a list because
      // you would be able to score leads published from EAP. The de-dupe against previous publish
      // option is only meant to apply when publishing new.
      this.pas.state.dedupeAgainstPreviousPublish = false
    }
  }

  getPublishTitle() {
    if (this.publishMethod.key === 'CSV') {
      return 'Publish to CSV'
    } else if (this.publishMethod.key === 'SFDC') {
      return 'Publish to Salesforce'
    } else if (this.publishMethod.key === 'MAS') {
      return 'Publish to Marketo'
    }
  }

  getNumCompaniesAfterFiltering() {
    return this.summary.companiesAfterFiltering
  }

  /* Logic around what step we're currently on */
  showSelectCriteria() {
    return (
      this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_CRITERIA
    )
  }

  showWhatToPublishSalesforceOrCsv() {
    return (
      this.currentStep === PUBLISH_AUDIENCE_STEPS.WHAT_TO_PUBLISH &&
        _.includes(['CSV', 'SFDC'], this.publishMethod.key))
  }

  showChooseFields() {
    return (
      this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_FIELDS
    )
  }

  showChooseList() {
    return (this.currentStep === PUBLISH_AUDIENCE_STEPS.CHOOSE_LIST)
  }

  showTitlesContacts() {
    return (this.currentStep === PUBLISH_AUDIENCE_STEPS.TITLES_CONTACTS)
  }

  showSummaryStep() {
    return (this.currentStep === PUBLISH_AUDIENCE_STEPS.SUMMARY)
  }

  showApplyDedupeList() {
    return (this.currentStep === PUBLISH_AUDIENCE_STEPS.APPLY_DEDUPE_LIST)
  }

  showSelectScoringList() {
    return (this.currentStep === PUBLISH_AUDIENCE_STEPS.SELECT_SCORING_LIST)
  }

  /* Dedupe-related methods */
  /**
   * Show step where you choose what to compare against.
   */
  showCompareAgainstWhatStep(publishType) {
    return this.pas.state[publishType]
  }

  /**
   * Show step where you choose where to publish companies.
   */
  showPublishCompaniesStep(publishType) {
    return (
      this.showCompareAgainstWhatStep(publishType) &&
        this.pas.isPublishingToSalesforce()
    )
  }

  /**
   * Whether to enable the step where you choose where to publish companies.
   */
  enablePublishCompaniesStep() {
    return this.pas.state.compareAgainstLeads ||
      this.pas.state.compareAgainstAccounts
  }

  /**
   * Show step asking where to publish contacts in Salesforce.
   */
  showPublishContactsStep(publishType) {
    if (!this.pas.isPublishingContacts()) {
      return false
    }

    // If you're publishing contacts to CSV, then this step is unneeded.
    if (this.pas.isPublishingToCsv()) {
      return false
    }

    if (this.pas.isPublishingToSalesforce()) {
      return this.showPublishCompaniesStep(publishType)
    }

    return true
  }

  shouldShowPublishPreferencesAccordion() {
    return (
      this.pas.canPublishToSalesforce() && (
        this.pas.isPublishingToCsv() ||
          this.pas.isPublishingToSalesforce()
      ))
  }

  showPublishFieldsAccordion() {
    return (
      this.pas.isPublishingToCsv() || this.pas.isPublishingToSalesforce()
    )
  }

  /**
   * The publish contacts step is disabled until you've published companies.
   */
  enablePublishContactsStep() {
    return this.enablePublishCompaniesStep()
  }

  /**
   * Gets the step number to show to the user.
   */
  getStepNumber(newOrExisting, stepName) {
    if (this.pas.isPublishingToSalesforce()) {
      if (stepName === DEDUPE_STEP_NAMES.COMPARE_AGAINST_WHAT) {
        return 1
      } else if (stepName === DEDUPE_STEP_NAMES.PUBLISH_COMPANIES) {
        return 2
      } else if (stepName === DEDUPE_STEP_NAMES.PUBLISH_CONTACTS) {
        return 3
      }
    } else if (this.pas.isPublishingToCsv()) {
      if (stepName === DEDUPE_STEP_NAMES.COMPARE_AGAINST_WHAT) {
        return 1
      } else if (stepName === DEDUPE_STEP_NAMES.PUBLISH_CONTACTS ||
                 stepName === DEDUPE_STEP_NAMES.PUBLISH_COMPANIES) {
        return 2
      }
    }
  }

  getPublishPreferencesMessage() {
    const publishMessages = []

    const newMessageArr = []

    if (this.pas.state.publishNew) {
      const key = `publishNewDetails`
      newMessageArr.push('Publish companies not in your CRM')

      if (this.pas.state[key].publishCompaniesTo === SALESFORCE.ACCOUNT) {
        newMessageArr.push('to Accounts.')
      } else if (this.pas.state[key].publishCompaniesTo === SALESFORCE.LEAD) {
        newMessageArr.push('to Leads.')
      } else if (this.pas.state[key].publishCompaniesTo === SALESFORCE.CONTACT) {
        newMessageArr.push('to Contacts.')
      }

      publishMessages.push(newMessageArr.join(' '))
    }

    const existingMessagesArr = []

    if (this.pas.state.publishExisting) {
      existingMessagesArr.push('Enrich companies already in your CRM.')
      publishMessages.push(existingMessagesArr.join(' '))
    }

    return publishMessages
  }

  /* misc methods */
  setPublishFields() {
    const camelToHumanReadable = (text) => {
      const result = text.replace(/([A-Z])/g, " $1")
      return result.charAt(0).toUpperCase() + result.slice(1)
    }

    this
      .audienceService
      .getPublishFieldsSupported()
      .then(fieldNames => {
        this.pas.state.publishFields = fieldNames.map((fieldName) => {
          return {
            text: camelToHumanReadable(fieldName),
            key: fieldName,
            selected: false
          }
        })
      })['finally'](() => {
        this.$timeout(() => {
          this.publishFieldsSupportedInited = true
        })
      })
  }

  /**
   * The salesforce fields accordion or the Marketo fields accordion.
   */
  toggleFieldsAccordion() {
    this.showFieldsAccordion = !this.showFieldsAccordion
  }

  togglePublishPreferencesAccordion() {
    this.showPublishPreferencesAccordion = !this.showPublishPreferencesAccordion
  }

  toggleListScoreAccordion() {
    this.showListScoreAccordion = !this.showListScoreAccordion
  }

  toggleDedupeListAccordion() {
    this.showDedupeListAccordion = !this.showDedupeListAccordion
  }

  // We used to be doing permission checking for this functionality, but
  // we're removing the check for now. Keeping the code around in case
  // we want to add permission controls for this again in the future.
  canPublishInCrm() {
    // return this.userService.scoringEnabled()
    return true
  }

  canPublishNotInCrm() {
    return this.userService.expansionEnabled()
  }

  openFallbackPreferences() {
    this.showModalService.openFocusSlide({
      templateUrl: './templates/seniority-fallback-slide.html',
      controller: 'SeniorityFallbackSlideController',
      controllerAs: 'sfsc',
      params: {},
    })
  }

  calculatePublishSummary() {
    const isSmallPublish = this.pas.state.numCompanies <= this.MAX_COMPANIES_SHOW_SUMMARY

    // Calculating summary is too slow in the worst case, so we'll omit the summary
    // in this case as well.
    const publishEvenIfNoContactsFound = !(
      this.pas.state.publishNewDetails.publishOnlyIfContactFound ||
        this.pas.state.publishExistingDetails.publishOnlyIfContactFound
    )

    return isSmallPublish && publishEvenIfNoContactsFound
  }

  previewTitles() {
    this.showModalService.openFocusSlide({
      templateUrl: './templates/preview-titles-slide.html',
      controller: 'PreviewTitlesController',
      controllerAs: 'ptc',
      params: {},
    })
  }
}
export default PublishAudienceController
