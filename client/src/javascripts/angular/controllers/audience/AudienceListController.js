import _ from 'lodash'

// TODO: Colocate this code with the React Component.
class AudienceListController {
  /* @ngInject */
  constructor($state,
              $filter,
              audienceService,
              showModalService,
              alertService,
              userService,
              $q,
              modelService) {
    this.$state = $state
    this.$filter = $filter
    this.audienceService = audienceService
    this.sms = showModalService
    this.alertService = alertService
    this.userService = userService
    this.modelService = modelService
    this.$q = $q

    this.inited = false
    this.audiences = []
    this.refreshingCount = {}

    if (this.isAudState()) {
      this.listType = 'AUDIENCE'
      this.initAudConsole()
    }
    if (this.isExcState()) {
      this.listType = 'EXCLUSION_LIST'
      this.initExcConsole()
    }

    this.findAudienceByKeywords = (this.findAudienceByKeywords).bind(this)
    this.findAudienceByUrls = this.findAudienceByUrls.bind(this)
    this.findAudienceByCsv = this.findAudienceByCsv.bind(this)
    this.findAudienceByFilters = this.findAudienceByFilters.bind(this)
    this.enrichAudience = this.enrichAudience.bind(this)
  }

  isExcState() {
    return this.$state.is('exclusion.console')
  }

  isAudState() {
    return this.$state.is('audience.console')
  }

  createBtnText() {
    return this.isAudState() ? 'Create Audience' : 'Create Exclusion List'
  }

  initAudConsole() {
    const modelStatusP = this.modelService.httpCRMFitModels()
    const audienceListP = this.audienceService.getAudiences()
    this.$q.all([modelStatusP, audienceListP]).then(
      (responses) => {
        const crmFitStatus = responses[0].data.data
        const audiences = responses[1].data.data

        this.crmFitStatus = crmFitStatus
        this.audiences = audiences

        this.inited = true
      }
    )
  }

  initExcConsole() {
    this.audienceService
      .getExcLists()
      .then((response) => {
        this.audiences = response.data.data
        this.inited = true
      })
  }

  refreshConsole() {
    this.refreshing = true
    const refreshP = this.isAudState() ? this.audienceService.getAudiences() : this.audienceService.getExcLists()
    return refreshP.then(
      (response) => {
        this.audiences = response.data.data
        this.refreshing = false
        return this.audiences
      }
    )
  }

  findAudienceByKeywords() {
    if (this.checkIfCRMFitExists()) {
      this.$state.go('audience.find.keywords')
    }
  }

  findAudienceByUrls() {
    if (this.checkIfCRMFitExists()) {
      this.$state.go('audience.find.urls')
    }
  }

  findAudienceByCsv() {
    if (this.checkIfCRMFitExists()) {
      this.$state.go('audience.find.csv')
    }
  }

  findAudienceByFilters() {
    if (this.checkIfCRMFitExists()) {
      this.$state.go('audience.find.criteria')
    }
  }

  enrichAudience() {
    if (this.checkIfCRMFitExists()) {
      this.$state.go('audience.enrich')
    }
  }

  viewCompanyList(audienceId) {
    if (this.isAudState()) {
      this.$state.go('audience.company', {audienceId})
    }
    if (this.isExcState()) {
      this.$state.go('exclusion.company', {audienceId})
    }
  }

  canFind() {
    return this.userService.canFindCompanies()
  }

  canScore() {
    return this.userService.canScoreList()
  }

  viewAudienceSummary(audience) {
    const params = {audience}
    this.sms.audienceSummary(params)
  }

  viewAudienceSettings(audience) {
    const refreshConsole = () => {
      return this.refreshConsole()
    }
    const params = {audience, refreshConsole}
    this.userService.fetchData().then(() => {
      this.sms.audienceSettings(params)
    })
  }

  viewAudienceExcLists(audience) {
    const selectedExcListIds = _.map(audience.exclusionLists, (list) => {
      return list.id
    })

    const onFinish = (exclusionListIds) => {
      const audWithExcIds = _.extend(audience, {exclusionListIds})
      return this.audienceService
        .updateAudience(audWithExcIds)
        .then(() => {
          this.refreshConsole()
        })
    }

    this.audienceService
      .getExcLists()
      .then((response) => {
        const allLists = response.data.data
        this.sms.chooseExclusion({
          onFinish,
          allLists,
          selectedExcListIds
        })
      })
  }

  /**
   * Show a human readable number or '--' for missing data.
   */
  getDispNum(num) {
    if (!num && num !== 0) {
      return '--'
    }

    if (num > 100000) {
      return '100K+'
    }

    return this.$filter('number')(num).toString()
  }

  deleteConfirm(audience) {
    const subject = this.isAudState() ? 'audience' : 'list'
    const modalMessage = `You are about to permanently delete this ${subject}. <br />
                          <b>Are you sure you want to delete ${audience.name}?</b>`
    const modalParams = {
      title: 'Warning',
      message: modalMessage,
      messageType: 'warning',
      buttonText: {
        confirm: 'Confirm',
        deny: 'Cancel'
      },
      onConfirm: (result, close) => {
        this.deleteAudience(audience)
        close()
      }
    }
    this.sms.confirmationModal(modalParams)
  }

  deleteAudience(audience) {
    this.audienceService.deleteAudience(audience.id).then(
      () => {
        const subject = this.isAudState() ? 'Audience' : 'Exclusion List'
        this.alertService.addAlert('success', `${subject} ${audience.name} deleted.`)
        this.refreshConsole()
      }
    )
  }

  checkIfCRMFitExists() {
    const crmFitModelComplete = this.modelService.isComplete(this.crmFitStatus)
    if (crmFitModelComplete || this.userService.isInternalAdmin()) {
      return true
    }
    const status = this.crmFitStatus

    const inProgress = this.modelService.isProgressing(status)
    const isAcctAdmin = this.userService.isAcctAdmin()

    let title = ''

    if (inProgress) {
      title = 'Looks like your model is in progress.'
    } else {
      const baseMsg = 'Whoa! You need a model to start finding companies.'
      title = isAcctAdmin ? baseMsg : `${baseMsg} </br> Please contact your account admin to enable this feature.`
    }

    const showButtons = isAcctAdmin && !inProgress

    this.sms.needCRMFitModel({status, title, showButtons})
  }

  refreshCount(ev, audience) {
    ev.stopPropagation()
    const audienceId = audience.id
    this.refreshingCount[audienceId] = true
    this
      .audienceService
      .refreshAudienceCount(audienceId)
      .then((response) => {
        const countData = response.data.data
        _.extend(audience, countData)
      })
      .finally(() => {
        this.refreshingCount[audienceId] = false
      })
  }
}

export default AudienceListController
