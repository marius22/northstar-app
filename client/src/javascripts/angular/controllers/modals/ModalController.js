import {Intercom} from 'intercom'
import _ from 'lodash'
import {getIconName} from 'enums'

class ModalController {
  /* @ngInject */
  constructor($scope, close, params, alertService, userService, $http, apiBaseUrl, $sce) {
    this.close = close
    this.userService = userService
    this.alertService = alertService
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.params = params
    this.$sce = $sce

    $scope.$on('$destroy', () => {
      Intercom.show()
    })

    this.processModalParams(params)
  }

  processModalParams(params) {
    if (params.onConfirm) {
      this.onConfirm = (result) => {
        params.onConfirm(result, this.close)
      }
    }

    if (params.onDeny) {
      this.onDeny = (result) => {
        params.onDeny(result, this.close)
      }
    }

    if (params.title) {
      this.title = params.title
    }

    // messageType can be success, error, warning, or info
    if (params.messageType) {
      this.messageClass = params.messageType
    }

    if (params.message) {
      this.message = this.$sce.trustAsHtml(params.message)
    }

    if (params.buttonText) {
      this.btnConfirm = params.buttonText.confirm
      this.btnDeny = params.buttonText.deny
    }

    if (params.chartConfig) {
      this.chartTitle = params.chartConfig.chartTitle
      this.chartData = params.chartConfig.chartData
    }

    if (params.dataViewItems) {
      this.dataViewItems = params.dataViewItems
    }

    if (params.totalContacts) {
      this.totalContacts = params.totalContacts
    }

    if (params.initialData) {
      this.data = params.initialData
    }
  }

  blockClose($event) {
    $event.stopPropagation()
  }

  upperCamel(text) {
    const result = text.replace(/([A-Z])/g, " $1")
    return result.charAt(0).toUpperCase() + result.slice(1)
  }

  requestUpgrade(result) {
    if (this.userService.isTrial()) {
      let invalidInputErrorMessage = ''
      // check empty
      _.each(['phone', 'lastName', 'firstName'], name => {
        if (!result[name]) {
          const emptyName = this.upperCamel(name)
          invalidInputErrorMessage = `Please enter a valid ${emptyName}.`
        }
      })

      // check phone
      if (!invalidInputErrorMessage) {
        if (!/\d{6,12}/.test(result.phone)) {
          invalidInputErrorMessage = 'Phone number should be 6-12 digits.'
        }
      }

      // if there were no problems with the user's input, we make the POST request.
      // Otherwise, we show invalidInputErrorMessage in the modal
      if (!invalidInputErrorMessage) {
        this.close()
        this.$http.post(`${this.apiBaseUrl}users/request_upgrade`, {
          firstName: result.firstName,
          lastName: result.lastName,
          phone: `${result.phone}`
        })
          .success(() => {
            this.alertService.addAlert('success', 'Request successfully submitted!', '#topAlert')
          })
          .error((response) => {
            this.alertService.addAlert('error', `${response.message}`, '#topAlert')
          })
      } else {
        this.alertService.addAlert('error', invalidInputErrorMessage, '#request-upgrade-error')
      }

    } else {
      // User is already a paid user, so we have their information already.
      this.close()
      this.$http.post(`${this.apiBaseUrl}users/request_more`)
        .success(() => {
          this.alertService.addAlert('success', 'Request successfully submitted!', '#topAlert')
        })
        .error((response) => {
          this.alertService.addAlert('error', `${response.message}`, '#topAlert')
        })
    }
  }

  getIconName() {
    if (!this.messageClass) {
      this.messageClass = 'info'
    }
    return getIconName(this.messageClass)
  }

  downloadCsv() {
    const blob = new Blob([this.params.csv]);
    const a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
    a.download = this.params.fileName || 'revised.csv';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    this.close()
  }
}

export default ModalController
