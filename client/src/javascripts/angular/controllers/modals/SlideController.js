import {Intercom} from 'intercom'


let numberOfSlidesOpen = 0

// Existing focus slides
const slides = []

class SlideController {
  constructor(close, $document, $timeout) {
    this.close = close
    this.$document = $document
    this.$timeout = $timeout
    this.showSlide = false
  }

  /**
   * Close all open focus slides
   */
  static closeAllSlides() {
    while (slides.length > 0) {
      const slide = slides.pop()
      slide.closeSlide()
    }
  }

  /**
   * Close the focus slide.
   */
  closeSlide() {
    this.showSlide = false
    numberOfSlidesOpen -= 1
    this.$document.find('body').removeClass('ng-modal-open')
    this.$timeout(() => {
      this.close()
    }, 300)
  }

  /**
   * Open/slide in the focus slide.
   */
  openSlide() {
    this.$timeout(() => {
      this.showSlide = true
      slides.push(this)
      numberOfSlidesOpen += 1
    }, 0)
    // if we don't add some delay here, api calls will happen in the same digest cycle and makes
    // css animation not smooth
    this.$timeout(() => {
      this.init()
    }, 300)
  }
  // init method should be implemented in child class
  init() {}
}

export default SlideController
