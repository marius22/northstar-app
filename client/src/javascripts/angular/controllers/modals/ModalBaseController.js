import $ from 'npm-zepto'
import {Intercom} from 'intercom'

class ModalBaseController {
  constructor(close, $scope) {
    const $body = $('body')
    this.close = close
    $body.addClass('ng-modal-open')

    $scope.$on('$destroy', () => {
      Intercom.show()
    })
  }
}

export default ModalBaseController
