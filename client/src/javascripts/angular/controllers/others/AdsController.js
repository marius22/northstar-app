class AdsController {
  /* @ngInject */
  constructor(userService,
              $sce) {
    this.userService = userService

    const userId = this.userService.userId()
    const targetUrl = `https://ads.uat.everstring.com/?uid=${userId}`
    this.adsUrl = $sce.trustAsResourceUrl(`${targetUrl}`)
  }
}

export default AdsController
