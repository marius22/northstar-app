/**
 * Controller that shows the fancy loading screen where a scanner
 * scans back and forth across the page.
 */
class ProgressViewController {
  /* @ngInject */
  constructor($interval, $timeout) {
    this.$interval = $interval
    this.$timeout = $timeout
    this.fancyStatusMsgs = [
      "Firing up Hadoop clusters",
      "Querying Hive tables",
      "Connecting 20,000 data signals",
      "Initializating predictive models",
      "Analyzing metadata",
      "Correcting load balancers",
      "Invoking parallel methods",
      "Calculating feature weights",
      "Querying on full outer joins",
      "Analyzing regression coefficients",
      "Maximizing expected utilities",
      "Transposing data matrices",
      "Mapping and reducing",
      "Joining asynchronous threads",
      "Rendering confabulation obsolete",
      "Eliminating outliers",
      "Cleaning ETL pipeline"
    ]
    this.showMsg = true
    this.msgIdx = 0
    this.currMsg = this.fancyStatusMsgs[this.msgIdx]
    this.startMsgUpdates()
  }

  startMsgUpdates() {
    const updateStatusMsg = () => {
      this.msgIdx++
      if (this.msgIdx === this.fancyStatusMsgs.length) {
        this.msgIdx = 0
      }
      // use this.showMsg with ng-if and ng-animate to show old message floating upwards
      // and new message appearing from below
      this.showMsg = false
      this.$timeout(() => {
        this.showMsg = true
      }, 0)
      this.currMsg = this.fancyStatusMsgs[this.msgIdx]
      this.appendEllipses()
    }

    this.$interval(updateStatusMsg, 6000)
    this.appendEllipses()
  }

  // add first, second, and third ellipses after 1, 3, and 5 seconds respectively
  appendEllipses() {
    const appendEllipse = () => {
      this.currMsg = `${this.currMsg}.`
    }
    this.$timeout(appendEllipse, 1000)
    this.$timeout(appendEllipse, 3000)
    this.$timeout(appendEllipse, 5000)
  }
}

export default ProgressViewController
