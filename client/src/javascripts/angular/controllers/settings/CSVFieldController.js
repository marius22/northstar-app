import _ from 'lodash'
import { insightDisplayName } from 'utils'

class CSVFieldController {
  /* @ngInject */
  constructor(fieldMappingService, $scope, $q, alertService, userService) {
    this.fieldMappingService = fieldMappingService
    this.$q = $q
    this.alertService = alertService
    this.fieldTypes = [
      {
        name: 'Insights',
        key: 'insights'
      }
    ]
    if (userService.scoringEnabled()) {
      this.fieldTypes.push({
        name: 'Models',
        key: 'models'
      })
    }
    this.currentFieldType = this.fieldTypes[0]
    this.fieldMappingService
      .getSelectedCsvFields()
      .then((fields) => {
        const insightIds = _.map(fields, (field) => {
          return field.insightId
        })
        const modelIds = _.map(fields, (field) => {
          return field.modelId
        })
        this.initMsData(insightIds, modelIds)
      })

    $scope.$on('selectionChanged', () => {
      this.saveChanges()
    })
  }

  initMsData(currentInsightIds, currentModelIds) {
    this.fieldMappingService
      .fetchPossibleFields({ targetType: 'csv'})
      .then((fieldData) => {
        _.forEach(fieldData.insights, (datum) => {
          datum.text = insightDisplayName(datum.displayName)
          datum.selected = _.includes(currentInsightIds, datum.id)
        })
        _.forEach(fieldData.models, (datum) => {
          datum.text = datum.name
          datum.selected = _.includes(currentModelIds, datum.id)
        })
        this.fieldData = fieldData
        this.inited = true
      })
  }

  saveChanges() {
    const type = this.currentFieldType.key
    const selectedTargetFields = this.getTargetFieldsByTypeAndSelection(type, true)
    const unselectedTargetFields = this.getTargetFieldsByTypeAndSelection(type, false)

    const selectFieldsP = this.fieldMappingService.selectCsvFields(selectedTargetFields)
    const deselectFieldsP = this.fieldMappingService.deselectCsvFields(unselectedTargetFields)
    return this.$q.all([selectFieldsP, deselectFieldsP])
      .then((responses) => {
        return responses
      }, () => {
        this.alertService.addAlert('error', 'Changes failed to save.')
      })
  }

  getTargetFieldsByTypeAndSelection(type, selection) {
    const defaultCsvMappings = this.fieldData.defaultMappings.csv.company

    return _(this.fieldData[type])
      .filter((item) => {
        return selection ? item.selected : !item.selected
      })
      .map((item) => {
        if (type === 'insights') {
          return {
            targetField: defaultCsvMappings[item.displayName],
            insightId: item.id
          }
        }
        if (type === 'models') {
          return {
            targetField: `Score (${item.name})`,
            modelId: item.id
          }
        }
      })
      .value()
  }
}

export default CSVFieldController
