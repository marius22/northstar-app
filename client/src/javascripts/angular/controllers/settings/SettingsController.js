class SettingsController {
  /* @ngInject */
  constructor(userService) {
    this.menuContent = {
      'Account Settings': [
        {
          text: 'Quotas and Users',
          sref: 'settings.users',
          enabled: true
        }
      ],
      'Publish Settings': [
        {
          text: 'Salesforce',
          sref: 'settings.sfdc',
          enabled: true
        },
        {
          text: 'Marketo',
          sref: 'settings.mkto',
          enabled: true
        },
        {
          text: 'CSV',
          sref: 'settings.csv',
          enabled: true
        },
        {
          text: 'REST API',
          sref: 'settings.api',
          enabled: userService.publicApisEnabled()
        }
      ]
    }
  }
}

export default SettingsController
