import _ from 'lodash'

class RealTimeScoringController {
  /* @ngInject */
  constructor(userService, alertService) {
    this.userService = userService
    this.alertService = alertService
    this.rtsTargetOptions = [
      {dispName: 'Accounts', value: 'account'},
      {dispName: 'Leads', value: 'lead'},
      {dispName: 'Contacts', value: 'contact'}
    ]

    this.rtsModeOptions = [
      {
        dispName: 'Never',
        value: '',
        tooltip: 'We will never automatically publish any data to Salesforce.'
      },
      {
        dispName: 'Globally',
        value: 'global',
        tooltip: 'We will automatically publish data to Salesforce for all inbound prospects.'
      },
      {
        dispName: 'Per audience',
        value: 'audience',
        tooltip: `We will automatically publish data to Salesforce for all inbound prospects
                  that exist in an Audience which has Real Time Scoring enabled.`
      }
    ]

    this.loading = true
    userService.fetchData().then(() => {
      this.selectedTargets = userService.getInboundLeadsTargets()
      this.selectedMode = userService.getRealtimeScoringMode()
      this.loading = false
    })
  }

  getSelectedTargets() {
    return _.keys(_.pickBy(this.selectedTargets))
  }

  neverScoringSelected() {
    return this.selectedMode === ''
  }

  // An invalid combination is when scoring is on "globally" or "per audience", but
  // no objects are selected for checking inbound prospects
  invalidCombination() {
    return !this.neverScoringSelected() && _.isEmpty(this.getSelectedTargets())
  }

  saveScoringSettings() {
    // don't save nonsensical settings
    if (this.invalidCombination()) {
      return
    }
    this.userService.updateScoringSettings(this.getSelectedTargets(), this.selectedMode).then(
      (response) => {
        this.userService.fetchData()
        return response.data.data
      },
      (response) => {
        this.alertService.addAlert('error', 'Error updating settings.')
        return response
      }
    )
  }
}

export default RealTimeScoringController
