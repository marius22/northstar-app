class PublishSettingsController {
  /* @ngInject */
  constructor($state, userService) {
    this.$state = $state
    this.userService = userService
    this.setTabs()
  }

  setTabs() {
    if (this.isSfdcSettings()) {
      this.tabs = [
        {
          text: 'Field Mapping',
          sref: 'settings.sfdc.fields',
          enabled: true
        },
        {
          text: 'Real Time Scoring',
          sref: 'settings.sfdc.rts',
          enabled: this.userService.scoringEnabled()
        }
      ]
    }

    if (this.isMktoSettings()) {
      this.tabs = [
        {
          text: 'Field Mapping',
          sref: 'settings.mkto.fields',
          enabled: true
        }
      ]
    }

    if (this.isCsvSettings()) {
      this.tabs = [
        {
          text: 'Field Selection',
          sref: 'settings.csv.fields',
          enabled: true
        }
      ]
    }

    if (this.isRestApiSettings()) {
      this.tabs = [
        {
          text: 'API Access',
          sref: 'settings.api.instructions',
          enabled: true
        },
        {
          text: 'Model ID Reference',
          sref: 'settings.api.modelRef',
          enabled: true
        }
      ]
    }
  }

  isSfdcSettings() {
    return this.$state.includes('settings.sfdc')
  }

  isMktoSettings() {
    return this.$state.includes('settings.mkto')
  }

  isCsvSettings() {
    return this.$state.includes('settings.csv')
  }

  isRestApiSettings() {
    return this.$state.includes('settings.api')
  }
}

export default PublishSettingsController
