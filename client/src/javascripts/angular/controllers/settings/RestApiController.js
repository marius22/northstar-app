// prod, demo, and uat envs have their own api container set up.
const PROD_ENRICH_URL = 'https://api.everstring.com/v1/companies/enrich'
const DEMO_ENRICH_URL = 'https://api.demo.everstring.com/v1/companies/enrich'
const UAT_ENRICH_URL = 'https://api.uat.everstring.com/v1/companies/enrich'

const STAGING_ENRICH_URL = 'https://eap.staging.everstring.com/app/companies/enrich'
const DEV_ENRICH_URL = 'https://eap.dev.everstring.com/app/companies/enrich'

const API_URL_TO_ENV_MAP = {
  'prod': PROD_ENRICH_URL,
  'demo': DEMO_ENRICH_URL,
  'uat': UAT_ENRICH_URL,
  'staging': STAGING_ENRICH_URL,
  'dev': DEV_ENRICH_URL,
  'local': DEV_ENRICH_URL
}

class RestApiController {
  /* @ngInject */
  constructor(showModalService, tokenService) {
    this.sms = showModalService
    this.tokenService = tokenService
    this.env = process.env.ENV.toLowerCase()
    this.apiUrl = API_URL_TO_ENV_MAP[this.env] || PROD_ENRICH_URL
    this.getToken()
  }

  dispToken() {
    return this.token || `You currently don't have a token. Click the refresh icon to generate one.`
  }

  clickRefresh() {
    if (this.token) {
      const params = {
        title: 'New Token',
        message: 'Generating a new token will replace your current token. Continue?',
        messageType: 'warning',
        onConfirm: (result, close) => {
          this.refreshToken()
          close()
        }
      }
      this.sms.confirmationModal(params)
    } else {
      this.refreshToken()
    }
  }

  refreshToken() {
    this.tokenLoading = true
    this.tokenService
      .generateNewToken()
      .then((token) => {
        this.token = token
        this.tokenLoading = false
      })
  }

  getToken() {
    this.tokenLoading = true
    this.tokenService
      .getCurrentToken()
      .then((token) => {
        this.token = token
        this.tokenLoading = false
      })
  }
}

export default RestApiController
