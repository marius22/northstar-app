import * as config from 'config'
import * as Promise from 'bluebird'
import _ from 'lodash'

import { getChoiceFromSide } from 'components/MappingRow'
import { insightDisplayName, guid } from 'utils'

const ACCOUNT_TAB = 'settings.sfdc.fields.account'
const LEAD_TAB = 'settings.sfdc.fields.lead'
const CONTACT_TAB = 'settings.sfdc.fields.contact'

// The promise from mapping loading
let loadMappingP

const marketoIntegrationTypes = [
  {value: 'marketo', label: 'Marketo (Production)'},
  {value: 'marketo_sandbox', label: 'Marketo (Sandbox)'}
]

const LABEL_TO_TOOLTIP_TEXT_MAP = {
  'Industry': `EAP will publish Industry data with values that may not be allowed
               by the default SFDC 'Industry' field.
               Instead, please use the 'ESIndustry' field from our managed package,
               or your own custom field.`,

  'Revenue': `EAP will publish Revenue data in a format not supported by the default SFDC
              'Revenue' field.
              Instead, please use the 'ESRevenue' field from our managed package,
              or your own custom field.`,

  'Employee Size': `EAP will publish Employee Size data in a format not supported by the default
                    SFDC 'Employees' field.
                    Instead, please use the 'ESEmployees' field from our managed package,
                    or your own custom field.`
}


/**
 * This controller handles the logic around getting the field mapping information. It's used for SFDC
 * and Marketo field mapping.
 */
export default class FieldMappingController {
  /* @ngInject */
  constructor(fieldMappingService, $state, userService, $http, $timeout, $scope,
              alertService, salesforceService, showModalService, $q, $filter, marketoService) {
    this.fieldMappingService = fieldMappingService
    this.$state = $state
    this.$http = $http
    this.userService = userService
    this.$timeout = $timeout
    this.$scope = $scope
    this.alertService = alertService
    this.salesforceService = salesforceService
    this.sms = showModalService
    this.$q = $q
    this.$filter = $filter
    this.marketoService = marketoService

    // Initialize this component based on what kind of integration we're doing.
    if (this.isSalesforceSettingsPage()) {
      this.initSalesforce()
    } else if (this.isMarketoSettingsPage()) {
      this.initMarketo()
    }
  }

  initSalesforce() {
    this.listenForTabChange()

    this.tabs = [
      {
        text: 'Account',
        sref: ACCOUNT_TAB,
      },
      {
        text: 'Lead',
        sref: LEAD_TAB,
      },
      {
        text: 'Contact',
        sref: CONTACT_TAB,
      }
    ]
    const userId = this.userService.userId()
    const checkIntegrationP = this.salesforceService.isIntegrated({forceCheck: true})
    const checkPackageP = this.salesforceService.packageInstalled(userId)

    const handleSetupRequired = (hasIntegration, hasPackage) => {
      let message = ''
      if (!hasIntegration && !hasPackage) {
        message = 'You must integrate with Salesforce and install our managed package to use this feature.'
      } else if (!hasIntegration) {
        message = 'You must integrate with Salesforce to use this feature.'
      } else if (!hasPackage) {
        message = 'You must install our managed package to use this feature.'
      }

      this.sms.warnNoIntegration({ message })
    }

    checkIntegrationP.then((response) => {
      const integrated = response.data.data
      checkPackageP.then(() => {
        if (!integrated) {
          handleSetupRequired(integrated, true)
        } else {
          this.loadMappingForSfdcObject(this.$state.current)
        }
      }, (err) => {
        handleSetupRequired(integrated, false)
      })
    })
  }

  initMarketo() {
    this.listenForTabChange()

    // The integrations we can choose from.
    this.marketoIntegrationDropdownProps = {
      options: [],
      value: null,
      searchable: false,
      onChange: (value) => {
        this.$timeout(() => {
          this.selectedIntegration =
            this.marketoIntegrationDropdownProps.options.find((option) => {
              return option.value === value
            })

          this.loadMapping()
        })
      },
      clearable: false,
    }
    this.isLoading = true

    return Promise
      .all(marketoIntegrationTypes.map((integrationType) => {
        // Check which integrations we have available
        return this
          .marketoService.isIntegrated(integrationType.value)
          .then((res) => {
            if (res.data.data) {
              return integrationType
            } else {
              return null
            }
          })
      }))
      .then((marketoPs) => {
        // If there are integrations, proceed. Otherwise, direct the user to go integrate first.
        const existingIntegrations = _.compact(marketoPs)
        if (existingIntegrations.length > 0) {
          // The integration we're mapping for
          this.selectedIntegration = this.selectedIntegration || existingIntegrations[0]
          this.$timeout(() => {
            this.marketoIntegrationDropdownProps.value = this.selectedIntegration
            this.marketoIntegrationDropdownProps.options = existingIntegrations
          })

          return true
        } else {
          const message = 'You must integrate with Marketo to use this feature.'
          this.sms.warnNoIntegration({ message })

          return false
        }
      })
      .then((hasMarketoIntegration) => {
        if (!hasMarketoIntegration) {
          return
        }

        return this.loadMapping()
      })
  }

  isSalesforceSettingsPage() {
    return this.$state.includes('settings.sfdc')
  }

  isMarketoSettingsPage() {
    return this.$state.includes('settings.mkto')
  }

  /**
   * For the given `esFieldChoice`, return the valid Salesforce field types.
   */
  getValidTypes(esFieldChoice) {
    let integerTypes
    let stringTypes
    let urlTypes
    let phoneTypes
    let boolTypes

    if (this.isSalesforceSettingsPage()) {
      // We restrict SFDC field selection based on ES field selection.
      integerTypes = ['int']
      stringTypes = ['string', 'textarea']
      urlTypes = ['string', 'textarea', 'url']
      phoneTypes = ['string', 'textarea', 'phone']
      boolTypes = ['boolean']

    } else if (this.isMarketoSettingsPage()) {
      integerTypes = ['integer']
      stringTypes = ['string', 'text']
      urlTypes = ['string', 'text', 'url']
      phoneTypes = ['string', 'text', 'phone']
      boolTypes = ['boolean']
    }

    // Domain or Linkedin URL
    if (_.includes(['domain', 'linkedinUrl'], esFieldChoice.camelCaseName)) {
      return urlTypes
    } else if (esFieldChoice.modelId || _.includes(['alexaRank'], esFieldChoice.camelCaseName)) {
      return integerTypes
    } else if (_.includes(['companyPhone'], esFieldChoice.camelCaseName)) {
      return phoneTypes
    } else if (_.includes(['esSource', 'esEnriched'], esFieldChoice.camelCaseName)) {
      return boolTypes
    }

    return stringTypes
  }

  /**
   * If we choose `selectedChoice` and we have `allChoices`, return all ES choices
   * we're allowed to choose from.
   */
  getAllESChoicesForRow(selectedChoices, allChoices) {
    const finalChoices = allChoices
      .filter((choice) => {
        return !_.includes(_.map(selectedChoices, 'value'), choice.value)
      })

    return finalChoices
  }

  /**
   * For the given `esFieldChoice` and `sfdcFieldChoice`, and given all `sfFields`
   * available, return the SFDC fields we're allowed to choose from.
   */
  getAllChoicesForRow(esFieldChoice, sfdcSelectedFields, sfFields) {
    const finalChoices = sfFields
    // If we've already selected an SFDC field choice, then we don't show it
    // in the rest of the dropdown.
      .filter((choice) => {
        return !_.includes(_.map(sfdcSelectedFields, 'value'), choice.value)
      })
    // Restrict SFDC fields to the allowed types
      .filter((choice) => {
        if (!esFieldChoice) {
          return true
        }
        return this.getValidTypes(esFieldChoice).includes(choice.type)
      })

    return finalChoices
  }

  getCurrentSfdcObjectStr() {
    const obj = _.find(this.tabs, (tab) => {
      return tab.sref === this.$state.current.name
    })
    if (obj) {
      return obj.text
    }
  }

  /**
   * Get the current target field choice from the given server mapping.
   */
  getSelectedTargetFieldChoice({ serverFieldMapping, targetFields }) {
    const targetField = _.find(targetFields, (field) => {
      return field.value === serverFieldMapping.targetField
    })

    if (targetField) {
      return {
        value: targetField.value,
        label: targetField.label,
        type: targetField.type,
      }
    } else {
      const actualDispName = insightDisplayName(serverFieldMapping.displayName)
      this.esFieldsWithoutTargetField.push(actualDispName)
    }
  }

  /**
   * Get ES fields we're allowed to map from.
   */
  getValidESFieldChoices({ serverFieldMappings, possibleEsFields }) {
    return this
      .getChoicesFromPossibleFields({possibleEsFields})
      .filter((choice) => {
        return !_.includes(choice.value)
      })
  }


  getInitialMappingState({targetFields,
                          leftSelectedChoice,
                          rightSelectedChoice,
                          leftSelectedChoices,
                          rightSelectedChoices,
                          validESFieldsChoices,
                          validTargetFieldChoices,
                          needsServerDelete,
                          updatedTimestamp}) {
    let mapping = {
      key: guid(),
      leftSide: {
        selectedChoice: leftSelectedChoice,
        isFixedChoice: needsServerDelete,
        // We only have tooltips in the Salesforce case for now.
        tooltipContent: (leftSelectedChoice && this.isSalesforceSettingsPage()) ? LABEL_TO_TOOLTIP_TEXT_MAP[leftSelectedChoice.label] : null,
      },
      rightSide: {
        selectedChoice: rightSelectedChoice,
      },
      needsServerDelete,
      updatedTimestamp
    }

    mapping.leftSide.choices = this.getAllESChoicesForRow(
      leftSelectedChoices, validESFieldsChoices)
    mapping.rightSide.choices = this.getAllChoicesForRow(
      mapping.leftSide.selectedChoice, rightSelectedChoices, validTargetFieldChoices)
    mapping.onSelectLeft = () => {
      this.$timeout(() => {
        this.mappingProps.mappings.forEach((selectedMapping) => {
          if (this.isSalesforceSettingsPage()) {
            selectedMapping.leftSide.tooltipContent = LABEL_TO_TOOLTIP_TEXT_MAP[selectedMapping.leftSide.selectedChoice.label]
          }
          selectedMapping.leftSide.choices = this.getAllESChoicesForRow(
            _.map(this.mappingProps.mappings, (m) => m.leftSide.selectedChoice), validESFieldsChoices)

          selectedMapping.rightSide.choices = this.getAllChoicesForRow(
            selectedMapping.leftSide.selectedChoice, _.map(this.mappingProps.mappings, (m) => m.rightSide.selectedChoice), validTargetFieldChoices)

          // If we choose an ES field on the left, then we'll null out the Salesforce field if
          // its type is invalid.
          if (selectedMapping.leftSide.selectedChoice && mapping.key === selectedMapping.key) {
            const esFieldValidTypes = this.getValidTypes(selectedMapping.leftSide.selectedChoice)
            const currentChoiceValid = esFieldValidTypes.includes(
              _.find(targetFields, selectedMapping.rightSide.selectedChoice).type)
            if (!currentChoiceValid) {
              selectedMapping.rightSide.selectedChoice = null
            }
          }
        })
        this.setUnsavedChanges()
      })
    }
    mapping.onSelectRight = () => {
      this.$timeout(() => {
        this.mappingProps.mappings.forEach((selectedMapping) => {
          selectedMapping.rightSide.choices = this.getAllChoicesForRow(
            selectedMapping.leftSide.selectedChoice, _.map(this.mappingProps.mappings, (m) => m.rightSide.selectedChoice), validTargetFieldChoices)
        })
        this.setUnsavedChanges()
      })
    }
    mapping.onDelete = () => {
      if (!mapping.needsServerDelete) {
        this.$timeout(() => {
          this.removeMappingRow(mapping)
        })
      } else {
        const onConfirm = (result, close) => {
          const leftChoice = mapping.leftSide.selectedChoice
          this.deleteMappingAsync(leftChoice)
            .then(() => {
              this.removeMappingRow(mapping)
              close()
            })
        }
        this.sms.confirmDeleteMapping(mapping, onConfirm)
      }
    }

    return mapping
  }

  removeMappingRow(row) {
    this.mappingProps.mappings = _.without(this.mappingProps.mappings, row)
  }

  /**
   * Set up props for mapping table.
   */
  initMappingTable({serverFieldMappings, targetFields, possibleEsFields, fixedMappings = []}) {
    const validESFieldsChoices = this.getValidESFieldChoices({ serverFieldMappings, possibleEsFields })
    const validTargetFieldChoices = targetFields

    const leftSelectedChoices = serverFieldMappings.map((serverFieldMapping) => {
      return this.getSelectedEsFieldChoice({ serverFieldMapping })
    })
    const rightSelectedChoices = serverFieldMappings.map((serverFieldMapping) => {
      return this.getSelectedTargetFieldChoice({ serverFieldMapping, targetFields })
    })

    this.$timeout(() => {
      this.mappingProps.numLoadingRows = 0

      this.mappingProps.mappings.push(...fixedMappings)

      this.mappingProps.mappings.push(...serverFieldMappings.map((serverFieldMapping) => {
        const leftSelectedChoice = this.getSelectedEsFieldChoice({serverFieldMapping})
        const rightSelectedChoice = this.getSelectedTargetFieldChoice({ serverFieldMapping, targetFields })

        return this.getInitialMappingState({
          targetFields, leftSelectedChoice, rightSelectedChoice,
          leftSelectedChoices,
          rightSelectedChoices,
          validESFieldsChoices,
          validTargetFieldChoices,
          needsServerDelete: true,
          updatedTimestamp: serverFieldMapping.updatedTs
        })
      }))

      this.mappingProps.onAddRow = () => {
        const mapping = this.getInitialMappingState({
          targetFields,
          leftSelectedChoice: null,
          rightSelectedChoice: null,
          leftSelectedChoices: _.map(this.mappingProps.mappings, (m) => m.leftSide.selectedChoice),
          rightSelectedChoices: _.map(this.mappingProps.mappings, (m) => m.rightSide.selectedChoice),
          validESFieldsChoices,
          validTargetFieldChoices,
          needsServerDelete: false,
          updatedTimestamp: null
        })

        this.mappingProps.mappings.push(mapping)
      }
    })
  }

  setUnsavedChanges() {
    this.$timeout(() => {
      this.unsavedChanges = true
    })
  }

  /**
   * Load mapping from server.
   */
  loadMapping() {
    this.loadedMapping = false
    let fieldMappingP
    let targetFieldsP
    let possibleEsFieldsP
    let marketoFixedMappingsP

    if (this.isSalesforceSettingsPage()) {
      this.mappingProps = {
        mapFrom: 'EverString field',
        mapTo: 'Salesforce field',
        mappings: [],
      }

      fieldMappingP = this.fieldMappingService.getSFDCMapping({
        targetObject: this.getTargetObject(),
      })
      targetFieldsP = this.getSchemaForSfdcObjectAsync(this.getTargetObject())
      possibleEsFieldsP = this.fieldMappingService.fetchPossibleFields({ targetType: 'salesforce' })
    } else if (this.isMarketoSettingsPage()) {
      // React props to be passed to the `Mapping` component
      this.mappingProps = {
        mapFrom: 'EverString Field',
        mapTo: 'Marketo Field',
        mappings: [],
      }
      fieldMappingP = this.fieldMappingService.getMarketoMapping({ targetType: this.selectedIntegration.value })
      targetFieldsP = this.marketoService.getMarketoFieldsAsync({ integrationType: this.selectedIntegration})
      possibleEsFieldsP = this.fieldMappingService.fetchPossibleFields({ targetType: this.selectedIntegration.value })
      marketoFixedMappingsP =
        this
          .marketoService.getFixedMappings()
          .then((mappingObj) => {
            // `mappingObj` is an objectg of key-value pairs of our Marketo fixed mapping fields.
            return Object.keys(mappingObj).map((esFieldValue) => {

              return {
                key: guid(),
                leftSide: {
                  choices: [],
                  isFixedChoice: true,
                  selectedChoice: {
                    value: esFieldValue,
                    label: insightDisplayName(esFieldValue),
                  }
                },
                rightSide: {
                  choices: [],
                  isFixedChoice: true,
                  selectedChoice: {
                    value: mappingObj[esFieldValue],
                    label: insightDisplayName(mappingObj[esFieldValue]),
                  }
                }
              }
            }
            )
          })
    }

    fieldMappingP.then((serverFieldMappings) => {
      this.$timeout(() => {
        this.mappingProps.numLoadingRows = serverFieldMappings.length
      })
    })

    return Promise
      .all([
        fieldMappingP,
        targetFieldsP,
        possibleEsFieldsP,
        marketoFixedMappingsP,
      ])
      .then(([serverFieldMappings, targetFields, possibleEsFields, marketoFixedMappings]) => {
        this.loadedMapping = true
        this.currentServerFieldMappings = _.cloneDeep(serverFieldMappings)

        return this.initMappingTable({
          serverFieldMappings, targetFields, possibleEsFields, fixedMappings: marketoFixedMappings})
      })
      .catch((err) => {
        this.loadedMapping = true
      })
  }

  convertMappingPropsToServerMapping(mappings) {
    return mappings
      .filter((mapping) => {
        return getChoiceFromSide(mapping.leftSide) && getChoiceFromSide(mapping.rightSide)
      })
      .filter((mapping) => !(mapping.leftSide.isFixedChoice && mapping.rightSide.isFixedChoice))
      .map((mapping) => {
        return {
          targetField: mapping.rightSide.selectedChoice.value,
          insightId: mapping.leftSide.selectedChoice.insightId,
          modelId: mapping.leftSide.selectedChoice.modelId,
        }
      })
  }

  deleteMappingAsync(leftChoice) {
    const mappingToDelete = _.find(this.currentServerFieldMappings, (mapping) => {
      if (leftChoice.insightId) {
        return mapping.insightId === leftChoice.insightId
      } else if (leftChoice.modelId) {
        return mapping.modelId === leftChoice.modelId
      }
    })

    const pickKeys = ['targetField']
    if (leftChoice.insightId) {
      pickKeys.push('insightId')
    } else {
      pickKeys.push('modelId')
    }

    const targetField = _.pick(mappingToDelete, pickKeys)
    let deleteP

    if (this.isSalesforceSettingsPage()) {
      deleteP = this.fieldMappingService.deleteSFDCMapping({
        targetObject: this.getTargetObject(),
        targetFields: [targetField],
      })
    } else if (this.isMarketoSettingsPage()) {
      deleteP = this.fieldMappingService.deleteMarketoMapping({
        targetType: this.selectedIntegration.value,
        targetFields: [targetField],
      })
    }

    return deleteP
      .then((res) => {
        this.currentServerFieldMappings = res
        this.alertService.addAlert('success', `${leftChoice.label} mapping deleted.`)
      })
  }

  confirmSaveMapping() {
    let message

    const currentObject = this.getCurrentSfdcObjectStr()
    if (this.isSalesforceSettingsPage()) {
      message = `<div>
                  This will change field mapping(s) in all future publishes to your Salesforce ${currentObject} object.
                  </br>
                  Continue?
                </div>`
    } else if (this.isMarketoSettingsPage()) {
      message = `<div>
                    This will change field mapping(s) in all future publishes to Marketo.
                 </br>
                 Continue?
                </div>`
    }
    const params = {
      title: 'Please Confirm Changes',
      message,
      buttonText: {
        confirm: 'Ok',
        deny: 'Cancel'
      },
      onConfirm: (result, close) => {
        this.saveMappingAsync()
        close()
      }
    }
    this.sms.confirmationModal(params)
  }

  saveMappingAsync() {
    const newServerMappings = this.convertMappingPropsToServerMapping(this.mappingProps.mappings)
    this.savingMapping = true

    let saveMappingP

    if (this.isSalesforceSettingsPage()) {
      saveMappingP = this
        .fieldMappingService
        .saveSFDCMapping({
          targetObject: this.getTargetObject(),
          targetFields: newServerMappings,
        })
    } else if (this.isMarketoSettingsPage()) {
      saveMappingP = this
        .fieldMappingService
        .saveMarketoMapping({
          targetType: this.selectedIntegration.value,
          targetFields: newServerMappings
        })
    }

    return saveMappingP
      .then((res) => {
        this.currentServerFieldMappings = res
        this.alertService.addAlert('success', 'Mapping Saved!')
        _.forEach(newServerMappings, (newMapping) => {
          const mappingRowSavedToServer = _.find(this.mappingProps.mappings, (m) => {
            const choice = getChoiceFromSide(m.rightSide)
            return choice && choice.value === newMapping.targetField
          })
          if (mappingRowSavedToServer) {
            mappingRowSavedToServer.needsServerDelete = true
            mappingRowSavedToServer.leftSide.isFixedChoice = true
            // Need to search though the response array of mappings to find the corresponding updatedTs
            const serverMapping = _.find(this.currentServerFieldMappings, (m) => {
              return m.targetField === newMapping.targetField
            })
            mappingRowSavedToServer.updatedTimestamp = serverMapping.updatedTs
          }
        })
        this.unsavedChanges = false
      })
      .catch((err) => {
        this.alertService.addAlert('error', 'Mapping failed to save.')
      })
      .finally(() => {
        this.savingMapping = false
      })
  }

  /**
   * Load the mapping for the router state passed in.
   */
  loadMappingForSfdcObject(state) {
    // An array of the ES Fields which have an ES field but no corresponding target field.
    this.esFieldsWithoutTargetField = []
    if (_.includes([ACCOUNT_TAB, LEAD_TAB, CONTACT_TAB], state.name)) {
      if (loadMappingP) {
        loadMappingP.cancel()
      }

      loadMappingP = this
        .loadMapping()
        .then(() => {
          // Show a warning if we have mappings where the left side has values but the right side doesn't.

          // we want to make sure we don't show this warning if user changes state
          // right before it pops up
          const stateNotChanged = this.$state.is(state.name)
          const shouldShowWarning = !_.isEmpty(this.esFieldsWithoutTargetField) && stateNotChanged
          if (shouldShowWarning) {
            const fields = this.esFieldsWithoutTargetField.join(', ')
            const object = this.getCurrentSfdcObjectStr()
            const modalMessage = `<div style="max-width: 70rem">
                                    We couldn't find target fields in your
                                    Salesforce ${object} object for the following ES fields:
                                    </br>
                                    <b class="error-color">${fields}</b>
                                    </br>
                                    These will not be included in publish until you
                                    save new mappings for them.
                                  </div>`
            const params = {
              title: 'Target Fields Not Found',
              message: modalMessage,
              messageType: 'warning',
              buttonText: {
                confirm: 'Ok'
              },
            }
            this.sms.messageModal(params)
          }
        })
    }
  }

  /**
   * Load mapping on tab change. For Salesforce, we change tabs between different
   * objects.
   */
  listenForTabChange() {
    this.$scope.$on(
      '$stateChangeStart',
      (evt, toState, toParams, fromState, fromParams) => {
        if (toState.redirectTo) {
          return
        }

        if (this.unsavedChanges) {
          // Prevent state transition
          evt.preventDefault()

          this.sms.showUnsavedChangesWarning({
            onConfirm: (result, close) => {
              this.unsavedChanges = false
              this.$state.go(toState)

              close()
            }
          })
        }
      })

    this.$scope.$on(
      '$stateChangeSuccess',
      (evt, toState, toParams, fromState, fromParams) => {
        this.loadMappingForSfdcObject(toState)
      })
  }

  /**
   * Get the choice that's been selected for an ES field from a mapping.
   */
  getSelectedEsFieldChoice({ serverFieldMapping }) {
    if (serverFieldMapping.insightId) {
      // TODO: Unify these object definitions so that we don't need to remember
      // to add `camelCaseName` or `insightId`.
      return {
        insightId: serverFieldMapping.insightId,
        camelCaseName: serverFieldMapping.displayName,
        value: serverFieldMapping.insightId,
        label: insightDisplayName(serverFieldMapping.displayName),
      }
    } else {
      return {
        modelId: serverFieldMapping.modelId,
        value: serverFieldMapping.modelId,
        label: `Model: ${serverFieldMapping.displayName}`,
      }
    }
  }

  /**
   * Get the choices for the fields we can map from.
   */
  getChoicesFromPossibleFields({ possibleEsFields }) {
    return possibleEsFields.insights
      .map((field) => ({
        insightId: field.id,
        camelCaseName: field.displayName,
        value: field.id,
        label: insightDisplayName(field.displayName),
      }))
      .concat(possibleEsFields.models.map((field) => ({
        modelId: field.id,
        value: field.id,
        label: `Model: ${field.name}`,
      })))
  }

  /**
   * Get the current tab's Salesforce Object.
   */
  getTargetObject() {
    // Marketo only has leads, so we return lead here.
    if (this.isMarketoSettingsPage()) {
      return 'lead'
    }

    if (this.$state.is(ACCOUNT_TAB)) {
      return 'account'
    } else if (this.$state.is(LEAD_TAB)) {
      return 'lead'
    } else if (this.$state.is(CONTACT_TAB)) {
      return 'contact'
    }
  }

  /**
   * Get the schema for the current Salesforce object.
   */
  getSchemaForSfdcObjectAsync() {
    const forbiddenSfdcFields = [
      'Website',
      'Name',
      'Company',
    ]
    return this
      .$http({
        method: 'GET',
        url: `${config.apiBaseURL}salesforce/schema/${this.getTargetObject()}`,
      })
      .then((res) => {
        const data = res.data.data

        return data
          .filter((datum) => {
            return datum.updateable && datum.type !== 'reference'
          })
        // Never allow mapping to certain fields
          .filter((datum) => {
            return !_.includes(forbiddenSfdcFields, datum.name)
          })
          .map((datum) => {
            return {
              // The unique identifier for this Salesforce field
              value: datum.name,
              // The human-readable label for this field.
              label: datum.label,
              // The type of this field. 'string', etc.
              type: datum.type,
            }
          })
      })
  }
}
