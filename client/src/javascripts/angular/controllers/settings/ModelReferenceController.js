import _ from 'lodash'

class ModelReferenceController {
  /* @ngInject */
  constructor(modelService, userService, alertService) {
    this.modelService = modelService
    this.accountId = userService.accountId()
    this.alertService = alertService
    this.initModelRefTable()
  }

  initModelRefTable() {
    this.loading = true
    this.modelService.getList(this.accountId)
      .then((fitModels) => {
        this.completedModels = _.filter(fitModels, (model) => {
          return model.status === 'COMPLETED'
        })
        this.loading = false
      }, () => {
        this.alertService.addAlert('error', 'Error getting model data.')
      })
  }
}

export default ModelReferenceController