import {Intercom} from 'intercom'
import _ from 'lodash'
import {publicPages} from 'enums'
import {getParameterByKey} from 'utils'

class TopController {
  /* @ngInject */
  constructor($state, $scope,
              $location,
              ModalService,
              userService,
              salesforceService,
              apiBaseUrl,
              $q) {
    this.$state = $state
    this.$location = $location
    this.$q = $q
    this.userService = userService
    this.salesforceService = salesforceService
    this.apiBaseUrl = apiBaseUrl
    // should not show header and main-view before TopController has been inited
    this.inited = false

    // Docs: https://github.com/angular-ui/ui-router/wiki
    $scope.$on('$stateChangeSuccess', (evt, toState, toParams, fromState, fromParams) => {
      Intercom.update()

      // Close modals whenever we change pages b/c modals are tied to the page they're opened on.
      ModalService.closeAll()

      if (this.isLoginPage() || !this.inited) {
        this.init()
      }

      // Don't want to have to worry about the impact of responsive components on unresponsive V1 pages.
      // We've defined the .static class on the root element to disable responsive behavior.
      if (this.isV1OnlyPage()) {
        document.documentElement.className = "static"
      } else {
        document.documentElement.className = ""
      }

      // Since EAP is an SPA, we need to manually tell mouseflow we're going onto a new page
      if (_.includes(['prod', 'demo'], process.env.ENV)) {
        const url = toState.url
        mouseflow.newPageView(url)
      }
    })


    $scope.$on('ghosting', function() {
        $scope.$broadcast('g_ghosting')
    })
  }

  shouldShowHeader() {
    return this.inited && !this.isPublicPage() && !this.isUserPage()
  }

  isUserPage() {
    return this.$state.is('user.login') ||
      this.$state.is('user.sign-up') ||
      this.$state.is('user.pass-sent')
  }

  is404Page() {
    return this.$state.is('404')
  }

  isTimeoutPage() {
    return this.$state.is('timeout')
  }

  isConfirmPage() {
    return this.$state.is('chrome-confirm')
  }

  isLoginPage() {
    return this.$state.is('user.login')
  }

  isPublicPage() {
    return _.includes(publicPages, this.$location.path())
  }

  isV1OnlyPage() {
    return this.$state.includes('segment')
  }

  getPageClass() {
    if (this.is404Page()) {
      return 'page-404'
    }
    if (this.isUserPage()) {
      return 'page-user'
    }
    if (this.isTimeoutPage()) {
      return 'page-timeout'
    }
    if (this.isConfirmPage()) {
      return 'page-chrome-confirm'
    }
  }

  init() {
    const userServiceP = this.userService
      .fetchData()
      .catch(() => {
        this.inited = true
        if (!this.isPublicPage()) {
          this.$state.go('user.login')
        }
        return this.$q.reject()
      })

    return userServiceP
      .then(() => {
        // Redirect to Zendesk if needed.
        const zendeskRedirectLink = getParameterByKey('return_to')
        if (zendeskRedirectLink) {
          window.location.href = `${this.apiBaseUrl}zendesk/go_to?url=${zendeskRedirectLink}`
          return this.$q.reject()
        }
        // The salesforce service needs to access the currently logged in user,
        // so it depends on the user service being ready.
        return this.salesforceService.fetchData()
      })
      .then(() => {
        if (this.isLoginPage()) {
          this.$state.go('audience.console')
        }
        this.inited = true
      })
  }
}

export default TopController
