class SegmentWelcomeController {
  /* @ngInject */
  constructor(segmentService) {
    this.segmentService = segmentService
    this.showConsoleLink = false
    this.inited = false
    this.init()
  }

  init() {
    this.segmentService.getAllAsync().then(() => {
      const data = this.segmentService.segments
      if (data.length) {
        this.showConsoleLink = true
      }
      this.inited = true
    })
  }
}

export default SegmentWelcomeController
