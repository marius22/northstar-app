import SlideController from '../modals/SlideController'
import _ from 'lodash'

class SegmentSettingController extends SlideController {
  /* @ngInject */
  constructor(close,
              segment,
              saveSuccess,
              apiBaseUrl,
              $document,
              userService,
              $timeout,
              $http,
              ghostService,
              alertService) {
    super(close, $document, $timeout)
    this.segment = segment
    this.saveSuccess = saveSuccess
    this.apiBaseUrl = apiBaseUrl
    this.userService = userService
    this.ghostService = ghostService
    this.alertService = alertService
    this.$http = $http
    this.$timeout = $timeout

    this.listUsers = []
    this.selectedUser = {}
    this.openSlide()
  }

  init() {
    const successHandler = (response) => {
      const ownerId = this.segment.ownerId
      this.listUsers = this.listUsers.concat(response.data.data)
      this.selectedUser = _.find(this.listUsers, (item) => {
        return item.id === ownerId
      })
    }
    if (this.ghostService.isGhosting()) {
      const ghostId = this.ghostService.ghostId()
      this.userService.getUsersByAccountId(ghostId).then(successHandler)
    }
    this.userService.getUsersByMyAccount().then(successHandler)
  }

  saveSettingSlide() {
    if (!_.isEmpty(this.selectedUser)) {
      const segmentId = this.segment.segmentId
      this.$http.put(`${this.apiBaseUrl}segments/${segmentId}/change_owner`, {
        userId: this.selectedUser.id
      }).success(() => {
        this.saveSuccess(segmentId)
        this.closeSlide()
      }).error((response) => {
        this.alertService.addAlert('error', response.message, '#slide-alert')
      })
    } else {
      this.alertService.addAlert('error', 'Please select an owner', '#slide-alert')
    }
  }

  selectUser(index) {
    this.selectedUser = this.listUsers[index]
  }
}

export default SegmentSettingController
