import * as enums from 'enums'
import _ from 'lodash'
import angular from 'angular'

// Number of segments to show on each page.
const NUM_PER_PAGE = 8
class SegmentListController {
  /* @ngInject */
  constructor($http,
              apiBaseUrl,
              alertService,
              segmentService,
              userService,
              showModalService,
              $state,
              $location,
              $filter,
              $interval,
              ModalService,
              ghostService) {

    this.$http = $http
    this.$state = $state
    this.$filter = $filter
    this.apiBaseUrl = apiBaseUrl
    this.alertService = alertService
    this.segmentService = segmentService
    this.$interval = $interval
    this.modalService = ModalService
    this.ghostService = ghostService

    this.$state = $state
    this.$location = $location
    this.userService = userService
    this.sms = showModalService
    this.data = []
    this.interval = null
    /* Pagination */
    // Number of pages of segments.
    this.numPages = 0
    // Segments for the current page.
    this.curPageSegments = []
    this.curPage = 1

    // Segment status enum.
    this.SegmentStatus = enums.SegmentStatus

    // Mapping from segment status to css class name.
    const statusMap = {
      [enums.SegmentStatus.EMPTY]: 'empty',
      [enums.SegmentStatus.COMPLETED]: 'completed',
      [enums.SegmentStatus.DRAFT]: 'draft',
      [enums.SegmentStatus.IN_PROGRESS]: 'in-progress'
    }

    this.statusMap = statusMap
    // generate buttons that is in loading status
    this.generateDisabledButtons = []

    // While the segments are loading, show a spinner.
    this.inited = false
    this.init()
    this.cardFilterQuery = ''
  }

  init() {
    this.update()
  }

  /**
   * Different headers will use different backgrounds depending on the kind of es-segment
   * it is.
   */
  getHeaderType(segment) {
    if (segment.status === 'DRAFT') {
      return 'draft'
    } else if (segment.isESSegment) {
      return 'es-admin'
    }

    return 'galaxy'
    // return `galaxy-bg-${segment.galaxyBgImageNumber}`
  }

  /**
   * Show a human readable number or '--' for missing data.
   */
  getDispNum(num) {
    if (!num) {
      return '--'
    }

    if (num > 100000) {
      return '100K+'
    }

    return this.$filter('number')(num).toString()
  }

  goToSegment(segment) {
    if (segment.status === 'DRAFT') {
      return
    }

    if (segment.status === 'COMPLETED') {
      this.$state.go('segment.overviewTabs.overview.segmentGraphs',
                     {segmentId: segment.segmentId})
    } else {
      this.$state.go('segment.overviewTabs.seedCompanies',
                     {segmentId: segment.segmentId})
    }
  }

  goToMostRecentDraftStep(segment) {
    const segmentId = segment.segmentId
    const url = `${this.apiBaseUrl}segments/${segmentId}`
    this.$http.get(url).then((response) => {
      const mostRecentStep = response.data.data.segmentStage
      switch (mostRecentStep) {
        case 1:
          this.$state.go('segment.create.qualification', {segmentId})
          break
        case 2:
          this.$state.go('segment.create.upload', {segmentId})
          break
        case 3:
          this.$state.go('segment.create.insights', {segmentId})
          break
        case 4:
          this.$state.go('segment.create.reach', {segmentId})
          break
        case 5:
          this.$state.go('segment.create.summary', {segmentId})
          break
        default:
          this.$state.go('segment.create.qualification', {segmentId})
      }
    })
  }

  procFilter() {
    const filteredData = this.$filter('filter')(this.data, this.cardFilterQuery)
    this.numPages = Math.ceil(filteredData.length / NUM_PER_PAGE)
    this.selectPageHandler(1)
  }

  /**
   * Callback to page selection. After the user tries to change
   * the page to `pageNum`, this function is called. The actual
   * work of switching the page is done here.
   */
  selectPageHandler(pageNum) {
    const newCurPageSegments = this.getSegmentsForPage(pageNum)
    this.curPageSegments = newCurPageSegments
    this.curPage = pageNum
  }

  addSegmentIfAllowed() {
    if (this.userService.atSegmentLimit()) {
      this.alertService.addUpgradeAlert()
    } else {
      this.$state.go('segment.create.name')
    }
  }

  update() {
    let ghostId = undefined
    if (this.ghostService.isGhosting()) {
      ghostId = this.ghostService.ghostId()
    }
    this.segmentService.getAllAsync(ghostId).then(
      () => {
        this.inited = true
        this.data = this.segmentService.segments
        this.canPublish = this.segmentService.canPublish
        this.numPages = Math.ceil(this.data.length / NUM_PER_PAGE)

        // Start at first page of segments
        this.curPageSegments = this.getSegmentsForPage(1)
        // TODO add following line if johny wants
        // this.updateProgress()
      }
    ).catch((res) => {
      this.inited = true
      throw res
    })
  }

  updateProgress() {
    if (!this.interval) {
      this.interval = this.$interval(() => {
        this.data = _.map(this.data, (segment) => {
          return _.extend(segment, {
            progress: this.segmentService.calcProgress(segment)
          })
        })
      }, 1000)
    }
  }

  requestUpgrade() {
    this.sms.requestUpgrade()
  }

  getSegmentsForPage(pageNum) {
    const n = pageNum - 1

    const startIndex = n * NUM_PER_PAGE
    const endIndex = startIndex + NUM_PER_PAGE
    const filteredData = this.$filter('filter')(this.data, this.cardFilterQuery)
    const thisPageSegments = filteredData.slice(startIndex, endIndex)
    return thisPageSegments
  }

  publish(pageIdx) {
    const segment = this.curPageSegments[pageIdx]

    this.segmentService.publishAsync(segment)
      .then((publishedSegment) => {
        this.curPageSegments[pageIdx] = publishedSegment
        this.curPageSegments = angular.copy(this.curPageSegments)
      })
      .catch((res) => {
        this.alertService.addAlert('error', res.data.message)
      })
  }

  unpublish(pageIdx) {
    const segment = this.curPageSegments[pageIdx]

    this.segmentService.unpublishAsync(segment)
      .then((unpublishedSegment) => {
        this.curPageSegments[pageIdx] = unpublishedSegment
        this.curPageSegments = angular.copy(this.curPageSegments)
      })
      .catch((res) => {
        this.alertService.addAlert('error', res.data.message)
      })
  }

  // check whether a generate button is disabled
  generateButtonDisabled(segmentId) {
    return _.includes(this.generateDisabledButtons, segmentId)
  }

  deleteSegment(segmentId) {
    this.$http.delete(`${this.apiBaseUrl}segments/${segmentId}`)
      .success(() => {
        this.alertService.addAlert('success', 'Delete successful')
        this.update()
      })
      .error((res) => {
        this.alertService.addAlert('error', `${res.message}`)
      })
  }

  showSummaryButton(status) {
    return status !== 'DRAFT'
  }

  toSummary(segmentId) {
    this.$state.go('segment.overviewTabs.seedCompanies.creationSummary', {segmentId})
  }

  deleteConfirm(segment, event) {
    const modalMessage = `You are about to permanently delete this segment. <br />
        <b>Are you sure you want to delete [${segment.segmentName}]?</b>`
    const modalParams = {
      title: 'Warning',
      message: modalMessage,
      messageType: 'warning',
      buttonText: {
        confirm: 'Confirm',
        deny: 'Cancel'
      },
      onConfirm: (result, close) => {
        this.deleteSegment(segment.segmentId)
        close()
      }
    }
    this.sms.confirmationModal(modalParams)
  }

  openSettingSlide(segment) {
    this.modalService.showModal({
      controller: 'SegmentSettingController',
      controllerAs: 'slc',
      inputs: {
        segment,
        saveSuccess: this.saveSuccess.bind(this)
      },
      templateUrl: '/templates/segment-setting.html'
    })
  }

  hasSettingItem(segment) {
    return this.userService.isAcctAdmin()
      || this.userService.isSuper()
      || (segment.ownerId === this.userService.userId() &&
      !this.userService.isTrial())
  }

  shouldShowPublish(segment) {
    return this.canPublish && segment.visibility === 'private'
  }

  shouldShowUnPublish(segment) {
    return segment.visibility === 'published' ||
      (segment.visibility === 'shared' && this.userService.isAcctAdmin())
  }

  saveSuccess() {
    //this.data = this.data.filter(item => item.segmentId !== this.editingSegmentId)
    this.alertService.addAlert('success', 'User changes successful', '#topAlert', 3)
    // reload current page
    this.update()
  }

  displayRequestUpgrade() {
    return this.userService.isTrial()
  }

  displayRequestMore() {
    return this.userService.isAcctAdmin()
  }

  displayRequestButton() {
    return this.displayRequestUpgrade() || this.displayRequestMore()
  }
}

export default SegmentListController
