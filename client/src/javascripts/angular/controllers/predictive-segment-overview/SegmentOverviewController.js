class SegmentOverviewController {
  /* @ngInject */
  constructor(alertService,
              $state,
              segmentService,
              showModalService,
              publishService) {
    this.alertService = alertService
    this.segmentId = $state.params.segmentId
    this.segmentService = segmentService
    this.sms = showModalService
    this.publishService = publishService
    this.segmentInfo = {}

    this.initSegment()
  }

  shouldShowExportButton() {
    return !!this.segmentInfo.canExport
  }

  initSegment() {
    this.segmentService.getByIdAsync(this.segmentId).then((response) => {
      this.segmentInfo = response
    })
  }

  /*
   * type is expected to be 'advanced' or 'basic', default is 'basic'
   * */
  exportCompany(type, compTable) {
    this.sms.exportCompany({
      type,
      compTable,
      segmentId: this.segmentId,
      onPublishSuccess: () => {
        this.alertService.addAlert('success', 'We will email you when your export is ready')
      }
    })
  }

  /*
   * type is expected to be 'advanced' or 'basic', default is 'basic'
   * */
  publishAudience(type, compTable) {
    this.sms.publishAudience({
      type,
      compTable,
      segmentId: this.segmentId,
      onPublishSuccess: () => {

      }
    })
  }
}

export default SegmentOverviewController
