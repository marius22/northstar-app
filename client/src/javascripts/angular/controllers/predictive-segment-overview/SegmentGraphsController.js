import $ from 'npm-zepto'
import VennDiagram from 'venn-diagram'
import _ from 'lodash'
import angular from 'angular'

/**
 * Creates the charts that serve as an overview of this segment.
 */
class SegmentGraphsController {
  /* @ngInject */
  constructor($state, $stateParams, $q, $element, $filter, segmentService) {
    this.$state = $state
    this.$element = $element
    this.$filter = $filter
    this.segmentService = segmentService
    this.companySummary = {
      seedsCnt: 0,
      companiesCnt: 0,
      inBoundaryCnt: 0
    }

    $q.all({
      overviewInsights: segmentService.getInsights($stateParams.segmentId, 'recommendation'),
      companySummary: segmentService.getCompanySummary($stateParams.segmentId)
    }).then((result) => {
      const overviewInsights = result.overviewInsights
      const companySummary = result.companySummary

      const {basicInsights, advancedInsights} = overviewInsights
      this.industryData = basicInsights.industry
      this.employeeSizeData = basicInsights.employeeSize
      this.accountsPresentInStates = basicInsights.state
      this.advancedInsights = advancedInsights

      // Currently selected dimension for dislay.
      this.currentAdvancedInsightDimension = 'tech'
      this.setAdvancedInsightsExceptCurrent()

      // Check if there's any advanced insight data to render
      this.hasAdvancedInsightData = (
        _.get(this.advancedInsights,
              [this.currentAdvancedInsightDimension]) || []).length > 0

      this.drawVennDiagram(companySummary)

      this.companySummary = companySummary
    })
  }

  selectAdvancedInsight(dimension) {
    this.currentAdvancedInsightDimension = dimension
    this.setAdvancedInsightsExceptCurrent()
  }

  /**
   * Set the advanced insights you can choose from in the rest of the <select> block
   */
  setAdvancedInsightsExceptCurrent() {
    const dimensionsExceptCurrent = _.filter(
      _.keys(this.advancedInsights),
      (dimension) => {
        return dimension !== this.currentAdvancedInsightDimension
      })

    this.advancedInsightsExceptCurrent = _.pick(
      this.advancedInsights, dimensionsExceptCurrent)
  }

  getDispName(dimension) {
    const dimensionDispNameMap = {
      tech: 'Technology',
      growth: 'Growth Trend'
    }

    return _.get(dimensionDispNameMap, dimension, dimension)
  }

  /**
   * Draw the venn diagram in the `.companies-found-cont`
   */
  drawVennDiagram(companySummary) {
    angular.element(document).ready(() => {
      this.venn = new VennDiagram({
        domId: 'venn-diagram-graphs',
        // The width and height need to be at least this much, otherwise the VD
        // gets cut off on the right.
        width: 450,
        height: 450,
        disableHover: true,
        disableClick: true,
        showDirectionLine: true,
        showMainText: false,
        showOutline: true
      })

      const $similarBusiness = $('<span class="venn-count" />', {
        css: {
          color: '#77ADDA',
          fontWeight: 'semi-bold',
          fontSize: '16px'
        },
        html: ['Business<br/>Similarity<br/>',
               `<b>${this.$filter('number')(companySummary.bizSimilarCnt)}</b>`
              ].join('')
      })
      const $similarTech = $('<span class="venn-count" />', {
        css: {
          color: '#8DC44F',
          fontWeight: 'semi-bold',
          fontSize: '16px'
        },
        html: ['Technology<br/>Similarity<br/>',
               `<b>${this.$filter('number')(companySummary.techSimilarCnt)}</b>`
              ].join('')
      })
      const $similarDepartments = $('<span class="venn-count" />', {
        css: {
          color: '#7280BC',
          fontWeight: 'semi-bold',
          fontSize: '16px'
        },
        html: ['Department<br/>Similarity<br/>',
               `<b>${this.$filter('number')(companySummary.depSimilarCnt)}</b>`
              ].join('')
      })
      this.venn.drawText($similarBusiness.get(0).outerHTML, 42, 18, 'center')
      this.venn.drawText($similarTech.get(0).outerHTML, 350, 35, 'center')
      this.venn.drawText($similarDepartments.get(0).outerHTML, 53, 335, 'center')

      // $vennCont has position:relative from the venn diagram module, but
      // we need it to have position absolute here.
      const $vennCont = $(this.$element[0]).find('.venn-diagram-cont')
      $vennCont.css({
        position: 'absolute'
      })

      this.vennInited = true
    })
  }
}

export default SegmentGraphsController
