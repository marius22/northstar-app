import _ from 'lodash'


class SeedInsightsController {
  /* @ngInject */
  constructor($stateParams,
              tableService,
              alertService,
              segmentService,
              apiBaseUrl,
              $http,
              $state,
              $document,
              $scope) {
    this.alertService = alertService
    this.segmentService = segmentService
    this.$http = $http
    this.$document = $document
    this.apiBaseUrl = apiBaseUrl
    this.tableService = tableService
    this.$state = $state

    this.segmentId = $state.params.segmentId
    this.showGenerateButton = $state.is('segment.create.insights')
    this.initializeTable()

    // Default view type
    this.view = 'graph'

    this.segmentInfo = {}
    this.init()

    // Handle the user going "Back" in his browser
    $scope.$on('$stateChangeStart', (event, toState) => {
      this.view = 'graph'
    })

    segmentService.getInsights($stateParams.segmentId, 'seed')
      .then((overviewInsights) => {
        const {basicInsights, advancedInsights} = overviewInsights
        this.basicInsights = basicInsights
        this.advancedInsights = advancedInsights
        this.insightTypes = ['basic', 'advanced']
        this.setInsights()
      })
  }

  getDimensionsInOrder(insightType) {
    if (insightType === 'basic') {
      const sortOrder = ['state', 'industry', 'employeeSize', 'revenue']
      return _(_.keys(this.basicInsights)).sortBy((dimension) => {
        return sortOrder.indexOf(dimension)
      }).value()
    } else if (insightType === 'advanced') {
      const sortOrder = ['alexaRank', 'tech']
      return _(_.keys(this.advancedInsights)).sortBy((dimension) => {
        return sortOrder.indexOf(dimension)
      }).value()
    }

    return []
  }

  getInsightsInOrder(insightType) {
    const dimensions = this.getDimensionsInOrder(insightType)
    const insights = _.map(dimensions, (d) => {
      return this.insights[d]
    })
    return insights
  }

  setInsights() {
    const nameMap = {
      employeeSize: 'Employee Size',
      state: 'Location',
      industry: 'Industry',
      revenue: 'Revenue',
      alexaRank: 'Alexa Rank',
      tech: 'Technology',
      growth: 'Growth Trend'
    }

    const chartMap = {
      employeeSize: 'donut',
      state: 'usa-map',
      industry: 'bar',
      revenue: 'donut',
      alexaRank: 'column',
      tech: 'column',
      growth: 'column'
    }

    const getInsightType = (dimensionName) => {
      if (_.includes(_.keys(this.basicInsights), dimensionName)) {
        return 'basic'
      } else if (_.includes(_.keys(this.advancedInsights), dimensionName)) {
        return 'advanced'
      }

      throw new Error(`Unrecognized dimensionName: ${dimensionName}`)
    }

    // Set initially active tabs.
    const activeBasicDimension = this.getDimensionsInOrder('basic')[0]
    const activeAdvancedDimension = this.getDimensionsInOrder('advanced')[0]

    this.insights = _(_.merge({}, this.basicInsights, this.advancedInsights))
      .mapValues((data, dimensionName) => {

        return {
          name: dimensionName,
          insightType: getInsightType(dimensionName),
          chartData: data,
          chartType: chartMap[dimensionName],
          dispName: nameMap[dimensionName],
          showChart: _.includes(
            [activeBasicDimension, activeAdvancedDimension], dimensionName)
        }
      }).value()
  }

  init() {
    this.segmentService.getByIdAsync(this.segmentId)
      .then((response) => {
        _.extend(this.segmentInfo, response)
      })
  }

  // In charts view, navigate to the appropriate tab
  navigateTo(insightType, dimensionName) {
    this.insights = _(this.insights).mapValues((insight) => {
      const showChart = (() => {
        if (insight.insightType === insightType && insight.name === dimensionName) {
          return true
        } else if (insight.insightType === insightType) {
          return false
        }
        return insight.showChart
      })()

      return _.merge(insight, {showChart})
    }).value()
  }

  canChangeSeed() {
    return this.segmentInfo.status === 'DRAFT'
  }

  initializeTable() {
    const seedListTableHeader = [
      {
        text: 'Company Name',
        key: 'companyName'
      },
      {
        text: 'Domain',
        key: 'domain'
      },
      {
        text: 'Employee Size',
        key: 'employeeSize'
      },
      {
        text: 'Revenue Range',
        key: 'revenue'
      },
      {
        text: 'Industry',
        key: 'industry'
      },
      {
        text: 'Country',
        key: 'country'
      },
      {
        text: 'State',
        key: 'state'
      },
      {
        text: 'City',
        key: 'city'
      },
      {
        text: 'Technology',
        key: 'tech'
      }
    ]

    const tableConfig = {
      subject: 'Seed Companies',
      header: seedListTableHeader,
      defaultQueryParams: {
        id: this.segmentId
      },
      columnCutoff: 7
    }

    this.seedTable = this.tableService.createTable(tableConfig)
    const crudConfig = this.seedTable.getResourceConfig()
    crudConfig.url = `${this.apiBaseUrl}segments/${this.segmentId}/seed_candidates`
    this.seedTable.configureCrudResource(crudConfig)
  }

  exportCSV() {
    let url = `${this.apiBaseUrl}segments/${this.segmentId}/companies?format=csv`
    const currentParams = this.seedTable.getCurrParams()
    if ('order' in currentParams) {
      const sortParams = `&order=${currentParams.order}`
      url = url + sortParams
    }
    window.open(url)
  }

  toggleView() {
    if (this.isSummaryState()) {
      this.toSeedInsights()
      this.view = 'graph'
    } else {
      this.view = this.view === 'table' ? 'graph' : 'table'
    }
  }

  // abcEfg => Abc Efg
  upperCamel(text) {
    const result = text.replace(/([A-Z])/g, " $1")
    return result.charAt(0).toUpperCase() + result.slice(1)
  }

  download() {
    const url = `${this.apiBaseUrl}segments/${this.segmentId}/seed_candidates?format=csv`
    window.open(url)
  }

  // We make the creation summary a child state of segment.overviewTabs.seedCompanies.
  // This is pretty weird because the table and charts aren't implemented in the same way,
  // and unfortunately makes for inelegant logic, but the tradeoff is that we get to re-use
  // the template and controller code for the summary via ui-router

  showSummaryButton() {
    return this.$state.includes('segment.overviewTabs') && !this.isSummaryState()
  }

  toSeedInsights() {
    this.$state.go('segment.overviewTabs.seedCompanies', {segmentId: this.segmentId})
  }

  toSummary() {
    this.view = 'summary'
    this.$state.go('segment.overviewTabs.seedCompanies.creationSummary', {segmentId: this.segmentId})
  }

  isSummaryState() {
    return this.$state.is('segment.overviewTabs.seedCompanies.creationSummary')
  }

  showCharts() {
    return this.view === 'graph' && !this.isSummaryState()
  }

  showTable() {
    return this.view === 'table' && !this.isSummaryState()
  }

  displayNameForToggle() {
    if (this.isSummaryState()) {
      return 'View Insights'
    }
    if (this.view === 'table') {
      return 'View as Graphs'
    }
    if (this.view === 'graph') {
      return 'View as Table'
    }
  }

  modalTitle(row, field) {
    return `${this.fieldDispName(field)}: <div class="sub-title">${row.companyName}</div>`
  }

  fieldDispName(field) {
    const nameMap = {
      tech: 'Technology'
    }
    return nameMap[field]
  }

}

export default SeedInsightsController
