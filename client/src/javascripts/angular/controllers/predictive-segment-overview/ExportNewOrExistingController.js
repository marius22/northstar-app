import * as errors from 'errors'
import {SALESFORCE} from 'enums'

import SlideController from '../modals/SlideController'

const STEP_NAMES = {
  COMPARE_AGAINST_WHAT: 'COMPARE_AGAINST_WHAT',
  PUBLISH_COMPANIES: 'PUBLISH_COMPANIES',
  PUBLISH_CONTACTS: 'PUBLISH_CONTACTS'
}

class ExportNewOrExistingController extends SlideController {
  /* @ngInject */
  constructor(close, $document, params, $timeout, publishService,
              alertService, showModalService) {
    super(close, $document, $timeout)

    this.ps = publishService
    this.pss = publishService.state
    this.SALESFORCE = SALESFORCE
    this.alertService = alertService
    this.sms = showModalService
    this.STEP_NAMES = STEP_NAMES

    // True if the publish request is ongoing
    this.isPublishPending = false
    this.openSlide()
  }

  /**
   * Show step where you choose what to compare against.
   */
  showCompareAgainstWhatStep(publishType) {
    return this.pss[publishType]
  }

  /**
   * Show step where you choose where to publish companies.
   */
  showPublishCompaniesStep(publishType) {
    return (
      this.showCompareAgainstWhatStep(publishType) &&
      this.ps.isPublishingToSalesforce()
    )
  }

  /**
   * Whether to enable the step where you choose where to publish companies.
   */
  enablePublishCompaniesStep() {
    return this.pss.compareAgainstLeads ||
           this.pss.compareAgainstAccounts
  }

  /**
   * Show step asking where to publish contacts in Salesforce.
   */
  showPublishContactsStep(publishType) {
    if (!this.ps.isPublishingContacts()) {
      return false
    }

    // If you're publishing contacts to CSV, then this step is unneeded.
    if (this.ps.isPublishingToCsv()) {
      return false
    }

    if (this.ps.isPublishingToSalesforce()) {
      return this.showPublishCompaniesStep(publishType)
    }

    return true
  }

  /**
   * The publish contacts step is disabled until you've published companies.
   */
  enablePublishContactsStep() {
    return this.enablePublishCompaniesStep()
  }

  /**
   * Gets the step number to show to the user.
   */
  getStepNumber(newOrExisting, stepName) {
    if (this.ps.isPublishingToSalesforce()) {
      if (stepName === STEP_NAMES.COMPARE_AGAINST_WHAT) {
        return 1
      } else if (stepName === STEP_NAMES.PUBLISH_COMPANIES) {
        return 2
      } else if (stepName === STEP_NAMES.PUBLISH_CONTACTS) {
        return 3
      }
    } else if (this.ps.isPublishingToCsv()) {
      if (stepName === STEP_NAMES.COMPARE_AGAINST_WHAT) {
        return 1
      } else if (stepName === STEP_NAMES.PUBLISH_CONTACTS ||
                 stepName === STEP_NAMES.PUBLISH_COMPANIES) {
        return 2
      }
    }

    throw new Error(`Unknown stepName: ${stepName}`)
  }


  publish() {
    const executePublish = () => {
      this.isPublishPending = true
      this.ps.publish()
        .then(() => {
          this.isPublishPending = false
          SlideController.closeAllSlides()
          this.alertService.addPublishSuccessAlert()
        })
        .catch((err) => {
          this.isPublishPending = false

          if (err instanceof errors.HttpError) {
            this.alertService.addAlert(
              'error',
              err.message,
              '#export-slide-alert')
          } else {
            this.alertService.addAlert(
              'error', 'There was an error while publishing.', '#export-slide-alert')
          }
        })
    }

    if (this.pss.canPublishContacts && this.pss.numContacts > 0) {
      const totalContacts = this.pss.numCompanies * this.pss.numContacts
      this.sms.showConfirmContactsModal({
        onConfirm: (result, close) => {
          executePublish()
          close()
        },
        totalContacts
      })
    } else {
      executePublish()
    }
  }
}

export default ExportNewOrExistingController
