import {Intercom} from 'intercom'

import _ from 'lodash'
import angular from 'angular'
import moment from 'moment'

class CompanyListController {
  /* @ngInject */
  constructor($state,
              tableService,
              alertService,
              apiBaseUrl,
              $http,
              $stateParams,
              userService,
              showModalService,
              segmentService,
              $scope) {
    this.$state = $state
    this.alertService = alertService
    this.tableService = tableService
    this.userService = userService
    this.$http = $http
    this.sms = showModalService
    this.apiBaseUrl = apiBaseUrl
    this.segmentService = segmentService
    this.$scope = $scope

    this.segmentId = $stateParams.segmentId
    this.tableLoading = false

    this.initializeTable()
    this.initializeFilters()

    this.filterSearchQuery = ""

    this.unlockedInsights = []
    userService.fetchData().then(
      () => {
        this.unlockedInsights = userService.getUnlockedInsights()
      }
    )
    this.onlyShowExports = false
    this.deptStrengthDict = {
      1: 'Low',
      5: 'Medium',
      9: 'High'
    }
  }

  /* Get the distinct items for all filterableKeys to populate our filter dropdown */
  initializeFilters() {
    this.filterableKeys = ['revenue', 'employeeSize', 'industry', 'state',
                            'administrative', 'engineering', 'finance', 'hr', 'legal',
                            'marketing', 'operations', 'sales', 'medicalHealth', 'researchDevelopment',
                            'IT', 'growth']

    this.deptStrengthKeys = ['administrative', 'engineering', 'finance', 'hr', 'legal', 'marketing',
                              'operations', 'sales', 'medicalHealth', 'researchDevelopment', 'IT']


    this.filters = {
      categories: {}
    }
    for (const key of this.filterableKeys) {
      this.filters.categories[key] = {
        distinctCounts: []
      }
    }
    const distinctCountsUrl = [
      this.apiBaseUrl,
      `segments/${this.segmentId}`,
      '/companies/distinct_counts',
      `?keys=${this.filterableKeys.join(',')}`
    ].join('')

    // Get the distinct values and counts for our filterable firmographic categories,
    // and set them to be selected by default in the filter dropdown
    this.$http.get(distinctCountsUrl).then(
      (response) => {
        for (const key of this.filterableKeys) {
          const distinctFirmographicCounts = response.data.data[key]
          if (_.includes(this.deptStrengthKeys, key)) {
            _.forEach(distinctFirmographicCounts, (deptObj) => {
              deptObj.value = this.deptStrengthToString(deptObj.value)
            })
          }
          this.filters.categories[key].distinctCounts = distinctFirmographicCounts
          this.selectAll(distinctFirmographicCounts)
        }
      }
    )
  }

  initializeTable() {
    const companyListTableHeader = [
      {
        text: 'Company Name',
        key: 'companyName'
      },
      {
        text: 'URL',
        key: 'domain'
      },
      {
        text: 'Similar Seed Companies',
        key: 'similarCompanies'
      },
      {
        text: 'Technologies',
        key: 'similarTechnologies'
      },
      {
        text: 'Departments',
        key: 'similarDepartments'
      },
      {
        text: 'Fit Score',
        key: 'score'
      },
      {
        text: 'Growth Trend',
        key: 'growth'
      },
      {
        text: 'Administrative Strength',
        key: 'administrative'
      },
      {
        text: 'Engineering Strength',
        key: 'engineering'
      },
      {
        text: 'Finance Strength',
        key: 'finance'
      },
      {
        text: 'HR Strength',
        key: 'hr'
      },
      {
        text: 'IT Strength',
        key: 'IT'
      },
      {
        text: 'Legal Strength',
        key: 'legal'
      },
      {
        text: 'Marketing Strength',
        key: 'marketing'
      },
      {
        text: 'Medical Strength',
        key: 'medicalHealth'
      },
      {
        text: 'Operations Strength',
        key: 'operations'
      },
      {
        text: 'R&D Strength',
        key: 'researchDevelopment'
      },
      {
        text: 'Sales Strength',
        key: 'sales'
      },
      {
        text: 'State',
        key: 'state'
      },
      {
        text: 'City',
        key: 'city'
      },
      {
        text: 'Zip Code',
        key: 'zipcode'
      },
      {
        text: 'Employee Size',
        key: 'employeeSize'
      },
      {
        text: 'Revenue',
        key: 'revenue'
      },
      {
        text: 'Alexa Rank',
        key: 'alexaRank'
      },
      {
        text: 'LinkedIn',
        key: 'linkedinUrl'
      },
      {
        text: 'Industry',
        key: 'industry'
      }
    ]

    const tableConfig = {
      subject: 'Recommendation Companies',
      header: companyListTableHeader,
      defaultQueryParams: {
        id: this.segmentId,
        onlyExported: this.onlyShowExports ? 1 : 0
      }
    }

    this.compTable = this.tableService.createTable(tableConfig)
    const crudConfig = this.compTable.getResourceConfig()
    crudConfig.url = `${this.apiBaseUrl}segments/:id/companies`
    this.compTable.configureCrudResource(crudConfig)
  }

  filterByExports() {
    this.resetTable()
  }

  resetTable() {
    this.compTable.reload({
      id: this.segmentId,
      onlyExported: this.onlyShowExports ? 1 : 0
    }).then(
      () => {
        this.compTable.state.sort = {}
      }
    )
  }

  headerClick(key) {
    if (!this.insightUnlocked(key)) {
      this.sms.requestUpgrade()
    }
  }

  cellClick(companyInfo, field) {
    if (field === 'companyName' && !this.companyUnlocked(companyInfo)) {
      this.sms.requestUpgrade()
    }
  }

  insightUnlocked(key) {
    if (this.userService.isSuper()) {
      return true
    }
    return this.unlockedInsights.indexOf(key) >= 0
  }

  allowTableActions(key) {
    return this.insightUnlocked(key) && !this.userService.isTrial()
  }

  displayInsightData(companyInfo, key) {
    let ret
    if (this.insightUnlocked(key) && this.companyUnlocked(companyInfo)) {
      ret = companyInfo[key]
    } else {
      ret = key === 'companyName' ? 'UNLOCK' : 'Hidden Insight'
    }
    return ret
  }

  companyUnlocked(companyInfo) {
    if (this.userService.isSuper()) {
      return true
    }
    return companyInfo['companyName'] !== 'LOCKED_CMP'
  }

  showLockInDataCell(companyInfo, field) {
    const companyIsLocked = !this.companyUnlocked(companyInfo)
    return field === 'companyName' && companyIsLocked
  }

  lockedInsightData(companyInfo, field) {
    const insightIsLocked = !this.insightUnlocked(field)
    const companyIsLocked = !this.companyUnlocked(companyInfo)
    return (insightIsLocked || companyIsLocked) && field !== 'companyName'
  }

  /* Filter and dropdown logic */

  isFilterable(key) {
    return this.filterableKeys.indexOf(key) >= 0
  }

  toggleDropdown(key = "") {
    this.dropdownColumn = key
    this.filterSearchQuery = ""
  }

  // Disallow the dropdown toggle from propagating to child elements,
  // so that we don't toggle the dropdown by clicking inside the dropdown
  blockDropdownToggle($event) {
    $event.stopPropagation()
  }

  toggleSelectAll(key) {
    const items = this.filters.categories[key].distinctCounts
    if (this.allFilterValuesSelected(key)) {
      this.deselectAll(items)
    } else {
      this.selectAll(items)
    }
  }

  selectAll(items) {
    for (const item of items) {
      item.selected = true
    }
  }

  deselectAll(items) {
    for (const item of items) {
      item.selected = false
    }
  }

  filterIsActive(key) {
    const filterInfo = this.compTable.getCurrParams()["where"]
    if (filterInfo) {
      return key in filterInfo
    }
    return false
  }

  allFilterValuesSelected(key) {
    const items = this.filters.categories[key].distinctCounts
    for (const item of items) {
      if (!item.selected) {
        return false
      }
    }
    return true
  }

  noFilterValuesSelected(key) {
    const items = this.filters.categories[key].distinctCounts
    for (const item of items) {
      if (item.selected) {
        return false
      }
    }
    return true
  }


  /*
   Takes the column key and reloads table with data filtered in respect to both of the following:
   1. The criteria specified in the filter dropdown
   2. The criteria of the filters that are already active

   If allFilterValuesSelected() = true, then that means the user either is trying to remove an existing filter,
   or is simply clicking Apply for no reason, in which case nothing happens.
   */
  applyFilter(key) {
    const currentParams = this.compTable.getCurrParams()
    if (!this.allFilterValuesSelected(key)) {
      const filterForKey = {"$in": []}
      const items = this.filters.categories[key].distinctCounts
      for (const item of items) {
        if (item.selected) {
          let val = item.value
          if (_.includes(this.deptStrengthKeys, key)) {
            val = this.deptStrengthToNum(val)
          }
          filterForKey["$in"].push(val)
        }
      }
      const updatedParamsWithFilter = {}
      const filterIsCurrentlyActive = ("where" in currentParams) && (Object.keys(currentParams["where"]).length > 0)
      angular.extend(updatedParamsWithFilter, currentParams)
      if (!filterIsCurrentlyActive) {
        updatedParamsWithFilter["where"] = {}
      }

      updatedParamsWithFilter["where"][key] = filterForKey
      this.compTable.reload(updatedParamsWithFilter).then(
        () => {
          this.toggleDropdown()
        }
      )
    } else {
      const filterRemovedParams = {}
      angular.extend(filterRemovedParams, currentParams)
      if ("where" in currentParams) {
        delete filterRemovedParams.where[key]
      }
      this.compTable.reload(filterRemovedParams).then(
        () => {
          this.toggleDropdown()
        }
      )
    }
  }

  transformDate(dateStr) {
    return moment(new Date(dateStr)).format('MMMM. Do, YYYY')
  }

  dispute(row, fieldName) {
    let method = 'post'
    if (this.isDisputed(row, fieldName)) {
      method = 'delete'
    }
    this.segmentService.dispute(this.segmentId, {
      companyId: row.companyId,
      fieldName
    }, method).then((response) => {
      _.map(this.compTable.data, (item, index) => {
        if (item.companyId === row.companyId) {
          if (method === 'post') {
            item['disputes'].push(response.data.data)
          } else {
            this.compTable.data[index]['disputes'] =
              _.filter(item['disputes'], (dispute) => {
                return dispute.fieldName !== fieldName
              })
          }
        }
      })
    })
  }

  isDisputed(row, fieldName) {
    const data = _.filter(row['disputes'], (item) => {
      return item.fieldName === fieldName
    })
    return data.length > 0
  }

  modalTitle(row, field) {
    return `${this.fieldDispName(field)}: <div class="sub-title">${row.companyName}</div>`
  }

  fieldDispName(field) {
    const nameMap = {
      similarCompanies: 'Similar Seed Companies',
      similarTechnologies: 'Tech Expansion',
      similarDepartments: 'Department Expansion',
      tech: 'Technology'
    }
    return nameMap[field]
  }

  deptStrengthToNum(val) {
    return _.invert(this.deptStrengthDict)[val]
  }

  deptStrengthToString(val) {
    return this.deptStrengthDict[val]
  }


  // Some temporary fixes here. Not relevant for long.
  shareTableSettings() {
    const readableInfo = this.compTable.shareTableParams()
    const segmentName = this.$scope.lc.segment.segmentName
    const filterString = JSON.stringify(readableInfo)
    const message = `Table settings for segment ${segmentName} (ID: ${this.segmentId}): ${filterString}`
    Intercom.showNewMessage(message)
  }

  canShareTableSettings() {
    const tableParams = this.compTable.getCurrParams()
    return tableParams && (tableParams.order || tableParams.where)
  }
}

export default CompanyListController
