class LayoutController {
  /* @ngInject */
  constructor($scope,
              segmentService,
              $stateParams) {
    this.segmentService = segmentService

    const segmentId = $stateParams.segmentId
    this.segmentService.getByIdAsync(segmentId).then((segment) => {
      this.segment = segment
    })

    // These $stateChange* events are documented here: http://bit.ly/20CHcVw
    $scope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
      this.segmentService.isLoading = true
    })
    $scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
      this.segmentService.isLoading = false
    })

    // START GALAXY_DEMO_CODE
    const productionSegmentId = '47'
    this.showDemoGalaxy = segmentId === productionSegmentId
    // END GALAXY_DEMO_CODE
  }
}

export default LayoutController
