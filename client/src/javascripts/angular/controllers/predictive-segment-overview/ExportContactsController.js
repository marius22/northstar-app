import SlideController from '../modals/SlideController'
import _ from 'lodash'
import deepcopy from 'deepcopy'
import equal from 'deep-equal'

class ExportContactsController extends SlideController {
  /* @ngInject */
  constructor($document, $timeout, $http, close, publishService) {
    super(close, $document, $timeout)

    this.openSlide()

    this.pss = publishService.state
    this.ps = publishService

    this.state = {
      // The current title being input.
      titleKeyword: '',
      // The number of contacts to be exported.
      numContacts: this.pss.numContacts,
      // The job titles that we want contacts to have.
      titles: deepcopy(this.pss.titles),
    }
  }

  onKeyDown(evt) {
    if (evt.keyCode === 13) {
      this.addTitle()
    }
  }

  addTitle() {
    if (!this.state.titleKeyword) {
      return
    }

    this.state.titles.push(this.state.titleKeyword)
    this.state.titleKeyword = ''
  }

  removeTitle(title) {
    _.remove(this.state.titles, (t) => {
      return t === title
    })
  }

  changesAreValid() {
    return this.stateChanged() && this.validNumber()
  }

  validNumber() {
    const nonNegative = this.state.numContacts >= 0
    return _.isInteger(this.state.numContacts) && nonNegative
  }

  stateChanged() {
    return (this.state.numContacts !== this.pss.numContacts ||
            !(equal(this.state.titles, this.pss.titles)))
  }

  saveChanges() {
    this.pss.numContacts = this.state.numContacts
    this.pss.titles = this.state.titles
    this.closeSlide()
  }
}

export default ExportContactsController
