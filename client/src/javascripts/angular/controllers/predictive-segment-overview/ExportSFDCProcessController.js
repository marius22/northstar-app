/*
  the main logic is based on this graph. http://bit.ly/1Tifb6f
 */
import {CLOSE, SFDCLabels} from 'enums'
import ModalBaseController from '../modals/ModalBaseController'
import {getIconName} from 'enums'

class ExportSFDCProcessController extends ModalBaseController {
  constructor($scope,
              params,
              close,
              salesforceService,
              userService,
              $q) {
    super(close, $scope)
    this.params = params
    this.salesforceService = salesforceService
    this.userService = userService
    this.$q = $q
    this.pending = false
    this.labels = SFDCLabels

    this.checkOauth()
  }

  // main logic start
  checkOauth() {
    if (this.params.SFDCIntegrated) {
      this.checkPackage()
    } else {
      // if not integrated with sfdc, update the ui and actions of the cancel/confirm button
      let SFDCPackageWin
      let lockDeny = false
      this.updateLabel('NO_OAUTH')
      this.onDeny = () => {
        if (lockDeny) {
          return
        }
        this.pending = false
        if (SFDCPackageWin) {
          SFDCPackageWin.cancel()
        }
        this.onCancel()
      }
      this.onConfirm = () => {
        if (this.pending) {
          return
        }
        this.pending = true
        this.updateLabel('WAITING_OAUTH')
        SFDCPackageWin = this.salesforceService.openSFDCOauthWindow(() => {
          lockDeny = true
          this.checkOauthAsync().then(() => {
            this.pending = false
            this.checkPackage()
            lockDeny = false
          }, () => {
            this.pending = false
            this.messageClass = 'error'
            this.message = 'Oh Snap! There was a problem connecting to sfdc. <br>Would you like to try again?'
            lockDeny = false
          })
        })
      }
    }
  }

  // check whether integrated with Salesforce account  by calling the api
  // we need checkPackageAsync whenever checkOauthAsync
  checkOauthAsync() {
    const defer = this.$q.defer()
    if (this.params.SFDCIntegrated) {
      this.checkPackageAsync().finally(() => {
        defer.resolve()
      })
      return defer.promise
    }

    const finishDefer = () => {
      if (this.params.SFDCIntegrated) {
        defer.resolve()
      } else {
        defer.reject()
      }
    }

    this.$q.all({
      integrated: this.salesforceService.isIntegrated().then((response) => {
        this.params.SFDCIntegrated = response.data.data === true
      }),
      installed: this.checkPackageAsync()
    }).finally(finishDefer)
    return defer.promise
  }

  checkPackage() {
    const SFDCPackage = this.params.SFDCPackage
    const allowedCode = 'HAVE_PERMISSION_INSTALL_PACKAGE'
    if (SFDCPackage.installed) {
      this.params.onSuccess()
    } else if (SFDCPackage.code === allowedCode) {
      // only in this case should we allow the user open a new window.
      let oauthWin
      let lockDeny = false
      this.updateLabel(SFDCPackage.code)
      this.onDeny = () => {
        if (lockDeny) {
          return
        }
        this.pending = false
        if (oauthWin) {
          oauthWin.cancel()
        }
        this.onCancel()
      }
      this.onConfirm = () => {
        if (this.pending) {
          return
        }
        this.pending = true
        this.updateLabel('WAITING_INSTALL')
        oauthWin = this.salesforceService.openInstallPackageWindow(() => {
          lockDeny = true
          this.checkPackageAsync().then(() => {
            this.pending = false
            this.updateLabel('INSTALL_PACKAGE_SUCCESS')
            this.onConfirm = () => {
              lockDeny = false
              this.close()
              this.params.onSuccess({
                SFDCIntegrated: this.params.SFDCIntegrated,
                SFDCPackage: this.params.SFDCPackage
              })
            }
          }, () => {
            this.updateLabel(this.params.SFDCPackage.code)
            this.pending = false
            lockDeny = false
          })
        })
      }
    } else {
      // if there is some error we can't deal in this process, just tell the user contact
      // EverString Customer Success Team and close the modal
      this.updateLabel(this.params.SFDCPackage.code)
    }
  }

  // check whether EverString package is installed by calling the api
  checkPackageAsync() {
    if (this.params.SFDCPackage.installed) {
      return this.$q.defer().resolve()
    }
    const userId = this.userService.userId()
    const defer = this.salesforceService.packageInstalled(userId)
    defer.then(() => {
      this.params.SFDCPackage = {
        installed: true,
        code: '',
        message: ''
      }
    }, (response) => {
      this.params.SFDCPackage = {
        installed: false,
        code: response.data.code || '',
        message: response.data.message || response.statusText
      }
    })
    return defer
  }
  // main logic end

  /*
    when user click cancel or close button, close the modal,
    and notify the focus slide to do something necessary, namely this.params.onCancel here.
   */
  onCancel() {
    this.close()
    this.params.onCancel({
      SFDCIntegrated: this.params.SFDCIntegrated,
      SFDCPackage: this.params.SFDCPackage
    })
  }

  // show corresponding text on the modal in different cases.
  updateLabel(type) {
    const label = this.labels[type] || this.labels.DEFAULT
    // update label explicitly
    this.title = label.title
    this.messageClass = label.messageClass
    this.message = label.message
    this.btnConfirm = label.btnConfirm || 'Yes'
    this.hideDenyButton = !!label.hideDenyButton
    if (this.btnConfirm === CLOSE) {
      this.onConfirm = this.onCancel
    }
  }

  getIconName() {
    const messageClass = this.messageClass
    return getIconName(messageClass)
  }
}

ExportSFDCProcessController.$inject = [
  '$scope',
  'params',
  'close',
  'salesforceService',
  'userService',
  '$q'
]

export default ExportSFDCProcessController
