import {componentsDetail, vennMap} from 'enums'
import {PUBLISH_METHODS} from '../../services/publish-service'

import SlideController from '../modals/SlideController'
import VennDiagram from 'venn-diagram'
import _ from 'lodash'
import angular from 'angular'

class ExportCompanyController extends SlideController {
  /* @ngInject */
  constructor($document,
              $timeout,
              $http,
              apiBaseUrl,
              close,
              params,
              alertService,
              segmentService,
              publishService,
              salesforceService,
              showModalService,
              ModalService,
              userService,
              $httpParamSerializer,
              $q) {
    super(close, $document, $timeout)
    this.$timeout = $timeout
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.segmentId = params.segmentId
    this.alertService = alertService
    this.salesforceService = salesforceService
    this.showModalService = showModalService
    this.sms = showModalService
    this.modalService = ModalService
    this.segmentService = segmentService

    this.ps = publishService
    this.pss = publishService.state

    this.userService = userService
    this.$httpParamSerializer = $httpParamSerializer
    this.$q = $q
    this.exportType = params.type
    this.ps.state.exportType = this.exportType
    // params.type is expected to be 'advanced' or 'basic', default is 'basic'
    this.isAdvancedExport = this.exportType === 'advanced'
    this.compTable = params.compTable
    this.onPublishSuccess = params.onPublishSuccess
    this.exporting = false
    this.canExportContact = this.userService.canExportContact()
    this.PUBLISH_METHODS = PUBLISH_METHODS
    this.inited = false

    /* publish to sfdc config */
    this.SFDCInited = false
    this.SFDCIntegrated = false
    this.SFDCPackage = {
      installed: false,
      code: '',
      message: ''
    }

    this.segmentService.getByIdAsync(this.segmentId).then(
      (segment) => {
        this.segment = segment
        const publishServiceP = this.ps.initialize(this.exportType, this.compTable)
        return publishServiceP
      }
    ).then(
      () => {
        this.openSlide()
        this.inited = true
      }
    ).catch((err) => {
      throw new Error(err)
    })
  }

  init() {
    this.initVennDiagram()
    this.checkOauthAsync()
  }

  initVennDiagram() {
    /* hard boundary config */
    this.data = {
      contactLimitPerCompany: this.canExportContact ? 1 : 0,
      limit: 1000,
      dedupe: false,
      sectors: [] // sectors for api, different from this.details.sectors
    }
    this.hovered = 0 // hovered section
    this.hoveredClass = ''
    this.sections = [] // selected sections
    this.vennData = {}
    this.summary = {} // for summary part on the top right
    this.details = { // for details part on the right
      sectors: [], // sectors for showing on the right
      dedupe: 0,
      total: 0,
      atop: false
    }

    if (!this.isAdvancedExport) {
      return
    }
    /* use the following code to simulate a ajax promise, for debugging convenience*/
    // this.vennSample()

    this.vennLoading = true
    this.$http.get(`${this.apiBaseUrl}segments/${this.segmentId}/sector_company_cnt`)
      .then((response) => {
        this.vennData = {}
        for (const key in vennMap) {
          const index = vennMap[key].backEnd
          this.vennData[key] = response.data.data[index]
        }
        angular.element(document).ready(() => {
          this.vennLoading = false
          this.drawHardBoundary()
        })
      }, () => {
        this.vennLoading = false
        this.vennData = {
          1: [0, 0],
          2: [0, 0],
          3: [0, 0],
          4: [0, 0],
          5: [0, 0],
          6: [0, 0],
          7: [0, 0]
        }
        angular.element(document).ready(() => {
          this.vennLoading = false
          this.drawHardBoundary()
        })
      })
  }

  vennSample() {
    this.$timeout(() => {
      // api response is expected like this
      this.vennData = {
        1: [60, 30], // [data without dedupe, data with dedupe]
        2: [60, 30],
        3: [40, 20],
        4: [60, 30],
        5: [40, 20],
        6: [40, 20],
        7: [20, 10]
      }
      angular.element(document).ready(() => {
        this.vennLoading = false
        this.drawHardBoundary()
      })
    }, 0)
  }

  drawHardBoundary() {
    this.venn = new VennDiagram({
      domId: 'venn-diagram',
      width: 575,
      height: 400,
      translate: {x: 26, y: 17},
      onHoverChanged: this._onHoverChanged.bind(this),
      onSelectionChanged: this._onSelectionChanged.bind(this),
      preventSelect: this._preventSelect.bind(this)
    })
  }

  removeSection(section) {
    this.sections = _.filter(this.sections, item => item !== section)
    this.venn.updateSelected(this.sections)
  }

  onDedupeChanged() {
    this._updateDetails()
  }

  onNumCompaniesChanged(evt) {
    this._updateDetails()
  }

  _drawOverviewText() {
    this.venn.drawText('<span style="color: white">Similar<br/>Businesses<br/><b>12,043</b></span>', 134, 79, 'center')
    this.venn.drawText('<span style="color: white">Similar' +
      '<br/>Technologies<br/><b>12,043</b></span>', 300, 119, 'center')
    this.venn.drawText('<span style="color: white">Similar' +
      '<br/>Departments<br/><b>12,043</b></span>', 184, 243, 'center')
  }

  _preventSelect() {
    return !!this.details.atop
  }

  _updateDetails() {
    if (!this.isAdvancedExport) {
      return
    }
    let limit = parseInt(this.pss.numCompanies, 10)
    if (limit <= 0 || _.isNaN(limit)) {
      limit = 0
    }
    const venData = this.vennData
    const isDedupe = this.pss.dedupeAgainstPreviousPublish

    // update 1:  details info
    this.details = _.reduce(this.sections, (result, section) => {
      if (result.atop) {
        return result
      }

      const sectionData = venData[section]
      const sumWithoutDedupe = result.sumWithoutDedupe + sectionData[0]
      const sumWithDedupe = result.sumWithDedupe + sectionData[0] - sectionData[1]
      const dedupe = result.dedupe + sectionData[1]
      const rawTotal = isDedupe ? sumWithDedupe : sumWithoutDedupe
      let total = rawTotal
      // Atop is true when the number of companies from sectors we've selected
      // are less than `limit`, which means we don't allow selecting more sectors
      let atop
      const sectors = result.sectors

      if (rawTotal >= limit) {
        atop = true
        if (limit > 0) {
          const number = sectionData[0] - (rawTotal - limit)
          total = limit
          sectors.push({section, number})
        } else {
          total = 0
        }
      } else {
        atop = false
        sectors.push({section, number: sectionData[0]})
      }
      return {total, atop, sectors, dedupe, sumWithoutDedupe, sumWithDedupe}
    }, {
      sectors: [],
      dedupe: 0,
      total: 0,
      atop: false,
      sumWithoutDedupe: 0,
      sumWithDedupe: 0
    })

    // update 2:  venn diagram
    const allowedLen = this.details.sectors.length
    if (allowedLen <= this.sections.length) {
      const newSections = this.sections.slice(0, allowedLen)
      this.venn.updateSelected(newSections)
    }

    // update 3:  sectors
    this.pss.advanced.sectors = _.map(this.sections, (item) => {
      return vennMap[item].backEnd
    })
  }

  _onHoverChanged(section) {
    this.$timeout(() => {
      this.hovered = section
      this.hoveredClass = section ? `section-${section}` : ''
      this.summary = {
        techList: this._getHoveredTechList(),
        recommendationNumber: this._getHoveredCompanyNumber(),
        description: this._getHoveredDescription()
      }
    }, 0)
  }

  _onSelectionChanged(sections) {
    this.$timeout(() => {
      this.sections = sections
      this._updateDetails()
    }, 0)
  }

  _getHoveredTechList() {
    return componentsDetail(this.hovered)
  }

  _getHoveredCompanyNumber() {
    const data = this.vennData
    return this.hovered > 0 ? data[this.hovered][0] : 0
  }

  _getHoveredDescription() {
    const components = componentsDetail(this.hovered)
    let desc = _.filter(components, (item) => item.selected)
    desc = _.map(desc, item => item.name)
    return `This section contains similarities in
    ${desc.join(', ')}${desc.length > 1 ? '' : ' only'}.`
  }

  _getSelectedList(dedupe) {
    const data = this.vennData
    return _.map(this.sections, (section) => {
      return {
        section,
        number: data[section][dedupe ? 1 : 0]
      }
    })
  }

  _getDedupeAndTotal() {
    const totalWithoutDedupe = _.reduce(this._getSelectedList(false),
      (prev, item) => {
        return prev + item.number
      }, 0)
    const total = this.data.dedupe ? _.reduce(this._getSelectedList(true),
      (prev, item) => {
        return prev + item.number
      }, 0) : totalWithoutDedupe
    return {
      total,
      dedupe: totalWithoutDedupe - total
    }
  }

  checkOauthAsync() {
    const finish = () => {
      this.SFDCInited = true
    }
    this.$q.all({
      integrated: this.salesforceService.isIntegrated().then((response) => {
        this.SFDCIntegrated = response.data.data === true
      }),
      installed: this.checkPackageAsync()
    }).then(finish, finish)
  }

  checkPackageAsync() {
    const userId = this.userService.userId()
    const defer = this.salesforceService.packageInstalled(userId)
    defer.then((response) => {
      this.SFDCPackage = {
        installed: true,
        code: '',
        message: ''
      }
    }, (response) => {
      this.SFDCPackage = {
        installed: false,
        code: response.data.code || '',
        message: response.data.message || response.statusText
      }
    })
    return defer
  }

  onPublishCSV(data) {
    if (this.isAdvancedExport) {
      if (data.sectors.length < 1) {
        this.showError('TOTAL EXPORT should be more than 0')
        return
      }
    }
    let url = `${this.apiBaseUrl}segments/${this.segmentId}/`
    url += this.isAdvancedExport ? 'sector_companies?' : 'companies?'
    let payload = this.getParamsFromConfirmData(data)
    payload = this.addSectorsOrTableParams(payload, data)

    url = `${url}${this.$httpParamSerializer(payload)}`
    const promise = this.$http.get(url)
    this.finishPublish(promise)
  }

  onPublishSFDC(data) {
    if (!this.SFDCInited) {
      // TODO maybe should disable the SFDC button
      return
    }
    if (this.isAdvancedExport) {
      if (data.sectors.length < 1) {
        this.showError('TOTAL EXPORT should be more than 0')
        return
      }
    }
    const url = `${this.apiBaseUrl}segments/${this.segmentId}/publish_with_dedupe`
    let payload = this.getParamsFromConfirmData(data)
    payload = this.addSectorsOrTableParams(payload, data)

    const publishSFDC = () => {
      const promise = this.$http({
        url,
        method: 'POST',
        params: payload
      })
      this.finishPublish(promise)
    }

    if (!this.SFDCIntegrated || !this.SFDCPackage.installed) {
      this.showModalService.handleSFDCErrorInModal({
        SFDCIntegrated: this.SFDCIntegrated,
        SFDCPackage: this.SFDCPackage,
        onSuccess: (updatedData) => {
          this.updateSFDCInfo(updatedData)
          publishSFDC()
        },
        onCancel: this.updateSFDCInfo.bind(this)
      })
    } else {
      publishSFDC()
    }
  }

  updateSFDCInfo(updatedData) {
    this.SFDCIntegrated = updatedData.SFDCIntegrated
    this.SFDCPackage = updatedData.SFDCPackage
  }

  finishPublish(promise) {
    promise.then(() => {
      this.close()
      this.onPublishSuccess()
    }, (response) => {
      this.showError(response.data.message || response.statusText)
    })
  }

  addSectorsOrTableParams(params, data) {
    const payload = params
    if (this.isAdvancedExport) {
      payload.sectors = data.sectors.join()
    } else {
      const tableParams = this.compTable.getCurrParams()
      payload.onlyExported = tableParams.onlyExported ? 1 : 0
      if (tableParams.order) {
        payload.order = tableParams.order
      }
      if (tableParams.where) {
        payload.where = JSON.stringify(tableParams.where)
      }
    }
    return payload
  }

  getParamsFromConfirmData(data) {
    // contactLimitPerCompany should be between 0-5
    const contactLimitPerCompany = Math.max(0, Math.min(data.contactLimitPerCompany, 5))
    return {
      limit: data.limit,
      contactLimitPerCompany,
      dedupe: data.dedupe ? 1 : 0,
      format: 'email'
    }
  }

  showError(message) {
    this.alertService.addAlert('error', message, '#segment-export-slide-alert')
  }

  shouldShowAdvance() {
    return this.isAdvancedExport
  }

  showPublishContactsSlide() {
    return this.sms.exportContacts()
  }

  /**
   * Whether to handle here, in this focus slide.
   * If true, we'll export here on this slide because we don't need to dedupg.
   * Otherwise, we'll continue on to the next slide
   */
  handlePublishHere() {
    return (this.shouldShowAdvance() || (
      this.ps.isPublishingToCsv() && !this.ps.canPublishToSalesforce()))
  }

  /**
   * Get the label/text of the button to progress to the next step.
   */
  getNextButtonLabel() {
    if (this.handlePublishHere()) {
      return 'Publish'
    }

    return 'Continue'
  }

  nextStep() {
    // These cases involve no dedupe, so we publish now.
    if (this.handlePublishHere()) {
      const executePublish = () => {
        this
          .ps
          .publish()
          .then(() => {
            SlideController.closeAllSlides()
            this.alertService.addPublishSuccessAlert()
          })
          .catch((err) => {
            this.showError(err.message)
          })
      }
      if (this.pss.canPublishContacts && this.pss.numContacts > 0) {
        const totalContacts = this.pss.numCompanies * this.pss.numContacts
        this.sms.showConfirmContactsModal({
          totalContacts,
          onConfirm: (result, close) => {
            executePublish()
            close()
          }
        })
      } else {
        executePublish()
      }
    } else {
      // Continue to de-dupe choices.
      this.sms.exportNewOrExisting({compTable: this.compTable})
    }
  }

  publishFormValid() {
    // Advanced export requires sectors.
    if ((this.pss.exportType === 'advanced') && _.isEmpty(this.pss.advanced.sectors)) {
      return false
    }

    return true
  }

  cancelPublish() {
    this.closeSlide()
  }
}

export default ExportCompanyController
