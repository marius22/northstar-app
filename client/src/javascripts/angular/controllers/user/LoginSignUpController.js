import {getParameterByKey} from 'utils'

class LoginSignUpController {
  /* @ngInject */
  constructor($http,
              apiBaseUrl,
              alertService,
              $state,
              $location,
              userService,
              showModalService,
              $window,
              salesforceService,
              $timeout) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.alertService = alertService
    this.salesforceService = salesforceService
    this.$state = $state
    this.$location = $location
    this.userService = userService
    this.$window = $window
    this.$timeout = $timeout
    this.sms = showModalService

    this.agree = false
    this.data = []
    this.errorSelector = '#page-user-error'
    this.username = ''
    this.password = ''
    this.loading = false

    // Without a timeout, the DOM element of `errorSelector` might not exist.
    this.$timeout(() => {
      if (this.$state.params.message) {
        this.alertService.addAlert('error', this.$state.params.message, this.errorSelector)
      }
      if (this.$state.params.refer) {
        this.$location.search('refer', this.$state.params.refer)
      }
    })
  }

  isValidEmail(email) {
    // send user an email and use confirmation as the validation, in front end, we just need add a simple check
    // see more: "https://davidcel.iss/posts/stop-validating-email-addresses-with-regex/"
    return /.+@.+\..+/i.test(email)
  }

  checkUser() {
    if (!this.isValidEmail(this.username)) {
      return 'Please use a valid email address.'
    }
    return ''
  }

  checkUserAndPass() {
    // check User
    const message = this.checkUser()
    if (message !== '') {
      return message
    }

    // check password
    if (this.password === '') {
      return 'Please enter a password.'
    }

    return ''
  }

  login() {
    const message = this.checkUserAndPass()
    if (message !== '') {
      this.warning(message)
      return
    }

    this.loading = true
    this.$http.post(`${this.apiBaseUrl}users/login`, {
      email: this.username,
      password: this.password
    }).success((response) => {
      this.userService.setAppropriateUser(response.data)
      this.salesforceService.fetchData()
      this.loading = false
      this.redirect()
    }).catch((response) => {
      this.loading = false
      // Give a response even the api doesn't work
      this.warning(response.data.message || response.statusText)
    })
  }

  redirect() {
    const zendeskRedirectLink = getParameterByKey('return_to')
    if (zendeskRedirectLink) {
      window.location.href = `${this.apiBaseUrl}zendesk/go_to?url=${zendeskRedirectLink}`
      return
    }

    const refer = this.$location.$$search.refer
    if (refer && this.isReferValid(refer)) {
      this.$window.location.href = refer
    } else {
      if (this.userService.isSuper()) {
        this.$state.go('admin.license')
      } else {
        this.$state.go('audience.console')
      }
    }
  }

  // refer url must be in the same domain with login page
  isReferValid(referUrl) {
    const link = document.createElement('a')
    link.href = referUrl

    // for protocol, parser returns https: or http:, thus remove the last character with slice
    return link.hostname === this.$location.host() &&
      link.protocol.slice(0, -1) === this.$location.protocol()
  }

  warning(message) {
    this.alertService.addAlert('error', message, this.errorSelector, 10)
  }

  signUp() {
    if (!this.agree) {
      this.warning('Please agree to our Terms and Conditions.')
      return
    }
    const message = this.checkUserAndPass()
    if (message !== '') {
      this.warning(message)
      return
    }

    this.loading = true
    this.$http.post(`${this.apiBaseUrl}users/register`, {
      email: this.username,
      password: this.password,
      sendEmail: false
    }).success((res) => {
      this.loading = false
      this.userService.setUser(res.data)
      this.$state.go('audience.console')
    }).error((response) => {
      this.loading = false
      this.warning(response.message)
    })
  }

  forgetPass() {
    const message = this.checkUser()
    if (message !== '') {
      this.warning(message)
      return
    }

    this.loading = true
    this.$http.put(`${this.apiBaseUrl}users/forget_password`, {
      email: this.username
    }).then(() => {
      this.loading = false
      this.$state.go('user.pass-sent')
    }, (response) => {
      this.loading = false
      this.warning(response.data.message || response.statusText)
    })
  }

  showAgreementModal() {
    const params = {
      onConfirm: (data, close) => {
        this.agree = true
        close()
      }
    }
    this.sms.showAgreementModal(params)
  }
}

export default LoginSignUpController
