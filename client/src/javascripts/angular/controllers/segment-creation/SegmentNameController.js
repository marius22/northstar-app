class SegmentNameController {
  /* @ngInject */
  constructor(alertService,
              apiBaseUrl,
              $http,
              $state,
              showModalService,
              userService,
              ghostService) {
    this.alertService = alertService
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.$state = $state
    this.sms = showModalService
    this.userService = userService
    this.ghostService = ghostService

    this.createButtonDisabled = false
    this.errorContainer = '#create-segment-error'
    this.inited = true

    this.init()
  }

  init() {
    if (!this.userService.isLogin()) {
      return
    }
    const submitName = (segmentName, close) => {
      if (segmentName && !this.createButtonDisabled) {
        const url = `${this.apiBaseUrl}segments`
        this.createButtonDisabled = true

        const params = {segmentName}
        if (this.ghostService.isGhosting()) {
          params['accountId'] = this.ghostService.ghostId()
        }
        this.$http.post(url, params)
          .success((response) => {
            this.createButtonDisabled = false
            close()
            this.$state.go('segment.create.qualification', {segmentId: response.data.segmentId})
          })
          .error((response) => {
            this.createButtonDisabled = false
            this.alertService.addAlert('error', `${response.message}`, this.errorContainer)
          })
      }
    }
    this.sms.createSegment({
      onConfirm: submitName
    })
  }
}

export default SegmentNameController
