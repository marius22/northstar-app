import _ from 'lodash'
import Papa from 'papaparse'

class UploadCSVController {
  /* @ngInject */
  constructor(Upload,
              alertService,
              apiBaseUrl,
              $http,
              $location,
              segmentService,
              $state,
              showModalService,
              salesforceService,
              userService,
              modelService,
              $timeout) {
    this.uploader = Upload
    this.alertService = alertService
    this.apiBaseUrl = apiBaseUrl
    this.$http = $http
    this.$location = $location
    this.segmentService = segmentService
    this.$state = $state
    this.sms = showModalService
    this.salesforceService = salesforceService
    this.modelService = modelService
    this.$timeout = $timeout

    this.inited = true
    this.isUploading = true
    this.info = {
      uploaded: false,
      isUploading: false,
      disableNext: true
    }

    this.accountId = userService.accountId()
  }

  validateThenUpload(file) {
    if (!file) {
      this.alertService.addAlert('error', 'Invalid file.')
      return
    }
    const complete = (results) => {
      const data = results.data
      const errors = results.errors

      // check errors
      if (errors.length > 0) {
        const error = errors[0]
        const rowNumber = error.row
        const errorMessage = error.message

        if (rowNumber) {
          this.$timeout(() => {
            this.info.isUploading = false
            this.alertService.addAlert('error', `Error in row ${rowNumber}: ${errorMessage}`)
          }, 0)
          return
        }
      }

      // check maxLines
      if (data.length > 10000) {
        this.$timeout(() => {
          this.info.isUploading = false
          this.alertService.addAlert('error', `Your CSV must have at most 10000 rows.`)
        }, 0)
        return
      }

      // check numColumns
      if (!_.includes([1, 2], data[0].length)) {
        this.$timeout(() => {
          this.info.isUploading = false
          this.alertService.addAlert('error', `Your CSV should have up to two columns: Company name and/or Company URL.`)
        }, 0)
        return
      }

      this.upload(file)
    }

    this.info.isUploading = true
    const config = {complete}
    Papa.parse(file, config)
  }

  upload(file) {
    const uploadUrl = `${this.apiBaseUrl}accounts/${this.accountId}/seeds`

    this.uploader.upload({
      url: uploadUrl,
      data: {file}
    }).then(() => {
      this.info.uploaded = true
      this.info.isUploading = false
      this.info.disableNext = false
    }, (resp) => {
      const domains = resp.data.message.domains
      const fileName = `revised ${file.name}`
      this.modelService.handleNotEnoughSeedDomains(domains, 70, fileName)
      this.info.uploaded = false
      this.info.isUploading = false
    })
  }

  CRMNextOnclick() {
    this.salesforceService.triggerAcctFitModelRun()
  }

  downloadSample() {
    this.modelService.downloadSampleCsv()
  }
}

export default UploadCSVController
