import _ from 'lodash'

class SegmentReachController {
  /* @ngInject */
  constructor($http, apiBaseUrl, $state, segmentService, alertService, $filter, $q) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.$filter = $filter
    this.$q = $q
    this.$state = $state

    this.segmentId = $state.params.segmentId
    this.segmentService = segmentService
    this.alertService = alertService

    this.filterSearchQuery = {
      text: ''
    }
    this.inited = false

    this.categories = ['department', 'tech']
    this.displayNameMap = {
      'tech': 'Tech Stack',
      'department': 'Department'
    }
    this.activeCategory = 'department'
    
    // Max number of items that is allowed in the selected field
    this.maxTechAllowed = 1000

    // Make 3 api calls to get all the necessary data.
    // Process everything after all 3 calls are successful.
    const prevChoicesPromise = this.segmentService.getPrevChoices('expansion', this.segmentId).then(
      (response) => {
        this.prevChoices = response.data.data
      }
    )

    const seedDataPromise = this.segmentService.getSeedAttrData(['tech'], this.segmentId).then(
      (response) => {
        this.seedAttrData = response.data.data
      }
    )
    const universeDataPromise = this.segmentService.getUniverseCategoryData(this.categories).then(
      (response) => {
        this.categoryData = response.data.data
      }
    )

    $q.all([prevChoicesPromise, seedDataPromise, universeDataPromise]).then(
      () => {
        this.configureChoicesFromData()
      }
    )
  }

  configureChoicesFromData() {
    let techCount = 0;
    let maxTechReached = false;
    _.forEach(this.categoryData, (value, categoryName) => {
      this.categoryData[categoryName] = _.map(this.categoryData[categoryName], (value) => {
        const inSeedList = _.includes(this.seedAttrData[categoryName], value)
        const wasPrevChoice = _.includes(this.prevChoices[categoryName], value)
        const shouldPreSelect = inSeedList || wasPrevChoice
        if (categoryName === 'tech' && shouldPreSelect) {
          techCount++
        }
        if (categoryName === 'tech' && techCount > this.maxTechAllowed) {
          this.alertService.addAlert('warning', `There are more than ${this.maxTechAllowed} technologies in the seed list. Only ${this.maxTechAllowed} will be selected.`)
          maxTechReached = true
        }
        return {
          text: typeof value === "string" ? value : value.join(", "),
          staged: false,
          selected: shouldPreSelect && !maxTechReached,
          fromSeed: inSeedList
        }
      })
    })
    this.inited = true
  }

  selectedTechsLowEnough() {
    const selectedTechs = _.filter(this.categoryData["tech"], (item) => {
      return item.selected
    })
    const lowEnough = selectedTechs.length <= this.maxTechAllowed
    if (!lowEnough) {
      this.alertService.addAlert('error', `You have over ${this.maxTechAllowed} techs selected. Please select ${this.maxTechAllowed} or less to continue.`)
    }
    return selectedTechs.length <= this.maxTechAllowed
  }

  disableNext() {
    return !this.inited || !this.selectedTechsLowEnough()
  }

  saveExpChoices() {
    const expansion = {}

    _.forEach(this.categories, (category) => {
      expansion[category] = []
      _.forEach(this.categoryData[category], (dataObj) => {
        if (dataObj.selected) {
          expansion[category].push(dataObj.text)
        }
      })
    })

    const url = `${this.apiBaseUrl}segments/${this.segmentId}/expansion`
    const params = {expansion}

    this.$http.put(url, params).then(
      () => {
        this.$state.go('segment.create.summary', {segmentId: this.segmentId})
      }
    )
  }

  /* Category Wizard Logic */

  setToActive(category) {
    this.activeCategory = category
    this.filterSearchQuery.text = ''
  }

  isActive(category) {
    return this.activeCategory === category
  }

  displayName(category) {
    return this.displayNameMap[category]
  }

}

export default SegmentReachController
