class UploadController {
  /* @ngInject */
  constructor($q,
              salesforceService,
              modelService,
              ghostService,
              apiBaseUrl,
              $interval,
              $state,
              showModalService) {
    this.$q = $q
    this.salesforceService = salesforceService
    this.modelService = modelService
    this.ghostService = ghostService
    this.apiBaseUrl = apiBaseUrl
    this.$state = $state
    this.$interval = $interval
    this.sms = showModalService

    this.inited = false
    this.SFDCIntegrated = false

    this.init()
  }

  init() {
    this.modelService.httpCRMFitModels(this.ghostService.accountId()).then((response) => {
     this.crmFitStatus = response.data.data
    })['finally'](() => {
     this.inited = true
     if (!this.crmFitStatus) {
       this.crmFitStatus = 'NO_MODEL' // set default status as 'no_model'
     }
    })
    // Careful about this in other places
    this.salesforceService.isIntegrated().then((response) => {
      this.SFDCIntegrated = response.data.data === true
    })
  }

  selectCSV() {
    this.checkCRMFitStatus().then(() => {
      this.$state.go('crm.uploadCSV')
    })
  }

  onclickSFDC() {
    this.checkCRMFitStatus().then(() => {
      if (this.SFDCIntegrated) {
        this.$state.go('crm.uploadSFDC')
        return
      }
      const checkIntegrated = () => {
        return this.salesforceService.isIntegrated()
          .then((response) => {
            if (response.data.data) {
              // todo should show a flash of success
              this.$state.go('crm.uploadSFDC')
            }
          })
      }
      this.salesforceService.openSFDCOauthWindow(checkIntegrated)
    })
  }

  getCRMFitStatus() {
    return this.crmFitStatus
  }

  getCRMFitStatusText() {
    const status = this.crmFitStatus
    if (status !== '') {
      const maps = {
        IN_PROGRESS: `The CRM Fit model build <span class="in_progress">in progress.</span>`,
        COMPLETED: 'The CRM Fit model was <span class="completed">built successfully.</span>',
        NO_MODEL: 'The CRM Fit model has <span class="no_model">not been started.</span>'
      }
      return maps[status]
    } else {
      return ''
    }
  }

  checkCRMFitStatus() {
    const defer = this.$q.defer()
    const status = this.crmFitStatus
    // not inited
    if (status === '') {
      defer.reject()
    } else if (status === 'IN_PROGRESS') {
      this.sms.messageModal({
        title: 'Warning',
        messageType: 'warning',
        message: 'CRM Fit model is currently running. <br />Please wait for it to finish before replacing the model.'
      })
      defer.reject()
    } else if (status === 'COMPLETED') {
      this.sms.confirmationModal({
        title: 'Warning',
        messageType: 'warning',
        message: 'Are you sure you want to replace the current CRM Fit model?',
        onConfirm: () => {
          defer.resolve()
        },
        onDeny: (data, close) => {
          defer.reject()
          close()
        }
      })
    } else {
      // NO_MODEL
      defer.resolve()
    }

    return defer.promise
  }

}

export default UploadController
