import _ from 'lodash'

class QualCriteriaController {
  /* @ngInject */
  constructor($http, apiBaseUrl, $state, segmentService, $filter, $q, showModalService) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.$state = $state
    this.$filter = $filter
    this.sms = showModalService

    this.segmentId = $state.params.segmentId
    this.segmentService = segmentService

    this.filterSearchQuery = {
      text: ''
    }

    this.inited = false
    this.categories = ['state', 'industry', 'employeeSize']
    this.displayNameMap = {
      'industry': 'Industry',
      'employeeSize': 'Employee Size',
      'state': 'Location'
    }

    this.activeCategory = 'state'

    // Make 3 api calls to get all the necessary data.
    // Process everything after all 3 calls are successful.
    const prevChoicesPromise = this.segmentService.getPrevChoices('qualification', this.segmentId).then(
      (response) => {
        this.prevChoices = response.data.data
      }
    )

    const seedDataPromise = this.segmentService.getSeedAttrData(this.categories, this.segmentId).then(
      (response) => {
        this.seedAttrData = response.data.data
      }
    )
    const universeDataPromise = this.segmentService.getUniverseCategoryData(this.categories).then(
      (response) => {
        this.categoryData = response.data.data
      }
    )

    $q.all([prevChoicesPromise, seedDataPromise, universeDataPromise]).then(
      () => {
        this.configureChoicesFromData()
      }
    )
  }

  configureChoicesFromData() {
    _.forEach(this.categoryData, (value, categoryName) => {
      this.categoryData[categoryName] = _.map(this.categoryData[categoryName], (value) => {
        const inSeedList = _.includes(this.seedAttrData[categoryName], value)
        const wasPrevChoice = _.includes(this.prevChoices[categoryName], value)
        return {
          text: typeof value === "string" ? value : value.join(", "),
          staged: false,
          selected: wasPrevChoice,
          fromSeed: inSeedList
        }
      })
    })
    this.inited = true
  }

  qualNextClick() {
    const qualification = {}
    const noneSelected = []

    const saveQualChoicesAndProceed = () => {
      const url = `${this.apiBaseUrl}segments/${this.segmentId}/qualification`
      // If there are no states selected by the user, we need to pass all 50 states.
      // Otherwise, our recommendations will come from all possible locations, which
      // includes companies outside of the US.
      if (qualification['state'].length === 0) {
        qualification['state'] = _.map(this.categoryData['state'], 'text')
      }

      // For `employeeSize`, if we have an empty list saved in our db, it means that
      // the user has expressed no preference for employee size. That means our result set
      // should contain companies of any employee size, so we fill in all the employee sizes
      // here. Same thing for industry.
      if (qualification['industry'].length === 0) {
        qualification['industry'] = _.map(this.categoryData['industry'], 'text')
      }
      if (qualification['employeeSize'].length === 0) {
        qualification['employeeSize'] = _.map(this.categoryData['employeeSize'], 'text')
      }

      const params = {qualification}
      this.$http.put(url, params)
      this.$state.go('segment.create.upload', {segmentId: this.segmentId})
    }

    // Iterate through each category of categoryData and
    // gather all of the selected items. If we have no selections
    // for a category, add that category to the noneSelected array
    // and ask the user if he's sure about making no selections
    // for the given category(s)
    _.forEach(this.categories, (category) => {
      qualification[category] = []
      _.forEach(this.categoryData[category], (dataObj) => {
        if (dataObj.selected) {
          qualification[category].push(dataObj.text)
        }
      })
      if (_.isEmpty(qualification[category])) {
        const dispName = this.displayName(category)
        noneSelected.push(dispName)
      }
    })

    if (_.isEmpty(noneSelected)) {
      saveQualChoicesAndProceed()
    } else {
      const noneSelectedMessage = _.map(noneSelected, (category) => {
        return `<b>${category}</b>`
      }).join('<br />')
      const modalMessage = `<div class="no-select-msg">
                              You didn't make any selections for these categories:
                            </div>
                            <div>
                              ${noneSelectedMessage}
                            </div>
                            <div class="click-yes-msg">
                              Click <b>YES</b> to <b>select all</b> in each category and continue.
                            </div>
                            <div class="click-no-msg">
                              Click <b>NO</b> to <b>select your own</b> in each category.
                            </div>`

      const modalParams = {
        title: 'Did you mean to skip these?',
        message: modalMessage,
        onConfirm: (result, close) => {
          saveQualChoicesAndProceed()
          close()
        }
      }
      this.sms.confirmationModal(modalParams)
    }
  }

  /*  Category Wizard Logic */

  setToActive(category) {
    this.activeCategory = category
    this.filterSearchQuery.text = ''
  }

  isActive(category) {
    return this.activeCategory === category
  }

  displayName(category) {
    return this.displayNameMap[category]
  }

}

export default QualCriteriaController
