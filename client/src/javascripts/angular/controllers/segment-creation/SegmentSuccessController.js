class SegmentSuccessController {
  /* @ngInject */
  constructor($http,
              userService,
              apiBaseUrl) {
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl
    this.user = userService.getUser()
    this.activated = !!this.user.activated
    this.emailSent = false
    this.emailSending = false
  }

  sendEmail() {
    if (!this.user.email) {
      return
    }
    this.emailSending = true
    this.$http.post(`${this.apiBaseUrl}users/resend_confirmation_email`, {
      email: this.user.email
    }).success(() => {
      this.emailSent = true
      this.emailSending = false
    }).error(() => {
      this.emailSent = false
      this.emailSending = false
    })
  }
}


export default SegmentSuccessController
