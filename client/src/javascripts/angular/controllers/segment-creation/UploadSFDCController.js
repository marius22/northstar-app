import _ from 'lodash'
import moment from 'moment'

/**
 * Handles the logic of uploading a segment to SFDC.
 */
class UploadSFDCController {
  /* @ngInject */
  constructor(alertService,
              salesforceService,
              apiBaseUrl,
              $state,
              segmentService,
              ModalService,
              showModalService,
              userService) {
    this.alertService = alertService
    this.salesforceService = salesforceService
    this.apiBaseUrl = apiBaseUrl
    this.$state = $state
    this.segmentService = segmentService
    this.modalService = ModalService
    this.sms = showModalService

    this.inited = false
    this.segmentId = $state.params.segmentId
    this.accountId = userService.accountId()
    this.startDate = this.getInitDate()
    this.endDate = moment(new Date()).format('MM/DD/YYYY')
    this.data = {
      status: 'EMPTY' // can be EMPTY, IN_PROGRESS, COMPLETED
    }
    this.pulling = false // waiting for pull API's response
    // step: selectDate -> showResult
    this.step = 'selectDate'
    this.init()
  }

  getInitDate() {
    const d = new Date()
    d.setTime(d.getTime() - 1000 * 3600 * 24 * 30)
    return moment(d).format('MM/DD/YYYY')
  }

  init() {
    // todo call GET api: /segments/<int:segment_id>/seeds/sfdc
    const response = {
      status: 'EMPTY'
    }
    _.extend(this.data, response)
    this.inited = true
  }
  // "06/07/2015" => "2016-06-07T07:00:00.000Z"
  // we can't use moment('2015/04/03') any more, see docs https://github.com/moment/moment/issues/1407
  convertLocalToUtc(dateStr) {
    return moment(dateStr, "MM/DD/YYYY").toISOString()
  }

  pullData() {
    this.alertService.clearAlerts()
    this.pulling = true
    this.salesforceService.postData(this.accountId, 'accounts', {
      startDate: this.convertLocalToUtc(this.startDate),
      endDate: this.convertLocalToUtc(this.endDate)
    }).success((response) => {
      _.extend(this.data, response)
      this.step = 'showResult'
      this.pulling = false
    }).error((response) => {
      this.alertService.addAlert('error', response.message)
      this.pulling = false
    })
    // todo if api call lasts for a minute, loading page should be shown
  }

  shouldActiveNextButton() {
    if (this.step === 'selectDate') {
      return this.checkDateValid() && !this.pulling
    } else if (this.step === 'showResult') {
      return true
    }
    return false
  }

  checkDateValid() {
    const isStartDateValid = moment(this.startDate, 'MM/DD/YYYY').isValid()
    const isEndDateValid = moment(this.endDate, 'MM/DD/YYYY').isValid()
    return isStartDateValid && isEndDateValid
  }

  next() {
    if (!this.shouldActiveNextButton()) {
      return
    }
    if (this.step === 'selectDate') {
      this.pullData()
    } else if (this.step === 'showResult') {
      this.salesforceService.triggerAcctFitModelRun('CRM')
    }
  }

  // type can be matches/unknown
  showCompanies(type) {
    const forCRM = this.salesforceService.isCRMContext()
    this.modalService.showModal({
      controller: 'SFDCSlideController',
      controllerAs: 'usc',
      inputs: {type, segmentId: this.segmentId, forCRM},
      templateUrl: '/templates/sfdc-slide.html'
    })
  }

  // transform date for html template
  transformDate(dateStr) {
    return moment(new Date(dateStr)).format('MMMM Do, YYYY')
  }

  instructionText() {
    return `EverString will fetch companies from the Accounts that have Opportunities.
            </br>
            Please select the date range based on the Opportunity created date.`
  }

  nextBtnText() {
    if (this.step === 'selectDate') {
      return 'Next'
    }
    if (this.step === 'showResult') {
      return 'Create Model'
    }
  }
}

export default UploadSFDCController
