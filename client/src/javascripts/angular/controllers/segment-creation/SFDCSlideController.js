import SlideController from '../modals/SlideController'
import _ from 'lodash'

class SFDCSlideController extends SlideController {
  /* @ngInject */
  constructor(close,
              type,
              segmentId,
              forCRM,
              apiBaseUrl,
              $document,
              tableService,
              $timeout,
              userService) {
    super(close, $document, $timeout)
    this.type = type
    this.segmentId = segmentId
    this.apiBaseUrl = apiBaseUrl
    this.tableService = tableService
    this.$timeout = $timeout
    this.limit = 8
    this.accountId = userService.accountId()
    this.forCRM = forCRM
    this.openSlide()
  }

  init() {
    this.showTable(this.type)
  }

  showTable(type) {
    this.type = type
    this.seedTables = {}
    let seedListTableHeader = [
      {
        text: 'EverString Company Name',
        key: 'companyName'
      },
      {
        text: 'SFDC ACCOUNT NAME',
        key: 'SFDCAccountName'
      },
      {
        text: 'SFDC ACCOUNT ID',
        key: 'SFDCAccountId'
      },
      {
        text: 'URL(MATCHING COLUMN)',
        key: 'domain'
      }
    ]
    if (type === 'unknown') {
      seedListTableHeader = [
        {
          text: 'SFDC ACCOUNT NAME',
          key: 'SFDCAccountName'
        },
        {
          text: 'SFDC ACCOUNT ID',
          key: 'SFDCAccountId'
        }
      ]
    }

    const tableConfig = {
      subject: 'Seed List',
      header: seedListTableHeader,
      defaultQueryParams: {
        type,
        order: 'domain'
      }
    }

    this.seedTables[type] = this.tableService.createTable(tableConfig)
    const crudConfig = this.seedTables[type].getResourceConfig()
    if (this.forCRM) {
      crudConfig.url = `${this.apiBaseUrl}accounts/${this.accountId}/seeds/sfdc/overview`
    } else {
      crudConfig.url = `${this.apiBaseUrl}segments/${this.segmentId}/seeds/sfdc/overview`
    }
    this.seedTables[type].configureCrudResource(crudConfig)
    this.seedTables[type].displayedFields = _.map(seedListTableHeader, (item) => item.key)
  }
}


export default SFDCSlideController
