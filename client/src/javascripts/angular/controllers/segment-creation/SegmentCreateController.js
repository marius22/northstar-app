class SegmentCreateController {
  /* @ngInject */
  constructor($state, $rootScope, $http, apiBaseUrl) {
    this.$state = $state
    this.$http = $http
    this.apiBaseUrl = apiBaseUrl

    const unsubscribe = $rootScope.$on('$stateChangeSuccess', (event, toState, toParams) => {
      if (!this.currentStateInSegmentCreationFlow()) {
        unsubscribe()
      } else {
        this.saveStepAsLastVisited()
      }
    })

    this.saveStepAsLastVisited()
  }

  currentStateInSegmentCreationFlow() {
    return this.isStep1() || this.isStep2() || this.isStep3() || this.isStep4() || this.isStep5()
  }

  saveStepAsLastVisited() {
    // I set this.segmentId here instead of the constructor in case
    // the user starts from the very first naming step during which
    // segmentId === undefined
    this.segmentId = this.$state.params.segmentId
    if (this.segmentId) {
      const stageNumFromCurrentStep = () => {
        if (this.isStep1()) {
          return 1
        }
        if (this.isStep2()) {
          return 2
        }
        if (this.isStep3()) {
          return 3
        }
        if (this.isStep4()) {
          return 4
        }
        if (this.isStep5()) {
          return 5
        }
        // this shouldn't be possible to reach, but just in case
        return 1
      }

      const url = `${this.apiBaseUrl}segments/${this.segmentId}/stage`
      const params = {
        stage: stageNumFromCurrentStep()
      }

      this.$http.put(url, params)
    }
  }

  isStep1() {
    return this.$state.is('segment.create.name') ||
        this.$state.is('segment.create.qualification')
  }

  isStep2() {
    return this.$state.is('segment.create.upload') ||
        this.$state.is('segment.create.csv') ||
        this.$state.is('segment.create.sfdc')
  }

  isStep3() {
    return this.$state.is('segment.create.insights')
  }

  isStep4() {
    return this.$state.is('segment.create.reach')
  }

  isStep5() {
    return this.$state.is('segment.create.summary')
  }
}

export default SegmentCreateController
