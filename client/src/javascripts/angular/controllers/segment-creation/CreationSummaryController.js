import _ from 'lodash'
import angular from 'angular'

class CreationSummaryController {
  /* @ngInject */
  constructor(apiBaseUrl, segmentService, $state, $http, $timeout, $filter, $q, $scope) {
    this.$state = $state
    this.$http = $http
    this.$timeout = $timeout
    // $filter is used in summary.html
    this.$filter = $filter
    this.apiBaseUrl = apiBaseUrl
    this.segmentId = $state.params.segmentId
    this.segmentService = segmentService
    this.$scope = $scope

    segmentService.getByIdAsync(this.segmentId).then((segment) => {
      this.segment = segment
    })

    this.getSeedCompCount()

    this.reachCategories = ['tech', 'department']
    this.qualCategories = ['industry', 'employeeSize', 'state']
    this.displayNameMap = {
      'tech': 'Tech Stack',
      'department': 'Department',
      'industry': 'Industry',
      'employeeSize': 'Employee Size',
      'state': 'Location'
    }
    this.prevChoices = {}

    this.titlePaneTitles = ['Predictive Preferences', 'Filter Criteria']
    this.segmentService.newCriteriaFromSeed(this.segmentId)

    // We make api calls to get the previously selected Qual Criteria, the previously selected
    // Expansion criteria, and the seed list data for this potential segment. After all the calls
    // are successful, we use the data from those APIs to configure the summary information shown
    // to the user
    const prevQualChoicesPromise = this.segmentService.getPrevChoices('qualification', this.segmentId).then(
      (response) => {
        angular.extend(this.prevChoices, response.data.data)
      }
    )

    const prevExpChoicesPromise = this.segmentService.getPrevChoices('expansion', this.segmentId).then(
      (response) => {
        angular.extend(this.prevChoices, response.data.data)
      }
    )

    const seedCategories = ['tech'].concat(this.qualCategories)
    const seedDataPromise = this.segmentService.getSeedAttrData(seedCategories, this.segmentId).then(
      (response) => {
        this.seedAttrData = response.data.data
      }
    )

    $q.all([prevQualChoicesPromise, prevExpChoicesPromise, seedDataPromise]).then(
      () => {
        this.configureDataForSummary()
      }
    )
  }

  getSeedCompCount() {
    const url = `${this.apiBaseUrl}segments/${this.segmentId}/seed_candidates?limit=0`
    this.$http.get(url).then(
      (response) => {
        this.seedCompCount = response.data.total
      }
    )
  }

  /*
    Set each category in prevChoices to map to an array of objects that we ng-repeat over
    to populate each summary box with the appropriate information. Each object in the array
    represents a selection that was made for a category.
  */
  configureDataForSummary() {
    _.forEach(this.prevChoices, (value, categoryName) => {
      let allValues = ''
      _.forEach(this.prevChoices[categoryName], (value) => {
        let valueText = typeof value === "string" ? value : value.join(", ")
        const inSeedList = _.includes(this.seedAttrData[categoryName], value)
        if (inSeedList) {
          valueText = `${valueText.trim()} (in seed)`
        }
        allValues = `${allValues}
                     ${valueText}`.trim()
      })
      this.prevChoices[categoryName] = allValues.trim()
    })
    this.tpOpenFlag = true
    this.inited = true
  }

  runModel() {
    const url = `${this.apiBaseUrl}segments/${this.segmentId}/model_run`
    this.$http.post(url)
    // go to success page whether it runs ok or not
    this.toSuccessPage()
  }

  toSuccessPage() {
    this.$state.go('segment.create.success')
  }

  displayName(category) {
    return this.displayNameMap[category]
  }

  inCreationFlow() {
    return this.$state.is('segment.create.summary')
  }

  showTopPaneMsg() {
    const titlePanes = document.querySelectorAll('.title-pane')
    const topPane = titlePanes[0]
    return this.isPaneActive(topPane)
  }

  showBottomPaneMsg() {
    const titlePanes = document.querySelectorAll('.title-pane')
    const bottomPane = titlePanes[1]
    return this.isPaneActive(bottomPane)
  }

  isPaneActive(titlePaneElem) {
    return titlePaneElem ? titlePaneElem.classList.contains('active') : false
  }

}

export default CreationSummaryController
