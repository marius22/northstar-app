/*eslint-disable */
// Don't lint this file for now because we imported this file from elsewhere
// and it'll take a long time to make it comply with our linter
import _ from 'lodash'
import angular from 'angular'
/*
    @typedef Table
    @type Object
    @description
        Object returned by TableService.createTable(config).
    @property {string} subject - Subject of table. Specified in config. Used as resource name in CRUD routes.
                               For example, if config.subject = "ExampleCompanies", then the default CRUD
                               route will be "api.base.url/examplecompanies/:id"
    @property {Array} header - Each object in the array defines a header column. Specified in config.
    @property {boolean} loading - true if waiting for some server response, intended for conditionally applying
                                loading animations.
    @property {Array} data - Each object in the array defines an "instance" of Table.subject
    @property {Object} _currParams - The query params used in the most recent reload()
    @property {string/number} totalItems - The server-side total items in the table. Set to be "--"
                                            when table is first initialized, for display purposes.
    @method reload(params)
        @returns {Promise}
        @description
            Performs a GET for all objects i.e GET api.base.url/examplecompanies. Automatically called
            after createEntry, updateEntry, and deleteEntry.
    @method createEntry(params)
        @return {Promise}
        @description
            Performs a POST request to create a new object i.e POST api.base.url/examplecompanies
    @method updateEntry(entryId, params)
        @return {Promise}
        @description
            Performs PUT request to update object i.e PUT api.base.url/examplecompanies/:id
    @method deleteEntry(entryId)
        @return {Promise}
        @description
            Performs DELETE request to delete object i.e DELETE api.base.url/examplecompanies/:id
    @method configureCrudResource(resourceConfig)
        @return {undefined}
        @description
            Customize the configuration of the Table's $resource if necessary.
            Refer to https://docs.angularjs.org/api/ngResource/service/$resource for more info.
    @method reloadOnAction(tableState)
        @return {undefined}
        @description
            This method is solely for use with our angular-smart-table's st-pipe directive, to
            do server-side pagination/sorting (Hence, the table reloads whenever an action is taken).
            Note that you just pass the method without calling it:
              <table st-table="myController.myTable.data" st-pipe="myController.myTable.reloadOnAction">
               .....
              </table>
    @method getResourceConfig()
        @return {Object}
        @description
            Returns the current resource config object for convenience when using configureCrudResource.
    @method shouldShowColumn(index)
        @return {boolean}
        @description
            Pass a header item's index and this method tells you if that item should be displayed or not.
    @method goToNextHeaderSection()
        @return {undefined}
        @decription
            Will set headerSection to += 1 and change the table's displayedFields accordingly
    @method goToPrevHeaderSection()
        @return {undefined}
        @description
            Counterpart to goToNextHeaderSection().
    @method disableHeaderNext()
        @return {boolean}
        @description
            Tells you if the header next button should be disabled.
    @method disableHeaderPrev()
        @return {boolean}
        @description
            Counterpart to disableHeaderNext()
    @method shouldShowPrevNext()
        @return {boolean}
        @description
            Tells you if header has enough items to warrant the presence of
            the navigation buttons.
    @example
        class MyController {
            constructor(tableService){
                let myTableConfig = {
                    subject: "companies",
                    header: [
                        {text: "Name"},
                        {text: "Industry"}
                    ]
                }
                this.myTable = tableService.createTable(myTableConfig)
            }

            exampleStuff() {
                this.myTable.reload()

                console.log(this.myTable.data)
                // [{id: 1, name: "Google", industry: "Technology"}]

                let newEntryParams = {
                    name: "Everstring",
                    industry: "Technology"
                }
                this.myTable.createEntry(newEntryParams)
                console.log(this.myTable.data)
                // [{id: 1, name: "Google", industry: "Technology"},
                //  {id: 2, name: "Everstring", industry: "Technology"}]

                this.myTable.updateEntry(2, {industry: "Awesomeness"})
                console.log(this.myTable.data)
                // [{id: 1, name: "Google", industry: "Technology"},
                //  {id: 2, name: "Everstring", industry: "Awesomeness"}]

                this.myTable.deleteEntry(1)
                console.log(this.myTable.data)
                // [{id: 2, name: "Everstring", industry: "Technology"}]
            }
        }
*/

/*
    @ngdoc service
    @name TableService
    @description
        TableService generates table objects with methods to handle CRUD operations
        and their possible interactions with other table concepts (sorting, pagination, etc.)
    @param {service} $resource - angular's $resource for table CRUD operations
    @param {constant} apiBaseUrl - our application back-end api base url
    @param {service} $log - angular's $log
    @param {service} $q - angular's $q
 */

// Here, we use Symbol for 'private' fields/methods

const _apiBaseUrl = Symbol('apiBaseUrl')
const _tables = Symbol('tables')
const _crudResources = Symbol('crudResources')
const _index = Symbol('index')
const _configureCrudResource = Symbol('configureCrudResource')
const _reloadTable = Symbol('reloadTable')
const _createEntry = Symbol('createEntry')
const _updateEntry = Symbol('updateEntry')
const _deleteEntry = Symbol('deleteEntry')
const _reloadOnAction = Symbol('reloadOnAction')
const _currParams = Symbol('currParams')
const _updateLoading = Symbol('uploadLoading')
const _shouldShowColumn = Symbol('shouldShowColumn')
const _goToNextHeaderSection = Symbol('goToNextHeaderSection')
const _goToPrevHeaderSection = Symbol('goToPrevHeaderSection')
const _disableHeaderNext = Symbol('disableHeaderNext')
const _disableHeaderPrev = Symbol('disableHeaderPrev')
const _setHeaderSection = Symbol('setHeaderSection')
const _setDisplayedFields = Symbol('setDisplayedFields')
const _shouldShowPrevNext = Symbol('shouldShowPrevNext')

class TableService {
  /* @ngInject */
  constructor($resource, apiBaseUrl, $log, $q, userService) {
    this[_apiBaseUrl] = apiBaseUrl
    this[_tables] = []
    this[_crudResources] = []

    // private methods. table methods interface with these.
    this[_configureCrudResource] = function (index, config) {
      this[_crudResources][index] = $resource(
        config.url,
        config.paramDefaults,
        config.actions,
        config.options
      )
    }

    // flag: true/false
    this[_updateLoading] = function (index, flag) {
      const table = this[_tables][index]
      table.loading = flag
      if (flag) {
        table.onLoadingStart()
      } else {
        table.onLoadingEnd()
      }
    }

    this[_reloadTable] = function (index, params, fromPipe=false) {
      const table = this[_tables][index]
      const resource = this[_crudResources][index]
      // When we reload, we want to go back to page 1 automatically if it's not a pagination
      // reload. Smart-table's pipe handles this for sorting, but we need to handle it manually
      // for filtering. We also want to set params.limit based on the current pagination.number
      // in the table's state now that we're exposing different rows per page options in the UI
      if (!fromPipe && params) {
        params.offset = 0
        params.limit = table.state.pagination.number
      }
      // make sure offset is not NaN, which will cause company list api broken
      if (params && _.isNaN(params.offset)) {
        params.offset = 0
      }
      const query = resource.get(params)
      const self = this
      this[_updateLoading](index, true)
      return query.$promise.then(
        function successHandler(response) {
          // table.state exists if our custom smart-table pipe is called at least once, which should
          // happen automatically the very first time the table loads. Still, this is a precaution.
          if (table.state) {
            table.state.pagination.numberOfPages = Math.ceil(response.total/params.limit)
            // If reloadTable is not called from our custom smart-table pipe (i.e reloadOnAction),
            // then we need to manually tell the table that it's back on page 1
            if (!fromPipe) {
              table.state.pagination.start = 0
            }
          }
          const filterIsCurrentlyActive = ("where" in params) && (Object.keys(params["where"]).length > 0)
          if (filterIsCurrentlyActive) {
            table.totalFilteredItems = response.total
          } else {
            table.totalItems = response.total
            table.totalFilteredItems = "--"
          }
          table[_currParams] = params
          // front-end enforcement of trial user company limits, assumed to be 5 companies for now
          const limitTrialUserInCompanyList = userService.isTrial() && table.subject === 'Recommendation Companies'
          if (limitTrialUserInCompanyList) {
            table.data = []
            const isFirstPage = table[_currParams].offset === 0
            let end = 10
            if (isFirstPage) {
              table.data = response.data.slice(0, 5)
              end = 5
            }
            for (let i = 0; i < end; i++) {
              const lockedCompany = {'companyName': 'LOCKED_CMP'}
              table.data.push(lockedCompany)
            }
          } else {
            table.data = response.data
          }
          self[_updateLoading](index, false)
          return response
        },
        function errorHandler(response) {
          $log.log(
            `Error getting data for ${table.subject} table:
                        ${response.status} ${response.statusText}`
          )
          self[_updateLoading](index, false)
          return $q.reject(response)
        }
      )
    }

    this[_createEntry] = function (index, params) {
      const resource = this[_crudResources][index]
      const newEntry = new resource(params)
      const self = this
      return newEntry.$save().then(
        function successHandler(response) {
          self[_reloadTable](index)
          return response
        },
        function errorHandler(response) {
          $log.log(
            `Error creating new Entry:
                        ${response.status} ${response.statusText}`
          )
          return $q.reject(response)
        }
      )
    }

    this[_updateEntry] = function (index, entryId, params) {
      const resource = this[_crudResources][index]
      const self = this
      const updatedEntry = resource.update(
        { id: entryId },
        params
      )

      return updatedEntry.$promise.then(
        function successHandler(response) {
          self[_reloadTable](index)
          return response
        },
        function errorHandler(response) {
          $log.log(
            `Error updating entry ${entryId}:
                        ${response.status} ${response.statusText}`
          )
          return $q.reject(response)
        }
      )
    }

    this[_deleteEntry] = function (index, entryId) {
      const resource = this[_crudResources][index]
      const self = this
      const deletedEntry = resource.delete(
        { id: entryId }
      )
      return deletedEntry.$promise.then(
        function successHandler(response) {
          self[_reloadTable](index)
          return response
        },
        function errorHandler(response) {
          $log.log(
            `Error deleting entry ${entryId}:
                        ${response.status} ${response.statusText}`
          )
          return $q.reject(response)
        }
      )
    }

    this[_reloadOnAction] = function (index, tableState, defaultParams) {
      const table = this[_tables][index]
      table.state = tableState
      const pagination = tableState.pagination
      const sort = tableState.sort
      const queryParams = {}
      angular.extend(queryParams, defaultParams)
      const filterIsCurrentlyActive = ("where" in table[_currParams]) && (Object.keys(table[_currParams]["where"]).length > 0)
      // we want to include the active filter(s) in our query params when we change pages or sort
      if (filterIsCurrentlyActive) {
        queryParams["where"] = table[_currParams]["where"]
      }

      const onlyExported = table[_currParams].onlyExported
      if (onlyExported) {
        queryParams["onlyExported"] = 1
      }

      const currentPage = Math.floor(pagination.start / pagination.number)
      queryParams.offset = currentPage*(pagination.number)
      queryParams.limit = pagination.number
      if ("predicate" in sort) {
        queryParams.order = sort.reverse ? `-${sort.predicate}` : sort.predicate
      }

      this[_reloadTable](index, queryParams, true)
    }

    this[_shouldShowColumn] = function (index, $colIdx) {
      const table = this[_tables][index]
      const isAnchorColumn = $colIdx === 0
      const isInDisplayedGroup = ($colIdx < table.headerSection * (table.columnCutoff - 1) + 1) &&
        ($colIdx > (table.headerSection - 1) * (table.columnCutoff - 1))
      if (isAnchorColumn || isInDisplayedGroup) {
        return true
      }
      return false
    }

    this[_goToNextHeaderSection] = function (index) {
      const table = this[_tables][index]
      const nextSection = table.headerSection + 1
      this[_setHeaderSection](index, nextSection)
    }

    this[_goToPrevHeaderSection] = function (index) {
      const table = this[_tables][index]
      const prevSection = table.headerSection - 1
      this[_setHeaderSection](index, prevSection)
    }

    this[_disableHeaderNext] = function (index) {
      const table = this[_tables][index]
      const headerLength = table.header.length
      // handle an edge case where we have a full page of columns and are exactly at the end of the table
      const noMoreHeaderItems = table.headerSection * (table.columnCutoff-1) === (headerLength-1)
      return table.displayedFields.length < table.columnCutoff || noMoreHeaderItems
    }

    this[_disableHeaderPrev] = function (index) {
      const table = this[_tables][index]
      return table.headerSection === 1
    }

    this[_setHeaderSection] = function (index, sectionNum) {
      const table = this[_tables][index]
      table.headerSection = sectionNum
      this[_setDisplayedFields](index, sectionNum)
    }

    this[_setDisplayedFields] = function (index, sectionNum) {
      const table = this[_tables][index]
      table.displayedFields = [table.anchorKey]
      // shift by 1 because we always include the anchorKey, the first header column
      let headerIdx = 1 + (sectionNum - 1) * (table.columnCutoff - 1)
      while (headerIdx <= sectionNum * (table.columnCutoff - 1) && headerIdx < table.header.length) {
        table.displayedFields.push(table.header[headerIdx].key)
        headerIdx++
      }
    }

    this[_shouldShowPrevNext] = function (index) {
      const table = this[_tables][index]
      return table.header.length > table.columnCutoff
    }
  }

  /*
   Create table from configuration object.
   @memberof TableService
   @param {Object} config - a value to convert
   @param {string} config.subject - subject of the table. Used by default in CRUD route.
   @param {Array} config.header - array of objects, each object defines a header column
   @param {Object} config.defaultQueryParams - Params to use by default if not specified by
                                               a user action.
                                               We also need this to pass additional params
                                               that aren't available in smart-table's
                                               tableState.
   @returns {Table} newTable - new Table created from config
   */
  createTable(config) {
    const configInvalid = !('subject' in config && 'header' in config)
    if (configInvalid) {
      throw new TypeError('createTable config missing required fields.')
    }

    const tableService = this

    // define default table config and extend it with input config
    const newTable = {
      subject: '',
      loading: false,
      header: [],
      data: [],
      totalItems: "--",
      totalFilteredItems: "--",
      onLoadingStart: angular.noop,
      onLoadingEnd: angular.noop,
      columnCutoff: 6,
      headerSection: 1,
      anchorKey: 'companyName'
    }

    angular.extend(newTable, config)

    newTable[_currParams] = {}

    const tableIndex = tableService[_tables].length
    newTable[_index] = tableIndex

    // define default CRUD config for newTable's $resource
    const fullCrudUrlFormat = tableService[_apiBaseUrl] + newTable.subject.toLowerCase() + '/:id'
    const resourceConfig = {
      url: fullCrudUrlFormat,
      paramDefaults: { id: '@id' },
      // update with PUT isn't included by default, so we add that here
      actions: {
        update: {
          method: 'PUT'
        }
      },
      options: {}
    }

    tableService[_configureCrudResource](tableIndex, resourceConfig)

    const tableMethods = {
      reload: function (params) {
        return tableService[_reloadTable](this[_index], params)
      },
      createEntry: function (params) {
        return tableService[_createEntry](this[_index], params)
      },
      updateEntry: function (entryId, params) {
        return tableService[_updateEntry](this[_index], entryId, params)
      },
      deleteEntry: function (entryId) {
        return tableService[_deleteEntry](this[_index], entryId)
      },
      configureCrudResource: function (resourceConfig) {
        tableService[_configureCrudResource](this[_index], resourceConfig)
      },
      reloadOnAction: function (tableState) {
        return tableService[_reloadOnAction](newTable[_index], tableState, config.defaultQueryParams)
      },
      getResourceConfig: function () {
        return resourceConfig
      },
      getCurrParams: function () {
        return this[_currParams]
      },
      shouldShowColumn: function ($colIdx) {
        return tableService[_shouldShowColumn](this[_index], $colIdx)
      },
      goToNextHeaderSection: function () {
        return tableService[_goToNextHeaderSection](this[_index])
      },
      goToPrevHeaderSection: function () {
        return tableService[_goToPrevHeaderSection](this[_index])
      },
      disableHeaderNext: function () {
        return tableService[_disableHeaderNext](this[_index])
      },
      disableHeaderPrev: function () {
        return tableService[_disableHeaderPrev](this[_index])
      },
      shouldShowPrevNext: function () {
        return tableService[_shouldShowPrevNext](this[_index])
      }
    }

    angular.extend(newTable, tableMethods)
    tableService[_tables].push(newTable)

    // if header has more items than newTable is configured to show per section,
    // then we configure the table's displayedFields to be the first N header items
    // where columnCutoff === N
    const hasMoreColumnsThanCutoff = newTable.header.length > newTable.columnCutoff
    if (hasMoreColumnsThanCutoff) {
      tableService[_setHeaderSection](tableIndex, 1)
    }

    return newTable
  }
}

module.exports = {TableService}
