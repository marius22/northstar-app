import _ from 'lodash'


// When the `currentPage` is in the middle of a large number of pages,
// we show `k` pages in the middle section that someone can click
// and directly go to. `k` must be odd.
const k = 5

// Number of buffer pages to add for symmetry
const l = (k - 1) / 2
const ELLIPSIS = '...'

/**
 * Get the pages that we'll show in the pagination widget
 * @param i - The number of the current page
 * @param n - The total number of pages we're paginating on.
 * @returns pages - An array of page numbers from 1 to the last page.
 */
const getPages = (i, n) => {
  let pages = []

  // When there are few enough pages, no need to bother with ellipses.
  if (n <= k + l + 1) {
    pages = _.range(1, n + 1)
  } else if (i <= k) {
    // We show (k+l) pages, an ellipsis, and the last page. The idea is that
    // we definitely want to show the first k pages, then add an extra `l` for buffer.
    pages = _.flatten([_.range(1, (k + l) + 1), ELLIPSIS, n])
  } else if (i >= n - (k - 1)) {
    // We show page 1, and ellipsis, then (k+l) pages.
    pages = _.flatten([1, ELLIPSIS, _.range(n - (k - 1) - l, n + 1)])
  } else {
    // We show `k` pages in the middle that are sandwiched by the first page + ellipsis
    // and ellipsis + last page.
    const rangeStart = i - l
    const rangeEnd = i + l
    pages = _.flatten([1, ELLIPSIS, _.range(rangeStart, rangeEnd + 1), ELLIPSIS, n])
  }

  return pages
}

/**
 * The pagination controls
 */
class Pagination {
  constructor() {
    this.templateUrl = 'templates/pagination.html'
    this.restrict = 'E'
    this.scope = {
      numPages: '=',
      selectPageHandler: '&',
      toFirstPage: '=?'
    }
  }

  link(scope) {
    /* eslint-disable no-param-reassign */

    // Set initial values.
    // The number of the current page.
    scope.ELLIPSIS = ELLIPSIS
    scope.currentPage = 1
    scope.$watch('numPages', (newVal) => {
      scope.pages = getPages(scope.currentPage, newVal)
    })

    scope.selectPage = (pageNumStrMaybe) => {
      const pageNum = _.toInteger(pageNumStrMaybe)

      if (pageNum === ELLIPSIS) {
        return
      }

      if (pageNum < 1 || pageNum > scope.numPages) {
        return
      }
      // Recalculate values.
      scope.currentPage = pageNum
      scope.pages = getPages(scope.currentPage, scope.numPages)

      scope.selectPageHandler({pageNum})
    }

    scope.toFirstPage = () => {
      scope.selectPage(1)
    }

    /* eslint-enable no-param-reassign*/
  }
}
Pagination.$inject = []

module.exports = {Pagination, getPages, ELLIPSIS}
