class SummaryBox {
  constructor() {
    this.restrict = 'AE'
    this.replace = true
    this.scope = {
      summaryData: '=',
      summaryFilter: '='
    }
    this.templateUrl = 'templates/summary-box.html'
  }

  link(scope) {
    scope._summaryFilter = scope.summaryFilter ? scope.summaryFilter : ''
  }
}

module.exports = {SummaryBox}