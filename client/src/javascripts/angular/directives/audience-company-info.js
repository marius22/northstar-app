import _ from 'lodash'
import * as errors from 'errors'

class AudienceCompanyInfo {
  constructor(observeOnScope, $timeout, $sce, proxyService, alertService, $state, audienceService,
              salesforceService, ghostService, $filter) {
    this.observeOnScope = observeOnScope
    this.$timeout = $timeout
    this.salesforceService = salesforceService
    this.ghostService = ghostService

    this.restrict = 'E'
    this.replace = true
    this.templateUrl = 'templates/audience-company-info.html'
    this.alertService = alertService
    this.audienceService = audienceService

    this.scope = {
      // the function to close this directive
      closeCallback: '&',

      // The company that this netflix card is showing data about.
      company: '=',

      // Whether we're loading info for this or not
      isLoading: '='
    }

    this.$sce = $sce
    this.proxyService = proxyService
    this.$state = $state
    this.$filter = $filter
  }

  link(scope) {
    /* eslint-disable no-param-reassign */
    // navType: overview/insights/news/contacts
    scope.navType = 'overview'

    this.observeOnScope(scope, 'company')
      .subscribe((change) => {
        const company = change.newValue
        if (!company) {
          return
        }
        // netflix will keep all the data to display for the netflix card. That way,
        // we can still safely mutate `company` without overwriting existing keys.
        scope.netflix = {}
        const netflix = scope.netflix

        scope.fAdded = false

        this.$timeout(() => {
          scope.switchNav('overview')

          const location = company.location || {}

          const possibleSocialTypes = [
            'linkedin', 'facebook', 'twitter'
          ]
          netflix.name = company.name
          netflix.domain = company.domain
          netflix.newsUrl = this.$sce.trustAsResourceUrl(`https://www.bing.com/news/search?q="${company.name}"&go=Search&qs=n&form=QBNT&pq=${company.name}&sc=8-9&sp=-1&sk=`)

          netflix.social = _(possibleSocialTypes).filter((st) => {
            const urlMaybe = _.get(company, [st, 'url'])
            return urlMaybe
          }).map((st) => {
            let url = _.get(company, [st, 'url'])
            /*
              The following logic is to account for some discrepancies in our LinkedIn URL data.
              Original ticket: https://everstring.atlassian.net/browse/NSV2-2233

              There are 3 possible cases:
                1. URL begins with "www" (i.e "www.linkedin.com")
                2. URL begins with "http://" (i.e "http://www.linkedin.com")
                3. URL begins with "https://" (i.e "https://www.linkedin.com")

              Ultimately, we want to modify the URL (or, if it's case #3, we do nothing) such that
              it has https:// in front, i.e "https://www.linkedin.com".

              We have decided to do this for all social URLs (linkedin, facebook, twitter) just in case.
            */
            url = url.trim()
            if (url.indexOf("http://") === 0) {
              // "http://www.linkedin.com" => "www.linkedin.com"
              url = url.replace("http://", "")
            }
            if (url.indexOf("https://") !== 0) {
              // "www.linkedin.com" => "https://www.linkedin.com"
              url = `https://${url}`
            }

            return {
              type: st,
              link: url
            }
          }).value()

          // Handle missing location info.
          if (location.street && location.city && location.state && location.postalCode) {
            netflix.addressStr = `${location.street}
                                  </br>
                                  ${location.city}, ${location.state} ${location.postalCode}`
          } else {
            netflix.addressStr = null
          }
          netflix.phone = company.phone
          netflix.firmographicItems = {
            employeeSize: {
              dispName: 'Employees',
              value: _.get(company, 'employeeSize'),
            },
            revenueRange: {
              dispName: 'Revenue',
              value: _.get(company, 'revenueRange'),
            },
            industry: {
              dispName: 'Industry',
              value: _.get(company, 'industry')
            },
            contactCount: {
              dispName: 'Contacts',
              value: this.$filter('number')(_.get(company, 'contactCount', 0))
            },
            alexaRank: {
              dispName: 'Alexa Rank',
              value: this.$filter('number')(_.get(company, 'metrics.alexaGlobalRank'))
            }
          }

          scope.switchTagsInfo('keywords')

          // TODO: What if there are no advanced insights? Do we just hide the tab?
          scope.availableTabs = [
            'overview',
            'insights',
            'news',
            'contacts'
          ]
          if (this.audienceService.showSimilaritiesTab()) {
            scope.availableTabs.push('similarities')

            const similarityItems = this.audienceService.currentSimilarityItems
            scope.similarityItemsCount = similarityItems.length
            const intersectWith = this.audienceService.isSimilarDomainsState() ? company.similarDomain : company.keywords
            const simItemsWithLemmas = _.union(similarityItems, this.audienceService.lemmaTags)
            scope.simItemsInSearch = _.intersection(simItemsWithLemmas, intersectWith)

            scope.similaritiesTitle = this.audienceService.isSimilarDomainsState() ? 'Similar Domains' : 'Similar Keywords'

            const wording = this.audienceService.isCompanyListState() ? 'Your audience was created using' : 'You searched using'
            const count = similarityItems.length
            const noun = this.audienceService.isSimilarDomainsState() ? 'domain(s)' : 'keyword(s)'
            scope.similaritiesDescription = `${wording} ${count} ${noun}`
          }
        })
      })

    scope.changeActiveInsight = (insight) => {
      scope.netflix.activeInsight = insight
    }

    scope.switchTagsInfo = (type) => {
      scope.tagsType = type
      if (type === 'keywords') {
        scope.netflix.tagsInfo = scope.company.keywords
      } else if (type === 'tech') {
        scope.netflix.tagsInfo = scope.company.tech
      }
    }

    const getContactsInfo = () => {
      const companyId = scope.company.id
      const getContactsInfoUrl = `/common/contacts/distribution?companyIds=${companyId}`
      scope.loadingContacts = true
      this.proxyService
        .get(getContactsInfoUrl)
        .then(
          (response) => {
            const contactsInfo = response.data.data
            // If a company has contacts info available, then
            // the contactsInfo object will have the companyId
            // as a key, with the value being an object with
            // department and management level information.
            // If this key doesn't exist then there's no contact
            // info for this companyId.
            scope.netflix.currentContactsInfo = contactsInfo[companyId]
            scope.netflix.currentContactsTotal = contactsInfo.totalCount
          }
        )
        .catch(res => {
          throw new errors.HttpError(res)
        })
        .finally(() => {
          scope.loadingContacts = false
        })
    }

    const getInsightsInfo = () => {
      const domain = scope.company.domain
      const getInsightsInfoUrl = `/common/advanced_insights?domains=${domain}`
      scope.loadingInsights = true
      this.proxyService
        .get(getInsightsInfoUrl)
        .then((response) => {
          const insightsInfo = response.data.data
          scope.netflix.currentInsightsInfo = insightsInfo
          scope.netflix.activeInsight = !_.isEmpty(insightsInfo) ? scope.netflix.currentInsightsInfo[0] : undefined
        })
        .catch(res => {
          throw new errors.HttpError(res)
        })
        .finally(() => {
          scope.loadingInsights = false
        })
    }

    scope.getBarWidth = (contactDatum) => {
      const decimal = contactDatum.count / scope.netflix.currentContactsTotal
      const normalized = decimal * 100
      // want at least a tiny bit of bar width, even when our rounding goes to 0
      const percentage = Math.max(normalized.toFixed(0), 1)
      return `'${percentage}%'`
    }

    scope.switchNav = (type) => {
      scope.navType = type
      if (type === 'contacts' && _.isEmpty(scope.netflix.currentContactsInfo)) {
        getContactsInfo()
      }
      if (type === 'insights') {
        getInsightsInfo()
      }
    }

    scope.dispIndicatorValue = (val) => {
      // we should be able to expect only 0 or 1, but there
      // may be some old data lying around, so just display
      // the raw value if it's not 0 or 1
      if (val === 0) {
        return 'False'
      }
      if (val === 1) {
        return 'True'
      }
      return val
    }

    scope.close = () => {
      scope.company = null
      scope.netflix = {}
      scope.closeCallback()
    }

    scope.showAddToCrm = () => {
      const isSavedAudience = this.audienceService.isCompanyListState()
      scope.company = scope.company || {}
      const inCrm = _.get(scope, 'company.inCrm', false)

      return isSavedAudience && !inCrm
    }

    scope.addToCrm = () => {
      scope.fLoading = true
      const domain = scope.company.domain
      const accountId = this.ghostService.accountId()
      this.salesforceService.httpAddSfdcObj(domain, accountId).then((res) => {
        scope.fLoading = false
        scope.fAdded = true

        const {operation_type: operationType, num_accounts: numAccounts} = res.data.data

        if (operationType === 'insert') {
          this.alertService.addAlert('success', `Added a Salesforce Account for ${domain}`, '#topAlert')
        } else {
          if (numAccounts === 1) {
            this.alertService.addAlert('success', `Enriched the Salesforce Account matching ${domain}`, '#topAlert')
          } else {
            this.alertService.addAlert('success', `Enriched ${numAccounts} Salesforce Accounts matching ${domain}`, '#topAlert')
          }
        }
      }, (response) => {
        scope.fLoading = false
        this.alertService.addAlert('error', response.data.message || response.statusText)
      })
    }

    /* eslint-enable no-param-reassign */
  }
}
AudienceCompanyInfo.$inject = ['observeOnScope', '$timeout', '$sce',
  'proxyService', 'alertService', '$state',
  'audienceService', 'salesforceService', 'ghostService',
  '$filter']

module.exports = {AudienceCompanyInfo}
