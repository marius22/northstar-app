import $ from 'npm-zepto'
import Highcharts from 'highcharts'
import _ from 'lodash'

// A datum with this `value` means that no data could
// be found.
const NULL = 'Others'
const ABSOLUTE_DATA_LIMIT = 500
const columnBlue = '#3F53B2'

const toHighChartsData = (data, limit, $filter, sort = true) => {
  const dataWithoutNULL = _.filter(data, (datum) => {
    return datum.value !== NULL
  })

  let descendedSortData
  if (sort) {
    descendedSortData = _(dataWithoutNULL)
      .sortBy((datum) => {
        return datum.count
      }).reverse().value()
  } else {
    descendedSortData = _(dataWithoutNULL).value()
  }

  // The buckets that each count is for on the x-axis.
  let categories = _(descendedSortData)
    .map((datum) => {
      return datum.value
    }).value()

  // The counts for each bucket, in order of the buckets.
  let counts = _(descendedSortData)
          .map((datum) => {
            return _.parseInt(datum.count)
          }).value()

  // Get 'Others' from the data if its count > 0
  const dataWithNULL = _.filter(data, (datum) => {
    return datum.value === NULL && datum.count > 0
  })

  // We use an absolute limit of 500 on our # of non-null data points,
  // there are potential performance problems beyond that.
  if (categories.length > ABSOLUTE_DATA_LIMIT) {
    // Move the counts of the data points that exceed the limit to 'Others'
    const countsBeyondLimit = counts.slice(ABSOLUTE_DATA_LIMIT, counts.length+1)
    if (dataWithNULL.length === 0) {
      dataWithNULL.push({
        count: 0,
        value: 'Others'
      })
    }
    dataWithNULL[0].count += _.sum(countsBeyondLimit)
    categories = categories.slice(0, ABSOLUTE_DATA_LIMIT)
    counts = counts.slice(0, ABSOLUTE_DATA_LIMIT)
  }

  // Add 'Others' to the end of the chart if appropriate
  if (dataWithNULL.length !== 0) {
    const nullCount = dataWithNULL[0].count

    categories.push('Others')
    counts.push(nullCount)
  }

  // Total counts. Used for calculating percentages.
  const totalCount = _.sum(counts)

  // Slice down to the limit, if number is provided. We do this at the very end
  // so that our totalCount still reflects the actual amount of data
  // and gives us accurate percentages.
  let limitExceeded = false
  if (limit) {
    limitExceeded = categories.length > limit
    categories = categories.slice(0, limit)
    counts = counts.slice(0, limit)
  }

  // Show count next to label
  for (let i = 0; i < categories.length; i++) {
    const formattedNum = $filter('number')(counts[i])
    categories[i] = `${categories[i]} (${formattedNum})`
  }

  return {categories, counts, totalCount, limitExceeded}
}
/**
 * Creates a horizontal bar chart using HighCharts
 *
 * Known bug: There's extra white space at the bottom of a bar chart.
 */
class ColumnChart {
  constructor($compile, showModalService, $filter) {
    this.templateUrl = 'templates/column-chart.html'
    this.restrict = 'E'
    this.scope = {
      // Raw data as an array of {count: Number, value: String}
      data: '=',
      modalTitle: '@',
      // viewMore determines if we have a "View More" button on the chart or not.
      // Needed so that viewing the chart in a modal doesn't also let you open
      // a redundant chart modal on top.
      viewMore: '@',
      // limit is the max data points that will be rendered for the chart.
      // We also have a hard limit of 500 for performance reasons.
      limit: '@',
      notSort: '@'
    }
    this.replace = true
    this.$compile = $compile
    this.sms = showModalService
    this.$filter = $filter
  }

  link(scope, element) {
    /* eslint-disable no-param-reassign */
    const isSort = !scope.notSort
    const hcData = toHighChartsData(scope.data, scope.limit, this.$filter, isSort)

    // for cases where we don't have ES6 block scoping
    const self = this

    // Config that doesn't show lines for tick marks/axes.
    const noAxisConfig = {
      // Disables both axes's various lines.
      lineWidth: 0,
      minorGridLineWidth: 0,
      lineColor: 'transparent',
      labels: {
        enabled: false
      },
      minorTickLength: 0,
      tickLength: 0,

      // This one is needed to clear our y-axis grid lines.
      gridLineColor: 'transparent',

      // Disabled y-axis title
      title: {
        text: null
      }
    }

    const xAxisConfig = _.assign({}, noAxisConfig, {
      categories: hcData.categories,
      labels: {
        align: 'left',
        reserveSpace: false,
        x: -40,
        y: 0,
        style: {
          color: '#666',
          fontSize: '12pt',
          fontWeight: 'normal',
          textOverflow: 'none',
          whiteSpace: 'nowrap'
        },
        rotation: 270
      }
    })

    const barThickness = 57
    const barPadding = 10
    const columnContainer = $(element)[0][0]
    Highcharts.chart(columnContainer, {
      tooltip: {
        enabled: false
      },
      plotOptions: {
        column: {
          tooltip: {
            followPointer: true,
            pointFormatter() {
              const percent = 100 * (this.y / hcData.totalCount)
              let toFixedCutoff = 0
              if (percent < 10) {
                toFixedCutoff = 1
              }
              if (percent < 0.1) {
                toFixedCutoff = 2
              }
              return $('<span />', {
                text: `Company count: ${self.$filter('number')(this.y)} (${percent.toFixed(toFixedCutoff)}%)`,
                class: 'highcharts-tooltip'
              }).html()
            }
          }
        }
      },
      scrollbar: {
        enabled: true
      },
      chart: {
        type: 'column',
        // We scale the width according to the number of categories so that there's
        // enough space between each column.
        width: hcData.categories.length * (barThickness + barPadding + 20),
        spacingTop: -40
      },
      title: {
        text: ''
      },
      xAxis: xAxisConfig,
      yAxis: noAxisConfig,
      series: [{
        data: hcData.counts,
        minPointLength: 3,
        // These are the percentage labels on each column.
        dataLabels: {
          y: 20,
          enabled: true,
          color: '#fff',
          inside: true,
          align: 'center',
          style: {
            fontSize: '24px',
            fontWeight: 800,
            textShadow: '1px 1px 2px black'
          },

          // Null out `format` and use `formatter` to label
          // the columns.
          format: null,
          formatter() {
            const percent = 100 * (this.y / hcData.totalCount)
            let toFixedCutoff = 0
            if (percent < 10) {
              toFixedCutoff = 1
            }
            if (percent < 0.1) {
              toFixedCutoff = 2
            }
            return `${percent.toFixed(toFixedCutoff)}%`
          }
        },
        color: columnBlue,

        // Bar width
        pointWidth: barThickness
      }],
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      }
    }).redraw()

    if (scope.viewMore && hcData.limitExceeded) {
      const addViewMoreButton = () => {
        scope.modalParams = {
          chartConfig: {
            chartTitle: scope.modalTitle ? scope.modalTitle : scope.title,
            chartData: scope.data
          }
        }

        scope.sms = this.sms

        const viewMoreDiv = `<div class="view-more" ng-click="sms.columnChart(modalParams)">View More</div>`
        const compiledDiv = this.$compile(viewMoreDiv)(scope)
        angular.element(columnContainer).append(compiledDiv)
      }
      addViewMoreButton()
    }

    /* eslint-enable no-param-reassign*/
  }
}
ColumnChart.$inject = ['$compile', 'showModalService', '$filter']

module.exports = {ColumnChart}
