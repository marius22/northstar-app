/* eslint-disable no-param-reassign */

class TitlePane {
  constructor() {
    this.templateUrl = 'templates/title-pane.html'
    this.restrict = 'E'
    this.scope = {
      title: '@',
      // If you only have 2 title panes on a page, setting binaryToggle to true on both
      // will cause one to open when its counterpart closes
      binaryToggle: '@',
      // Use this to trigger the opening of a title pane via 2-way binding with some
      // controller field
      initialOpenFlag: '=',
      alwaysOpen: '='
    }
    this.transclude = true
  }

  link(scope, element, attrs) {
    scope.showSection = false || scope.alwaysOpen
    scope.ignoreBroadcast = false

    scope.$on('titlePane: open', () => {
      if (scope.ignoreBroadcast) {
        scope.ignoreBroadcast = false
      } else {
        scope.showSection = true
      }
    })

    scope.$on('titlePane: close', () => {
      if (scope.ignoreBroadcast) {
        scope.ignoreBroadcast = false
      } else {
        scope.showSection = false
      }
    })

    const removeOpenWatch = scope.$watch('initialOpenFlag', (newVal) => {
      if (newVal) {
        scope.showSection = true
        removeOpenWatch()
      }
    })

    scope.shouldShowSection = () => {
      return scope.showSection || scope.alwaysOpen
    }

    scope.toggleSection = () => {
      scope.showSection = !scope.showSection || scope.alwaysOpen

      // Tell other title panes to close if this title pane is toggled open.
      // We assume that all title pane scopes are siblings.
      if (scope.showSection) {
        scope.ignoreBroadcast = true
        // If you're using ng-repeat to make a bunch of title panes,
        // we need to broadcast one $parent scope deeper because of the
        // additional ng-repeat scope
        if (attrs.ngRepeat) {
          scope.$parent.$parent.$broadcast('titlePane: close')
        } else {
          scope.$parent.$broadcast('titlePane: close')
        }
      }
      // Tell the other title pane to open if this title pane is toggled closed and binaryToggle === true
      if (!scope.showSection && scope.binaryToggle) {
        scope.ignoreBroadcast = true
        scope.$parent.$broadcast('titlePane: open')
      }
    }
  }
}

module.exports = {TitlePane}
