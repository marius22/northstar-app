import $ from 'npm-zepto'
import Highcharts from 'highcharts'
import _ from 'lodash'
import angular from 'angular'

// A datum with this `value` means that no data could
// be found.
const NULL = 'Others'

const toHighChartsData = (data) => {
  const dataWithoutNULL = _.filter(data, (datum) => {
    return datum.value !== NULL
  })

  const descendedSortData = _(dataWithoutNULL)
        .sortBy((datum) => {
          return datum.count
        }).reverse().value()

  // The counts for each bucket, in order of the buckets.
  const counts = _(descendedSortData)
    .map((datum) => {
      return _.parseInt(datum.count)
    }).value()

  // Total counts. Used for calculating percentages.
  const totalCount = _.sum(counts)

  const seriesDataForPie = []
  for (const datum of descendedSortData) {
    const pieDatum = {
      y: datum.count,
      name: datum.value
    }
    seriesDataForPie.push(pieDatum)
  }

  return {seriesDataForPie, totalCount}
}
/**
 * Creates a donut chart using HighCharts
 */

class DonutChart {
  constructor($filter, $window) {
    this.templateUrl = 'templates/donut-chart.html'
    this.restrict = 'EA'
    this.scope = {
      // Raw data as an array of {count: Number, value: String}
      data: '=',
      title: '@',
      // subject is used as a unique id on the render-donut div so that we
      // can target it precisely if we have multiple donut charts on the same page
      subject: '@'
    }
    this.replace = true
    this.$filter = $filter
    this.$window = $window
  }

  link(scope, element) {
    /* eslint-disable no-param-reassign */

    const donutContainer = $($(element)[0][0])

    // We need to add this div in this fashion, rather than including it in
    // donut-chart.html. Otherwise, id={{subject}} will not be set properly
    // when we render the chart

    const renderDonutDiv = `<div class="render-donut" id="${scope.subject}">
                            </div>`
    donutContainer.append(renderDonutDiv)

    scope.$watch('data', (newVal) => {
      if (newVal) {
        const hcData = toHighChartsData(newVal)
        scope.drawChart(hcData)
      }
    })

    let added = false
    const addTotalCountOverlay = (hcData) => {
      if (added) {
        return
      }

      const totalCountDisplay = this.$filter('number')(hcData.totalCount)
      const totalCountOverlay = `<div class="total-count-overlay">
                                <div class="total-count" id="${scope.subject}-total-count">
                                  <div class="label">
                                    Total Count
                                  </div>
                                  <div class="value">
                                    ${totalCountDisplay}
                                  </div>
                                </div>
                              </div>`
      donutContainer.append(totalCountOverlay)
      added = true
    }

    // for cases where we don't have ES6 block scoping
    const self = this

    scope.drawChart = (hcData) => {
      addTotalCountOverlay(hcData)
      const donutChart = Highcharts.chart({
        tooltip: {
          headerFormat: `<span style="font-size: 10px">{point.key}</span><br/>`,
          pointFormatter() {
            const percent = 100 * (this.y / hcData.totalCount)
            let toFixedCutoff = 0
            if (percent < 10) {
              toFixedCutoff = 1
            }
            if (percent < 0.1) {
              toFixedCutoff = 2
            }
            return $('<span />', {
              text: `Company count: ${self.$filter('number')(this.y)} (${percent.toFixed(toFixedCutoff)}%)`,
              class: 'highcharts-tooltip'
            }).html()
          }
        },
        chart: {
          type: 'pie',
          style: {
            fontFamily: 'Proxima Nova'
          },
          renderTo: scope.subject
        },
        colors: [
          '#2a97f2',
          '#8ec351',
          '#38b295',
          '#f5a83f',
          '#9946c6',
          '#bf2e63'
        ],
        title: {
          text: scope.title,
          align: 'left',
          useHTML: true,
          style: {
            fontWeight: 600,
            textTransform: 'uppercase',
            fontSize: '14px',
            color: 'black'
          },
          x: 10,
          y: 18
        },
        series: [{
          name: 'Company count',
          showInLegend: true,
          innerSize: '60%',
          data: hcData.seriesDataForPie,
          point: {
            events: {
              legendItemClick() {
                return false
              }
            }
          },
          // These are the percentage labels on each slice.
          dataLabels: {
            enabled: true,
            color: '#fff',
            distance: -28,
            style: {
              fontSize: '22px',
              fontWeight: 'bold',
              textShadow: '1px 1px 2px black'
            },

            // Null out `format` and use `formatter` to label
            // the slices.
            format: null,
            formatter() {
              const percent = 100 * (this.y / hcData.totalCount)
              if (percent < 7) {
                // Don't show a label for very small percentages because it won't fit
                // on the slice.
                return ''
              }
              return `${percent.toFixed(0)}%`
            }
          }
        }],
        credits: {
          enabled: false
        },
        legend: {
          enabled: true,
          align: "right",
          layout: "vertical",
          lineHeight: 14,
          symbolRadius: 10,
          symbolHeight: 14,
          symbolWidth: 14,
          symbolPadding: 12,
          itemMarginBottom: 5,
          verticalAlign: "middle",
          y: 30
        }
      })


      const correctChartLayout = () => {
        // calling reflow() here helps the chart size itself properly on initial page view
        // IMPORTANT: Must call reflow() before we position the total-count div
        donutChart.reflow()

        const textX = donutChart.plotLeft + (donutChart.series[0].center[0])
        const textY = donutChart.plotTop + (donutChart.series[0].center[1])
        // position in center of donut
        const div = $(`#${scope.subject}-total-count`)
        div.css('left', textX + (div.width() * -0.5))
        div.css('top', textY + (div.height() * -0.5))
      }

      correctChartLayout()

      const w = angular.element(this.$window)
      w.bind('resize', _.debounce(correctChartLayout, 200))
    }
    /* eslint-enable no-param-reassign*/
  }
}
DonutChart.$inject = ['$filter', '$window']

module.exports = {DonutChart}
