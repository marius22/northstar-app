import Papa from 'papaparse'
import _ from 'lodash'

const DEFAULT_VALIDATION_RULES = {
  maxLines: 100000, // max lines allowed for the csv
  minLines: undefined, // min lines alowed for the csv
  numColumns: [1, 2], // num columns that the csv must have
}

class CSVUploader {
  constructor(Upload, alertService, $state, $timeout, modelService) {
    this.templateUrl = 'templates/csv-uploader.html'
    this.restrict = 'AE'
    this.replace = true
    /**
     *
     * onlyValidate - hand off actual upload responsibility to a controller.
     *                Selecting/dropping a csv in the uploader will only validate it.
     *
     * onValidateSuccess - method that triggers when validation is successful. Intended as a way
     *                     to pass the validated file to a controller.
     *
     * methods - expose internal method for outside controllers, extensible. for now there is only api.upload method
     *
     * validateLocally - true/false. Defaults to 'false' behavior.
     *                   If true, then ngf-select and ngf-drop will result in
     *                   local validation, and an action that actually uploads the CSV
     *                   to the server (i.e clicking 'Enrich' in the Enrich List flow)
     *                   will trigger the actual upload logic.
     *
     *                   If false, then ngf-select and ngf-drop will result in upload logic
     *                   immediately happening.
     *
     * validated - two-way binding on true/false
     *
     * validationRules - object with validation parameters for client-side validation.
     *                    Optional, and will use DEFAULT_VALIDATION_RULES if not provided.
     *                    See DEFAULT_VALIDATION_RULES for possible params.
     *
     *
     * onUploadSuccess - Takes a server response as input. Will run when
     *                   the uploadUrl endpoint returns successfully
     *
     *
     * uploadUrl - url that we will make a POST request to with the file uploader
     *
     * uploadParams - non-file params to be included in upload data
     *
     */

    this.scope = {
      onlyValidate: '=',
      onValidateSuccess: '&',
      methods: '=?',
      validateLocally: '=',
      forModel: '=?',
      validated: '=',
      validationRules: '=?',
      onUploadSuccess: '&',
      onUploadFailed: '&',
      uploadUrl: '=',
      uploadParams: '=',
      alertTarget: '@'
    }
    this.uploader = Upload
    this.alertService = alertService
    this.$state = $state
    this.$timeout = $timeout
    this.modelService = modelService
  }

  link(scope) {
    // true while we're waiting for a response from the uploadUrl endpoint
    scope.uploading = false

    // true if upload is successful
    scope.uploaded = false

    // true while Papa is validating the file locally
    scope.validating = false

    scope._onUploadSuccess = scope.onUploadSuccess || _.noop
    scope._onUploadFailed = scope.onUploadFailed || _.noop
    scope._onValidateSuccess = scope.onValidateSuccess || _.noop

    // for each specific rule, use the default if it's not specified in scope.validationRules
    scope._validationRules = _.extend(_.cloneDeep(DEFAULT_VALIDATION_RULES), scope.validationRules)

    const addErrorAlert = (message) => {
      if (!scope.alertTarget) {
        this.alertService.addAlert('error', message)
      } else {
        this.alertService.addAlert('error', message, `${scope.alertTarget}`)
      }
    }

    scope.uploadClick = ($event) => {
      $event.stopPropagation()
      scope.upload()
    }

    scope.upload = (file) => {
      if (scope._validatedFile) {
        file = scope._validatedFile
      }

      scope.uploading = true

      if (!file) {
        addErrorAlert('Invalid file.')
        scope.uploading = false
        return
      }

      const extraData = scope.uploadParams || {}

      const fileData = _.extend({file}, extraData)
      this.uploader.upload({
        url: scope.uploadUrl,
        data: fileData
      }).then((resp) => {
        scope.uploaded = true
        scope.uploading = false
        scope._onUploadSuccess({response: resp, csvFile: file})
      }, (resp) => {
        this.alertService.addAlert('error', resp.data.message)
        scope._onUploadFailed({response: resp, csvFile: file})
        scope.uploaded = false
        scope.uploading = false
      })
    }

    scope.validateFile = (file) => {
      if (_.isNull(file)) {
        return
      }

      if (!file) {
        addErrorAlert('Invalid file.')
        return
      }

      const complete = (results) => {
        const data = results.data
        const errors = results.errors
        const rules = scope._validationRules
        // check errors
        if (errors.length > 0) {
          const error = errors[0]
          const rowNumber = error.row
          const errorMessage = error.message

          if (rowNumber) {
            this.$timeout(() => {
              addErrorAlert(`Error in row ${rowNumber}: ${errorMessage}`)
              scope.validating = false
            }, 0)
            return
          }
        }

        // check minLines
        if (rules.minLines && data.length < rules.minLines) {
          this.$timeout(() => {
            addErrorAlert(`Your CSV must have at least ${rules.minLines} rows.`)
            scope.validating = false
          }, 0)
          return
        }
        // check maxLines
        if (rules.maxLines && data.length > rules.maxLines) {
          this.$timeout(() => {
            addErrorAlert(`Your CSV must have at most ${rules.maxLines} rows.`)
            scope.validating = false
          }, 0)
          return
        }
        // check numColumns
        const assumedHeader = data[0]
        const maxNumCols = _.max(rules.numColumns)
        const colsPastMax = _.slice(assumedHeader, maxNumCols)
        const nonEmptyColsPastMax = _.filter(colsPastMax, (colText) => {
          return !_.isEmpty(colText)
        })
        // We want to still allow files with an extra comma in the header row, which may be uploaded by clients
        // who use excel (which gives no visual indication that the file has an extra comma in the header).
        const onlyUpToTwoRealColumns = _.includes(rules.numColumns, assumedHeader.length) || _.isEmpty(nonEmptyColsPastMax)
        // assumedHeader === undefined if CSV is blank, so we want to check for that as well.
        if (!assumedHeader || !onlyUpToTwoRealColumns) {
          this.$timeout(() => {
            addErrorAlert(`Your CSV should have up to two columns: Company name and/or Company URL.`)
            scope.validating = false
          }, 0)
          return
        }

        this.$timeout(() => {
          if (scope.forModel) {
            const revisedFileName = `revised ${file.name}`
            const min = rules.minLines || 70
            // Use our c2d service to get all the domains we can find from the CSV
            // data. If the total found is less than min, then modelService
            // will show a modal window telling the user what's up.
            this.modelService
              .c2dCheckSeedDomains(data, min, revisedFileName)
              .then((valid) => {
                scope.validating = false
                if (valid) {
                  scope.validated = true
                  scope._validatedFile = file
                  scope._onValidateSuccess({data: data, csvFile: file})
                }
              })

          } else {
            scope.validating = false
            scope.validated = true
            scope._validatedFile = file
            scope._onValidateSuccess({data: data, csvFile: file})
          }
        }, 0)
      }

      scope.validating = true
      const config = {complete}
      Papa.parse(file, config)
    }

    // this is the logic connected to ngf-drop and ngf-select. See the above
    // documentation on validateLocally for more details.
    scope._processFile = scope.validateLocally ? scope.validateFile : scope.upload

    scope.reuploadOnclick = ($event) => {
      $event.stopPropagation()
      scope.validated = false
      scope.uploaded = false
    }

    // display logic for the 3 states of csv uploading: before, during, after
    scope.dispBeforeUpload = () => {
      return !scope.validated && !scope.uploaded && !scope.dispDuringUpload()
    }

    scope.dispAfterUpload = () => {
      return (scope.validated || scope.uploaded) && !scope.dispDuringUpload()
    }

    scope.dispDuringUpload = () => {
      return scope.uploading || scope.validating
    }

    scope.uploadButtonText = () => {
      if (this.$state.is('audience.enrich')) {
        return 'Score'
      }
      if (this.$state.is('audience.find.csv')) {
        return 'Find Companies'
      }
      return 'Upload'
    }

    scope.downloadSample = () => {
      this.modelService.downloadSampleCsv()
    }

    scope.methods = {
      upload: scope.upload
    }
  }
}

CSVUploader.$inject = ['Upload', 'alertService', '$state', '$timeout', 'modelService']

module.exports = {CSVUploader}
