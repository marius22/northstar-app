/**
 *
 *  Defines `number-picker` directive which can only be used as element.
 *
 *  It allows end-user to choose number, instead of typing
 *
 *  usage:
 *
 *       <number-picker value="input.num" min="1" max="10" step="1" change="onChange()"></number-picker>
 *
 */
import * as ng from 'angular';

class NumberPicker {
  constructor($compile) {
    this.template = `<div class="input-group number-picker">
          <span class="input-group-addon" type="down" ng-disabled="!canDown">
          <i class="btr bt-minus-circle" type="down"></i></span>
          <input type="text" class="number" ng-model="value" style="30px;"/>
          <span class="input-group-addon" type="up" ng-disabled="!canUp">
          <i class="btr bt-plus-circle" type="up"></i></span>
          </div>`
    this.restrict = 'E'
    this.scope = {
      value: '=',
      singular: '@',
      plural: '@',
      unitPosition: '@',
      min: '@',
      max: '@',
      step: '@',
      change: '&'
    }

    this.replace = true

    this.$compile = $compile
  }

  link($scope, element) {

    const defaults = {
      min: 0,
      max: 100,
      step: 1,
      timeout: 600
    };

    var toNumber = function (value) {
      return Number(value) || 0;
    };

    var checkNumber = function (value) {
      if (!ng.isNumber(value)) {
        throw new Error('value [' + value + '] is not a valid number');
      }
    };

    var getTarget = function (e) {
      if (e.touches && e.touches.length > 0) {
        return ng.element(e.touches[0].target);
      }
      return ng.element(e.target);
    };

    var getType = function (e) {
      return getTarget(e).attr('type');
    };

    var transform = function (opts) {
      for (var key in opts) {
        var value = opts[key];
        opts[key] = toNumber(value);
      }
    };

    var opts = Object.assign({}, defaults, {
      min: $scope.min,
      max: $scope.max,
      step: $scope.step
    });

    transform(opts);

    checkNumber(opts.min);
    checkNumber(opts.max);
    checkNumber(opts.step);


    if (opts.min > $scope.value) {
      $scope.value = opts.min;
    }

    $scope.$watch('value', function (newValue, oldValue) {
      $scope.canDown = newValue > opts.min;
      $scope.canUp = newValue < opts.max;
      $scope.unit = newValue === 1 ? $scope.singular : $scope.plural;

      if (newValue !== oldValue) {
        $scope.change();
      }
    });

    var changeNumber = function ($event) {
      var type = getType($event);
      $scope.value = Number($scope.value)
      if (type === 'up') {
        if ($scope.value >= opts.max) {
          return;
        }
        $scope.value += opts.step;
        if ($scope.value > opts.max) {
          $scope.value = opts.max;
        }
      } else if (type === 'down') {
        if ($scope.value <= opts.min) {
          return;
        }
        $scope.value -= opts.step;
        if ($scope.value < opts.min) {
          $scope.value = opts.min;
        }
      }
    };

    var isPressing;
    var timeoutPro;
    var intervalPro;
    var start;
    var end;
    var addon = element.find('span');

    addon.on('click', function (e) {
      changeNumber(e);
      $scope.$apply();
      e.stopPropagation();
    });

    addon.on('touchstart', function (e) {
      if (isPressing) {
        return;
      }
      isPressing = true;
      getTarget(e).addClass('active');
      start = new Date().getTime();
      timeoutPro = $timeout(function () {
        intervalPro = $interval(function () {
          changeNumber(e);
        }, 200);
      }, opts.timeout);
      e.preventDefault();
    });

    addon.on('touchend', function (e) {
      end = new Date().getTime();
      if (intervalPro) {
        $interval.cancel(intervalPro);
        intervalPro = undefined;
      }
      if (timeoutPro) {
        $timeout.cancel(timeoutPro);
        timeoutPro = undefined;
      }
      if ((end - start) < opts.timeout) {
        changeNumber(e);
        $scope.$apply();
      }
      getTarget(e).removeClass('active');
      e.stopPropagation();
      isPressing = false;
    });

    $scope.$on('$destroy', function () {
      addon.off('touchstart touchend click');
    });

  }
}
NumberPicker.$inject = ['$compile', '$timeout', '$interval']

module.exports = {NumberPicker}
