// Disable linting b/c this is an external library.
/* eslint-disable */
// https://github.com/lorenooliveira/ng-text-truncate
// Not available via npm, so we're including it in the repo
// import * as services from '../services'

import _ from 'lodash'

/*
  Truncation Directive

  Explanation of associated attributes:
    Required:
      data: The data that will potentially be truncated. Expected to be a string, number, or array.

      charsThreshold: Amount of characters before text is truncated. If data is an array, then it
                      will be displayed as the result of data.join(', '), and that string will
                      be truncated in respect to this threshold as well.

    Optional:
      modalTitle: Title of the modal that shows when user clicks "View More" in a tooltip.


    Other useful information:
      If data is an array with <= TOOLTIP_MAX_ITEMS items, then we just show each item in a tooltip when the truncated
      text is moused over. If it has > TOOLTIP_MAX_ITEMS items, then there will be a button that says "View More" which
      will open a modal window with all of the items.
                                                                                            */

(function () {
  'use strict'

  const TOOLTIP_MAX_ITEMS = 4
  angular.module('ngTextTruncate', [])

    .directive("ngTextTruncate", ["$compile", "showModalService",
      function ($compile, showModalService) {
        return {
          templateUrl: "templates/truncation.html",
          restrict: "A",
          scope: {
            data: "=ngTextTruncate",
            charsThreshold: "@ngTtCharsThreshold",
            modalTitle: '='
          },
          link: function ($scope, $element, $attrs) {
            const formatTextFromData = (data) => {
              if (Array.isArray(data)) {
                return data.join(", ")
              }
              switch (typeof data) {
                case "number":
                  return String(data)

                case "string":
                  return data
                default:
                  return ''
              }
            }

            $scope.render = () => {
              const formattedText = formatTextFromData($scope.data)
              const CHARS_THRESHOLD = parseInt($scope.charsThreshold)
              $scope.truncationApplies = formattedText.length > CHARS_THRESHOLD
              $scope.modalApplies = Array.isArray($scope.data) && $scope.data.length > TOOLTIP_MAX_ITEMS
              $scope.tooltipItems = Array.isArray($scope.data) ? $scope.data.slice(0, TOOLTIP_MAX_ITEMS) : [formattedText]

              if ($scope.modalApplies) {
                $scope.showDataViewModal = () => {
                  const modalParams = {
                    dataViewItems: $scope.data,
                    title: $scope.modalTitle
                  }
                  showModalService.dataViewerModal(modalParams)
                }
              }
              $scope.truncClass = $scope.truncationApplies ? 'truncated-text' : ''
              $scope.dispText = $scope.truncationApplies ? `${formattedText.substr(0, CHARS_THRESHOLD)}...` : formattedText
            }
            $scope.render()
            $scope.$watchCollection('[charsThreshold, data]', (newValue, oldValue) => {
              if (newValue !== oldValue) {
                $scope.render()
              }
            })
          }
        }
      }])

}())

module.exports = 'ngTextTruncate'
