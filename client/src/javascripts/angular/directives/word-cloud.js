import * as d3 from 'd3'
import _ from 'lodash'
import {GalaxyColorPalette} from 'galaxyModule/colors'
const cloud = require('d3-cloud')

class WordCloud {
  constructor($timeout, $window) {
    this.template = `<div class="directive-word-cloud"></div>`
    this.restrict = 'EA'
    this.scope = {
      // Raw data as an array of {"key": "Angularjs", "value": 10}
      tags: '='
    }
    this.$timeout = $timeout
    this.$window = $window
    this.replace = true
  }

  link(scope, element) {
    /* eslint-disable no-param-reassign */
    scope.$watch('tags', (newVal) => {
      if (newVal) {
        this.$timeout(() => {
          scope.draw()
        }, 0)
      }
    })

    scope.draw = () => {
      const colors = (new GalaxyColorPalette).getColorList()
      const fill = d3.scaleOrdinal(colors)
      const $dom = element[0].parentNode

      const w = $dom.clientWidth
      const h = $dom.clientHeight

      let fontSize

      const svg = d3.select(element[0]).append("svg")
        .attr("width", w)
        .attr("height", h)
      const vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")")

      const draw = function (data, bounds) {
        svg.attr("width", w).attr("height", h)

        const scale = bounds ? Math.min(
          w / Math.abs(bounds[1].x - w / 2),
          w / Math.abs(bounds[0].x - w / 2),
          h / Math.abs(bounds[1].y - h / 2),
          h / Math.abs(bounds[0].y - h / 2)) / 2 : 1

        const text = vis.selectAll("text")
          .data(data, function (d) {
            return d.text.toLowerCase()
          })
        text.enter().append("text")
          .attr("text-anchor", "middle")
          .attr("transform", function (d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"
          })
          .style("font-size", function (d) {
            return d.size + "px"
          })
          .style("opacity", 1e-6)
          .transition()
          .duration(1000)
          .style("opacity", 1)
          .style("font-family", function (d) {
            return 'Proxima Nova'
          })
          .style("fill", function (d) {
            return fill(d.text.toLowerCase())
          })
          .text(function (d) {
            return d.text
          })

        vis.transition().attr("transform", "translate(" + [w >> 1, h >> 1] + ")scale(" + scale + ")")
      }

      const layout = cloud()
        .timeInterval(Infinity)
        .size([w, h])
        .fontSize(function (d) {
          return fontSize(+d.value)/1.2
        })
        .rotate(function () {
          return 0
        })
        .text(function (d) {
          return d.key
        })
        .on("end", draw)

      const update = function () {
        const tags = _.cloneDeep(scope.tags)
        layout.spiral('archimedean')
        fontSize = d3.scaleSqrt().range([10, 100])
        if (tags.length) {
          fontSize.domain([+tags[tags.length - 1].value || 1, +tags[0].value])
        }
        layout.words(tags).start()
      }

      update()
    }
    /* eslint-enable no-param-reassign*/
  }
}

WordCloud.$inject = ['$timeout', '$window']

module.exports = {WordCloud}
