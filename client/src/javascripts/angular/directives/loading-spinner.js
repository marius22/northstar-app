import $ from 'npm-zepto'
import _ from 'lodash'

const BLUR_CLASS = 'blur-for-es-loading'

class LoadingSpinner {
  constructor() {
    this.restrict = 'E'
    this.scope = {
      // Set this to true to have the loader cover all of <body>, instead of being absolutely
      // positioned across the <loading-spinner>'s immediate parent
      appendToBody: '=',
      // Set this to true to use the new loading spinner with the ES logo and pulsing dots.
      // Otherwise, will be the old version with bt-spinner.
      esLogoVersion: '=',
      /// Index of loading-spinner parent's child that we want to blur.
      blurIndex: '=?'
    }
  }

  link(scope, element) {
    let directiveElem

    if (scope.esLogoVersion) {
      directiveElem = `<div class="es-loading-spinner-directive">
                        <div class="center-items-wrapper">
                            <div class="logo-outer-circle">
                              <div class="logo-inner-circle">
                                <img src="images/es-loader-logo.png"></img>
                              </div>
                            </div>
                            <div class="dots">
                              <div class="dot"></div>
                              <div class="dot"></div>
                              <div class="dot"></div>
                              <div class="dot"></div>
                            </div>
                          </div>
                      </div>`
    } else {
      directiveElem = `<div class="loading-spinner-directive">
                        <div class="spinner-wrapper">
                          <i class="btr bt-spinner bt-3x bt-spin"></i>
                        </div>
                      </div>`
    }

    element.html(directiveElem)

    if (scope.appendToBody) {
      // Move element from where it was defined to body
      $('body').append($(element[0]))
      // add classes for blurring background
      $('.main-view').addClass(BLUR_CLASS)
      $('.header-nav').addClass(BLUR_CLASS)
      scope.$on('$destroy', () => {
        // undo the stuff we did above
        $('.main-view').removeClass(BLUR_CLASS)
        $('.header-nav').removeClass(BLUR_CLASS)
        element.remove()
      })
    } else {
      const parentElem = element[0].parentElement
      // assume <loading-spinner> is always defined above the content that it's overlaying
      // in the HTML, so the content that it's overlaying (which we want to blur) is the second item in
      // parentElem.children
      const blurIndex = scope.blurIndex || 1
      const blurElem = parentElem.children[blurIndex]
      $(blurElem).addClass(BLUR_CLASS)
      scope.$on('$destroy', () => {
        $(blurElem).removeClass(BLUR_CLASS)
        if (scope.esLogoVersion) {
          element.remove()
        }
      })
    }

  }
}

LoadingSpinner.$inject = []

module.exports = {LoadingSpinner}
