import $ from 'npm-zepto'
import _ from 'lodash'
import angular from 'angular'

const statesMapping = {
  'Alabama': 'AL',
  'Alaska': 'AK',
  'Arizona': 'AZ',
  'Arkansas': 'AR',
  'California': 'CA',
  'Colorado': 'CO',
  'Connecticut': 'CT',
  'Delaware': 'DE',
  'District Of Columbia': 'DC',
  'Florida': 'FL',
  'Georgia': 'GA',
  'Hawaii': 'HI',
  'Idaho': 'ID',
  'Illinois': 'IL',
  'Indiana': 'IN',
  'Iowa': 'IA',
  'Kansas': 'KS',
  'Kentucky': 'KY',
  'Louisiana': 'LA',
  'Maine': 'ME',
  'Maryland': 'MD',
  'Massachusetts': 'MA',
  'Michigan': 'MI',
  'Minnesota': 'MN',
  'Mississippi': 'MS',
  'Missouri': 'MO',
  'Montana': 'MT',
  'Nebraska': 'NE',
  'Nevada': 'NV',
  'New Hampshire': 'NH',
  'New Jersey': 'NJ',
  'New Mexico': 'NM',
  'New York': 'NY',
  'North Carolina': 'NC',
  'North Dakota': 'ND',
  'Ohio': 'OH',
  'Oklahoma': 'OK',
  'Oregon': 'OR',
  'Pennsylvania': 'PA',
  'Rhode Island': 'RI',
  'South Carolina': 'SC',
  'South Dakota': 'SD',
  'Tennessee': 'TN',
  'Texas': 'TX',
  'Utah': 'UT',
  'Vermont': 'VT',
  'Virginia': 'VA',
  'Washington': 'WA',
  'West Virginia': 'WV',
  'Wisconsin': 'WI',
  'Wyoming': 'WY'
}

/*
  Expected statesData format as dictated by our distinct_counts API:
    [{"count": 23, "value": "California"}, {"count": 11, "value": "Texas"}, etc...]
*/

class UsaMap {
  constructor($filter, $animate, $compile) {
    this.templateUrl = 'templates/usa-map.html'
    this.restrict = 'EA'
    this.scope = {
      statesData: '=',
      title: '@'
    }
    this.replace = true
    this.$filter = $filter
    this.$animate = $animate
    this.$compile = $compile
  }

  link(scope, element) {
    /* eslint-disable no-param-reassign */
    scope.$watch('statesData', (newVal, oldVal) => {
      scope.placeDotsOnMap(newVal)
    })

    scope.placeDotsOnMap = () => {
      /* eslint-disable no-param-reassign */
      const $mapInfographic = $(element[0])
      const $overlayElem = $mapInfographic.find('.overlay')

      const statesInUSA = _.filter(scope.statesData, (state) => {
        // The API doesn't gives states outside of the US, so we need
        // to clean it up here.
        return _.includes(_.keys(statesMapping), state.value)
      })

      const totalCount = _.reduce(statesInUSA, (sum, state) => {
        return sum + state.count
      }, 0)

      scope.statesInUSA = statesInUSA

      for (const state of statesInUSA) {
        const stateName = state.value
        const stateAbbr = angular.lowercase(statesMapping[stateName])
        const stateCount = this.$filter('number')(state.count)
        let statePercentage
        // don't divided by a zero
        if (totalCount <= 0) {
          statePercentage = 0
        } else {
          statePercentage = 100 * (state.count / totalCount)
        }
        let toFixedCutoff = 0
        if (statePercentage < 10) {
          toFixedCutoff = 1
        }
        if (statePercentage < 1) {
          toFixedCutoff = 2
        }

        const dotElem = this.$compile(`<div class="${stateAbbr}">
                          <div class="tooltip-window">
                            <div class="name">State: ${stateName}</div>
                            <div class="count">Company Count: ${stateCount} (${statePercentage.toFixed(toFixedCutoff)}%)</div>
                          </div>
                        </div>`)(scope)
        this.$animate.enter(dotElem, $overlayElem)
      }
      scope.$applyAsync()
    }

    /* eslint-enable no-param-reassign*/
  }
}
UsaMap.$inject = ['$filter', '$animate', '$compile']

module.exports = {UsaMap}
