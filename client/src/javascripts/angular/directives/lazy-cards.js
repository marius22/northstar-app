import 'velocity-animate'

import * as Rx from 'rx'
import * as config from 'config'
import * as errors from 'errors'
import * as kws from '../services/keyword-service.js'

import $ from 'npm-zepto'
import _ from 'lodash'
import Promise from 'bluebird'
import request from 'superagent-bluebird-promise'
import scroll from 'scroll'

const horizontalMargin = 3

const fadeInOutConfig = {
  delay: 200,
  duration: 1000
}

// Vertical space between cards
const verticalMargin = 59

const audienceCompanyInfoStyling = {
  height: 620
}

// Amount to debounce on hovering/clicking the cards
const hoverClickDebounce = 100

// Amount to scale the card on hover
const scaleAmount = 1.2

// Height and width values here are used to calculate what cards are visible.
// We also need to set the css width/height separately
const cardHeight = 181
const cardWidth = 347

const containerTopPadding = 25

class LazyCards {
  constructor($timeout, observeOnScope, $http, keywordService, audienceService,
              salesforceService) {
    this.restrict = 'AE'
    this.observeOnScope = observeOnScope
    this.$http = $http
    this.keywordService = keywordService
    this.audienceService = audienceService
    this.salesforceService = salesforceService

    this.scope = {
      // The string selector to select the container of these cards
      containerSelector: '@',

      // The DOM Element which has a scrollbar that is letting us scroll up and down
      scrollableElementSelector: '@',

      moreCompanies: '=',
      initialData: '=?allCompanies'
    }

    this.templateUrl = (elem, attrs) => {
      return attrs.cardTemplateUrl
    }
    this.$timeout = $timeout
  }

  link(scope, element, attrs) {
    // Array of all rxjs subscriptions
    const subscriptions = []

    // Number of companies per page.
    scope.limit = 250
    scope.offset = 0

    // If true, it means we're currently loading companies and we shouldn't try to load more
    scope.loadingCompanies = false

    const fetchFirstCompanies = () => {
      if (scope.loadingCompanies) {
        return
      }

      scope.$emit('LazyCards:first_cards', {
        limit: scope.limit,
        offset: scope.offset
      })
      scope.offset += scope.limit
      scope.loadingCompanies = true
    }

    const fetchMoreCompanies = () => {
      if (scope.loadingCompanies) {
        return
      }

      scope.$emit('LazyCards:more_cards', {
        limit: scope.limit,
        offset: scope.offset
      })
      scope.offset += scope.limit
      scope.loadingCompanies = true
    }

    // Data that we're showing cards for.
    const initialData$ = new Rx.BehaviorSubject([])
    subscriptions.push(initialData$.subscribe(val => {
      this.$timeout(() => {
        scope.initialData = val
      }, 0)
    }))

    // Fetch companies starting from offset 0
    scope.$on('LazyCards:reset', (evt, params = {}) => {
      // Start at first page of cards
      scope.offset = 0
      scope.noFurtherPages = false
      scope.loadingCompanies = false
      // Wipe out data that we already have
      initialData$.onNext([])

      // Scroll to top of container
      $(scope.scrollableElementSelector).scrollTop(0)

      // Close card in case it's open.
      scope.closeNetflixCard()

      if (!params.withoutReload) {
        fetchFirstCompanies()
      }
    })

    scope.cardStyle = {}

    if (!scope.scrollableElementSelector) {
      scope.scrollableElementSelector = window
    }

    $(scope.scrollableElementSelector).css({
      overflow: 'scroll'
    })

    // Total height of a single card, including vertical margin.
    const cardHeight$ = Rx.Observable.just(window.parseFloat(cardHeight) + verticalMargin)
    // Total width of a single card, including horizontal margin.
    const cardMinWidth$ = Rx.Observable.just(window.parseFloat(cardWidth) + horizontalMargin)

    // The container (direct parent) holding the cards.
    const $container = $(scope.containerSelector)

    // Whether the cursor is currently on a card
    const cursorOnCard$ = new Rx.BehaviorSubject(false)
    subscriptions.push(cursorOnCard$.subscribe(cursorOnCard => {
      scope.cursorOnCard = cursorOnCard
    }))

    // The currently opened card index.
    const currentCardIdxMaybe$ = new Rx.BehaviorSubject(null)
    const currentCardIdx$ = currentCardIdxMaybe$
            .debounce(hoverClickDebounce)
            .filter((idx) => {
              // We can only open a netflix card if the cursor is currently on a card
              if (idx) {
                return scope.cursorOnCard
              }

              return true
            }).startWith(null).distinctUntilChanged()

    scope.netflixCardOpen = false

    // index 0: Index of the hovered card
    // index 1: Indexes of the card to the left of the currently hovered card
    // index 2: Indexes of the cards to the right of the currently hovered card
    const hoveredCardNeighborsMaybe$ = new Rx.BehaviorSubject([null, [], []])

    scope.pieChartOptions = {
      barColor: 'rgb(147, 214, 249)',
      trackColor: 'rgb(119, 119, 119)',
      size: 80,
      scaleColor: false,
      lineWidth: 6,
    }

    const moreCompanies$ = this
            .observeOnScope(scope, 'moreCompanies')
            .map((change) => {
              const moreCompanies = change.newValue
              return moreCompanies
            }).filter((moreCompanies) => {
              scope.loadingCompanies = false
              return moreCompanies
            })
    subscriptions.push(moreCompanies$.subscribe(companies => {
      scope.noFurtherPages = companies.length < scope.limit
      const domains = _.map(companies, 'domain')

      this.salesforceService.isIntegrated().then(res => {
        if (res.data.data) {
          this
            .audienceService
            .getInCrmStatus(domains)
            .then((statuses) => {
              Object.keys(statuses).forEach(domain => {
                const matchingCompany = companies.find(c => {
                  return c.domain === domain
                })

                if (matchingCompany) {
                  matchingCompany.inCrm = statuses[domain]
                }
              })
            })
            .catch(errorRes => {
              throw new errors.HttpError(errorRes)
            })
        }
      })
      initialData$.onNext(initialData$.getValue().concat(companies))

    }))

    /**
     * Draw lines at different y-coordinates for debugging.
     */
    let oldLine = null
    /* eslint-disable */
    const drawHorizLine = (top, color) => {
      if (oldLine) {
        oldLine.remove()
      }
      const newLine = $('<div class="line"></div>').css({
        border: `1px solid ${color}`,
      })
      newLine.appendTo($(scope.containerSelector))
      $('.line').offset({top})

      oldLine = newLine
    }
    /* eslint-enable */

    const scroll$ = Rx.Observable
            .fromEvent($(scope.scrollableElementSelector)[0], 'scroll')
            .map((evt) => {
              const elem = $(scope.scrollableElementSelector)[0]

              return elem.scrollTop
            })
            .startWith($(scope.scrollableElementSelector)[0].scrollTop)
            .debounce(hoverClickDebounce)

    /**
     * Once we're halfway down the middle of the final page, we load the next page.
     */
    const scrollCloseToMiddleOfBatch$ = (
      Rx.Observable
        .fromEvent($(scope.scrollableElementSelector)[0], 'scroll')
        .filter(evt => {
          // Say we have a batch size of 250. Once we reach the middle of the last batch of 250,
          // we emit from this stream.
          const numRowsInAdvance = scope.limit / 2
          const curTotal = scope.limit + scope.offset
          const bottomRange = _.range(curTotal - numRowsInAdvance * scope.numPerRow, curTotal)
          const intersection = _.intersection(scope.visibleIndices, bottomRange)
          const atMiddle = intersection.length > 0

          return atMiddle && !scope.noFurtherPages
        })
    )

    subscriptions.push(scrollCloseToMiddleOfBatch$
      .subscribe(scrollCloseToMiddle => {
        fetchMoreCompanies()
      }))


    // Index of the first row that's visible (i.e. the first row where some part of it is in the viewport.)
    const firstVisibleRow$ = Rx.Observable.combineLatest(
      scroll$, cardHeight$,
      (y, h) => {
        const vertSpaceForCards = y

        // For some reason, if we add margin-top: 25px, there's an invisible margin-bottom: 25px as well.
        // so we need to add another verticalmargin / 2 vertical space to account for it.
        const cardHeight = h
        // We get the number of rows that would fit in the viewport.
        // Subtract 1 to account for zero-indexing.
        // max with 0 in case the top of screen is above the cards.
        const firstVisibleRow = Math.max(0, Math.floor(vertSpaceForCards / cardHeight))

        return firstVisibleRow
      }
    ).distinctUntilChanged()

    /**
     * The window dimensions, updated on resize.
     */
    const windowDims$ = (
      Rx.Observable
        .fromEvent(window, 'resize')
        .map(() => [window.innerWidth, window.innerHeight])
        .startWith([window.innerWidth, window.innerHeight])).distinctUntilChanged()

    /**
     * The dimensions of the card container
     */
    const containerDimsOnResize$ = (
      windowDims$.map(() => {
        return [$container.width(), $container.height()]
      })
    ).distinctUntilChanged()

    const containerDimsInterval$ = (
      Rx.Observable
        .interval(500)
        .map(() => {
          return [$container.width(), $container.height()]
        })
    )
    const containerDims$ = Rx.Observable.merge(containerDimsOnResize$, containerDimsInterval$)

    const containerWidth$ = (
      windowDims$.map(() => {
        return $container.width()
      })
    ).debounce(100)

    /**
     * The number of cards per row.
     */
    const numPerRow$ = Rx.Observable.combineLatest(
      containerDims$, windowDims$, cardMinWidth$,
      ([containerWidth], [windowWidth], w) => {
        // If the window width is smaller than the container width, that's the width we use
        // when calculating num per row.
        const effectiveWidth = Math.min(containerWidth, windowWidth)
        const n = Math.max(1, Math.floor(effectiveWidth / w))

        // Enforce at least 2 cards per row since anything less looks stupid.
        const finalN = Math.max(n, 2)
        return finalN
      }
    ).distinctUntilChanged()
    subscriptions.push(numPerRow$.subscribe(n => {
      scope.numPerRow = n
    }))

    // Number of rows that fit in the viewport
    const rowCount$ = Rx.Observable.combineLatest(
      containerDims$, cardHeight$,
      ([ww, hh], h) => {
        return Math.ceil(hh / h)
      }
    ).distinctUntilChanged()

    /**
     * Indices of cards that are visible. Indices are relative to entire data set.
     */
    const visibleIndices$ = (
      Rx.Observable
        .combineLatest(
          firstVisibleRow$, rowCount$, numPerRow$, initialData$,
          (firstVisibleRow, rowCount, numPerRow, initialData) => {
            const lastRow = firstVisibleRow + rowCount

            // Number of rows to show above/below the viewport for smoothness reasons.
            const extraRows = 4

            const firstElementOfFirstVisibleRow = firstVisibleRow * numPerRow

            // Get the last row index. Plus 1 to get the row below the viewportMultiple by num per row
            const lastIndexOfLastVisibleRow = (lastRow + 1) * numPerRow - 1
            const firstIndex = Math.max(0, firstElementOfFirstVisibleRow - (extraRows * numPerRow))
            const lastIndex = Math.min(
              lastIndexOfLastVisibleRow + (extraRows * numPerRow), initialData.length - 1)
            const visibleIndices = _.range(firstIndex, lastIndex + 1)

            scope.visibleIndices = visibleIndices

            return visibleIndices
          }
        )
        .distinctUntilChanged()
        // Need to debounce, otherwise the visibleIndices changes too often and it looks weird.
        // If this value is too high, then loading cards feels too slow. If this value is too low, we
        // get weird artifacts when scrolling very rapidly.
        .debounce(100)
    )

    /**
     * Position the netflix card state in the right place. There's a single instance of this
     * DOM element and we absolutely position it to the correct location.
     */
    subscriptions.push(Rx.Observable
      .combineLatest(currentCardIdx$, numPerRow$, cardHeight$, cardMinWidth$,
                     // We need to listen to `visibleIndices$`, otherwise our container width
                     // is out of date
                     visibleIndices$,
                     // Update netflix card width/positioning if the window size changes.
                     windowDims$)
      .subscribe((params) => {
        const [curCardIdx, numPerRow, cardHeightPlusMargin, cardWidthPlusMargin] = params

        // Extra left spacing needed to horizontally center cards.
        const extraLeft = ($container.width() - numPerRow * cardWidthPlusMargin) / 2

        const row = Math.floor(curCardIdx / numPerRow)
        const top = `${(row + 1) * cardHeightPlusMargin - verticalMargin}px`

        const $companyInfo = $('.audience-company-info')
        $companyInfo.css({
          position: 'absolute',
          width: `${numPerRow * cardWidthPlusMargin - 2}px`,
          top,
          left: extraLeft,
        })
      }))

    /**
     * Update `visibleData`, which is the data that visible cards render.
     */
    subscriptions.push(Rx.Observable
      .combineLatest(visibleIndices$, initialData$)
      .subscribe(([indices, initialData]) => {
        this.$timeout(() => {
          scope.visibleData = indices.map((idx) => {
            return {
              // Index relative to entire data set
              index: idx,
              company: scope.initialData[idx]
            }
          })
        })
      }))

    const hoveredCardNeighbors$ = (hoveredCardNeighborsMaybe$.debounce(hoverClickDebounce))

    /**
     * Position each card at the appropriate location. We absolutely position each card
     * relative to the card container based on the input streams.
     */
    const styling$ = Rx.Observable.combineLatest(
      visibleIndices$,
      cardMinWidth$, cardHeight$, numPerRow$,
      currentCardIdx$,
      hoveredCardNeighbors$,
      // Every time width changes, we need to re-center the cards
      containerWidth$
    ).distinctUntilChanged()

    subscriptions.push(styling$.subscribe(
      (params) => {
        const [
          visibleIndices,
          widthPlusMargin,
          heightPlusMargin,
          numPerRow,
          currentCardIdx,
          hoveredCardNeighbors
        ] = params

        const [curHoveredIdx, leftNeighborsIdxs, rightNeighborsIdxs] = hoveredCardNeighbors

        // Extra left spacing needed to horizontally center cards.
        const extraLeft = ($container.width() - numPerRow * widthPlusMargin) / 2

        const lastRow = Math.floor(visibleIndices[visibleIndices.length - 1] / numPerRow)
        let spinnerTop = (lastRow + 1) * heightPlusMargin + 20
        if (scope.netflixCardOpen) {
          spinnerTop += audienceCompanyInfoStyling.height
        }

        const $spinner = $('.last-row-spinner')
        const spinnerRules = {
          display: 'block',
          position: 'absolute',
          top: spinnerTop,
          left: (
            $(scope.containerSelector).width() - $spinner.width()) / 2
        }

        // If we're at the last row of *all* the companies in the audience, hide the spinner. Otherwise, show it.
        const lastIndexVisible = visibleIndices[visibleIndices.length - 1] === scope.initialData.length - 1
        if ((lastIndexVisible && !scope.loadingCompanies) || scope.initialData.length === 0) {
          _.merge(spinnerRules, {display: 'none'})
        }
        $spinner.css(spinnerRules)

        this.$timeout(() => {
          visibleIndices.forEach((idx) => {
            const row = Math.floor(idx / numPerRow)
            const col = idx % numPerRow

            // Each card takes up width + horizontalMargin
            // amount of space. We set `left` based on how many cards
            // are to our left.
            let left = col * widthPlusMargin + extraLeft

            // Amount to move cards horizontally to make room for the scaled card
            const moveAmount = (cardWidth * (scaleAmount - 1)) / 2
            if (_.includes(leftNeighborsIdxs, idx)) {
              left -= moveAmount
            }
            if (_.includes(rightNeighborsIdxs, idx)) {
              left += moveAmount
            }

            // The first row of cards has no top margin, so everything is shifted up by that amount
            let top = row * heightPlusMargin - verticalMargin

            if (currentCardIdx) {
              // The first index of the first card of the row after the current card's row.
              // All these cards after the current row need to be pushed down to make room for the
              // Netflix state.
              const nextRowIdx = currentCardIdx - (currentCardIdx % numPerRow) + numPerRow
              if (idx >= nextRowIdx) {
                const netflixHeight = audienceCompanyInfoStyling.height
                top += netflixHeight
              }
            }

            const updateCardPosition = () => {
              const rules = {
                opacity: 1,
                position: 'absolute',
                width: `${widthPlusMargin - horizontalMargin}px`,
                height: `${heightPlusMargin - verticalMargin}px`,
                left: `${left}px`,
                top: `${top}px`,
                'margin-top': `${verticalMargin}px`,
              }

              // These rules need to be applied at the same time as the `position` rules above,
              // so we can't put them in css.
              $('.company-card').removeClass('hover-state')
              if (!_.isNull(curHoveredIdx)) {
                const $card = $(`#card-${curHoveredIdx}`)
                $card.addClass('hover-state')

                if (idx === curHoveredIdx) {
                  Object.assign(rules, {
                    transform: `scale(${scaleAmount})`,
                  })
                } else {
                  Object.assign(rules, {
                    transform: 'scale(1)',
                  })
                }
              } else {
                Object.assign(rules, {
                  transform: 'scale(1)'
                })
              }

              if (scope.netflixCardOpen) {
                if (idx === currentCardIdx ||
                    (Math.floor(idx / numPerRow) !== Math.floor(currentCardIdx / numPerRow))) {
                  Object.assign(rules, {
                    opacity: 1,
                  })
                } else {
                  Object.assign(rules, {
                    opacity: 0.3,
                  })
                }
              } else {
                Object.assign(rules, {
                  opacity: 1,
                })
              }

              // Save the style on scope so we can apply it as the card pops in
              scope.cardStyle[idx] = rules
            }

            // If we closed the netflix card, wait for it to close before sliding up.
            if (_.isNull(currentCardIdx)) {
              this.$timeout(() => {
                updateCardPosition()
              }, 200)
            } else {
              updateCardPosition()
            }
          })
        })
      }))

    /**
     * Stream of indexes of cards to load data for
     */
    const netflixLoadData$ = new Rx.BehaviorSubject(null)
    subscriptions.push(netflixLoadData$.filter(x => !_.isNull(x)).flatMapLatest(idx => {
      const domain = scope.initialData[idx].domain

      // Show netflix card immediately, show loading
      scope.isNetflixLoading = true

      // Only include similarDomain (which actually holds an array of all similar domains, despite not being
      // plural) in the /companies/profile response if we're in a "Find Audience" mode that involves domain
      // search inputs
      const expandFields = this.audienceService.isSimilarDomainsState() ? 'SimilarDomain' : ''

      // Load data needed for card, then set loading false
      const enrichP = request
              .get(`${config.apiBaseURL}companies/profile`)
              .query({domains: [domain],
                      expands: expandFields})
              .then((res) => {
                const data = res.body.data
                // Enrich this company with info needed for netflix card state
                const allKeys = Object.keys(data[domain])
                const excludedKeys = ['revenueRange', 'employeeSize']
                const includedKeys = _.difference(allKeys, excludedKeys)
                const enrichmentData = _.pick(data[domain], includedKeys)
                _.merge(scope.initialData[idx], enrichmentData)

                return idx
              })

      const params = {
        url: `/common/domains/to/ngrams?domains=${domain}&type=${kws.DEFAULT_GRAMS.join(',')}`,
        method: 'get'
      }
      const keywordsP = request
              .post(`${config.apiBaseURL}proxy`)
              .send(params)
              .then((res) => {
                const data = res.body.data

                const keywords = _.map(data[domain], 'grams')
                _.merge(scope.initialData[idx], {keywords})
              })

      const prom = (
        Promise
          .all([enrichP, keywordsP])
          .then(() => idx)
      )

      return Rx.Observable
        .fromPromise(prom)
        // When this stream is disposed, just cancel the promise and
        // its pending requests.
        .finally(() => {
          prom.cancel()
        })
    }).subscribe((idx) => {
      this.$timeout(() => {
        scope.isNetflixLoading = false
        scope.currentCompany = scope.initialData[idx]
      })
    }))

    /**
     * Handles everything related to opening a new netflix card.
     */
    subscriptions.push(currentCardIdx$.subscribe(idx => {
      hoveredCardNeighborsMaybe$.onNext([
        null,
        [],
        []
      ])

      $('.company-card').removeClass('open-state')
      const lastNetflixCardState = scope.netflixCardOpen
      // The code after this is for when we opened a netflix card.
      if (_.isNull(idx)) {
        scope.netflixCardOpen = false
        $('.down-triangle').hide()
        return
      }

      const $card = $(`#card-${idx}`)

      $card.addClass('open-state')
      // The row number of the current iteration in this loop
      const lastCardRow = Math.floor(scope.lastCardIdx / scope.numPerRow)
      const curCardRow = Math.floor(idx / scope.numPerRow)

      // True if there's already a card open and the new card we're trying to open is on different row.
      const openNetflixOtherRow = lastNetflixCardState && lastCardRow !== curCardRow

      const downTriangleLeft = window.parseInt($card.css('left')) + cardWidth / 2
      const downTriangleTop = window.parseInt($card.css('top')) + cardHeight + containerTopPadding + 34

      $('.down-triangle').css({
        display: 'block',
        left: `${downTriangleLeft}px`,
        top: `${downTriangleTop}px`
      })

      // Scroll screen to top of newly opened card.
      scroll.top($(scope.scrollableElementSelector)[0], $card[0].offsetTop)

      const $companyInfo = $('.audience-company-info')
      if (openNetflixOtherRow) {
        // Immediately fade out, then fade back in
        $companyInfo.velocity('fadeOut', {
          duration: 1,
        })
        $companyInfo.velocity('fadeIn', _.merge({}, fadeInOutConfig, {
          delay: 0,
          complete: () => {
            scroll.top($(scope.scrollableElementSelector)[0], $card[0].offsetTop)
          }
        }))
      } else if (!scope.netflixCardOpen) {
        // If there isn't a card already open, we need to fade the card in
        $companyInfo.velocity('fadeIn', _.merge({}, fadeInOutConfig, { delay: 250 }))
      }

      scope.lastCardIdx = scope.currentCardIdx
      scope.currentCardIdx = idx
      scope.netflixCardOpen = true

      netflixLoadData$.onNext(idx)
    }))

    // Public methods
    scope.onMouseEnter = (evt) => {
      cursorOnCard$.onNext(true)
      const dataIndex = $(evt.currentTarget).attr('data-index')
      const hoveredIdx = parseInt(dataIndex, 10)
      const hoveredRow = Math.floor(hoveredIdx / scope.numPerRow)

      // If the card isn't open, we scale on hover. Otherwise if the card is open and we mouse over a
      // different row, the card scales
      if (!scope.netflixCardOpen ||
          (scope.netflixCardOpen && Math.floor(scope.currentCardIdx / scope.numPerRow) !== hoveredRow)) {
        hoveredCardNeighborsMaybe$.onNext([
          hoveredIdx,
          _.range(hoveredRow * scope.numPerRow, hoveredIdx),
          _.range(hoveredIdx + 1, (hoveredRow + 1) * scope.numPerRow)
        ])
      } else {
        currentCardIdxMaybe$.onNext(hoveredIdx)
      }
    }

    scope.onMouseLeave = (evt) => {
      cursorOnCard$.onNext(false)
      hoveredCardNeighborsMaybe$.onNext([
        null,
        [],
        []
      ])
    }

    /**
     * On click, based on the card we're clicking, display the company info state.
     */
    scope.onClick = (evt) => {
      const dataIndex = $(evt.currentTarget).attr('data-index')
      const idx = parseInt(dataIndex, 10)

      $(`#card-${dataIndex}`).addClass('open-state')
      currentCardIdxMaybe$.onNext(idx)
    }

    scope.closeNetflixCard = () => {
      $('.audience-company-info').velocity('fadeOut', _.merge({}, fadeInOutConfig, {delay: false}))
      this.$timeout(() => {
        currentCardIdxMaybe$.onNext(null)
      })
    }

    scope.scrollToTop = () => {
      scroll.top($(scope.scrollableElementSelector)[0], 0)
    }

    scope.hasScrolled = () => {
      return $(scope.scrollableElementSelector).scrollTop() > 20
    }

    scope.$on('$destroy', () => {
      // Stop listening to streams once this directive is gone.
      subscriptions.forEach(sub => {
        sub.dispose()
      })
    })
  }
}
LazyCards.$inject = [
  '$timeout',
  'observeOnScope',
  '$http',
  'keywordService',
  'audienceService',
  'salesforceService'
]

module.exports = {LazyCards}
