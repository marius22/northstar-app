import $ from 'npm-zepto'
import _ from 'lodash'

// Maximum items we show at once in a multi-select box
const SLICE_SIZE = 100

// This is multiplied with SLICE_SIZE to determine
// how much we increment/decrement the selected/unselected offset
const OFFSET_DELTA = 0.5

const STEP_SIZE = SLICE_SIZE * OFFSET_DELTA

/*
  Multi-Select Directive

  Explanation of associated attributes:

    Required attributes:
      msDataSrc: data model for the multi-select boxes, expected to be of format:
                  msDataSrc = {
                    msDataType1: [
                      {
                        text: 'Data Object 1',
                        staged: false,
                        selected: false
                      },
                      ..etc.
                    ],

                    msDataType2: [..etc.],
                    ..etc.
                  }
      msDataType: one of the keys in msDataSrc
      inited: used in case you do extra processing on the msDataSrc to set some objects to
              be selected by default and don't want to see objects jumping from once box
              to the other when the page loads. If you don't care then pass in "true"

    Optional attributes:
      withFilter: whether or not we want to have a filter in the "Unselected" box for
                  potentially large amounts of data
      autoThreshold: if autoThreshold is set true, truncThreshold will be decided by ms-choices' width
                                                                                      */

class MultiSelect {
  constructor($filter, $rootScope, $timeout, $window) {
    this.templateUrl = 'templates/multi.select.html'
    this.restrict = 'A'
    this.scope = {
      disabled: '=',
      msDataSrc: '=',
      msDataType: '=',
      leftFilter: '@',
      rightFilter: '@',
      inited: '=',
      leftTitle: '@',
      rightTitle: '@',
      truncThreshold: '@',
      reverse: '=',
      autoThreshold: '='
    }
    this.filter = $filter
    this.$rs = $rootScope
    this.$timeout = $timeout
    this.$window = $window
  }

  link(scope, element) {
    /* eslint-disable no-param-reassign */

    const msElem = $(element[0])
    const msChoices = msElem.find('.ms-choices')
    const unselected = msChoices[0]
    const selected = msChoices[1]

    scope.sliceSizeUnslct = scope.sliceSizeSlct = SLICE_SIZE

    if (scope.autoThreshold) {
      const cacheElementSize = (_scope, _element) => {
        _scope.cachedElementWidth = _element.width()
      }
      const resetTruncThreshold = () => {
        // every letter is about 9.45px width
        const msChoicesWidth = msChoices.width()
        const newThreshold = Math.ceil((msChoicesWidth - 10) / 9.5)
        scope.truncThreshold = newThreshold > 0 ? newThreshold : scope.truncThreshold
      }
      const onWindowResize = () => {
        const isSizeChanged = scope.cachedElementWidth !== msElem.width()
        if (isSizeChanged) {
          this.$timeout(resetTruncThreshold, 0)
        }
      }
      cacheElementSize(scope, msElem)
      resetTruncThreshold()
      this.$window.addEventListener('resize', onWindowResize)
      scope.$on('$destroy', () => {
        this.$window.removeEventListener('resize', onWindowResize)
      })
    }

    unselected.onscroll = _.throttle(() => {
      if (scope.fltrUnselected) {
        const numUnselected = scope.fltrUnselected.length
        if (numUnselected > SLICE_SIZE) {
          const scrollH = unselected.scrollHeight
          const scrollT = $(unselected).scrollTop()
          const scrollLowEnough = scrollT > (scrollH * (4 / 5))

          if (scrollLowEnough) {
            this.$rs.$evalAsync(() => {
              scope.sliceSizeUnslct += STEP_SIZE
            })
          }
        }
      }
    }, 100)

    selected.onscroll = _.throttle(() => {
      if (scope.fltrSelected) {
        const numSelected = scope.fltrSelected.length
        if (numSelected > SLICE_SIZE) {
          const scrollH = selected.scrollHeight
          const scrollT = $(selected).scrollTop()
          const scrollLowEnough = scrollT > (scrollH * (4 / 5))

          if (scrollLowEnough) {
            this.$rs.$evalAsync(() => {
              scope.sliceSizeSlct += STEP_SIZE
            })
          }
        }
      }
    }, 100)

    scope.filterSearchQuery = {
      left: {text: ''},
      right: {text: ''}
    }

    scope.resetSliceSizes = () => {
      scope.sliceSizeUnslct = scope.sliceSizeSlct = SLICE_SIZE
    }

    scope.$watch('filterSearchQuery.left.text', (newVal) => {
      scope.sliceSizeUnslct = SLICE_SIZE
      scope.resetGroupingState()
    })

    scope.$watch('filterSearchQuery.right.text', (newVal) => {
      scope.sliceSizeSlct = SLICE_SIZE
      scope.resetGroupingState()
    })

    scope.$watch('msDataType', () => {
      scope.filterSearchQuery = {
        left: {text: ''},
        right: {text: ''}
      }
      scope.resetSliceSizes()
      scope.resetGroupingState()
    })

    scope.currentData = () => {
      return scope.msDataSrc[scope.msDataType]
    }

    scope.totalUnselected = () => {
      if (scope.fltrUnselected) {
        return scope.fltrUnselected.length
      }
      return 0
    }

    scope.totalSelected = () => {
      if (scope.fltrSelected) {
        return scope.fltrSelected.length
      }
      return 0
    }

    scope.selectAll = () => {
      const filteredData = this.filter('filter')(scope.currentData(), scope.filterSearchQuery.left)
      for (const datum of filteredData) {
        datum.staged = false
        datum.selected = true
      }
      scope.$emit('selectionChanged')
      scope.resetSliceSizes()
      scope.filterSearchQuery.left = {text: ''}
    }

    scope.deselectAll = () => {
      const filteredData = this.filter('filter')(scope.currentData(), scope.filterSearchQuery.right)
      for (const datum of filteredData) {
        datum.staged = false
        datum.selected = false
      }
      scope.$emit('selectionChanged')
      scope.resetSliceSizes()
      scope.filterSearchQuery.right = {text: ''}
    }

    scope.select = () => {
      let changeFlag = false
      for (const datum of scope.currentData()) {
        if (datum.staged) {
          datum.staged = false
          datum.selected = true
          changeFlag = true
        }
      }
      if (changeFlag) {
        scope.$emit('selectionChanged')
      }
    }

    scope.deselect = () => {
      let changeFlag = false
      for (const datum of scope.currentData()) {
        if (datum.staged) {
          datum.staged = false
          datum.selected = false
          changeFlag = true
        }
      }
      if (changeFlag) {
        scope.$emit('selectionChanged')
      }
    }

    scope.getTruncThreshold = () => {
      return scope.truncThreshold ? scope.truncThreshold : 22
    }

    scope.isDisabled = () => {
      return scope.disabled
    }

    scope.toggle = (ev, datum) => {
      if (!ev.shiftKey) {
        datum.selected = !datum.selected
        datum.staged = false
        scope.$emit('selectionChanged')
      } else {
        datum.selected ? scope.deselect() : scope.select()
      }
    }

    // If shift is held down while a choice X is selected,
    // we want to select all choices between choice X and
    // the most recently selected choice
    scope.handleGroupStaging = (ev, datum) => {
      if (!ev.shiftKey) {
        scope.mostRecentNoShiftDatum = datum
      } else {
        scope.mostRecentShiftDatum = datum
      }

      if (scope.mostRecentShiftDatum && ev.shiftKey) {
        // the "terminal" datum is the item that we're shift-clicking.
        // Every item from "anchor" to the "terminal"
        // gets staged upon shift-click. If the shift-click happens
        // without a valid user-selected anchor, then we use a fallback anchor,
        // which is the first item in the multi-select box that the "terminal"
        // datum lives in. This mirrors the shift-click behavior in many applications
        // in the case where you shift-click without previously selecting something.
        const terminal = scope.mostRecentShiftDatum
        let anchor = scope.mostRecentNoShiftDatum

        const filteredData = terminal.selected ? scope.fltrSelected : scope.fltrUnselected

        const useFallbackAnchor = !anchor || !anchor.staged || anchor.selected !== terminal.selected

        if (useFallbackAnchor) {
          anchor = _.find(filteredData, (datum) => {
            return datum.selected === terminal.selected
          })
          // If we shift-click something without a valid user-selected anchor,
          // we want the shift-clicked item to act as the anchor in subsequent
          // shift-clicks (you can try out this scenario in OSX Finder)
          scope.mostRecentNoShiftDatum = terminal
        }

        const anchorIdx = _.indexOf(filteredData, anchor)
        const terminalIdx = _.indexOf(filteredData, terminal)
        const startIdx = anchorIdx < terminalIdx ? anchorIdx : terminalIdx
        const endIdx = anchorIdx < terminalIdx ? terminalIdx + 1 : anchorIdx + 1

        const targetData = _(filteredData)
          .slice(startIdx, endIdx)
          .forEach(datum => {
            datum.staged = true
          })

        // unstage everything else.
        const everythingElse = _.difference(scope.currentData(), targetData)
        _.forEach(everythingElse, (datum) => {
          datum.staged = false
        })
      }
    }

    // Mainly want to call this when the visible set of data changes,
    // either by changing a filterSearchQuery, or by changing msDataType.
    scope.resetGroupingState = () => {
      scope.mostRecentShiftDatum = undefined
      scope.mostRecentNoShiftDatum = undefined
    }

    /* eslint-enable no-param-reassign*/
  }
}
MultiSelect.$inject = ['$filter', '$rootScope', '$timeout', '$window']

module.exports = {MultiSelect}
