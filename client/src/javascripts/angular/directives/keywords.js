import $ from 'npm-zepto'
import _ from 'lodash'
import angular from 'angular'
import URI from 'urijs'


// How many keywords we see initially in the UI,
// and how many get incrementally added by the user
const KW_GROUP_SIZE = 5

class Keywords {
  constructor(keywordService, $state, $compile, $timeout) {
    this.templateUrl = 'templates/keywords.html'
    this.restrict = 'E'

    /**
     * kwModel - Model for two-way binding that will hold your keyword objects.
     *           IMPORTANT: It needs to be formatted like this for two-way
     *           binding to work properly: kwModel = {items: []}. In other words,
     *           the actual keywords model (items, in this case) needs to be within
     *           some wrapper object.
     *           ALSO IMPORTANT: Note that kwModel.items will actually contain domains
     *                           instead of keywords for searchType === 'companiesByDomains'.
     *                           Simpler to do this instead of renaming everything for this
     *                           unanticipated use-case.
     *
     * searchType - Possible values and behavior:
     *                'keywordsByDomains': Select domains by prefix match. On searching, finds keywords
     *                                     from the selected domains.
     *
     *                'companiesByDomains': Select domains by prefix match. On searching, uses the
     *                                      onSearch method passed by AudienceCreateController to
     *                                      find similar companies from selected domains.
     *
     *                'companiesByKeywords': Select keywords by prefix match. On searching, uses
     *                                       the onSearch method passed by AudienceCreateController to
     *                                       find companies from selected keywords.
     *
     *
     * disableTagArea - ng-if's the tag area out of existence if true
     *
     * disableInputArea - ng-if's the input area out of existence if true
     *
     * disableTagEditing - ng-if's all 'x' tag deletion buttons out of existene of true
     *
     * placeholderText - Placeholder text for search input
     *
     * tableCrudItems - Array of strings representing desired table crud items.
     *                  For example, tableCrudItems = ['clearAll', 'galaxy']
     *                  Valid strings:
     *
     *                    'clearAll' - button to set kwModel.items = [], effectively
     *                                 clearing all the keywords
     *                    'galaxy' - not implemented yet. This is for 'Add From Galaxy'
     *                               in our mockups.
     *
     * onSearch - method called when the search button on the right side of the input is clicked.
     *            IMPORTANT: If you are searching by 'domains', the method will always be getTagsForDomainBatch().
     *                       I didn't want to pass a method from some controller in this case, because
     *                       selectionModel (which is used in getTagsForDomainBatch)
     *                       should be a concept confined to this directive.
     *
     */

    this.scope = {
      kwModel: '=',
      searchType: '@',
      disableTagArea: '=',
      disableInputArea: '=',
      disableTagEditing: '=',
      placeholderText: '=',
      tagAreaHeader: '=',
      tableCrudItems: '=',
      onSearch: '&',
      accentFlag: '=?',
      disableFlag: '=?'
    }
    this.replace = true
    this.keywordService = keywordService
    this.$state = $state
    this.$compile = $compile
    this.$timeout = $timeout
  }

  link(scope, elem) {
    scope.loadingMatches = false

    const keywordsByDomains = () => {
      return scope.searchType === 'keywordsByDomains'
    }

    const companiesByDomains = () => {
      return scope.searchType === 'companiesByDomains'
    }

    const companiesByKeywords = () => {
      return scope.searchType === 'companiesByKeywords'
    }

    const shouldPrefixMatchOnDomains = () => {
      return companiesByDomains() || keywordsByDomains()
    }

    scope.getRelevantPartOfURLSearchString = () => {
      let searchString = scope.$select.search
      let uri = new URI(searchString)
      let domain = uri.domain()
      // urijs will return undefined if the protocol is not present
      // in the search string. In this case, we manually add a protocol to it just
      // so urijs can then identify the domain.
      if (!domain) {
        const forcedProtocolSearchString = `http://${searchString}`
        uri = new URI(forcedProtocolSearchString)
        domain = uri.domain()
      }
      // Here, we need to check if the domain that urijs has identified
      // is actually legitimate. If it's given "http://www.evers", it will
      // think that "www.evers" is the domain, so we want to take just "evers"
      // in that case.
      domain = angular.lowercase(domain)
      const components = domain.split('.')
      const wwwCausingFakeDomain = components[0] === 'www'
      if (wwwCausingFakeDomain) {
        const withoutWWW = components[1]
        return withoutWWW
      } else {
        const nothingAfterDot = _.isEmpty(components[1])
        return nothingAfterDot ? components[0] : domain
      }
    }

    const getTagsFromPrefix = (tagPrefix) => {
      scope.loadingMatches = true
      tagPrefix = angular.lowercase(tagPrefix)

      const getLemmasP = this.keywordService.getLemmasFromKeywords(scope.selectedKwNames())

      this.keywordService.getTagsFromPrefix(tagPrefix).then(
        (response) => {
          const respKeywords = response.data.data.ngram
          getLemmasP
            .then((selectedKwsAndLemmas) => {
              // only show items in the dropdown that don't already exist in kwModel.items or their lemmas
              const relevantRespKeywords = _.filter(respKeywords, (kwName) => {
                return !_.includes(selectedKwsAndLemmas, kwName)
              })

              const exactMatch = _.find(relevantRespKeywords, (kwName) => {
                return kwName === tagPrefix
              })
              // If we find a keyword that exactly matches the user's input, we bring that to the front
              // of our matches array so it's the first item in the matches dropdown.
              // This allows the user to add an exact match by just pressing enter.
              if (exactMatch) {
                scope.matches = _.concat([exactMatch], _.without(relevantRespKeywords, exactMatch))
              } else {
                scope.matches = relevantRespKeywords
              }
              // Set scope.$select.items = scope.matches here to fix an edge case, described as follows:
              //  - User has input X in the search bar, with matches dropdown showing.
              //  - User clicks outside the dropdown. ui-select will clear the search input
              //    and the dropdown disappears from this click action.
              //  - If the user then types the same input X that used to be in the search bar,
              //    the matches dropdown will not display properly.
              scope.$select.items = scope.matches
              scope.showNoResults = _.isEmpty(scope.matches)
              scope.loadingMatches = false
            })
        },
        (err) => {
          if (!err.wasCanceled) {
            scope.loadingMatches = false
          }
        }
      )
    }

    const getDomainsFromPrefix = (domainPrefix) => {
      scope.loadingMatches = true
      domainPrefix = scope.getRelevantPartOfURLSearchString()
      this.keywordService.getDomainsFromPrefix(domainPrefix).then(
        (response) => {
          const respDomains = response.data.data.domains
          scope.matches = _.map(respDomains, (domain) => {
            return domain
          })
          scope.$select.items = scope.matches
          scope.showNoResults = _.isEmpty(scope.matches)
          scope.loadingMatches = false
        },
        (err) => {
          if (!err.wasCanceled) {
            scope.loadingMatches = false
          }
        }
      )
    }

    const getTagsFromDomains = () => {
      if (_.isEmpty(scope.selectionModel.items)) {
        return
      }
      const domainArr = _.map(scope.selectionModel.items, (item) => {
        return item
      })
      const onlyOneDomain = domainArr.length === 1
      scope.loadingTagsFromDomains = true
      this.keywordService.getTagsFromDomains(domainArr).then(
        (response) => {
          // response.data.data is a key-value pair if our getTagsFromDomains endpoint
          // is only given one domain, so we account for that case here.
          const mappableResponse = onlyOneDomain ? response.data.data[domainArr[0]] : response.data.data

          const multiDomainTags = _.map(mappableResponse, (item) => {
            return {text: item.grams, selected: false}
          })
          // keep our selected keywords and overwrite the rest with the new results
          const currentlySelectedItems = _.filter(scope.kwModel.items, (kwObj) => {
            return kwObj.selected
          })
          scope.displayLimit = currentlySelectedItems.length + KW_GROUP_SIZE
          scope.kwModel.items = _.unionBy(currentlySelectedItems, multiDomainTags, 'text')
          scope.loadingTagsFromDomains = false
        },
        (err) => {
          if (!err.wasCanceled) {
            scope.loadingTagsFromDomains = false
          }
        }
      )
    }

    if (companiesByKeywords()) {
      scope.$watch('kwModel.items', () => {
        scope.bringRecentIntoView()
      })
    }

    scope.selectedKwNames = () => {
      return scope.kwModel.items
    }

    scope.numSelected = () => {
      return keywordsByDomains() ? scope.selectionModel.items.length : scope.kwModel.items.length
    }

    scope.shouldShowNoResults = () => {
      return scope.showNoResults && scope.$select.search
    }

    scope.initEnterKeyListener = ($select) => {
      const searchInputElem = $(elem[0]).find('.ui-select-search')[0]
      let prevNumSelected = scope.numSelected()
      searchInputElem.addEventListener('keyup', (e) => {
        const keyCode = e.which || e.keyCode
        let currNumSelected = scope.numSelected()
        const enterPressedToAddMatch = prevNumSelected !== currNumSelected
        prevNumSelected = currNumSelected
        /* only fire our search if:
            1. Enter key pressed
            2. If our numSelected has NOT changed between before enter was
               pressed and after enter was pressed. If it has changed,
               then the purpose of the enter press was to add a match to the
               selected tags, in which case the user did not intend to also
               trigger search.
            3. There are tag selections to search on
        */
        if (keyCode === 13 && !enterPressedToAddMatch && !scope.noSelections()) {
          scope._onSearch()
        }
      })

      // no results logic
      scope.$select = $select
      scope.$watch('$select.search', () => {
        scope.showNoResults = false
      })
      const noResultsElem = $(elem[0]).find('.ui-select-no-choice')[0]
      const keywordOrUrl = (keywordsByDomains() || companiesByDomains()) ? 'URL' : 'keyword'
      const innerNoResults = this.$compile(
        `<div class="no-results-content">
          <i class="btr bt-ban"></i>
          We couldn't find anything for "{{$select.search}}".
          Search for another ${keywordOrUrl}.
         </div>`)(scope)[0]
      noResultsElem.appendChild(innerNoResults)
    }

    scope._onSearch = keywordsByDomains() ? getTagsFromDomains : scope.onSearch

    // Model of the dropdown list when you do prefix-match searches
    scope.matches = []

    // Model of items you selected from the prefix-match dropdown list.
    // If searchType = 'companiesByKeywords' or 'companiesByDomains',
    // then this is the same as scope.kwModel. For the keywordsByDomains case,
    // this is a seperate model because we need to hold state for two things:
    //   1. Selected domains by which we search for keywords (selectionModel used for this)
    //   2. Keywords that we found from searching on our selected domains (kwModel used for this)
    scope.selectionModel = keywordsByDomains() ? {items: []} : scope.kwModel

    // onRefresh controls the prefix match logic in the search input.
    scope.onRefresh = shouldPrefixMatchOnDomains() ? getDomainsFromPrefix : getTagsFromPrefix

    // Force selected tags container to always be scrolled to the right, so we see the most
    // recent selections on the right side, and always keep the most recent selctions
    // in view.
    scope.bringRecentIntoView = () => {
      scope.matches = []
      const numSelections = scope.selectionModel.items.length
      if (numSelections > 0) {
        const directiveElem = $(elem[0])
        const matchElemsWrapper = $(directiveElem.find('.ui-select-match'))
        const matchElems = matchElemsWrapper.children()

        // pre-emptively remove event listener, which may have been added
        // in a previous call to bringRecentIntoView()
        matchElemsWrapper.off('DOMNodeInserted', scope.bringRecentIntoView)

        // If the user saves the keywords found from URLs in the
        // find-keywords focus slide, scope.selectionModel will be updated
        // with the new keywords, but keyword tags still need time to
        // be inserted into the DOM via ng-repeat. We only want to
        // scroll all the way to the right once we know that all the elements
        // have been inserted into the DOM.
        if (matchElems.length === numSelections) {
          // Without debounce, we don't properly scroll all the way to the right.
          // Don't know why.
          _.debounce(() => {
            matchElemsWrapper.scrollLeft(Number.MAX_VALUE)
          }, 10)()
        } else {
          matchElemsWrapper.on('DOMNodeInserted', scope.bringRecentIntoView)
        }
      }
    }

    scope.isEmpty = () => {
      return _.isEmpty(scope.kwModel.items)
    }

    scope.noSelections = () => {
      return _.isEmpty(scope.selectionModel.items)
    }

    scope.removeKwTag = (tagName) => {
      _.remove(scope.kwModel.items, (tag) => {
        return tag === tagName
      })
    }

    scope.removeSelectionTag = (tagName) => {
      _.remove(scope.selectionModel.items, (tag) => {
        return tag === tagName
      })
    }

    scope.clearAllKwTags = () => {
      scope.kwModel = {
        items: []
      }
      scope.displayLimit = KW_GROUP_SIZE
    }

    scope.clearAllSelectionTags = () => {
      scope.selectionModel = {
        items: []
      }
    }

    scope.addFromGalaxy = () => {
      // to be implemented later
    }

    scope.displayLimit = KW_GROUP_SIZE

    scope.showMoreMsg = () => {
      const kwsActuallyRemaining = scope.kwModel.items.length - scope.displayLimit
      const numberToShow = Math.min(kwsActuallyRemaining, KW_GROUP_SIZE)
      const appropriateGrammar = numberToShow === 1 ? 'keyword' : 'keywords'
      return `Show ${numberToShow} more ${appropriateGrammar}`
    }

    scope.showMoreOnclick = () => {
      scope.displayLimit += KW_GROUP_SIZE
      const tagsAreaElem = $(elem[0]).find('.tags-area')[0]
      this.$timeout(() => {
        tagsAreaElem.scrollTop = Number.MAX_VALUE
      }, 100)
    }

    scope.allKwsAreShown = () => {
      return scope.displayLimit >= scope.kwModel.items.length
    }

    scope.emptyText = () => {
      const viewingAudienceSummary = this.$state.is('audience.console')
      if (viewingAudienceSummary) {
        const keywordsOrUrls = companiesByDomains() ? 'URLs' : 'keywords'
        return `This audience was created without ${keywordsOrUrls}.`
      } else if (companiesByDomains()) {
        return 'URLs will populate here.'
      } else {
        return 'Keywords will populate here.'
      }
    }

    scope.showGuideImage = () => {
      return keywordsByDomains()
    }

    scope.filterRelevantSearchString = () => {
      const compareAgainstRelevantString = (match) => {
        const relevantSearchString = scope.getRelevantPartOfURLSearchString()
        return _.includes(match, angular.lowercase(relevantSearchString))
      }
      return compareAgainstRelevantString
    }
  }

}

Keywords.$inject = ['keywordService', '$state', '$compile', '$timeout']

module.exports = {Keywords}
