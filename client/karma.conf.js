// The single entry point for our tests. We need this file to
// import all our tests and all our source for instrumentation and coverage
// to work.
var testEntryPoint = 'src/javascripts/__tests__/index.js'
var webpackConfig = require('./gulpfile.js/lib/webpack-multi-config')

/**
 * Karma is the actual test runner. It's a framework-agnostic way
 * of running javascript tests.
 *
 * Karma config docs: http://bit.ly/1TzwehM
 */
var karmaConfig = {
  // Will run tests in Chrome by default.
  // browsers can be Chrome/PhantomJS
  browsers: [
    // Only run on PhantomJS for speed.
    'PhantomJS',
  ],

  // Mocha is the test framework, and sinon/chai are test libraries
  // that provide assertions, mocks, etc.
  frameworks: ['mocha', 'sinon-chai'],

  // List of files to load in the browser.
  files: [
    testEntryPoint
  ],

  // For code coverage to work, we need a single file as an entry
  // point for our tests. We create that single entry point and
  // use Webpack to bundle it properly.
  preprocessors: {
    'src/javascripts/__tests__/index.js': 'webpack'
  },

  // These are the Webpack settings Karma uses when running the tests.
  webpack: webpackConfig('test'),
  reporters: ['progress', 'coverage', 'verbose'],
  coverageReporter: {
    reporters: [
      {type: 'cobertura', dir: 'coverage/'},
      {type: 'html', dir: 'coverage/'},
    ]
  },
  phantomjsLauncher: {
    // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
    exitOnResourceError: true
  }
}

module.exports = function (config) {
  config.set(karmaConfig)
}
