var config = require('../config')
var debug = require('gulp-debug')
if (!config.tasks.html) {
  return
}

var browserSync = require('browser-sync')
var data = require('gulp-data')
var gulp = require('gulp')
var gulpif = require('gulp-if')
var handleErrors = require('../lib/handleErrors')
var htmlmin = require('gulp-htmlmin')
var path = require('path')
var render = require('gulp-nunjucks-render')
var fs = require('fs')
var R = require('ramda')
var merge = require('merge-stream')

var exclude = path.normalize('!**/{' + config.tasks.html.excludeFolders.join(',') + '}/**')

// Files that we want to get bundled with Nunjucks and copied over to the public folder directly.
var nunjucksHtmlFiles = [
  'body', 'index', 'mouseflow'
].join('|')

// In `paths`, we copy over all html except body and index, which are special. index.html
// is processed by nunjucks so that we can serve different html based on environment.
// Since body.html contains angular code, we're separating and using Nunjucks {% raw %} tag on it.
var paths = {
  src: [
    path.join(config.root.src, config.tasks.html.src, '**/*.html'),
    '!' + path.join(config.root.src, config.tasks.html.src, '+(' + nunjucksHtmlFiles + ').html'),
    exclude
  ],
  dest: path.join(config.root.dest, config.tasks.html.dest)
}

var indexHtmlPaths = {
  src: [path.join(config.root.src, config.tasks.html.src, 'index.html')],
  dest: path.join(config.root.dest, config.tasks.html.dest)
}

var getData = function (file) {
  var pathObj = path.parse(file.path)

  // For each page asdf.html, we'll grab the src/html/data/asdf.json file
  // TODO: This doesn't actually work; only the global data is
  // accessible by the page. We might need to pass to the data:
  // parameter somehow.
  var pageSpecificDataPath = path.resolve(
    config.root.src,
    config.tasks.html.src,
    'data/',
    [pathObj.name, '.json'].join('')
  )
  var globalDataPath = path.resolve(
    config.root.src,
    config.tasks.html.src,
    config.tasks.html.dataFile)

  // Include json objects if they exist.
  var existingDataPaths = R.filter(function (dataPath) {
    try {
      fs.statSync(dataPath)
    } catch (err) {
      if (err.code === 'ENOENT') {
        return false
      }
    }
    return true
  }, [globalDataPath, pageSpecificDataPath])

  var parseJSON = function (filePath) {
    return JSON.parse(fs.readFileSync(filePath, 'utf8'))
  }
  var getJSONFromPaths = R.compose(
    R.mergeAll, R.flatten, R.map(parseJSON))

  return getJSONFromPaths(existingDataPaths)
}

var htmlTask = function () {

  var nunjucksOptions = {
    path: [path.join(config.root.src, config.tasks.html.src)],
    manageEnv: function (environment) {
      environment.addGlobal('ENV', process.env.ENV)
    }
  }

  var mainHtmlStream = gulp
    .src(paths.src)
    .pipe(data(getData))
    .on('error', handleErrors)
    .pipe(gulpif(process.env.ENV === 'prod', htmlmin(config.tasks.html.htmlmin)))
    .pipe(gulp.dest(paths.dest))
    // Leaving this debug output here as a temporary 'fix' for an issue where
    // gulp.dest() returns before finishing. The html files are handled
    // in alphabetical folder order and this issue causes most of the files
    // in /templates and all the files in /user to not update properly in /public.
    // More reading: https://github.com/gulpjs/gulp/issues/1254
    .pipe(gulpif(process.env.ENV !== 'prod', debug({title: 'You are awesome'})))
    .pipe(browserSync.stream())
    .pipe(gulpif(process.env.ENV !== 'prod', debug({title: 'All your dreams will come true'})))

  var indexHtmlStream = gulp
    .src(indexHtmlPaths.src)
    .pipe(render(nunjucksOptions))
    .pipe(gulp.dest(indexHtmlPaths.dest))

  return merge(mainHtmlStream, indexHtmlStream)
}

gulp.task('html', htmlTask)
module.exports = htmlTask
