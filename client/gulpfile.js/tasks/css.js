var config = require('../config')
if (!config.tasks.css) {
  return
}


var gulp = require('gulp')
var browserSync = require('browser-sync')
var sass = require('gulp-sass')
var handleErrors = require('../lib/handleErrors')
var autoprefixer = require('gulp-autoprefixer')
var path = require('path')
var merge = require('merge-stream')
var concat = require('gulp-concat')

var cssPaths = {
  src: path.join(config.root.src,
                   config.tasks.css.src,
                   '/*.{' + config.tasks.css.extensions + '}'),
  dest: path.join(config.root.dest, config.tasks.css.dest)
}

var routeSpecificCssPaths = {
  src: path.join(config.root.src, config.tasks.css.src, 'route-specific/**/*.scss'),
  dest: path.join(config.root.dest, config.tasks.css.dest)
}

var fontsPath = {
  src: [
    path.join(config.root.src, config.tasks.css.src, 'vendor/black-tie/black-tie.scss'),
    path.join(config.root.src, config.tasks.css.src, 'vendor/font-awesome/font-awesome.scss'),
    path.join(config.root.src, config.tasks.css.src, 'vendor/google/roboto.scss'),
  ],
  dest: path.join(config.root.dest, config.tasks.css.dest)
}

function cssTask() {

  var scssStream = gulp.src([cssPaths.src])
            .pipe(sass(config.tasks.css.sass))
            .on('error', handleErrors)
            .pipe(autoprefixer(config.tasks.css.autoprefixer))
            .pipe(gulp.dest(cssPaths.dest))
            .pipe(browserSync.stream())

  var routeSpecificScssStream = gulp
    .src(routeSpecificCssPaths.src, {base: 'src/stylesheets/route-specific'})
    .pipe(sass(config.tasks.css.sass))
    .on('error', handleErrors)
    .pipe(autoprefixer(config.tasks.css.autoprefixer))
    .pipe(gulp.dest(cssPaths.dest))
    .pipe(browserSync.stream())

  var fontsStream = gulp
    .src(fontsPath.src)
    .pipe(sass(config.tasks.css.sass))
    .on('error', handleErrors)
    .pipe(concat('external.css'))
    .on('error', handleErrors)
    .pipe(gulp.dest(fontsPath.dest))
    .pipe(browserSync.stream())

  return merge(scssStream, routeSpecificScssStream, fontsStream)
}

gulp.task('css', cssTask)
module.exports = cssTask
