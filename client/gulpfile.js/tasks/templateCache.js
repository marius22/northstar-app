// This task is for generating a template cache js file so that
// we don't need to make separate fetches for each template. We're not
// using this yet for various reasons, but I'm going to leave this task around
// so we can adapt it in the future.
var templateCache = require('gulp-angular-templatecache')
var gulp = require('gulp')
var path = require('path')
var config = require('../config')
var concat = require('gulp-concat')
var debug = require('gulp-debug')

if (!config.tasks.templateCache) {
  return
}

var paths = {
  src: path.join(config.root.src,
                 config.tasks.templateCache.src,
                 '**/*.html'),
  dest: path.join(config.root.src, config.tasks.templateCache.dest)
}

var templateModuleName = 'es-templates'
var templateCacheOptions = {
  module: templateModuleName,
  standalone: true,
  // customizations so our linter doesn't complain
  templateHeader: '/*eslint-disable */ \n' +
    '/* this file is generated during the build process from src/html/angular/*.html. \n' +
    'see templateCache.js for more details. */ \n' +
    'import angular from "angular"\n' +
    '(function (ng, undefined){ "use strict"; ' +
    'ng.module("<%= module %>"<%= standalone %>).run(["$templateCache", function($templateCache) {',
  templateFooter: '}]); })(angular); \n' +
    'module.exports = "' + templateModuleName + '"'
}


gulp.task('templateCache', function () {
  return gulp.src(paths.src)
    .pipe(debug({title: 'templateCache:'}))
    .pipe(templateCache(templateCacheOptions))
    .pipe(concat('angularTemplateCache.js'))
    .pipe(gulp.dest(paths.dest))
})
