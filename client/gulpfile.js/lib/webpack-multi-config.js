var config = require('../config')

var NGAnnotatePlugin = require('ng-annotate-webpack-plugin')
var path = require('path')
var webpack = require('webpack')
var WebpackManifest = require('./webpackManifest')

var _ = require('lodash')

module.exports = function (env) {
  var jsSrc = path.resolve(config.root.src, config.tasks.js.src)
  var jsDest = path.resolve(config.root.dest, config.tasks.js.dest)
  var modulesPath = path.resolve('./src/javascripts/modules/')
  var angularPath = path.resolve('./src/javascripts/angular/')
  var publicPath = path.join(config.tasks.js.dest, '/')
  var filenamePattern = env === 'production' ? '[name]-[hash].js' : '[name].js'
  var extensions = config.tasks.js.extensions.map(function (extension) {
    return '.' + extension
  })

  var webpackConfig = {
    context: jsSrc,
    plugins: [],
    resolve: {
      root: [jsSrc, modulesPath, angularPath],
      extensions: [''].concat(extensions)
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader'
        }
      ]
    },
    node: {
      // Needed for `handlebars`
      fs: 'empty'
    }
  }

  if (env === 'test') {
    var babelPresets = ['es2015', 'stage-0']

    // The root folder containing our tests. All our test cases
    // will go in this folder
    var testDir = 'src/javascripts/__tests__/'

    // Directory of javascript that we want to test. This is the
    // javascript whose coverage will be reported on.
    var srcDir = 'src/javascripts/modules/'
    webpackConfig = _.extend(webpackConfig, {
      // *optional* babel options: isparta will use it as well as babel-loader
      babel: {
        presets: babelPresets
      },
      // *optional* isparta options: istanbul behind isparta will use it
      // We use isparta to instrument our code so that our coverage tools
      // know how of our code is being covered
      isparta: {
        embedSource: true,
        noAutoWrap: true,
        // these babel options will be passed only to isparta and not to babel-loader
        babel: {
          presets: babelPresets
        }
      },
      module: {
        // We run babel to all of our test files so that we can write our tests with ES6.
        // transpile test files with babel as usual
        loaders: [
          // transpile test files with babel as usual
          {
            test: /\.js$/,
            include: path.resolve(testDir),
            loader: 'babel'
          }
        ],
        preLoaders: [
          // transpile and instrument only source code with
          // To instrument ES6 code with babel, we use the isparta-loader
          {
            test: /\.js$/,
            include: path.resolve(srcDir),
            loader: 'isparta'
          }
        ]
      }
    })
  } else {
    // Karma doesn't need entry points or output settings
    webpackConfig.entry = config.tasks.js.entries

    webpackConfig.output = {
      path: path.normalize(jsDest),
      filename: filenamePattern,
      publicPath: publicPath
    }

    if (config.tasks.js.extractSharedJs) {
      // Factor out common dependencies into a shared.js
      webpackConfig.plugins.push(
        new webpack.optimize.CommonsChunkPlugin({
          name: 'shared',
          filename: filenamePattern
        })
      )
    }
  }

  if (env === 'development') {
    // Makes for better error messages because you can read the original source
    // rather than the compiled source.
    // Commenting out for now because it slows down the build too much.
    /* webpackConfig.devtool = 'source-map'*/
    webpackConfig.watch = true
    webpack.debug = true
  }

  if (env === 'production') {
    webpackConfig.plugins.push(
      new WebpackManifest(publicPath, config.root.dest),
      new webpack.optimize.DedupePlugin(),
      new NGAnnotatePlugin({add: true}),
      // Turn off minification for now to help us debug things.
      // new webpack.optimize.UglifyJsPlugin(),
      new webpack.NoErrorsPlugin()
    )
  }

  // Makes ENV available in browser code.
  webpackConfig.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        ENV: JSON.stringify(process.env.ENV)
      }
    })
  )

  return webpackConfig
}
